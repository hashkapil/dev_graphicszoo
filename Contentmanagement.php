<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contentmanagement extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('myfunctions');
        $this->load->library('customfunctions');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Welcome_model');
        $this->load->model('Request_model');
        $this->load->model('Affiliated_model');
        $this->load->model('Stripe');
    }

    public function index() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Additional Modules" => base_url().'admin/Contentmanagement/index');
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/index');
        $this->load->view('admin/admin_footer');
    }

    public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "admin") {
            redirect(base_url());
        }
    }

    public function blog() {
        $this->myfunctions->checkloginuser("admin");
        $role_permissions['show_blogs_for_writer'] = $this->myfunctions->admin_role_permissions("show_blogs_for_writer");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Blogs " => base_url().'admin/Contentmanagement/blog'
        );
        if (!empty($_POST)) {
                //print_r($_POST);
                $_POST['created'] = date("Y-m-d H:i:s");
                if (!empty($_FILES)) {

                    $config = array(
                        'upload_path' => "public/img",
                        'allowed_types' => "jpg|png|jpeg|pdf",
                        'encrypt_name' => TRUE
                    );

                    $new_name = time() . $_FILES["blog_file"]['name']; // Instead of proof write your file name means form file name
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload("blog_file")) {
                        $data = array($this->upload->data());
                        $_POST['image'] = $data[0]['file_name'];
                    } else {
                        echo "not insert";
                        exit;
                        // If file is not submitted successfully..!
                    }
                }

                if ($this->Admin_model->insert_data('blog', $_POST)) {
                    $this->session->set_flashdata('message_success', "Blog Insert Successfully", 5);
                    redirect(base_url() . "admin/contentmanagement");
                } else {
                    $this->session->set_flashdata('message_error', "Blog Not Insert", 5);

                    redirect(base_url() . "admin/contentmanagement");
                }
                exit();
            }
        
        $data = $this->Admin_model->get_all_blog();
       
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/blog', array("data" => $data));
        $this->load->view('admin/admin_footer');
    }

    public function portfolio() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Portfolios &nbsp;" => base_url().'admin/Contentmanagement/portfolio'
        );
        if (!empty($_POST)) {
            if (isset($_POST['category'])) {

                $_POST['created'] = date("Y-m-d H:i:s");
                if (!empty($_FILES)) {
                    $config = array(
                        'upload_path' => "public/img",
                        'allowed_types' => "jpg|png|jpeg|pdf",
                        'encrypt_name' => TRUE
                    );

                    $new_name = time() . $_FILES["protfolio_file"]['name']; // Instead of proof write your file name means form file name
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload("protfolio_file")) {
                        $data = array($this->upload->data());
                        $_POST['image'] = $data[0]['file_name'];
                    } else {
                        echo "not insert";
                        exit;
                        // If file is not submitted successfully..!
                    }
                }
                if ($this->Admin_model->insert_data('protfolio', $_POST)) {
                    $this->session->set_flashdata('message_success', "Portfolio Insert Successfully", 5);
                    redirect(base_url() . "admin/contentmanagement");
                } else {
                    $this->session->set_flashdata('message_error', "Portfolio Not  Insert", 5);

                    redirect(base_url() . "admin/contentmanagement");
                }
                exit();
            } 
        }
        $protfolio_list = $this->Admin_model->get_protfolio_category();
        $dt = $this->Admin_model->get_all_portfolio();
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs,'edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/content_management/portfolio', array("protfolio_list" => $protfolio_list, "dt" => $dt));
        $this->load->view('admin/admin_footer');
    }

    public function email() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
            "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
            "Email Settings" => base_url().'admin/Contentmanagement/email'
        );
        $emailslist = $this->Admin_model->get_all_emails();
        //echo "<pre/>";print_R($emailslist);
        foreach ($emailslist as $email_header_footer) {
            if ($email_header_footer['email_slug'] == 'email_header') {
                $email_header_footer_sec['header_part_id'] = $email_header_footer['id'];
                $email_header_footer_sec['header_part'] = $email_header_footer['email_text'];
            } elseif ($email_header_footer['email_slug'] == 'email_footer') {
                $email_header_footer_sec['footer_part_id'] = $email_header_footer['id'];
                $email_header_footer_sec['footer_part'] = $email_header_footer['email_text'];
            }

            if ($email_header_footer['email_slug'] != 'email_header' && $email_header_footer['email_slug'] != 'email_footer') {
                $emailslist_temps[] = $email_header_footer;
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/email', array('emailslist' => $emailslist_temps, 'email_header_footer_sec' => $email_header_footer_sec));
        $this->load->view('admin/admin_footer');
    }

    public function addblog() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Blogs &nbsp; >" => base_url().'admin/Contentmanagement/blog',
        "Add blog " => base_url().'admin/Contentmanagement/addblog'
        );

        if (!empty($_POST)) {
                 
            $data1 = array();
            $ads_blog = $_POST['recomended_blog'];
            $data1['recommand_blog'] = implode(', ', $ads_blog);
            $text = $_POST['title'];
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $text)));
            $data1['slug'] = rtrim(substr($slug, 0, 70), "-");
            $data1['created'] = date("Y-m-d H:i:s");
            $data1['title'] = $_POST['title'];
            $data1['category'] = "Web";
            $data1['start_date'] = $_POST['start_date'];
            $data1['meta_title'] = $_POST['meta_title'];
            $data1['meta_description'] = $_POST['meta_description'];
            if($_POST['is_active'] == 'on'){
                $data1['is_active'] = 1;
            }else{
                $data1['is_active'] = 0;
            }
            $data1['body'] = $_POST['body'];

            if (!empty($_FILES)) {
                $new_name = time() . $_FILES["blog_file"]['name']; // Instead of proof write your file name means form file name
                $config = array(
                    'upload_path' => "public/uploads/blogs/",
                    'allowed_types' => array("jpg", "jpeg", "png", "pdf"),
                    'file_name' => $new_name
                );
                $check = $this->myfunctions->CustomFileUpload($config, 'blog_file');
                if ($check['status'] == 1) {
                    $data1['image'] = $new_name;
                } else {
                    $this->session->set_flashdata('message_error', "Please upload valid Image", 5);
                    redirect(base_url() . "admin/Contentmanagement/addblog");
                }
            }
//                                echo "<pre/>";print_r($_POST);
//                                echo "****************";
//              echo "<pre/>";print_r($data1);exit;
            if ($this->Admin_model->insert_data('blog', $data1)) 
            {
                if($_POST['is_preview']=="preview"){
                    // echo "<script type=\"text/javascript\">
                    // window.open( '".base_url()."article/".$data1['slug']."?preview=1', '_blank')
                    // </script>";
                    redirect(base_url() . "article/".$data1['slug']."?preview=1");
                    die(); 
                    
                }
                $this->session->set_flashdata('message_success', "Blog inserted successfully!", 5);
                redirect(base_url() . "admin/Contentmanagement/blog");
            } else {
                $this->session->set_flashdata('message_error', "Blog not inserted!", 5);

                redirect(base_url() . "admin/Contentmanagement/blog");
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $allblogs = $this->Admin_model->get_all_blog();
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/addblog', array('allblogs' => $allblogs));
        $this->load->view('admin/admin_footer');
    }

    public function addportfolio() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Portfolios &nbsp; >" => base_url().'admin/Contentmanagement/portfolio',
        "Add portfolio " => base_url().'admin/Contentmanagement/addportfolio'
        );
        if (!empty($_POST)) {
            if (isset($_POST['category'])) {
                $_POST['created'] = date("Y-m-d H:i:s");
                if (!empty($_FILES)) {
                    if (UPLOAD_FILE_SERVER == 'bucket') {
                        $new_name = time() . $_FILES["protfolio_file"]['name'];
                        $config = array(
                            'upload_path' => 'public/uploads/portfolios/',
                            'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                            'file_name' => $new_name
                        );
                        $check = $this->myfunctions->CustomFileUpload($config, 'protfolio_file');
                        $_POST['image'] = $new_name;
                        list($width, $height) = getimagesize($check['msg']);
                        $rowHeight = 20;
                        $rowGap = 10;
                        $rowSpan = ceil((int) ($height + $rowGap) / ($rowHeight + $rowGap));
                        $_POST['span_grid'] = $rowSpan;
                    } else {
                        $config = array(
                            'upload_path' => "public/uploads/portfolios/",
                            'allowed_types' => "jpg|png|jpeg|pdf",
                            'encrypt_name' => TRUE
                        );

                        $new_name = time() . $_FILES["protfolio_file"]['name']; // Instead of proof write your file name means form file name
                        $config['file_name'] = $new_name;

                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload("protfolio_file")) {
                            $data = array($this->upload->data());
                            $_POST['image'] = $data[0]['file_name'];
                        } else {
                            $this->session->set_flashdata('message_error', "Please upload valid Image", 5);
                            redirect(base_url() . "admin/Contentmanagement/addportfolio");
                            // If file is not submitted successfully..!
                        }
                    }
                }
                //echo "<pre>";print_r($check);exit;
                if ($this->Admin_model->insert_data('protfolio', $_POST)) {
                    $this->session->set_flashdata('message_success', "Portfolio inserted successfully!", 5);
                    redirect(base_url() . "admin/contentmanagement/portfolio");
                } else {
                    $this->session->set_flashdata('message_error', "Portfolio not inserted!", 5);
                    redirect(base_url() . "admin/contentmanagement/portfolio");
                }
            }
        }
        $all_portfolio_category = $this->Request_model->get_protfolio_category();
        $user_id = $_SESSION['user_id'];
        $Allportfolios = $this->Admin_model->get_all_portfolio();
        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/addportfolio', array('all_portfolio_category' => $all_portfolio_category, 'Allportfolios' => $Allportfolios));
        $this->load->view('admin/admin_footer');
    }

    public function blog_edit($id) {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Blogs &nbsp; >" => base_url().'admin/Contentmanagement/blog',
        "Edit blog " => base_url().'admin/Contentmanagement/blog_edit/'.$id
        );
                // echo "<pre>"; print_r($_POST); echo "</pre>"; 
                // die(); 
        if ($_POST) {
            $data1 = array();
            $ads_blog = $_POST['recomended_blog'];
            $data1['recommand_blog'] = implode(', ', $ads_blog);
//            echo "<pre/>";print_r($_FILES);exit;
            $text = $_POST['title'];
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $text)));
            $_POST['slug'] = rtrim(substr($slug, 0, 70), "-");
            $_POST['created'] = date("Y-m-d H:i:s");

            if(isset($_POST['is_active']) && $_POST['is_active'] =='on'){
                $_POST['is_active'] = 1;
            }else{
             $_POST['is_active'] = 0; 
            }
//            echo "<pre>";print_R($_POST);exit;
            $startdate = (isset($_POST['start_date']) && $_POST['start_date'] != "")?$_POST['start_date']:"";
            $updatearray = array('title' => $_POST['title'], 'slug' => $_POST['slug'], 'body' => $_POST['body'],'meta_title' => $_POST['meta_title'],'meta_description' => $_POST['meta_description'], 'category' => "Web",  'start_date' => $startdate,'modified' => date("Y-m-d H:i:s"), 'recommand_blog' => $data1['recommand_blog'],"is_active" =>$_POST['is_active']);
            if (isset($_FILES['blog_file']['name']) && $_FILES['blog_file']['name'] != "") {
                $new_name = time() . $_FILES["blog_file"]['name']; // Instead of proof write your file name means form file name
                $config = array(
                    'upload_path' => "public/uploads/blogs/",
                    'allowed_types' => array("jpg", "jpeg", "png", "pdf"),
                    'file_name' => $new_name
                );
                $check = $this->myfunctions->CustomFileUpload($config, 'blog_file');
//                echo "<pre/>";print_R($new_name);exit;
                if ($check['status'] == 1) {
                    $updatearray['image'] = $new_name;
                } else {
                    $this->session->set_flashdata('message_error', "Please upload valid Image", 5);
                    redirect(base_url() . "admin/Contentmanagement/blog_edit/".$id);
                }
            }

            if ($this->Admin_model->update_data('blog', $updatearray, array("id" => $id))) {
                $this->session->set_flashdata('message_success', "Blog information updated Successfully", 5);
                redirect(base_url() . "admin/contentmanagement/blog_edit/".$id);
            } else {
                $this->session->set_flashdata('message_error', "Blog information not updated Successfully", 5);
                redirect(base_url() . "admin/contentmanagement/blog_edit/".$id);
            }
        }
        $data['blogdata'] = $this->Request_model->get_blog_by_id($id);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $data['allblogs'] = $this->Admin_model->get_all_blog();
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/blog_edit', $data);
        $this->load->view('admin/admin_footer');
    }

    public function portfolio_edit($id) {
        $this->myfunctions->checkloginuser("admin");  
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Portfolios &nbsp; >" => base_url().'admin/Contentmanagement/portfolio',
        "Edit portfolio " => base_url().'admin/Contentmanagement/portfolio_edit/'.$id
        );
        if ($_POST) {
            $_POST['created'] = date("Y-m-d H:i:s");
            $updatearray = array('title' => $_POST['title'], 'position' => $_POST['position'], 'body' => $_POST['body'], 'modified' => date("Y-m-d H:i:s"), 'category' => $_POST['category']);
            if (isset($_FILES['protfolio_file']['name']) && $_FILES['protfolio_file']['name'] != "") {
                if (UPLOAD_FILE_SERVER == 'bucket') {
                    $new_name = time() . $_FILES["protfolio_file"]['name'];
                    $config = array(
                        'upload_path' => "public/uploads/portfolios/",
                        'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                        'file_name' => $new_name
                    );
                    $check = $this->myfunctions->CustomFileUpload($config, 'protfolio_file');
                    $updatearray['image'] = $new_name;
//                                          echo "<pre/>";print_r($check)."<br/>";
                    list($width, $height) = getimagesize($check['msg']);
                    $rowHeight = 20;
                    $rowGap = 10;
                    $rowSpan = ceil((int) ($height + $rowGap) / ($rowHeight + $rowGap));
                    $updatearray['span_grid'] = $rowSpan;
//                                        echo "<pre/>";print_r($height);exit;
                } else {
                    $config = array(
                        'upload_path' => "public/uploads/portfolios/",
                        'allowed_types' => "jpg|png|jpeg|pdf",
                        'encrypt_name' => TRUE
                    );

                    $new_name = time() . $_FILES["protfolio_file"]['name']; // Instead of proof write your file name means form file name
                    $config['file_name'] = $new_name;
                    //$imagepath = FS_PATH_PUBLIC_UPLOADS_PORTFOLIOS.$new_name;
                    //list($width, $height, $type, $attr) = getimagesize('http://dev.graphicszoo.com/public/uploads/portfolios/1df73112ef5e2ee48ee9ac1a5e24c176.jpg');
//                  echo $height;exit;
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload("protfolio_file")) {
                        $data = array($this->upload->data());
                        $updatearray['image'] = $data[0]['file_name'];
                    } else {
                        $this->session->set_flashdata('message_error', "Please upload valid Image", 5);
                        redirect(base_url() . "admin/Contentmanagement/portfolio_edit/" . $id);
                        // If file is not submitted successfully..!
                    }
                }
            }
//                                echo "<pre/>";print_R($_POST);exit;

            if ($this->Admin_model->update_data('protfolio', $updatearray, array('id' => $id))) {
                $this->session->set_flashdata('message_success', "Portfolio Update Successfully", 5);
                redirect(base_url() . "admin/contentmanagement/portfolio_edit/".$id);
            } else {
                $this->session->set_flashdata('message_error', "Portfolio Update Not Successfully", 5);
                redirect(base_url() . "admin/contentmanagement/portfolio_edit/".$id);
            }
        }
        $data['all_portfolio_category'] = $this->Request_model->get_protfolio_category();
        $data['portfoliodata'] = $this->Request_model->get_portfolio_by_id($id);
        $user_id = $_SESSION['user_id'];
        $data['Allportfolios'] = $this->Admin_model->get_all_portfolio();
        $data['selected_portfolio_category'] = $this->Admin_model->get_edit_protfolio_category($id);
        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/portfolio_edit', $data);
        $this->load->view('admin/admin_footer');
    }

    
    public function edit_blog() {
        $this->myfunctions->checkloginuser("admin");            
        if (!empty($_POST)) {

            if (isset($_POST['portfolio_category'])) {
                echo "no data";
            } else {
                //print_r($_POST);
                $_POST['created'] = date("Y-m-d H:i:s");
                $updatearray = array('title' => $_POST['title'], 'body' => $_POST['body'], 'category' => $_POST['category'], 'modified' => date("Y-m-d H:i:s"));
                if (isset($_FILES['blog_file']['name']) && $_FILES['blog_file']['name'] != "") {

                    $config = array(
                        'upload_path' => "public/img",
                        'allowed_types' => "jpg|png|jpeg|pdf",
                        'encrypt_name' => TRUE
                    );

                    $new_name = time() . $_FILES["blog_file"]['name']; // Instead of proof write your file name means form file name
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload("blog_file")) {
                        $data = array($this->upload->data());
                        $updatearray['image'] = $data[0]['file_name'];
                    } else {
                        echo "not insert";
                        exit;
                        // If file is not submitted successfully..!
                    }
                }

                if ($this->Admin_model->update_data('blog', $updatearray, array('id' => $_POST['id']))) {
                    $this->session->set_flashdata('message_success', "Update Successfully", 5);
                    redirect(base_url() . "admin/contentmanagement");
                } else {
                    $this->session->set_flashdata('message_error', "Update Not Successfully", 5);

                    redirect(base_url() . "admin/contentmanagement");
                }
                exit();
            }
        }
    }

    public function delete_blog($id) {
        $this->myfunctions->checkloginuser("admin");
        if ($id) {
            if ($this->Admin_model->delete_blog("blog", $id)) {
                $this->session->set_flashdata("message_success", "Blog Deleted Successfully..!");
            } else {
                $this->session->set_flashdata("message_error", "Blog Not Deleted Successfully..!");
            }
        }
        redirect(base_url() . "admin/contentmanagement/blog");
    }

    public function delete_portfolio($id = "") {
        $this->myfunctions->checkloginuser("admin");
        if ($id) {
            $portfoliodata = $this->Request_model->get_portfolio_by_id($id);
            if ($this->Admin_model->delete_blog("protfolio", $id)) {
                $src_filename = "public/uploads/portfolios/" . $portfoliodata[0]['image'];
                $this->load->library('s3_upload');
                $is_unlink = $this->s3_upload->delete_file_from_s3($src_filename);
                $this->session->set_flashdata("message_success", "Portfolio Deleted Successfully..!");
            } else {
                $this->session->set_flashdata("message_error", "Portfolio Not Deleted Successfully..!");
            }
        }
        redirect(base_url() . "admin/contentmanagement/portfolio");
    }

    /* add portfolio catgeory */
    /*  public function add_category(){

      $this->myfunctions->checkloginuser("admin");

      if(!empty($_POST))
      {
      //print_r($_POST);
      $_POST['created'] = date("Y-m-d H:i:s");
      if(!empty($_FILES))
      {
      $config = array(
      'upload_path' => "public/img",
      'allowed_types' => "jpg|png|jpeg|pdf",
      'encrypt_name'=>TRUE
      );

      $new_name = time().$_FILES["protfolio_file"]['name']; // Instead of proof write your file name means form file name
      $config['file_name'] = $new_name;

      $this->load->library('upload', $config);
      if($this->upload->do_upload("protfolio_file"))
      {
      $data = array($this->upload->data());
      $_POST['image'] = $data[0]['file_name'];

      }
      else
      {
      echo "not insert";
      exit;
      // If file is not submitted successfully..!
      }
      }

      // echo "<pre>"; print_r($_POST); echo "</pre>";
      // die();

      if($this->Admin_model->insert_data('protfolio_categories',$_POST))
      {
      $this->session->set_flashdata('message_success', "Portfolio category Insert Successfully", 5);
      redirect(base_url()."admin/contentmanagement");
      }else
      {
      $this->session->set_flashdata('message_error', "Portfolio category Not Insert", 5);

      redirect( base_url()."admin/contentmanagement");
      }
      exit();
      //    }
      }
      $protfolio_list = $this->Admin_model->get_protfolio_category();
      $data = $this->Admin_model->get_all_blog();
      $dt = $this->Admin_model->get_all_portfolio();
      $this->load->view('admin/admin_header_1');
      $this->load->view('admin/content_management/index',array("data"=>$data,"protfolio_list" => $protfolio_list,"dt" => $dt));
      $this->load->view('admin/admin_footer');

      } */

    public function client_projects() {
        $this->myfunctions->checkloginuser("admin");

        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/request/client_projects');
        $this->load->view('admin/admin_footer');
    }

    public function edit_customer($customer_id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($customer_id == "") {
            redirect(base_url() . "admin/all_customer");
        }
        $customer = $this->Admin_model->getuser_data($customer_id);
        if (empty($customer)) {
            $this->session->set_flashdata('message_error', 'No Customer Found.!', 5);
            redirect(base_url() . "admin/all_customer");
        }
        $subscription_plan_list = array();
        $subscription = array();
        $planid = "";
        if ($customer[0]['current_plan'] != "") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            print_r($getcurrentplan);
            if (!empty($getcurrentplan)) {
                $plandetails = $getcurrentplan->items->data[0]->plan;
                $planid = $plandetails->id;
            } else {
                $plandetails = "";
                $planid = "";
            }
        }
        $allplan = $this->Stripe->getallsubscriptionlist();
        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }
        $carddetails = array();
        $carddetails['last4'] = "";
        $carddetails['exp_month'] = "";
        $carddetails['exp_year'] = "";
        $carddetails['brand'] = "";
        $plan = array();
        if ($customer[0]['current_plan'] != "" && $customer[0]['current_plan'] != "0") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            if (!empty($getcurrentplan)) {
                $plan['userid'] = $getcurrentplan->id;
                $plan['current_period_end'] = $getcurrentplan->current_period_end;
                $plan['current_period_start'] = $getcurrentplan->current_period_start;
                $plan['billing'] = $getcurrentplan->billing;
                $plan['trial_end'] = $getcurrentplan->trial_end;
                $plan['trial_start'] = $getcurrentplan->trial_start;


                $plandetails = $getcurrentplan->items->data[0]->plan;
                $plan['planname'] = $plandetails->name;
                $plan['planid'] = $plandetails->id;
                $plan['interval'] = $plandetails->interval;
                $plan['amount'] = $plandetails->amount / 100;
                $plan['created'] = $plandetails->created;

                $customerdetails = $this->Stripe->getcustomerdetail($customer[0]['current_plan']);
                if (!empty($customerdetails)) {
                    $carddetails['last4'] = $customerdetails->sources->data[0]->last4;
                    $carddetails['exp_month'] = $customerdetails->sources->data[0]->exp_month;
                    $carddetails['exp_year'] = $customerdetails->sources->data[0]->exp_year;
                    $carddetails['brand'] = $customerdetails->sources->data[0]->brand;
                }
            }
        } else {
            $plan['planid'] = "";
        }
        $designer_list = $this->Admin_model->get_total_customer("designer");
        $designs_requested = $this->Admin_model->get_all_requested_designs(array(), $customer_id, "no");
        $designs_approved = $this->Admin_model->get_all_requested_designs(array("approved"), $customer_id, "", "", "", "status_admin");
        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/edit_customer', array("customer" => $customer, "subscription" => $subscription, "planid" => $planid, "carddetails" => $carddetails, "designer_list" => $designer_list, "designs_requested" => $designs_requested, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function view_request($id) {
        $this->myfunctions->checkloginuser("admin");
        if ($id == "") {
            redirect(base_url() . "admin/all_requests");
        }
        $request = $this->Admin_model->get_all_requested_designs(array(), "", "", $id);

        //$this->RequestDiscussions->updateAll(array('admin_seen' => 1),array('request_id' => $request_id));
        $request_id = $request[0]['id'];
        if ($_POST) {

            if (isset($_POST['changestate'])) {

                if ($_POST['state_id'] != '0') {
                    $update_data = array('status' => $_POST['state_id'], 'status_designer' => $_POST['state_id'], 'status_admin' => $_POST['state_id'], 'modified' => date("Y-m-d h:i:s"));
                    if ($_POST['state_id'] == "active") {
                        $update_data['dateinprogress'] = date("Y-m-d h:i:s");
                    }
                    if ($_POST['state_id'] == "disapprove") {
                        $update_data['dateinprogress'] = date("Y-m-d h:i:s");
                    }
                    if ($this->Requests->updateAll($update_data, array('id' => $request_id))) {
                        $this->Flash->success(__('State Change Successfully.'));
                    } else {
                        $this->Flash->error(__('State is not Changed'));
                    }
                } else {
                    $this->Flash->error(__('Please Select State.'));
                }
                $this->redirect();
            } else {
                if ($_POST['designer_id'] != "" && $_POST['designer_id'] != 0) {
                    if ($request->id) {
                        $designer_id = $_POST["designer_id"];
                        $request_id = $request->id;
                        if ($request->status == "pending" || $request->status == "running" || $request->status == "active" || $request->status == "assign" || $request->status == "pendingforapprove" || $request->status == "checkforapprove") {
                            if (isset($_POST['assigndesigner'])) {

                                $activerequest = $this->Requests
                                        ->find()
                                        ->where(['Requests.status' => "active", 'Requests.customer_id' => $request->customer_id, 'Requests.id !=' => $request_id])
                                        ->all()
                                        ->toArray();


                                if (sizeof($activerequest) > 0) {
                                    if ($this->Requests->updateAll(array('designer_id' => $designer_id, 'status' => 'assign', 'status_designer' => 'assign', 'status_admin' => 'assign', 'modified' => date("Y-m-d h:i:s")), array('id' => $request_id))) {
                                        $this->Flash->success(__('Designer Assign Successfully.'));
                                        //return $this->redirect(\Cake\Routing\Router::url($this->referer('/', TRUE), TRUE));
                                    } else {
                                        $this->Flash->error(__('Not Savesss.'));
                                    }
                                } else {
                                    if ($this->Requests->updateAll(array('designer_id' => $designer_id, 'status' => 'active', 'status_designer' => 'active', 'status_admin' => 'active', 'modified' => date("Y-m-d h:i:s"), 'dateinprogress' => date("Y-m-d h:i:s")), array('id' => $request_id))) {
                                        $this->Flash->success(__('Designer Assign Successfully.'));
                                        //return $this->redirect(\Cake\Routing\Router::url($this->referer('/', TRUE), TRUE));
                                    } else {
                                        $this->Flash->error(__('Not Savesss.'));
                                    }
                                }
                            } elseif (isset($_POST["activedesigner"])) {
                                if ($this->Requests->updateAll(array('status' => "active", 'status_designer' => 'active', 'dateinprogress' => date("Y-m-d h:i:s")), array('id' => $request_id))) {
                                    $this->Flash->success(__('Designer Active Successfully.'));
                                    //return $this->redirect(\Cake\Routing\Router::url($this->referer('/', TRUE), TRUE));
                                } else {
                                    $this->Flash->error(__('Not Save.'));
                                }
                            }
                        } elseif ($request->status == "active") {
                            $this->Flash->error(__('Design Already in Progress.'));
                        }
                    }
                } else {
                    $this->Flash->error(__('Please choose Designer. Please, try again.'));
                }
            }
        }

        $request_files = $this->Admin_model->get_requested_files($request_id, $request[0]['customer_id'], "customer");

        $admin_request_files = $this->Admin_model->get_requested_files($request_id, "", "designer");

        $customer_additional_files = $this->Admin_model->get_requested_files($request_id, "", "customer");

        $admin_files = $this->Admin_model->get_requested_files($request_id, "", "admin");

        $designer_list = $this->Admin_model->get_total_customer("designer");

        $request = $this->Admin_model->get_all_requested_designs(array(), "", "", $id, "");

        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/view_request', array("request" => $request, "request_files" => $request_files, "admin_request_files" => $admin_request_files, "designer_list" => $designer_list, "customer_additional_files" => $customer_additional_files, "admin_files" => $admin_files));
        $this->load->view('admin/admin_footer');
    }

    public function all_designer() {
        $this->myfunctions->checkloginuser("admin");
        $designers = $this->Admin_model->get_total_customer("designer");

        for ($i = 0; $i < sizeof($designers); $i++) {
            $customer = $this->Admin_model->get_total_customer("customer", $designers[$i]['id']);
            $designer_customer[$designers[$i]['id']] = sizeof($customer);
        }
        //print_r($designers); print_r($designer_customer);

        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/all_designer', array("designers" => $designers, "designer_customer" => $designer_customer));
        $this->load->view('admin/admin_footer');
    }

    public function view_designer($designer_id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($designer_id == "") {
            redirect(base_url() . "admin/all_designer");
        }
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email'], $designer_id);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture1"]["name"]) && !empty($_FILES["profile_picture1"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture1"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture1")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/view_designer/" . $designer_id);
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "profile_picture" => $_POST['profile_picture'],
                "modified" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->update_data("users", $data, array('id' => $designer_id));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Updated Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            } else {
                $this->session->set_flashdata('message_error', 'Designer Updated Not Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
        }
        $designer = $this->Admin_model->getuser_data($designer_id);

        $designs_queue = $this->Admin_model->get_all_requested_designs("", "", "", "", $designer_id, "status_admin");

        $designs_approved = $this->Admin_model->get_all_requested_designs("approved", "", "", "", $designer_id, "status_admin");

        $customer = $this->Admin_model->get_total_customer("customer", $designer_id);

        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/view_designer', array("designer" => $designer, "designs_queue" => $designs_queue, "customer" => $customer, "designer" => $designer, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function add_designer() {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email']);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/add_designer");
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture"]["name"]) && !empty($_FILES["profile_picture"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/add_designer");
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "new_password" => $_POST['password'],
                "profile_picture" => $_POST['profile_picture'],
                "role" => "designer",
                "created" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->insert_data("users", $data);
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Added Not Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            }
        }
        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/add_designer');
        $this->load->view('admin/admin_footer');
    }

    public function add_customer() {
        $this->myfunctions->checkloginuser("admin");
        $allplan = $this->Stripe->getallsubscriptionlist();

        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }

        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['password'] = $_POST['password'];

            $customer['current_plan'] = $_POST['subscription_plans_id'];

            $customer['role'] = "customer";


            //$stripe_controller = new StripesController();
            $stripe_customer = $stripe_controller->createCustomer($customer->email);
            if (!$stripe_customer['status']) {

                $cusomer_save = $this->Welcome_model->insert_data("users", $customer);
                //$customer->current_plan = $stripe_customer['data']['customer_id'];
                if ($cusomer_save <= 0) {
                    $this->Flash->error('Customer cannot ne created.');
                    return $this->redirect(Router::url(['action' => 'index'], TRUE));
                }


                $this->Users->delete($customer);
                $this->Flash->error($stripe_customer['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }

            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($this->request->getData('last4'));
            $expiry = explode('/', $this->request->getData('exp_month'));
            $expiry_month = $expiry[0];
            $expiry_year = $expiry[1];
            $cvc = $this->request->getData('cvc');



            $stripe_card = $stripe_controller->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);

            if (!$stripe_card['status']) {
                $this->Users->delete($customer);
                $this->Flash->error($stripe_card['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }

            $subscription_plan_id = $this->request->getData('subscription_plans_id');

            if ($subscription_plan_id) {
                $stripe_subscription = '';
                $stripe_subscription = $stripe_controller->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id);
            }

            //Add Plan to a customer
            if (!$stripe_subscription['status']) {
                $this->Users->delete($customer);
                $this->Flash->error($stripe_subscription['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }


            $customer->current_plan = $stripe_subscription['data']['subscription_id'];

            $customer->customer_id = $stripe_customer['data']['customer_id'];
            $customer->plan_name = $this->request->getData('subscription_plans_id');
            $plandetails = $stripe_controller->retrieveoneplan($this->request->getData('subscription_plans_id'));
            $customer->plan_amount = $plandetails['amount'] / 100;
            $customer->plan_interval = $plandetails['interval'];
            $customer->plan_trial_period_days = $plandetails['trial_period_days'];
            $customer->plan_turn_around_days = $plandetails['metadata']->turn_around_days;

            $is_customer_save = $this->Users->save($customer);
            // $is_customer_save = "1";

            if ($is_customer_save) {
                $email_data = new \stdClass();
                $email_data->to_email = $customer->email;
                $email_data->subject = 'Welcome to Graphics Zoo!';
                $html = "";

                $html .= '<div style="background: #fff;">
                        <div style="width:800px;border: 2px solid #f23f5b;margin: auto;border-radius:20px;">
                                <div style="text-align:center;margin-bottom: 20px;">
                                        <img src="' . SITE_IMAGES_URL . 'img/logo.png" height=80px; style="padding: 25px;"></img>
                                </div>
                                <div style="margin-top:10px;">
                                        <p style="padding-left: 25px;">Hi ' . $customer->first_name . ', </p>
                                </div>
                                <div style="margin-top:10px;text-align:justify;">
                                        <p style="padding: 0px 25px;">We are very excited that you have decided to join us for your design needs. As you get started, expect to have your designs automatically assigned to the best suited designer. you will continue working with that dedicated designer for all your needs. if you have any questions or concerns please fell free to contact us at <u>hello@graphicszoo.com</u> and our team will find a solution for you. We are here to help you with all your needs.</p>
                                </div>
                                <div style="margin-top:20px;text-align:justify;;padding-bottom:5px;">
                                        <p style="padding: 0px 25px;">Now, Let\'s go your first design request started. Click below to login and submit a request.</p>
                                </div>
                                <div style="margin-top:10px;text-align:justify;display: flex;">
                                        <a href="http://graphicszoo.com/login" style="margin: auto;"><button style="padding:10px;font-size:22px;font-weight:600;color:#fff;background:#f23f5b;border: none;margin: auto;">Request Design</button></a>
                                </div>

                                <div style="margin-top:10px;text-align:justify;display: flex;">
                                        <p style="padding-left: 25px;">Thank you,</p>
                                </div>

                                <div style="text-align:justify;display: flex;">
                                        <p style="padding-left: 25px;">Andrew Jones</br>Customer Relations</p>
                                </div>
                        </div>
                </div>';
                $email_data->template = $html;

                $this->sendMail($email_data);
                // exit;
                $plan_amount = 0;

                switch ($subscription_plan_id):
                    case STRIPE_MONTH_SILVER_PLAN_ID:
                        $plan_amount = STRIPE_MONTH_SILVER_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_MONTH_GOLDEN_PLAN_ID:
                        $plan_amount = STRIPE_MONTH_GOLDEN_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_YEAR_SILVER_PLAN_ID:
                        $plan_amount = STRIPE_YEAR_SILVER_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_YEAR_GOLDEN_PLAN_ID:
                        $plan_amount = STRIPE_YEAR_GOLDEN_PLAN_AMOUNT / 100;
                        break;
                    default :
                        break;
                endswitch;

                $this->request->session()->write('plan_amount', $plan_amount);

                $this->Flash->success('User created successfully and subscribed successfully.');

                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }else {
                $this->Flash->error('User cannot be created.');
            }
        }
        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/add_customer', array("subscription" => $subscription));
        $this->load->view('admin/admin_footer');
    }

    public function search_ajax_blog() {
        $this->myfunctions->checkloginuser("admin");
        $dataArray = $this->input->get('title');
        if ($dataArray != "") {
            $data = $this->Request_model->get_blog_ajax(array('keyword' => $dataArray));
        } else {
            $data = $this->Admin_model->get_all_blog();
        }
        ?>
        <?php for ($i = 0; $i < sizeof($data); $i++): 
           $data['blog'] = $data[$i];  
           $this->load->view('admin/content_management/blog_listing_template',$data); 
        endfor; ?>
        <?php
    }

    public function search_ajax_portfolio() {
        $this->myfunctions->checkloginuser("admin");
        $dataArray = $this->input->get('title');
        if ($dataArray != "") {
            $dt = $this->Request_model->get_portfolio_ajax(array('keyword' => $dataArray));
        } else {
            $dt = $this->Admin_model->get_all_portfolio();
        }
        ?>
        <?php for ($i = 0; $i < sizeof($dt); $i++):
          $data['portfolio'] = $dt[$i];  
          $this->load->view('admin/content_management/portfolio_listing_template',$data);  
        endfor;
    }
    
    public function billing_subscription(){
         $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Billing & Subscription" => base_url().'admin/Contentmanagement/billing_subscription',
        );
        $subscriptionplan = $this->Request_model->getAllSubscriptionPlan();
       $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
       $this->load->view('admin/content_management/billing_subscription', array('subscriptionplan' => $subscriptionplan));
       $this->load->view('admin/admin_footer'); 
    }
    
    public function edit_plan($id) {
            $this->myfunctions->checkloginuser("admin");
            $plan_id = isset($_POST['plan_id']) ? $_POST['plan_id'] : '';
            $allusers_info = $this->Request_model->get_user_by_plan_name($plan_id);
            $global_active_request = isset($_POST['global_active_request']) ? $_POST['global_active_request'] : '';
            $global_inprogress_request = isset($_POST['global_inprogress_request']) ? $_POST['global_inprogress_request'] : '';
            $active_request = isset($_POST['active_request']) ? $_POST['active_request'] : '';
            $total_sub_user = isset($_POST['how_many_user']) ? $_POST['how_many_user'] : '';
            $filemanagement = isset($_POST['file_management']) ? $_POST['file_management'] : 0;
            $file_sharing = isset($_POST['file_sharing']) ? $_POST['file_sharing'] : 0;
            $brand_profile_count = isset($_POST['how_many_brandpro']) ? $_POST['how_many_brandpro'] : '';
            $turn_around_days = isset($_POST['turn_around_days']) ? $_POST['turn_around_days'] : 1;
            $overwite_setting = isset($_POST['overwite_setting']) ? $_POST['overwite_setting'] : 1;
            $is_agency = isset($_POST['is_agency']) ? $_POST['is_agency'] : 1;
            $globarray = array(
                'global_active_request' => $global_active_request,
                'global_inprogress_request' => $global_inprogress_request,
                'active_request' => $active_request,
                'total_sub_user' => $total_sub_user,
                'brand_profile_count' => $brand_profile_count,
                'turn_around_days' => $turn_around_days,
                'file_management' => $filemanagement,
                'file_sharing' => $file_sharing,
                'is_agency' => $is_agency,
                'modified' => date("Y:m:d H:i:s"));
            $user_data = array('total_active_req' => $global_active_request,
                'total_inprogress_req' => $global_inprogress_request,
                'total_requests' => $active_request,
                'turn_around_days' => $turn_around_days,
                'modified' => date("Y:m:d H:i:s"));
            $user_data_info = array('brand_profile_count' => $brand_profile_count,
                'sub_user_count' => $total_sub_user,
                'file_management' => $filemanagement,
                'file_sharing' => $file_sharing,
                'modified' => date("Y:m:d H:i:s"));
            if (!empty($_POST)) {
                $success = $this->Admin_model->update_plan("subscription_plan", $globarray, $id);
                if($success) {
                    if (!empty($allusers_info)) {
                        if ($overwite_setting == 1) {
                            foreach ($allusers_info as $allusersInfo) {
                            $update = $this->Admin_model->update_data("users", $user_data, array("plan_name" => $plan_id));
                                if($update && $allusersInfo['overwrite'] == '1'){
                                 $user_info = $this->Admin_model->getextrauser_data($allusersInfo['id']);
                                    if (empty($user_info)) {
                                       $user_data_info['user_id'] = $allusersInfo['id'];
                                       $user_data_info['created'] = date("Y:m:d H:i:s");
                                       $this->Welcome_model->insert_data("users_info", $user_data_info);
                                   } else {
                                       $this->Welcome_model->update_data("users_info", $user_data_info, array('user_id' => $allusersInfo['id']));
                                   }
                               }
                            }
                        } else {
                            foreach ($allusers_info as $allusersInfo) {
                              $this->Admin_model->update_data("users", $user_data, array("plan_name" => $plan_id, "overwrite" => 0));

                            }
                        }
                    }
                    $this->session->set_flashdata("message_success", "Plan Updated Successfully..!");
                    redirect(base_url() . "admin/Contentmanagement/billing_subscription");
                } else {
                    $this->session->set_flashdata("message_error", "Plan Not Updated Successfully..!");
                    redirect(base_url() . "admin/Contentmanagement/billing_subscription");
                }
            }
            
        }
    
    public function notifications(){
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Notifications" => base_url().'admin/Contentmanagement/notifications',
        );
        $countnotifications = $this->Request_model->get_notifications($_SESSION['user_id'],true,'','',true,'shown');
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id'],false,true,'',true,'shown');
        for ($i = 0; $i < sizeof($notifications); $i++) { 
        $notifications[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($notifications[$i]['profile_picture']);
        }
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/notifications', array('notifications' => $notifications,'countnotifications' => $countnotifications));
        $this->load->view('admin/admin_footer');    
    }
    
    public function load_more_notifications(){
        $this->myfunctions->checkloginuser("admin");
        $group_no = $this->input->post('group_no');
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
//        echo "<pre/>";print_R($_POST);exit;
        $countnotifications = $this->Request_model->get_notifications($_SESSION['user_id'],true,$start, $content_per_page,true,'shown');
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id'],false,$start, $content_per_page,true,'shown');
        for ($i = 0; $i < sizeof($notifications); $i++) { 
        $notifications[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($notifications[$i]['profile_picture']);
        }
        foreach ($notifications as $nk => $nv) { 
        ?>
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard/view_files/<?php echo $nv['url']; ?>">
                    <div class="setnoti-fication">
                        <figure class="pro-circle-k1">
                            <img src="<?php echo $nv['profile_picture']; ?>" class="img-responsive">
                        </figure>
                        <div class="notifitext">
                             <p>
                                <strong><?php echo $nv['first_name']. ' '. $nv['last_name'];?></strong>
                                <span class="time-date">
                                    <?php echo date('M d, Y H:i A',strtotime($nv['created']));?> 
                                </span>  
                            </p>
                            <p><?php echo $nv['title'];?></p>
                        </div>
                    </div>
                </a>
            </li>
        <?php } 
    }
    
    public function changeuserandbrand_tooverwrite() {
        $users = $this->Admin_model->overwriteuserdata();
        foreach ($users as $user){
            $user = array('user_id' => $user['id'],
                'sub_user_count' => $user['total_sub_user'],
                'brand_profile_count' => $user['brand_profile_count'],
                'modified' => date("Y:m:d H:i:s"),
                'created' => date("Y:m:d H:i:s"));
            $this->Admin_model->insert_data('users_info', $user);
        }
    }
    
    public function onlytimezone($date_zone,$dateformat="") {
            $this->myfunctions->checkloginuser("admin");
            $nextdate = $this->myfunctions->onlytimezoneforall($date_zone,'',$dateformat);
            return $nextdate;
    }
        
    public function abandoned_users_list() {
        $this->myfunctions->checkloginuser("admin");
        $data = array();
       // $breadcrumbs = array("Dashboard &nbsp; >" => base_url() . 'admin/dashboard', "Abandoned Users" => base_url() . 'admin/dashboard/abandoned_users_list');
        $data["users"] = $this->Welcome_model->select_data($select="*",$table="cart",$where="email!=''","","","DESC","created_at");
        $timeZone = array();
        foreach ($data["users"] as $key => $value){
             $timeZone[$key]["created_at"] = $this->onlytimezone($value["created_at"]);
             $timeZone[$key]['modified_at'] = $this->onlytimezone($value["modified_at"]);
                    
        } 
        
        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/content_management/abandoned_user',array("data" => $data,"timezone"=>$timeZone));
        $this->load->view('admin/admin_footer');
    }
    public function abandoned_loadmore() {
        $limit = $this->input->post("limit");
        $start = $this->input->post("start");  
        $timeZone = array();
        $data = $this->Welcome_model->select_data($select="*",$table="cart",$where="email!=''",$limit,$start,'DESC','created_at');
        foreach ($data as $key => $value){
             $timeZone[$key]["created_at"] = $this->onlytimezone($value["created_at"]);
             $timeZone[$key]['modified_at'] = $this->onlytimezone($value["modified_at"]);

        }
        $dataArr = array("data"=>$data,"timezone" =>$timeZone);
        echo json_encode($dataArr); die; 

    }
    public function download_abonededform( ) {
        $this->myfunctions->checkloginuser("admin");
        $pending_form = $this->Welcome_model->select_data("first_name,last_name,email,state,created_at","cart",$where="email!=''","","","DESC","created_at");
        $filename = 'form_' . date('Ymd') . '.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
        // file creation 
        $file = fopen('php://output', 'w');

        $header = array("First Name","Last Name","Email","State","Created");
        fputcsv($file, $header);
        foreach ($pending_form as $key => $line) {
            fputcsv($file, $line);
        }
        fclose($file);
        exit;
    }
    /**************************Project Reports*****************************************************/
    public function projectReports(){
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Project Reports" => base_url().'admin/Contentmanagement/projectReports',
        );
        $averagerevisionperdesign = $this->Admin_model->averageRevisionperDesign();
        $countrev = $this->Admin_model->SumofRevisionbasedonNumber(AVG_REVISION_NUMBER);
        $countqualityrev = $this->Admin_model->SumofQualityRevisionbasedonNumber(AVG_REVISION_NUMBER);
        $delayedprojectcount = $this->Admin_model->DelayedProjectsCount();
        $lateProjectsCount = $this->Admin_model->lateProjectsCount();
        $unverifiedProjectsCount = $this->Admin_model->unverifiedProjectsCount();
         $activeprojectscount = $this->Admin_model->activeProjectsCount();
        $activenotapprovedProjectsCount = $this->Admin_model->activebutnotcompletedProjectsCount();
        $currentdate = $this->onlytimezone(date('M d, Y h:i A'));
//        echo "<pre/>";print_R($activenotapprovedProjectsCount);
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/project_reports', 
            array('averagerevisionperdesign' => $averagerevisionperdesign,'countrev' => $countrev,'countqualityrev' => $countqualityrev,
            'lateProjectsCount' => $lateProjectsCount,'unverifiedProjectsCount'=> $unverifiedProjectsCount,
            'delayedprojectcount' => $delayedprojectcount,'activenotapprovedProjectsCount' => $activenotapprovedProjectsCount,
             'currentdate' => $currentdate,'activeprojectscount' => $activeprojectscount));
        $this->load->view('admin/admin_footer');  
     }
     
     public function view_projects_basedonslug($slug,$num = NULL){
        $num = (isset($num) && $num != '') ? $num : '';
        $data = array();
        $currentdate = $this->onlytimezone(date('M d, Y h:i A'));
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Project Reports &nbsp; >" => base_url().'admin/Contentmanagement/projectReports',
        "Project Listing" => base_url().'admin/Contentmanagement/view_projects_basedonslug',
        );
        $getprojects = $this->Request_model->projectReportAdminlisting($slug,$num);
        $countallprojects =  $this->Request_model->projectReportAdminlisting($slug,$num,true);
        if($slug == 'revisions_count'){
            $desc_basedon_slug = "Count of projects gone to in-revision by client more than ".($num - 1). " time";
        }elseif($slug == 'quality_revision_count'){
            $desc_basedon_slug = "Count of projects quality failed by admin/QA more than ".($num - 1). " time";
        }elseif($slug == 'delayed_project'){
            $desc_basedon_slug = "Undelivered expired projects of today + delivered projects today after expired datetime";
        }elseif($slug == 'late_project'){
            $desc_basedon_slug = "Active projects having expected date greater than current datetime (".$currentdate.")";
        }elseif($slug == 'unverified'){
            $desc_basedon_slug = "Active projects that are not verified";
        }elseif($slug == 'activepro'){
            $desc_basedon_slug = "Total number of projects having today's expected date (".date('M d, Y',strtotime($currentdate)).")";
        }else{
            $desc_basedon_slug = "Total number of projects with today's expected date and delivered today (".date('M d, Y',strtotime($currentdate)).")";
        }
//        echo "<pre/>";print_R($getprojects);
        foreach ($getprojects as $key => $value){
//            echo "<pre/>";print_R($value);
            if($value['status_admin'] == 'active' || $value['status_admin'] == 'disapprove'){
               $datetime =  $this->onlytimezone($value['expected_date']);
            }elseif($value['status_admin'] == 'pendingrevision'){
               $datetime =  $this->onlytimezone($value['modified']);
            }elseif($value['status_admin'] == 'assign'){
               $datetime =  $this->onlytimezone($value['created']);
            }elseif($value['status_admin'] == 'checkforapprove'){
               $datetime =  $this->onlytimezone($value['modified']);
            }elseif($value['status_admin'] == 'approved'){
               $datetime =  $this->onlytimezone($value['approvaldate']);
            }elseif($value['status_admin'] == 'hold'){
               $datetime =  $this->onlytimezone($value['modified']);
            }else{
               $datetime =  $this->onlytimezone($value['modified']);
            }
            $data['reports_projects'][$key] = $this->myfunctions->adminVariables($value['status_admin'],$datetime,1);
            $data['reports_projects'][$key]['title'] = $value['title'];
            $data['reports_projects'][$key]['id'] = $value['id'];
            if($slug == 'revisions_count'){
               $data['reports_projects'][$key]['count_revision'] = $value['count_customer_revision']; 
            }elseif($slug == 'quality_revision_count'){
               $data['reports_projects'][$key]['count_revision'] = $value['count_quality_revision'];
            }
            //echo "<pre/>";print_R($data['revision_pro']);
        }
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/view_projects_basedonslug', array('slug' => $slug,'num' => $num,'reports_projects' => $data['reports_projects'],
            'countprojects' => count($data['reports_projects']),'countallprojects' => $countallprojects,'desc_basedon_slug' => $desc_basedon_slug));
        $this->load->view('admin/admin_footer');   
     }
     
     public function load_more_reports(){
        $this->myfunctions->checkloginuser("admin");
        $slug = $this->input->post('slug');
        $num = $this->input->post('num');
        $group_no = $this->input->post('group_no');
        $search = ($this->input->post('search') != "") ? $this->input->post('search') : $loadsearch;
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $getprojects = $this->Request_model->projectReportAdminlisting($slug,$num,'',$start,$content_per_page,$search);
        $countallprojects =  $this->Request_model->projectReportAdminlisting($slug,$num,true,$start,$content_per_page,$search);
//         echo "<pre/>";print_R($getprojects);
        foreach ($getprojects as $key => $value){
//            echo "<pre/>";print_R($value);
            if($value['status_admin'] == 'active' || $value['status_admin'] == 'disapprove'){
               $datetime =  $this->onlytimezone($value['expected_date']);
            }elseif($value['status_admin'] == 'pendingrevision'){
               $datetime =  $this->onlytimezone($value['modified']);
            }elseif($value['status_admin'] == 'assign'){
               $datetime =  $this->onlytimezone($value['created']);
            }elseif($value['status_admin'] == 'checkforapprove'){
               $datetime =  $this->onlytimezone($value['modified']);
            }elseif($value['status_admin'] == 'approved'){
               $datetime =  $this->onlytimezone($value['approvaldate']);
            }elseif($value['status_admin'] == 'hold'){
               $datetime =  $this->onlytimezone($value['modified']);
            }else{
               $datetime =  $this->onlytimezone($value['modified']);
            }
            $data['reports_projects'][$key] = $this->myfunctions->adminVariables($value['status_admin'],$datetime,1);
            $data['reports_projects'][$key]['title'] = $value['title'];
            $data['reports_projects'][$key]['id'] = $value['id'];
            if($slug == 'revisions_count'){
               $data['reports_projects'][$key]['count_revision'] = $value['count_customer_revision']; 
            }elseif($slug == 'quality_revision_count'){
               $data['reports_projects'][$key]['count_revision'] = $value['count_quality_revision'];
            }
        }
        for ($i = 0; $i < count($data['reports_projects']); $i++) {
            $data['projects'] = $data['reports_projects'][$i];
             //$data['projects']['slug'] = $slug;
            $this->load->view('admin/content_management/project_listing_templ', $data);
        }
        if ($countallprojects > 0 || $countallprojects != '') { ?><span id="loadingAjaxCount" data-value="<?php echo $countallprojects; ?>"></span>
            <script> countTimer();</script>
            <?php
        }  
     }
     
     
    /*******************Category based questions*******************************/
     
    public function category_based_questions(){
        $result = array();
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Request Questions" => base_url().'admin/Contentmanagement/category_based_questions',
        );
        $result['all_quest'] = $this->Category_model->getallQuestions('','','admin');
        $countallquestions = $this->Category_model->getallQuestions('','','admin',true);
//        echo $countallquestions;exit;
//        echo "<pre/>";print_R($result['all_quest']);
        foreach($result['all_quest'] as $kq => $vq){
        $result['all_quest'][$kq]['cat_name'] = $this->Category_model->getcat_subcatName($vq['category_id'],0);
        $result['all_quest'][$kq]['subcat_name'] = $this->Category_model->getcat_subcatName($vq['category_id'],$vq['subcategory_id']);
        }
        //$countallquestions = sizeof($result['all_quest']);
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/category_based_questions', array('result' => $result['all_quest'],'countallquestions' => $countallquestions));
        $this->load->view('admin/admin_footer');   
    }
    
    public function load_more_questions(){
        $this->myfunctions->checkloginuser("admin");
        $group_no = $this->input->post('group_no');
        $search = ($this->input->post('search') != "") ? $this->input->post('search') : '';
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $result['all_quest'] = $this->Category_model->getallQuestions('','','admin',false,$start,$content_per_page,$search);
        $countallquestions = $this->Category_model->getallQuestions('','','admin',true,$start,$content_per_page,$search);
        for ($i = 0; $i < sizeof($result['all_quest']); $i++){
        $data['questions'] = $result['all_quest'][$i];
        $data['questions']['cat_name'] = $this->Category_model->getcat_subcatName($result['all_quest'][$i]['category_id'],0);
        $data['questions']['subcat_name'] = $this->Category_model->getcat_subcatName($result['all_quest'][$i]['category_id'],$result['all_quest'][$i]['subcategory_id']);
        $this->load->view('admin/content_management/questions_template', $data);
        }
        
     }
    
     public function get_subcategorybasedoncat($id){
         $sub_categories = $this->Category_model->get_categories($id,1);
         echo json_encode($sub_categories);
     }
     
     public function add_category_questions($edit_id = NULL){
         $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Request Questions  &nbsp; >" => base_url().'admin/Contentmanagement/category_based_questions',
        "Add/Edit Question" => base_url().'admin/Contentmanagement/add_category_questions',
        );
        $categories = $this->Category_model->get_categories(0,1);
        $editdata = $this->Category_model->getQuestdata($edit_id);
        $sub_categories = $this->Category_model->get_categories($editdata[0]['category_id'],1);
//         echo "<pre/>";print_R($_POST);exit;
        if(!empty($_POST) && $edit_id == ''){
           $allquestions = $data = array();
           foreach ($_POST['question_type'] as $k => $val){
               $allquestions[$k] = $_POST;
               foreach ($allquestions[$k] as $ky => $questval){
                    $allquestions[$k][$ky] = $questval[$k];
               }
           }
//           echo "<pre/>";print_R($allquestions);exit;
           foreach($allquestions as $kk => $vv){
//               echo "<pre/>";print_R($allquestions[0]);
            if(isset($vv['quest_default'])){
                $data['category_id'] = 0;
                $data['subcategory_id'] = 0;
            }else{
               $data['category_id'] = isset($allquestions[0]['multicat'])?$allquestions[0]['multicat']:'';
               $data['subcategory_id'] = isset($allquestions[0]['multisubcat'])?$allquestions[0]['multisubcat']:'';
            }
            $data['question_label'] = isset($vv['question_label'])?$vv['question_label']:'';
            $data['position'] = isset($vv['position'])?$vv['position']:'';
            $data['question_type'] = isset($vv['question_type'])? $vv['question_type']:'';
            $data['question_placeholder'] = isset($vv['question_placeholder'])? $vv['question_placeholder']:'';
            if($vv['question_type'] == 'text' || $vv['question_type'] == 'textarea'){
                $data['question_options'] = '0';
            }else{
                $optn = rtrim($vv['question_options'],',');
                $data['question_options'] = $optn;
            }
            $data['is_active'] = isset($vv['quest_active'])?'1':'0';
            $data['is_default'] = isset($vv['quest_default'])?'1':'0';
            $data['is_required'] = isset($vv['quest_reqrd'])?'1':'0';
            $data['modified'] = date("Y:m:d H:i:s");
            $data['created'] = date("Y:m:d H:i:s");
            $succ = $this->Admin_model->insert_data('request_questions', $data);
           }
           if($succ){
                $this->session->set_flashdata("message_success", "New Question added successfully.");
                redirect(base_url() . "admin/Contentmanagement/category_based_questions");
            }else{
                $this->session->set_flashdata("message_error", "New Question not added successfully.");
                redirect(base_url() . "admin/Contentmanagement/category_based_questions");
            }
            if($edit_id){
            if(isset($_POST['quest_default'])){
                $data['category_id'] = 0;
                $data['subcategory_id'] = 0;
            }else{
               $data['category_id'] = isset($_POST['category'])?$_POST['category']:'';
               $data['subcategory_id'] = isset($_POST['subcategories'])?$_POST['subcategories']:''; 
            }
            $data['question_label'] = isset($_POST['question_label'])?$_POST['question_label']:'';
            $data['question_placeholder'] = isset($_POST['question_placeholder'])?$_POST['question_placeholder']:'';
            $data['position'] = isset($_POST['position'])?$_POST['position']:'';
            $data['question_type'] = isset($_POST['question_type'])? $_POST['question_type']:'';
            $data['question_placeholder'] = isset($_POST['question_placeholder'])? $_POST['question_placeholder']:'';
            if($_POST['question_type'] == 'text' || $_POST['question_type'] == 'textarea'){
                $data['question_options'] = '0';
            }else{
                $optn = rtrim($_POST['question_options'],',');
                $data['question_options'] = $optn;
            }
            $data['is_active'] = isset($_POST['quest_active'])?'1':'0';
            $data['is_default'] = isset($_POST['quest_default'])?'1':'0';
            $data['is_required'] = isset($_POST['quest_reqrd'])?'1':'0';
            $data['modified'] = date("Y:m:d H:i:s");
            if(!empty($data)){
              $updated = $this->Welcome_model->update_data("request_questions", $data, array('id' => $edit_id));
            }
            if($updated){
                $this->session->set_flashdata("message_success", "Question info updated successfully.");
                redirect(base_url() . "admin/Contentmanagement/add_category_questions/".$edit_id);
            }else{
                $this->session->set_flashdata("message_error", "Question info not updated successfully.");
                redirect(base_url() . "admin/Contentmanagement/add_category_questions/".$edit_id);
            }
            }
        }else{
            if(!empty($_POST)){
            $edquestions = $data = array();
           foreach ($_POST['question_type'] as $k => $val){
               $edquestions[$k] = $_POST;
               foreach ($edquestions[$k] as $ky => $questval){
                    $edquestions[$k][$ky] = $questval[$k];
               }
           }
           foreach($edquestions as $edk => $edv){
            if(isset($edv['quest_default'])){
                $data['category_id'] = 0;
                $data['subcategory_id'] = 0;
            }else{
               $data['category_id'] = isset($edv['category'])?$edv['category']:'';
               $data['subcategory_id'] = isset($edv['subcategories'])?$edv['subcategories']:''; 
            }
            $data['question_label'] = isset($edv['question_label'])?$edv['question_label']:'';
            $data['question_placeholder'] = isset($edv['question_placeholder'])?$edv['question_placeholder']:'';
            $data['position'] = isset($edv['position'])?$edv['position']:'';
            $data['question_type'] = isset($edv['question_type'])? $edv['question_type']:'';
            $data['question_placeholder'] = isset($edv['question_placeholder'])? $edv['question_placeholder']:'';
            if($edv['question_type'] == 'text' || $edv['question_type'] == 'textarea'){
                $data['question_options'] = '0';
            }else{
                $optn = rtrim($edv['question_options'],',');
                $data['question_options'] = $optn;
            }
            $data['is_active'] = isset($edv['quest_active'])?'1':'0';
            $data['is_default'] = isset($edv['quest_default'])?'1':'0';
            $data['is_required'] = isset($edv['quest_reqrd'])?'1':'0';
            $data['modified'] = date("Y:m:d H:i:s");
            if(!empty($data)){
              $updated = $this->Welcome_model->update_data("request_questions", $data, array('id' => $edit_id));
            }
           }
            if($updated){
                $this->session->set_flashdata("message_success", "Question info updated successfully.");
                redirect(base_url() . "admin/Contentmanagement/add_category_questions/".$edit_id);
            }else{
                $this->session->set_flashdata("message_error", "Question info not updated successfully.");
                redirect(base_url() . "admin/Contentmanagement/add_category_questions/".$edit_id);
            }
        }
        }
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/add_category_questions', array('categories' => $categories,'sub_categories' => $sub_categories,'editdata' => $editdata,'edit_id'=> $edit_id));
        $this->load->view('admin/admin_footer'); 
     }
     
     public function deleteQuest($id){
         $quest_id = $id;
         $is_deleted = $this->Category_model->deleteQuestions('request_questions',$quest_id);
         if($is_deleted){
            $this->session->set_flashdata("message_success", "Question deleted successfully.");
            redirect(base_url() . "admin/Contentmanagement/category_based_questions");
         }
     }
     
    /**********sample based on cat*********************/
    function category_based_samples(){
        $result = array();
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Samples" => base_url().'admin/Contentmanagement/category_based_samples',
        );
        $categories = $this->Category_model->get_categories(0);
        $result['all_samples'] = $this->Category_model->getallSampledata('','','admin');
        $countallsamples = $this->Category_model->getallSampledata('','','admin',true);
        foreach($result['all_samples'] as $kq => $vq){
        $result['all_samples'][$kq]['cat_name'] = $this->Category_model->getcat_subcatName($vq['category_id'],0);
        $result['all_samples'][$kq]['subcat_name'] = $this->Category_model->getcat_subcatName($vq['category_id'],$vq['subcategory_id']);
        }
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/category_based_samples', array('result' => $result['all_samples'],'countallsamples' => $countallsamples));
        $this->load->view('admin/admin_footer');
    }
    
    function add_samples($edit_id = NULL){
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Samples &nbsp; >" => base_url().'admin/Contentmanagement/category_based_samples',
        "Add/Edit Sample" => base_url().'admin/Contentmanagement/add_samples',
        );
        $categories = $this->Category_model->get_categories(0,1);
        $editdata = $this->Category_model->getSampledata($edit_id);
        $sub_categories = $this->Category_model->get_categories($editdata[0]['category_id'],1);
        $getsamplematerial = $this->Category_model->getSamplematerialdata($edit_id);
       // echo "<pre/>";print_R($sub_categories);exit;
        if(!empty($_POST)){
            if(isset($_POST['quest_default'])){
                $data['category_id'] = 0;
                $data['subcategory_id'] = 0;
            }else{
               $data['category_id'] = isset($_POST['category'])?$_POST['category']:'';
               $data['subcategory_id'] = isset($_POST['subcategories'])?$_POST['subcategories']:''; 
            }
            $data['minimum_sample'] = isset($_POST['min_sample'])?$_POST['min_sample']:'';
            $data['name'] = isset($_POST['sample_name'])?$_POST['sample_name']:'';
            $data['maximum_sample'] = isset($_POST['max_sample'])?$_POST['max_sample']:'';
           // $data['position'] = isset($_POST['position'])?$_POST['position']:'';
            $data['is_active'] = isset($_POST['quest_active'])?'1':'0';
            $data['modified'] = date("Y:m:d H:i:s");
        if($edit_id){
            if(!empty($data)){
              $updated = $this->Welcome_model->update_data("sample_data", $data, array('id' => $edit_id));
              if($updated){
                $this->uploadSamples($edit_id);   
              }
            }
            if($updated){
                $this->session->set_flashdata("message_success", "Sample edited successfully..!");
                redirect(base_url() . "admin/Contentmanagement/add_samples/".$edit_id);
            }else{
                $this->session->set_flashdata("message_error", "Sample not edited successfully..!");
                redirect(base_url() . "admin/Contentmanagement/add_samples/".$edit_id);
            }
        }else{
            $data['created'] = date("Y:m:d H:i:s");
            $succ = $this->Admin_model->insert_data('sample_data', $data);
            if($succ){
                $this->uploadSamples($succ);
            }
            if($succ){
                $this->session->set_flashdata("message_success", "Sample added successfully..!");
                redirect(base_url() . "admin/Contentmanagement/category_based_samples");
            }else{
                $this->session->set_flashdata("message_error", "Sample not added successfully..!");
                redirect(base_url() . "admin/Contentmanagement/category_based_samples");
            }
        
        }
        }
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/add_samples', array('categories' => $categories,'sub_categories' => $sub_categories,'editdata' => $editdata,'getsamplematerial' => $getsamplematerial));
        $this->load->view('admin/admin_footer');
    }
     
    public function uploadSamples($sampleid){
        $data1 = array();
        if (!is_dir('public/uploads/samples/' . $sampleid)) {
            $data = mkdir('./public/uploads/samples/' . $sampleid, 0777, TRUE);
        }
        if($sampleid != 0){
            $uploaded = $this->customfunctions->movefilestos3($success,"public/uploads/samples/".$sampleid,true);
        }else{
            $uploaded = $this->customfunctions->movefilestos3($success,"public/uploads/samples/",true);
        }
        if(!empty($uploaded)){
            
            $editdata1 = $this->Category_model->getSampledata($sampleid);
//            echo "<pre/>";print_R($uploaded);exit;
            $savedfile = implode(',',array_slice($uploaded,2));
            $filesarr = explode(',',$savedfile);
          //  echo "<pre/>";print_R($filesarr);exit;
            foreach($filesarr as $fkk => $fvv){
                $data1['sample_material'] = $fvv;
                $data1['sample_id'] = $sampleid;
                $data1['created'] = date("Y:m:d H:i:s");
                $data1['modified'] = date("Y:m:d H:i:s");
               $succus = $this->Admin_model->insert_data('sample_material_data', $data1);
//               if($succus){
//                   die("inserted");
//               }else{
//                   die("updated");
//               }
            }
        }
     }
     
     function load_more_samples(){
        $this->myfunctions->checkloginuser("admin");
        $group_no = $this->input->post('group_no');
        $search = ($this->input->post('search') != "") ? $this->input->post('search') : '';
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $result['all_samples'] = $this->Category_model->getallSampledata('','','admin',false,$start,$content_per_page,$search);
        $countallquestions = $this->Category_model->getallSampledata('','','admin',true,$start,$content_per_page,$search);
        for ($i = 0; $i < sizeof($result['all_samples']); $i++){
        $data['samples'] = $result['all_samples'][$i];
        $data['samples']['cat_name'] = $this->Category_model->getcat_subcatName($result['all_quest'][$i]['category_id'],0);
        $data['samples']['subcat_name'] = $this->Category_model->getcat_subcatName($result['all_quest'][$i]['category_id'],$result['all_quest'][$i]['subcategory_id']);
        $this->load->view('admin/content_management/sample_template', $data);
     }     
     } 
     
      public function deleteSamplemateial(){
         $sample_id = $this->input->post("id");
         $is_deleted = $this->Admin_model->delete_data('sample_material_data', $sample_id);
         if($is_deleted){
             echo "success";
         }
     }
     
     public function change_sample_status(){
        $this->myfunctions->checkloginuser("admin");
        $cust_status = $_POST['status'];
        $cust_id = $_POST['data_id'];
        if ($_POST['status'] == 'true') {
            echo "1";
            $this->Request_model->update_customer_active_status("sample_data", array("is_active" => 1), array("id" => $cust_id));
        } elseif ($_POST['status'] == 'false') {
            echo "0";
            $this->Request_model->update_customer_active_status("sample_data", array("is_active" => 0), array("id" => $cust_id));
        }
    
     }
     
   public function feedback_matrix()
    {   
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
       "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
       "Feedback Matrix" => base_url().'admin/Contentmanagement/feedback_matrix'
       );
         $this->myfunctions->checkloginuser("admin");
         $login_user_id = $this->load->get_var('login_user_id'); 
         $order = $this->input->post("sort");  
         $type = $this->input->post("type");                   
         $dislIke  = $this->Admin_model->LikeORdislIke("< 3");        
         $Like  = $this->Admin_model->LikeORdislIke(">= 4");      
         $last5Avg["svg"] = $this->myfunctions->getfacesaccavg($last5Avg['lastAvg']);       
         $customer_rating  = $this->Admin_model->FeedbackColumns_customer();      
        foreach ($customer_rating as $ky => $value) {
            $customer_rating[$ky]["svg"] = $this->myfunctions->getfacesaccavg($value['total_avg']);
            $customer_rating[$ky]['lastAvg'] =  $this->Admin_model->select_custom("FORMAT(AVG(items.satisfied_with_design),2) as avg ,COUNT(items.user_id) as Avgtotal_design","(SELECT rfr.satisfied_with_design,rfr.user_id FROM request_files_review as rfr WHERE `user_id` = '".$value['customer_id']."' ORDER BY rfr.id DESC LIMIT 5 ) items");

            $customer_rating[$ky]["lastsvg"] = $this->myfunctions->getfacesaccavg($customer_rating[$ky]['lastAvg']['avg']);

             

            //echo "<pre>"; print_R($customer_rating[$ky]['total_design']);
            if ($value["is_active"] == 1) {
                $totaldraftscL+= $value['total_design'];
                $totaldraftslast5+= $value['lastAvg']['Avgtotal_design'];
                $totalOfAvgRatingcL+=$value['total_avg'];
                $totalavgRatingcl += $value['total_design']*$value['total_avg']; 

                $customer_rating5Cl += $customer_rating[$ky]['lastAvg']['avg']; 

                $toal5DesignCl +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design'];

                $AvgWeightedCl +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design'] * $customer_rating[$ky]['lastAvg']['avg'];

                 $totalCount["actv_totaldraftscL"] =  $totaldraftscL; 
                 $totalCount["actv_totalavgRatingcl"] =  $totalavgRatingcl; 
                 $totalCount["actv_toal5DesignCl"] =  $toal5DesignCl; 
                 $totalCount["totaldraftslast5"] =  $totaldraftslast5; 
                 $totalCount["actv_AvgWeightedCl"] =  $AvgWeightedCl; 
                 $getParent = array_search($value['parentid'], array_column($customer_rating, 'id'));
                 if($value['parentid'] == '0'){   
                  $NewratingArr[$ky] = $customer_rating[$ky];  
                  $NewratingArr[$ky]['sub'] = []; 
                }
                else{      
                    $NewratingArr[$getParent]['main']  =  $customer_rating[$getParent];  

                     
                    
                    
                    $NewratingArr[$getParent]['subuserTdesign'] +=  $customer_rating[$ky]['total_design'];  
                    $NewratingArr[$getParent]['subuserLstTdesign'] +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design'];  
                    $NewratingArr[$getParent]['subuserAvg'] +=  $customer_rating[$ky]['total_design']*$customer_rating[$ky]['total_avg'];
                    $NewratingArr[$getParent]['subuserlastAvg'] +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design']*$customer_rating[$ky]['lastAvg']['avg']; 
                    $NewratingArr[$getParent]['sub'][] =  $customer_rating[$ky];  


                }
            }else{
                $de_totaldraftscL+= $value['total_design'];
                $de_totaldraftslast5+= $value['lastAvg']['Avgtotal_design'];
                $totalOfAvgRatingcL+=$value['total_avg'];
                $de_totalavgRatingcl += $value['total_design']*$value['total_avg']; 

                $customer_rating5Cl += $customer_rating[$ky]['lastAvg']['avg']; 

                $de_toal5DesignCl +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design'];

                $de_AvgWeightedCl +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design'] * $customer_rating[$ky]['lastAvg']['avg'];

                 $totalCount["de_totaldraftscL"] =  $de_totaldraftscL; 
                 $totalCount["de_totalavgRatingcl"] =  $de_totalavgRatingcl; 
                 $totalCount["de_toal5DesignCl"] =  $de_toal5DesignCl; 
                 $totalCount["de_totaldraftslast5"] =  $de_totaldraftslast5; 
                 $totalCount["de_AvgWeightedCl"] =  $de_AvgWeightedCl; 
                 $getParent = array_search($value['parentid'], array_column($customer_rating, 'id'));
                 if($value['parentid'] == '0'){   
                  $NewratingArr[$ky] = $customer_rating[$ky];  
                  $NewratingArr[$ky]['sub'] = []; 
                }
                else{      
                    $NewratingArr[$getParent]['main'] =  $customer_rating[$getParent];  

                    $NewratingArr[$getParent]['subuserTdesign'] +=  $customer_rating[$ky]['total_design']; 
                    $NewratingArr[$getParent]['subuserLstTdesign'] +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design'];
                    $NewratingArr[$getParent]['subuserAvg'] +=  $customer_rating[$ky]['total_design']*$customer_rating[$ky]['total_avg']; 
                    $NewratingArr[$getParent]['subuserlastAvg'] +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design']*$customer_rating[$ky]['lastAvg']['avg']; 
                    $NewratingArr[$getParent]['sub'][] =  $customer_rating[$ky]; 

                }
                
            }

                
            $all_totaldraftscL+= $value['total_design'];
            $all_totalavgRatingcl += $value['total_design']*$value['total_avg']; 

            $customer_rating5Cl += $customer_rating[$ky]['lastAvg']['avg']; 

            $all_toal5DesignCl +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design'];

            $all_AvgWeightedCl +=  $customer_rating[$ky]['lastAvg']['Avgtotal_design'] * $customer_rating[$ky]['lastAvg']['avg'];

             $totalCount["totaldraftscL"] =  $all_totaldraftscL; 
             $totalCount["totalavgRatingcl"] =  $all_totalavgRatingcl; 
             $totalCount["toal5DesignCl"] =  $all_toal5DesignCl; 
             $totalCount["AvgWeightedCl"] =  $all_AvgWeightedCl;  
            
        }
        //  foreach for SubUser calculation 
        if($this->input->post("sort")==null){
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
           $this->load->view('admin/content_management/feedback_matrix',array('customer_info' => $NewratingArr ,'customer_rating5' =>$customer_rating5 , 'dislIke'=>$dislIke,'Like' =>$Like,'last5Avg' =>$last5Avg,'totalCountCust' => $totalCount));
           $this->load->view('admin/admin_footer'); 
          }else{
            if($type =='designer'){
                echo json_encode($Desinger_rating); exit;     
            }else if($type =='qa'){
                echo json_encode($Qa_rating); exit; 
            }else{
               echo json_encode($customer_rating); exit;  
            }
           
       }
                  
        
    }
    public function getfacesaccavg($avg){
        if ($avg >= 0 && $avg < 3) {
            $svg = "sad-red.svg";
        } else if ($avg >= 3 && $avg <= 4.24) {
            $svg = "happiness-yellow.svg";
        } else {
            $svg = "happy-green.svg";
        }
        return $svg;
    }

    public function overall_rating()
    {   //2019-12-03 18:07:03 echo date("Y-m-d", strtotime("last week monday"))
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
       "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
       "Overall Rating" => base_url().'admin/Contentmanagement/overall_rating'
       );
     
        // $startDate =  date("Y-m-d", strtotime("last week monday")); 
        // $endDate =  date("Y-m-d", strtotime("last sunday")); 
        $this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');      
        // $dataARR = $this->Admin_model->OverAllCustmoreFeedBack($startDate,$endDate);
        $dataARR = $this->Admin_model->OverAllCustmoreFeedBack();
       
        foreach ($dataARR as $key => $value) {
            $dataARR[$key]["svg"] = $this->getfacesaccavg($value['total_rating']);

             $total_reject  = $this->Admin_model->exc_custom_qry("COUNT(rf.id) as total_reject","requests as rq",$tbl2="request_files as rf","rf.status IN ('Reject') and  rq.category_id = '".$value['cat_id']."' && rq.subcategory_id='".$value['sub_catid']."'",$joincond="rf.request_id = rq.id",$join="left",$orderby="",$order="",$group_by="");
             $total_draft  = $this->Admin_model->exc_custom_qry("COUNT(rf.id) as total_draft","requests as rq",$tbl2="request_files as rf","rq.category_id = '".$value['cat_id']."' && rq.subcategory_id='".$value['sub_catid']."'",$joincond="rf.request_id = rq.id",$join="left",$orderby="",$order="",$group_by="");
               $dataARR[$key]["total_rejected"] = ($total_reject[0]['total_reject'] / $total_draft[0]['total_draft'])*100;

        }
        // echo "<pre>"; print_r($dataARR); die; 
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/overall-rating',array('info' => $dataARR));
        $this->load->view('admin/admin_footer');
 
    }
    public function overall_rating_ajax(){
        // $startDate  =  $this->input->post('start');
        // $endDate    =  $this->input->post('end');
        $limit      =  $this->input->post('limit');
        $start      =  $this->input->post('startfrom');

       $rangeSelector = $this->input->post("range"); 
       $range = explode("-",$rangeSelector);
       $starfrom = str_replace("/","-",$range[0]);
       $staronend = str_replace("/","-",$range[1]);
       $dataARR    = $this->Admin_model->OverAllCustmoreFeedBack($starfrom,$staronend,$limit,$start); 
        foreach ($dataARR as $key => $value) {
            $dataARR[$key]["svg"] = $this->getfacesaccavg($value['total_rating']);
            $total_reject  = $this->Admin_model->exc_custom_qry("COUNT(rf.id) as total_reject","requests as rq",$tbl2="request_files as rf","rf.status IN ('Reject') and  rq.category_id = '".$value['cat_id']."' && rq.subcategory_id='".$value['sub_catid']."'",$joincond="rf.request_id = rq.id",$join="left",$orderby="",$order="",$group_by="");

             $total_draft  = $this->Admin_model->exc_custom_qry("COUNT(rf.id) as total_draft","requests as rq",$tbl2="request_files as rf","rq.category_id = '".$value['cat_id']."' && rq.subcategory_id='".$value['sub_catid']."'",$joincond="rf.request_id = rq.id",$join="left",$orderby="",$order="",$group_by="");

               $dataARR[$key]["total_rejected"] = ($total_reject[0]['total_reject'] / $total_draft[0]['total_draft'])*100;
        }
        echo json_encode($dataARR); exit; 
    }
      public function rating_details()
       {   
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
       "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
       "Rating Details" => base_url().'admin/Contentmanagement/rating_details'
       );
        $this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');   
        $withText = $this->input->post("withText"); 
        $rating_details = $this->Admin_model->rating_details("50","0");
        $AvgCustomer = $this->Admin_model->rating_details_AvgCustomer();
        $AvgDesinger = $this->Admin_model->rating_details_AvgDesinger();
        // $Avgqa = $this->Admin_model->rating_details_qa();
        foreach ($rating_details as $ky => $rating_info) { 
          $rating_details[$ky]["svg"] = $this->getfacesaccavg($rating_info['satisfied_with_design']);
          if($rating_info['created'] != ""){
           $rating_details[$ky]['created'] = $this->onlytimezone($rating_info['created']);
          }else{
            $rating_details[$ky]['created'] = $this->onlytimezone($rating_info['user_feedback_date']); 
          }
         }
                 // echo "<pre>"; print_r($rating_details); echo "</pre>"; 
                 // die(); 
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/rating_details',array('rating_details' =>$rating_details,'cAvgrating'=>$AvgCustomer,'qa_rating' => $Avgqa, 'd_rating' => $AvgDesinger));
        $this->load->view('admin/admin_footer');
 
    }

     public function rating_details_ajax()
    {   
        $this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');   
        $withText = $this->input->post("withText"); 
        $start = $this->input->post("start"); 
        $limit = $this->input->post("limit"); 
        $rating_details = $this->Admin_model->rating_details($withText,$start,$limit); 
        $AvgCustomer = $this->Admin_model->rating_details_AvgCustomer();
        foreach ($rating_details as $ky => $rating_info) { 
          $rating_details[$ky]["svg"] = $this->getfacesaccavg($rating_info['satisfied_with_design']);
          if($rating_info['created'] != ""){
           $rating_details[$ky]['created'] = $this->onlytimezone($rating_info['created']);
          }else{
            $rating_details[$ky]['created'] = $this->onlytimezone($rating_info['user_feedback_date']); 
          }
         } 
        echo json_encode($rating_details); exit; 
 
    }
    public function ExportdataINExl(){
        $withtext = $this->input->post('withtext');
        
        $data  = $this->Admin_model->fetch_dataForExcelFile($withtext);
       // $data  = $this->Admin_model->fetch_dataForExcelFile($withtext);
//        echo "<pre>";print_r($data);exit;
        // foreach ($data as $ky => $r_info) { 
        //        $data[$ky]['DateDelivered'] = $this->onlytimezone($r_info['created']);
        //  }  
//        $filename =  "rating_details.xls"; 
//        header("Content-Type: application/vnd.ms-excel");
//        header("Content-Disposition: attachment; filename=\"$filename\"");
//        $this->ExportFile($data);      
        
        $filename =  "rating_details.xls"; 
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/vnd.ms-excel; ");
    // file creation 
        $file = fopen('php://output', 'w');

        $header = array("Customer Name", "Name of Drafts", "Name of Design", "Design Drafts", "Design Rating", "Rating Text", "Designer Name", "QA Name","Date Created","Date Delivered");
        fputcsv($file, $header);
        foreach ($data as $key => $line) {
            fputcsv($file, $line);
        }
        fclose($file);
        exit;
    }
        public function  ExportFile($records) {
            // echo "<pre>"; print_r( $records); die('asd'); 
            $heading = false;
            if(!empty($records))

              foreach($records as $k => $row) {
                if(!$heading) {
                    // display field/column names as a first row
                  echo implode("\t", array_keys($row)) . "\n";
                  $heading = true;
              }
              echo implode("\t", array_values($row)) . "\n";
          }
          exit;
        }


         public function ajaxForTopNav(){
            $mainArr =[];   
            $start = explode("-",$this->input->post('start'));
            $end =  explode("-",$this->input->post('end'));
            $starfrom = str_replace("/","-",$start[0]);
            $staronend = str_replace("/","-",$start[1]);
            $endfrom = str_replace("/","-",$end[0]);
            $endonend = str_replace("/","-",$end[1]);   
               
            $mainArr['customerRatingOne'] =  $this->Admin_model->ajaxForTopNav($starfrom,$staronend);
            $mainArr['customerRatingtwo'] =  $this->Admin_model->ajaxForTopNav($endfrom,$endonend);

            $mainArr['dislIkeOne']  = $this->Admin_model->LikeORdislIke("< 3",$starfrom,$staronend);        
            $mainArr['dislIkeTwo']  = $this->Admin_model->LikeORdislIke("< 3",$endfrom,$endonend);        
            $mainArr['likeOne'] = $this->Admin_model->LikeORdislIke(">= 4",$starfrom,$staronend);
            $mainArr['likeTwo'] = $this->Admin_model->LikeORdislIke(">= 4",$endfrom,$endonend);

            $mainArr['last5avgOne'] = $this->Admin_model->select_custom("IFNULL(FORMAT(AVG(items.satisfied_with_design),2),'0') as avg","(SELECT rfr.satisfied_with_design, rfr.user_id FROM request_files_review AS rfr WHERE `user_id` IN( SELECT rfr2.user_id FROM request_files_review rfr2 WHERE rfr2.user_id != 0 ) && rfr.user_feedback_date > '".$starfrom." 00:00:00' && rfr.user_feedback_date < '".$staronend." 00:00:00' ORDER BY rfr.id DESC LIMIT 5 ) items");

            $mainArr['last5avgTwo'] = $this->Admin_model->select_custom("IFNULL(FORMAT(AVG(items.satisfied_with_design),2),'0') as avg","( SELECT rfr.satisfied_with_design, rfr.user_id FROM request_files_review AS rfr WHERE `user_id` IN( SELECT rfr2.user_id FROM request_files_review rfr2 WHERE rfr2.user_id != 0 ) && rfr.user_feedback_date > '".$endfrom." 00:00:00' && rfr.user_feedback_date < '".$endonend." 00:00:00' ORDER BY rfr.id DESC LIMIT 5 ) items");



            foreach ($mainArr['dislIkeOne'] as $k => $value) {
                $DisLikeCustCount1 =$k; 
            }
            foreach ($mainArr['dislIkeTwo'] as $ky => $value) {
                $DisLikeCustCount2 =$ky; 
            }

            foreach ($mainArr['likeOne'] as $k => $value) {
                $LikeCustCount1 =$k; 
            }
            foreach ($mainArr['likeTwo'] as $ky => $value) {
                $LikeCustCount2 =$ky; 
            }
            unset($mainArr['likeOne']);
            unset($mainArr['likeTwo']);
            unset($mainArr['dislIkeOne']);
            unset($mainArr['dislIkeTwo']);
                         
            $mainArr['likeOne']['total_count'] =  !empty($LikeCustCount1) ? $LikeCustCount1  : 0;
            $mainArr['likeTwo']['total_count'] = !empty($LikeCustCount2) ? $LikeCustCount2  : 0;
            $mainArr['dislIkeOne']['total_count'] = !empty($DisLikeCustCount1) ? $DisLikeCustCount1  : 0;
            $mainArr['dislIkeTwo']['total_count'] = !empty($DisLikeCustCount2) ? $DisLikeCustCount2  : 0;
            echo json_encode($mainArr); exit; 
        }
        
/******affiliated users**********/
public function getAffiateUsers(){
    $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
   "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
   "Affiliate Users" => base_url().'admin/Contentmanagement/getAffiateUsers'
   );
   $this->myfunctions->checkloginuser("admin");
   $isfullaccess = $this->hasAdminfullaccess('full_admin_access', 'admin');
   $countaffusers= $this->Affiliated_model->affiliatedusers_load_more("",true);
   $user_info = $this->Affiliated_model->affiliatedusers_load_more();
   for ($i = 0; $i < sizeof($user_info); $i++) {
       $user_info[$i]['total_rating'] = $this->Request_model->get_average_rating($user_info[$i]['id']);
       $user_info[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($user_info[$i]['profile_picture']);
       $user_info[$i]['designer'][0]['profile_picture'] = $this->customfunctions->getprofileimageurl($user_info[$i]['designer'][0]['profile_picture']);
       $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $user_info[$i]['id']);
       $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $user_info[$i]['id']);
       $revision = $this->Account_model->get_all_active_view_request_by_customer(array('pendingrevision'), $user_info[$i]['id'],true);
       $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $user_info[$i]['id']);
       $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $user_info[$i]['id']);
       $hold = $this->Account_model->get_all_active_view_request_by_customer(array('hold'), $user_info[$i]['id']);
       $cancelled = $this->Account_model->get_all_active_view_request_by_customer(array('cancel'), $user_info[$i]['id']);
       $user = $this->Account_model->get_all_active_request_by_customer($user_info[$i]['id']);
       $plan_data = $this->Request_model->getsubscriptionlistbyplanid($user_info[$i]['plan_name']);
       $user_info[$i]['created'] = $this->onlytimezone($user_info[$i]['created'], 'M/d/Y');
       if ($user_info[$i]['billing_end_date'] != '') {
           $user_info[$i]['billing_end_date'] = $this->onlytimezone($user_info[$i]['billing_end_date'], 'M/d/Y');
       } else {
           $user_info[$i]['billing_end_date'] = 'N/A';
       }

       $user_info[$i]['active'] = $active;
       $user_info[$i]['inque_request'] = $inque;
       $user_info[$i]['revision_request'] = $revision;
       $user_info[$i]['review_request'] = $review;
       $user_info[$i]['complete_request'] = $complete;
       $user_info[$i]['hold'] = $hold;
       $user_info[$i]['addClass'] = "paid";
       $user_info[$i]['cancelled'] = $cancelled;
       $user_info[$i]['total_request'] = ($active + $inque + $revision + $review + $complete + $hold + $cancelled);
       $user_info[$i]['isfullaccess'] = $isfullaccess;
       $user_info[$i]['client_plan'] = (isset($plan_data[0]['plan_name']) && $plan_data[0]['plan_name'] != '') ? $plan_data[0]['plan_name'] : 'No Member';
       if ($user_info[$i]['designer_id'] != 0) {
           $user_info[$i]['designer'] = $this->Request_model->get_user_by_id($user_info[$i]['designer_id']);
       } else {
           $user_info[$i]['designer'] = "";
       }
       $user_info[$i]['va'] = $this->Request_model->get_user_by_id($user_info[$i]['va_id']);
   }
   $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
   $this->load->view('admin/content_management/affiliated_users', array('user_info' => $user_info,'count_aff' => $countaffusers));
   $this->load->view('admin/admin_footer');
}

public function changeaffstatus(){
   $this->myfunctions->checkloginuser("admin");
   $cust_status = $_POST['status'];
   $cust_id = $_POST['data_id'];
   if ($_POST['status'] == 'true') {
       echo "1";
       $this->Request_model->update_customer_active_status("users", array("is_active" => 1), array("id" => $cust_id));
   } elseif ($_POST['status'] == 'false') {
       echo "0";
       $this->Request_model->update_customer_active_status("users", array("is_active" => 0), array("id" => $cust_id));
   }
}

public function load_more_affclientlist(){
   $this->myfunctions->checkloginuser("admin");
   $group_no = $this->input->post('group_no');
   $search = $this->input->post('search');
   $status = $this->input->post('status');
   $content_per_page = LIMIT_ADMIN_LIST_COUNT;
   $start = ceil($group_no * $content_per_page);
   $planlist = $this->Stripe->getallsubscriptionlist();
   $countaffcustomer = $this->Affiliated_model->affiliatedusers_load_more("", true, $start, $content_per_page, '', $search, $cancelsubuser);
   $affClients = $this->Affiliated_model->affiliatedusers_load_more("", false, $start, $content_per_page, '', $search, $cancelsubuser);
//        echo "<pre/>";print_R($affClients);exit;
   for ($i = 0; $i < sizeof($affClients); $i++) {
       $affClients[$i]['total_rating'] = $this->Request_model->get_average_rating($affClients[$i]['id']);
       $affClients[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($affClients[$i]['profile_picture']);
       $affClients[$i]['designer'][0]['profile_picture'] = $this->customfunctions->getprofileimageurl($affClients[$i]['designer'][0]['profile_picture']);
       $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $affClients[$i]['id']);
       $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $affClients[$i]['id']);
       $revision = $this->Account_model->get_all_active_view_request_by_customer(array('disapprove'), $affClients[$i]['id']);
       $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $affClients[$i]['id']);
       $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $affClients[$i]['id']);
       $hold = $this->Account_model->get_all_active_view_request_by_customer(array('hold'), $affClients[$i]['id']);
       $cancelled = $this->Account_model->get_all_active_view_request_by_customer(array('cancel'), $affClients[$i]['id']);
       $user = $this->Account_model->get_all_active_request_by_customer($affClients[$i]['id']);
       $plan_data = $this->Request_model->getsubscriptionlistbyplanid($affClients[$i]['plan_name']);
       $affClients[$i]['created'] = $this->onlytimezone($affClients[$i]['created'], 'M/d/Y');
       if ($affClients[$i]['billing_end_date'] != '') {
           $affClients[$i]['billing_end_date'] = $this->onlytimezone($affClients[$i]['billing_end_date'], 'M/d/Y');
       } else {
           $affClients[$i]['billing_end_date'] = 'N/A';
       }
       $affClients[$i]['addClass'] = $status;
       $affClients[$i]['active'] = $active;
       $affClients[$i]['inque_request'] = $inque;
       $affClients[$i]['revision_request'] = $revision;
       $affClients[$i]['review_request'] = $review;
       $affClients[$i]['complete_request'] = $complete;
       $affClients[$i]['hold'] = $hold;
       $affClients[$i]['cancelled'] = $cancelled;
       $affClients[$i]['total_request'] = ($active + $inque + $revision + $review + $complete + $hold + $cancelled);
       $affClients[$i]['isfullaccess'] = $isfullaccess;
       $affClients[$i]['client_plan'] = (isset($plan_data[0]['plan_name']) && $plan_data[0]['plan_name'] != '') ? $plan_data[0]['plan_name'] : 'No Member';
       if ($affClients[$i]['designer_id'] != 0) {
           $affClients[$i]['designer'] = $this->Request_model->get_user_by_id($activeClients[$i]['designer_id']);
       } else {
           $affClients[$i]['designer'] = "";
       }
       $affClients[$i]['va'] = $this->Request_model->get_user_by_id($activeClients[$i]['va_id']);
   }
   // echo "<pre/>";print_R($affClients);exit;
   for ($i = 0; $i < sizeof($affClients); $i++) {
       $data['aff_clients'] = $affClients[$i];
       $this->load->view('admin/affiliate_users_template', $data);
   }if ($countcustomer != 0 || $countcustomer != '') {
       ?>
       <span id="loadingAjaxCount" data-value="<?php echo $countcustomer; ?>"></span>
       <?php
   }
   $data["all_designers"] = $this->Request_model->get_all_designers();
   for ($i = 0; $i < sizeof($data['all_designers']); $i++) {
       $user = $this->Account_model->get_all_active_request_by_designer($data['all_designers'][$i]['id']);
       $data['all_designers'][$i]['active_request'] = sizeof($user);
   }
   $subscription_plan_list = array();
   $j = 0;
   for ($i = 0; $i < sizeof($planlist); $i++) {
       if ($planlist[$i]['id'] == '30291' || $planlist[$i]['id'] == 'A21481D' || $planlist[$i]['id'] == 'A1900S' || $planlist[$i]['id'] == 'M99S' || $planlist[$i]['id'] == 'M199G' || $planlist[$i]['id'] == 'M1991D' || $planlist[$i]['id'] == 'M199S' || $planlist[$i]['id'] == 'A10683D' || $planlist[$i]['id'] == 'M993D') {
           continue;
       }
       $subscription_plan_list[$j]['id'] = $planlist[$i]['id'];
       $subscription_plan_list[$j]['name'] = $planlist[$i]['name'] . " - $" . $planlist[$i]['amount'] / 100 . "/" . $planlist[$i]['interval'];
       $subscription_plan_list[$j]['amount'] = $planlist[$i]['amount'] / 100;
       $subscription_plan_list[$j]['interval'] = $planlist[$i]['interval'];
       $subscription_plan_list[$j]['trial_period_days'] = $planlist[$i]['trial_period_days'];
       $j++;
   }
}

function hasAdminfullaccess($key, $role) {
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->Admin_model->getuser_data($login_user_id);
        // echo "<pre/>";print_R($login_user_data);
        if (($login_user_data[0][$key] != 0 || $login_user_data[0][$key] == NULL ) && $login_user_data[0]['role'] == $role) {
            return true;
        }
    }

      public function qa_question_matrix()
    {   
        $rangeSelector = $this->input->post("range"); 
        $range = explode("-",$rangeSelector);
        $starfrom = str_replace("/","-",$range[0]);
        $staronend = str_replace("/","-",$range[1]);

        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
       "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
       "Feedback matrix &nbsp; >" => base_url().'admin/Contentmanagement/feedback_matrix',
       "Qa Question matrix" => base_url().'admin/Contentmanagement/Qa_question_matrix'
       );
         $this->myfunctions->checkloginuser("admin");
         $login_user_id = $this->load->get_var('login_user_id'); 
             if($rangeSelector){
               $date_selecotr ="rfr.qa_feedback_date IS NOT NULL AND rfr.qa_feedback_date > '".$starfrom." 00:00:00' AND rfr.qa_feedback_date < '".$staronend." 00:00:00'";
               }else{
                $date_selecotr ="rfr.qa_feedback_date IS NOT NULL";
            }

          $questionData =  $this->Admin_model->exc_custom_qry("rfr.*,rf.id as rid,rf.request_id as rqid,rf.file_name as name",$tbl1="request_files_review as rfr",$tbl2="request_files as rf","rf.status IN ('Reject') and  rfr.qa_id != '0' and ".$date_selecotr."",$joincond="rf.id = rfr.draft_id",$join="left",$orderby="",$order="",$group_by="",$limt="",$start="");

          if($rangeSelector){
            echo json_encode($questionData); die; 
          }
        
         $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
         $this->load->view('admin/content_management/question_matrix',array("questionData" =>$questionData));
         $this->load->view('admin/admin_footer'); 
            
                  
        
    }

    public function all_other_matrix(){

     $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
       "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
       "Feedback matrix &nbsp; >" => base_url().'admin/Contentmanagement/feedback_matrix',
       "Designer and Qa Stats &nbsp " => base_url().'admin/designer_qa_stats'
   );
         $this->myfunctions->checkloginuser("admin");
         $login_user_id = $this->load->get_var('login_user_id'); 
         $order = $this->input->post("sort");  
         $type = $this->input->post("type");  

         $Desinger_rating  = $this->Admin_model->FeedbackColumns_desinger($order);   
         $Qa_rating        = $this->Admin_model->FeedbackColumns_qa();     
         $ratingByplanName  = $this->Admin_model->ratingByplanName(); 

        foreach ($Qa_rating as $key => $va) {

 
            $Qa_rating[$key]["total_drafts"] = $this->Admin_model->total_draftsby_designer("qa_id",$va["qa_id"]);
            $Qa_rating[$key]["svg"] = $this->myfunctions->getfacesaccavg($va['total_avg']);
            $Qa_rating[$key]['lastAvg'] =  $this->Admin_model->select_custom("FORMAT(AVG(items.satisfied_with_design),2) as avg ,COUNT(items.qa_id) as Avgtotal_design","(SELECT rfr.satisfied_with_design,rfr.qa_id FROM request_files_review as rfr WHERE `qa_id` = '".$va['qa_id']."' AND user_id!=0 ORDER BY rfr.id DESC LIMIT 50 ) items");

            $Qa_rating[$key]["lastsvg"] = $this->myfunctions->getfacesaccavg($Qa_rating[$key]['lastAvg']['avg']);

           $qascor = $this->Admin_model->exc_custom_qry("COUNT(rfr.draft_id) as total_reject","request_files_review as rfr",$tbl2="request_files as rf","rf.status IN ('Reject') and  rfr.qa_id = '".$va['qa_id']."'",$joincond="rfr.draft_id = rf.id",$join="left",$orderby="",$order="",$group_by="");
            $NotapprovedbyQA = ($qascor[0]["total_reject"]/$Qa_rating[$key]["total_drafts"])*100; 
            $Qa_rating[$key]['qa_notApprov'] =  $NotapprovedbyQA;


            $total_draftsSentfromQa =  $this->Admin_model->exc_custom_qry("COUNT(rf.id) as sent_drafts","request_files as rf",$tbl2="request_files_review as rfr","rf.status IN ('Approve','customerreview') and  rfr.qa_id = '".$va['qa_id']."' ",$joincond="rfr.draft_id = rf.id",$join="left",$orderby="",$order="",$group_by="");


            $total_draftsApproved =  $this->Admin_model->exc_custom_qry("COUNT(rf.id) as approved_byclient","request_files as rf",$tbl2="request_files_review as rfr","rf.status IN ('Approve') and  rfr.qa_id = '".$va['qa_id']."'  ",$joincond="rfr.draft_id = rf.id",$join="left",$orderby="",$order="",$group_by="");
           // echo "<pre>"; print_r($totalNumberOfDrafts); die; 
             $Qa_rating[$key]['percentageOfApprovedByClint'] = ($total_draftsApproved[0]['approved_byclient']/$total_draftsSentfromQa[0]['sent_drafts'])*100; 
 
        }  

        foreach ($Desinger_rating as $ky => $val) {
            $Desinger_rating[$ky]["total_drafts"] = $this->Admin_model->total_draftsby_designer("designer_id",$val["d_id"]);
            $Desinger_rating[$ky]["svg"] = $this->myfunctions->getfacesaccavg($val['total_avg']);
            $Desinger_rating[$ky]['lastAvg'] =  $this->Admin_model->select_custom("FORMAT(AVG(items.satisfied_with_design),2) as avg,items.designer_id as de_id,COUNT(items.designer_id) as Avgtotal_design","(SELECT rfr.satisfied_with_design, rfr.designer_id  FROM request_files_review as rfr WHERE `designer_id` = '".$val['d_id']."' AND user_id!=0 ORDER BY rfr.id DESC LIMIT 15 ) items"); 
            $Desinger_rating[$ky]["lastsvg"] = $this->myfunctions->getfacesaccavg($Desinger_rating[$ky]['lastAvg']['avg']);

           $designerscor = $this->Admin_model->exc_custom_qry("(SELECT COUNT(rf1.id) from request_files as rf1 Where rf1.user_id = '".$val['d_id']."') as total_projects,count(rf.id) as rejected_project","request_files as rf",$tbl2="",$where="rf.user_id = '".$val['d_id']."' and rf.status ='Reject'",$joincond="",$join="",$orderby="",$order="",$group_by="");

            $TotalRejection = ($designerscor[0]["rejected_project"]/$designerscor[0]["total_projects"])*100; 
            $Desinger_rating[$ky]['qa_rejected'] =  $TotalRejection; 
            $total_draftsSentfromQa =  $this->Admin_model->exc_custom_qry("COUNT(rf.id) as sent_drafts","request_files as rf",$tbl2="request_files_review as rfr","rf.status NOT IN ('Reject','pending') and  rfr.designer_id = '".$val['d_id']."'",$joincond="rfr.draft_id = rf.id",$join="left",$orderby="",$order="",$group_by="");

            $total_draftsApproved =  $this->Admin_model->exc_custom_qry("COUNT(rf.id) as approved_byclient","request_files as rf",$tbl2="request_files_review as rfr","rf.status IN ('Approve') and  rfr.designer_id = '".$val['d_id']."'  ",$joincond="rfr.draft_id = rf.id",$join="left",$orderby="",$order="",$group_by="");

             $total_draftsRejectedByClient =  $this->Admin_model->exc_custom_qry("COUNT(rf.id) as RejectedByClient","request_files as rf",$tbl2="request_files_review as rfr","rf.status IN ('revision')  and rfr.designer_id = '".$val['d_id']."'  ",$joincond="rfr.draft_id = rf.id",$join="inner",$orderby="",$order="",$group_by="");

           if($total_draftsApproved[0]['approved_byclient'] != 0){
             $Desinger_rating[$ky]['percentageOfApprovedByClint'] = ($total_draftsApproved[0]['approved_byclient']/$total_draftsSentfromQa[0]['sent_drafts'])*100; 
             }else{
                $Desinger_rating[$ky]['percentageOfApprovedByClint'] = 0; 
            }
            if($total_draftsRejectedByClient[0]['RejectedByClient'] != 0){
               $Desinger_rating[$ky]['RejectedByClient'] = ($total_draftsRejectedByClient[0]['RejectedByClient']/$total_draftsSentfromQa[0]['sent_drafts'])*100; 
               }else{
                $Desinger_rating[$ky]['RejectedByClient'] = 0; 
              }

        
        }
           // echo "<pre>"; print_r($Desinger_rating); die; 

        foreach ($ratingByplanName as $key => $val) {
            $drafts = $this->Admin_model->ratingByplanName(1);
            $ratingByplanName[$key]["total_drafts"] = $drafts[$key]["total_designs"];
        } 

        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/content_management/all_matrix',array('designer_info' =>$Desinger_rating ,'qa_info' =>$Qa_rating, 'Desinger_rating15' =>$Desinger_rating15,'Qa_rating15' =>$Qa_rating15,'dislIke'=>$dislIke,'Like' =>$Like,'last5Avg' =>$last5Avg,'ratingByplanName' =>$ratingByplanName));
        $this->load->view('admin/admin_footer');      
               
   }
}
