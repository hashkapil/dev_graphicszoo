function sliderInit() {
    jQuery('.wiz_sample').slick({
        slidesToShow: 4,
        rows: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
    });
}

function destroyCarousel() {
    if ($('.wiz_sample').hasClass('slick-initialized')) {
        $('.wiz_sample').slick('destroy');
    }
}

jQuery(document).ready(function () {
    sliderInit();
    var value = $('select#cat_req option:selected').val();
    if(value){
        $('.subcat_div').css('display','block');
        $('#child_'+value).css('display','block');
    }
//    $('#brand_sell').niceSelect();
//    $('#cat_req').niceSelect();
});

$(document).on('change','.text_included',function(){
    var textincl = $("input[name='text_included']:checked").val();
  //  console.log("textincl",textincl);
    if(textincl == 'on'){
        $('.answer_toggle').html('YES');
        $('.include_text').css('display','block');
    }else{
        $('.answer_toggle').html('NO');
        $('.include_text').css('display','none');
        $('.include_text').val('');
    }
});

$(document).on('change','.subcategory_class',function(){
   $('#is_quest_loaded_onback').val('0'); 
});

$(document).on('click', '.next_req', function () {
    var id = $("fieldset.active").data('chck');
    var isvalidate = validatewhileAdd(id);
    var catid = $('select#cat_req option:selected').val();
    var subcatid = $("input[name='subcategory_id']:checked").val();
    //console.log('id',id);
//    console.log('catid',catid);
//    console.log('subcatid',subcatid);
    //if (isvalidate == true) {
     if ($("#addRequestform").valid() && isvalidate == true) {
        if (id == '1') {
            getSamples(catid, subcatid);
        }
        if (id == '2') {
            $('.1_leftbar').addClass('success_step');
            $('.1_leftbar').removeClass('current_step');
            $('.2_leftbar').addClass('current_step');
           // getSamples(catid, subcatid);
        }
        if (id == '3') {
            $('.2_leftbar').addClass('success_step');
            $('.2_leftbar').removeClass('current_step');
            $('.3_leftbar').addClass('current_step');
            getCatQuest(catid,subcatid);
        }
        if (id == '3') {
            $('.3_leftbar').removeClass('success_step');
            $('.3_leftbar').addClass('current_step');
        }
        if (id == '4') {
            $('.3_leftbar').addClass('success_step');
            $('.3_leftbar').removeClass('current_step');
            $('.4_leftbar').addClass('current_step');
        }
        if (id == '5') {
            $('.4_leftbar').addClass('success_step');
            $('.4_leftbar').removeClass('current_step');
            $('.5_leftbar').addClass('current_step');
            getSummary();
        }
        Addrequestwizard(id, 'next');
    }
});

$(document).on('click', '.back_req', function () {
    var id = $("fieldset.active").data('chck');
    Addrequestwizard(id, 'back');
    destroyCarousel();
    sliderInit();
});

function Addrequestwizard(n, type) {
    var is_sample_exist = $('#is_sample_exist').val();
    console.log("Addrequestwizard",n);
    //console.log("is_sample_exist",is_sample_exist);
    if (type == 'next') {
        if(n == 1 || (is_sample_exist == 1 && n == 2)){
            autoSave(n);
        }
        var field_id = parseInt(n) + 1;
        if (n == '5') {
            $('.next_req').css('display', 'none');
            $('.save_submit').css('display', 'inline-block');
            $('.draft_req').css('display', 'inline-block');
        }
        if (n) {
            $('.back_req').css('display', 'inline-block');
        }
        $("#1_field").css('display','none');
    } else {
        var field_id = parseInt(n) - 1;
        if(is_sample_exist == '1'){
        if (n == '2') {
            $('.back_req').css('display', 'none');
            $('.draft_req').css('display', 'none');
            $('.next_req').css('display', 'inline-block');
            $('.save_submit').css('display', 'none');
        }}else{
            if (n == '3') {
            $('#2_field').css('display', 'none');  
            $('#1_field').css('display', 'block');  
            $('.back_req').css('display', 'none');
            $('.save_submit').css('display', 'none');
            $('.next_req').css('display', 'inline-block');
            $('.draft_req').css('display', 'none');
            }
        }
        if(is_sample_exist == '0'){
            if(n == '2'){
                $("#3_field").css('display', 'none');
                $('.back_req').css('display', 'none');
            }
            if(n == '3'){
                $("#2_field").addClass("hide");
            }else{
              $("#2_field").removeClass("hide"); 
            }
        }
        if (n) {
            $('.draft_req').css('display', 'none');
            $('.next_req').css('display', 'inline-block');
            $('.save_submit').css('display', 'none');
        }
    }
    $('#' + field_id + '_field').css('display', 'block'); 
    $('#' + field_id + '_field').addClass('active');
    $('#' + n + '_field').css('display', 'none');
    $('#' + n + '_field').removeClass('active');
    
}

function getsubcat(sel)
{
    var catID = sel.value;
    $('.subcat_div').css('display','block');
    $('option:selected', '#cat_req').attr('selected', true).siblings().removeAttr('selected');
    $('#child_' + sel.value).siblings().css('display', 'none');
    $('#child_' + sel.value).css('display', 'block');
    $('#is_quest_loaded').val(1);
}


$(document).on('change', '.color_codrr', function () {
    var color_type = $("input[name='color_pref[]']:checked").val();
    if (color_type == "Color Code") {
        $('.choose_color_pref_type').css('display', 'block');
        $('.enter_hax_code').css('display', 'block');
    } else {
        $('.choose_color_pref_type').css('display', 'none');
        $('.enter_hax_code').css('display', 'none');
    }
    autoSave();
});

$(document).on('change','.deliverables_up',function(){
   autoSave(); 
});

var max_fields_limit = 10; //set limit for maximum input fields
var x = 1; //initialize counter for text box
$('.add_more_colors').click(function (e) { //click event on add more fields button having class add_more_button
    e.preventDefault();
    var name = $(this).parent().find(".f_color_nm").attr('name');
//    console.log(name);
    if (x < max_fields_limit) { //check conditions
        x++; //counter increment
        $('.input_color_container').append('<div><div class="remove-pre"><input placeholder="Enter Color Code" type="text" name="' + name + '" class="form-control f_color_nm"><a href="#" class="remove_field"><i class="fas fa-plus"></i></a></div></div></div>'); //add input field
    }
});

$('.input_color_container').on("click", ".remove_field", function (e) { //user click on remove text links
    e.preventDefault();
    jQuery(this).fadeOut('slow', function () {
        jQuery(this).parent('div').remove();
    });
    // jQuery(this).parent('div').remove();
    x--;
});

function getSamples(catID, subcat) {
    var req, valreq, samplesfetch,edit_id,duplicate_id;
    var searchParams = new URLSearchParams(window.location.search);
   // console.log("searchParams",searchParams);
    if(typeof(searchParams) != 'undefined'){
     edit_id = searchParams.get('reqid');
     duplicate_id = searchParams.get('did');
   }
    $.ajax({
        method: 'POST',
        url: baseUrl + "customer/Request/getSamples",
        data: {'catID': catID, 'subcat': subcat, 'edit_id': edit_id,'duplicate_id':duplicate_id},
        dataType: "json",
        success: function (response) {
            if (response.status == 'success') {
               $('.count_step_wiz_apper').html('3');
               $('.count_step_wiz_qust').html('4');
               $('.count_step_wiz_pht').html('5');
               $('.count_step_wiz_smry').html('6');
               $('#is_sample_exist').val(1);
               var checked = '';
               $.each(response.data, function (nameArr_k, nameArr_v) {
                   if($.inArray(nameArr_v.material_id, response.selected_mat) >= 0){
                       checked = 'checked';
                   }else{
                     checked = '';  
                   }
                    var maxval = nameArr_v.maximum_sample;
                    var minval = nameArr_v.minimum_sample;
                    samplesfetch += '<div class="col-md-3"><div class="wiz_sample_item"><input data-minchk=' + minval + ' data-maxchk=' + maxval + ' value="' + nameArr_v.material_id + '" type="checkbox" class="sample_subcat" name="sample_subcat[]" '+checked+'><span class="wiz_sample_check"></span><div class="wiz_sample_img"><img src="' + $uploadPath + 'samples/' + nameArr_v.sample_id + '/' + nameArr_v.sample_material + '" class="img-responsive"></div></div></div>';
                    $('.min_des').text(nameArr_v.minimum_sample + ' design(s)');
                    $('.max_des').text(nameArr_v.maximum_sample + ' design(s)');
                    $('#2_field').attr('data-min_val', nameArr_v.minimum_sample);
                });
                var samplesfetch = samplesfetch.replace("undefined", '');
                $('.samples_categories').html(samplesfetch);
                destroyCarousel();
                sliderInit();
            } else {
                $('#is_sample_exist').val(0);
                $('#2_field').css('display', 'none');
                $('#3_field').css('display', 'block');
                $('.1_leftbar').addClass('success_step');
                $('.1_leftbar').removeClass('current_step');
                $('.2_leftbar').addClass('current_step');
            }
        }
    });
}

function getCatQuest(catID, subcat) {
    var req, valreq;
    var htmlData = '';
//    console.log("rejectedids",rejectedids);
   var edit_id,did;
//    console.log("rejectedids",rejectedids);
     var searchParams = new URLSearchParams(window.location.search);
     var edit_id = searchParams.get('reqid');
     var did = searchParams.get('did');
     console.log("edit",edit_id);
     console.log("did",did);
    var is_quest_loaded_once = $('#is_quest_loaded_once').val();
    var is_quest_loaded_onback = $('#is_quest_loaded_onback').val();
    if((is_quest_loaded_once == '0' && is_quest_loaded_onback == '0') || (is_quest_loaded_once == '1' && is_quest_loaded_onback == '0')){
    $.ajax({
        method: 'POST',
        url: baseUrl + "customer/Request/getCategoryquestions",
        data: {'catID': catID, 'subcat': subcat, 'edit_id': edit_id,'did':did},
        dataType: "json",
        success: function (response) {
             var brkgpnt = ((response.data).length);
            if (response.status == 'success') {
                //console.log(response.data);
                for (i = 0; i < brkgpnt; i++) {
                     htmlData += questionData(response.data[i]);  
                }
                $('.question_based_on_catsubcat').html(htmlData);
                $('#is_quest_loaded_once').val('1');
                $('#is_quest_loaded_onback').val('1');
            }else{}
        }
    });
}}


function questionData(value){
    var answer = value.answer;
//    console.log(answer);
    var qid = value.id;
    var answer = value.answer;
    var html_data = val_ans  = reqclass = '';
    if(answer === 'null' || typeof answer === 'undefined' || answer === null){
        val_ans = '';
        label_class = '';
        reqclass = '';
    }else{
       val_ans =  value.answer;
       label_class = 'label-active';
       reqclass = '';
    }
    if(value.is_required == 1){
         req = '*';
         valreq = 'required';
         reqclass = 'reqclass';
    }else{
        req = '';
        valreq = '';
        reqclass = '';
    }
    if(value.is_default != '1'){
      if(value.question_type == 'text'){
        html_data += '<div class="form-group add_question_text"><label>'+value.question_label+''+req+'</label><input class="form-control getinsumry '+reqclass+' questionsup" type="text" name="quest['+value.id+']" placeholder="'+value.question_placeholder+'" value="'+val_ans+'"><span class="sub_exp">'+value.question_info+'</span></div>';
      }else if(value.question_type == 'textarea'){
        html_data +=  '<div class="form-group add_question_text"><div class="inpt_wrappr"><label>'+value.question_label+''+req+'</label><textarea name="quest['+value.id+']" class="form-control getinsumry '+reqclass+' questionsup" placeholder="'+value.question_placeholder+'">'+val_ans+'</textarea></div></div>';
      }else if(value.question_type == 'radio'){
        var res = value.question_options.split(",");
        var slrad = value.answer;
        html_data +=  '<div class="form-group add_question_text"><label>'+value.question_label+''+req+'</label><span class="sub_exp">'+value.question_placeholder+'</span><div class="wiz_prefrence">';
        $.each(res, function (kk, val) {
            if(val == slrad || (kk == 0 && val.is_required == 1)){
                var resrad = "checked";
            }else{
                var resrad = ""; 
            }
          html_data += '<label for="soundsignal'+kk+'"><input type="radio" name="quest['+value.id+']" class="preference color_codrr getinsumry '+reqclass+' questionsup" id="soundsignal'+kk+' value="'+val+'" '+resrad+'><span>'+val+'</span></label>';
         });
         html_data += '</div></div>'
      }else if(value.question_type == 'checkbox'){
          var res = value.question_options.split(",");
        html_data +=  '<div class="form-group add_question_text"><label>'+value.question_label+''+req+'</label><span class="sub_exp">'+value.question_placeholder+'</span><div class="wiz_prefrence">';
        $.each(res, function (k, vv) {
            if(k == 0 && vv.is_required == 1){
                var chckrad = "checked";
            }else{
                 var chckrad = "";
            }
          html_data += '<label for="soundsignal'+k+'"><input type="checkbox" name="quest['+value.id+']" class="preference color_codrr getinsumry '+reqclass+' questionsup" id="soundsignal'+k+' value="'+vv+'" '+chckrad+'><span>'+vv+'</span></label>';
         });
         html_data += '</div></div>'
      }else{
        html_data +=  '<label for="soundsignal'+value.id+'"><input type="radio" name="quest['+value.id+']" class="preference color_codrr getinsumry '+reqclass+'" id="soundsignal'+value.id+'>" value="'+value.question_label+'"><span>'+value.question_label+'</span></label>';
      } 
    }
    return html_data;
}

function validatewhileAdd(id) {
//    console.log(id);
    var output,msg = '';
    var title = $('.pro_name').val();
    var dimnsn = $('.des_dimnsn').val();
    var selctedcat = $('#cat_req').children("option:selected").val();
    var selctedsubcat = $('input:radio[name="subcategory_id"]').is(':checked');
    var is_sample_exist = $('#is_sample_exist').val();
    var regex = /^[A-Za-z]+$/;
//    console.log("selctedsubcat",selctedsubcat);
    var subcat = $("input[name='subcategory_id']:checked").val();
    /*******basic info****/
    if (id == '1') {
//        if (title == '') {
//            toastr_msg('error', 'please enter project title');
//            output = false;
//        }else if(title.length <= 4 || title.length >= 200){
////            console.log(title.length);
//           toastr_msg('error', 'title length should be in between 4 to 200');
//           output = false; 
//        }else if(!regex.test(title)){
//          toastr_msg('error', 'title contains only alphabets');
//           output = false;   
//        }else if(selctedcat == ''){
//           toastr_msg('error', 'please choose category for your design.');
//           output = false; 
//        }
//        if(selctedsubcat == false){
//           toastr_msg('error', 'please choose subcategory for your design.');
//           output = false; 
//        }else {
            output = true;
        //}
        /****if samples exist*******/
    } 
    else if (id == '2') {
        var sample = [];
        $("input[name='sample_subcat[]']:checked").each(function () {
            sample.push($(this).val());
        });
        var minval = $('#2_field').attr('data-min_val');
        if(minval && is_sample_exist == '1'){
        if (sample.length < minval) {
            toastr_msg('error', "Please choose atleast " + minval + " sample.");
            output = false;
        }else{
           output = true; 
        }
        }
//        else if(minval == '' && dimnsn == ''){
//            toastr_msg('error', "Please enter design dimension.");
//            output = false;
//           
//        }
        else {
            output = true;
        }
    } 
    else if (id == '3') {
//    if(dimnsn == ''){
//     toastr_msg('error', "Please enter design dimension.");
//     output = false;   
//    }else{
      output = true; 
    //}
    }
    else if(id == '4'){
//        var arrans = $(".add_question_text input").map(function(){return $(this).val();}).get();
//        if($(".form-control").hasClass("reqclass")){
//            $(".reqclass").each(function() {
////                console.log($('.reqclass').length);
////                console.log($(this).val());
//                if($(this).val() == ''){
//                    output = false;  
//                    msg = "error";
//                }else{
//                   output = true;  
//                   msg = '';
//                }
//            });
//            if(msg == 'error'){
//                toastr_msg('error', "Please enter required field(s).");
//            }
       // }else{
         output = true;   
        //}
    }else {
        output = true;
    }
    return output;
}

var storedFiles = [];
storedFiles['logo_upload'] = [];
storedFiles['materials_upload'] = [];
storedFiles['additional_upload'] = [];
var deletedFiles = [];
$(document).on("change", "#add_files_req", function () {
    var $fileListContainer = $(".uploadFileListContain");
    var loadingImg = $assets_path + 'img/ajax-loader.gif';
    $fileListContainer.append('<div class="ajax-img-loader"><img src="' + loadingImg + '" height="150"/></div>');
    $('#request_submit').prop('disabled', true);
    var fileInput = $('#add_files_req')[0];
    if (fileInput.files.length > 0) {
        var formData = new FormData();
        $.each(fileInput.files, function (k, file) {
            var fileName = file.name;
            var type = fileName.split('.').pop();
            var name = fileName.split('.');
            var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
            var newFileName = name_file + '.' + type;
            formData.append('file-upload[]', file, newFileName);
        });
        var $url = baseUrl + "customer/request/process";
//        console.log(formData);
        $.ajax({
            method: 'post',
            url: $url,
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                //  console.log(response);
                if (response.status == '1') {
                    if (response.files.length > 0) {
                        $.each(response.files, function (k, v) {
                            storedFiles.push(v[0]);
                            $fileListContainer.html(createStoredFilesHtml());
                            $('#request_submit').prop('disabled', false);
                        });
                    }
                }
            }
        });
    }
});

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();


$(document).on("click","#image_gallery li",function(){
       var downloaded_url,targetid,get_method,currnt_state,pageUrl,markpoint;
       var status_str;
       var search_keyword = $("#search_keywrd").val();
       $("#search_keyword_hid").val(search_keyword);
       $this = $(this);
       $checkmark = $(".checkmark_pop");
       $this.toggleClass("selected-li");
       $this.find("i").toggleClass("selected");
       $this.find("i").toggleClass("fa fa-check");
//       $this.parents("li").find(".checkmark_pop").toggleClass("selected");
//       $this.parents("li").find(".checkmark_pop").toggleClass("fa fa-check");
//       $this.parents("li").find("a").toggleClass("selected_img");
         
//      $this.find(".checkmark_pop").toggleClass("selected");
//      $this.find(".checkmark_pop").toggleClass("fa fa-check");
      $this.find("a").toggleClass("selected_img");
       downloaded_url = $this.find("a").attr("data-href");
       targetid = $this.find("a").attr("data-id");
       pageUrl = $this.find("a").attr("data-pageurl");
       if($this.find("i").hasClass("selected")){
        var text = downloaded_url;
        var text2 = pageUrl;
        downloaded_url = text.replace("https://", "");
        pageUrl = text2.replace("https://", "");
        $(".selected_images").append('<input type="hidden" id="'+targetid+'pgurl" name="pageurl[]" value="'+pageUrl+'">');
        $(".selected_images").append('<input type="hidden" multiple="" name="file-upload[]" class="multiImage" id="'+targetid+'" value="'+downloaded_url+'">');
        $(".selected_images").append('<input type="hidden" id="'+targetid+'ids" name="ids[]" value="'+targetid+'">');
        status_str = $(".selected_images").html();
       }else{
          $("#"+targetid).remove(); 
          $("#"+targetid+"ids").remove(); 
          $("#"+targetid+"pgurl").remove(); 
           status_str =  $(".selected_images").html();
       }
       currnt_state= status_str.replace(/\s/g, '');
      if(currnt_state!=""){   
          $(".DownloadBtn").fadeIn();
          $(".upload_btn").fadeIn();
         }else{
          $(".DownloadBtn").fadeOut();
          $(".upload_btn").fadeOut();
         } 
});

$(document).on("click",".image_open",function(){
        $this = $(this); 
        $(".fancybox-button--download").attr("href","")
        var url = $this.parents("li").find("a").attr("data-href");
        var author = $this.parents("li").find("a").attr("data-author");
        var likes = $this.parents("li").find("a").attr("data-likes");
        
          $.fancybox.open([
            {
              src  : url,
              opts : {
                caption : 'Likes <span class="fa fa-thumbs-up"></span> '+likes+' <br> Photo By <b> '+author+' </b>',
              }
            } 
                ], {
            loop : true,
            thumbs : {
              autoStart : true
            },buttons: [
              "zoom",
              "share",
              "slideShow",
              "fullScreen",
              //"download",
              //"thumbs",
              "close"
            ], });
      });
      
$(".upload_btn").click(function(){
//        $('.stockdiv_add_wizard').removeAttr("style");
      $this = $(this);
      storedFiles['file_name']= [];
      $this.button('loading');
     // console.log("formdata",$('#uploadForm').serialize());
      $.ajax({
          type:"POST",
          url:  baseUrl+"customer/request/download_remote_server",
          data:$('#uploadForm').serialize(),
          dataType: 'json',
          success: function (response) {
           // $this.button('reset');
            var count = ''; 
            if (response.status == '1') {
//                console.log("response",response);
                //$('.stockdiv_add_wizard').css('display','none');
                if (response.files.length > 0) {
                    //console.log("123231",response.files.length);
                    var files ='';
                    count =  response.img_link.length; 
                    $.each(response.files,function (k, v) {  
                         storedFiles.push({
                          "file_name":v,
                          "file_size":response.file_size[k],
                          "error":false,
                          "image_link":response.img_link[k],
                          "file_path":response.path[k],
                         });
                        $(".uploadFileListContain").html(createStoredFilesHtml());
//                        $(".content_Container").html(createStoredFilesHtml());
                        $(".content_Container").html(" ");
                            if(count == 1){
                                 files = "file";
                            }else{
                                  files = "files";
                            }
                             
//                        $(".content_Container").html("<span>" +count+" "+files+"  uploaded</span>");
                        
                        $(".ad_new_reqz").show();
                        $('#request_submit').prop('disabled', false);
                    });
                    //toastr.success("<span>" +count+" "+files+"  uploaded</span>");
                    toastr.success("<span>" +count+" "+files+"  uploaded</span>", 'Uploaded Items',{ 
                        showDuration: 5000,
                        positionClass: 'toast-top-right',
                        progressBar: true
                        });

                     attach_ment(); 
                }
            }else{
                 var html = ''; 
                 var msg = ''; 
                 var length  = response.filename.length; 
                 if(length == 1){
                            var Efiles = "File";
                        }else{
                            Efiles  = "Files";
                        }
                $.each(response.filename,function (k, value) {
                    
                   html += "<a href='"+response.path[k]+value+"' target='_blank'>"+value+"</a> &nbsp; ";
                 });
                 $(".uploaded-items").addClass("custom_outer");
                  $(".ad_new_reqz").hide();
                   toastr.error("<span class='custom_error'>"+Efiles+" already exist on server " + html+"</span>");    
             //$(".content_Container").html("<span class='custom_error'>"+Efiles+" already exist on server " + html+"</span>");
            }
        }
      });
    });
    
function attach_ment(){
    $(".upload_btn").button('reset');
//    console.log("get_method",get_method);
    setTimeout(function(){  
    if(get_method == "add_request_new"){
         $("#uploadstock").modal("hide");
         //$("#request_submit").trigger("click"); 
        }else{
          $("#"+get_method+"_btnid").trigger("click");    
        }
 }, 1000);
}


$("#search_keywrd").keypress(function(event) { 
    if (event.keyCode === 13) { 
        event.preventDefault();
//        $("#addbrandspopup").modal('hide');
        $(".serach_stock_btn").click(); 
    } 
}); 

$(document).on('click','.serach_stock_btn',function(e){
    var search_keyword = $("#search_keywrd").val();
//    console.log(search_keyword);
    if(search_keyword != ''){
     $('.error_search_msg').text('');
     $('#uploadstock').modal('show');
     $(".ajax_searchload").show();
     $(".serach_stock_btn").removeClass("active");
     $(this).addClass("active");
     search_images(search_keyword, load_more = "1", type = "sr",'all','portrait','all');
    }else{
        $('.error_search_msg').text('This field is required.');
    }
});

function search_images(search_keyword, load_more, type, filter,filter_unsplash,default_text) {
    if ($.trim(search_keyword)!= "" && $.trim(search_keyword) != null) {
       $.ajax({
            type: "POST",
            url:  baseUrl+"customer/request/get_all_imagesPost",
            data: {"keyword":search_keyword,"filter":filter,"load_more":load_more,"filter_un":filter_unsplash,"filter":filter},
            dataType:'json',
            success: function (res) {
            if (parseInt(res[0].totalHits) > 0) {
                $("#image_gallery").removeClass("custom-error");
                $(".search_head").show();
                $("#search").css("border-color", "#dddddd");
                if (parseInt(res[0].total) > 20 && parseInt(res[0].totalHits) <= parseInt(res[0].total)) {
                    $("#loadMore").show();
                    $("#loadMore").attr("data-count", load_more);
                } else if (parseInt(res[1].total) > 20) {
                    $("#loadMore").show();
                    $("#loadMore").attr("data-count", load_more);
                }else{
                    $("#loadMore").hide();
                }
                var html = '';
                var html2 = '';
                var error = '';
             //   var loaderHtml  = '<div class="loader_button"><div class="ajax_se_loaderbtnn" style="display:none;"> <img src="<?php echo base_url(); ?>public/assets/img/ajax-loader.gif"></div> <a href="#" id="loadMore" style="display:none;" onclick="load_more_images($(this))">Load More </a></div>'; 
                $("#search").css("border-color", "#dddddd");
                $(".errorSpan").hide();
              //  result from pixabay                
                $.each(res[0].hits, function(i, original_data) { 
                     var count = i + 1;
                     html += '<li class="item thumb"><div class="content box-overflow"> <a data-href="'+original_data.largeImageURL+'" data-from="pixabay" data-author="'+original_data.user+'" data-likes="'+original_data.likes+'" data-id="'+original_data.id+'" data-pageUrl ="'+original_data.pageURL+'"></a><img id="img_gallary_px' + load_more + count + '" src=' + original_data.webformatURL + ' alt=' + search_keyword + ' style="width:100%;opacity:0; "class="gallary_thumb"><i class="checkmark_pop" aria-hidden="true"></i></div><div class="credit-area" style="opacity:0;" id="img_gallary_px' + load_more + count + 'cr"><a href="'+original_data.pageURL+'" target="_blank"><img src="https://pixabay.com/favicon-32x32.png"></a></div></li>'; 
                });
                //  result from unsplash
                $.each(res[1].results, function(i, original_data) { 
                 var count = i + 1;
                 html2 += '<li class="item thumb"><div class="content box-overflow"> <a data-href="'+original_data.urls.regular+'" data-from="unsplash"  data-authorlink = "'+original_data.user.links.html+'" data-author="'+original_data.user.name+'" data-likes="'+original_data.likes+'" data-id="'+original_data.id+'" data-pageUrl ="'+original_data.links.html+'"></a><img id="img_gallary_un' + load_more + count + '" src=' + original_data.urls.regular + ' alt=' + original_data.alt_description + ' style="width:100%; opacity:0;" class="gallary_thumb"><i class="checkmark_pop" aria-hidden="true"></i></div><div class="credit-area" style="opacity:0;" id="img_gallary_un' + load_more + count + 'cr"><a href="'+original_data.links.html+'" target="_blank"><img src="https://unsplash.com/favicon-32x32.png"></a></div></li>';
                });
            } else {
                $(".search_head").hide();
                $(".errorSpan").hide();
                html = '';
                $("#image_gallery").addClass("custom-error");
                html += '<h4 class="no_data"> No Data Found for Search <b>"' + search_keyword + '"</b> </h4>';
                //loader_hide();
                $("#loadMore").hide();
            }
            if (type != "lm") {
             // loader_show();
             
                $(".search_head").html("<h2> Result For your search keyword <b>"+search_keyword+"</b> & search type <b>"+default_text+"</b></h2>");
                $("#image_gallery").html(html);
                $("#image_gallery").append(html2);
               CheckImageLoad('gallary_thumb'); 
               
                 
                // make_gridForImages(); 
            } else {
               
              //loader_show();
               $(".search_head").html("<h2> Result For your search keyword <b>"+search_keyword+"</b> & search type <b>"+default_text+"</b></h2>");
               $("#image_gallery").append(html);
               $("#image_gallery").append(html2);
                 CheckImageLoad('gallary_thumb'); 
                 
                 
 }
             $(".ajax_searchload").hide();
             $(".ajax_se_loaderbtnn").hide();
//            loader_hide();
        }
    });
    } else {
         $(".ajax_searchload").hide();
//        loader_hide();
        $(".search_head").hide();
        $("#search").css("border-color", "red");
        $(".errorSpan").show();
    }
}

function CheckImageLoad(imageClass){
    $('.'+imageClass)
        .load(function(){
        var imgId = $(this).attr("id");
        //console.log("imgId",imgId); 
            make_gridForImages(imgId); 
         })
        .error(function(){
            console.log("Found Error in loading image");
        });
        return true; 
}

function make_gridForImages(imgid){
      $this = $(this); 
      setTimeout(function(){ 
            var grid = $(".grid");
            var getItem = grid.children('li');
            getItem.each(function(){
                 var getItem_height = 0;
                getItem_height = $(this).find('div').children("img").height(); 
//                console.log("getItem_height",getItem_height); 
                var rowHeight =  parseInt($(grid).css( "grid-auto-rows" ));
                var rowGap = parseInt($(grid).css('grid-row-gap'));
                var rowSpan = Math.ceil((getItem_height + rowGap) / (rowHeight + rowGap)); 
//              console.log("rowSpan",rowSpan); 
                // $(this).css( "grid-row-end", "span " + rowSpan  );
                $(this).css( "grid-row-end", "span " + (rowSpan - 1) );
                $("#"+imgid).css("opacity","1");
                $("#"+imgid+"cr").css("opacity","1");
            });
        },1000);
}    

function load_more_images(event) {
    $(".ajax_se_loaderbtnn").show();
//    $(".ajax_loader").show();
    var search_keyword = $("#search_keywrd").val();
//    var filter = $("#image_filter").val();
//    var filter_unsplash;
//    filter_unsplash = orientation_filter(filter,"");
//    var default_text = $("#image_filter").val();
//    default_text = orientation_filter("", default_text);
    var current_count = event.attr("data-count");
    var nextLoad = 1;
    var load_more = parseInt(current_count) + parseInt(nextLoad);
    search_images(search_keyword, load_more, type = "lm", 'all','portrait','portrait');
}

$(document).on("click", ".delete_file", function () {
    var file_index = $(this).data("file-index");
    deletedFiles.push($(this).data("id"));
    storedFiles.splice(file_index, 1);
    var file_name = $(this).data("file-name");
    var folderPath = $(".upload_path").val();
    var dataString = 'file_name=' + file_name + '&folderPath=' + folderPath;
    $("#file" + file_index).remove();
    var $fileListContainer = $(".uploadFileListContain");
    $fileListContainer.append('<div class="ajax-img-loader"><img src="' + $assets_path + '"img/default-img/Loading_icon.gif" height="150"/></div>');
    $fileListContainer.html(createStoredFilesHtml());
    $.ajax({
        method: 'POST',
        url: baseUrl + "customer/request/delete_file_from_folder",
        data: dataString,
        success: function (response) {

        }
    });
});

function createlogoStoredFilesHtml(upload) {
        var ouptput = "";
        $.each(storedFiles[upload], function (key, value) {
            if (value.error == false) {
                $('.ajax-img-loader').css('display', 'none');
                ouptput += '<div  class="uploadFileRow " id="logo_file' + key + '"><div class="col-md-12"><div class="extnsn-lst">\n\
        <p class="text-mb">' + value.file_name + '<strong> (' + formatFileSize(value.file_size, 2) + ') </strong></p>\n\
        <p class="cross-btnlink">\n\
        <a href="javascript:void(0)" class="logo_delete_file" data-upload="' + upload + '" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
        <span>x</span>\n\
        </a>\n\
        <input type="hidden" value="' + value.file_name + '" name="upload_file_name[]" class="logo_delete_file"/><input type="hidden" value="' + upload + '" name="upload_type[]" class="upload_type_file"/><input type="hidden" value="" name="logo_upload_path" class="logo_upload_path" />\n\
        \n\
        \n\
        \n\
        </p>\n\
        </div></div></div>';
            } else {
                ouptput += '<div  class="uploadFileRow" id="logo_file' + key + '"><div class="col-md-12" ><div class="extnsn-lst error-list">\n\
        <p class="text-mb">' + value.file_name + '<br/><strong>' + value.error_msg + '</strong></p><p class="cross-btnlink">\n\
        <a href="javascript:void(0)" class="logo_delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
        <span>x</span>\n\
        </a>\n\
        \n\
        \n\
        \n\
        \n\
        </p>\n\
        </div></div></div>';
            }
        });
        return ouptput;
    }

function createStoredFilesHtml() {
    var ouptput = "<div class='attached-files'><h3>Attachments</h3><div class='row'>";
    //console.log("storedFiles",storedFiles);
    $.each(storedFiles, function (key, value) {
        var imageLink = "";
//        console.log(value);
//        console.log("value.file_path",value.file_path);
        if(value.file_path){
           var parts = (value.file_path).split('/');
           var lastSegment = parts.pop() || parts.pop(); 
        }
        if (value.error == false) {
           // console.log(lastSegment);
            $('.ajax-img-loader').css('display', 'none');
            if (value.image_link != "") {
                imageLink = value.image_link;
                ouptput += '<div class="uploadFileRow" data-temp='+lastSegment+' id="file' + key + '"><div class="col-md-6"><div class="extnsn-lst">\n\
            <p class="text-mb">' + value.file_name + '<strong> (' + formatFileSize(value.file_size, 2) + ') </strong></p>\n\
            <p class="cross-btnlink">\n\
            <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
            <i class="fa fa-times"></i>\n\
            </a>\n\
            <input type="hidden" value="' + value.file_name + '" name="delete_file[]" class="delete_file"/><input type="hidden" value="" name="upload_path" class="upload_path" /><input type="hidden" value="' + imageLink + '" name="image_link[]"/>\n\
            \n\
            \n\
            \n\
            </p>\n\
            </div></div></div>';
            }
        } else {
            ouptput += '<div class="uploadFileRow" data-temp='+lastSegment+' id="file' + key + '"><div class="col-md-6" ><div class="extnsn-lst error-list">\n\
            <p class="text-mb">' + value.file_name + '<br/><strong>' + value.error_msg + '</strong></p><p class="cross-btnlink">\n\
            <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
            <i class="fa fa-times"></i>\n\
            </a>\n\
            \n\
            \n\
            \n\
            \n\
            </p>\n\
            </div></div></div>';
        }
    });
    ouptput += "</div></div>";
    return ouptput;
}

function formatFileSize(bytes, decimalPoint) {
    if (bytes == 0)
        return '0 B';
    var k = 1000,
            dm = decimalPoint || 2,
            sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function getSummary(){
    var htmlData = '';
     var appendedpref = '';
     var fileName = "";
    var pro_name = $('.pro_name').val();
    var selctedcat = $('#cat_req').children("option:selected").text();
    var color_pref = $("input[name='color_pref[]']:checked").val();
    if(color_pref == 'Color Code'){
       color_pref = $('#color_pref_sel').children("option:selected").text();
       var color_value = $("input[name='color_values[]']").map(function(){return $(this).val();}).get();
       color_value = color_value.join(', ');
       appendedpref = color_pref + ' ('+color_value+')';
    }else{
       appendedpref = color_pref;
    }
    var deliverable = $("input[name='filetype[]']:checked").map(function(){return $(this).val();}).get().join(', ');
    var des_dimnsn = $('.des_dimnsn').val();
//    var design_req = $('.design_reqntt').val();
    var design_req = [];
    $(".design_reqntt").each(function () {
        design_req.push($(this).val());
    });
//    console.log("design_req",design_req);
    $('.proj_value').html(pro_name);
    $('.selectedcat_name').html(selctedcat);
    $('.des_dmmn').html(des_dimnsn);
    $('.color_preee').html(appendedpref);
    $('.deleverable').html(deliverable);
    $('.design_requirnmnt').html(design_req);
    $('.add_question_text').each(function() {
        var qlabel = $(this).find('label').text();
        var sumry = $(this).find('.getinsumry').val();
        var is_desc = $(this).find('.getinsumry').attr('name');
        if(is_desc === 'quest[2][]'){
            sumry = design_req;
        }else if(typeof sumry === 'undefined' ){
            sumry = '';
        }else{
            sumry = sumry;
        }
        htmlData += '<div class="wiz_summry_list"><h4 class="additional_qus">'+qlabel+'</h4><p class="additional_desc">'+sumry+'</p></div>'
    });
//    console.log("reqID",reqID);
//    console.log("dupID",dupID);
    if(reqID == '' && dupID == ''){
    $('.uploadFileRow').each(function(){
       var tmppath = $(this).attr('data-temp');
       var fullfilename = $(this).find('p.text-mb').text();
       var filename_arr = (fullfilename.split("("));
       var filename = filename_arr[0];
       if(filename){
          // console.log("filename",filename);
           fileName += '<span><img src='+$uploadPath+'temp/'+tmppath+'/'+filename+'></span>';
       }
    });
    if(fileName){
        $('.appndfile').html(fileName);
    }else{
        $('.appndfile').html("No attachment");
    }
    }
    $('.additional_divss').html(htmlData);
}

$(document).on('click','.save_submit',function(){
    $('.save_submit').prop('disabled',true);
    var formData = $('#addRequestform').serialize();
    var formdata = new FormData();
    formdata.append('save_submit','save_submit');
    formdata.append('formData',formData);
    if(reqID){
     formdata.append('reqID',reqID);   
    }
    if(dupID){
      formdata.append('dupID',dupID);   
    }
    saveForm(formdata);
});

$(document).on('click','.draft_req',function(){
    $('.save_submit').prop('disabled',true);
    var formData = $('#addRequestform').serialize();
    var formdata = new FormData();
    formdata.append('draft_req','draft_req');
    formdata.append('formData',formData);
    if(reqID){
     formdata.append('reqID',reqID);   
    }
    if(dupID){
      formdata.append('dupID',dupID);   
    }
    saveForm(formdata);
});


function saveForm(formdata){
    $.ajax({
        type: 'POST',
        url: baseUrl + "customer/request/add_request_new",
        data: formdata,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (response) {
            $('.save_submit').prop('disabled',false);
//            console.log(response);
           // return false;
            if(response.status == 'success'){
              toastr_msg('success', response.msg);   
              window.location.href = baseUrl+'customer/request/design_request';
            }

        }
    });
}

$(document).on('keypress',".design_reqntt",function(e){
    var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13){
          add_description_Row(); 
          e.preventDefault();
        }
});

$(document).on('click','.enter_btn',function(){
  add_description_Row();    
});

function add_description_Row(){
    var htmll = '';
    htmll = '<div class="input_wrapper"><textarea class="form-control design_reqntt getinsumry reqclass" name="quest[2][]" placeholder="Press Enter for new line item"></textarea></div>';
    $('.add_wrapper').append(htmll);
}

/*******************edit button in wi****************/
$(document).on('click','.title_edit',function(){
    $('#1_field').css('display','block');
    $('#6_field').css('display','none');
    $("#1_field").addClass('active');
    $('.save_submit').css('display','none');
    $('.save_as_draft_btn').css('display','none');
    $('.next_req').css('display','inline-block');
});

$(document).on('click','.cat_edit',function(){
    $('#1_field').css('display','block');
    $('#6_field').css('display','none');
    $("#1_field").addClass('active');
    $('.save_submit').css('display','none');
    $('.save_as_draft_btn').css('display','none');
    $('.next_req').css('display','inline-block');
});

$(document).on('click','.des_edit',function(){
    $('#3_field').css('display','block');
    $('#6_field').css('display','none');
    $("#3_field").addClass('active');
    $('.save_submit').css('display','none');
    $('.save_as_draft_btn').css('display','none');
    $('.next_req').css('display','inline-block');
});

$(document).on('click','.color_edit',function(){
    $('#3_field').css('display','block');
    $('#6_field').css('display','none');
    $("#3_field").addClass('active');
    $('.save_submit').css('display','none');
    $('.save_as_draft_btn').css('display','none');
    $('.next_req').css('display','inline-block');
});

$(document).on('click','.del_edit',function(){
    $('#3_field').css('display','block');
    $('#6_field').css('display','none');
    $("#3_field").addClass('active');
    $('.save_submit').css('display','none');
    $('.save_as_draft_btn').css('display','none');
    $('.next_req').css('display','inline-block');
});

$(document).on('click','.adqst_edit',function(){
    $('#4_field').css('display','block');
    $('#6_field').css('display','none');
    $("#4_field").addClass('active');
    $('.save_submit').css('display','none');
    $('.save_as_draft_btn').css('display','none');
    $('.next_req').css('display','inline-block');
});

$(document).on('click','.attchmnt_edit',function(){
    $('#5_field').css('display','block');
    $('#6_field').css('display','none');
    $("#5_field").addClass('active');
    $('.save_submit').css('display','none');
    $('.save_as_draft_btn').css('display','none');
    $('.next_req').css('display','inline-block');
});

/************Add brands****************/
var max_fields_limit = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        var name = $(this).parent().find(".f_cls_nm").attr('name');
        if (x < max_fields_limit) { //check conditions
            x++; //counter increment
            $('.input_fields_container').append('<div><div class="remove-pre"><input type="text" class="form-control" name="'+name+'"/><a href="#" class="remove_field"><i class="icon-gz_plus_icon"></i></a></div></div></div>'); //add input field
        }
});
 $('.add_more_script').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        var name = $(this).parent().find(".f_cls_nm").attr('name');
        var tracking_nm = $(this).parent().find(".f_trkngcls_nm").attr('name');
        var pos_nm = $(this).parent().find(".f_scrpt_pos_nm").attr('name');
        var pg_nm = $(this).parent().find(".f_scrpt_pg_nm").attr('name');
        if (x < max_fields_limit) { //check conditions
            x++; //counter increment
            $('.input_fields_container').append('<div><div class="remove-pre"><div class="col-lg-12 col-md-12"><label class="form-group"><p class="label-txt">Name</p><input name="'+name+'" class="input"/><div class="line-box"><div class="line"></div></div></label></div><div class="col-lg-12 col-md-12"><label class="form-group"><p class="label-txt">Paste your tracking code here</p><textarea class="input" name="'+tracking_nm+'"></textarea><div class="line-box"><div class="line"></div></div></label></div><div class="col-lg-6 col-md-6"><label class="form-group"><p class="label-txt label-active">Position inside the code</p><select name="'+pos_nm+'" class="input"><option value="before_head">Before < /HEAD ></option><option value="before_body">Before < /BODY ></option></select></label></div><div class="col-lg-6 col-md-6"><label class="form-group"><p class="label-txt label-active">Show only on specific page</p><select name="'+pg_nm+'" class="input"><option value="login">Login</option><option value="signup">Signup</option><option value="cus_portl">Customer Portal</option><option value="all_pages">All Pages</option></select></label></div><a href="#" class="remove_field"><i class="icon-gz_plus_icon"></i></a></div></div></div>'); //add input field
        }
    });
    $('.input_fields_container').on("click", ".remove_field", function (e) { //user click on remove text links
        e.preventDefault();
        jQuery(this).parent('div').remove();
        x--;
    });
    
$('#logo_file_input').change(function () {
var $fileListContainer = $(".logo_uploadFileListContain");
$fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'img/ajax-loader.gif"/></div>');

var fileInput = $('#logo_file_input')[0];
if (fileInput.files.length > 0) {

    var formData = new FormData();
    $.each(fileInput.files, function (k, file) {
        var fileName = file.name;
        var type = fileName.split('.').pop();
        var name = fileName.split('.');
        var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
        var newFileName = name_file + '.' + type;
        formData.append('logo_upload[]', file, newFileName);
    });

    call_ajax_to_upload(formData, $fileListContainer);
} else {
//    console.log('No Files Selected');
}
});

$('#materials_file_input').change(function () {
    var $fileListContainer = $(".materials_uploadFileListContain");
    $fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'img/ajax-loader.gif"/></div>');

    var fileInput = $('#materials_file_input')[0];
    if (fileInput.files.length > 0) {

        var formData = new FormData();
        $.each(fileInput.files, function (k, file) {
            var fileName = file.name;
            var type = fileName.split('.').pop();
            var name = fileName.split('.');
            var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
            var newFileName = name_file + '.' + type;
            formData.append('materials_upload[]', file, newFileName);
        });

        call_ajax_to_upload(formData, $fileListContainer);

    } else {
     //   console.log('No Files Selected');
    }
});

$('#additional_file_input').change(function () {
        var $fileListContainer = $(".additional_uploadFileListContain");
        $fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'img/ajax-loader.gif"/></div>');

        var fileInput = $('#additional_file_input')[0];
        if (fileInput.files.length > 0) {

            var formData = new FormData();
            $.each(fileInput.files, function (k, file) {
                var fileName = file.name;
                var type = fileName.split('.').pop();
                var name = fileName.split('.');
                var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
                var newFileName = name_file + '.' + type;

                formData.append('additional_upload[]', file, newFileName);
            });

            call_ajax_to_upload(formData, $fileListContainer);
        } else {
        //    console.log('No Files Selected');
        }
    });
    
 $(document).on("click", ".logo_delete_file", function () {
        var file_index = $(this).data("file-index");
        var file_upload = $(this).data("upload");
        deletedFiles.push($(this).data("id"));
        //console.log('check',file_upload);
        storedFiles[file_upload].splice(file_index, 1);
        var file_name = $(this).data("file-name");
        var folderPath = $(".logo_upload_path").val();
        var dataString = 'file_name=' + file_name + '&folderPath=' + folderPath;
        if (file_upload == 'logo_upload') {
            var $fileListContainer = $(".logo_uploadFileListContain");
        }
        if (file_upload == 'materials_upload') {
            var $fileListContainer = $(".materials_uploadFileListContain");
        }
        if (file_upload == 'additional_upload') {
            var $fileListContainer = $(".additional_uploadFileListContain");
        }

        $fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'img/ajax-loader.gif" height="150"/></div>');
        $fileListContainer.html(createlogoStoredFilesHtml(file_upload));
        $.ajax({
            method: 'POST',
            url: baseUrl+"customer/BrandProfile/delete_brand_file_from_folder",
            data: dataString,
            success: function (response) {

            }
        });
});
    
function call_ajax_to_upload(formData, $fileListContainer) {
        $.ajax({
            method: 'post',
            url: baseUrl+"customer/BrandProfile/upload_logo_process",
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
//                   console.log(response);
                if (response.status == '1') {
                    if (response.type == 'logo_upload') {
                        if (response.logo_upload.length > 0) {
                            $.each(response.logo_upload, function (k, v) {
                                 //console.log("v",v[0]);
                                storedFiles['logo_upload'].push(v[0]);
                                //console.log(storedFiles);
                                $fileListContainer.html(createlogoStoredFilesHtml('logo_upload'));
                            });
                        }
                    }
                    if (response.type == 'materials_upload') {
                        if (response.materials_upload.length > 0) {

                            $.each(response.materials_upload, function (k, v) {
                                storedFiles['materials_upload'].push(v[0]);
                                $fileListContainer.html(createlogoStoredFilesHtml('materials_upload'));
                            });
                        }
                    }
                    if (response.type == 'additional_upload') {
                        if (response.additional_upload.length > 0) {
                            $.each(response.additional_upload, function (k, v) {
                                storedFiles['additional_upload'].push(v[0]);
                                $fileListContainer.html(createlogoStoredFilesHtml('additional_upload'));
                            });
                        }
                    }
                }
            }
        });
    }



$(document).on('click','#brand_profile_submit_add',function(e){
    e.preventDefault();
    var formData = $('#addbranDs').serialize();
    var formdata = new FormData();
    formdata.append('brand_profile','brand_profile');
    formdata.append('formData',formData);
    var brandname = $("input[name=brand_name]").val();
    var description = $('#comment').val();
//    console.log("brandname",brandname);
//    console.log("description",description);
    if(brandname == ''){
        toastr_msg('error', "Please enter brand name!"); 
        return false;
    }if(description == ''){
        toastr_msg('error', "Please enter description!"); 
        return false;
    }else{
       $.ajax({
        type: 'POST',
        url: baseUrl + "customer/BrandProfile/add_brand_profile/true",
        data: formdata,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (response) {
            if(response.status == 'success'){
              $("#addbrandspopup").modal('hide');
              toastr_msg('success', "Brand added successfully!");
              var length = $('#brand_sell > option').length;
              $('#brand_sell').find('.none').before('<option value='+response.id+'>'+brandname+'</option>');
              $('select#brand_sell').val(response.id).trigger('change');
              return false;
            }
        }
    }); 
    }
});

function check_activeClass(){
 var id = $(".list-header-blog").attr("id");
    if($('#'+id+' li:first-child').hasClass("active")==false){ 
      $('#'+id+' li:first-child a').trigger("click"); 
       setTimeout(function() { 
         introJs().start();
      }, 700);
    }else{
        introJs().start();
    }
}

function toastr_msg(mgstyp, msgtext) {
    if (mgstyp == "success") {
        toastr.success(msgtext, {});
    } else {
        toastr.error(msgtext, {});
    }
}


$(function() {
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );
    $.validator.addClassRules({
        des_dimnsn: {
            required: true,
            //regex: /^[a-z A-Z 0-9 "_@!#$%-^&*()]+[a-z]{1,40}$/,
           // minlength: 5,
            maxlength: 50,
        },
        color_drpdn:{
         required: true,   
        },
        f_color_nm:{
          required: true, 
          maxlength: 20,
          regex: /^#([0-9A-F]{3}){1,2}$/i,
        },
        reqclass:{
        required: true, 
        minlength: 5,
       // maxlength: 55,
        //regex: /^[a-z A-Z 0-9 -_()]+[a-z -()#%^&*/]{1,}$/
        }
    });

    $("#addRequestform").validate({
       rules: {
        project_name: 
        {
         required: true,
         minlength: 4,
         maxlength: 200,
         regex: /^[a-z A-Z 0-9 _-]+[a-z]{1,40}$/,
        },
        category_id:{
         required: true   
        },
        subcategory_id:{
         required: true     
        },
     },
     messages: {
        project_name:{
            required: 'Please enter project title',
            minlength: 'Project title must have length between 4 to 200',
        },
        category_id: "Please choose category name",
        subcategory_id: "Please choose subcategory name",
     },
     errorPlacement: function(error, element) {
         if (element[0].id == 'pro_name') {
            error.appendTo(element.parent());
         }
         if (element[0].id == 'cat_req') {
            error.appendTo(element.parent());
         }
         if (element[0].id == 'subct_id') {
            error.appendTo(element.parents().find('.show_err'));
         }
         if (element[0].id == 'des_dimnsn') {
            error.appendTo(element.parent());
         }
         if(element[0].id == 'color_pref_sel'){
          error.appendTo(element.parent());   
         }
        if(element[0].id == 'color_val'){
          error.appendTo(element.parent());   
         }
         if(element.hasClass('reqclass')){
           error.appendTo(element.parent());   
         }
     },
     submitHandler: function(form) {
         // form.submit();
         return true;
      }
   });
});

$(document).on('change','#change_request_layout',function(){
 var layout = $("#change_request_layout option:selected").val();
    if(layout == '0'){
         window.location.href = baseUrl+'customer/request/add_new_request';
    }else{
         window.location.href = baseUrl+'customer/request/add_request_new';
    }
});

$("#brand_sell").select2( {
    placeholder: "Choose brand profile",
});

/*$(document).ready(function(){*/
    $("#cat_req").select2( {
        placeholder: "Choose Category",
        allowClear : true,
        ajax: { 
            url: baseUrl + "customer/Request/searchCatSubcat",  
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
              return {
                 results: response
              };
            },
            cache: true
        }
    });
/*});*/


//$(document).on('change','.sample_subcat',function(){
//    var sample = [];
//    $("input[name='sample_subcat[]']:checked").each(function () {
//        sample.push($(this).val());
//    }); 
//    autoSave(2,sample);
//});

$(document).on('change','#brand_sell',function(){
    var brand = $("#brand_sell option:selected").val();
     $('#3_field').find('.saved_msg').html("All changes saved in Draft"); 
//    autoSave(2,brand);
    autoSave();
    
});

$(document).on('blur','.questionsup',function(){
    if(this.value.length > 4){
    delay(function () {
     autoSave();   
    }, 1000);
    }
});

function autoSave(n)  
{  
//    console.log("edir");
    var formData = $('#addRequestform').serialize(); 
    var savedmsg = "All changes saved in Draft";
//    console.log("formData",formData);
//    return false;
    $.ajax({  
        type: 'POST',
        url: baseUrl + "customer/Request/add_request_new",  
        data: formData,
        dataType: 'json',
        processData: false,
         success:function(data)  
         {
            if(data.drafted_id){
                var reqID = data.drafted_id;
            }else{
                var reqID = 0;
            }
            if(n == 1 || reqID){
               $('#drafted_id').val(reqID);
               $('#1_field').find('.saved_msg').html(savedmsg);
            }
            if(n == 2){
               $('#2_field').find('.saved_msg').html(savedmsg);   
            }
         }  
    });  
}  
     
