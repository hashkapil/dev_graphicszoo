/************Wizard subdomain form****************/
$(document).ready(function () {
    $('#design_type').chosen();
    $('.sel_brandcheck').chosen();
});
var res; 
$(document).on('click','.back',function(){
    var id = $("fieldset.active").data('chck');
    var client = $("input[name='client']:checked").val();
    if(id == 3 && is_agency != 1){ //for go back from brand profile to general setting
        Subdomainwizard(2,'back',3); 
    }else if(id == 7 && is_agency != 1){ //for go back from get started to team member
        Subdomainwizard(5,'back',7); 
        $('.save').css('display','inline-block');
    }else{
        Subdomainwizard(id,'back');
    }
    if(id == 6 || id == 5 || id == 4){ // show skip button for email,team and payment steps to agency user
        $("#skip_without_save").css("display","inline-block");
    }else if(id == 7 && is_agency == 1 && client == 1){
         $("#skip_without_save").css("display","inline-block");
    }else if(id == 7 && is_agency != 1){ // show skip button for team step that are not agency user
        $("#skip_without_save").css("display","inline-block");
    }else{
        $("#skip_without_save").hide(); // hide skip button
    }
    var brand = $("#exampleInputName").val();
    var comment = $("#comment").val();
//    if(id == 3){ // enable save & next button if user back from brand profile step 
//        $(".save_next.save").prop("disabled",false);
//    }
    if(id == 4 && (brand == "" || comment == "")){ // disable save & next button if user not fill brand name and description
        $(".save_next.save").prop("disabled",true);
    }else{
        $(".save_next.save").prop("disabled",false); // enable save & next button if user back from brand profile step 
    }
    
    var add_brand = $("#want_create").attr("data-flag");
    if(id == 4 && add_brand == 0){ // hide save & next if user don't want create brand profile
        $(".save_next.save").fadeOut();  // hide save button
      //  $("#skip_without_save").hide();
    }
});

$(document).on('change', '.pmt_check_item input[name="payment_mode"]', function () {
    var value = $(this).val();
//    console.log("value",value);
    if(value == 1){
        $(".strp_acnt").show();
        $(".subs_text em").html("Complete the form below to add new subscription. This will be an offline payment subscription, once you will connect with stripe then you can add online payment subscriptions as well.")
        $(".subscription_blk").hide();
    }else{
        $(".strp_acnt").hide();
//        $(".subs_text em").html("Complete the form below to add new subscription.");
        $(".subscription_blk").show();
    }
    
});

$(document).on('submit','#subdomainwiz',function(e){
    e.preventDefault();
    
    var fid = $("fieldset.active").attr('id');
    var formData = $("#"+fid).serialize();
    var id = $("fieldset.active").data('chck');
    var invalid_email = $(".invalid_email").val();
    var is_smtp_enable = ($("#is_smtp_enable").is(':checked')) ? 1 : 0;
//    console.log("is_smtp_enable",is_smtp_enable);
    if(id == 6){
        var client = $("#"+fid+" input[name='client']:checked").val();
        if(client == undefined ||client == ""){
            $("#"+id+"_field .val_err").addClass("alert-danger");
            $("#"+id+"_field .val_err").html("<p>Please select atleast one</p>");
            $('html, body').animate({
                scrollTop: $("#"+id+"_field .val_err").offset().top - 100
            }, 'slow');
            return false;
        }
        
         //Subdomainwizard(id,'next'); 
         
//        else if(client == 1){
//            $("#"+id+"_field .val_err").removeClass("alert-danger");
//            $("#"+id+"_field .val_err").html("");
//            Subdomainwizard(id,'next'); 
//        }else if(client == 2){
//            $("#"+id+"_field .val_err").removeClass("alert-danger");
//            $("#"+id+"_field .val_err").html("");
//            Subdomainwizard(5,'next',3); 
//            if(invalid_email == "1"){
//               $(".save_next.save").prop("disabled",true);
//            }
//        }
    }else{
        if(id == 5 && invalid_email == "1"){
           $(".save_next.save").prop("disabled",true);
        }else if(id == 1 && is_agency != 1 && invalid_email == "1"){
            $(".save_next.save").prop("disabled",true);
        }
        //SavesubdomainWizard(formData,id);
    }
    SavesubdomainWizard(formData,id);
    if(id ==2 || id == 3 || id == 5 || id == 4){ // show skip button for email,team and payment steps to agency user
        setTimeout(function () {
            $("#skip_without_save").css("display","inline-block");
        }, 1000);
    }else if(id == 6 && is_agency == 1 && res == 0){
        setTimeout(function () {
            $("#skip_without_save").css("display","inline-block");
        }, 1000);
    }else if(id == 1 && is_agency != 1){ // show skip button for team step that are not agency user
        setTimeout(function () {
            $("#skip_without_save").css("display","inline-block");
        }, 1000);
    }else{
        $("#skip_without_save").hide();  // hide skip button
    }
    
    if(id == 4 && is_agency == 1 && is_smtp_enable == 1){
        $(".port_settings").show();
    }
    var brand = $("#exampleInputName").val();
    var comment = $("#comment").val();
    if(id == 2 && (brand == "" || comment == "")){ // disable save & next button if user not fill brand name and description
        $(".save_next.save").prop("disabled",true);
    }
    
});

function Subdomainwizard(n,type,nxtid){
    
  //  var is_agency = $('#subdomainwiz').data('isagency');
    var primry_colr = $("#d_primary_color").val();
    var client = $("input[name='client']:checked").val();
   if(type == 'next'){
        var field_id = parseInt(n)+1;
        if(is_agency == 0){
           $('#2_field').css('display','none');  
        }
        if(n == 6){
            $('.save').css('display','none');
        }else{
            $('.save').css('display','inline-block');
        }
        if(n){
             $('.back').css('display','inline-block');  
        }
        if(n == 5){
            $(".save_next.save").hide();  // hide save button
            $("#skip_without_save").hide();  // hide skip button
        }
        
        if(n == 5 && client == 1 && is_agency == 1){
            $(".save_next.save").show();  // show save button
            $("#skip_without_save").css("display","inline-block");  // show skip button
        }
        $('html, body').animate({
            scrollTop: $(".welcome_wizard").offset().top - 100
        }, 'slow');
      //  $('input[type="text"]').blur();
      //  $('#'+field_id+'_field').find('input[type="text"]:first').addClass("focusclstest"); 
//        $('#'+field_id+'_field').find('input[type="text"]:first').focus(); 
        var add_brand = $("#want_create").attr("data-flag");
        if(n == 2 && add_brand == 0){ // hide save & next if user don't want create brand profile
            $(".save_next.save").fadeOut();  // hide save button
        }else if(n == 1 && add_brand == 0 && is_agency != 1){ // hide save & next if user don't want create brand profile
            $(".save_next.save").fadeOut();  // hide save button
        }
       
    }else{
       //if(n == 8 || n == 9){ //show save button when click on from last two steps
       if(n == 7 && is_agency == 1 && client != 1){
           $('.save_next.save').css('display','none');
       }else{
           $('.save').css('display','inline-block');
       }
      // }
//       if(n == 6){ //back button from add team member steps
//            var client = $("input[name='client']:checked").val();
//            if(client == '2'){
//                var field_id = '3';
//            }else{
//                var field_id = parseInt(n)-1;
//            }
//       }else{
            var field_id = parseInt(n)-1;
//       }
//       if(n == 4 && is_agency != 1){
//           var field_id = 1;
//       }
       if(n == '2'){   //hide back button from first step
         $('.back').css('display','none');  
         $('.save').css('display','inline-block');
         $('.finish').css('display','none');
       }
        if(n == 5){ //hide email setting that show required field on click back button
            $(".port_settings").css('display', 'none');
            $(".port_settings input").prop("required", false);
            $(".is_smtp_enable input").prop("check", false);
        }
        
        $(".save_next.save").prop("disabled",false);
      
    }
  
   if(nxtid != ""){
       $('#'+nxtid+'_field').css('display','none'); //for client section to hide
       $('#'+nxtid+'_field').removeClass('active');
       $('#'+nxtid+'_bar').removeClass('active');
       $('#'+nxtid+'_bar').removeClass('cstm_primary_colr');
       $('#'+nxtid+'_bar').removeAttr('style');
   }
   console.log("field_id",field_id);
   console.log("nxtid",nxtid);
   console.log("n",n);
//   console.log("n_id",n);
//   console.log("nxtid",nxtid);
    $('#'+field_id+'_field').css('display','block');
    $('#'+field_id+'_field').addClass('active');
    $('#'+n+'_field').css('display','none');
    $('#'+n+'_field').removeClass('active');
    $('#'+n+'_bar').removeClass('active');
    $('#'+n+'_bar').removeClass('cstm_primary_colr');
    $('#'+field_id+'_bar').addClass('active');
    $('#'+field_id+'_bar').addClass('cstm_primary_colr'); 
    $('#'+n+'_bar').css("background","");
    $('.cstm_primary_colr').css('background', '' + primry_colr + '');
    
}

function SavesubdomainWizard(formData,id,preview){
    var file = $('#onboardingpic').get(0).files[0];
    var brand_logo = $('#brand_logo').get(0).files[0];
    var logo_upload = $('#logo_file_input').get(0).files[0];
    var materials_upload = $('#materials_file_input').get(0).files[0];
    var additional_upload = $('#additional_file_input').get(0).files[0];
    var formdata = new FormData();
    formdata.append('profile', file);
    formdata.append('formData', formData);
    formdata.append('brand_logo', brand_logo);
    formdata.append('logo_upload', logo_upload);
    formdata.append('materials_upload', materials_upload);
    formdata.append('additional_upload', additional_upload);
    
    $.ajax({
        type: 'POST',
        url: baseUrl + "customer/AgencyUserSetting/SaveonboardingDetail",
        dataType: "json",
        data: formdata,
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status == "error" || data.status == 0){
                if(data.role && data.role != ""){
                     if(data.key == 0){
                         var animate_cls = ".adduser_item";
                         $("#"+id+"_field .adduser_item .not_allow").addClass("alert-danger");
                         $("#"+id+"_field .adduser_item .not_allow").html("<p>"+data.msg+"</p>");
                         $("#"+id+"_field .inner_team_"+data.key+" .not_allow").html("");
                         $("#"+id+"_field .inner_team_"+data.key+" .not_allow").removeClass("alert-danger");
                     }else{
                         var animate_cls = ".inner_team_"+data.key;
                         $("#"+id+"_field .inner_team_"+data.key+" .not_allow").addClass("alert-danger");
                         $("#"+id+"_field .inner_team_"+data.key+" .not_allow").html("<p>"+data.msg+"</p>");
                         $("#"+id+"_field .adduser_item .not_allow").html("");
                         $("#"+id+"_field .adduser_item .not_allow").removeClass("alert-danger");
                         
                     }
                     $('html, body').animate({
                            scrollTop: $("#"+id+"_field "+animate_cls+" .not_allow").offset().top - 100
                        }, 'slow');
                }else{
                    $("#"+id+"_field .not_allow").html("");
                    $("#"+id+"_field .not_allow").removeClass("alert-danger");
                    $("#"+id+"_field .val_err").addClass("alert-danger");
                    $("#"+id+"_field .val_err").html("<p>"+data.msg+"</p>");
                    console.log("field error","#"+id+"_field");
                    $('html, body').animate({
                        scrollTop: $("#"+id+"_field .val_err").offset().top - 100
                    }, 'slow');
                }
                res = 0; 
                 
            }else{
                if(preview == 1){
                    //$(".preview_subs").html("<a href='http://'"+data.domain_name+"/user_sign>");
                    if(data.domain_name && data.domain_name != ""){
                       // console.log("just testinggg");
                        window.open(
                                'https://'+data.domain_name+'/user_signup',
                                '_blank' // <- This is what makes it open in a new window.
                                );
                    }
                }else{
                    $("#"+id+"_field .not_allow").html("");
                    $("#"+id+"_field .not_allow").removeClass("alert-danger");
                    if(data.id && data.id != ""){
                        $("#f_subs_id").val(data.id);
                    }
                    if(data.brand_id && data.brand_id != ""){
                        $("#brand_id").val(data.brand_id);
                    }
                    $("#"+id+"_field .val_err").removeClass("alert-danger");
                    $("#"+id+"_field .val_err").html("");
                    if(id == 1 && is_agency != 1){ //to move general setting to team member
                        Subdomainwizard(2,'next',1); 
                    }else if(id == 4 && is_agency != 1){ //to move team member to get started section
                        Subdomainwizard(6,'next',4); 
                    }else{
                       Subdomainwizard(id,'next');  
                    }
                    res = 1;
                }
            }
        }
         
   });
}

$(document).on('click',".preview_subs", function (){
    var fid = $("fieldset.active").attr('id');
    var formData = $("#"+fid).serialize();
    var id = $("fieldset.active").data('chck');
    SavesubdomainWizard(formData,id,1);
});
$(document).on('change', '#d_primary_color', function () {
    var primary_color = $(this).val();
    $('.cstm_primary_colr').css('background', '' + primary_color + '');
});

$(document).on('change', '#d_secondary_color', function () {
    var secondary_color = $(this).val();
    $('.cstm_sec_btn').css('background', '' + secondary_color + '');
});
$(document).on('keyup', '.domain_url', function () {
    var ischecked = ($("#slct_domain_subdomain").is(':checked')) ? 1 : 0;
    var domain = $(this).val();
    if(ischecked == 1){
        $(".url_txt").html("http://"+domain);
    }else{
       $(".url_txt").html("https://"+domain+".graphicszoo.com"); 
    }
});

$(document).on('change','#slct_domain_subdomain',function(){
     var ischecked = ($(this).is(':checked')) ? 1 : 0;
     if(ischecked == 1){
         $('#setmaindomain_msg').modal('show');
         $('.save_next').prop('disabled',true);
         $('.confirmed_arecord').val(0);
     }else{
       $('.save_next').prop('disabled',false);  
       $('.subdomain_nm').css('display','flex');
       $('.domain_nm').css('display','none');
     }
});

$(document).on('click','.arec_cnfrm_btn',function(){
    $('.confirmed_arecord').val(1); 
    $('.subdomain_nm').css('display','none');
    $('.domain_nm').css('display','flex');
     $('.save_next').prop('disabled',false);
});

$(document).on('click','.arec_ntcnfrm_btn',function(){
   $("#slct_domain_subdomain").prop('checked', false);
   $('.subdomain_nm').css('display','flex');
   $('.domain_nm').css('display','none');
   $('.save_next').prop('disabled',false);  
   
});

$('.add_more_subscription').click(function (e) { //click event on add more fields button having class add_more_button
    e.preventDefault();
    x = 1;
    var name = $(this).parent().find(".plan_features").attr('name');
    if (x < max_fields_limit) { //check conditions
        x++; //counter increment
        $('.input_fields_container').append('<div class="remove-pre"><input type="text" class="form-control" name="'+name+'" placeholder="Enter the plan features"/><a href="#" class="remove_field subs-btn"><i class="icon-gz_plus_icon"></i></a></div></div>'); //add input field
    }
});

var max_fields_limit = 5; //set limit for maximum input fields
var y = 0; //initialize counter for text box
$('.add_more_member').click(function (e) { //click event on add more fields button having class add_more_button
    e.preventDefault();
    var data = $('.adduser_item').html(); 
    var vl = ($("#view_only").is(':checked')) ? 1 : 0;
    //var brand_vl = ($("#switch_brand_access").is(':checked')) ? 1 : 0;
    
    
    if (y < max_fields_limit) { //check conditions
        y++;
        var updated_class = 'inner_team_'+ y;
        //$('.input_team_container .'+updated_class).find('.sel_brandcheck').chosen();
        
        $('.input_team_container').append('<div class="'+ updated_class +'">'+data+'<div class="remove-pre"><a href="#" class="remove_field subs-btn"><i class="icon-gz_plus_icon"></i></a></div></div>');//add input field
        $('.input_team_container .'+updated_class).find('.switch_brand_access').attr('id',"switch_brand_access_"+y);
        $('.input_team_container .'+updated_class).find('.switch_brand_access').next('label').attr('for',"switch_brand_access_"+y);
        $('.input_team_container .'+updated_class).find('.sel_brandcheck').attr('name','brandids['+y+'][]');
        
        $('.input_team_container .' + updated_class).find('#switch_brand_access_' + y).prop('checked', true);
        $('.input_team_container .' + updated_class).find('.sel_brandshowing').hide();
        $('.input_team_container .' + updated_class).find('.permission_list').hide();
        
        
        $('.input_team_container .'+updated_class).find('.sel_brandcheck').trigger("chosen:updated");
        $('.input_team_container .'+updated_class +' .user_prms_switch').each(function() {
            var id = $(this).attr('id');
            $('.input_team_container .'+updated_class).find('#'+id).next('label').attr('for',id+"_"+y);
            $('.input_team_container .'+updated_class).find('#'+id).attr('id',id+"_"+y);
//            if(vl == 1){
              $('.input_team_container .'+updated_class).find('#view_only_'+y).prop('checked',true);
//            }else{
//              $('.input_team_container .'+updated_class).find('#view_only_'+y).prop('checked',false); 
//            }
            
        });
        $('.input_team_container .'+updated_class).find(".validation_err").html("");
        $('[data-toggle="tooltip"]').tooltip();
    }
});

$(document).on("click", ".input_team_container .remove-pre", function (e) { //user click on remove text links
    e.preventDefault();
    jQuery(this).parent('div').remove();
    x--;
});

//$(document).on('change', '.password_ques', function () {
//    var ischecked = ($(this).is(':checked')) ? 1 : 0;
//    console.log("ischecked",ischecked);
//    if(ischecked === 0){
//        $(this).parents().next('.create_password').css("display","block");
//        $(this).parents().next('.create_password').find('.password').prop("required",true);
//        $(this).parents().next('.create_password').find('.password').attr("disabled",false)
//    }else{
//        $(this).parents().next('.create_password').css("display","none");
//        $(this).parents().next('.create_password').find('.password').prop("required",false);
//        $(this).parents().next('.create_password').find('.password').attr("disabled",true);
//    }
//});

//$(document).on('click', '.member_role', function () {
//    $this = $(this); 
//    var role = $(this).val();
//    if(role == "customer"){
//        $this.parents().next('.users_permission').hide();
//        $.ajax({
//            type: 'POST',
//            url: baseUrl + "customer/AgencyClients/getclientsubscription",
//            dataType: "json",
//            data: {subscription: 1},
//            success: function (data) {
//                if(data != ""){
//                    var select_html = ""
//                    if (data.length > 1){
//                        $.each(data, function(k,v){
//                            if(v.payment_mode == 0){
//                                var mode = "(Offline)";
//                            }else{
//                                var mode = "(Online)";
//                            }
//                          select_html +=  '<option value="'+v.plan_id+'" data-paymentmode="'+v.payment_mode+'" data-inprogress="'+v.global_inprogress_request+'" data-plan_type="'+v.plan_type_name+'" id="option_'+v.plan_id+'" data-shared="'+v.shared_user_type+'" data-payment="'+v.payment_mode+'">'+v.plan_name+mode+'</option>';
//                        });
//                    }else{
//                        var v = data;
//                        if (v.payment_mode == 0) {
//                            var mode = "(Offline)";
//                        } else {
//                            var mode = "(Online)";
//                        }
//                        select_html +=  '<option value="'+v.plan_id+'" data-paymentmode="'+v.payment_mode+'" data-inprogress="'+v.global_inprogress_request+'" data-plan_type="'+v.plan_type_name+'" id="option_'+v.plan_id+'" data-shared="'+v.shared_user_type+'" data-payment="'+v.payment_mode+'">'+v.plan_name+mode+'</option>';
//                    } 
//                    $this.parents(".adduser_membrr").find(".clintsubscription").html(select_html);
//                    $this.parents(".adduser_membrr").find(".client_subscription").show();
//            }else{
//                $this.parents(".adduser_membrr").find(".client_subscription").hide();
//            }
//        }
//        });
//    }else if(role == "designer"){
//        $this.parents().next('.users_permission').hide();
//        $this.parents(".adduser_membrr").find(".client_subscription").hide();
//    }else{
//        $this.parents().next('.users_permission').show();
//        $this.parents(".adduser_membrr").find(".client_subscription").hide();
//    }
//});

$(document).on('change', '.view_membr_prmsn_only input[type="checkbox"]', function () {
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if(ischecked === 0){
        $(this).parents().next('.permission_list').css("display","block");
    }else{
        $(this).parents().next('.permission_list').css("display","none");
    }
});

$(document).on('blur', '.chk_usr_mail', function () {
    $this = $(this);
    var usr_mail  = $this.val();
    if(usr_mail != ""){
        $this.next(".validation_err").html("");
        $.ajax({
                type: 'POST',
                url: baseUrl + "account/DesignerManagement/EmailCheckerValidation",
                data: {email: usr_mail},
                success: function (data) {
                       if(data == "false"){
                         $this.next(".validation_err").html("Email Address is already available.!");
                         $(".invalid_email").val("1");
                         $(".save_next.save").prop("disabled",true);
                       }else{
                          $this.next(".validation_err").html("");
                          $(".save_next.save").prop("disabled",false);
                          $(".invalid_email").val("0");
                       }
                }
            });
    }
//    else{
//        $this.next(".validation_err").html("Please Enter Email Address!");
//    }
});

$(document).on('change', '.is_smtp_enable input[type="checkbox"]', function () {
        var ischecked = ($(this).is(':checked')) ? 1 : 0;
        if (ischecked) {
            $(".show-hidelabel em").hide();
            $(".port_settings").css('display', 'block');
            $(".port_settings input").prop("required", true);
        } else {
            $(".show-hidelabel em").show();
            $(".port_settings").css('display', 'none');
            $(".port_settings input").prop("required", false);
}
});
$(document).on('click', '.usr_connect_stripe', function () {
    // repeat with the interval of 2 seconds
   $('.loading_connect_procs').html('<span class="loading_process" style="text-align:center;"><img src="' + $assets_path + 'img/ajax-loader.gif" alt="loading"/ style="margin: auto;display: block;"></span>');
   timerId = setInterval(function() {
       check_usr_connect_with_stripe();
    }, 1000);
    setTimeout(function(){
         clearInterval(timerId);
         $('.loading_connect_procs').html("");
         window.close();
         console.log("close window");
    }, 2000);
});

function check_usr_connect_with_stripe(){
    $.ajax({
                type: 'POST',
                url: baseUrl + "customer/AgencyClients/connect_stripe",
                dataType: "json",
                success: function (data) {
                    console.log("response d",data);
                    console.log("please wait...");
                    if(data == 1){
                        $('.loading_connect_procs').html("Stripe connect successfully");
                        clearInterval(timerId);
                    }
                }
            });
}
$(document).on('keyup','.entr_compny_name', function(){
    var comny = $(this).val();
    if(comny == ""){
        comny = "Company Name";
    }
    $(".agecy_cmpny_name").html(comny);
});

 $(document).on('keyup','.password-val', function(){
     $this = $(this);
     var str = $this.val();
//     console.log("str",str);
     var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
        if(!re.test(str)){
            $this.next().next(".validation_err").html("at least one number, one lowercase and one uppercase letter");
        }else{
            $this.next().next(".validation_err").html("");
        }
 });
  
$(document).on('click','.change_pss_mode i', function(){
      var type = $(this).parent().prev('.password-val').attr("type");
      if(type == "password"){
          $(this).removeClass("fa fa-eye");
          $(this).addClass("fa fa-eye-slash");
          $(this).parent().prev('.password-val').attr("type","text");
      }else{
          $(this).removeClass("fa fa-eye-slash");
          $(this).addClass("fa fa-eye");
          $(this).parent().prev('.password-val').attr("type","password");
      }
});

$(document).on('click', ".dropdown dt a", function () {
    $(".dropdown dd ul").slideToggle('fast');
});

$(document).on('click',".dropdown dd ul li a", function () {
    $(".dropdown dd ul").hide();
});
$(document).on('click','.mutliSelect input[type="checkbox"]', function () {
       // console.log("click1");
    var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
            title = "," + $(this).val();

    if ($(this).is(':checked')) {
        var html = '<span title="' + title + '">' + title + '</span>';
        $('.multiSel').append(html);
        $(".hida").hide();
    } else {
        $('span[title="' + title + '"]').remove();
        var ret = $(".hida");
        $('.dropdown dt a').append(ret);

    }

});
$("#questionaries").submit(function (e) {
        var phone = $('#phone').val();
        $(".email-valid-text .validation").html("");
        if (phone.length < 10 ) {
            $(".email-valid-text .validation").html("<span>Phone number should be atleast 10 digits.</span>");
            return false;
        } else {
            $(".email-valid-text .validation").html("");
            return true;
        }
});
$(document).on('click','.clnt_typ', function(){
    var value = $(this).val();
    if(value == 1){
        $(".pay_mode_blk").show();
        $(".save_next.save").show();  // hide save button
        $("#skip_without_save").css("display","inline-block");  // hide skip button
    }else{
        var id = $("fieldset.active").data('chck');
        $(".pay_mode_blk").hide();
        $(".subscription_blk").hide();
        Subdomainwizard(id,'next'); 
        $('#skip_without_save').hide();
    }
});
$(document).on('click','.skip_without_save',function(){
    var id = $("fieldset.active").data('chck');
    if(id == 4 && is_agency != 1){ // move team member to final step and user have not permission for agency setting
        Subdomainwizard(6,'next',4);
        $(this).hide(); //hide skip from last step
    }else if(id == 5 && is_agency != 1){ // move team member to final step and user have not permission for agency setting
        Subdomainwizard(6,'next',5);
        $(this).hide(); //hide skip from last step
    }else if(id == 6 && is_agency == 1){ // move payment step to final step and user have permission for agency setting
        $(this).hide(); //hide skip from last step
        Subdomainwizard(id,'next');
    }else{
        Subdomainwizard(id,'next');
    }
    
    var client = $("input[name='client']:checked").val();
    if(id == 5 && client == 1 && is_agency == 1){
        $(".save_next.save").show();  // show save button
        $("#skip_without_save").css("display","inline-block");  // show skip button
    }
    if(id == 3){
        $(".save_next.save").prop("disabled",false);  // remove disable property
        $("#skip_without_save").css("display","inline-block");  // show skip button
    }
});

$('.custom-select').on('mouseover','.select-items',function(){
    $(this).find("div").removeClass("same-as-selected");
});

$(document).on('click','.want_create',function(){
    $(this).parent().hide();
    $(this).attr("data-flag",1);
    $(".welcome_brand_outline").fadeIn();
     $(".save_next.save").css("display","inline-block");
});
$("#exampleInputName").focusout(function(){
    var brand_n = $(this).val();
    var brand_len = brand_n.length;
    if(brand_len == 0){
        $(this).next(".error_field").html("please enter brand name");
    }else if(brand_len < 4){
        $(this).next(".error_field").html("please enter atleast 4 character");
    }else{
        $(this).next(".error_field").html("");
        $(".des_brand_sec").show();
    }
//    console.log("brand_len",brand_len);
//  console.log("focusout working",brand_n);
});

$("#comment").focusout(function(){
    var comment = $(this).val();
    var comment_len = comment.length;
    if(comment_len == 0){
        $(this).next(".error_field").html("please enter description");
    }else if(comment_len < 4){
        $(this).next(".error_field").html("please enter atleast 4 character");
    }else{
        $(this).next(".error_field").html("");
        $(".upload_for_brand").show();
        $(".save_next.save").prop("disabled",false);
    }
});

var k = 0;
$('.add_more_reference').click(function (e) { //click event on add more fields button having class add_more_button
    e.preventDefault();
    var name = $(this).parent().find(".f_cls_nm").attr('name');
    console.log("name", name);
    if (k < max_fields_limit) { //check conditions
        k++; //counter increment
        $('.reference_field').append('<div><div class="remove-pre"><input type="text" class="input refernce_lnk" name="' + name + '"/><div class="line-box"><div class="line"></div></div><a href="#" class="remove_field"><i class="icon-gz_plus_icon"></i></a></div></div></div>'); //add input field
    }
});

$(document).on("click", ".reference_field .remove_field", function (e) { //user click on remove text links
    e.preventDefault();
    console.log("add subs");
    jQuery(this).parent('div').remove();
    k--;
});

$(document.body).on('click','.des_business_detail .select-items div',function(){
    
    var text = $(this).text();
    if(text == "Other"){
        $(".other_business").fadeIn();
        $(".other_business input").prop("required",true);
    }else{
        $(".other_business").fadeOut();
        $(".other_business input").prop("required",false);
    } 
});

$(document.body).on('click','.company_describe .select-items div',function(){
    
    var text = $(this).text();
    if(text == "Other"){
        $(".other_company").fadeIn();
        $(".other_company input").prop("required",true);
    }else{
        $(".other_company").fadeOut();
        $(".other_company input").prop("required",false);
    } 
});
    
$(document).on('change', '.switch_brand_access', function () {
    $this = $(this);
        var ischecked = ($this.is(':checked')) ? 1 : 0;
        
        if (ischecked == 0) {
           // $('#sel_brandcheck').chosen();
           console.log($this.parents('.brand_selections'),"parents");
           console.log($this.parents('.brand_selections').children('.sel_brandshowing'),"childern");
           console.log($this.parents('.brand_selections').children('.sel_brandshowing').find('.sel_brandcheck'),"find next");
            $this.parents('.brand_selections').children('.sel_brandshowing').find('.sel_brandcheck').chosen();
            $.ajax({
                type: 'POST',
                url: baseUrl + "customer/BrandProfile/get_brandprofiles",
                dataType: "json",
                success: function (data) {
                    console.log(data, "data");
                    if (data != "") {
                        var select_html = "<option value=''>Select Brand</option>";
                        if (data.length > 1) {
                            $.each(data, function (k, v) {
                                select_html += '<option value="' + v.id + '">' + v.brand_name + '</option>';
                            });
                        } else {
                            var v = data;
                            select_html += '<option value="' + v.id + '">' + v.brand_name + '</option>';
                        }
                        $this.parents('.brand_selections').children('.sel_brandshowing').find('.sel_brandcheck').html(select_html);
                        $this.parents('.brand_selections').children('.sel_brandshowing').find('.sel_brandcheck').trigger("chosen:updated");
//                        $("#sel_brandcheck").html(select_html);
//                        $("#sel_brandcheck").trigger("chosen:updated");
                    }
                }
            });
            $this.parents('.brand_selections').find('.sel_brandshowing').show();
           // $('#sel_brandshowing').show();
        } else {
            $this.parents('.brand_selections').find('.sel_brandshowing').hide();
           // $('#sel_brandshowing').hide();
        }
});
