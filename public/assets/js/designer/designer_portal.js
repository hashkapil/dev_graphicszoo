$(document).ready(function () {
    
    if ($(window).width() <= '767') {
        console.log("test"); 
        $(document).on('click','.arrow_btn',function(){
            console.log("test on btn"); 
            $('.admin-sidebar').toggleClass('open');
        });
    $(document).click(function(e) {
        var classnm = $('.arrow_btn');
        if (!classnm.is(e.target) && classnm.has(e.target).length === 0){
          $(".admin-sidebar").removeClass("open");
        }
    }); 
    }
    
    $('.input').focus(function () {
        $(this).parent().find(".label-txt").addClass('label-active');
    });
    $(".input").focusout(function () {
        if ($(this).val() == '') {
            $(this).parent().find(".label-txt").removeClass('label-active');
        };
    });
    $('.input').each(function () {
        if ($(this).val() != '') {
            $(this).parent().find(".label-txt").addClass('label-active');
        } else {
            $(this).parent().find(".label-txt").removeClass('label-active');
        }
    });
});


if(($('.chattype').length) > 0){
$(".chattype").on('change', function (e) {
    var valueSelected = this.value;
    $(".send_request_chat").attr('data-customerwithornot', valueSelected);
    $("#shre_file").attr('data-withornot', valueSelected);
    if (valueSelected == 0) {
        $('#message_container_without_cus').show();
        $('#message_container').hide();
//        $('#message_container_without_cus').stop().animate({
//            scrollTop: $('#message_container_without_cus')[0].scrollHeight
//        }, 1);
    } else if (valueSelected == 1) {
        $('#message_container_without_cus').hide();
        $('#message_container').show();
//        $('#message_container').stop().animate({
//            scrollTop: $('#message_container')[0].scrollHeight
//        }, 1);
    }
});
}

$(document).on('click', '.openchange', function (e) {
    var chatid = $(this).data('chatid');
    $(".open-edit_" + chatid).slideToggle()
});

$(document).on('click', '.editmsg', function () {
    var editid = $(this).data('editid');
    $('.msg_desc_' + editid).css('display', 'none');
    $('.open-edit_' + editid).css('display', 'none');
    $('.edit_main_' + editid).css('display', 'block');
});
$(document).on('click', '.edit_save', function () {
    var editid = $(this).data('id');
    var msg = $("#edit_main_msg_" + editid).val();
    $.ajax({
        type: 'POST',
        url: baseUrl + "admin/Request/Editmsg",
        data: { editid: editid, msg: msg },
        success: function (data) {
            if (data) {
                $('.edit_icon_' + editid).html('<i class="fas fa-pen"></i>');
                $('.took_' + editid).html('<div class="edited_msg">' + msg + '</div>');
                $('.msg_desc_' + editid).css('display', 'block');
                $('.edit_main_' + editid).css('display', 'none');
            }
        }
    });
});
$(document).on('click', '.cancel_main', function () {
    var msgid = $(this).data('msgid');
    $('.edit_main_' + msgid).css('display', 'none');
    $('.msg_desc_' + msgid).css('display', 'block');
});
$(document).on('click', '.deletemsg', function () {
    var r = confirm('Are you sure you want to delete this message?');
    if (r == true) {
        var delid = $(this).data('delid');
        $.ajax({
            type: 'POST',
            url: baseUrl + "admin/Request/Deletemsg",
            data: { delid: delid },
            success: function (data) {
                if (data) {
                    //console.log(delid);
                    $('.editdeletetoggle_' + delid).css('display', 'none');
                    $('.took_' + delid).html('<div class="edited_msg deleted_msg"><i class="fa fa-ban" aria-hidden="true"></i> You have deleted this message.</div>');
                }
            }
        });
    }
});

$(document).on('click','.clearall_msges',function(){
    var all_msg = $(this).data('allmsges');
    var vals=[];
    for(var i=0;i<all_msg.length;i++){
       vals.push(all_msg[i].id);
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + "designer/Request/clearAllmessages",
        data: { delid: vals },
        success: function (data) {
            if (data) {
                
            }
        }
    });
});
/******file sharing in chat******/
$(document).on('click', '.attchmnt', function () {
    $("input[id='shre_file']").click();
});

$('#file_input').change(function () {
        var type = $(this).attr('data-type');
        if(type == 'sample'){
          type = 'sample';  
        }else{
            type = '';
        }
        var storedFiles = [];
        var filesCount = $(this)[0].files.length;
        var $textContainer = $(this).prev();
        var imgpath =  $assets_path+'img/ajax-loader.gif';
        var $fileListContainer = $(".uploadFileListContainer");
        filesCount = storedFiles.length;
        if (filesCount === 1) {
            var fileName = $(this).val().split('\\').pop();
        } else if (filesCount >= 1) { }
        $(".uploadFileListContainer").addClass("asdjgasd"); 
        $(".uploadFileListContainer").append('<div class="ajax-img-loader"><img src='+imgpath+' height="150"/></div>');
        var fileInput = $('#file_input')[0];
        if (fileInput.files.length > 0) {
            var formData = new FormData();
            $.each(fileInput.files, function (k, file) {
                var fileName = file.name;
                var type = fileName.split('.').pop();
                var name = fileName.split('.');
                var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
                var newFileName = name_file + '.' + type;
                formData.append('file_upload[]', file, newFileName);
            });
            $.ajax({
                method: 'post',
                url: baseUrl + "admin/dashboard/attach_file_process/"+type,
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status == '1') {
                        if (response.files.length > 0) {
                            $.each(response.files, function (k, v) {
                                storedFiles.push(v[0]);
                                $fileListContainer.html("");
                                console.log("storedFiles",storedFiles); 
                                $.each(storedFiles, function (key, value) {
                                    if (value.error == false) {
                                        $('.ajax-img-loader').css('display', 'none');
                                        $fileListContainer.append('<div  class="uploadFileRow " id="file' + key + '"><div class="col-md-6"><div class="extnsn-lst">\n\
                                        <p class="text-mb">' + value.file_name + '<strong> (' + formatFileSize(value.file_size, 2) + ') </strong></p>\n\
                                        <p class="cross-btnlink">\n\
                                        <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
                                        <span>x</span>\n\
                                        </a>\n\
                                        <input type="hidden" value="' + value.file_name + '" name="delete_FL[]" class="delete_file"/><input type="hidden" value="" name="upload_path" class="upload_path" />\n\
                                        \n\
                                        \n\
                                        \n\
                                        </p>\n\
                                        </div></div></div>');
                                    } else {
                                        $fileListContainer.append('<div  class="uploadFileRow" id="file' + key + '"><div class="col-md-6" ><div class="extnsn-lst error-list">\n\
                                    <p class="text-mb">' + value.file_name + '<br/><strong>' + value.error_msg + '</strong></p><p class="cross-btnlink">\n\
                                    <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
                                    <span>x</span>\n\
                                    </a>\n\
                                    \n\
                                    \n\
                                    \n\
                                    \n\
                                    </p>\n\
                                    </div></div></div>');
                                    }
                                });
                            });
                        }
                    } else {
                    }
                }
            });
        } else {
            //console.log('No Files Selected');
        }
    });

function formatFileSize(bytes, decimalPoint) {
    if (bytes == 0)
        return '0 Bytes';
    var k = 1000,
        dm = decimalPoint || 2,
        //sizes = ['Bytes','KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
         sizes = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}