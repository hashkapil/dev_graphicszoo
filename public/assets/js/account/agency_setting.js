/************Wizard subdomain form****************/
$(document).on('click','.back',function(){
    var id = $("fieldset.active").data('chck');
    Subdomainwizard(id,'back');
});

$(document).on('change', '.pmt_check_item input[name="payment_mode"]', function () {
    var value = $(this).val();
//    console.log("value",value);
    if(value == 1){
        $(".strp_acnt").css("display","block");
        $(".subs_text em").html("Complete the form below to add new subscription. This will be an offline payment subscription, once you will connect with stripe then you can add online payment subscriptions as well.")
    }else{
        $(".strp_acnt").css("display","none");
        $(".subs_text em").html("Complete the form below to add new subscription.")
    }
    
});

$(document).on('submit','#subdomainwiz',function(e){
    e.preventDefault();
    
    var fid = $("fieldset.active").attr('id');
    var formData = $("#"+fid).serialize();
    var id = $("fieldset.active").data('chck');
    var invalid_email = $(".invalid_email").val();
    if(id == 3){
        var client = $("#"+fid+" input[name='client']:checked").val();
        if(client == undefined ||client == ""){
            $("#"+id+"_field .val_err").addClass("alert-danger");
            $("#"+id+"_field .val_err").html("<p>Please select atleast one</p>");
            $('html, body').animate({
                scrollTop: $("#"+id+"_field .val_err").offset().top - 100
            }, 'slow');
            return false;
        }else if(client == 1){
            $("#"+id+"_field .val_err").removeClass("alert-danger");
            $("#"+id+"_field .val_err").html("");
            Subdomainwizard(id,'next'); 
        }else if(client == 2){
            $("#"+id+"_field .val_err").removeClass("alert-danger");
            $("#"+id+"_field .val_err").html("");
            Subdomainwizard(5,'next',3); 
            if(invalid_email == "1"){
               $(".save_next.save").prop("disabled",true);
            }
        }
    }else{
        if(id == 5 && invalid_email == "1"){
           $(".save_next.save").prop("disabled",true);
        }
        SavesubdomainWizard(formData,id);
    }
});

function Subdomainwizard(n,type,nxtid){
    var is_agency = $('#subdomainwiz').data('isagency');
    var primry_colr = $("#d_primary_color").val();
   if(type == 'next'){
        var field_id = parseInt(n)+1;
        if(is_agency == '0'){
           $('#2_field').css('display','none');  
        }
        if(n == 8){
            $('.save').css('display','none');
        }else{
            $('.save').css('display','inline-block');
        }
        if(n){
             $('.back').css('display','inline-block');  
        }
    }else{
       if(n == 8 || n == 9){ //show save button when click on from last two steps
            $('.save').css('display','inline-block');
       }
       if(n == 6){ //back button from add team member steps
            var client = $("input[name='client']:checked").val();
            if(client == '2'){
                var field_id = '3';
            }else{
                var field_id = parseInt(n)-1;
            }
       }else{
            var field_id = parseInt(n)-1;
       }
       if(n == 3 && is_agency != 1){
           var field_id = 1;
       }
       if(n == '2'){   //hide back button from first step
         $('.back').css('display','none');  
         $('.save').css('display','inline-block');
         $('.finish').css('display','none');
       }
        if(n == 7){ //hide email setting that show required field on click back button
            $(".port_settings").css('display', 'none');
            $(".port_settings input").prop("required", false);
            $(".is_smtp_enable input").prop("check", false);
        }
        
        $(".save_next.save").prop("disabled",false);
      
    }
  
   
   if(nxtid != ""){
       $('#'+nxtid+'_field').css('display','none'); //for client section to hide
       $('#'+nxtid+'_field').removeClass('active');
       $('#'+nxtid+'_bar').removeClass('active');
       $('#'+nxtid+'_bar').removeClass('cstm_primary_colr');
   }
    $('#'+field_id+'_field').css('display','block');
    $('#'+field_id+'_field').addClass('active');
    $('#'+n+'_field').css('display','none');
    $('#'+n+'_field').removeClass('active');
    $('#'+n+'_bar').removeClass('active');
    $('#'+n+'_bar').removeClass('cstm_primary_colr');
    $('#'+field_id+'_bar').addClass('active');
    $('#'+field_id+'_bar').addClass('cstm_primary_colr'); 
    $('#'+n+'_bar').css("background","");
    $('.cstm_primary_colr').css('background', '' + primry_colr + '');
}

function SavesubdomainWizard(formData,id){
    var file = $('#onboardingpic').get(0).files[0];
    var brand_logo = $('#brand_logo').get(0).files[0];
    var formdata = new FormData();
    formdata.append('profile', file);
    formdata.append('formData', formData);
    formdata.append('brand_logo', brand_logo);
    $.ajax({
        type: 'POST',
        url: baseUrl + "account/AgencyUserSetting/SaveonboardingDetail",
        dataType: "json",
        data: formdata,
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status == "error" || data.status == 0){
                if(data.role && data.role != ""){
                     if(data.key == 0){
                         var animate_cls = ".adduser_item";
                         $("#"+id+"_field .adduser_item .not_allow").addClass("alert-danger");
                         $("#"+id+"_field .adduser_item .not_allow").html("<p>"+data.msg+"</p>");
                         $("#"+id+"_field .inner_team_"+data.key+" .not_allow").html("");
                         $("#"+id+"_field .inner_team_"+data.key+" .not_allow").removeClass("alert-danger");
                     }else{
                         var animate_cls = ".inner_team_"+data.key;
                         $("#"+id+"_field .inner_team_"+data.key+" .not_allow").addClass("alert-danger");
                         $("#"+id+"_field .inner_team_"+data.key+" .not_allow").html("<p>"+data.msg+"</p>");
                         $("#"+id+"_field .adduser_item .not_allow").html("");
                         $("#"+id+"_field .adduser_item .not_allow").removeClass("alert-danger");
                         
                     }
                     $('html, body').animate({
                            scrollTop: $("#"+id+"_field "+animate_cls+" .not_allow").offset().top - 100
                        }, 'slow');
                }else{
                    $("#"+id+"_field .not_allow").html("");
                    $("#"+id+"_field .not_allow").removeClass("alert-danger");
                    $("#"+id+"_field .val_err").addClass("alert-danger");
                    $("#"+id+"_field .val_err").html("<p>"+data.msg+"</p>");
                    $('html, body').animate({
                        scrollTop: $("#"+id+"_field .val_err").offset().top - 100
                    }, 'slow');
                }
            }else{
                $("#"+id+"_field .not_allow").html("");
                $("#"+id+"_field .not_allow").removeClass("alert-danger");
                if(data.id && data.id != ""){
                    $("#f_subs_id").val(data.id);
                }
                $("#"+id+"_field .val_err").removeClass("alert-danger");
                $("#"+id+"_field .val_err").html("");
                if(id == 1 && is_agency != 1){
                    Subdomainwizard(2,'next',1); 
                }else{
                   Subdomainwizard(id,'next');  
                }
                
            }
        }
   });
}

$(document).on('change', '#d_primary_color', function () {
    var primary_color = $(this).val();
    $('.cstm_primary_colr').css('background', '' + primary_color + '');
});

$(document).on('change', '#d_secondary_color', function () {
    var secondary_color = $(this).val();
    $('.cstm_sec_btn').css('background', '' + secondary_color + '');
});
$(document).on('keyup', '.domain_url', function () {
    var ischecked = ($("#slct_domain_subdomain").is(':checked')) ? 1 : 0;
    var domain = $(this).val();
    if(ischecked == 1){
        $(".url_txt").html("http://"+domain);
    }else{
       $(".url_txt").html("https://"+domain+".graphicszoo.com"); 
    }
});

$(document).on('change','#slct_domain_subdomain',function(){
     var ischecked = ($(this).is(':checked')) ? 1 : 0;
     if(ischecked == 1){
         $('#setmaindomain_msg').modal('show');
         $('.save_next').prop('disabled',true);
         $('.confirmed_arecord').val(0);
     }else{
       $('.save_next').prop('disabled',false);  
       $('.subdomain_nm').css('display','flex');
       $('.domain_nm').css('display','none');
     }
});

$(document).on('click','.arec_cnfrm_btn',function(){
    $('.confirmed_arecord').val(1); 
    $('.subdomain_nm').css('display','none');
    $('.domain_nm').css('display','flex');
     $('.save_next').prop('disabled',false);
});

$(document).on('click','.arec_ntcnfrm_btn',function(){
   $("#slct_domain_subdomain").prop('checked', false);
   $('.subdomain_nm').css('display','flex');
   $('.domain_nm').css('display','none');
   $('.save_next').prop('disabled',false);  
   
});

$('.add_more_subscription').click(function (e) { //click event on add more fields button having class add_more_button
    e.preventDefault();
    x = 1;
    var name = $(this).parent().find(".plan_features").attr('name');
    if (x < max_fields_limit) { //check conditions
        x++; //counter increment
        $('.input_fields_container').append('<div class="remove-pre"><input type="text" class="form-control" name="'+name+'" placeholder="Enter the plan features"/><a href="#" class="remove_field subs-btn"><i class="icon-gz_plus_icon"></i></a></div></div>'); //add input field
    }
});

var max_fields_limit = 5; //set limit for maximum input fields
var y = 0; //initialize counter for text box
$('.add_more_member').click(function (e) { //click event on add more fields button having class add_more_button
    e.preventDefault();
    var data = $('.adduser_item').html();
    if (y < max_fields_limit) { //check conditions
        y++;
        var updated_class = 'inner_team_'+ y;
        $('.input_team_container').append('<div class="'+ updated_class +'">'+data+'<div class="remove-pre"><a href="#" class="remove_field subs-btn"><i class="icon-gz_plus_icon"></i></a></div></div>');//add input field
        $('.input_team_container .'+updated_class +' .user_prms_switch').each(function() {
            var id = $(this).attr('id');
            $('.input_team_container .'+updated_class).find('#'+id).next('label').attr('for',id+"_"+y);
            $('.input_team_container .'+updated_class).find('#'+id).attr('id',id+"_"+y);
            
        });
        $('.input_team_container .'+updated_class).find(".validation_err").html("");
    }
});

$('.input_team_container').on("click", ".remove-pre", function (e) { //user click on remove text links
    e.preventDefault();
    jQuery(this).parent('div').remove();
    x--;
});

$(document).on('change', '.password_ques', function () {
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    console.log("ischecked",ischecked);
    if(ischecked === 0){
        $(this).parents().next('.create_password').css("display","block");
        $(this).parents().next('.create_password').find('.password').prop("required",true);
        $(this).parents().next('.create_password').find('.password').attr("disabled",false)
    }else{
        $(this).parents().next('.create_password').css("display","none");
        $(this).parents().next('.create_password').find('.password').prop("required",false);
        $(this).parents().next('.create_password').find('.password').attr("disabled",true);
    }
});

$(document).on('click', '.member_role', function () {
    $this = $(this); 
    var role = $(this).val();
    if(role == "customer"){
        $this.parents().next('.users_permission').hide();
        $.ajax({
            type: 'POST',
            url: baseUrl + "account/AgencyClients/getclientsubscription",
            dataType: "json",
            data: {subscription: 1},
            success: function (data) {
                if(data != ""){
                    var select_html = ""
                    if (data.length > 1){
                        $.each(data, function(k,v){
                            if(v.payment_mode == 0){
                                var mode = "(Offline)";
                            }else{
                                var mode = "(Online)";
                            }
                          select_html +=  '<option value="'+v.plan_id+'" data-paymentmode="'+v.payment_mode+'" data-inprogress="'+v.global_inprogress_request+'" data-plan_type="'+v.plan_type_name+'" id="option_'+v.plan_id+'" data-shared="'+v.shared_user_type+'" data-payment="'+v.payment_mode+'">'+v.plan_name+mode+'</option>';
                        });
                    }else{
                        var v = data;
                        if (v.payment_mode == 0) {
                            var mode = "(Offline)";
                        } else {
                            var mode = "(Online)";
                        }
                        select_html +=  '<option value="'+v.plan_id+'" data-paymentmode="'+v.payment_mode+'" data-inprogress="'+v.global_inprogress_request+'" data-plan_type="'+v.plan_type_name+'" id="option_'+v.plan_id+'" data-shared="'+v.shared_user_type+'" data-payment="'+v.payment_mode+'">'+v.plan_name+mode+'</option>';
                    } 
                    $this.parents(".adduser_membrr").find(".clintsubscription").html(select_html);
                    $this.parents(".adduser_membrr").find(".client_subscription").show();
            }else{
                $this.parents(".adduser_membrr").find(".client_subscription").hide();
            }
        }
        });
    }else if(role == "designer"){
        $this.parents().next('.users_permission').hide();
        $this.parents(".adduser_membrr").find(".client_subscription").hide();
    }else{
        $this.parents().next('.users_permission').show();
        $this.parents(".adduser_membrr").find(".client_subscription").hide();
    }
});

$(document).on('change', '.view_membr_prmsn_only input[type="checkbox"]', function () {
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if(ischecked === 0){
        $(this).parents().next('.permission_list').css("display","block");
    }else{
        $(this).parents().next('.permission_list').css("display","none");
    }
});

$(document).on('blur', '.chk_usr_mail', function () {
    $this = $(this);
    var usr_mail  = $this.val();
    if(usr_mail != ""){
        $this.next(".validation_err").html("");
        $.ajax({
                type: 'POST',
                url: baseUrl + "account/DesignerManagement/EmailCheckerValidation",
                data: {email: usr_mail},
                success: function (data) {
                       if(data == "false"){
                         $this.next(".validation_err").html("Email Address is already available.!");
                         $(".invalid_email").val("1");
                         $(".save_next.save").prop("disabled",true);
                       }else{
                          $this.next(".validation_err").html("");
                          $(".save_next.save").prop("disabled",false);
                          $(".invalid_email").val("0");
                       }
                }
            });
    }
//    else{
//        $this.next(".validation_err").html("Please Enter Email Address!");
//    }
});

$(document).on('change', '.is_smtp_enable input[type="checkbox"]', function () {
        var ischecked = ($(this).is(':checked')) ? 1 : 0;
        if (ischecked) {
            $(".show-hidelabel em").hide();
            $(".port_settings").css('display', 'block');
            $(".port_settings input").prop("required", true);
        } else {
            $(".show-hidelabel em").show();
            $(".port_settings").css('display', 'none');
            $(".port_settings input").prop("required", false);
}
});
$(document).on('click', '.usr_connect_stripe', function () {
    var id = $("fieldset.active").data('chck');
    Subdomainwizard(id,'next'); 
});

$(document).on('keyup','.entr_compny_name', function(){
    var comny = $(this).val();
    if(comny == ""){
        comny = "Company Name";
    }
    $(".agecy_cmpny_name").html(comny);
});

 $(document).on('keyup','.password-val', function(){
     $this = $(this);
     var str = $this.val();
//     console.log("str",str);
     var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
        if(!re.test(str)){
            $this.next().next(".validation_err").html("at least one number, one lowercase and one uppercase letter");
        }else{
            $this.next().next(".validation_err").html("");
        }
 });
  
$(document).on('click','.change_pss_mode i', function(){
      var type = $(this).parent().prev('.password-val').attr("type");
      if(type == "password"){
          $(this).removeClass("fa fa-eye");
          $(this).addClass("fa fa-eye-slash");
          $(this).parent().prev('.password-val').attr("type","text");
      }else{
          $(this).removeClass("fa fa-eye-slash");
          $(this).addClass("fa fa-eye");
          $(this).parent().prev('.password-val').attr("type","password");
      }
});