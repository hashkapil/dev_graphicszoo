<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/front_end/Updated_Design/owl/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/front_end/Updated_Design/owl/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/front_end/Updated_Design/css/jquery.fancybox.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/front_end/Updated_Design/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/front_end/Updated_Design/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/front_end/Updated_Design/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/front_end/Updated_Design/css/custom.css">
    <script src="<?php echo base_url(); ?>public/front_end/Updated_Design/owl/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>public/front_end/Updated_Design/owl/owl.carousel.js"></script>
    <title>Graphics Zoo | Register a New Account</title>
  </head>
  
  <body>
    <header  class="form-header">
        <div class="container">
    <div class="row">
          <div class="col-3"><div id="logo" class="sign-logo">
            <!-- <h1><a href="#body" class="scrollto"><span>e</span>Startup</a></h1> -->
            <!-- Uncomment below if you prefer to use an image logo -->
             <a href="<?php echo base_url(); ?>"><img src="https://graphicszoo.com/public/front_end/Updated_Design/img/logo.png" alt="Graphics Zoo" title="Graphics Zoo" /></a>
          </div>
        </div>
        <div class="col-9">
        <div class="right-number">
         <div class="number"> <span><i class="fas fa-phone"></i></span><a href="tel:(800) 225-6674">(800) 225-6674</a></div>
          <a href="<?php echo base_url(); ?>login">Sign In</a>
        </div>
        </div></div></div>
      </header>
 <!-- #header -->
<section class="sing-up-heading ">
   <div class="container">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2 class="sect-heading">Sign up</h2>
               <p>14 Day Risk Free Trial</p>
               
           </div>
       </div>
       <div class="row sign-here">
        <div class="col-md-12 text-center">
          <div class="both-outer">
               <?php if($this->session->flashdata('message_error') != '') {?>
                   <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                    </div>
                   <?php }?>
                   <?php if($this->session->flashdata('message_success') != '') {?>
                   <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong><?php echo $this->session->flashdata('message_success');?></strong>
                    </div>
                   <?php }?>
            <div class="parsonial-plan">
           
              <div class="par-out">
                    <h2>Subscription Plan</h2>
               <div class="select-bx-p">
                    <div class="custom-select" >
                        <select  id="priceselector">
                        <?php 
                                            for($i=0;$i<sizeof($planlist);$i++){
                                                    $priceexplode = explode('-',$planlist[$i]['name']);
                                                    $Netprice=  explode('/',$priceexplode[0]);
                                                    $timeperiod = "";
                                                if(str_replace(' ', '', $Netprice[0])=="AnnualGoldenPlan"){
                                                    $timeperiod= "Annual";
                                                }elseif(str_replace(' ', '', $Netprice[0])=="MonthlyGoldenPlan"){
                                                    $timeperiod= "Monthly";
                                                }
                                                echo '<option value="'.$planlist[$i]['id'].'_'.$timeperiod.'">'.$planlist[$i]['name'].'</option>';
                                                        } ?>
                        </select>
                        </div>
                     </div>
                
<!--                    <option value="item1">Monthly Golden Plan</option>
                    <option value="item2">Annual Golden Plan</option></select>-->
            <!-- <div class="icon-down-p"><i class="fas fa-angle-down"></i></div> -->
              <div class="monthly-activate">
              <ul>
                    <li>1-Day Turnaround</li>
                    <li>14 Day Risk Free Guarantee</li>
                    <li>No Contract</li>
                    <li>Unlimited Design Requests</li>         
                 </ul>
                 <ul>
                    <li>Unlimited Revisions</li>
                    <li> Quality Designers</li>
                    <li> Dedicated Support</li>
                    <li> Unlimited free stock  images from Pixabay and Unsplash</li>
                 </ul>
                </div>
               
              <p class="plan-price">Plan Price: <span id="" class="box item1">$299/Mo</span></p>

               <?php //  for($i=0;$i<sizeof($planlist);$i++){ 
//                   $priceexplode = explode('-',$planlist[$i]['name']);
//                   $Netprice=  explode('/',$priceexplode[0]);
//                 //  echo trim($Netprice[0]);
//                  if(str_replace(' ', '', $Netprice[0])=="AnnualGoldenPlan"){
//                      echo "Annual";
//                  }elseif(str_replace(' ', '', $Netprice[0])=="MonthlyGoldenPlan"){
//                      echo "monthly";
//                  }
                  // echo"<pre>";print_R($Netprice);echo"</pre>";
                    //echo"<pre>";print_R($priceexplode);echo"</pre>";
                   ?>
<!--               <span class="box item<?php //echo $i ?>" id=""> <?php //echo $Netprice[0];?></span> -->
                <?php //} ?>
               </p>
              </div>
            </div>
            <div class="bill-infoo">
                <div class="pb-info">
                    <h3>Personal & Billing Information</h3>
                   <form action="" method="post" role="form" class="contactForm">
                       <input type="hidden" value=""  name="plan_name" id="annual_price">
                      <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group">
                                  <div class="input-group"> <span class="input-group-addon"><span><i class="fas fa-user"></i></span></span>
                                      <input type="text"  name="first_name"  id="fname" class="form-control" data-rule="minlen:4" data-msg="Please enter First Name" required="" placeholder="First Name">
                                  </div>
                                  <div class="validation"></div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input-group"> <span class="input-group-addon"><span><i class="fas fa-user"></i></span></span>
                                    <input type="text" name="last_name" class="form-control" id="lname" placeholder="Last Name"  data-msg="Please enter Last Name" required="">
                                </div>
                                   <div class="validation"></div>
                            </div>
                         </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                  <div class="input-group"> <span class="input-group-addon"><span><i class="fas fa-envelope"></i></span></span>
                                      <input type="email"   name="email" class="form-control" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" required="">
                                  </div>
                                 <div class="validation"></div>
                              </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group"> <span class="input-group-addon"><span><i class="fas fa-key"></i></span></span>
                                    <input type="password" name="password" class="form-control" placeholder="Password" data-rule="minlen:4" data-msg="Please enter the password" required="" >
                                </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <div class="input-group"> <span class="input-group-addon"><span><i class="fas fa-credit-card"></i></span></span>
                                      <input type="text" class="form-control" name="card_number" placeholder="Card Number" required="" maxlength="16">
                                  </div>
                              </div>
                            </div>
                         <div class="col-md-12">
                           <div class="row">
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group"> <span class="input-group-addon"><i class="fas fa-calendar"></i></span></span>
                                        <input type="text" name="expir_date" class="form-control" placeholder="MM  /  YY" onfocusout="ValidDate(this.value);" onkeyup="dateFormat(this.value);" required="">
                                    </div>
                                </div>
                             </div>
                             <div class="col-sm-6 cvv">
                                <div class="form-group">
                                    <div class="input-group"> <span class="input-group-addon"><i class="fas fa-credit-card"></i></span></span>
                                        <input type="text" name="cvc" class="form-control" placeholder="CVV" maxlength="3" required="">
                                    </div>
                                </div>
                              </div>
                           </div>
                       </div>
                       <div class="col-md-12">
                          <div class="form-group">
                           <p class="terms-txt"> <input type="checkbox" name="term" value="1" class="form-control" required="">
                            I accept the <a href="<?php echo base_url()?>termsandconditions">terms and conditions</a></p>
                          </div>
                         
                      </div>


                       <div class="col-md-6">
                          <div class="form-group">
                              <div class="input-group"> <span class="input-group-addon disc"><span>%</span></span>
                                  <input type="text" class="form-control" name="Discount" placeholder="Discount Code" data-rule="email" data-msg="Please enter a valid email">
                              </div>
                          </div>
                          <hr>
                        </div>

                     

                      </div>
                   <input type="submit" name="submit" id="submit" value="Finish and Pay" class="btn ">


                   <div class="card-sec row">
                     <div class="col-lg-12  col-12 text-center">
                      <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/stripe-lock.png" class="img-fluid stripe-lock">
                     </div>
                     <!-- <div class="col-lg-5 col-md-5 col-sm-5 offset-sm-2 offset-md-2  offset-lg-2 col-7">
                       <p>Cards accepted:</p>
                       <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/payment-systm.png" class="img-fluid">
                      </div> -->
                   </div>
                 </form>
                 </div>
            </div>
          </div>
          </div>
          </div>
          </div>
   </div>
</section>



<!--------------Footer----------------->
<footer class="form-footer">
        <div class="container">
          <div class="row">
    
            
            <div class="col-md-12 ">
              <div class="footer-logo">
                <ul class="footer5">
                  <li>Connect</li>
                  <li><a target="_blank" href="https://www.facebook.com/graphicszoo1/"><i class="fab fa-facebook-square"></i></a></li>
                   <li><a target="_blank" href="https://www.instagram.com/graphics_zoo/"><i class="fab fa-instagram"></i></a></li>
                </ul>
    
              </div>
            </div>
    
          </div>
        </div>
    
      </footer>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

 <script src="<?php echo base_url(); ?>public/front_end/Updated_Design/js/bootstrap.min.js" ></script>
 <script src="<?php echo base_url(); ?>public/front_end/Updated_Design/js/jquery.fancybox.js"></script>

 
 
 
    <script>
            var tabChange = function(){
                var tabs = $('.nav-tabs > li')
                var active = tabs.find('a.active');
                var next = active.parent().next('li').length? active.parent().next('li').find('a') : tabs.filter(':first-child').find('a');
                    next.tab('show');
             }
             // Tab Cycle function
             var tabCycle = setInterval(tabChange, 3000)
             // Tab click event handler
             $(function(){
                 $('.nav-tabs a').click(function(e) {
                     e.preventDefault();
                     // Parar o loop
                     // mosta o tab clicado, default bootstrap
                     $(this).tab('show')
                     clearInterval(tabCycle);
                     tabCycle = setInterval(tabChange, 3000);
                    //  tabCycle1 = setInterval(tabChange, 5000)
                     // Inicia o ciclo outra vez
                    //  setTimeout(function(){
                    //     tabChange();
                    //     //  tabCycle = setInterval(tabChange, 6000)//quando recomeça assume este timing
                    //  }, 500);
                 });
             });
           </script>
<script type="text/javascript">
  jQuery(document).ready(function ($) {
    $(".fancybox")
    .attr('rel', 'gallery')
    .fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        nextEffect  : 'none',
        prevEffect  : 'none',
        padding     : 0,
        margin      : [20, 60, 20, 60] // Increase left/right margin
    });
  });
   jQuery(document).ready(function ($) {
  var selectedText =   $('.select-selected').text();
   // console.log(selectedText);
       //var setSelected=jQuery("#priceselector").val();
       Setselectedvalue(selectedText);
   });
   jQuery('#priceselector').on('change', function () {
//       jQuery('.item1').text('');
       var getSelectedPrice = this.value;
       Setselectedvalue(getSelectedPrice);
      
     
   });
  
  function Setselectedvalue(selectedval){
       var ActualPirce= jQuery(selectedval.split('_'));
       jQuery('#annual_price').val(ActualPirce[0]);
       console.log(ActualPirce[1]);
      if(ActualPirce[1]=="Annual"){
          jQuery('.Annualplan').css('display','block');
          jQuery('.monthliy_plan').css('display','none');
         jQuery('.item1').text('$239/MON');
      }
      if(ActualPirce[1]=="Monthly"){
          jQuery('.Annualplan').css('display','none');
          jQuery('.monthliy_plan').css('display','block');
          jQuery('.item1').text('$299/MON');
      }
  }
  
  
</script>
    <script>
            $(document).ready(function(){
                $("button.menu-toggle").click(function(){
                    $(".mobile-class").slideToggle();
                });
            });
            </script>
<script type="text/javascript">
//    $(document).ready(function(){
//        $("select").change(function(){
//            $(this).find("option:selected").each(function(){
//                var optionValue = $(this).attr("value");
//                if(optionValue){
//                    $(".box").not("." + optionValue).hide();
//                    $("." + optionValue).show();
//                } else{
//                    $(".box").hide();
//                }
//            });
//        }).change();
//    });
    </script>
    <script>
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
</script>


</body>
</html>