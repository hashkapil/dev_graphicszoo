<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class S_link_sharing extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('account/S_myfunctions');
        $this->load->helper('form');
        $this->load->helper('url');
         $this->load->model('Affiliated_model');
        $this->load->model('Welcome_model');
        $this->load->model('Clients_model');
        $this->load->model('account/S_admin_model');
        $this->load->model('Category_model');
        $this->load->model('account/S_request_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $this->load->helper(array('meta'));
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
    }
    
    public function s_project_info($sharekey = NULL){
        $sharekey = isset($sharekey)? $sharekey : '';
//        echo $sharekey;exit;
        $permissiondata = $this->Welcome_model->getpermissionsbySherekey($sharekey);
//        echo "<pre/>";print_R($permissiondata);exit;
        if($permissiondata[0]['is_disabled'] == 0){
           $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name']!= '')?$permissiondata[0]['user_name']:$_COOKIE['name'];
        if($permissiondata[0]['commenting'] == '1'){
           $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name']!= '')?$permissiondata[0]['user_name']:$_COOKIE['name'];
        }
        $chat_request = $this->S_request_model->get_chat_request_by_id($permissiondata[0]['request_id'],'desc');
        $request_data = $this->S_request_model->get_request_by_id($permissiondata[0]['request_id']);
        $request_meta = $this->Category_model->get_quest_Ans($permissiondata[0]['request_id']);
        $sampledata = $this->S_request_model->getSamplematerials($permissiondata[0]['request_id']);
        $userdata = $this->S_admin_model->getuser_data($request_data[0]['customer_id']);
        $iscanel = $this->checkuserplaniscancel($userdata);
        if($iscanel == 1){
            redirect(base_url()."accessdenied");
        }
//                echo "<pre/>";print_R($chat_request);exit;
        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            $chat_request[$j]['sender_data'] = $this->S_admin_model->getuser_data($chat_request[$j]['sender_id']);
            $chat_request[$j]['msg_created'] = $this->s_myfunctions->onlytimezoneforall($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
        }
        $data1 = $this->S_request_model->get_request_by_id($permissiondata[0]['request_id']);
        $cat_data = $this->Category_model->get_category_byID($data1[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($data1[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $data1['cat_name'] = $cat_name;
        $data1['subcat_name'] = $subcat_name;
//        echo $permissiondata[0]['request_id'];
        $designer_file = $this->S_admin_model->get_requested_files($permissiondata[0]['request_id'], "", "designer", array('1', '2'),true);
        $data1['count_designerfile'] = count($designer_file);
//        echo "<pre/>";print_R($designer_file);exit;
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
            $designer_file[$i]['chat'] = $this->S_request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
            $designer_file[$i]['chat'][$j]['created'] = $this->s_myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['msg_created'] = $this->s_myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['replies'] = $this->S_request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
            $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
            }
        }
//        echo "<pre/>";print_R($data1);exit;
        for ($i = 0; $i < count($data1[0]['designer_attachment']); $i++) {  
        if($data1[0]['designer_attachment'][$i]['status'] != 'pending' && $data1[0]['designer_attachment'][$i]['status'] != 'Reject'){
            $data1[0]['approved_attachment'] =  $data1[0]['designer_attachment'][$i]['id']; break;
        }
        }
//         echo "<pre/>";print_R($data1);exit;
        $draft_id = $permissiondata[0]['draft_id'];
        if($permissiondata[0]['request_id'] && $permissiondata[0]['draft_id'] == 0){
            $this->load->view('account/customer_header_1', array("popup" => 0));
            $this->load->view('front_end/s_project_info', array('permissiondata'=> $permissiondata,'data' => $data1,'chat_request'=>$chat_request,'checkusernamenotexist'=>$checkusernamenotexist,'userdata' =>$userdata,
                "request_meta" => $request_meta,
            "sampledata" => $sampledata));  
        }else
        {
            $this->load->view('account/customer_header_1', array("popup" => 0));
            $this->load->view('front_end/s_project_image_view', array('permissiondata'=>$permissiondata,'draft_id'=>$draft_id,'designer_file' => $designer_file,'chat_request'=>$chat_request,'userdata' =>$userdata,'checkusernamenotexist'=>$checkusernamenotexist,'request_data'=>$request_data));  
        }
     }else{
         redirect(base_url()."accessdenied");
     }
    }
    
    
    public function checkuserplaniscancel($userdata="") {
       if($userdata[0]['parent_id'] != 0){
            $mainuser = $userdata[0]['parent_id'];
//            echo "main".$mainuser;exit;
            $customer_data = $this->S_admin_model->getuser_data($mainuser,'id,is_cancel_subscription,plan_name');
        }else{
            $customer_data = $userdata;
        }
//        echo "<pre/>";print_R($customer_data);exit;
        if($customer_data[0]['is_cancel_subscription'] == 1){
            return true;
        } 
    }
    
    
     public function SaveSharedusername(){
        if(isset($_POST['savename'])){
           $user_name = isset($_POST['user_name'])?$_POST['user_name']:'';
           $share_key = isset($_POST['share_key'])?$_POST['share_key']:'';
           setcookie("name", $user_name, time() + (86400 * 30), "/");          
          if(isset($_COOKIE['name']) && $_COOKIE['name'] != ''){
              redirect(base_url() . "s_project-info/".$share_key);
          }
           $this->session->set_flashdata('message_success','Congratulations! you can do comments.', 5);
          redirect(base_url() . "s_project-info/".$share_key);
        }
    }
    
        public function s_project_image_view($key){
            $permissiondata = $this->Welcome_model->getpermissionsbySherekey($key);
            if($permissiondata[0]['is_disabled'] == 0){
            $designer_file = $this->S_admin_model->get_requested_files($permissiondata[0]['request_id'], "", "designer", array('1', '2'));
            $request_data = $this->S_request_model->get_request_by_id($permissiondata[0]['request_id']);
            $userdata = $this->S_admin_model->getuser_data($request_data[0]['customer_id']);
            $iscanel = $this->checkuserplaniscancel($userdata);
            if($iscanel == 1){
                redirect(base_url()."accessdenied");
            }
            $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name'] != '') ? $permissiondata[0]['user_name'] : $_COOKIE['name'];
            if($permissiondata[0]['commenting'] == '1'){
                $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name'] != '') ? $permissiondata[0]['user_name'] : $_COOKIE['name'];
            }
            for ($i = 0; $i < sizeof($designer_file); $i++) {
                $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
                $designer_file[$i]['chat'] = $this->S_request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
                for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
                $designer_file[$i]['chat'][$j]['created'] = $this->s_myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['msg_created'] = $this->s_myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['replies'] = $this->S_request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
                $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                }
            }
            $this->load->view('account/customer_header_1', array("popup" => 0));
            $this->load->view('front_end/s_project_image_view', array('permissiondata'=>$permissiondata,'designer_file' => $designer_file,'checkusernamenotexist'=>$checkusernamenotexist,'request_data'=> $request_data,'userdata' => $userdata));  
        }else{
          redirect(base_url()."accessdenied");  
        }
    }
    
       /********not working yet********/
        public function s_project_info_for_unseenmsg($id) {
        // if no user ID then redirect the user to access denied page
        $userid = isset($_GET['id']) ? $_GET['id'] : 0;
        if ($userid == 0) {
            redirect(base_url() . "accessdenied");
        }
        $permissiondata[0]['request_id'] = $id;
        $permissiondata[0]['share_key'] = $id;
        $permissiondata[0]['draft_id'] = 0;
        $permissiondata[0]['cron_msg'] = 1;
        $data1 = $this->S_request_model->get_request_by_id($id);
        // for some cases $data1 is used but for some other checks $request_data is used / gaurav
        $request_data = $data1;
        $chat_request = $this->S_request_model->get_chat_request_by_id($id);
        $userdata = $this->S_admin_model->getuser_data($userid);
        $iscanel = $this->checkuserplaniscancel($userdata);
        $request_meta = $this->Category_model->get_quest_Ans($permissiondata[0]['request_id']);
        $sampledata = $this->S_request_model->getSamplematerials($permissiondata[0]['request_id']);
        if ($iscanel == 1) {
            redirect(base_url() . "accessdenied");
        }
        // if user ID is of parent user but user is not related to that request
        if ($userdata[0]['parent_id'] == 0 && $userid != $request_data[0]['customer_id']) {
            redirect(base_url() . "accessdenied");
        }
        // if user ID is of sub user but parent user is not related to that request 
        if ($userdata[0]['parent_id'] != 0 && $userdata[0]['parent_id'] != $request_data[0]['customer_id']) {
            redirect(base_url() . "accessdenied");
        }

        //for sub user permission
        if ($userdata[0]['parent_id'] != 0) {
            $permissiondata[0]['cancomment_on_req'] = $this->s_myfunctions->isUserPermission('comment_on_req', $userid);
            $permissiondata[0]['candownload_file'] = $this->s_myfunctions->isUserPermission('download_file', $userid);
            $permissiondata[0]['canapprove_file'] = $this->s_myfunctions->isUserPermission('approve/revision_requests', $userid);
            $permissiondata[0]['canbrand_profile_access'] = $this->s_myfunctions->isUserPermission('brand_profile_access', $userid);
            if ($permissiondata[0]['canbrand_profile_access'] != 1 && $data1[0]['created_by'] == $userid) {
                $permissiondata[0]['canbrand_profile_access'] = 1;
            } else if ($permissiondata[0]['canbrand_profile_access'] != 1) {
                $permissiondata[0]['sub_usersbrands'] = $this->S_request_model->getuserspermisionBrands($userid);

                if (!empty($permissiondata[0]['sub_usersbrands'])) {
                    if (in_array($request_data[0]['brand_id'], $permissiondata[0]['sub_usersbrands'])) {
                        $permissiondata[0]['see_draft'] = 1;
                    } else {
                        $permissiondata[0]['see_draft'] = 0;
                        redirect(base_url());
                    }
                } elseif ($request_data[0]['created_by'] == $userid) {
                    $permissiondata[0]['see_draft'] = 1;
                } else {
                    $permissiondata[0]['see_draft'] = 0;
                    redirect(base_url());
                }
            }
        } else {
            $permissiondata[0]['cancomment_on_req'] = 1;
            $permissiondata[0]['candownload_file'] = 1;
            $permissiondata[0]['canapprove_file'] = 1;
            $permissiondata[0]['canbrand_profile_access'] = 1;
        }


        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            $chat_request[$j]['sender_data'] = $this->S_admin_model->getuser_data($chat_request[$j]['sender_id']);
            $chat_request[$j]['msg_created'] = $this->s_myfunctions->onlytimezoneforall($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
        }

        $cat_data = $this->Category_model->get_category_byID($data1[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($data1[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $data1['cat_name'] = $cat_name;
        $data1['subcat_name'] = $subcat_name;
        $designer_file = $this->S_admin_model->get_requested_files($id, "", "designer", array('1', '2'));
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
            $designer_file[$i]['chat'] = $this->S_request_model->get_chat_by_id($designer_file[$i]['id'], "asc");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
                $designer_file[$i]['chat'][$j]['created'] = $this->s_myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['msg_created'] = $this->s_myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['created'] = date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                $designer_file[$i]['chat'][$j]['replies'] = $this->S_request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
                $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                
            }
        }
        $draft_id = $permissiondata[0]['draft_id'];

        $this->load->view('account/customer_header_1', array("popup" => 0));
        $this->load->view('front_end/s_project_info', array('login_user_id' => $userid, 'checkusernamenotexist' => 1, 'permissiondata' => $permissiondata, 'userdata' => $userdata, 'data' => $data1, 'chat_request' => $chat_request, 'userid' => $userid,
            "request_meta" => $request_meta,
            "sampledata" => $sampledata));
    }
      /********not working yet********/
    
    
        public function s_project_image_view_afteraprovedesign($id) {
        // if no user ID then redirect the user to access denied page
        $userid = isset($_GET['id']) ? $_GET['id'] : 0;
        if ($userid == 0) {
            redirect(base_url() . "accessdenied");
        }
        $requestidbydraftid = $this->S_request_model->get_image_by_request_file_id($id);
        $permissiondata[0]['request_id'] = $requestidbydraftid[0]['request_id'];
        $permissiondata[0]['draft_id'] = $id;
        $permissiondata[0]['admin_approvedraft'] = 1;
        $designer_file = $this->S_admin_model->get_requested_files($requestidbydraftid[0]['request_id'], "", "designer", array('1', '2'));
        $request_data = $this->S_request_model->get_request_by_id($requestidbydraftid[0]['request_id']);

        $userdata = $this->S_admin_model->getuser_data($userid);
        $iscanel = $this->checkuserplaniscancel($userdata);
        // if user subscription cancel then redirec the user to access denied page
//        if ($iscanel == 1) {
//            redirect(base_url() . "accessdenied");
//        }
        // if user ID is of parent user but user is not related to that request
        if ($userdata[0]['parent_id'] == 0 && $userid != $request_data[0]['customer_id']) {
            redirect(base_url() . "accessdenied");
        }
//        echo "<pre/>";print_R($userdata);
//       echo "*****************";
//       echo "<pre/>";print_R($request_data);
//       echo "*****************";
//       echo $userid;
//        exit;
        // if user ID is of sub user but parent user is not related to that request 
        if ($userdata[0]['parent_id'] != 0 && $userid != $request_data[0]['customer_id']) {
            redirect(base_url() . "accessdenied");
        }

        //for sub user permission
        if ($userdata[0]['parent_id'] != 0) {
            $permissiondata[0]['cancomment_on_req'] = $this->s_myfunctions->isUserPermission('comment_on_req', $userid);
            $permissiondata[0]['candownload_file'] = $this->s_myfunctions->isUserPermission('download_file', $userid);
            $permissiondata[0]['canapprove_file'] = $this->s_myfunctions->isUserPermission('approve/revision_requests', $userid);
            $permissiondata[0]['canbrand_profile_access'] = $this->s_myfunctions->isUserPermission('brand_profile_access', $userid);
//            echo "<pre/>";print_R($userdata);exit;
            if ($permissiondata[0]['canbrand_profile_access'] != 1 && $data1[0]['created_by'] == $userid) {
                $permissiondata[0]['canbrand_profile_access'] = 1;
            } else if ($permissiondata[0]['canbrand_profile_access'] != 1) {
                   if($userdata[0]['role'] == 'customer' && $permissiondata[0]['canbrand_profile_access'] == 0){
                       $permissiondata[0]['see_draft'] = 1;
                   }
                //$permissiondata[0]['sub_usersbrands'] = $this->S_request_model->getuserspermisionBrands($userid);
//                if (!empty($permissiondata[0]['sub_usersbrands'])) {
//                    if (in_array($request_data[0]['brand_id'], $permissiondata[0]['sub_usersbrands'])) {
//                        $permissiondata[0]['see_draft'] = 1;
//                    } else {
//                        $permissiondata[0]['see_draft'] = 0;
//                        redirect(base_url());
//                    }
//                } else
//                    echo "<pre/>";print_R($permissiondata);exit;
//                 if ($request_data[0]['created_by'] == $userid) {
//                    $permissiondata[0]['see_draft'] = 1;
//                } else {
//                    $permissiondata[0]['see_draft'] = 0;
//                    redirect(base_url());
//                }
            }
            
        } else {
            $permissiondata[0]['cancomment_on_req'] = 1;
            $permissiondata[0]['candownload_file'] = 1;
            $permissiondata[0]['canapprove_file'] = 1;
            $permissiondata[0]['canbrand_profile_access'] = 1;
        }
  
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
            $designer_file[$i]['chat'] = $this->S_request_model->get_chat_by_id($designer_file[$i]['id'], "DESC");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
                $designer_file[$i]['chat'][$j]['created'] = $this->s_myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['msg_created'] = $this->s_myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['created'] = date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                $designer_file[$i]['chat'][$j]['replies'] = $this->S_request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
                $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
            }
        }
        $this->load->view('account/customer_header_1', array("popup" => 0));
        $this->load->view('front_end/s_project_image_view', array('login_user_id' => $userid, 'checkusernamenotexist' => 1, 'permissiondata' => $permissiondata, 'designer_file' => $designer_file, 'data' => $request_data, 'userdata' => $userdata,'request_data' => $request_data));
    }
}