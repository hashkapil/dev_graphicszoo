<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('Myfunctions');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Welcome_model');
        $this->load->model('Request_model');
        $this->load->model('Stripe');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }
    
   public function dashboard() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Reporting " => base_url().'admin/report/dashboard');
        $user_id = $_SESSION['user_id'];
        $today_dt = date('Y-m-d');
        $todaytime = date('Y-m-d H:i:s');
        $ts = strtotime($today_dt);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
        $sevendayback = date('m/d/Y', $start);
        $today = date('m/d/Y', strtotime('next saturday', $start));
        $filterdate = date( 'Y-m-d', strtotime( 'monday this week' ) );
        $tax_report = $this->Admin_model->get_tax_report($filterdate,$today_dt);
        //Get users week data
        $getusersweekdata = $this->Admin_model->getusersbasedcreated($sevendayback, $today);
        $getweeklyusers = $this->getalldaydata($getusersweekdata);
        //Get utm users week data
        $getutmusersweekdata = $this->Admin_model->getutmusersbasedcreated('2019-01-01', $today);
        $Linechartdata = $this->Admin_model->getLinechartdata($sevendayback, $today, 'new_project');
        $getLinechartdata = $this->getalldaydata($Linechartdata);
        $all_category = $this->Request_model->getall_main_category();
        if($_POST['category_flag'] == 1){
        if($_POST['value'] == 'all'){
            $status = '';
        }else{
            $status = array('active','disapprove','checkforapprove');
        }
        foreach ($all_category as $all_categories) {
             $count = $this->Request_model->count_requests_basedon_category($status,$all_categories['id'],$all_categories['name']);
             $req_based_oncategory[$all_categories['name']] = $count;
             //echo $count;
        }
        echo json_encode($req_based_oncategory);exit;
        
        }
        
        //get all active requests count
        $all_project_count['active_disapprove_project'] = $this->Request_model->admin_load_more(array('active', 'disapprove'), true, '', '', '', '');
        $all_project_count['active_project'] = $this->Request_model->admin_load_more(array('active'), true, '', '', '', '');
        $all_project_count['late_project'] = $this->Request_model->count_all_late_projects(array('active','disapprove'),$todaytime);
        $all_project_count['disapprove_project'] = $this->Request_model->admin_load_more(array('disapprove'), true, '', '', '', '');
        $all_project_count['quality_revision_project'] = $this->Request_model->count_quality_revision_projects('disapprove',0);
        $all_project_count['pending_project'] = $this->Request_model->admin_load_more(array('pendingrevision'), true, '', '', '', '');
        $all_project_count['review_project'] = $this->Request_model->admin_load_more(array("pendingforapprove", "checkforapprove"), true, '', '', '', '');
        $all_project_count['approved_project'] = $this->Request_model->admin_load_more(array("approved"), true, '', '', '', '');
        
        //calcualte percenatge of completed project
        $all_pro_exclude_draft = $this->Request_model->count_all_projects(array("draft"));
        $percentofcomplete_project =  $all_project_count['approved_project']/$all_pro_exclude_draft*100;
        $all_project_count['completed_percent'] = round($percentofcomplete_project);
        
        //calcualte percenatge of active project
        $all_pro_for_inpprogress = $this->Request_model->count_all_projects(array("draft","assign","approved"));
        $percentofactive_project =  $all_project_count['active_project']/$all_pro_for_inpprogress*100;
        $all_project_count['active_project_percent'] = round($percentofactive_project);
        
        //calcualte percenatge of revision project
        $percentofrevesion_project =  $all_project_count['disapprove_project']/$all_pro_for_inpprogress*100;
        $all_project_count['disapprove_project_percent'] = round($percentofrevesion_project);
        
        //calcualte percenatge of quality project
        $percentoflate_project =  $all_project_count['quality_revision_project']/$all_pro_for_inpprogress*100;
        $all_project_count['late_project_percent'] = round($percentoflate_project);
        
        
        //get average customers and designers
        $all_customer_count = $this->Request_model->getall_customer_have_request_designers('r.customer_id');
        $all_designer_count = $this->Request_model->getall_customer_have_request_designers('r.designer_id');
        $getallrequestdesigner = $this->Request_model->getTotalrequestbyDesigner();
        $differenceapproval = $this->Request_model->getTimeofrequestsubmission();
        $avg_count = sizeof($all_customer_count)/sizeof($all_designer_count);
        $all_project_count['customer_avg'] = round($avg_count);
        $designer_avg_count = sizeof($getallrequestdesigner)/sizeof($all_designer_count);
        $all_project_count['designer_avg'] = round($designer_avg_count);
        $allRequestCount = $this->Request_model->getall_request_for_all_status();
        $avgdays = $differenceapproval/sizeof($allRequestCount);
        $all_project_count['avgdaysforcompletion'] = round($avgdays);
        
        
        //get all catefory count from requests
        foreach ($all_category as $all_categories) {
             $count = $this->Request_model->count_requests_basedon_category(array('active','disapprove'),$all_categories['id'],$all_categories['name']);
             $req_based_oncategory[$all_categories['name']] = $count;
             //echo $count;
        }
        
        //Taxes Report section
        $allusers = $this->Admin_model->getall_customer();
        $activeUsers = $this->Admin_model->getall_Internationalcustomer();
        $usausers = $this->Admin_model->getall_usacustomer();
        $taxusers = $this->Admin_model->getalltaxcustomer();
        $taxamount = $this->Admin_model->getalltaxamount();
        
        
        //Get messages track data
        $messages['all'] = $this->Admin_model->getallmesaagesfromreqdiscussion($today_dt);
        $messages['customer'] = $this->Admin_model->getallmesaagesfromreqdiscussion($today_dt,'','customer');
        $messages['team'] = $this->Admin_model->getallmesaagesfromreqdiscussion($today_dt,'','','customer');
        $messages['discussion'] = $this->Admin_model->getallmesaagesfromreqdiscussion($today_dt,'','','','request_id');
        $messages['avg'] = $this->Admin_model->getavgmesaagestimefromreqdiscussion($today_dt="2019-05-01","");
        
        
        $diff = array_sum(array_map(function($item) {
                    return $item['time_difference'];
        }, $messages['avg']));
        $time =  $diff/sizeof($messages['avg']);
        
        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $designerdetail = $this->Admin_model->get_designer_n_project();
//        echo "<pre/>";print_R($designerdetail);exit;
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/reports',array("tax_report" => $tax_report,'all_project_count' => $all_project_count,'newprojectReport' => $newprojectReport,'req_based_oncategory' => $req_based_oncategory,'designerdetail' => $designerdetail,'taxinternationalUsers' => $activeUsers,'usausers'=> $usausers,'taxusers' => $taxusers,'allusers' => $allusers,'taxamount' => $taxamount,'messages'=>$messages,'getLinechartdata'=>$getLinechartdata,'getusersweekdata'=>$getweeklyusers,'getutmusersweekdata'=>$getutmusersweekdata));

    }
     public function designer_overview_DateRange(){
         $output = array();
         $start = isset($_POST['start_date'])?$_POST['start_date']:'';
         $end = isset($_POST['end_date'])?$_POST['end_date']:'';
         $designerdetail = $this->Admin_model->get_designer_n_project($start,$end);
         echo json_encode($designerdetail);
     }
    
    public function tax_report(){
        
        $output = array();
        $start_date = isset($_POST['start_date'])? $_POST['start_date'] :'';
        $end_date = isset($_POST['end_date'])? $_POST['end_date'] :'';
        $tax_report = $this->Admin_model->get_tax_report($start_date,$end_date);
        if(!empty($tax_report)){
            $output['tax_report'] = $tax_report;
             $output['status'] = 1;
        }else{
           $output['status'] = 0; 
            $output['tax_report'] = '';
        }
        echo json_encode($output,JSON_NUMERIC_CHECK);exit;
        
    }
    
    public function messages_report(){
        
        $messages = array();
        $start_date = isset($_POST['start_date'])? $_POST['start_date'] :'';
        $end_date = isset($_POST['end_date'])? $_POST['end_date'] :'';
        $messages['all'] = $this->Admin_model->getallmesaagesfromreqdiscussion($start_date,$end_date);
        $messages['customer'] = $this->Admin_model->getallmesaagesfromreqdiscussion($start_date,$end_date,'customer');
        $messages['team'] = $this->Admin_model->getallmesaagesfromreqdiscussion($start_date,$end_date,'','customer');
        $messages['discussion'] = $this->Admin_model->getallmesaagesfromreqdiscussion($start_date,$end_date,'','','request_id');
        echo json_encode($messages);exit;
    }
    
     public function getlinechartData() {
        $output = array();
        $new_array = array();
        $start_date = isset($_POST['start']) ? $_POST['start'] : '';
        $end_date = isset($_POST['end']) ? $_POST['end'] : '';
        $selected_project_cat = isset($_POST['selected_pro_type']) ? $_POST['selected_pro_type'] : '';
        $today = date('Y-m-d');
        $sevendayback = date('Y-m-d', strtotime('-6 days'));
        if($start_date == ""){
            $Linechartdata = $this->Admin_model->getLinechartdata($sevendayback, $today, 'new_project');
        }else{
            $Linechartdata = $this->Admin_model->getLinechartdata($start_date, $end_date, $selected_project_cat);
        }
        $getLinechartdata = $this->getalldaydata($Linechartdata);
        if(!empty($getLinechartdata)){
            $output['linechart'] = $getLinechartdata;
            $output['status'] = 1;
        }else{
          $output['status'] = 0;
        }
        echo json_encode($output,JSON_NUMERIC_CHECK);exit;
        
    }
    
    public function getweeklyusers() {
        $output = array();
        $start_date = isset($_POST['start']) ? $_POST['start'] : '';
        $end_date = isset($_POST['end']) ? $_POST['end'] : '';
        $usersweekdata = $this->Admin_model->getusersbasedcreated($start_date, $end_date);
        $getusersweekdata = $this->getalldaydata($usersweekdata);
        if(!empty($getusersweekdata)){
            $output['linechart'] = $getusersweekdata;
            $output['status'] = 1;
        }else{
          $output['status'] = 0;
        }
        echo json_encode($output,JSON_NUMERIC_CHECK);exit;
    }
    
    public function getWeeklyutmusers() {
        $output = array();
        $start_date = isset($_POST['start']) ? $_POST['start'] : '';
        $end_date = isset($_POST['end']) ? $_POST['end'] : '';
        $getutmusersweekdata = $this->Admin_model->getutmusersbasedcreated($start_date,$end_date);
        if(!empty($getutmusersweekdata)){
            $output['linechart'] = $getutmusersweekdata;
            $output['status'] = 1;
        }else{
          $output['status'] = 0;
        }
        echo json_encode($output,JSON_NUMERIC_CHECK);exit;
    }
    
    
    public function getTotalTaxpaid() {
        $output = array();
        $start_date = isset($_POST['start_date'])? $_POST['start_date'] :'';
        $end_date = isset($_POST['end_date'])? $_POST['end_date'] :'';
        $gettaxamnt = $this->Admin_model->getTotalTaxes($start_date,$end_date);
        if(!empty($gettaxamnt)){
            $output['status'] = 'success'; 
            $output['data'] = $gettaxamnt;
        }else{
           $output['status'] = 'error'; 
           $output['data'] = ''; 
        }
        echo json_encode($output);        
    }
    
    public function getalldaydata($getusersweekdata) {
        $day_array = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
        $userweekly_key = array_column($getusersweekdata, 'y', 'label');
        $userweeklyinfo = array();
        $i=0;
        foreach ($day_array as $weekly_key => $weekly_value) {

            if (array_key_exists($weekly_value,$userweekly_key)){
                foreach ($userweekly_key as $key => $value) {

                    if($key == $weekly_value){
                        $userweeklyinfo[$i]['label'] = $weekly_value;
                        $userweeklyinfo[$i]['y'] = $value;
                    }
                }
                }else{
                    $userweeklyinfo[$i]['label'] = $weekly_value;
                   $userweeklyinfo[$i]['y'] = 0;
            }
             $i++;
        }
        return $userweeklyinfo;
    }

}