<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('Myfunctions');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
    }
    
    public function index(){
        $checkIndetail = array();
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Checkin Users" => base_url().'admin/checkin_listing',
        );
        $checkInUsers = $this->Admin_model->checkInUsers();
        $count_checkInUsers = $this->Admin_model->checkInUsers(true);
        foreach ($checkInUsers as $kk => $vv) {
            $checkInUsers[$kk]['lastlogin_date'] = $this->onlytimezone($vv['lastlogin_date'], 'M/d/Y');
            $checkInUsers[$kk]['checkin_date'] = $this->onlytimezone($vv['checkin_date'], 'M/d/Y');
            $customer_rating = $this->Admin_model->FeedbackColumns_customer($vv['cust_id']);
            $checkInUsers[$kk]["total_avg"] = $customer_rating[0]['total_avg'];
            $checkInUsers[$kk]["svg"] = $this->myfunctions->getfacesaccavg($customer_rating[0]['total_avg']);
        }
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/checkin_listing',array('checkInUsers' => $checkInUsers,'count_checkInUsers' => $count_checkInUsers));
        $this->load->view('admin/admin_footer');
    }
    
    public function load_more_chckinclientlist(){
        $group_no = $this->input->post('group_no');
        $search = $this->input->post('search');
        $isSearch = (trim($search) != '') ? true : false;
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $checkin_users = $this->Admin_model->checkInUsers(false, $start, $content_per_page,$search);
        $count_total_checkins = $this->Admin_model->checkInUsers(true, $start, $content_per_page,$search);
//        echo $count_total_checkins;exit;
        if($checkin_users){
            foreach ($checkin_users as $kk => $vv) { 
            $customer_rating = $this->Admin_model->FeedbackColumns_customer($vv['cust_id']);
//            echo "<pre/>";print_R($customer_rating);
            $vv["total_avg"] = $customer_rating[0]['total_avg'];
            $vv["svg"] = $this->myfunctions->getfacesaccavg($customer_rating[0]['total_avg']);
//            $vv[$kk]['lastAvg'] =  $this->Admin_model->select_custom("FORMAT(AVG(items.satisfied_with_design),2) as avg ,COUNT(items.user_id) as Avgtotal_design","(SELECT rfr.satisfied_with_design,rfr.user_id FROM request_files_review as rfr WHERE `user_id` = '".$vv['cust_id']."' ORDER BY rfr.id DESC LIMIT 5 ) items");
//            echo "<pre/>";print_R($vv);
            $this->load->view('admin/checkin_listing_temp',array('data' => $vv));
            }
            if($count_total_checkins > 0 || $count_total_checkins != ''){ ?>
            <span id="loadingAjaxCount" data-value="<?php echo $count_total_checkins; ?>"></span>
            <?php }
    }
    }
    
    public function checkinByadmin(){
        $chekindata = $output = array();
        $uid = $this->input->post('uid');
        $login_user_id = $this->load->get_var('login_user_id');  
        $user = $this->Admin_model->getuser_data($uid);
//        echo "<pre/>";print_R($user);exit;
        if($uid){
            $chekindata['user_id'] = $uid;
            $chekindata['checkin_by'] = $login_user_id;
            $chekindata['last_checkin'] = date("Y-m-d H:i:s");
            $success = $this->Welcome_model->insert_data("checkin_detail", $chekindata);
            if($success){
                $output['status'] = 'success';
                $output['msg'] = $user[0]['first_name'].' '.$user[0]['last_name']. ' Checkedin successfully!';
                $output['last_checkin'] = $this->onlytimezone(date("Y-m-d H:i:s"), 'M/d/Y');
            }else{
                $output['status'] = 'error';
            }
        }else{
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    public function getcheckinhistory(){
        $output = array();
        $userID = $this->input->post('user_id');
        $checkinhistory = $this->Admin_model->getcheckinhistory($userID);
        $this->onlytimezone($vv['checkin_date'], 'M/d/Y');
        if(!empty($checkinhistory)){
            foreach($checkinhistory as $ck => $cv){
               $output[$ck]['user_name'] = $cv['user_name'];
               $output[$ck]['admin_name'] = $cv['admin_name'];
               $output[$ck]['last_checkin'] =  $this->onlytimezone($cv['last_checkin'], 'M/d/Y');
            }
            echo json_encode($output);
        }
    }
    
    public function getcheckinnotes(){
        $output = array();
       $userID = $this->input->post('user_id');
       $checkinnotes = $this->Admin_model->getcheckinnotes($userID);
       $user = $this->Admin_model->getuser_data($userID);
       $output['user_name'] = $user[0]['first_name'].' '.$user[0]['last_name'];
       $output['result'] = $checkinnotes;
        echo json_encode($output);
    }
    
    public function add_checkinnotes(){
        $notesdata = $output = array();
        $sender_id = $this->input->post('sender_id');
        $userID = $this->input->post('user_id');
        $notes = $this->input->post('notes');
        $notesdata['user_id'] = $userID;
        $notesdata['sender_id'] = $sender_id;
        $notesdata['notes'] = $notes;
        $notesdata['created'] = date("Y-m-d H:i:s");
        $success = $this->Welcome_model->insert_data("checkin_notes", $notesdata);
        if($success){
            $output['status'] = 'success';
            $output['user_id'] = $userID;
            $output['sender_id'] = $sender_id;
            $output['notes'] = $notes;
            echo json_encode($output);
        }
    }
    
    public function rating_details_by_id(){
        $uid = $this->input->post('userid');
        $ratingdetail = $this->Admin_model->rating_details("","","",$uid);
        if($ratingdetail){
            $data['status'] = 'success';
            $data['result'] = $ratingdetail;
        }else{
            $data['status'] = 'error';
            $data['result'] = 'No Detail found.';
        }
        echo json_encode($data);
    }
    
    public function onlytimezone($date_zone) {
        //echo $date_zone;
        $nextdate = $this->myfunctions->onlytimezoneforall($date_zone);
        return $nextdate;
    }
}
