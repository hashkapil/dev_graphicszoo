<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('Myfunctions');
        $this->load->library('Customfunctions');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Category_model');
        $this->load->model('Welcome_model');
        $this->load->model('Request_model');
        $this->load->model('Stripe');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }

    public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "admin") {
            redirect(base_url());
        }
    }

    public function downloadTrialusers() {
        $this->myfunctions->checkloginuser("admin");
        $trailarr = $this->Admin_model->downloadTrial();
        $filename = 'trialusers_' . date('Ymd') . '.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
        // file creation 
        $file = fopen('php://output', 'w');

        $header = array("Id", "First name", "Last name", "Email", "Created date", "Total requests");
        fputcsv($file, $header);
        foreach ($trailarr as $key => $line) {
            fputcsv($file, $line);
        }
        fclose($file);
        exit;
    }

    public function downloadActiveusers() {
        $this->myfunctions->checkloginuser("admin");
        $trailarr = $this->Admin_model->downloadActiveUser();
        $filename = 'activeusers_' . date('Ymd') . '.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
        // file creation 
        $file = fopen('php://output', 'w');

        $header = array("Id", "First name", "Last name", "Email", "Plan Name", "Designer Name", "Created date");
        fputcsv($file, $header);
        foreach ($trailarr as $key => $line) {
            fputcsv($file, $line);
        }
        fclose($file);
        exit;
    }

    public function index($buckettype = NULL) {
        $this->myfunctions->checkloginuser("admin");
        $client_id = isset($_GET['client_id']) ? $_GET['client_id']: '';
        $designer_id = isset($_GET['designer_id']) ? $_GET['designer_id']: '';
        if($client_id){
            $userdata = $this->Admin_model->getuser_data($client_id);
        }
        if($designer_id){
            $userdata = $this->Admin_model->getuser_data($designer_id);
        }
        $va_assignproject = isset($_GET['assign'])?$_GET['assign']:"";
        $count_active_project = $this->Request_model->admin_load_more(array('active', 'disapprove'), true, '', '', '', '', $buckettype,'',$va_assignproject,$client_id,$designer_id);
        $active_project = $this->Request_model->admin_load_more(array('active', 'disapprove'), false, '', '', '', '', $buckettype,'',$va_assignproject,$client_id,$designer_id);
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['category_data'] = $this->Request_model->getRequestCatbyid($active_project[$i]['id'], $active_project[$i]['category_id']);
            if (isset($active_project[$i]['category_data'][0]['category']) && $active_project[$i]['category_data'][0]['category'] != '') {
                $active_project[$i]['category_name'] = $active_project[$i]['category_data'][0]['category'];
            } else {
                $active_project[$i]['category_name'] = $active_project[$i]['category_data'][0]['cat_name'];
            }
            $active_project[$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($active_project[$i]['id'], $active_project[$i]['category_id'], $active_project[$i]['subcategory_id']);
            $getfileid = $this->Request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $active_project[$i]['comment_count'] = $commentcount;
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "admin");
            if ($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL) {
                $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update'], $active_project[$i]['current_plan_name']);
            } else {
                $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
            }
            $active_project[$i]['usertimezone_date'] = $active_project[$i]['user_timezone'];
            $active_project[$i]['plan_color'] = $active_project[$i]['plan_color'];
            $active_project[$i]['addClass'] = $this->adminCheckClassforDesignerAssign($active_project[$i]['status_admin']);

        }
        $data['count_project_a'] = $count_active_project;
        $data['active_project'] = $active_project;
        $data['count_project_i'] = $this->Request_model->admin_load_more(array('assign', 'pending'), true, '', '', '', '', $buckettype,'',$va_assignproject,$client_id,$designer_id);
      
        $data['count_project_pe'] = $this->Request_model->admin_load_more(array("pendingforapprove", "checkforapprove"), true, '', '', '', '', $buckettype,'',$va_assignproject,$client_id,$designer_id);
       
        $data['count_project_c'] = $this->Request_model->admin_load_more(array("approved"), true, '', '', '', '', $buckettype,'',$va_assignproject,$client_id,$designer_id);
        
        $data['count_project_h'] = $this->Request_model->admin_load_more(array("hold"), true, '', '', '', '', $buckettype,'',$va_assignproject,$client_id,$designer_id);
        
        $data['count_project_cancel'] = $this->Request_model->admin_load_more(array("cancel"), true, '', '', '', '', $buckettype,'',$va_assignproject,$client_id,$designer_id);
       
        $data['bucket_type'] = $buckettype;
        $data['designer_list'] = $this->Admin_model->get_total_customer("designer");
        for ($i = 0; $i < sizeof($data['designer_list']); $i++) {
            $user = $this->Account_model->get_all_active_request_by_designer($data['designer_list'][$i]['id']);
            $data['designer_list'][$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($data['designer_list'][$i]['profile_picture']);
            $data['designer_list'][$i]['active_request'] = sizeof($user);
        }
        
        $data['full_admin_access'] = $this->hasAdminfullaccess('full_admin_access','admin');
       
        if($client_id){
            $data['client_url'] = '?client_id=' . $client_id;
            $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
            "View Clients &nbsp; >" => base_url().'admin/dashboard/view_clients',
            $userdata[0]['first_name'].' '.$userdata[0]['last_name'] => base_url().'admin/dashboard/client_id='.$client_id);
        }elseif($designer_id){
            $data['designer_url'] = '?designer_id=' . $designer_id;
            $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
            "View Designers &nbsp; >" => base_url().'admin/dashboard/view_designers',
            $userdata[0]['first_name'].' '.$userdata[0]['last_name'] => base_url().'admin/dashboard/designer_id='.$designer_id);
        }else{
            $breadcrumbs = array("Dashboard" => base_url().'admin/dashboard');
        }
        $data["role_permission"]['remove_csv_dwnld_project'] = $this->myfunctions->admin_role_permissions("remove_csv_dwnld_project");
        $this->load->view('admin/admin_header_1', array("breadcrumbs" => $breadcrumbs));
        $this->load->view('admin/index', $data);
        $this->load->view('admin/admin_footer');
    }

    public function delete_file_req() {
        $this->myfunctions->checkloginuser("admin");
        $reqid = isset($_POST['request_id']) ? $_POST['request_id'] : '';
        $filename = isset($_POST['filename']) ? $_POST['filename'] : '';
        $success = $this->Request_model->delete_request_file('request_files', $reqid, $filename);
        if ($success) {
            $dir = FCPATH . '/public/uploads/requests/' . $reqid;
            unlink($dir . "/" . $filename); //gi
        }
        $this->session->set_flashdata('message_success', "File is Deleted Successfully.!", 5);
    }

    public function adminprofile() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
            "My Profile " => base_url().'admin/dashboard/adminprofile');
        if (($_POST)) {
            $sucess = $this->Account_model->addSubscriptionPlan('subscription_plan', $_POST);
            if ($sucess) {
                $this->session->set_flashdata('success', 'Successfully add subscription plan', 5);
                redirect(base_url() . "admin/dashboard/adminprofile?status=2");
            } else {
                $this->session->set_flashdata('error', 'Subscription plan not added', 5);
                redirect(base_url() . "admin/dashboard/adminprofile?status=2");
            }
        }
        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);
        $data[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($data[0]['profile_picture']);
        $setting_fields = $this->Admin_model->getSettingfields();
        $timezone = $this->Admin_model->get_timezone();
        //echo sizeof($timezone);
        //  echo "<pre>";print_r($timezone);
        $subscriptionplan = $this->Request_model->getAllSubscriptionPlan();
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $this->load->view('admin/admin_header_1', array("breadcrumbs" => $breadcrumbs));
        $this->load->view('admin/adminprofile', array("data" => $data, "setting_fields" => $setting_fields, "timezone" => $timezone, 'subscriptionplan' => $subscriptionplan));
        $this->load->view('admin/admin_footer');
    }

    public function change_customer_status_settings() {
        $this->myfunctions->checkloginuser("admin");
        $cust_status = $_POST['status'];
        $cust_id = $_POST['data_id'];
        $datekey = $_POST['data_key'];
        //echo $datekey;exit;
        if ($_POST['status'] == 'true') {
            $this->Request_model->update_customer_active_status("settings", array("value" => 1), array("id" => $cust_id, "key" => $datekey));
        } elseif ($_POST['status'] == 'false') {
            $this->Request_model->update_customer_active_status("settings", array("value" => 0), array("id" => $cust_id, "key" => $datekey));
        }
    }

    public function edit_profile_image_form() {
        $this->myfunctions->checkloginuser("admin");
        $user_id = $_SESSION['user_id'];
        $fname = explode(' ', $_POST['first_name']);
//        echo "<pre/>";print_R($_FILES);exit;
        if ($_FILES['profile']['name'] != "") {
            $file = pathinfo($_FILES['profile']['name']);
            $ext = strtolower(pathinfo($_FILES['profile']['name'], PATHINFO_EXTENSION));
            $s3_file = $file['filename'] . '-' . rand(1000, 1) . '.' . $ext;
            if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_PROFILE .'_thumb')) {
                    mkdir('./' . FS_UPLOAD_PUBLIC_UPLOADS_PROFILE .'_thumb', 0777, TRUE);
            }
            $config = array(
                'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_PROFILE,
                'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                'file_name' => $s3_file
            );
            $check = $this->myfunctions->CustomFileUpload($config, 'profile');
            if ($check['status'] == 1) {
                /** auto resize profile **/
                $thumb_tmp_path = FS_UPLOAD_PUBLIC_UPLOADS_PROFILE .'_thumb/';
                $tmp = $_FILES['profile']['tmp_name'];
                $this->myfunctions->make_thumb($tmp,$thumb_tmp_path . $s3_file,'', $ext,150);
                if (UPLOAD_FILE_SERVER == 'bucket') {
                    $staticName = base_url() . $thumb_tmp_path . $s3_file;
                    //echo $staticName;exit;
                    $config = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_PROFILE .'_thumb/',
                        'file_name' => $s3_file
                    );
                    $this->load->library('s3_upload');
                    $this->s3_upload->initialize($config);
                    $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName);
                    if ($uplod_thumb['status'] == 1) {
                        $delete = $thumb_tmp_path . $s3_file;
                        unlink($delete);
                        unlink('./tmp/tmpfile/' . basename($staticName));
                        rmdir($thumb_tmp_path);
                    }
                }
                $profile_pic = array(
                    'profile_picture' => $s3_file,
                );
                $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                if ($update_data_result) {
                    $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!');
                    redirect(base_url() . "admin/dashboard/adminprofile");
                } else {
                    $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!');
                    redirect(base_url() . "admin/dashboard/adminprofile");
                }
            } else {
                $this->session->set_flashdata('message_error', $check['msg'], 5);
                //$this->session->set_flashdata('message_error', $data, 5);
            }
        }
//        if (!empty($this->input->post())) {
//            $profile_pic = array(
//                'first_name' => $fname[0],
//                'last_name' => $fname[1],
//                'notification_email' => $_POST['notification_email'],
//            );
//            $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
//            if ($update_data_result) {
//                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
//            } else {
//                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
//            }
//        }
    }

    public function customer_edit_profile() {
        $this->myfunctions->checkloginuser("admin");
        $user_id = $_SESSION['user_id'];
        if ($_POST['company_name'] != "") {
            $profileUpdate = array(
                'company_name' => $_POST['company_name'],
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
                'notification_email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'address_line_1' => $_POST['address_line_1'],
                'hobbies' => trim($_POST['hobbies']),
                'about_info' => trim($_POST['about_info']),
                'title' => $_POST['title'],
                'city' => $_POST['city'],
                'state' => $_POST['state'],
                'zip' => $_POST['zip'],
                'timezone' => $_POST['timezone'],
                
            );
            //print_r($profileUpdate);
            $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
            if ($update_data_result) {
                $this->session->set_userdata('timezone', $profileUpdate['timezone']);
                //echo "hello";
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                //echo "hiii";
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
        }
    }

    public function change_password_front() {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
            if ($user_data[0]['new_password'] != md5($_POST['old_password'])) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "admin/dashboard/adminprofile");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "admin/dashboard/adminprofile");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "admin/dashboard/adminprofile");
        }
    }

    public function all_requests() {
        $this->myfunctions->checkloginuser("admin");
        $requested_designs = $this->Admin_model->get_all_requested_designs(array("active", "disapprove"), "", "", "", "", "status_admin");

        $inprogressrequest = $this->Admin_model->get_all_requested_designs(array("assign", "pending"), "", "", "", "", "status_admin");
        $checkforapproverequests = $this->Admin_model->get_all_requested_designs(array("pendingforapprove", "checkforapprove"), "", "", "", "", "status_admin");
        $approved_designs = $this->Admin_model->get_all_requested_designs(array("approved"), "", "", "", "", "status_admin");

        //$get_message = $this->Admin_model->get_unread_message();

        $admin_unread_message_count_array = array();
        $total_unread_message = 0;
        $total_assign_message = 0;
        $total_checkforapprove_message = 0;
        $total_approved_message = 0;

        for ($i = 0; $i < sizeof($requested_designs); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($requested_designs[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array[$requested_designs[$i]['id']] = $admin_unread_msg_count;
            if ($requested_designs[$i]['status_admin'] == "active" || $requested_designs[$i]['status_admin'] == "disapprove") {
                $total_unread_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "assign") {
                $total_assign_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "checkforapprove" || $requested_designs[$i]['status_admin'] == "pendingforapprove") {
                $total_checkforapprove_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "approved") {
                $total_approved_message += $admin_unread_msg_count;
            }
        }

        $admin_unread_message_count_array2 = array();
        $total_unread_message2 = 0;
        for ($i = 0; $i < sizeof($approved_designs); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($approved_designs[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array2[$approved_designs[$i]['id']] = $admin_unread_msg_count;
            $total_unread_message2 += $admin_unread_msg_count;
        }
        for ($i = 0; $i < sizeof($inprogressrequest); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($inprogressrequest[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array2[$inprogressrequest[$i]['id']] = $admin_unread_msg_count;
            $total_unread_message2 += $admin_unread_msg_count;
        }
        $customers = $this->Admin_model->get_total_customer();

        $customerarray = array();

        for ($i = 0; $i < sizeof($customers); $i++) {
            if ($customers[$i]['current_plan'] != "") {

                if ($customers[$i]['plan_turn_around_days'] == 1) {
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "red";
                } elseif ($customers[$i]['plan_turn_around_days'] !== 3) {
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "blue";
                } else {
                    $customerarray[$i]['current_plan_name'] = "";
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "";
                }
            } else {
                $customerarray[$i]['current_plan_name'] = "";
                $customerarray[$customers[$i]['id']]['current_plan_color'] = "";
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/all_requests', array("customerarray" => $customerarray, "customers" => $customers, "total_unread_message2" => $total_unread_message2, "admin_unread_message_count_array2" => $admin_unread_message_count_array2, "total_approved_message" => $total_approved_message, "total_checkforapprove_message" => $total_checkforapprove_message, "total_assign_message" => $total_assign_message, "total_unread_message" => $total_unread_message, "admin_unread_message_count_array" => $admin_unread_message_count_array, "requested_designs" => $requested_designs, "inprogressrequest" => $inprogressrequest, "checkforapproverequests" => $checkforapproverequests, "approved_designs" => $approved_designs));
        $this->load->view('admin/admin_footer');
    }

    public function all_customer() {
        $this->myfunctions->checkloginuser("admin");
        $all_customer = $this->Admin_model->get_total_customer("customer");
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/all_customer', array("all_customer" => $all_customer));
        $this->load->view('admin/admin_footer');
    }

    public function edit_customer($customer_id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($customer_id == "") {
            redirect(base_url() . "admin/all_customer");
        }
        $customer = $this->Admin_model->getuser_data($customer_id);
        if (empty($customer)) {
            $this->session->set_flashdata('message_error', 'No Customer Found.!', 5);
            redirect(base_url() . "admin/all_customer");
        }
        $subscription_plan_list = array();
        $subscription = array();
        $planid = "";
        if ($customer[0]['current_plan'] != "") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            print_r($getcurrentplan);
            if (!empty($getcurrentplan)) {
                $plandetails = $getcurrentplan->items->data[0]->plan;
                $planid = $plandetails->id;
            } else {
                $plandetails = "";
                $planid = "";
            }
        }
        $allplan = $this->Stripe->getallsubscriptionlist();
        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }
        $carddetails = array();
        $carddetails['last4'] = "";
        $carddetails['exp_month'] = "";
        $carddetails['exp_year'] = "";
        $carddetails['brand'] = "";
        $plan = array();
        if ($customer[0]['current_plan'] != "" && $customer[0]['current_plan'] != "0") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            if (!empty($getcurrentplan)) {
                $plan['userid'] = $getcurrentplan->id;
                $plan['current_period_end'] = $getcurrentplan->current_period_end;
                $plan['current_period_start'] = $getcurrentplan->current_period_start;
                $plan['billing'] = $getcurrentplan->billing;
                $plan['trial_end'] = $getcurrentplan->trial_end;
                $plan['trial_start'] = $getcurrentplan->trial_start;


                $plandetails = $getcurrentplan->items->data[0]->plan;
                $plan['planname'] = $plandetails->name;
                $plan['planid'] = $plandetails->id;
                $plan['interval'] = $plandetails->interval;
                $plan['amount'] = $plandetails->amount / 100;
                $plan['created'] = $plandetails->created;

                $customerdetails = $this->Stripe->getcustomerdetail($customer[0]['current_plan']);
                if (!empty($customerdetails)) {
                    $carddetails['last4'] = $customerdetails->sources->data[0]->last4;
                    $carddetails['exp_month'] = $customerdetails->sources->data[0]->exp_month;
                    $carddetails['exp_year'] = $customerdetails->sources->data[0]->exp_year;
                    $carddetails['brand'] = $customerdetails->sources->data[0]->brand;
                }
            }
        } else {
            $plan['planid'] = "";
        }
        $designer_list = $this->Admin_model->get_total_customer("designer");
        $designs_requested = $this->Admin_model->get_all_requested_designs(array(), $customer_id, "no");
        $designs_approved = $this->Admin_model->get_all_requested_designs(array("approved"), $customer_id, "", "", "", "status_admin");
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/edit_customer', array("customer" => $customer, "subscription" => $subscription, "planid" => $planid, "carddetails" => $carddetails, "designer_list" => $designer_list, "designs_requested" => $designs_requested, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function phpinfo() {
        phpinfo();
    }

    public function attach_file_process($issample = NULL) {
        // $this->myfunctions->checkloginuser("admin");
        if($issample){
            $allowedtype = ALLOWED_SAMPLE_TYPES;
        }else{
            $allowedtype = ALLOWED_FILE_TYPES;
        }
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        $dateFolder = strtotime(date('Y-m-d H:i:s'));

        if (!(isset($_SESSION['temp_attachfolder_name']) && $_SESSION['temp_attachfolder_name'] != '')) {
            if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                $_SESSION['temp_attachfolder_name'] = './public/uploads/temp/' . $dateFolder;
            } else {
                $_SESSION['temp_attachfolder_name'] = '';
            }
        } else {
            if (!is_dir($_SESSION['temp_attachfolder_name'])) {
                if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                    $_SESSION['temp_attachfolder_name'] = './public/uploads/temp/' . $dateFolder;
                } else {
                    $_SESSION['temp_attachfolder_name'] = '';
                }
            }
        }
        // echo "<pre>";print_R($_SESSION);die; 
        if ($_SESSION['temp_attachfolder_name'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['file_upload']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['file_upload']['name'] = $files['file_upload']['name'][$i];
                $_FILES['file_upload']['type'] = $files['file_upload']['type'][$i];
                $_FILES['file_upload']['tmp_name'] = $files['file_upload']['tmp_name'][$i];
                $_FILES['file_upload']['error'] = $files['file_upload']['error'][$i];
                $_FILES['file_upload']['size'] = $files['file_upload']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['temp_attachfolder_name'],
//                    'allowed_types' => '*',
                     'allowed_types' => $allowedtype,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("file_upload")) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $error_data = array();
                    $error_data[0] = array();
                    $error_data[0]['file_name'] = $files['file_upload']['name'][$i];
                    $error_data[0]['error'] = true;
                    $error_data[0]['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $error_data;
                    $output['status'] = true;
                    $output['error'] = '';
                }
            }
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        echo json_encode($output);
        exit;
    }

    public function delete_attachfile_from_folder() {
        $this->myfunctions->checkloginuser("admin");
        $folderPath = $_SESSION['temp_attachfolder_name'];
        $folderPaths = $_SESSION['temp_attachfolder_name'] . '/*';
        if (!empty($_POST)) {
            $data = $_POST['file_name'];
            $files = glob($folderPaths); // get all file names
            $dir = FCPATH . $folderPath;
            $dirHandle = opendir($dir);
            while ($file = readdir($dirHandle)) {
                if ($file == $data) {
                    $res = unlink($dir . "/" . $file); //give correct path,
                } else {
                    $res  = 0; 
                }
            }
            echo $res; 
        }
    }

    public function view_request($id) {
        $this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');
        $request_meta = $this->Category_model->get_quest_Ans($id);
        $sampledata = $this->Request_model->getSamplematerials($id);
        if ($id == "") {
            redirect(base_url() . "admin/all_requests");
        }
        $fromclient = isset($_GET['client_id']) ? $_GET['client_id'] : '';
        $fromdesigner = isset($_GET['designer_id']) ? $_GET['designer_id'] : '';
        if ($fromclient) {
            $bckurl = '?client_id=' . $fromclient;
        } elseif ($fromdesigner) {
            $bckurl = '?designer_id=' . $fromdesigner;
        } else {
            $bckurl = '';
        }
        $url = "admin/dashboard/view_request/" . $id;
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
            "Projects " => base_url().'admin/dashboard/view_request/'.$id,);
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $login_user_id, "url" => $url));
//        $this->Admin_model->update_data("request_discussions", array("admin_seen" => 1), array("request_id" => $id));
        if ($_POST['attach_file']) {
            if ($_FILES['file_upload']['name'] != "") {
                if (!is_dir('public/uploads/requests/' . $id)) {
                    $data = mkdir('./public/uploads/requests/' . $id, 0777, TRUE);
                }
                //Move files from temp folder
                // Get array of all source files
                $uploadPATH = $_SESSION['temp_attachfolder_name'];
                $uploadPATHs = str_replace("./", "", $uploadPATH);
                //echo $uploadPATHs;exit; 
                $files = scandir($uploadPATH);
                // Identify directories
                $source = $uploadPATH . '/';
                //echo "<pre>"; print_r($destination);exit;
                $destination = './public/uploads/requests/' . $id . '/';
                // echo "<pre>"; print_r($uploadPATH);exit;
                if (UPLOAD_FILE_SERVER == 'bucket') {
                    foreach ($files as $file) {

                        if (in_array($file, array(".", "..")))
                            continue;

                        $staticName = base_url() . $uploadPATHs . '/' . $file;
                        //echo $staticName;exit;
                        $config = array(
                            'upload_path' => 'public/uploads/requests/' . $id . '/',
                            'file_name' => $file
                        );
                        $this->load->library('s3_upload');
                        $this->s3_upload->initialize($config);
                        $is_uploaded = $this->s3_upload->upload_multiple_file($staticName);
                        if ($is_uploaded['status'] == 1) {
                            $delete[] = $source . $file;
                            unlink('./tmp/tmpfile/' . basename($staticName));
                        }
                    }
                } else {
                    foreach ($files as $file) {

                        if (in_array($file, array(".", "..")))
                            continue;
                        // If we copied this successfully, mark it for deletion
                        //echo APPPATH.$source.$file;
                        $filepath = APPPATH . $source . $file;

                        if (copy($source . $file, $destination . $file)) {
                            $delete[] = $source . $file;
                        }
                    }
                }
                // exit;
                // Cycle through all source files
                //echo "kfdljgi";
                // Delete all successfully-copied files
                foreach ($delete as $file) {
                    unlink($file);
                }
                rmdir($source);

                unset($_SESSION['temp_attachfolder_name']);
                if ($is_uploaded['status'] == 1 || isset($filepath)) {
                    if (isset($_POST['delete_FL']) && $_POST['delete_FL'] != '') {
                        // echo "<pre>"; print_R($_POST['delete_FL']);exit;
                        foreach ($_POST['delete_FL'] as $SubFIlekey => $GetFIleName) {
                            // $GetFIleName = substr($SubFIleName, strrpos($SubFIleName, '/') + 1);
                            $request_files_data = array("request_id" => $id,
                                "user_id" => $_SESSION['user_id'],
                                "user_type" => "customer",
                                "file_name" => $GetFIleName,
                                "created" => date("Y-m-d H:i:s"));
                            //print_r($request_files_data);exit;
                            $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                        }
                        $this->myfunctions->capture_project_activity($id,'','','add_attachment_byadmin','view_request',$login_user_id);
                    }
                } else {
                    $this->session->set_flashdata('message_error', "Your file is not uploaded, Try Again!", 5);
                    if ($bckurl != '') {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . $bckurl);
                    } else {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
                }
                if ($bckurl != '') {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . $bckurl);
                    } else {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
            } else {
                $this->session->set_flashdata('message_error', "Please Upload Preview  Files.!", 5);
                if ($bckurl != '') {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . $bckurl);
                    } else {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
            }
        }

        $request = $this->Request_model->get_request_by_id_admin($id);
        //echo "<pre>";print_r($data);
        $cat_data = $this->Category_model->get_category_byID($request[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $request['cat_name'] = $cat_name;
        $request['subcat_name'] = $subcat_name;
        $branddata = $this->Request_model->get_brandprofile_by_id($request[0]['brand_id']);
        $brand_materials_files = $this->Request_model->get_brandprofile_materials_files($branddata[0]['id']);
        for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
            if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
                $brand_materials_files['latest_logo'] = $brand_materials_files[$i]['filename'];
                break;
            }
        }
        $request_id = $request[0]['id'];
        $files = $this->Request_model->get_attachment_files($id, "designer");
        $customer = $this->Account_model->getuserbyid($request[0]['customer_id']);
        $cancel_suscription = $customer[0]["is_cancel_subscription"];
        $is_approveDraftemailbyadmin = $this->Admin_model->approveDraftemailbyadmin();
        $this->Admin_model->approveDraftemailbyuser($request[0]['customer_id']);
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->Request_model->get_file_chat($files[$i]['id'], "admin");

            if ($chat_of_file) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = $chat_of_file;
                $file_chat_array[$i]['id'] = $files[$i]['id'];
                $files[$i]['chat_count'] = $chat_of_file;
            } else {
                $files[$i]['chat_count'] = "";
                $file_chat_array[$i]['id'] = "";
            }
            $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
            $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        }
        if (!empty($_FILES)) {
            if ($_FILES['src_file']['name'] != "" && $_FILES['preview_file']['name'] != "") {
                $config['image_library'] = 'gd2';
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id)) {
                    $data = mkdir('./' . FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id, 0777, TRUE);
                }
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb')) {
                    $data = mkdir('./' . FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb', 0777, TRUE);
                }
                $this->load->library('image_lib', $config);
                $requestforfile = $this->Request_model->get_request_by_id($id);
                $file_name_title = substr($requestforfile[0]['title'], 0, 15);
                $file_name_title = str_replace(" ", "_", $file_name_title);
                $file_name_title = preg_replace('/[^A-Za-z0-9\_]/', '', $file_name_title);
                $six_digit_random_number = mt_rand(100, 999);
                $ext = strtolower(pathinfo($_FILES['src_file']['name'], PATHINFO_EXTENSION));
                $ext1 = strtolower(pathinfo($_FILES['preview_file']['name'], PATHINFO_EXTENSION));
                $sourcename = $file_name_title . '_' . $six_digit_random_number . '.' . $ext;
                $previewname = $file_name_title . '_' . $six_digit_random_number . '1.' . $ext1;
                if (!empty($_FILES)) {
                    $config = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/',
                        'allowed_types' => '*',
                        'file_name' => $sourcename,
                    );
                    $config2 = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/',
                        'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                        'max_size' => '5000000',
                        'file_name' => $previewname,
                    );

                    if (($_FILES['preview_file']['size'] >= $config2['max_size']) || ($_FILES['preview_file']['size'] == 0)) {
                        $this->session->set_flashdata('message_error', "File too large. File must be less than 5 MB.", 5);
                        if ($bckurl != '') {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . $bckurl);
                    } else {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
                    }
                    if (in_array($ext1, $config2['allowed_types'])) {

                        $check_src_file = $this->myfunctions->CustomFileUpload($config, 'src_file');

                        $src_data = array();
                        $src_data[0]['file_name'] = "";
                        if ($check_src_file['status'] == 1) {
                            // echo $check_src_file['msg'];
                        } else {
                            //$data = array('error' => $this->upload->display_errors());

                            $this->session->set_flashdata('message_error', $check_src_file['msg'], 5);
                        }


                        //$config2['preview_file'] = $_FILES['preview_file']['name'];

                        $_FILES['src_file']['name'] = $file_name_title . '_' . $six_digit_random_number . '.' . $ext1;
                        $_FILES['src_file']['type'] = $_FILES['preview_file']['type'];
                        $_FILES['src_file']['tmp_name'] = $_FILES['preview_file']['tmp_name'];
                        $_FILES['src_file']['error'] = $_FILES['preview_file']['error'];
                        $_FILES['src_file']['size'] = $_FILES['preview_file']['size'];

                        $check_preview_file = $this->myfunctions->CustomFileUpload($config2, 'preview_file');

                        if ($check_preview_file['status'] == 1) {
                            //echo $check_src_file['msg'];exit;

                            $thumb_tmp_path = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb/';
                            $tmp = $_FILES['preview_file']['tmp_name'];

                            $thumb = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $previewname, 600, $ext1);
                            if (UPLOAD_FILE_SERVER == 'bucket') {
                                $staticName = base_url() . $thumb_tmp_path . $previewname;
                                //echo $staticName;exit;
                                $config = array(
                                    'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb/',
                                    'file_name' => $previewname
                                );
                                $this->load->library('s3_upload');
                                $this->s3_upload->initialize($config);
                                $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName);
                                if ($uplod_thumb['status'] == 1) {
                                    $delete = $thumb_tmp_path . $previewname;
                                    unlink($delete);
                                    unlink('./tmp/tmpfile/' . basename($staticName));

                                    rmdir($thumb_tmp_path);
                                    rmdir($thumb_dummy_path);
                                }
                            }
                            $request_files_data = array("request_id" => $id,
                                "user_id" => $_SESSION['user_id'],
                                "user_type" => "designer",
                                "file_name" => $previewname,
                                "preview" => 1,
                                "admin_seen" => 1,
                                "status" => "customerreview",
                                "created" => date("Y-m-d H:i:s"),
                                "modified" => date("Y-m-d H:i:s"),
                                "src_file" => $sourcename);
                            $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                            $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($request[0]['customer_id'], $customer[0]['plan_name']);
                            $this->myfunctions->send_email_after_admin_approval($request[0]['title'], $request[0]['id'], $customer[0]['email'], $data,$subdomain_url);
                            $send_emailto_subusers = $this->myfunctions->send_emails_to_sub_user($request[0]['customer_id'], 'approve/revision_requests', $is_approveDraftemailbyadmin, $request[0]['id']);
                            if (!empty($send_emailto_subusers)) {
                                foreach ($send_emailto_subusers as $send_emailto_subuser) {
                                    $this->myfunctions->send_email_after_admin_approval($request[0]['title'], $request[0]['id'], $send_emailto_subuser['email'], $data, $subdomain_url);
                                }
                            }

                            $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => "checkforapprove", "status_qa" => "checkforapprove", "status_designer" => "checkforapprove", "status_admin" => "checkforapprove", "priority" => 0,"sub_user_priority" => 0, "modified" => date("Y-m-d H:i:s")), array("id" => $id));
                            
                            $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                            if ($ActiveReq == 1) {
                                $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','view_request',$id);

                            }
                             /**capture upload draft event**/
                            $this->myfunctions->capture_project_activity($id,$data,$request[0]['designer_id'],'admin_upload_draft','view_request',$login_user_id,'0','1','1');
                            $this->myfunctions->capture_project_activity($id,$data,$request[0]['designer_id'],$request[0]['status'].'_to_checkforapprove','view_request',$login_user_id,'1','1','0');
                            /* save notifications when upload draft design */

                            $this->myfunctions->show_notifications($id, $login_user_id, "Upload New File For \"" . $request[0]['title'] . "\" . Please Check And Approve..!", "customer/request/project_info/" . $id, $request[0]['customer_id']);
                            $this->myfunctions->show_notifications($id, $login_user_id, "Approved your design For \"" . $request[0]['title'] . "\"", "designer/request/project_info/" . $id, $request[0]['designer_id']);
                            /* end save notifications when upload draft design */
                            $this->session->set_flashdata('message_success', "File is Uploaded Successfully.!", 5);
                            if ($bckurl != '') {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . $bckurl);
                    } else {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
                        } else {
                            $this->session->set_flashdata('message_error', $check_preview_file['msg'], 5);
                        }
                    } else {
                        $this->session->set_flashdata('message_error', "Please upload valid files", 5);
                        if ($bckurl != '') {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . $bckurl);
                    } else {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
                    }
                }
            } else {
                $this->session->set_flashdata('message_error', "Please Upload Preview or Source Both Files.!", 5);
                if ($bckurl != '') {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . $bckurl);
                    } else {
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
            }
        }

       if (isset($_POST['adddesingerbtn'])) { 
            $login_user_id = $this->load->get_var('login_user_id');
            $reqID = (isset($_POST['request_id']) && !empty($_POST['request_id'])) ? $_POST['request_id'] : $id;
            $datasucc = $this->Admin_model->assign_designer_by_qa($_POST['assign_designer'], $reqID);
            $assign_id = $this->input->post('assign_designer'); 
            $checkDraft_id = $this->Welcome_model->select_data("draft_id,designer_id","activity_timeline","request_id='".$reqID."' AND slug ='assign_designer' AND action_perform_by ='".$login_user_id."'",$limit="",$start="","desc","draft_id");
            $checkDraft_idForReas = $this->Welcome_model->select_data("draft_id,designer_id","activity_timeline","request_id='".$reqID."' AND slug ='reassign_designer' AND action_perform_by ='".$login_user_id."' ",$limit="",$start="","desc","draft_id");
            
        if(!empty($checkDraft_id) ){ 
            if(empty($checkDraft_idForReas) && $checkDraft_id[0]['draft_id'] == 0 && $checkDraft_id[0]['designer_id'] != 0){
             // die("ok3"); 
             //    die("if"); 
                $new_assign_id =$assign_id;
                $assign_id =$checkDraft_id[0]['designer_id'];
                $slug="reassign_designer";
            }else if($checkDraft_idForReas[0]['draft_id'] != 0 && $checkDraft_idForReas[0]['designer_id'] != 0){
                // die("elseif"); 
                $new_assign_id =$assign_id;
                $assign_id = $checkDraft_idForReas[0]['draft_id'];  
                $slug='reassign_designer';
            }
           }else{
             //die("else"); 
               $new_assign_id = 0;
               $assign_id = $assign_id; 
               $slug="assign_designer";
           }
           // die("here"); 
          
            $this->myfunctions->capture_project_activity($reqID,$new_assign_id, $assign_id, $slug, 'view_request', $login_user_id);
            if ($datasucc) {
                $this->session->set_flashdata('message_success', 'Designer assigned Successfully.!', 5);
                if ($bckurl != '') {
                    redirect(base_url() . "admin/dashboard/view_request/" . $id . $bckurl);
                } else {
                    redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                }
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $profile_data[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($profile_data[0]['profile_picture']);
        $request_files = $this->Admin_model->get_requested_files_admin($request_id);
        for ($i = 0; $i < count($request_files); $i++) {
            $request_files[$i]['img_created'] = $this->onlytimezone($request_files[$i]['created']);
            $request_files[$i]['created'] = date('d M, Y', strtotime($request_files[$i]['img_created']));
        }

        $admin_request_files = $this->Admin_model->get_requested_files($request_id, "", "designer");

        $customer_additional_files = $this->Admin_model->get_requested_files($request_id, "", "customer");

        $admin_files = $this->Admin_model->get_requested_files($request_id, "", "admin");

        $designer_list = $this->Admin_model->get_total_customer("designer");
        for ($dl = 0; $dl < count($designer_list); $dl++) {
            $user = $this->Account_model->get_all_active_request_by_designer($designer_list[$dl]['id']);
            $designer_list[$dl]['profile_picture'] = $this->customfunctions->getprofileimageurl($designer_list[$dl]['profile_picture']);
            $designer_list[$dl]['active_request'] = sizeof($user);
        }
        $chat_request = $this->Request_model->get_chat_request_by_id($id,'DESC');
        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            $chat_request[$j]['msg_created'] = $this->onlytimezone($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
            $chat_request[$j]['chat_created_date1'] =   date('M-d-Y h:i A', strtotime($chat_request[$j]['msg_created']));
            //echo "<pre/>";print_r($chat_request[$j]['chat_created_date1']);
            $currentdate = date('M-d-Y');
            $datetime = new DateTime($chat_request[$j]['chat_created_date1']);
            $date = $datetime->format('M-d-Y');
            $time = $datetime->format('h:i A');
            //$chat_request[$j]['chat_created_time'] = $time;
            
            if($timediff == '' && $datetimediff = ''){
             $chat_request[$j]['chat_created_datee'] =   $date; 
             $timediff =  $chat_request[$j]['chat_created_datee'];
             $datetimediff =   $chat_request[$j]['chat_created_date1'];
            }
            $sincedate = $chat_request[$j]['chat_created_date1'];
            $datetime1 = new DateTime($datetimediff);
            $datetime2 = new DateTime($sincedate);
            $mindiff = $datetime1->diff($datetime2);
            
            if ($mindiff->i >= BATCH_MESSAGE_TIMELIMIT || $chat_request[$j]['sender_id'] != $chat_request[$j - 1]['sender_id'] || $chat_request[$j]['shared_user_name'] != $chat_request[$j - 1]['shared_user_name']) {
//                echo "value of i".$i."<br/>";
                $datetimediff = $sincedate;
                $timeee = date('h:i A', strtotime($sincedate));
                $chat_request[$j]['chat_created_time'] = $timeee;
                $chat_request[$j]['profile_picture'] = $this->customfunctions->getprofileimageurl($chat_request[$j]['profile_picture']);
                $chat_request[$j]['fname'] = $chat_request[$j]['first_name'];
                $chat_request[$j]['shared_name'] = $chat_request[$j]['shared_user_name'];
                
            }
            
            if($date != $timediff){
                $timediff = $date;
                $chat_request[$j]['chat_created_datee'] = $date;
                if($date == $currentdate){
                    $chat_request[$j]['chat_created_datee'] = "Today";
                }
            }

        }

        $chat_request_without_customer = $this->Request_model->get_chat_request_by_id_without_customer($id,'DESC');
        $timediff_nocust = '';
        for ($j = 0; $j < count($chat_request_without_customer); $j++) {
            $chat_request_without_customer[$j]['msg_created'] = $this->onlytimezone($chat_request_without_customer[$j]['created']);
            $chat_request_without_customer[$j]['chat_created_date'] = $chat_request_without_customer[$j]['msg_created'];
            $chat_request_without_customer[$j]['chat_created_date1'] =   date('M-d-Y h:i A', strtotime($chat_request_without_customer[$j]['msg_created']));
            //echo "<pre/>";print_r($chat_request[$j]['chat_created_date1']);
            $currentdate = date('M-d-Y');
            $datetime = new DateTime($chat_request_without_customer[$j]['chat_created_date1']);
            $date = $datetime->format('M-d-Y');
            $time = $datetime->format('h:i A');
            //$chat_request_without_customer[$j]['chat_created_time'] = $time;
            if($timediff == '' && $datetimediff = ''){
             $chat_request_without_customer[$j]['chat_created_datee'] =   $date; 
             $timediff =  $chat_request_without_customer[$j]['chat_created_datee'];
             $datetimediff =   $chat_request_without_customer[$j]['chat_created_date1'];
            }
            $sincedate = $chat_request_without_customer[$j]['chat_created_date1'];
            $datetime1 = new DateTime($datetimediff);
            $datetime2 = new DateTime($sincedate);
            $mindiff = $datetime1->diff($datetime2);
            if ($mindiff->i >= BATCH_MESSAGE_TIMELIMIT || $chat_request_without_customer[$j]['sender_id'] != $chat_request_without_customer[$j - 1]['sender_id'] || $chat_request_without_customer[$j]['shared_user_name'] != $chat_request_without_customer[$j - 1]['shared_user_name']) {
//                echo "value of i".$i."<br/>";
                $datetimediff = $sincedate;
                $timeee = date('h:i A', strtotime($sincedate));
                $chat_request_without_customer[$j]['chat_created_time'] = $timeee;
                $chat_request_without_customer[$j]['profile_picture'] = $this->customfunctions->getprofileimageurl($chat_request_without_customer[$j]['profile_picture']);
                $chat_request_without_customer[$j]['fname'] = $chat_request_without_customer[$j]['first_name'];
                $chat_request_without_customer[$j]['shared_name'] = $chat_request_without_customer[$j]['shared_user_name'];
            }
            if($date != $timediff){
                $timediff = $date;
                $chat_request_without_customer[$j]['chat_created_datee'] = $date;
                if($date == $currentdate){
                    $chat_request_without_customer[$j]['chat_created_datee'] = "Today";
                }
            }

        }
        $role_permission['remove_design_draft'] = $this->myfunctions->admin_role_permissions("remove_design_draft");
        $activities = $this->myfunctions->projectactivities($id,'admin_shown');
        $data = $this->customfunctions->getprojectteammembers($request[0]['customer_id'],$request[0]['designer_id']);
        $publiclink = $this->Request_model->getPubliclinkinfo($id);
        $sharedUser = $this->Request_model->getSharedpeopleinfobyreqID($id); 
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header_1', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number,'breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/view_request', array("data" => $data,"request" => $request, "branddata" => $branddata,"publiclink" => $publiclink,'sharedUser' => $sharedUser,"brand_materials_files" => $brand_materials_files, "request_files" => $request_files, "admin_request_files" => $admin_request_files, "customer_additional_files" => $customer_additional_files, "admin_files" => $admin_files, "chat_request" => $chat_request, "chat_request_without_customer" => $chat_request_without_customer, "edit_profile" => $profile_data, "file_chat_array" => $file_chat_array,"activities" => $activities,"cancel_subscription" => $cancel_suscription,'bckurl'=>$bckurl,"designer_list" => $designer_list,"request_meta" => $request_meta,'sampledata' => $sampledata,"role_permission" => $role_permission));
        $this->load->view('admin/admin_footer');
    }

    public function all_designer() {
        $this->myfunctions->checkloginuser("admin");
        $designers = $this->Admin_model->get_total_customer("designer");

        for ($i = 0; $i < sizeof($designers); $i++) {
            $customer = $this->Admin_model->get_total_customer("customer", $designers[$i]['id']);
            $designer_customer[$designers[$i]['id']] = sizeof($customer);
        }
        //print_r($designers); print_r($designer_customer);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/all_designer', array("designers" => $designers, "designer_customer" => $designer_customer));
        $this->load->view('admin/admin_footer');
    }

    public function view_designer($designer_id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($designer_id == "") {
            redirect(base_url() . "admin/all_designer");
        }
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email'], $designer_id);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture1"]["name"]) && !empty($_FILES["profile_picture1"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture1"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture1")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/view_designer/" . $designer_id);
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "profile_picture" => $_POST['profile_picture'],
                "modified" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->update_data("users", $data, array('id' => $designer_id));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Updated Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            } else {
                $this->session->set_flashdata('message_error', 'Designer Updated Not Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
        }
        $designer = $this->Admin_model->getuser_data($designer_id);

        $designs_queue = $this->Admin_model->get_all_requested_designs("", "", "", "", $designer_id, "status_admin");

        $designs_approved = $this->Admin_model->get_all_requested_designs("approved", "", "", "", $designer_id, "status_admin");

        $customer = $this->Admin_model->get_total_customer("customer", $designer_id);

        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header_1', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_designer', array("designer" => $designer, "designs_queue" => $designs_queue, "customer" => $customer, "designer" => $designer, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function add_designer() {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email']);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/add_designer");
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture"]["name"]) && !empty($_FILES["profile_picture"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/add_designer");
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "new_password" => md5($_POST['password']),
                "profile_picture" => $_POST['profile_picture'],
                "role" => "designer",
                "created" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->insert_data("users", $data);
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Added Not Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/add_designer');
        $this->load->view('admin/admin_footer');
    }

    public function edit_designer($customer_id) {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['phone'] = $_POST['phone'];
            if ($_POST['password'] != "") {
                if ($_POST['password'] == $_POST['confirm_password']) {
                    $customer['new_password'] = md5($_POST['password']);
                    $success = $this->Admin_model->update_data('users', $customer, array('id' => $customer_id));
                    if ($success) {
                        $this->session->set_flashdata('message_success', 'Designer profile edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_designers");
                    } else {
                        $this->session->set_flashdata('message_error', 'Designer profile not edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_designers");
                    }
                } else {
                    $this->session->set_flashdata('message_error', 'Password dose not match', 5);
                    redirect(base_url() . "admin/dashboard/view_designers");
                }
            } else {
                $success = $this->Admin_model->update_data('users', $customer, array('id' => $customer_id));
                if ($success) {
                    $this->session->set_flashdata('message_success', 'Designer profile edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_designers");
                } else {
                    $this->session->set_flashdata('message_error', 'Designer profile not edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_designers");
                }
            }
        }
    }

    public function edit_client($customer_id) {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['phone'] = $_POST['phone'];
            if ($_POST['password'] != "") {
                if ($_POST['password'] == $_POST['confirm_password']) {
                    $customer['new_password'] = md5($_POST['password']);
                    $success = $this->Admin_model->update_data('users', $customer, array('id' => $customer_id));
                    if ($success) {
                        $this->session->set_flashdata('message_success', 'Designer profile edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_clients");
                    } else {
                        $this->session->set_flashdata('message_error', 'Designer profile not edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_clients");
                    }
                } else {
                    $this->session->set_flashdata('message_error', 'Password dose not match', 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                }
            } else {
                $success = $this->Admin_model->update_data('users', $customer, array('id' => $customer_id));
                if ($success) {
                    $this->session->set_flashdata('message_success', 'Designer profile edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                } else {
                    $this->session->set_flashdata('message_error', 'Designer profile not edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                }
            }
        }
    }

    public function gz_delete_files() {
        $this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');
        if (isset($_POST)) {
            $id = $_POST['requests_files'];
            $request_id = $_POST['request_id'];
            $delete = $this->Admin_model->delete_data('request_files', $id);
            
            if ($delete) {

                if (array_key_exists('prefile', $_POST)) {
                    $src_filename = $_POST['srcfile'];
                    $pre_filename = $_POST['prefile'];
                    $thumb_prefile = $_POST['thumb_prefile'];
                    
                    $this->load->library('s3_upload');
                    $this->s3_upload->delete_file_from_s3($src_filename);
                    $this->s3_upload->delete_file_from_s3($pre_filename);
                    $this->s3_upload->delete_file_from_s3($thumb_prefile);
                    $this->myfunctions->capture_project_activity($request_id,$id,'','delete_draft_by_admin','gz_delete_files',$login_user_id,'0','1','1');
                    redirect(base_url() . 'admin/dashboard/view_request/' . $request_id . '/1');

                }
            }
        }
    }

    public function add_customer() {
        $this->myfunctions->checkloginuser("admin");
        $allplan = $this->Stripe->getallsubscriptionlist();

        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }

        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['new_password'] = md5($_POST['password']);

            $customer['current_plan'] = $_POST['subscription_plans_id'];

            $customer['role'] = "customer";


            //$stripe_controller = new StripesController();
            $stripe_customer = $stripe_controller->createCustomer($customer->email);
            if (!$stripe_customer['status']) {

                $cusomer_save = $this->Welcome_model->insert_data("users", $customer);
                //$customer->current_plan = $stripe_customer['data']['customer_id'];
                if ($cusomer_save <= 0) {
                    $this->Flash->error('Customer cannot ne created.');
                    return $this->redirect(Router::url(['action' => 'index'], TRUE));
                }
                $this->Users->delete($customer);
                $this->Flash->error($stripe_customer['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }

            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($this->request->getData('last4'));
            $expiry = explode('/', $this->request->getData('exp_month'));
            $expiry_month = $expiry[0];
            $expiry_year = $expiry[1];
            $cvc = $this->request->getData('cvc');



            $stripe_card = $stripe_controller->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);

            if (!$stripe_card['status']) {
                $this->Users->delete($customer);
                $this->Flash->error($stripe_card['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }

            $subscription_plan_id = $this->request->getData('subscription_plans_id');

            if ($subscription_plan_id) {
                $stripe_subscription = '';
                $stripe_subscription = $stripe_controller->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id);
            }

            //Add Plan to a customer
            if (!$stripe_subscription['status']) {
                $this->Users->delete($customer);
                $this->Flash->error($stripe_subscription['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }


            $customer->current_plan = $stripe_subscription['data']['subscription_id'];

            $customer->customer_id = $stripe_customer['data']['customer_id'];
            $customer->plan_name = $this->request->getData('subscription_plans_id');
            $plandetails = $stripe_controller->retrieveoneplan($this->request->getData('subscription_plans_id'));
            $customer->plan_amount = $plandetails['amount'] / 100;
            $customer->plan_interval = $plandetails['interval'];
            $customer->plan_trial_period_days = $plandetails['trial_period_days'];
            $customer->plan_turn_around_days = $plandetails['metadata']->turn_around_days;

            $is_customer_save = $this->Users->save($customer);
            // $is_customer_save = "1";

            if ($is_customer_save) {
                $email_data = new \stdClass();
                $email_data->to_email = $customer->email;
                $email_data->subject = 'Welcome to Graphics Zoo!';
                $html = "";

                $html .= '<div style="background: #fff;">
                <div style="width:800px;border: 2px solid #f23f5b;margin: auto;border-radius:20px;">
                <div style="text-align:center;margin-bottom: 20px;">
                <img src="' . SITE_IMAGES_URL . 'img/logo.png" height=80px; style="padding: 25px;"></img>
                </div>
                <div style="margin-top:10px;">
                <p style="padding-left: 25px;">Hi ' . $customer->first_name . ', </p>
                </div>
                <div style="margin-top:10px;text-align:justify;">
                <p style="padding: 0px 25px;">We are very excited that you have decided to join us for your design needs. As you get started, expect to have your designs automatically assigned to the best suited designer. you will continue working with that dedicated designer for all your needs. if you have any questions or concerns please fell free to contact us at <u>hello@graphicszoo.com</u> and our team will find a solution for you. We are here to help you with all your needs.</p>
                </div>
                <div style="margin-top:20px;text-align:justify;;padding-bottom:5px;">
                <p style="padding: 0px 25px;">Now, Let\'s go your first design request started. Click below to login and submit a request.</p>
                </div>
                <div style="margin-top:10px;text-align:justify;display: flex;">
                <a href="http://graphicszoo.com/login" style="margin: auto;"><button style="padding:10px;font-size:22px;font-weight:600;color:#fff;background:#f23f5b;border: none;margin: auto;">Request Design</button></a>
                </div>

                <div style="margin-top:10px;text-align:justify;display: flex;">
                <p style="padding-left: 25px;">Thank you,</p>
                </div>

                <div style="text-align:justify;display: flex;">
                <p style="padding-left: 25px;">Andrew Jones</br>Customer Relations</p>
                </div>
                </div>
                </div>';
                $email_data->template = $html;

                $this->sendMail($email_data);
                // exit;
                $plan_amount = 0;

                switch ($subscription_plan_id):
                    case STRIPE_MONTH_SILVER_PLAN_ID:
                    $plan_amount = STRIPE_MONTH_SILVER_PLAN_AMOUNT / 100;
                    break;
                    case STRIPE_MONTH_GOLDEN_PLAN_ID:
                    $plan_amount = STRIPE_MONTH_GOLDEN_PLAN_AMOUNT / 100;
                    break;
                    case STRIPE_YEAR_SILVER_PLAN_ID:
                    $plan_amount = STRIPE_YEAR_SILVER_PLAN_AMOUNT / 100;
                    break;
                    case STRIPE_YEAR_GOLDEN_PLAN_ID:
                    $plan_amount = STRIPE_YEAR_GOLDEN_PLAN_AMOUNT / 100;
                    break;
                    default :
                    break;
                endswitch;

                $this->request->session()->write('plan_amount', $plan_amount);

                $this->Flash->success('User created successfully and subscribed successfully.');

                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }else {
                $this->Flash->error('User cannot be created.');
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/add_customer', array("subscription" => $subscription));
        $this->load->view('admin/admin_footer');
    }

    public function view_clients() {
        $this->myfunctions->checkloginuser("admin");
        $planlist = $this->Stripe->getallsubscriptionlist();
        $va_assignproject = isset($_GET['assign'])?$_GET['assign']:"";
        $countcustomer = $this->Account_model->clientlist_load_more("paid", "", true,"","","","","","",$va_assignproject);
        $activeClients = $this->Account_model->clientlist_load_more("paid", "","","","","","","","",$va_assignproject);
        $isfullaccess = $this->hasAdminfullaccess('full_admin_access','admin');
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',"Clients" => base_url().'admin/dashboard/view_clients');
        for ($i = 0; $i < sizeof($activeClients); $i++) {
            $activeClients[$i]['total_rating'] = $this->Request_model->get_average_rating($activeClients[$i]['id']);
            $activeClients[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($activeClients[$i]['profile_picture']);
            $activeClients[$i]['designer'][0]['profile_picture'] = $this->customfunctions->getprofileimageurl($activeClients[$i]['designer'][0]['profile_picture']);
            $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $activeClients[$i]['id']);
            $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $activeClients[$i]['id']);
            $revision = $this->Account_model->get_all_active_view_request_by_customer(array('pendingrevision'), $activeClients[$i]['id'],true);
            $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $activeClients[$i]['id']);
            $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $activeClients[$i]['id']);
            $hold = $this->Account_model->get_all_active_view_request_by_customer(array('hold'), $activeClients[$i]['id']);
            $cancelled = $this->Account_model->get_all_active_view_request_by_customer(array('cancel'), $activeClients[$i]['id']);
            $user = $this->Account_model->get_all_active_request_by_customer($activeClients[$i]['id']);
            $plan_data = $this->Request_model->getsubscriptionlistbyplanid($activeClients[$i]['plan_name']);
            $activeClients[$i]['created'] = $this->onlytimezone($activeClients[$i]['created'],'M/d/Y');
            if($activeClients[$i]['billing_end_date'] != ''){
                $activeClients[$i]['billing_end_date'] = $this->onlytimezone($activeClients[$i]['billing_end_date'],'M/d/Y');
            }else{
                $activeClients[$i]['billing_end_date'] = 'N/A';
            }
            $activeClients[$i]['active'] = $active;
            $activeClients[$i]['inque_request'] = $inque;
            $activeClients[$i]['revision_request'] = $revision;
            $activeClients[$i]['review_request'] = $review;
            $activeClients[$i]['complete_request'] = $complete;
            $activeClients[$i]['hold'] = $hold;
            $activeClients[$i]['addClass'] = "paid";
            $activeClients[$i]['cancelled'] = $cancelled;
            $activeClients[$i]['total_request'] = ($active + $inque + $revision + $review + $complete + $hold + $cancelled);
            $activeClients[$i]['isfullaccess'] = $isfullaccess;
            $activeClients[$i]['client_plan'] = (isset($plan_data[0]['plan_name']) && $plan_data[0]['plan_name'] != '') ? $plan_data[0]['plan_name'] : 'No Member';
            if ($activeClients[$i]['designer_id'] != 0) {
                $activeClients[$i]['designer'] = $this->Request_model->get_user_by_id($activeClients[$i]['designer_id']);
            } else {
                $activeClients[$i]['designer'] = "";
            }
            $activeClients[$i]['va'] = $this->Request_model->get_user_by_id($activeClients[$i]['va_id']);
            $activeClients[$i]['am'] = $this->Request_model->get_user_by_id($activeClients[$i]['am_id']);
        }
        $this->setsessionforbackbutton("view_vlients","paid"); 
        $counttrailcustomer = $this->Account_model->clientlist_load_more("free", "", true,"","","","","","",$va_assignproject);
        $trailClients = $this->Account_model->clientlist_load_more("free", "","","","","","","","",$va_assignproject);


        $countrequestcustomer = $this->Account_model->clientlist_load_more("", "", true, "", "", "", "", "1","",$va_assignproject);
        $requestClients = $this->Account_model->clientlist_load_more("", "", false, "", "", "", "", "1","",$va_assignproject);


        $countcancelsubscription = $this->Account_model->clientlist_load_more("", "", true, "", "", "", "", "", 1,$va_assignproject);
        $deactivatedusers = $this->Account_model->clientlist_load_more("", "", false, "", "", "", "", "", 1,$va_assignproject);

        $data["all_prt"] = $this->Request_model->get_all_va(3);
        for ($i = 0; $i < sizeof($data['all_prt']); $i++) {
            $data['all_prt'][$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($data['all_prt'][$i]['profile_picture']);
        }
        $data["all_am"] = $this->Request_model->get_all_va(5);
        for ($i = 0; $i < sizeof($data['all_am']); $i++) {
            $data['all_am'][$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($data['all_am'][$i]['profile_picture']);
        }
        $data["all_designers"] = $this->Request_model->get_all_designers(true);
        for ($i = 0; $i < sizeof($data['all_designers']); $i++) {
            $user = $this->Account_model->get_all_active_request_by_designer($data['all_designers'][$i]['id']);
            $data['all_designers'][$i]['active_request'] = sizeof($user);
            $data['all_designers'][$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($data['all_designers'][$i]['profile_picture']);
        }
        $subscription_plan_list = $this->Welcome_model->getsubscriptionlist();
        $role_permissions = array('remove_client_count' => $this->myfunctions->admin_role_permissions("remove_client_count"),
        'remove_download_client_csv' => $this->myfunctions->admin_role_permissions("remove_download_client_csv"));
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs,'edit_profile' => $profile_data));
        $this->load->view('admin/view_clients', array("isfullaccess" => $isfullaccess,"edit_profile" => $profile_data, "activeClients" => $activeClients, "data" => $data, "planlist" => $subscription_plan_list, 'count_customer' => $countcustomer, 'counttrailcustomer' => $counttrailcustomer, 'trailClients' => $trailClients, 'countrequestcustomer' => $countrequestcustomer, 'requestClients' => $requestClients, "countcancelsubscription" => $countcancelsubscription, "deactivatedusers" => $deactivatedusers,"role_permissions" => $role_permissions));
        $this->load->view('admin/admin_footer');
    }

    public function view_designers() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',"Designers" => base_url().'admin/dashboard/view_designers');
        $isfullaccess = $this->hasAdminfullaccess('full_admin_access','admin');
        $qa_assignproject = isset($_GET['assign'])?$_GET['assign']:"";
        $array = array();
        if (!empty($_POST)) {
            if ($_POST['password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);
                redirect(base_url() . "admin/dashboard/view_designers");
            }
            $email_check = $this->Account_model->getuserbyemail($_POST['email'], 'designer');
            if (!empty($email_check)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);

                redirect(base_url() . "admin/dashboard/view_designers");
            }
            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => md5($_POST['password']), "status" => "1", "is_active" => "1", "role" => "designer", "created" => date("Y-m-d H:i:s")));
            $array['about'] = $_POST['about'];
            $array['hobbies'] = $_POST['hobbies'];
            $array['user_id'] = $success;
            $array['created'] = date('Y-m-d H:i:s');
            $array['modified'] = date('Y-m-d H:i:s');
            if ($success) {
                $this->Welcome_model->insert_data("user_skills", $array);
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);
                redirect(base_url() . "admin/dashboard/view_designers");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Not Added Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_designers");
            }
        }
        // $designers = $this->Account_model->getall_designer();
        $designerscount = $this->Account_model->designerlist_load_more("", true,"","","","",$qa_assignproject);
        $designers = $this->Account_model->designerlist_load_more("", "","","","","",$qa_assignproject);
        
        for ($i = 0; $i < sizeof($designers); $i++) {
            $designers[$i]['pending_requests'] = $this->Account_model->getcount_requests_fordesigners($designers[$i]['id'], array("checkforapprove"));
            $designers[$i]['active_requests'] = $this->Account_model->getcount_requests_fordesigners($designers[$i]['id'], array("active", "disapprove"));
            $designers[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($designers[$i]['profile_picture']);
            $designers[$i]['created'] = $this->onlytimezone($designers[$i]['created'],'M/d/Y');
            $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);
            $designers[$i]['qa'] = $this->Request_model->get_user_by_id($designers[$i]['va_id']);
            $designers[$i]['am'] = $this->Request_model->get_user_by_id($designers[$i]['am_id']);
            $designers[$i]['isfullaccess'] = $isfullaccess;
            //echo $designers[$i]['id']."</br>";
        }
        $all_qa = $this->Request_model->get_all_va(4);
        for ($i = 0; $i < sizeof($all_qa); $i++) {
            $all_qa[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($all_qa[$i]['profile_picture']);
        }
        $all_am = $this->Request_model->get_all_va(5);
        for ($i = 0; $i < sizeof($all_am); $i++) {
            $all_am[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($all_am[$i]['profile_picture']);
        }
        $role_permissions = array('remove_active_deactivate_designer' => $this->myfunctions->admin_role_permissions("remove_active_deactivate_designer"));
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs,'edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_designers', array("isfullaccess" => $isfullaccess,"allDesigners" => $designers, 'designerscount' => $designerscount,"role_permissions" => $role_permissions,"all_qa" => $all_qa,"all_am" => $all_am));
        $this->load->view('admin/admin_footer');
    }


    public function reports() {
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',"Reports" => base_url().'admin/dashboard/reports');
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $stateData = $this->Admin_model->getNumberofusersbyState();
        if (isset($_POST['get_reports']) && $_POST['get_reports'] != '') {
            $current_date = isset($_POST['start_date']) ? $_POST['start_date'] : date('Y-m-d');
            //$current_date = date("Y-m-d H:i:s");
            $customercount = $this->Request_model->getCustomercount($current_date);
            $freecustcount = $this->Request_model->getfreecustomercount($current_date);
            $reqcount = $this->Request_model->gettodaysrequestcount($current_date);
            $trailuserreqcount = $this->Request_model->gettrialcustRequestcount();
            $normaluserreqcount = $this->Request_model->getnormalcustRequestcount();
            $trailapprovedreqcount = $this->Request_model->gettrialapprovedrequest();
            $avgreqcount = $this->Request_model->getavgrequestcount();
            $getavgapprovedrequestcount = $this->Request_model->getavgapprovedrequestcount();
            $gettrialdesigncount = $this->Request_model->gettraildesignscount();
        } else {
            $current_date = date('Y-m-d');
            $customercount = $this->Request_model->getCustomercount($current_date);
            $freecustcount = $this->Request_model->getfreecustomercount($current_date);
            $reqcount = $this->Request_model->gettodaysrequestcount($current_date);
            $trailuserreqcount = $this->Request_model->gettrialcustRequestcount();
            $normaluserreqcount = $this->Request_model->getnormalcustRequestcount();
            $trailapprovedreqcount = $this->Request_model->gettrialapprovedrequest();
            $avgreqcount = $this->Request_model->getavgrequestcount();
            $getavgapprovedrequestcount = $this->Request_model->getavgapprovedrequestcount();
            $gettrialdesigncount = $this->Request_model->gettraildesignscount();
            $refund_tax_amount = $this->Admin_model->refund_tax_report();
        }
        $this->load->view('admin/admin_header_1',array('breadcrumbs'=> $breadcrumbs));
        $this->load->view('admin/request/reports', array('profile' => $profile_data,
            'customercount' => $customercount,
            'freecustcount' => $freecustcount,
            'reqcount' => $reqcount,
            'normaluserreqcount' => $normaluserreqcount,
            'trailapprovedreqcount' => $trailapprovedreqcount,
            'avgreqcount' => $avgreqcount,
            'getavgapprovedrequestcount' => $getavgapprovedrequestcount,
            'gettrialdesigncount' => $gettrialdesigncount,
            'stateData' => $stateData,
            'refund_tax_amount' => $refund_tax_amount
        ));
//        $this->load->view('admin/admin_footer');
    }

        public function view_qa() {
        $this->myfunctions->checkloginuser("admin");
        $am_assignproject = isset($_GET['assign'])?$_GET['assign']:"";
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',"Qa/Va" => base_url().'admin/dashboard/view_qa');
        if (!empty($_POST)) {
//            echo "<pre/>";print_R($_POST);exit;
            if ($_POST['password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);
                redirect(base_url() . "admin/dashboard/view_qa");
            }
            $email_check = $this->Account_model->getuserbyemail($_POST['email']);
            if (!empty($email_check)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);
                redirect(base_url() . "admin/dashboard/view_qa");
            }
            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => md5($_POST['password']), "is_active" => 1, "status" => "1", "role" => $_POST['role'],"full_admin_access" => $_POST['full_admin_access'],"about_info" => $_POST['about_info'],"hobbies" => $_POST['hobbies'],"created" => date("Y-m-d H:i:s")));
            if ($success) {
                $this->session->set_flashdata('message_success','QA Added Successfully.!', 5);
                redirect(base_url() . "admin/dashboard/view_qa");
            } else {
                $this->session->set_flashdata('message_error', 'QA Not Added Successfully.!', 5);
                redirect(base_url() . "admin/dashboard/view_qa");
            }
        }
        $countqa = $this->Account_model->qavalist_load_more('admin', true,"","","","",$am_assignproject);
        $qa = $this->Account_model->qavalist_load_more('admin', false,"","","","",$am_assignproject);
        //$qa = $this->Account_model->getqa_member();
        /* new */
        for ($i = 0; $i < sizeof($qa); $i++) {
            $count_active_project = $this->Request_model->countVaProjects(array('active','disapprove'),$qa[$i]['id']);
            $count_pending_project = $this->Request_model->countVaProjects(array("pendingforapprove","checkforapprove"),$qa[$i]['id']);
            $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
            $qa[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($qa[$i]['profile_picture']);
            $qa[$i]['created'] = $this->onlytimezone($qa[$i]['created'],'M/d/Y');
            $qa[$i]['count_active_project'] = $count_active_project;
            $qa[$i]['user_role'] = $this->getadminuserrole($qa[$i]['full_admin_access']);
            $qa[$i]['count_pending_project'] = $count_pending_project;
            $qa[$i]['total_designer'] = sizeof($designers);
            $qa[$i]['am'] = $this->Request_model->get_user_by_id($qa[$i]['am_id']);
            $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);
            if ($mydesigner) {
                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                $qa[$i]['total_clients'] = sizeof($data);
            } else {
                $qa[$i]['total_clients'] = 0;
            }
        }
        $isfullaccess = $this->hasAdminfullaccess('full_admin_access','admin');
        $all_am = $this->Request_model->get_all_va(5);
        for ($i = 0; $i < sizeof($all_am); $i++) {
            $all_am[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($all_am[$i]['profile_picture']);
        }
        $role_permissions = array('remove_active_deactivate__qa' => $this->myfunctions->admin_role_permissions("remove_active_deactivate__qa"));
        $countva = $this->Account_model->qavalist_load_more('va', true);
        $va = $this->Account_model->qavalist_load_more('va', false);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs,'edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_qa', array("allQa" => $qa, "allVa" => $va, "countqa" => $countqa, "countva" => $countva,"role_permissions" => $role_permissions,"isfullaccess" => $isfullaccess,"all_am"=> $all_am));
        $this->load->view('admin/admin_footer');
    }

    public function send_message_request() {
        $this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');
        $sender_type = $_POST['sender_type'];
        if ($sender_type == 'designer') {
            $_POST['designer_seen'] = '1';
        } elseif ($sender_type == 'customer') {
            $_POST['customer_seen'] = '1';
        } elseif ($sender_type == 'admin') {
            $_POST['admin_seen'] = '1';
        }
        $designer_id = $_POST['designer_id'];
        unset($_POST['designer_id']);
        $_POST['created'] = date("Y-m-d H:i:s");
        $success = $this->Welcome_model->insert_data("request_discussions", $_POST);
        if ($success) {
            $rtitle = $this->Request_model->get_request_by_id($_POST['request_id']);
            $qa = $this->Admin_model->getuser_data($designer_id);
            if (!empty($rtitle)) {
                /** *****insert messages notifications****** */
                if ($_POST['with_or_not_customer'] == "1") {
                    /* Message notification for customer */
                    $this->myfunctions->show_messages_notifications($rtitle[0]['title'], $_POST['message'], $_POST['request_id'], $login_user_id, "customer/request/project_info/" . $_POST['request_id'], $_POST['reciever_id']);
                }
                /* Message notification for qa */
                $this->myfunctions->show_messages_notifications($rtitle[0]['title'], $_POST['message'], $_POST['request_id'], $login_user_id, "designer/request/project_info/" . $_POST['request_id'], $designer_id);
                /* Message notification for admin */
                $this->myfunctions->show_messages_notifications($rtitle[0]['title'], $_POST['message'], $_POST['request_id'], $login_user_id, "qa/dashboard/view_project/" . $_POST['request_id'], $qa[0]['qa_id']);

                /*                 * *****end insert messages notifications****** */
            } else {
                $data['title'] = "New Message Arrived";
                $data['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("notification", $data);
            }
            $this->Request_model->get_notifications($_SESSION['user_id']);
        } else {}
        echo $success;exit;
    }

    public function send_message() {
        $this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');
        $request_files = $this->Admin_model->get_request_files_byid($_POST['request_file_id']);
        $request_data = $this->Request_model->get_request_by_id($request_files[0]['request_id']);
        $_POST['created'] = date("Y-m-d H:i:s");
        if ($this->Welcome_model->insert_data("request_file_chat", $_POST)) {
            echo $this->db->insert_id();
        } else {
            echo '0';
        }

        /*         * *****insert messages notifications****** */
        $userdata = $this->Admin_model->getuser_data($_POST['receiver_id']);
        if ($request_files[0]['status'] != 'pending' && $request_files[0]['status'] != 'Reject') {
            $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_files[0]['request_id'], $login_user_id, "customer/request/project_image_view/" . $_POST['request_file_id'] . "?id=" . $request_files[0]['request_id'], $_POST['receiver_id']);
        }
        /* Message notification for qa */
        $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_files[0]['request_id'], $login_user_id, "qa/dashboard/view_files/" . $_POST['request_file_id'] . "?id=" . $request_files[0]['request_id'], $userdata[0]['qa_id']);
        /* Message notification for designer */
        $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_files[0]['request_id'], $login_user_id, "designer/request/project_image_view/" . $_POST['request_file_id'] . "?id=" . $request_files[0]['request_id'], $userdata[0]['designer_id']);

        /*         * *****end insert messages notifications****** */

        exit;
    }

    public function load_messages_from_all_user() {
        $this->myfunctions->checkloginuser("admin");
        $project_id = $_POST['project_id'];
        $customer_id = $_POST['cst_id'];
        $designer_id = $_POST['desg_id'];
        $msg_seen = $_POST['admin_seen'];
        $chat_request_msg = $this->Request_model->get_admin_unseen_msg($project_id, $customer_id, $designer_id, $msg_seen);
        if (!empty($chat_request_msg)) {
            $message['message'] = $chat_request_msg[0]['message'];
            $message['profile_picture'] = isset($chat_request_msg[0]['profile_picture']) ? $chat_request_msg[0]['profile_picture'] : 'user-admin.png';
            $message['typee'] = $chat_request_msg[0]['sender_type'];
            $message['with_or_not'] = $chat_request_msg[0]['with_or_not_customer'];
            $message['msg_first_name'] = $chat_request_msg[0]['first_name'];
            echo json_encode($message);
            $this->Admin_model->update_data("request_discussions", array("admin_seen" => 1), array("request_id" => $project_id));
        } else {
            // $message = "Hello Everyone";
            // echo $message;
        }
    }

    public function view_files($id) {
        $reqid = isset($_GET['id'])?$_GET['id']:'';
        $this->myfunctions->checkloginuser("admin");
        if ($id == "") {
            redirect(base_url() . "admin/dashboard");
        }
        $feedBack = $this->Request_model->get_reviewfeedback('',"customer",$reqid);
//        echo "<pre/>";print_R($feedBack);
        $trail_cust = $this->Request_model->get_user_by_requestid($_GET['id']);
        $login_user_id = $this->load->get_var('login_user_id');
        $is_trail = $trail_cust[0]['is_trial'];
        $can_trialdownload = $trail_cust[0]['can_trialdownload'];
        $this->Welcome_model->update_data("request_files", array("admin_seen" => 1), array('id' => $id));
//        $this->Admin_model->update_data("request_file_chat", array("admin_seen" => '1'), array("request_file_id" => $id));
        $url = "admin/dashboard/view_files/" . $id . "?id=" . $_GET['id'];
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $login_user_id, "url" => $url));
        if (!empty($_POST)) {
            $success = $this->Welcome_model->update_data("request_files", array("grade" => $_POST['grade']), array('id' => $_POST['id']));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Grade is Submitted Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_files/" . $id . "?id=" . $_GET['id']);
            } else {
                $this->session->set_flashdata('message_error', 'Grade is Not Submitted Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_files/" . $id . "?id=" . $_GET['id']);
            }
        }

        $designer_file = $this->Admin_model->get_requested_files($_GET['id'], "", "designer");
        $data['request'] = $this->Request_model->get_request_by_id($_GET['id']);
        $data['request_meta'] = $this->Category_model->get_quest_Ans($_GET['id']);
        $data['main_id'] = $id;

        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], 'DESC');

            $designer_file[$i]['modified_date'] = $this->onlytimezone($designer_file[$i]['modified']);
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified_date'];

            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
                $designer_file[$i]['chat'][$j]['replies'] = $this->Request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
                $designer_file[$i]['chat'][$j]['profile_picture'] = $this->customfunctions->getprofileimageurl($designer_file[$i]['chat'][$j]['profile_picture']);
                $designer_file[$i]['chat'][$j]['created'] = $this->onlytimezone($designer_file[$i]['chat'][$j]['created']);
                    for ($k = 0; $k < count($designer_file[$i]['chat'][$j]['replies']); $k++) {
                        $designer_file[$i]['chat'][$j]['replies'][$k]['profile_picture'] = $this->customfunctions->getprofileimageurl($designer_file[$i]['chat'][$j]['replies'][$k]['profile_picture']);
                    }
            }
        }
        $alldraftpubliclinks = $this->Request_model->getallPubliclinkinfo($reqid);
        $sharedreqUser = $this->Request_model->getSharedpeopleinfobyreq_draftID($reqid,0);
        $shareddraftUser = $this->Request_model->getSharedpeopleinfobyreq_draftID($reqid,$id);
        $dataa = $this->Request_model->get_request_by_id($_GET['id']);
//        $designer = $this->Account_model->getuserbyid($dataa[0]['designer_id']);
//        $customer = $this->Account_model->getuserbyid($dataa[0]['customer_id']);
//        $qa = $this->Account_model->getuserbyid($customer[0]['qa_id']);
        $data['data'] = $this->customfunctions->getprojectteammembers($dataa[0]['customer_id'],$dataa[0]['designer_id']);
        $data['user'] = $this->Account_model->getuserbyid($_SESSION['user_id']);
//        $data['user'][0]['designer_image'] = $this->customfunctions->getprofileimageurl($data['user'][0]['designer_image']);
//        $data['user'][0]['customer_image'] = $this->customfunctions->getprofileimageurl($data['user'][0]['customer_image']);
//        $data['user'][0]['qa_image'] = $this->customfunctions->getprofileimageurl($data['user'][0]['qa_image']);
//        if (!empty($designer)) {
//            $data['data'][0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
//            $data['data'][0]['title'] = $dataa[0]['title'];
//            $data['data'][0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
//        }
//        if (!empty($customer)) {
//            $data['data'][0]['customer_name'] = $customer[0]['first_name'] . " " . $designer[0]['last_name'];
//            $data['data'][0]['customer_sortname'] = substr($customer[0]['first_name'], 0, 1) . substr($customer[0]['last_name'], 0, 1);
//        }
//        if (!empty($qa)) {
//            $data['data'][0]['qa_name'] = $qa[0]['first_name'] . " " . $qa[0]['last_name'];
//            $data['data'][0]['qa_sortname'] = substr($qa[0]['first_name'], 0, 1) . substr($qa[0]['last_name'], 0, 1);
//        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $data['chat_request'] = $this->Request_model->get_chat_request_by_id($_GET['id']);
        $data['designer_file'] = $designer_file;
        $data['alldraftpubliclinks'] = $alldraftpubliclinks;
        $data['sharedreqUser'] = $sharedreqUser;
        $data['shareddraftUser'] = $shareddraftUser;
        $data['draftrequest_id'] = $reqid;
        $data['feedBack'] = $feedBack;
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, 'is_trail' => $is_trail, 'can_trialdownload' => $can_trialdownload, 'client_id' => $trail_cust[0]['customer_id'], "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_files', $data);
        $this->load->view('admin/admin_footer');
    }

    public function client_projects($id = null) {
        if($id){
            redirect(base_url() . 'admin/dashboard?client_id='.$id);
        }
        $login_user_id = $this->load->get_var('login_user_id');
        $titlestatus = "view_clients";
        $this->myfunctions->checkloginuser("admin");
        if ($id == "") {
            redirect(base_url() . "admin/dashboard");
        }
        $data['userdata'] = $this->Account_model->getuserbyid($id);

        if (empty($data['userdata'])) {
            redirect(base_url() . "admin/dashboard");
        }

        $count_active_project = $this->Account_model->customer_projects_load_more(array('active', 'disapprove', 'assign', 'pendingrevision'), $id, true);
        $active_project = $this->Account_model->customer_projects_load_more(array('active', 'disapprove', 'assign', 'pendingrevision'), $id, false, '', '', '');

        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['priority'] = $active_project[$i]['priority'];
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "admin");
            $active_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($active_project[$i]['id'], $_SESSION['user_id'], "admin");
            $active_project[$i]['total_files'] = $this->Request_model->get_files_count($active_project[$i]['id'], $active_project[$i]['designer_id'], "admin");
            $active_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($active_project[$i]['id']);
            $getfileid = $this->Request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                if ($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL) {
                    $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update'], $active_project[$i]['current_plan_name']);
                } else {
                    $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                }
            }
            $active_project[$i]['comment_count'] = $commentcount;

            $designer = $this->Request_model->getuserbyid($active_project[$i]['designer_id']);
            $customer = $this->Request_model->getuserbyid($active_project[$i]['customer_id']);
            if (!empty($designer)) {
                $active_project[$i]['designer_first_name'] = $designer[0]['first_name'];
                $active_project[$i]['designer_last_name'] = $designer[0]['last_name'];
                $active_project[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $active_project[$i]['designer_first_name'] = "";
                $active_project[$i]['designer_last_name'] = "";
                $active_project[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $active_project[$i]['customer_first_name'] = $customer[0]['first_name'];
                $active_project[$i]['customer_last_name'] = $customer[0]['last_name'];
                $active_project[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $active_project[$i]['customer_first_name'] = "";
                $active_project[$i]['customer_last_name'] = "";
                $active_project[$i]['plan_turn_around_days'] = 0;
            }
        }

        $count_check_approve_project = $this->Account_model->customer_projects_load_more(array('checkforapprove', 'pendingrevision'), $id, true);
        $check_approve_project = $this->Account_model->customer_projects_load_more(array('checkforapprove', 'pendingrevision'), $id);

        for ($i = 0; $i < sizeof($check_approve_project); $i++) {
            $check_approve_project[$i]['total_chat'] = $this->Request_model->get_chat_number($check_approve_project[$i]['id'], $check_approve_project[$i]['customer_id'], $check_approve_project[$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($check_approve_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $check_approve_project[$i]['comment_count'] = $commentcount;
            $check_approve_project[$i]['modified'] = $this->onlytimezone($check_approve_project[$i]['modified']);
            $check_approve_project[$i]['deliverydate'] = $this->onlytimezone($check_approve_project[$i]['latest_update']);
            $check_approve_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($check_approve_project[$i]['id'], $_SESSION['user_id'], "admin");
            $check_approve_project[$i]['total_files'] = $this->Request_model->get_files_count($check_approve_project[$i]['id'], $check_approve_project[$i]['designer_id'], "admin");
            $check_approve_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($check_approve_project[$i]['id']);
        }
        $count_complete_project = $this->Account_model->customer_projects_load_more(array('approved'), $id, true);
        $complete_project = $this->Account_model->customer_projects_load_more(array('approved'), $id);

        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($complete_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $complete_project[$i]['comment_count'] = $commentcount;
            $complete_project[$i]['approvddate'] = $this->onlytimezone($complete_project[$i]['approvaldate']);
            $complete_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($complete_project[$i]['id'], $_SESSION['user_id'], "admin");
            $complete_project[$i]['total_files'] = $this->Request_model->get_files_count($complete_project[$i]['id'], $complete_project[$i]['designer_id'], "admin");
            $complete_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($complete_project[$i]['id']);
        }

        /*         * ******** hold projects of client ************ */
        $count_hold_project = $this->Account_model->customer_projects_load_more(array('hold'), $id, true);
        $hold_project = $this->Account_model->customer_projects_load_more(array('hold'), $id);
        for ($i = 0; $i < sizeof($hold_project); $i++) {
            $hold_project[$i]['total_chat'] = $this->Request_model->get_chat_number($hold_project[$i]['id'], $hold_project[$i]['customer_id'], $hold_project[$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($hold_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $hold_project[$i]['comment_count'] = $commentcount;
            if ($hold_project[$i]['modified']) {
                $hold_project[$i]['modified'] = $this->onlytimezone($hold_project[$i]['modified']);
            } else {
                $hold_project[$i]['modified'] = $this->onlytimezone($hold_project[$i]['created']);
            }
            $hold_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($hold_project[$i]['id'], $login_user_id, "admin");
            $hold_project[$i]['total_files'] = $this->Request_model->get_files_count($hold_project[$i]['id'], $hold_project[$i]['designer_id'], "admin");
            $hold_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($hold_project[$i]['id']);
        }

        /*         * ******** cancel projects of client ************ */
        $count_cancel_project = $this->Account_model->customer_projects_load_more(array('cancel'), $id, true);
        $cancel_project = $this->Account_model->customer_projects_load_more(array('cancel'), $id);

        for ($i = 0; $i < sizeof($cancel_project); $i++) {
            $cancel_project[$i]['total_chat'] = $this->Request_model->get_chat_number($cancel_project[$i]['id'], $cancel_project[$i]['customer_id'], $cancel_project[$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($cancel_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($cancel_project[$i]['modified']) {
                $cancel_project[$i]['modified'] = $this->onlytimezone($cancel_project[$i]['modified']);
            } else {
                $cancel_project[$i]['modified'] = $this->onlytimezone($cancel_project[$i]['created']);
            }
            $cancel_project[$i]['comment_count'] = $commentcount;
            $cancel_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($cancel_project[$i]['id'], $login_user_id, "admin");
            $cancel_project[$i]['total_files'] = $this->Request_model->get_files_count($cancel_project[$i]['id'], $cancel_project[$i]['designer_id'], "admin");
            $cancel_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($cancel_project[$i]['id']);
        }
        $user_id = $login_user_id;
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($user_id);
        $notification_number = $this->Request_model->get_notifications_number($user_id);
        $messagenotifications = $this->Request_model->get_messagenotifications($user_id);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($user_id);
        $designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $this->load->view('admin/admin_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, 'titlestatus' => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('admin/client_projects', array("custid" => $id, "count_active_project" => $count_active_project, "active_project" => $active_project, "count_complete_project" => $count_complete_project, "complete_project" => $complete_project, "pending_project" => $pending_project, "disapprove_project" => $disapprove_project, "count_check_approve_project" => $count_check_approve_project, "check_approve_project" => $check_approve_project, "userdata" => $data['userdata'], "designer_list" => $designer_list, "count_hold_project" => $count_hold_project, "hold_project" => $hold_project, "count_cancel_project" => $count_cancel_project, "cancel_project" => $cancel_project));
        $this->load->view('admin/admin_footer');
    }

    public function change_current_plan() {
        $this->myfunctions->checkloginuser("admin");
//        echo "<pre/>";print_R($_POST);exit;
        $user_data = $this->Admin_model->getuser_data($_POST['plan_customer_id']);
        $customer_id = $_POST['plan_customer_id'];
        $current_plan_price = isset($_POST['plan_price']) ? $_POST['plan_price'] : '';
        $current_plan_name = $user_data[0]['plan_name'];
        $next_plan_name = $user_data[0]['next_plan_name'];
        $subscription_plan_id = $_POST['plan_name'];
        if ($user_data[0]['invoice'] == '' || $user_data[0]['invoice'] == NULL || $user_data[0]['invoice'] == 1) {
            $current_plan_weight = $this->Request_model->getsubscriptionlistbyplanid($current_plan_name);
        } elseif ($user_data[0]['invoice'] == 0) {
            $current_plan_weight = $this->Request_model->getsubscriptionlistbyplanid($next_plan_name);
        }
        $post_plan_weight = $this->Request_model->getsubscriptionlistbyplanid($subscription_plan_id);
        if ($current_plan_weight[0]['plan_weight'] <= $post_plan_weight[0]['plan_weight']) {
            $invoice = 1;
        } else {
            $invoice = 0;
        }
        $inprogress_request = $_POST['in_progress_request'];
        if ($subscription_plan_id == SUBSCRIPTION_99_PLAN) {
            $current_date = date("Y:m:d H:i:s");
            $monthly_date = strtotime("+1 months", strtotime($current_date)); // returns timestamp
            $billing_expire = date('Y:m:d H:i:s', $monthly_date);
            $customer['billing_cycle_request'] = 3;
        } else {
            $customer['billing_cycle_request'] = 0;
        }
       

        $updateuserplan = $this->Stripe->updateuserplan($user_data[0]['customer_id'], $_POST['plan_name'], $inprogress_request, $invoice, strtotime($user_data[0]['billing_end_date']));
        //echo $updateuserplan[0].'<br>'.$updateuserplan[1];exit;
        $subscription = $updateuserplan['data']['subscriptions'];
        $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
        $customer['invoice'] = $invoice;
        $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
        //if invoice created
        if ($invoice == 1) {
            $invoice_created = $this->Stripe->create_invoice($user_data[0]['customer_id']);
            $customer['plan_name'] = $subscription_plan_id;
            $customer['current_plan'] = $subscription['data'][0]->id;
            $customer['billing_start_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_start);
            $customer['billing_end_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_end);
            $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
            $customer['display_plan_name'] = $plandetails['name'];
            $customer['total_inprogress_req'] = $inprogress_request;
            $customer['total_active_req'] = $inprogress_request * TOTAL_ACTIVE_REQUEST;
            $customer['plan_amount'] = $current_plan_price;
            $customer['plan_interval'] = $plandetails['interval'];
            $customer['invoice'] = 1;
        }
        //if invoice not created
        if ($invoice == 0) {
            $customer['next_plan_name'] = $subscription_plan_id;
            $customer['next_current_plan'] = $subscription['data'][0]->id;
            $customer['invoice'] = 0;
        }
        $customer['modified'] = date("Y:m:d H:i:s");
        if ($updateuserplan[0] != 'success') {
            $this->session->set_flashdata('message_error', $updateuserplan['1'], 5);
            redirect(base_url() . "admin/accounts/view_client_profile/" . $_POST['plan_customer_id']);
        } else {
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $customer_id));
            if ($id) {
                $this->session->set_flashdata('message_success', $updateuserplan['1'], 5);
                redirect(base_url() . "admin/accounts/view_client_profile/" . $_POST['plan_customer_id']);
            }
        }
    }

    public function brand_profile($id = null) {
        //echo $id;
        $this->myfunctions->checkloginuser("admin");
        $brand_profiles = $this->Request_model->get_brand_profile_by_user_id($id);
        for ($i = 0; $i < sizeof($brand_profiles); $i++) {
            $materials_files = $this->Request_model->get_brandprofile_materials_files($brand_profiles[$i]['id']);
            //echo "<pre>";print_r($materials_files);
        }
        //echo "<pre>";print_r($brand_profiles);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $this->load->view('admin/admin_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, 'titlestatus' => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('admin/brand_profile', array("custid" => $id, "brand_profiles" => $brand_profiles, "materials_files" => $materials_files));
        $this->load->view('admin/admin_footer');
    }

    public function brand_materials($id = null) {
        //echo $id;
        $this->myfunctions->checkloginuser("admin");
        $materials_files = $this->Request_model->get_brandprofile_materials_files($id);
        //echo "<pre>";print_r($materials_files);
        //echo "<pre>";print_r($brand_profiles);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $this->load->view('admin/admin_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, 'titlestatus' => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('admin/brand_materials', array("brand_id" => $id, "materials_files" => $materials_files));
        $this->load->view('admin/admin_footer');
    }

    public function logout() {
        $this->myfunctions->checkloginuser("admin");
        $this->Welcome_model->update_data("users", array("online" => 0), array("id" => $_SESSION['user_id']));
        $this->Request_model->logout();
        redirect(base_url());
    }

    public function active_view_designer($id) {
        $this->myfunctions->checkloginuser("admin");
        if($id){
         redirect(base_url() . 'admin/dashboard?designer_id='.$id);
        }
        $login_user_id = $this->load->get_var('login_user_id');
        $titlestatus = 'view_designer';
        $sql = "select * from users where designer_id= '" . $id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $designers = $this->Account_model->getuserbyid($id);
        $skills = $this->Request_model->getallskills($id);
        $datadesigner['activeproject'] = array();
        $datadesigner['inqueueproject'] = array();
        $datadesigner['pendingreviewproject'] = array();
        $datadesigner['pendingproject'] = array();
        $datadesigner['completeproject'] = array();
        // Active Designer Project Section Start Here
        $datadesigner['count_active_project'] = $this->Account_model->designer_projects_load_more(array('active', 'disapprove'), $id, true);
        $datadesigner['activeproject'] = $this->Account_model->designer_projects_load_more(array('active', 'disapprove'), $id, false, '', '', '');

        for ($i = 0; $i < sizeof($datadesigner['activeproject']); $i++) {

            $datadesigner['activeproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['activeproject'][$i]['id'], $datadesigner['activeproject'][$i]['customer_id'], $datadesigner['activeproject'][$i]['designer_id'], "designer");
            $datadesigner['activeproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['activeproject'][$i]['id'], $_SESSION['user_id'], "admin");
            if ($datadesigner['activeproject'][$i]['expected_date'] == '' || $datadesigner['activeproject'][$i]['expected_date'] == NULL) {
                $datadesigner['activeproject'][$i]['expected'] = $this->myfunctions->check_timezone($datadesigner['activeproject'][$i]['latest_update'], $datadesigner['activeproject'][$i]['current_plan_name']);
            } else {
                $datadesigner['activeproject'][$i]['expected'] = $this->onlytimezone($datadesigner['activeproject'][$i]['expected_date']);
            }
            $datadesigner['activeproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['activeproject'][$i]['id'], $datadesigner['activeproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['activeproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['activeproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['activeproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['activeproject'][$i]['id']);
        }

        // Active Designer Project Section End Here
        // In-Queue Designer Project Section Start Here
        $datadesigner['count_inqueueproject'] = $this->Account_model->designer_projects_load_more(array('assign', 'pending'), $id, true);
        $datadesigner['inqueueproject'] = $this->Account_model->designer_projects_load_more(array('assign', 'pending'), $id, false, '', '', '');

        for ($i = 0; $i < sizeof($datadesigner['inqueueproject']); $i++) {
            $datadesigner['inqueueproject'][$i]['priority'] = $datadesigner['inqueueproject'][$i]['priority'];
            $datadesigner['inqueueproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['inqueueproject'][$i]['id'], $datadesigner['inqueueproject'][$i]['customer_id'], $datadesigner['inqueueproject'][$i]['designer_id'], "designer");
            $datadesigner['inqueueproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['inqueueproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['inqueueproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['inqueueproject'][$i]['id'], $datadesigner['inqueueproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['inqueueproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['inqueueproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['inqueueproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['inqueueproject'][$i]['id']);
        }
        // In-Queue Designer Project Section End Here
        // Pending Review Designer Project Section Start Here
        $datadesigner['count_pendingreviewproject'] = $this->Account_model->designer_projects_load_more(array('pendingrevision'), $id, true);
        $datadesigner['pendingreviewproject'] = $this->Account_model->designer_projects_load_more(array('pendingrevision'), $id, false, '', '', '');

        for ($i = 0; $i < sizeof($datadesigner['pendingreviewproject']); $i++) {
            $datadesigner['pendingreviewproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['pendingreviewproject'][$i]['id'], $datadesigner['pendingreviewproject'][$i]['customer_id'], $datadesigner['pendingreviewproject'][$i]['designer_id'], "designer");
            $datadesigner['pendingreviewproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['pendingreviewproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['pendingreviewproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['pendingreviewproject'][$i]['id'], $datadesigner['pendingreviewproject'][$i]['designer_id'], 'admin');
            $datadesigner['pendingreviewproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['pendingreviewproject'][$i]['id'], $datadesigner['pendingreviewproject'][$i]['designer_id'], "admin");
            $datadesigner['pendingreviewproject'][$i]['revisiondate'] = $this->onlytimezone($datadesigner['pendingreviewproject'][$i]['modified']);
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['pendingreviewproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['pendingreviewproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['pendingreviewproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['pendingreviewproject'][$i]['id']);
        }
        // Pending Review Designer Project Section End Here
        // Pending Approval Designer Project Section Start Here
        $datadesigner['count_pendingproject'] = $this->Account_model->designer_projects_load_more(array('checkforapprove'), $id, true);
        $datadesigner['pendingproject'] = $this->Account_model->designer_projects_load_more(array('checkforapprove'), $id, false, '', '', '');

        for ($i = 0; $i < sizeof($datadesigner['pendingproject']); $i++) {
            $datadesigner['pendingproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['pendingproject'][$i]['id'], $datadesigner['pendingproject'][$i]['customer_id'], $datadesigner['pendingproject'][$i]['designer_id'], "designer");
            $datadesigner['pendingproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['pendingproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['pendingproject'][$i]['reviewdate'] = $this->onlytimezone($datadesigner['pendingproject'][$i]['modified']);
            $datadesigner['pendingproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['pendingproject'][$i]['id'], $datadesigner['pendingproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['pendingproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['pendingproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['pendingproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['pendingproject'][$i]['id']);
        }
        // Pending Approval Designer Project Section End Here
        // Complete Designer Project Section Start Here
        $datadesigner['count_completeproject'] = $this->Account_model->designer_projects_load_more(array('approved'), $id, true);
        $datadesigner['completeproject'] = $this->Account_model->designer_projects_load_more(array('approved'), $id, false, '', '', '');
        for ($i = 0; $i < sizeof($datadesigner['completeproject']); $i++) {
            $datadesigner['completeproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['completeproject'][$i]['id'], $datadesigner['completeproject'][$i]['customer_id'], $datadesigner['completeproject'][$i]['designer_id'], "designer");
            $datadesigner['completeproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['completeproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['completeproject'][$i]['approvedate'] = $this->onlytimezone($datadesigner['completeproject'][$i]['approvaldate']);
            $datadesigner['completeproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['completeproject'][$i]['id'], $datadesigner['completeproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['completeproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['completeproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['completeproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['completeproject'][$i]['id']);
        }
        // Complete Designer Project Section End Here
        // Hold Designer Project Section Start Here
        $datadesigner['count_holdproject'] = $this->Account_model->designer_projects_load_more(array('hold'), $id, true);
        $datadesigner['holdproject'] = $this->Account_model->designer_projects_load_more(array('hold'), $id, false, '', '', '');

        for ($i = 0; $i < sizeof($datadesigner['holdproject']); $i++) {
            $datadesigner['holdproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['holdproject'][$i]['id'], $datadesigner['holdproject'][$i]['customer_id'], $datadesigner['holdproject'][$i]['designer_id'], "designer");
            $datadesigner['holdproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['holdproject'][$i]['id'], $_SESSION['user_id'], "admin");
            if ($datadesigner['holdproject'][$i]['modified']) {
                $datadesigner['holdproject'][$i]['modified'] = $this->onlytimezone($datadesigner['holdproject'][$i]['modified']);
            } else {
                $datadesigner['holdproject'][$i]['modified'] = $this->onlytimezone($datadesigner['holdproject'][$i]['created']);
            }
            $datadesigner['holdproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['holdproject'][$i]['id'], $datadesigner['holdproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['holdproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['holdproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['holdproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['holdproject'][$i]['id']);
        }
        // Hold Designer Project Section End Here
        // Cancel Designer Project Section Start Here
        $datadesigner['count_cancelproject'] = $this->Account_model->designer_projects_load_more(array('cancel'), $id, true);
        $datadesigner['cancelproject'] = $this->Account_model->designer_projects_load_more(array('cancel'), $id, false, '', '', '');

        for ($i = 0; $i < sizeof($datadesigner['cancelproject']); $i++) {
            $datadesigner['cancelproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['cancelproject'][$i]['id'], $datadesigner['cancelproject'][$i]['customer_id'], $datadesigner['cancelproject'][$i]['designer_id'], "designer");
            $datadesigner['cancelproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['cancelproject'][$i]['id'], $_SESSION['user_id'], "admin");
            if ($datadesigner['cancelproject'][$i]['modified']) {
                $datadesigner['cancelproject'][$i]['modified'] = $this->onlytimezone($datadesigner['cancelproject'][$i]['modified']);
            } else {
                $datadesigner['cancelproject'][$i]['modified'] = $this->onlytimezone($datadesigner['cancelproject'][$i]['created']);
            }
            $datadesigner['cancelproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['cancelproject'][$i]['id'], $datadesigner['holdproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['cancelproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['cancelproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['cancelproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['cancelproject'][$i]['id']);
        }
        // Cancel Designer Project Section End Here

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $alldesigners = $this->Request_model->get_all_designer_for_qa($_SESSION['user_id']);
        for ($i = 0; $i < sizeof($alldesigners); $i++) {

            $user = $this->Account_model->get_all_active_request_by_designer($alldesigners[$i]['id']);
            $alldesigners[$i]['active_request'] = sizeof($user);
        }
        if ($_POST) {
            $this->Admin_model->assign_designer_by_qa($_POST['assign_designer'], $_POST['request_id']);
            $this->myfunctions->capture_project_activity($_POST['request_id'],'','','assign_designer','active_view_designer',$login_user_id);
            redirect(base_url() . "admin/dashboard/active_view_designer/" . $_POST['assign_designer']);
        }
        $this->load->view('admin/admin_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "titlestatus" => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('admin/active_view_designer', array("data" => $result, "designers" => $designers, 'datadesigner' => $datadesigner, 'alldesigners' => $alldesigners, 'skills' => $skills));
        $this->load->view('admin/admin_footer');
    }

    public function search_ajax_designer_project() {
        $this->myfunctions->checkloginuser("admin");
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $datauserid = $this->input->get('id');
        $dataStatus = explode(",", $dataStatus);
        $rows = $this->Request_model->GetAutocomplete_designer(array('keyword' => $dataArray), $dataStatus, $datauserid, "designer");
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "designer");
            $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "qa");
            $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, 'admin');
            $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $rows[$i]->comment_count = $commentcount;
            if ($rows[$i]->status == "active" || $rows[$i]->status == "disapprove") {
                if ($rows[$i]->expected_date == '' || $rows[$i]->expected_date == NULL) {
                    $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update, $rows[$i]->current_plan_name);
                } else {
                    $rows[$i]->expected = $this->onlytimezone($rows[$i]->expected_date);
                }
            } elseif ($rows[$i]->status == "checkforapprove") {
                $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->latest_update);
            } elseif ($rows[$i]->status == "approved") {
                $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
            }
            if ($rows[$i]->status_admin == "pendingrevision") {
                $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
            }
            $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            if ($rows[$i]->status_designer == "disapprove" || $rows[$i]->status_designer == "active") {
                ?>
                <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            echo "Expected on";
                                        } elseif ($rows[$i]->status_admin == "disapprove") {
                                            echo "Expected on";
                                        }
                                        ?>
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                        } elseif ($rows[$i]->status_admin == "disapprove") {
                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 48%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">

                                            <div class="cell-col" >
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                                <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                            </div>

                                            <div class="cell-col col-w1">
                                                <p class="neft text-center">
                                                    <?php if ($rows[$i]->status_admin == "active") { ?>
                                                        <span class="green text-uppercase text-wrap">In-Progress</span>
                                                    <?php } elseif ($rows[$i]->status_admin == "disapprove" && $rows[$i]->who_reject == 1) { ?>
                                                        <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                                    <?php } elseif ($rows[$i]->status_admin == "disapprove" && $rows[$i]->who_reject == 0) { ?>
                                                        <span class="red text-uppercase text-wrap">Quality Revision</span>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 20%;">
                                    <div class="cli-ent-xbox text-left p-left1">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                                    <figure class="pro-circle-img">
                                                        <?php if ($rows[$i]->profile_picture != "") { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                        <?php } else { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                        <?php } ?>
                                                    </figure>
                                                </a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                    <?php echo $rows[$i]->designer_first_name; ?>
                                                    <?php
                                                    if (strlen($rows[$i]->designer_last_name) > 5) {
                                                        echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                    } else {
                                                        echo $rows[$i]->designer_last_name;
                                                    }
                                                    ?>

                                                </p>
                                                <p class="pro-b">Designer</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                    <?php } ?></a></p>
                                                </div>
                                            </div>
                                            <div class="cli-ent-col td" style="width: 10%;">
                                                <div class="cli-ent-xbox text-center">
                                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                            <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                        </div>
                                                    </div>
                                                </div> <!-- End Row -->
                                            <?php } ?>
                                        </div>
                                        <!--QA Active Section End Here -->
                                    <?php } elseif ($rows[$i]->status_designer == "assign" || $rows[$i]->status_designer == "pending") { ?>
                                        <!--QA Active Section Start Here -->
                                        <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                                            <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                                                <!-- Start Row -->
                                                <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'" style="cursor: pointer;">
                                                    <div class="cli-ent-col td" style="width: 12%;">
                                                        <div class="cli-ent-xbox">
                                                            <p class="pro-a">In Queue</p>
                                                        </div>
                                                    </div>
                                                    <div class="cli-ent-col td" style="width: 48%;">
                                                        <div class="cli-ent-xbox text-left">
                                                            <div class="cell-row">

                                                                <div class="cell-col" >
                                                                    <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                                                    <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                                                </div>
                                                                <!--                                        <div class="cell-col"><?php //echo isset($rows[$i]->priority) ? $rows[$i]->priority : '';           ?></div>-->
                                                                <div class="cell-col col-w1">
                                                                    <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="cli-ent-col td" style="width: 20%;">
                                                        <div class="cli-ent-xbox text-left p-left1">
                                                            <div class="cell-row">
                                                                <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                                    <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                                                        <figure class="pro-circle-img">
                                                                            <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                            <?php } else { ?>
                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                            <?php } ?>
                                                                        </figure>
                                                                    </a>
                                                                </div>
                                                                <div class="cell-col">
                                                                    <p class="text-h" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                                        <?php echo $rows[$i]->designer_first_name; ?>
                                                                        <?php
                                                                        if (strlen($rows[$i]->designer_last_name) > 5) {
                                                                            echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                                        } else {
                                                                            echo $rows[$i]->designer_last_name;
                                                                        }
                                                                        ?>

                                                                    </p>
                                                                    <p class="pro-b">Designer</p>
                                                                    <p class="space-a"></p>
                                                                    <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                                        <span class="sma-red">+</span> Add Designer
                                                                    </a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="cli-ent-col td" style="width: 10%;">
                                                        <div class="cli-ent-xbox text-center">
                                                            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                                    <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                        <span class="numcircle-box">
                                                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                        </span>
                                                                        <?php } ?></span></a></p>
                                                                    </div>
                                                                </div>
                                                                <div class="cli-ent-col td" style="width: 10%;">
                                                                    <div class="cli-ent-xbox text-center">
                                                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                                                                <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                                            </div>
                                                                        </div>
                                                                    </div> <!-- End Row -->
                                                                <?php } ?>
                                                            </div>
                                                            <!--QA Active Section End Here -->
                                                        <?php } elseif ($rows[$i]->status_designer == "pendingrevision") { ?>
                                                            <!--QA Pending Approval Section Start Here -->
                                                            <div class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel">
                                                                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                                                                    <!-- Start Row -->
                                                                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                                                                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                                                            <div class="cli-ent-xbox">
                                                                                <p class="pro-a">Expected on</p>
                                                                                <p class="space-a"></p>
                                                                                <p class="pro-b">
                                                                                    <?php echo date('M d, Y h:i A', strtotime($rows[$i]->revisiondate)); ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                                                            <div class="cli-ent-xbox text-left">
                                                                                <div class="cell-row">

                                                                                    <div class="cell-col" >
                                                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                                                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                                                                    </div>

                                                                                    <div class="cell-col col-w1">
                                                                                        <p class="neft text-center"><span class=" lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                                                            <div class="cli-ent-xbox text-left">
                                                                                <div class="cell-row">
                                                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                        <a href=""><figure class="pro-circle-img">
                                                                                            <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                                            <?php } else { ?>
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                                            <?php } ?>
                                                                                        </figure></a>
                                                                                    </div>
                                                                                    <div class="cell-col">
                                                                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                                                                        ">
                                                                                        <?php echo $rows[$i]->customer_first_name; ?>
                                                                                        <?php
                                                                                        if (strlen($rows[$i]->customer_last_name) > 5) {
                                                                                           echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                                                       } else {
                                                                                           echo $rows[$i]->customer_last_name;
                                                                                       }
                                                                                       ?>

                                                                                   </p>
                                                                                   <p class="pro-b">Client</p>
                                                                               </div>
                                                                           </div>

                                                                       </div>
                                                                   </div>

                                                                   <div class="cli-ent-col td" style="width: 16%;">
                                                                    <div class="cli-ent-xbox text-left">
                                                                        <div class="cell-row">
                                                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                <a href="" class="<?php echo $rows[$i]->id; ?>""><figure class="pro-circle-img">
                                                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                                    <?php } else { ?>
                                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                                    <?php } ?>
                                                                                </figure></a>
                                                                            </div>
                                                                            <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                                                                <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                                                    <?php echo $rows[$i]->designer_first_name; ?>
                                                                                    <?php
                                                                                    if (strlen($rows[$i]->designer_last_name) > 5) {
                                                                                        echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                                                    } else {
                                                                                        echo $rows[$i]->designer_last_name;
                                                                                    }
                                                                                    ?>
                                                                                </p>
                                                                                <p class="pro-b">Designer</p>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                                                    <div class="cli-ent-xbox text-center">
                                                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                                <span class="numcircle-box">
                                                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                                </span>
                                                                                <?php } ?></span>
                                                                                <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                                                                            <div class="cli-ent-xbox text-center">
                                                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                                                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                                        <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                                                                        <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                                                    </div>
                                                                                </div>
                                                                            </div> 
                                                                            <!-- End Row -->
                                                                        <?php } ?>
                                                                    </div>
                                                                    <!--QA Pending Approval Section End Here -->
                                                                <?php } elseif ($rows[$i]->status == "checkforapprove") { ?>
                                                                    <!--QA Pending Approval Section Start Here -->
                                                                    <div class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel">
                                                                        <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                                                                            <!-- Start Row -->
                                                                            <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                                                                                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                                                                    <div class="cli-ent-xbox">
                                                                                        <p class="pro-a">Delivered on</p>
                                                                                        <p class="space-a"></p>
                                                                                        <p class="pro-b">
                                                                                            <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate)); ?>
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                                                                    <div class="cli-ent-xbox text-left">
                                                                                        <div class="cell-row">

                                                                                            <div class="cell-col" >
                                                                                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4"><?php echo $rows[$i]->title; ?></a></h3>
                                                                                                <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                                                                            </div>

                                                                                            <div class="cell-col col-w1">
                                                                                                <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-approval</span></p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                                                                    <div class="cli-ent-xbox text-left">
                                                                                        <div class="cell-row">
                                                                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                                <a href="#"><figure class="pro-circle-img">
                                                                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                                                    <?php } else { ?>
                                                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                                                    <?php } ?>
                                                                                                </figure></a>
                                                                                            </div>
                                                                                            <div class="cell-col">
                                                                                                <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                                                                                ">
                                                                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                                                                <?php
                                                                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                                                               } else {
                                                                                                   echo $rows[$i]->customer_last_name;
                                                                                               }
                                                                                               ?>

                                                                                           </p>
                                                                                           <p class="pro-b">Client</p>
                                                                                       </div>
                                                                                   </div>

                                                                               </div>
                                                                           </div>

                                                                           <div class="cli-ent-col td" style="width: 16%;">
                                                                            <div class="cli-ent-xbox text-left">
                                                                                <div class="cell-row">
                                                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                        <a href="#" class="<?php echo $rows[$i]->id; ?>""><figure class="pro-circle-img">
                                                                                            <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                                            <?php } else { ?>
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                                            <?php } ?>
                                                                                        </figure></a>
                                                                                    </div>
                                                                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                                                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                                                            <?php echo $rows[$i]->designer_first_name; ?>
                                                                                            <?php
                                                                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                                                            } else {
                                                                                                echo $rows[$i]->designer_last_name;
                                                                                            }
                                                                                            ?>
                                                                                        </p>
                                                                                        <p class="pro-b">Designer</p>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                                                            <div class="cli-ent-xbox text-center">
                                                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                                                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                                        <span class="numcircle-box">
                                                                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                                        </span>
                                                                                        <?php } ?></span>
                                                                                        <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                                                                                    <div class="cli-ent-xbox text-center">
                                                                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                                                                                <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div> 
                                                                                    <!-- End Row -->
                                                                                <?php } ?>
                                                                            </div>
                                                                            <!--QA Pending Approval Section End Here -->
                                                                        <?php } elseif ($rows[$i]->status == "approved") { ?>
                                                                            <!--QA Completed Section Start Here -->
                                                                            <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
                                                                                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                                                                                    <!-- Start Row -->
                                                                                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5'"style="cursor: pointer;">
                                                                                        <div class="cli-ent-col td" style="width: 13%;">
                                                                                            <div class="cli-ent-xbox">
                                                                                                <h3 class="app-roved green">Approved on</h3>
                                                                                                <p class="pro-b">
                                                                                                    <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate)); ?>
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="cli-ent-col td" style="width: 37%;">
                                                                                            <div class="cli-ent-xbox text-left">
                                                                                                <div class="cell-row">

                                                                                                    <div class="cell-col" >
                                                                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5"><?php echo $rows[$i]->title; ?></a></h3>
                                                                                                        <p class="pro-b">
                                                                                                            <?php echo substr($rows[$i]->description, 0, 30); ?>
                                                                                                        </p>
                                                                                                    </div>

                                                                                                    <div class="cell-col col-w1">
                                                                                                        <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="cli-ent-col td" style="width: 16%;">
                                                                                            <div class="cli-ent-xbox text-left">
                                                                                                <div class="cell-row">
                                                                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                                        <a href=""><figure class="pro-circle-img">
                                                                                                            <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                                                            <?php } else { ?>
                                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                                                            <?php } ?>
                                                                                                        </figure></a>
                                                                                                    </div>
                                                                                                    <div class="cell-col">
                                                                                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                                                                            <?php echo $rows[$i]->customer_first_name; ?>
                                                                                                            <?php
                                                                                                            if (strlen($rows[$i]->customer_last_name) > 5) {
                                                                                                                echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                                                                            } else {
                                                                                                                echo $rows[$i]->customer_last_name;
                                                                                                            }
                                                                                                            ?>

                                                                                                        </p>
                                                                                                        <p class="pro-b">Client</p>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="cli-ent-col td" style="width: 16%;">
                                                                                            <div class="cli-ent-xbox text-left">
                                                                                                <div class="cell-row">
                                                                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                                        <a href=""><figure class="pro-circle-img">
                                                                                                            <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                                                            <?php } else { ?>
                                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                                                            <?php } ?>
                                                                                                        </figure></a>
                                                                                                    </div>
                                                                                                    <div class="cell-col">
                                                                                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?> ">
                                                                                                            <?php echo $rows[$i]->designer_first_name; ?>
                                                                                                            <?php
                                                                                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                                                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                                                                            } else {
                                                                                                                echo $rows[$i]->designer_last_name;
                                                                                                            }
                                                                                                            ?>

                                                                                                        </p>
                                                                                                        <p class="pro-b">Designer</p>

                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="cli-ent-col td" style="width: 9%;">
                                                                                            <div class="cli-ent-xbox text-center">
                                                                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5">
                                                                                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                                                        <span class="numcircle-box">
                                                                                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                                                        </span>
                                                                                                        <?php } ?></span>
                                                                                                        <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="cli-ent-col td" style="width: 9%;">
                                                                                                    <div class="cli-ent-xbox text-center">
                                                                                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5">
                                                                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                                                                                                <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div> <!-- End Row -->
                                                                                                <?php } ?>
                                                                                            </div>
                                                                                            <!--QA Completed Section End Here -->
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                }

                                                                                public function search_ajax_client() {
                                                                                    $this->myfunctions->checkloginuser("admin");
                                                                                    $dataArray = $this->input->get('name');
                                                                                    if ($dataArray != "") {
                                                                                        $data = $this->Request_model->get_all_clients_for_admin(array('keyword' => $dataArray));
                                                                                        for ($i = 0; $i < sizeof($data); $i++) {
                                                                                            $user = $this->Account_model->get_all_request_by_customer($data[$i]['id']);
                                                                                            $data[$i]['no_of_request'] = sizeof($user);
                                                                                            $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $data[$i]['id']);
                                                                                            $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $data[$i]['id']);
                                                                                            $revision = $this->Account_model->get_all_active_view_request_by_customer(array('disapprove'), $data[$i]['id']);
                                                                                            $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $data[$i]['id']);
                                                                                            $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $data[$i]['id']);
                                                                                            $user = $this->Account_model->get_all_active_request_by_customer($data[$i]['id']);
                                                                                            $data[$i]['active'] = $active;
                                                                                            $data[$i]['inque_request'] = $inque;
                                                                                            $data[$i]['revision_request'] = $revision;
                                                                                            $data[$i]['review_request'] = $review;
                                                                                            $data[$i]['complete_request'] = $complete;

                                                                                            $data[$i]['active_request'] = sizeof($user);

                                                                                            $data[$i]['designer_name'] = "";
                                                                                            if ($data[$i]['designer_id'] != 0) {
                                                                                                $data[$i]['designer'] = $this->Request_model->get_user_by_id($data[$i]['designer_id']);
                                                                                            } else {
                                                                                                $data[$i]['designer'] = "";
                                                                                            }
                                                                                            $designer_name = $this->Account_model->getuserbyid($data[$i]['designer_id']);

                                                                                            if (!empty($designer_name)) {
                                                                                                $data[$i]['designer_name'] = $designer_name[0]['first_name'];
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        $data = $this->Account_model->getall_customer("assigned");
                                                                                    }
                                                                                    for ($i = 0; $i < sizeof($data); $i++) {
                                                                                        ?>
                                                                                        <!-- Start Row -->
                                                                                        <div class="cli-ent-row tr brdr not-styler"  style="cursor: pointer;">
                                                                                            <div class="cli-ent-col td" style="width: 60%;">
                                                                                                <div class="cli-ent-xbox text-left">
                                                                                                    <div class="cell-row">
                                                                                                        <div class="cell-col" style="width: 80px; padding-right: 15px;">
                                                                                                            <?php if (($data[$i]['profile_picture'] != "")) { ?>
                                                                                                                <figure class="cli-ent-img circle one">
                                                                                                                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $data[$i]['profile_picture']; ?>" class="img-responsive one">
                                                                                                                </figure>
                                                                                                            <?php } else { ?>
                                                                                                                <figure class="cli-ent-img circle one">
                                                                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive one">
                                                                                                                </figure>
                                                                                                            <?php } ?>
                                                                                                        </div>

                                                                                                        <div class="cell-col" style="width: 185px;">
                                                                                                            <h3 class="pro-head-q">
                                                                                                                <a href="<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>"><?php echo ucwords($data[$i]['first_name'] . " " . $data[$i]['last_name']); ?></a>
                                                                                                                <br>
                                                                                                                <a href="<?php echo base_url(); ?>admin/accounts/view_client_profile/<?php echo $data[$i]['id']; ?>">
                                                                                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                                                                                </a>
            <!-- <a href="#" data-toggle="modal" data-target="#<?php echo $data[$i]['id']; ?>">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </a> -->
        <a href="javascript:void(0)" data-customerid="<?php echo $data[$i]['id']; ?>" class="delete_customer">
            <i class="fa fa-trash" aria-hidden="true"></i></a>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#viewinfo<?php echo $data[$i]['id']; ?>" class="viewinfo_customer">
                <i class="fa fa-eye" aria-hidden="true"></i>
            </a>
        </h3>
        <p class="pro-a"> <?php echo $data[$i]['email']; ?></p>
    </div>

    <div class="cell-col">
        <p class="neft">
            <span class="blue text-uppercase">
                <?php
                if ($data[$i]['plan_turn_around_days'] == "1") {
                    echo "Premium Member";
                } elseif ($data[$i]['plan_turn_around_days'] == "3") {
                    echo "Standard Member";
                } else {
                    echo "No Member";
                }
                ?>
            </span>
        </p>
    </div>
</div>

</div>
</div>
<div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
    <div class="cli-ent-xbox text-center">
        <p class="word-wrap-one">Sign Up Date</p>
        <p class="space-a"></p>
        <p class="pro-b"><?php echo date("M/d/Y", strtotime($data[$i]['created'])) ?></p>
    </div>
</div>
<div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
    <div class="cli-ent-xbox text-center">
        <p class="word-wrap-one">Next Billing Date</p>
        <p class="space-a"></p>
        <p class="pro-b">
            <?php
            $date = "";
            if (checkdate(date("m", strtotime($data[$i]['modified'])), date("d", strtotime($data[$i]['modified'])), date("Y", strtotime($data[$i]['modified'])))) {
                $over_date = "";
                if ($data[$i]['plan_turn_around_days']) {
                    $date = date("Y-m-d", strtotime($data[$i]['modified']));
                    $time = date("h:i:s", strtotime($data[$i]['modified']));
                    $data[$i]['plan_turn_around_days'] = "2";
                    $date = date("m/d/Y g:i a", strtotime($date . " " . $data[$i]['plan_turn_around_days'] . " weekdays " . $time));
                    $diff = date_diff(date_create(date("Y-m-d", strtotime($data[$i]['modified']))), date_create($date));
                }
            }
            $current_date = strtotime(date("Y-m-d"));
                            $now = time(); // or your date as well

                            $expiration_date = strtotime($date);
                            $datediff = $now - $expiration_date;
                            $date_due_day = round($datediff / (60 * 60 * 24));
                            $color = "";
                            $text_color = "white";
                            if ($date_due_day == 0) {
                                $date_due_day = "Due  </br>Today";
                                $color = "#f7941f";
                            } else
                            if ($date_due_day == (-1)) {
                                $date_due_day = "Due  </br>Tomorrow";
                                $color = "#98d575";
                            } else
                            if ($date_due_day > 0) {
                                $date_due_day = "Over Due";
                                $color = "red";
                            } else if ($date_due_day < 0) {
                                $date_due_day = "Due from </br>" . number_format($date_due_day) . " days";
                                $text_color = "black";
                            }
                            ?>
                            <?php echo date_format(date_create($date), "M /d /Y"); ?></p>
                        </div>
                    </div>

                    <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Active Requests</p>
                            <p class="space-a"></p>
                            <p class="pro-a"><?php echo $data[$i]['active']; ?></p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">In Queue Requests</p>
                            <p class="space-a"></p>
                            <p class="pro-a"><?php echo $data[$i]['inque_request']; ?></p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Pending Approval Requests</p>
                            <p class="space-a"></p>
                            <p class="pro-a"><?php echo $data[$i]['review_request']; ?></p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="min-width: 171px; max-width: 171px; width: 10%;">
                        <?php if (!empty($data[$i]['designer'])) { ?>
                            <div class="cli-ent-xbox">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($data[$i]['designer'][0]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $data[$i]['designer'][0]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $data[$i]['designer'][0]['first_name'] . " " . $data[$i]['designer'][0]['last_name']; ?>
                                        ">
                                        <?php echo $data[$i]['designer'][0]['first_name']; ?>
                                        <?php
                                        if (strlen($data[$i]['designer'][0]['last_name']) > 5) {
                                           echo ucwords(substr($data[$i]['designer'][0]['last_name'], 0, 1));
                                       } else {
                                           echo $data[$i]['designer'][0]['last_name'];
                                       }
                                       ?>

                                   </p>
                                   <p class="pro-b">Designer</p>
                                   <p class="space-a"></p>
                                   <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $data[$i]['id']; ?>">
                                    <span class="sma-red">+</span> Add Designer
                                </a>
                            </div>
                        </div> 
                    </div>
                <?php } else { ?>

                    <div class="cli-ent-xbox">
                        <a href="#"  class="upl-oadfi-le noborder permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign" data-customerid="<?php echo $data[$i]['id']; ?>">
                            <span class="icon-crss-3">
                                <span class="icon-circlxx55 margin5">+</span>
                                <p class="attachfile">Assign designer<br> permanently</p>
                            </span>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="viewinfo<?php echo $data[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="cli-ent-model-box">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="cli-ent-model">
                                <header class="fo-rm-header">
                                    <h3 class="head-c">View Information</h3>
                                </header>
                                <div class="fo-rm-body">
                                    <div class="projects">
                                        <ul class="projects_info_list">
                                            <li>
                                                <div class="starus"><b>Active Projects</b></div>
                                                <div class="value"><?php echo $data[$i]['active']; ?>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="starus"><b>In -Queue Projects</b></div>
                                            <div class="value"><?php echo $data[$i]['inque_request']; ?>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>Revision Projects</b></div>
                                        <div class="value"><?php echo $data[$i]['revision_request']; ?>
                                    </div>
                                </li>
                                <li>
                                    <div class="starus"><b>Review Projects</b></div>
                                    <div class="value"><?php echo $data[$i]['review_request']; ?>
                                </div>
                            </li>
                            <li>
                                <div class="starus"><b>Complete Projects</b></div>
                                <div class="value"><?php echo $data[$i]['complete_request']; ?>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="<?php echo $data[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Edit Client</h3>
                    </header>
                    <div class="fo-rm-body">
                        <form onSubmit="return EditFormSQa(<?php echo $data[$i]['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/dashboard/edit_client/<?php echo $data[$i]['id']; ?>">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">First Name</label>
                                        <p class="space-a"></p>
                                        <input type="text" name="first_name" required class="efirstname form-control input-c" value="<?php echo ucwords($data[$i]['first_name']); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Last Name</label>
                                        <p class="space-a"></p>
                                        <input type="text" name="last_name" required class="elastname form-control input-c" value="<?php echo ucwords($data[$i]['last_name']); ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Email</label>
                                        <p class="space-a"></p>
                                        <input type="email" name="email" required  class="eemailid form-control input-c" value="<?php echo $data[$i]['email']; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Phone</label>
                                        <p class="space-a"></p>
                                        <input type="tel" name="phone" required class="ephone form-control input-c" value="<?php echo $data[$i]['phone']; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x3">Password</label>
                                <p class="space-a"></p>
                                <input type="password" name="password"   class="form-control input-c epassword" >
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x3">Confirm Password</label>
                                <p class="space-a"></p>
                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword" >
                            </div>
                            <div class="epassError" style="display: none;">
                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                            </div>
                                            <!--<hr class="hr-b">
                                            <p class="space-b"></p>
                                            
                                             <div class="form-group goup-x1">
                                                    <label class="label-x3">Select payment Method</label>
                                                    <p class="space-a"></p>
                                                    <select class="form-control select-c">
                                                            <option>Credit/Debit Card.</option>
                                                            <option>Credit/Debit Card.</option>
                                                            <option>Credit/Debit Card.</option>
                                                    </select>
                                            </div>
                                            
                                            <div class="form-group goup-x1">
                                                    <label class="label-x3">Card Number</label>
                                                    <p class="space-a"></p>
                                                    <input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx" name="Card Number" required>
                                            </div>
                                            
                                            <div class="row">
                                                    <div class="col-sm-5">
                                                            <div class="form-group goup-x1">
                                                                    <label class="label-x3">Expiry Date</label>
                                                                    <p class="space-a"></p>
                                                                    <input type="text" name="Expiration Date" required class="form-control input-c" placeholder="22.05.2022">
                                                            </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-3 col-md-offset-4">
                                                            <div class="form-group goup-x1">
                                                                    <label class="label-x3">CVC</label>
                                                                    <p class="space-a"></p>
                                                                    <input type="text" name="CVC" required class="form-control input-c" placeholder="xxx" maxlength="3">
                                                            </div>
                                                    </div>
                                                </div> -->

                                                <p class="space-b"></p>

                                                <p class="btn-x"><button type="submit" class="btn-g">Add Customer</button></p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row -->
                <?php
            }
        }

        public function search_ajax_designer() {
            $this->myfunctions->checkloginuser("admin");
            $dataArray = $this->input->get('name');
            if ($dataArray != "") {
                $designers = $this->Request_model->get_all_designer_for_admin(array('keyword' => $dataArray), $_SESSION['user_id']);

                for ($i = 0; $i < sizeof($designers); $i++) {
                    $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);

                    $user = $this->Account_model->get_all_request_by_designer($designers[$i]['id']);
                    $designers[$i]['handled'] = sizeof($user);

                    $user = $this->Account_model->get_all_active_request_by_designer($designers[$i]['id']);
                    $designers[$i]['active_request'] = sizeof($user);
                }
            } else {
                $designers = $this->Account_model->getall_designer();
            }
            for ($i = 0; $i < sizeof($designers); $i++) {
                ?>
                <!-- Start Row -->
                <div class="cli-ent-row tr brdr"  style="cursor: pointer;">
                    <div class="cli-ent-col td" style="width: 10%;">
                        <?php
                        if ($designers[$i]['profile_picture']) {
                            ?>
                            <figure class="cli-ent-img circle one">
                                <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $designers[$i]['profile_picture']; ?>" class="img-responsive one">
                            </figure>
                        <?php } else { ?>
                            <figure class="cli-ent-img circle one">
                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive one">
                            </figure>
                        <?php } ?>
                    </div>
                    <div class="cli-ent-col td" style="width: 35%;">
                        <div class="cli-ent-xbox text-left">
                            <div class="cell-row">

                                <div class="cell-col">
                                    <h3 class="pro-head space-b">
                                        <a href="<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>"><?php echo $designers[$i]['first_name'] . " " . $designers[$i]['last_name'] ?></a>

                                        <a href="<?php echo base_url(); ?>admin/accounts/view_designer_profile/<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                        <?php if ($designers[$i]['is_active'] == 0) { ?>
                                            <a title="Enable" id="enable<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" data-target="#confirmation" data-status="enable" class="confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                            </a>
                                            <a title="Disable" id="disable<?php echo $designers[$i]['id']; ?>" style="display: none;" href="#" data-toggle="modal" class="confirmation" data-status="disable" data-target="#confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a title="Enable" id="enable<?php echo $designers[$i]['id']; ?>" style="display: none;" href="#" data-toggle="modal" data-target="#confirmation" data-status="enable" class="confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                            </a>
                                            <a title="Disable" id="disable<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" class="confirmation" data-status="disable" data-target="#confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                                            </a>
                                        <?php } ?>
                                        <a title="delete" id="delete<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" class="confirmationdelete" data-target="#confirmationdelete" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                        <p class="pro-b"><?php echo $designers[$i]['email']; ?></p>
                                    </h3>
                                    <p class="pro-b"><?php echo $designers[$i]['about_me']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="cli-ent-col td" style="width: 15%;"onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Sign Up Date</p>
                            <p class="space-a"></p>
                            <p class="pro-b"><?php echo date("M /d /Y", strtotime($designers[$i]['created'])) ?></p>
                        </div>
                    </div>

                    <div class="cli-ent-col td" style="width: 20%;"onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Requests Being Handled</p>
                            <p class="space-a"></p>
                            <p class="neft text-center"><span class="green text-uppercase"><?php echo $designers[$i]['handled']; ?></span></p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 20%;"onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Active Requests</p>
                            <p class="space-a"></p>
                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers[$i]['active_request']; ?></span></p>
                        </div>
                    </div>
                    <!-- Edit Designer Modal -->
                    <div class="modal fade" id="<?php echo $designers[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="cli-ent-model-box">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="cli-ent-model">
                                        <header class="fo-rm-header">
                                            <h3 class="head-c">Edit Designer</h3>
                                        </header>
                                        <div class="fo-rm-body">
                                            <form onSubmit="return EditFormSQa(<?php echo $designers[$i]['id']; ?>)" action="<?php echo base_url(); ?>admin/dashboard/edit_designer/<?php echo $designers[$i]['id']; ?>" method="post">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">First Name</label>
                                                            <p class="space-a"></p>
                                                            <input name="first_name" type="text" required class="form-control input-c efirstname" value="<?php echo ucwords($designers[$i]['first_name']); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Last Name</label>
                                                            <p class="space-a"></p>
                                                            <input type="text" name="last_name" required  class="form-control input-c elastname" value="<?php echo ucwords($designers[$i]['last_name']); ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Email</label>
                                                            <p class="space-a"></p>
                                                            <input type="email" name="email" required class="form-control input-c eemailid" value="<?php echo $designers[$i]['email']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Phone</label>
                                                            <p class="space-a"></p>
                                                            <input type="tel" name="phone" required class="form-control input-c ephone" value="<?php echo $designers[$i]['phone']; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr class="hr-b">

                                                <p class="space-c"></p>

                                                <div class="form-group goup-x1">
                                                    <label class="label-x3">Password</label>
                                                    <p class="space-a"></p>
                                                    <input type="password" name="password"   class="form-control input-c epassword">
                                                </div>

                                                <div class="form-group goup-x1">
                                                    <label class="label-x3">Confirm Password</label>
                                                    <p class="space-a"></p>
                                                    <input type="password" name="confirm_password"  class="form-control input-c ecpassword">
                                                </div>
                                                <div class="epassError" style="display: none;">
                                                    <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                                </div>
                                                <p class="space-b"></p>

                                                <p class="btn-x"><button type="submit" class="btn-g">Add Designer</button></p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Row -->
                <?php
            }
        }

        public function search_ajax_customer_project() {
            $this->myfunctions->checkloginuser("admin");
            $dataArray = $this->input->get('title');
            $dataStatus = $this->input->get('status');
            $datauserid = $this->input->get('id');
            $dataStatus = explode(",", $dataStatus);
            $rows = $this->Request_model->GetAutocomplete_for_admin(array('keyword' => $dataArray), $dataStatus, $datauserid, "admin");
            for ($i = 0; $i < sizeof($rows); $i++) {
                $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "admin");
                $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "admin");
                $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
                $commentcount = 0;
                for ($j = 0; $j < sizeof($getfileid); $j++) {

                    $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
                }
                if ($rows[$i]->status == "active" || $rows[$i]->status == "disapprove") {
                    if ($rows[$i]->expected_date == '' || $rows[$i]->expected_date == NULL) {
                        $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update, $rows[$i]->current_plan_name);
                    } else {
                        $rows[$i]->expected = $this->onlytimezone($rows[$i]->expected_date);
                    }
                } elseif ($rows[$i]->status == "checkforapprove") {
                //$rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->latest_update);
                    $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->modified);
                } elseif ($rows[$i]->status == "approved") {
                    $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
                }
                if ($rows[$i]->status_admin == "pendingrevision") {
                    $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
                }
                $rows[$i]->comment_count = $commentcount;
                $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, "admin");
                $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
            }
            for ($i = 0; $i < sizeof($rows); $i++) {
                if ($rows[$i]->status == "disapprove" || $rows[$i]->status == "active" || $rows[$i]->status == "assign") {
                    ?>
                    <!--QA Active Section Start Here -->
                    <div class="tab-pane active content-datatable datatable-width" id="Qa_client_active" role="tabpanel">
                        <?php
                        for ($i = 0; $i < sizeof($rows); $i++) {
                            if ($rows[$i]->status == "disapprove" || $rows[$i]->status == "active") {
                                ?>
                                <!-- Start Row -->
                                <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                                    <div class="cli-ent-col td" style="width: 12%;">
                                        <div class="cli-ent-xbox">
                                            <p class="pro-a">
                                                <?php
                                                if ($rows[$i]->status == "active") {
                                                    echo "Expected on";
                                                } elseif ($rows[$i]->status == "disapprove") {
                                                    echo "Expected on";
                                                } elseif ($rows[$i]->status == "assign") {
                                                    echo "In-Queue";
                                                }
                                                ?>
                                            </p>
                                            <p class="space-a"></p>
                                            <p class="pro-b">
                                                <?php
                                                if ($rows[$i]->status == "active") {
                                                    echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                                } elseif ($rows[$i]->status == "disapprove") {
                                                    echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 48%;">
                                        <div class="cli-ent-xbox text-left">
                                            <div class="cell-row">

                                                <div class="cell-col" >
                                                    <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                                    <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                                </div>

                                                <div class="cell-col col-w1">
                                                    <p class="neft text-center">
                                                        <?php if ($rows[$i]->status == "active" || $rows[$i]->status == "checkforapprove") { ?>
                                                            <span class="green text-uppercase">In-Progress</span>
                                                        <?php } elseif ($rows[$i]->status == "disapprove") { ?>
                                                            <span class="red orangetext text-uppercase">REVISION</span>
                                                        <?php } elseif ($rows[$i]->status == "assign") { ?>
                                                            <span class="gray text-uppercase">In-Queue</span>
                                                        <?php } ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cli-ent-col td" style="width: 20%;">
                                        <div class="cli-ent-xbox text-left p-left1">
                                            <div class="cell-row">
                                                <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                    <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                        <?php if ($rows[$i]->profile_picture != "") { ?>
                                                            <figure class="pro-circle-img">
                                                                <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                            </figure>
                                                        <?php } else { ?>
                                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                            </figure>
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="cell-col">
                                                    <p class="text-h">
                                                        <?php
                                                        if ($rows[$i]->designer_first_name) {
                                                            echo $rows[$i]->designer_first_name;
                                                        } else {
                                                            echo "No Designer";
                                                        }
                                                        ?>  
                                                    </p>
                                                    <p class="pro-b">Designer</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 10%;">
                                        <div class="cli-ent-xbox text-center">
                                            <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                    <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                        <span class="numcircle-box">
                                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                        </span>
                                                        <?php } ?></span>
                                                        <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                                                    </div>
                                                </div>
                                                <div class="cli-ent-col td" style="width: 10%;">
                                                    <div class="cli-ent-xbox text-center">
                                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                            <?php } ?>
                                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                        </div>
                                                    </div>
                                                </div> <!-- End Row -->
                                                <?php
                                            }
                                            if ($rows[$i]->status == "assign") {
                                                ?>
                                                <!-- Start Row -->
                                                <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                                                    <div class="cli-ent-col td" style="width: 12%;">
                                                        <div class="cli-ent-xbox">
                                                            <p class="pro-a">
                                                                <?php
                                                                if ($rows[$i]->status == "active") {
                                                                    echo "Expected on";
                                                                } elseif ($rows[$i]->status == "disapprove") {
                                                                    echo "Expected on";
                                                                } elseif ($rows[$i]->status == "assign") {
                                                                    echo "In-Queue";
                                                                }
                                                                ?>
                                                            </p>
                                                            <p class="space-a"></p>
                                                            <p class="pro-b">
                                                                <?php
                                                                if ($rows[$i]->status == "active") {
                                                                    echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                                                } elseif ($rows[$i]->status == "disapprove") {
                                                                    echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                                                }
                                                                ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="cli-ent-col td" style="width: 48%;">
                                                        <div class="cli-ent-xbox text-left">
                                                            <div class="cell-row">

                                                                <div class="cell-col" >
                                                                    <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                                                    <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                                                </div>
                                                                <!--                                            <div class="cell-col"><?php //echo isset($rows[$i]->priority) ? $rows[$i]->priority : '';           ?></div>-->

                                                                <div class="cell-col col-w1">
                                                                    <p class="neft text-center">
                                                                        <?php if ($rows[$i]->status == "active" || $rows[$i]->status == "checkforapprove") { ?>
                                                                            <span class="green text-uppercase">In-Progress</span>
                                                                        <?php } elseif ($rows[$i]->status == "disapprove") { ?>
                                                                            <span class="red orangetext text-uppercase">REVISION</span>
                                                                        <?php } elseif ($rows[$i]->status == "assign") { ?>
                                                                            <span class="gray text-uppercase">In-Queue</span>
                                                                        <?php } ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="cli-ent-col td" style="width: 20%;">
                                                        <div class="cli-ent-xbox text-left p-left1">
                                                            <div class="cell-row">
                                                                <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                                    <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                                        <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                            <figure class="pro-circle-img">
                                                                                <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                                            </figure>
                                                                        <?php } else { ?>
                                                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                                <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                                            </figure>
                                                                        <?php } ?>
                                                                    </a>
                                                                </div>
                                                                <div class="cell-col">
                                                                    <p class="text-h">
                                                                        <?php
                                                                        if ($rows[$i]->designer_first_name) {
                                                                            echo $rows[$i]->designer_first_name;
                                                                        } else {
                                                                            echo "No Designer";
                                                                        }
                                                                        ?>  
                                                                    </p>
                                                                    <p class="pro-b">Designer</p>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="cli-ent-col td" style="width: 10%;">
                                                        <div class="cli-ent-xbox text-center">
                                                            <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                                    <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                        <span class="numcircle-box">
                                                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                        </span>
                                                                        <?php } ?></span>
                                                                        <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                                                                    </div>
                                                                </div>
                                                                <div class="cli-ent-col td" style="width: 10%;">
                                                                    <div class="cli-ent-xbox text-center">
                                                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                                            <?php } ?>
                                                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                                        </div>
                                                                    </div>
                                                                </div> <!-- End Row -->
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                    <!--QA Active Section End Here -->
                                                <?php } elseif ($rows[$i]->status == "checkforapprove") { ?>
                                                    <!--QA Pending Approval Section Start Here -->
                                                    <div class="tab-pane content-datatable datatable-width" id="Qa_client_pending" role="tabpanel">
                                                        <?php for ($i = 0; $i < sizeof($rows); $i++) {
                                                            ?>
                                                            <!-- Start Row -->
                                                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'"  style="cursor: pointer;">
                                                                <div class="cli-ent-col td" style="width: 10%;">
                                                                    <div class="cli-ent-xbox">
                                                                        <p class="pro-a">
                                                                            Delivered on
                                                                        </p>
                                                                        <p class="space-a"></p>
                                                                        <p class="pro-b">
                                                                            <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate));
                                                                            ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="cli-ent-col td" style="width: 40%;">
                                                                    <div class="cli-ent-xbox text-left">
                                                                        <div class="cell-row">

                                                                            <div class="cell-col">
                                                                                <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                                                                <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                                                            </div>

                                                                            <div class="cell-col col-w1">
                                                                                <p class="neft text-center">
                                                                                    <span class="red bluetext text-uppercase text-wrap">Pending-approval</span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="cli-ent-col td" style="width: 16%;">
                                                                    <div class="cli-ent-xbox text-left">
                                                                        <div class="cell-row">
                                                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                                                        <figure class="pro-circle-img">
                                                                                            <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->customer_profile_picture; ?>" class="img-responsive">
                                                                                        </figure>
                                                                                    <?php } else { ?>
                                                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                                            <?php echo ucwords(substr($rows[$i]->customer_first_name, 0, 1)) . ucwords(substr($rows[$i]->customer_last_name, 0, 1)); ?>
                                                                                        </figure>
                                                                                    <?php } ?>
                                                                                </a>
                                                                            </div>
                                                                            <div class="cell-col">
                                                                                <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                                                    <?php echo $rows[$i]->customer_first_name; ?>
                                                                                    <?php
                                                                                    if (strlen($rows[$i]->customer_last_name) > 5) {
                                                                                        echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                                                    } else {
                                                                                        echo $rows[$i]->customer_last_name;
                                                                                    }
                                                                                    ?>
                                                                                </p>
                                                                                <p class="pro-b">Client</p>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="cli-ent-col td" style="width: 16%;">
                                                                    <div class="cli-ent-xbox text-left">
                                                                        <div class="cell-row">
                                                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                                        <figure class="pro-circle-img">
                                                                                            <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                                                        </figure>
                                                                                    <?php } else { ?>
                                                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                                            <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                                                        </figure>
                                                                                    <?php } ?>
                                                                                </a>
                                                                            </div>
                                                                            <div class="cell-col">
                                                                                <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                                                    <?php echo $rows[$i]->designer_first_name; ?>
                                                                                    <?php
                                                                                    if (strlen($rows[$i]->designer_last_name) > 5) {
                                                                                        echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                                                    } else {
                                                                                        echo $rows[$i]->designer_last_name;
                                                                                    }
                                                                                    ?>
                                                                                </p>
                                                                                <p class="pro-b">Designer</p>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="cli-ent-col td" style="width: 9%;">
                                                                    <div class="cli-ent-xbox text-center">
                                                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                                <span class="numcircle-box">
                                                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                                </span>
                                                                                <?php } ?></span>
                                                                                <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cli-ent-col td" style="width: 9%;">
                                                                            <div class="cli-ent-xbox text-center">
                                                                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                                                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                                        <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                                                    <?php } ?>
                                                                                    <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                                                </div>
                                                                            </div>
                                                                        </div> <!-- End Row -->
                                                                    <?php } ?>
                                                                </div>
                                                                <!--QA Pending Approval Section End Here -->
                                                            <?php } elseif ($rows[$i]->status == "approved") { ?>
                                                                <!--QA Completed Section Start Here -->
                                                                <div class="tab-pane content-datatable datatable-width" id="Qa_client_completed" role="tabpanel">
                                                                    <?php
                                                                    for ($i = 0; $i < sizeof($rows); $i++) {
                        // echo "<pre>";
                        // print_r($rows);
                        // echo "</pre>";
                                                                        ?>
                                                                        <!-- Start Row -->
                                                                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'" style="cursor: pointer;">
                                                                            <div class="cli-ent-col td" style="width: 13%;">
                                                                                <div class="cli-ent-xbox">
                                                                                    <h3 class="app-roved green">Approved on</h3>
                                                                                    <p class="space-a"></p>
                                                                                    <p class="pro-b">
                                                                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate));
                                                                                        ?>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="cli-ent-col td" style="width: 37%;">
                                                                                <div class="cli-ent-xbox text-left">
                                                                                    <div class="cell-row">

                                                                                        <div class="cell-col">
                                                                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                                                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                                                                        </div>

                                                                                        <div class="cell-col col-w1">
                                                                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="cli-ent-col td" style="width: 16%;">
                                                                                <div class="cli-ent-xbox text-left">
                                                                                    <div class="cell-row">
                                                                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                                                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                                                                    <figure class="pro-circle-img">
                                                                                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->customer_profile_picture; ?>" class="img-responsive">
                                                                                                    </figure>
                                                                                                <?php } else { ?>
                                                                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                                                        <?php echo ucwords(substr($rows[$i]->customer_first_name, 0, 1)) . ucwords(substr($rows[$i]->customer_last_name, 0, 1)); ?>
                                                                                                    </figure>
                                                                                                <?php } ?>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="cell-col">
                                                                                            <p class="text-h text-wrap" title=" <?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                                                                <?php
                                                                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                                                                    echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                                                                } else {
                                                                                                    echo $rows[$i]->customer_last_name;
                                                                                                }
                                                                                                ?>
                                                                                            </p>
                                                                                            <p class="pro-b">Client</p>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                            <div class="cli-ent-col td" style="width: 16%;">
                                                                                <div class="cli-ent-xbox text-left">
                                                                                    <div class="cell-row">
                                                                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                                                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                                                    <figure class="pro-circle-img">
                                                                                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                                                                    </figure>
                                                                                                <?php } else { ?>
                                                                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                                                        <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                                                                    </figure>
                                                                                                    <?php } ?></a>
                                                                                                </div>
                                                                                                <div class="cell-col">
                                                                                                    <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                                                                        <?php echo $rows[$i]->designer_first_name; ?>
                                                                                                        <?php
                                                                                                        if (strlen($rows[$i]->designer_last_name) > 5) {
                                                                                                            echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                                                                        } else {
                                                                                                            echo $rows[$i]->designer_last_name;
                                                                                                        }
                                                                                                        ?>
                                                                                                    </p>
                                                                                                    <p class="pro-b">Designer</p>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cli-ent-col td" style="width: 9%;">
                                                                                        <div class="cli-ent-xbox text-center">
                                                                                            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                                                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                                                    <span class="numcircle-box">
                                                                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                                                    </span>
                                                                                                    <?php } ?></span>
                                                                                                    <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="cli-ent-col td" style="width: 9%;">
                                                                                                <div class="cli-ent-xbox text-center">
                                                                                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                                                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                                                            <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                                                                        <?php } ?>
                                                                                                        <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div><!-- End Row -->
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                    <!--QA Completed Section End Here -->
                                                                                    <?php
                                                                                }
                                                                            }
                                                                        }

                                                                        public function search_ajax_request() {
                                                                            $this->myfunctions->checkloginuser("admin");
                                                                            $dataArray = $this->input->get('title');
                                                                            $dataStatus = $this->input->get('status');
                                                                            $dataStatus = explode(",", $dataStatus);
                                                                            $rows = $this->Request_model->GetAutocomplete_admin(array('keyword' => $dataArray), $dataStatus, "");
                                                                            $count_active_project = $this->Admin_model->count_active_project(array("active", "disapprove"), "", "", "", "", "status_admin");
        // echo "<pre>";print_r($rows);
                                                                            for ($i = 0; $i < sizeof($rows); $i++) {
                                                                                $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "admin");
                                                                                $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "admin");
                                                                                $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
                                                                                $commentcount = 0;
                                                                                for ($j = 0; $j < sizeof($getfileid); $j++) {

                                                                                    $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
                                                                                }
                                                                                if ($rows[$i]->status_admin == "active" || $rows[$i]->status_admin == "disapprove") {
                                                                                    if ($rows[$i]->expected_date == '' || $rows[$i]->expected_date == NULL) {
                                                                                        $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update, $rows[$i]->current_plan_name);
                                                                                    } else {
                                                                                        $rows[$i]->expected = $this->onlytimezone($rows[$i]->expected_date);
                                                                                    }
                                                                                } elseif ($rows[$i]->status_admin == "checkforapprove") {
                                                                                    $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->modified);
                                                                                } elseif ($rows[$i]->status_admin == "approved") {
                                                                                    $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
                                                                                } elseif ($rows[$i]->status_admin == "pendingrevision") {
                                                                                    $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
                                                                                }
                                                                                $rows[$i]->designer_project_count = $this->Request_model->get_designer_active_request($rows[$i]->designer_id);
                                                                                $rows[$i]->comment_count = $commentcount;
                                                                                $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, "admin");
                                                                                $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
                                                                            }
                                                                            for ($i = 0; $i < sizeof($rows); $i++) {
            // For Active Tab
                                                                                if ($rows[$i]->status_admin == "disapprove" || $rows[$i]->status_admin == "active") {
                                                                                    ?>
                                                                                    <!--QA Active Section Start Here -->
                                                                                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                                                                                        <!-- Start Row -->
                                                                                        <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                                                                                            <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'">
                                                                                                <div class="cli-ent-xbox">
                                                                                                    <p class="pro-a">
                                                                                                        <?php
                                                                                                        if ($rows[$i]->status == "active") {
                                                                                                            echo "Expected on";
                                                                                                        } elseif ($rows[$i]->status_admin == "disapprove") {
                                                                                                            echo "Expected on";
                                                                                                        }
                                                                                                        ?>
                                                                                                    </p>
                                                                                                    <p class="space-a"></p>
                                                                                                    <p class="pro-b">
                                                                                                        <?php
                                                                                                        if ($rows[$i]->status == "active") {
                                                                                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                                                                                        } elseif ($rows[$i]->status_admin == "disapprove") {
                                                                                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                                                                                        }
                                                                                                        ?>
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="cli-ent-col td" style="width: 34%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                                                                                                <div class="cli-ent-xbox text-left">
                                                                                                    <div class="cell-row">

                                                                                                        <div class="cell-col" >
                                                                                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                                                                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                                                                                        </div>

                                                                                                        <div class="cell-col col-w1">
                                                                                                            <p class="neft text-center">
                                                                                                                <?php if ($rows[$i]->status_admin == "active") { ?>
                                                                                                                    <span class="green text-uppercase text-wrap">In-Progress</span>
                                                                                                                <?php } elseif ($rows[$i]->status_admin == "disapprove" && $rows[$i]->who_reject == 1) { ?>
                                                                                                                    <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                                                                                                <?php } elseif ($rows[$i]->status_admin == "disapprove" && $rows[$i]->who_reject == 0) { ?>
                                                                                                                    <span class="red text-uppercase text-wrap">Quality Revision</span>
                                                                                                                <?php } ?>
                                                                                                            </p>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                                                                                                <div class="cli-ent-xbox text-left">
                                                                                                    <div class="cell-row">
                                                                                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                                            <a href="#" >
                                                                                                                <figure class="pro-circle-img">
                                                                                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                                                                    <?php } else { ?>
                                                                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                                                                    <?php } ?>
                                                                                                                </figure>
                                                                                                            </a>
                                                                                                        </div>
                                                                                                        <div class="cell-col">
                                                                                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                                                                                            ">
                                                                                                            <?php echo $rows[$i]->customer_first_name; ?>
                                                                                                            <?php
                                                                                                            if (strlen($rows[$i]->customer_last_name) > 5) {
                                                                                                               echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                                                                           } else {
                                                                                                               echo $rows[$i]->customer_last_name;
                                                                                                           }
                                                                                                           ?>

                                                                                                       </p>
                                                                                                       <p class="pro-b">Client</p>
                                                                                                   </div>
                                                                                               </div>
                                                                                           </div>
                                                                                       </div>

                                                                                       <div class="cli-ent-col td" style="width: 20%;">
                                                                                        <div class="cli-ent-xbox text-left">
                                                                                            <div class="cell-row">
                                                                                                <span class="count_project"><?php echo $rows[$i]->designer_project_count; ?></span>
                                                                                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                                                    <a href="#" class="<?php echo $rows[$i]->id; ?>">
                                                                                                        <figure class="pro-circle-img">
                                                                                                            <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                                                                            <?php } else { ?>
                                                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                                                                            <?php } ?>
                                                                                                        </figure>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                                                                                    <p class="text-h text-wrap"title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>
                                                                                                    ">
                                                                                                    <?php echo $rows[$i]->designer_first_name; ?>
                                                                                                    <?php
                                                                                                    if (strlen($rows[$i]->designer_first_name) > 5) {
                                                                                                       echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                                                                   } else {
                                                                                                       echo $rows[$i]->designer_last_name;
                                                                                                   }
                                                                                                   ?>
                                                                                               </p>
                                                                                               <p class="pro-b">Designer</p>
                                                                                               <p class="space-a"></p>
                                                                                               <?php
                                                                                               if ($rows[$i]->status == "active") {
                                                                                                if ($rows[$i]->designer_skills_matched == 0) {
                                                                                                    ?>
                                                                                                    <div class="checkbox custom-checkbox mob-txt-center  text-left " id="<?php echo $rows[$i]->id; ?>">
                                                                                                        <label>
                                                                                                            <input type="checkbox" class="checkcorrect" value="1" data-request="<?php echo $rows[$i]->id; ?>" onchange="check(this.value, '<?php echo $rows[$i]->id; ?>');"> 
                                                                                                            <span class="cr  right-cr   ">
                                                                                                                <i class="cr-icon fa fa-check"></i>
                                                                                                            </span> <br>
                                                                                                        </label>
                                                                                                    </div>
                                                                                                    <?php
                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                                                                <span class="sma-red">+</span> Add Designer
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'">
                                                                                <div class="cli-ent-xbox text-center">
                                                                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                                                            <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                                                <span class="numcircle-box">
                                                                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                                                </span>
                                                                                            <?php } ?>
                                                                                        </span>
                                                                                        <?php //echo $rows[$i]->total_chat_all;     ?></a></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'">
                                                                                    <div class="cli-ent-xbox text-center">
                                                                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                                                                                <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                                                                <?php } ?>
                                                                                                <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- End Row -->
                                                                                <?php } ?>
                <!--                    <input type="hidden" id="row" value="0">
                <input type="hidden" id="all" value="<?php //echo $count_active_project;       ?>">
                <div class="" style="display:block;text-align:center">
                <a href="javascript:void(0)" class="load_more button">Load more</a>
            </div>-->
            <!--QA Active Section End Here --> 
            <?php
        } elseif ($rows[$i]->status_admin == "assign") {
            ?>
            <!--QA IN Queue Section Start Here -->
            <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                <!-- Start Row -->
                <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                    <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                        <div class="cli-ent-xbox">
                            <p class="pro-a">In Queue</p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 36%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                        <div class="cli-ent-xbox text-left">
                            <div class="cell-row">

                                <div class="cell-col" >
                                    <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                    <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                </div>
                                <div class="cell-col"><?php echo isset($rows[$i]->priority) ? $rows[$i]->priority : ''; ?></div>
                                <div class="cell-col col-w1">
                                    <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                        <div class="cli-ent-xbox text-left">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                    <a href="#" >
                                        <figure class="pro-circle-img">
                                            <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                            <?php } else { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                            <?php } ?>
                                        </figure>
                                    </a>
                                </div>
                                <div class="cell-col">
                                    <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                    ">
                                    <?php echo $rows[$i]->customer_first_name; ?>
                                    <?php
                                    if (strlen($rows[$i]->customer_last_name) > 5) {
                                       echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                   } else {
                                       echo $rows[$i]->customer_last_name;
                                   }
                                   ?>

                               </p>
                               <p class="pro-b">Client</p>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="cli-ent-col td" style="width: 19%;">
                <div class="cli-ent-xbox text-left p-left1">
                    <div class="cell-row">
                        <h4 class="head-c draft_no">No Designer assigned yet</h4>
                    </div>

                </div>
            </div>
            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                <div class="cli-ent-xbox text-center">
                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                            <span class="numcircle-box">
                                <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                            </span>
                            <?php } ?></span>
                            <?php //echo $rows[$i]->total_chat_all;     ?></a></p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                        <div class="cli-ent-xbox text-center">
                            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                    <?php if (count($rows[$i]->total_files) != 0) { ?>
                                        <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                        <?php echo count($rows[$i]->total_files_count); ?>
                                    </a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>   
                    <!--QA IN Queue Section End Here --> 

                <?php } elseif ($rows[$i]->status_admin == "pendingrevision") {
                    ?>
                    <!--QA Pending Review Section Start Here -->
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Expected on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->revisiondate));
                                        ?>
                                    </p>
                                    <!-- <p class="pro-b">May 30, 2018</p> -->
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 30%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="#"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                            ">
                                            <?php echo $rows[$i]->customer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->customer_last_name) > 5) {
                                               echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                           } else {
                                               echo $rows[$i]->customer_last_name;
                                           }
                                           ?>

                                       </p>
                                       <p class="pro-b">Client</p>
                                   </div>
                               </div>

                           </div>
                       </div>

                       <div class="cli-ent-col td" style="width: 16%;">
                        <div class="cli-ent-xbox text-left">
                            <div class="cell-row">
                                <span class="count_project"><?php echo $rows[$i]->designer_project_count; ?></span>
                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                    <a href="#" class="<?php echo $rows[$i]->id; ?>"><figure class="pro-circle-img">
                                        <?php if ($rows[$i]->profile_picture != "") { ?>
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                        <?php } else { ?>
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                        <?php } ?>
                                    </figure></a>
                                </div>
                                <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                    <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                        <?php echo $rows[$i]->designer_first_name; ?>
                                        <?php
                                        if (strlen($rows[$i]->designer_last_name) > 5) {
                                            echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                        } else {
                                            echo $rows[$i]->designer_last_name;
                                        }
                                        ?>
                                    </p>
                                    <p class="pro-b">Designer</p>
                                    <p class="space-a"></p>
                                    <?php if ($rows[$i]->designer_assign_or_not == 0) { ?>
                                        <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                            <span class="sma-red">+</span> Add Designer
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                        <div class="cli-ent-xbox text-center">
                            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                    <span class="numcircle-box">
                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                    </span>
                                    <?php } ?></span></span>
                                    <?php //echo $rows[$i]->total_chat_all;      ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                            <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                        <?php } ?>
                                        <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                    </div>
                                </div>
                            </div> 
                            <!-- End Row -->
                        <?php } ?>
                        <!--QA Pending Review Section End Here -->
                        <?php
                // End Active Tab
                    } elseif ($rows[$i]->status_admin == "checkforapprove") {
                        ?>
                        <!--QA Pending Approval Section Start Here -->
                        <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                            <!-- Start Row -->
                            <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                                <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                    <div class="cli-ent-xbox">
                                        <p class="pro-a">Delivered on</p>
                                        <p class="space-a"></p>
                                        <p class="pro-b">
                                            <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate));
                                            ?>
                                        </p>
                                        <!-- <p class="pro-b">May 30, 2018</p> -->
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 34%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">

                                            <div class="cell-col" >
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4"><?php echo $rows[$i]->title; ?></a></h3>
                                                <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                            </div>

                                            <div class="cell-col col-w1">
                                                <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending Approval</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                <a href="#"><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                                ">
                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                           </p>
                                           <p class="pro-b">Client</p>
                                       </div>
                                   </div>

                               </div>
                           </div>

                           <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $rows[$i]->designer_project_count; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" class="<?php echo $rows[$i]->id; ?>"><figure class="pro-circle-img">
                                            <?php if ($rows[$i]->profile_picture != "") { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                            <?php } else { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                            <?php } ?>
                                        </figure></a>
                                    </div>
                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                            <?php echo $rows[$i]->designer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->designer_last_name;
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php if ($rows[$i]->designer_assign_or_not == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                        <span class="numcircle-box">
                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                        </span>
                                        <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;     ?></a></p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                            <?php } ?>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                        </div>
                                    </div>
                                </div> 
                                <!-- End Row -->
                            <?php } ?>
                            <!--QA Pending Approval Section End Here -->
                        <?php } elseif ($rows[$i]->status_admin == "approved") { ?>
                            <!--QA Completed Section Start Here -->
                            <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                                <!-- Start Row -->
                                <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5'"style="cursor: pointer;">
                                    <div class="cli-ent-col td" style="width: 13%;">
                                        <div class="cli-ent-xbox">
                                            <h3 class="app-roved green">Approved on</h3>
                                            <p class="space-a"></p>
                                            <p class="pro-b">
                                                <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate));
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 37%;">
                                        <div class="cli-ent-xbox text-left">
                                            <div class="cell-row">

                                                <div class="cell-col" >
                                                    <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5"><?php echo $rows[$i]->title; ?></a></h3>
                                                    <p class="pro-b">
                                                        <?php echo substr($rows[$i]->description, 0, 30); ?>
                                                    </p>
                                                </div>

                                                <div class="cell-col col-w1">
                                                    <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cli-ent-col td" style="width: 16%;">
                                        <div class="cli-ent-xbox text-left">
                                            <div class="cell-row">
                                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                    <a href="#"><figure class="pro-circle-img">
                                                        <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                        <?php } else { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                        <?php } ?>
                                                    </figure></a>
                                                </div>
                                                <div class="cell-col">
                                                    <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                        <?php echo $rows[$i]->customer_first_name; ?>
                                                        <?php
                                                        if (strlen($rows[$i]->customer_last_name) > 5) {
                                                            echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                        } else {
                                                            echo $rows[$i]->customer_last_name;
                                                        }
                                                        ?>

                                                    </p>
                                                    <p class="pro-b">Client</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="cli-ent-col td" style="width: 16%;">
                                        <div class="cli-ent-xbox text-left">
                                            <div class="cell-row">
                                                <span class="count_project"><?php echo $rows[$i]->designer_project_count; ?></span>
                                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                    <a href="#"  class="<?php echo $rows[$i]->id; ?>">
                                                        <figure class="pro-circle-img">
                                                            <?php if ($rows[$i]->profile_picture != "") { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                            <?php } else { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                            <?php } ?>
                                                        </figure>
                                                    </a>
                                                </div>
                                                <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                                    <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?> ">
                                                        <?php echo $rows[$i]->designer_first_name; ?>
                                                        <?php
                                                        if (strlen($rows[$i]->designer_last_name) > 5) {
                                                            echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                        } else {
                                                            echo $rows[$i]->designer_last_name;
                                                        }
                                                        ?>

                                                    </p>
                                                    <p class="pro-b">Designer</p>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 9%;">
                                        <div class="cli-ent-xbox text-center">
                                            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                    <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                        <span class="numcircle-box">
                                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                        </span>
                                                        <?php } ?></span>
                                                        <?php //echo $rows[$i]->total_chat_all;     ?></a></p>
                                                    </div>
                                                </div>
                                                <div class="cli-ent-col td" style="width: 9%;">
                                                    <div class="cli-ent-xbox text-center">
                                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5">
                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                            <?php } ?>
                                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                                        </div>
                                                    </div>
                                                </div> <!-- End Row -->
                                            <?php } ?>
                                            <!--QA Completed Section End Here -->
                                            <?php
                                        }
                                    }
                                }

                                public function search_ajax_qa() {
                                    $this->myfunctions->checkloginuser("admin");
                                    $dataArray = $this->input->get('name');
                                    if ($dataArray != "") {
                                        $qa = $this->Request_model->get_qa_member(array('keyword' => $dataArray));
                                        /* new */
                                        for ($i = 0; $i < sizeof($qa); $i++) {
                                            $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
                                            $qa[$i]['total_designer'] = sizeof($designers);

                                            $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);


                                            if ($mydesigner) {
                                                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                                                $qa[$i]['total_clients'] = sizeof($data);
                                            } else {
                                                $qa[$i]['total_clients'] = 0;
                                            }
                                        }
                                    } else {
                                        $qa = $this->Account_model->getqa_member();
                                        /* new */
                                        for ($i = 0; $i < sizeof($qa); $i++) {
                                            $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
                                            $qa[$i]['total_designer'] = sizeof($designers);

                                            $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);


                                            if ($mydesigner) {
                                                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                                                $qa[$i]['total_clients'] = sizeof($data);
                                            } else {
                                                $qa[$i]['total_clients'] = 0;
                                            }
                                        }
                                    }
                                    ?>
                                    <?php for ($i = 0; $i < sizeof($qa); $i++): ?>
                                        <!-- Start Row -->
                                        <div class="cli-ent-row tr brdr">

                                            <div class="cli-ent-col td" style="width: 65%;">
                                                <div class="cell-row">
                                                    <div class="cell-col" style="padding-right: 15px; width: 100px;">
                                                        <?php if ($qa[$i]['profile_picture'] != "") { ?>
                                                            <figure class="cli-ent-img circle one">
                                                                <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $qa[$i]['profile_picture']; ?>" class="img-responsive">
                                                            </figure>
                                                        <?php } else { ?>
                                                            <figure class="cli-ent-img circle one">
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                            </figure>
                                                        <?php } ?>
                                                    </div>

                                                    <div class="cell-col">
                                                        <h3 class="pro-head">
                                                            <a href="javascript:void(0)"><?php echo ucwords($qa[$i]['first_name'] . " " . $qa[$i]['last_name']); ?></a>
                                                            <a href="<?php echo base_url(); ?>admin/accounts/view_qa_profile/<?php echo $qa[$i]['id']; ?>">
                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                            </a>
                                                            <a href="#" data-toggle="modal" data-target="#<?php echo $qa[$i]['id']; ?>">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            </a>
                                                        </h3>
                                                        <p class="text-a">Member Since <?php echo date("F Y", strtotime($qa[$i]['created'])); ?></p>
                                                    </div>

                        <!--<div class="cell-col col-w1">
                                <p class="neft"><span class="blue text-uppercase">Premium Member</span></p>
                            </div>-->
                        </div>

                    </div>
                    <div class="cli-ent-col td" style="width: 20%;">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">No. of Designers</p>
                            <p class="space-a"></p>
                            <p class="neft text-center"><span class="green text-uppercase"><?php echo $qa[$i]['total_clients']; ?></span></p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 20%;">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Active Requests</p>
                            <p class="space-a"></p>
                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $qa[$i]['total_designer']; ?></span></p>
                        </div>
                    </div>
                    <!-- Edit QA Modal -->
                    <div class="modal fade" id="<?php echo $qa[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="cli-ent-model-box">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <form method="post" onSubmit="return EditFormSQa(<?php echo $qa[$i]['id']; ?>)" action="<?php echo base_url(); ?>admin/accounts/edit_qa/<?php echo $qa[$i]['id']; ?>">
                                        <div class="cli-ent-model">
                                            <header class="fo-rm-header">
                                                <h3 class="head-c">Edit QA</h3>
                                            </header>
                                            <div class="fo-rm-body">

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">First Name</label>
                                                            <p class="space-a"></p>
                                                            <input name="first_name" type="text" class="form-control input-c efirstname" value="<?php echo $qa[$i]['first_name']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Last Name</label>
                                                            <p class="space-a"></p>
                                                            <input type="text" name="last_name" class="form-control input-c elastname" value="<?php echo $qa[$i]['last_name']; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Email</label>
                                                            <p class="space-a"></p>
                                                            <input type="email" name="email" class="form-control input-c eemailid" value="<?php echo $qa[$i]['email']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Phone</label>
                                                            <p class="space-a"></p>
                                                            <input type="tel" name="phone"  class="form-control input-c ephone" value="<?php echo $qa[$i]['phone']; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr class="hr-b">

                                                <p class="space-c"></p>

                                                <div class="form-group goup-x1">
                                                    <label class="label-x3">Password</label>
                                                    <p class="space-a"></p>
                                                    <input type="password" name="password" class="form-control input-c epassword">
                                                </div>

                                                <div class="form-group goup-x1">
                                                    <label class="label-x3">Confirm Password</label>
                                                    <p class="space-a"></p>
                                                    <input type="password" name="confirm_password"  class="form-control input-c ecpassword">
                                                </div>
                                                <div class="epassError" style="display: none;">
                                                    <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                                </div>
                                                <p class="space-b"></p>

                                                <p class="btn-x"><button  type="submit" class="btn-g">Add Person</button></p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Row -->
                <?php
            endfor;
        }

        public function search_ajax_va() {
            $this->myfunctions->checkloginuser("admin");
            $dataArray = $this->input->get('name');
            if ($dataArray != "") {
                $va = $this->Request_model->get_va_member(array('keyword' => $dataArray));
                /* new */
                for ($i = 0; $i < sizeof($va); $i++) {
                    $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
                    $va[$i]['total_designer'] = sizeof($designers);

                    $mydesigner = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);


                    if ($mydesigner) {
                        $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                        $va[$i]['total_clients'] = sizeof($data);
                    } else {
                        $va[$i]['total_clients'] = 0;
                    }
                }
            } else {
                $va = $this->Account_model->getva_member();
                /* new */
                for ($i = 0; $i < sizeof($va); $i++) {
                    $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
                    $va[$i]['total_designer'] = sizeof($designers);

                    $mydesignerr = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);


                    if ($mydesignerr) {
                        $data = $this->Request_model->get_customer_list_for_qa("array", $mydesignerr);
                        $va[$i]['total_clients'] = sizeof($data);
                    } else {
                        $va[$i]['total_clients'] = 0;
                    }
                }
            }
            ?>
            <?php for ($i = 0; $i < sizeof($va); $i++): ?>
                <!-- Start Row -->
                <div class="cli-ent-row tr brdr">

                    <div class="cli-ent-col td" style="width: 65%;">
                        <div class="cell-row">
                            <div class="cell-col" style="padding-right: 15px; width: 100px;">
                                <?php if ($va[$i]['profile_picture'] != "") { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $va[$i]['profile_picture']; ?>" class="img-responsive">
                                    </figure>
                                <?php } else { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                    </figure>
                                <?php } ?>
                            </div>

                            <div class="cell-col">
                                <h3 class="pro-head">
                                    <a href="javascript:void(0)"><?php echo ucwords($va[$i]['first_name'] . " " . $va[$i]['last_name']); ?></a>
                                    <a href="#" data-toggle="modal" data-target="#<?php echo $va[$i]['id']; ?>">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                </h3>
                                <p class="text-a">Member Since <?php echo date("F Y", strtotime($va[$i]['created'])); ?></p>
                            </div>

                        <!--<div class="cell-col col-w1">
                                <p class="neft"><span class="blue text-uppercase">Premium Member</span></p>
                            </div>-->
                        </div>

                    </div>
                    <div class="cli-ent-col td" style="width: 20%;">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">No. of Designers</p>
                            <p class="space-a"></p>
                            <p class="neft text-center"><span class="green text-uppercase"><?php echo $va[$i]['total_clients']; ?></span></p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 20%;">
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Active Requests</p>
                            <p class="space-a"></p>
                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $va[$i]['total_designer']; ?></span></p>
                        </div>
                    </div>
                    <!-- Edit VA Modal -->
                    <div class="modal fade" id="<?php echo $va[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="cli-ent-model-box">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <form onSubmit="return EditFormSQa(<?php echo $va[$i]['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/accounts/edit_va/<?php echo $va[$i]['id']; ?>">
                                        <div class="cli-ent-model">
                                            <header class="fo-rm-header">
                                                <h3 class="head-c">Edit VA</h3>
                                            </header>
                                            <div class="fo-rm-body">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">First Name</label>
                                                            <p class="space-a"></p>
                                                            <input name="first_name" type="text" class="form-control input-c efirstname" value="<?php echo $va[$i]['first_name']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Last Name</label>
                                                            <p class="space-a"></p>
                                                            <input type="text" name="last_name" class="form-control input-c elastname" value="<?php echo $va[$i]['last_name']; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Email</label>
                                                            <p class="space-a"></p>
                                                            <input type="email" name="email" class="form-control input-c eemailid" value="<?php echo $va[$i]['email']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x3">Phone</label>
                                                            <p class="space-a"></p>
                                                            <input type="tel" name="phone"  class="form-control input-c ephone" value="<?php echo $va[$i]['phone']; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr class="hr-b">

                                                <p class="space-c"></p>

                                                <div class="form-group goup-x1">
                                                    <label class="label-x3">Password</label>
                                                    <p class="space-a"></p>
                                                    <input type="password" name="password" class="form-control input-c epassword">
                                                </div>

                                                <div class="form-group goup-x1">
                                                    <label class="label-x3">Confirm Password</label>
                                                    <p class="space-a"></p>
                                                    <input type="password" name="confirm_password"  class="form-control input-c ecpassword">
                                                </div>
                                                <div class="epassError" style="display: none;">
                                                    <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                                </div>
                                                <p class="space-b"></p>

                                                <p class="btn-x"><button  type="submit" class="btn-g">Add Person</button></p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Row -->
                <?php
            endfor;
        }

        public function request_response() {
            // $id = $this->Welcome_model->insert_data("request_files_review",$dataArr);
           //      if($id){
           //          $this->session->set_flashdata('msg-success', 'feedback submitted successfully');
           //      }else{
           //          $this->session->set_flashdata('msg-error', 'feebback not submitted');
           //      }
            $result = array();
            $this->myfunctions->checkloginuser("admin");
            $this->Welcome_model->update_online();
            $login_user_id = $this->load->get_var('login_user_id');
            $request_data = $this->Request_model->get_request_by_id($_POST['request_id']);
            $count_quality_rev = $request_data[0]['count_quality_revision'];
            $email = $this->Request_model->getuserbyid($request_data[0]['customer_id']);
            $is_approveDraftemailbyadmin = $this->Admin_model->approveDraftemailbyadmin();
           // $email_designer = $this->Request_model->getuserbyid($request_data[0]['designer_id']);
            $request_file = $this->Request_model->get_image_by_request_file_id($_POST['fileid']);
            $success = $this->Welcome_model->update_data("request_files", array("modified" => date("Y:m:d H:i:s"), "status" => $_POST['value']), array("id" => $_POST['fileid']));
            if ($success) {
                if ($_POST['value'] == "Reject") {
                    $_POST['value'] = "disapprove";
                    $count_rev = $count_quality_rev + 1;
                    $this->Welcome_model->update_data("requests", array("status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "who_reject" => 0,"count_quality_revision" => $count_rev), array("id" => $_POST['request_id']));
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'','reject_design_draft','admin_request_response',$login_user_id,'0','1','1');
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Design For \"" . $request_data[0]['title'] . "\" is Rejected..!", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);
                } elseif ($_POST['value'] == "customerreview") {
                    $_POST['value'] = "checkforapprove";
                    if ($request_data[0]['status'] != 'approved') {
                        $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_qa" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    } else {
                        $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status_qa" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    }
                    
                    $ActiveReq = $this->myfunctions->CheckActiveRequest($request_data[0]['customer_id']);
                    if ($ActiveReq == 1) {
                        $this->myfunctions->move_project_inqueqe_to_inprogress($request_data[0]['customer_id'],'','request_response',$_POST['request_id']);
                    }
                    $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($request_data[0]['customer_id'], $email[0]['plan_name']);
                    $this->myfunctions->send_email_after_admin_approval($request_data[0]['title'], $request_data[0]['id'], $email[0]['email'], $_POST['fileid'],$subdomain_url);
                    $send_emailto_subusers = $this->myfunctions->send_emails_to_sub_user($request_data[0]['customer_id'], 'approve/revision_requests', $is_approveDraftemailbyadmin, $request_data[0]['id']);
                    if (!empty($send_emailto_subusers)) {
                        foreach ($send_emailto_subusers as $send_emailto_subuser) {
                            $this->myfunctions->send_email_after_admin_approval($request_data[0]['title'], $request_data[0]['id'], $send_emailto_subuser['email'], $_POST['fileid'], $subdomain_url);
                        }
                    }
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'','approved_draft_design','admin_request_response',$login_user_id,'0','','1');
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'',$request_data[0]['status_admin'].'_to_'.$_POST['value'],'admin_request_response',$login_user_id,'1','','0');
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Upload New File For \"" . $request_data[0]['title'] . "\". Please Check And Approve..!", "customer/request/project_info/" . $_POST['request_id'], $request_data[0]['customer_id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Approved your \"" . $request_data[0]['title'] . "\" design", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);
                } else {
                    $_POST['value'] = "approved";
                    $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));

                    $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'',$request_data[0]['status_admin'].'_to_'.$_POST['value'],'admin_request_response',$login_user_id,'1');
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Design \"" . $request_data[0]['title'] . "\" is Approved..!", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);



                    $request2 = $this->Admin_model->get_all_request_count(array("active"), $request_data[0]['customer_id']);

                    if (sizeof($request2) <= 1) {
                        $all_requests = $this->Request_model->getall_request($request_data[0]['customer_id'], "'pending','assign'", "", "index_number");
                        if (isset($all_requests[0]['id'])) {
                            $success = $this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "dateinprogress" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $all_requests[0]['id']));
                        }
                    }
                }
                
                $feedbackColl = $_POST["form-input"];
                $dataArr = array(
                    "request_id" => $_POST['request_id'],
                    "draft_id" => $_POST['fileid'],
                    "user_role" => "admin",
                    "designer_id" => $request_data[0]['designer_id'],
                    "qa_id" => $login_user_id,
                    "q_feedback" => 1,
                    "q_instructions" => $feedbackColl[3]['value'],
                    "q_brand_guidelines" => $feedbackColl[4]['value'],
                    "q_attachments" => $feedbackColl[5]['value'],
                    "q_dimensions" => $feedbackColl[6]['value'],
                    "q_color_preferences" => $feedbackColl[7]['value'],
                    "q_required_text" => $feedbackColl[8]['value'],
                    "q_source_files" => $feedbackColl[9]['value'],
                    "q_font_files" => $feedbackColl[10]['value'],
                    "q_quality_review" => $feedbackColl[11]['value'],
                    "qa_feedback_date" => date("Y-m-d H:i:s"),
                );

                $data_q = $this->Welcome_model->select_data("id", "request_files_review", "request_id = '" . $_POST['request_id'] . "' AND draft_id = '" . $_POST['fileid'] . "'");
                if (empty($data_q)) {
                    $this->Welcome_model->insert_data("request_files_review", $dataArr);
                } else {
                    $this->Welcome_model->update_data("request_files_review", $dataArr, array("request_id" => $_POST['request_id'], "draft_id" => $_POST['fileid']));
                }
            }
            $modified_date = $this->onlytimezone(date("Y:m:d H:i:s"));
            $result['modified'] = date("M d, Y h:i A", strtotime($modified_date));
            $result['success'] = $success;
            echo json_encode($result);
            exit;
        }

        public function delete_plan($id) {
            $this->myfunctions->checkloginuser("admin");
            if ($id) {
                if ($this->Admin_model->delete_blog("subscription_plan", $id)) {
                    $this->session->set_flashdata("success", "Plan Deleted Successfully..!");
                } else {
                    $this->session->set_flashdata("error", "Plan Not Deleted Successfully..!");
                }
            }
            redirect(base_url() . "admin/dashboard/adminprofile?status=2");
        }

        public function edit_plan($id) {
            $this->myfunctions->checkloginuser("admin");
            $plan_id = isset($_POST['plan_id']) ? $_POST['plan_id'] : '';
            $allusers_info = $this->Request_model->get_user_by_plan_name($plan_id);
            $globarray = array(
                'global_active_request' => isset($_POST['global_active_request']) ? $_POST['global_active_request'] : '',
                'global_inprogress_request' => isset($_POST['global_inprogress_request']) ? $_POST['global_inprogress_request'] : '',
                'active_request' => isset($_POST['active_request']) ? $_POST['active_request'] : '',
                'turn_around_days' => isset($_POST['turn_around_days']) ? $_POST['turn_around_days'] : 1);
            if (!empty($_POST)) {
                if ($this->Admin_model->update_plan("subscription_plan", $globarray, $id)) {
                    if (!empty($allusers_info)) {
                        if ($_POST['overwite_setting'] == 1) {
                            foreach ($allusers_info as $allusersInfo) {
                                $this->Admin_model->update_data("users", array('total_active_req' => $_POST['global_active_request'], 'total_inprogress_req' => $_POST['global_inprogress_request'], 'total_requests' => $_POST['active_request'], 'turn_around_days' => $_POST['turn_around_days']), array("plan_name" => $plan_id));
                            }
                        } else {
                            foreach ($allusers_info as $allusersInfo) {
                                $this->Admin_model->update_data("users", array('total_active_req' => $_POST['global_active_request'], 'total_inprogress_req' => $_POST['global_inprogress_request'], 'total_requests' => $_POST['active_request'], 'turn_around_days' => $_POST['turn_around_days']), array("plan_name" => $plan_id, "overwrite" => 0));
                            }
                        }
                    }
                    $this->session->set_flashdata("success", "Plan Updated Successfully..!");
                } else {
                    $this->session->set_flashdata("error", "Plan Not Updated Successfully..!");
                }
            }
            redirect(base_url() . "admin/dashboard/adminprofile#billing");
        }

        public function onlytimezone($date_zone,$dateformat="") {
            $this->myfunctions->checkloginuser("admin");
            $nextdate = $this->myfunctions->onlytimezoneforall($date_zone,'',$dateformat);
            return $nextdate;
        }

         public function assign_designer_for_customer($checking_from='') {
            $this->myfunctions->checkloginuser("admin");
            $login_user_id = $this->load->get_var('login_user_id');


            $success_add = $this->Admin_model->update_data("users", array('designer_id' => $_POST['assign_designer']), array("id" => $_POST['customer_id']));
            $this->myfunctions->capture_project_activity('', '', $_POST['assign_designer'], 'assign_paramanent_designer', 'assign_designer_for_customer', $login_user_id, '', '', '', $_POST['customer_id']);
            $all_request = $this->Request_model->get_request_by_customer_id($_POST['customer_id']);
           foreach ($all_request as $request) {
                $request_id[] = $request['id'];
             $assign_id = $this->input->post('assign_designer'); 
             $checkDraft_id = $this->Welcome_model->select_data("draft_id,designer_id","activity_timeline","request_id='".$request['id']."' AND slug ='assign_designer' AND action_perform_by ='".$login_user_id."'",$limit="",$start="","desc","draft_id");
             $checkDraft_idForReas = $this->Welcome_model->select_data("draft_id,designer_id","activity_timeline","request_id='".$request['id']."' AND slug ='reassign_designer' AND action_perform_by ='".$login_user_id."' ",$limit="",$start="","desc","id");

           if(!empty($checkDraft_id) ){ 
            if(empty($checkDraft_idForReas) &&  $checkDraft_id[0]['draft_id'] == 0 && $checkDraft_id[0]['designer_id'] != 0){
                $new_assign_id =$assign_id;
                $assign_id =$checkDraft_id[0]['designer_id'];
                $slug='reassign_designer';
            }else if($checkDraft_idForReas[0]['draft_id'] != 0 && $checkDraft_idForReas[0]['designer_id'] != 0){
                $new_assign_id =$assign_id;
                $assign_id =$checkDraft_idForReas[0]['draft_id'];  
                $slug='reassign_designer';
            }
           }else{
               $new_assign_id = 0;
               $assign_id = $assign_id; 
               $slug='assign_designer';
           }
       $this->myfunctions->capture_project_activity($request['id'],$new_assign_id, $assign_id, $slug, 'assign_designer_for_customer', $login_user_id);

            //                $request_id[] = $request['id'];
            //            if ($request['designer_id'] != 0) {
            //                 $this->myfunctions->capture_project_activity($request['id'],$new_assign_id, $assign_id, $slug, 'assign_designer_for_customer', $login_user_id);
            //                //$this->myfunctions->capture_project_activity($request['id'], '', $request['designer_id'], 'reassign_designer', 'assign_designer_for_customer', $login_user_id);
            //            } else {
            //                $this->myfunctions->capture_project_activity($request['id'],$new_assign_id, $assign_id, $slug, 'assign_designer_for_customer', $login_user_id);
            //                //$this->myfunctions->capture_project_activity($request['id'], '', '', 'assign_designer', 'assign_designer_for_customer', $login_user_id);
            //            }
       }

        $this->Request_model->update_request_admin($request_id, array('designer_id' => $_POST['assign_designer']));
        if($checking_from=="change_req"){
            // die("hi"); 
            $data =array("status"=>"0"); 
           $this->Welcome_model->update_data("change_designer_request",$data,array("user_id"=>$_POST['customer_id'])); 
           $this->session->set_flashdata('message_success', "Assigned designer Successfully", 5);
           redirect(base_url() . "admin/Contentmanagement/change_designer_admin_view");
       }
        if ($success_add) {
             $data =array("status"=>"0"); 
           $this->Welcome_model->update_data("change_designer_request",$data,array("user_id"=>$_POST['customer_id'])); 
           $this->session->set_flashdata('message_success', "Assigned designer Successfully", 5);
            $this->session->set_flashdata('message_success', "Assigned designer Successfully", 5);

            redirect(base_url() . "admin/dashboard/view_clients");

        }
    }

        public function change_project_status() {
            $this->myfunctions->checkloginuser("admin");
            $login_user_id = $this->load->get_var('login_user_id');
            $request = $this->Request_model->get_request_by_id($_POST['request_id']);
            $count_quality_rev = $request[0]['count_quality_revision'];
            $subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
            $customer_data = $this->Admin_model->getuser_data($request[0]['customer_id']);
            $getsubscriptionplan = $this->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
            $getlatestfiledraft = $this->Request_model->get_latestdraft_file($_POST['request_id']);
            if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
                $turn_around_days = $customer_data[0]['turn_around_days'];
            } else {
                $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
            }
            $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $request[0]['category_bucket'], $subcat_data[0]['timeline'], $turn_around_days);
            if ($request[0]['status'] == 'assign') {
                if (isset($_POST['value'])) {
                    $deletedpriority = isset($request[0]['priority']) ? $request[0]['priority'] : '';
                    $user_id = isset($request[0]['customer_id']) ? $request[0]['customer_id'] : '';
                    $priorfrom = $deletedpriority + 1;
                    if ($request[0]['latest_update'] == '' || $request[0]['latest_update'] == null) {
                        $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    } else {
                        $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    }
                    $this->Admin_model->update_priority_after_approved_qa(1, $priorfrom, $user_id);
                }
            }

            if ($_POST['value'] == "assign") {
                $priority = $this->Welcome_model->priority($request[0]['customer_id']);
                $data2['priority'] = $priority[0]['priority'] + 1;
                if($request[0]['created_by'] != $request[0]['customer_id'] || $request[0]['created_by'] != 0){
                    $sub_user_priority = $CI->Welcome_model->sub_user_priority($request[0]['created_by']);
                    $data2['sub_user_priority'] = $sub_user_priority[0]['sub_user_priority'] + 1;
                }else{
                    $data2['sub_user_priority'] = 0;
                }
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => $data2['priority'],"sub_user_priority" => $data2['sub_user_priority'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                   $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "active") {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                if ($ActiveReq == 1) {
                    $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status',$_POST['request_id']);
                }
                $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                $message_count = $this->Request_model->request_discussions_count_for_qa_message($_POST['request_id']);
                if ($message_count == 0) {
                    $this->myfunctions->SendQAmessagetoInprogressReq($_POST['request_id'], $customer_data[0]['qa_id'], $request[0]['customer_id']);
                }
                if ($success_add) {
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "disapprove") {
                $count_rev = $count_quality_rev + 1;
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "sub_user_priority" => 0,"who_reject" => 1,"count_quality_revision" => $count_rev,"verified" => 0, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status'] . "..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status_qa'] . "..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status_designer'] . "..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                   echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "checkforapprove") {
                $success_add = $this->Admin_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), 'status' => $_POST['value'], 'latest_update' => date("Y-m-d H:i:s"), 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1,), array("id" => $_POST['request_id']));
                if ($success_add) {
                    $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                     if ($ActiveReq == 1) {
                         $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status',$_POST['request_id']);
                     }
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Upload New File For \"" . $request[0]['title'] . "\" . Please Check And Approve..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Approved your design For \"" . $request[0]['title'] . "\"", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "approved") {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                    if ($ActiveReq == 1) {
                        $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status',$_POST['request_id']);
                    }
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            }elseif ($_POST['value'] == "cancel") {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                    if ($ActiveReq == 1) {
                        $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status',$_POST['request_id']);
                    }
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            } else {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            }
        }

        public function deletecustomer() {
            $this->myfunctions->checkloginuser("admin");
            $all_request_id = array();
            $file_id = array();
            $file_name = array();
            $all_requests = $this->Account_model->get_all_request_by_customer($_POST['id']);

            foreach ($all_requests as $request) {
                $all_request_id[] = $request['id'];
            }
            if (!empty($all_request_id)) {
                $this->Admin_model->delete_request_data("request_discussions", $all_request_id);
                $this->Admin_model->delete_request_data("message_notification", $all_request_id);
                $this->Admin_model->delete_request_data("notification", $all_request_id);

                $all_files = $this->Admin_model->get_all_files_rid($all_request_id);
                foreach ($all_files as $files) {
                    $file_id[] = $files['id'];
                }
            }

            if (!empty($all_requests)) {
                foreach ($all_requests as $request_all) {
                //echo $request_all;
                    $all_files_nm = $this->Admin_model->get_all_files_rid($request_all['id']);
                    $folderNmae = 'public/uploads/requests/' . $request_all['id'];
                    foreach ($all_files_nm as $files) {
                        $file_name[] = $files['file_name'];
                        $file_name[] = $files['src_file'];
                    }
                // echo "<pre>";print_r($file_name);exit;
                    foreach ($file_name as $req_files) {
                        if (in_array($req_files, array(".", "..")))
                            continue;
                        $this->load->library('s3_upload');
                        $this->s3_upload->delete_file_from_s3($folderNmae . '/' . $req_files);
                        $this->s3_upload->delete_file_from_s3($folderNmae . '/_thumb/' . $req_files);
                        $this->s3_upload->delete_file_from_s3($folderNmae);
                    }

                // $path = FCPATH . "public/uploads/requests/" . $request_all['id'] . "/*";
                // $path1 = FCPATH . "public/uploads/requests/" . $request_all['id'];
                // if (file_exists($path1)) {
                // $files = glob($path); // get all file names
                // foreach ($files as $file) { // iterate files
                // if (is_file($file))
                // unlink($file); // delete file
                // }
                // rmdir($path1);
                    $all_requests_id[] = $request_all['id'];
                // }
                }
            }
            if (!empty($file_id)) {
                $this->Admin_model->delete_request_file_data("request_file_chat", $file_id);
                $this->Admin_model->delete_request_data("request_files", $all_request_id);
            }
            if (!empty($all_request_id)) {
                $this->Admin_model->alldelete_request('requests', $all_request_id);
            }
            $this->Admin_model->customer_delete($_POST['id']);
            echo "1";
        }

        public function send_email_after_adminapprove($project_title, $id, $email) {
            require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
            require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
            require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
            $data = array(
                'title' => $project_title,
                'project_id' => $id
            );
            $body = $this->load->view('emailtemplate/qa_approved_template', $data, TRUE);
            $receiver = $email;

            $subject = 'Admin Approved';

            $message = $body;

            $title = 'GraphicsZoo';
        }

        public function send_email_when_inqueue_to_inprogress($project_title, $id, $custemail) {
            require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
            require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
            require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
            $receiver = $custemail;
            $query1 = $this->db->select('first_name')->from('users')->where('email', $receiver)->get();
            $fname = $query1->result();
            $data = array(
                'title' => $project_title,
                'project_id' => $id,
                'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
            );
            $body = $this->load->view('emailtemplate/inqueue_template', $data, TRUE);
            $subject = 'Designer has Started Working on your New Project.';

            $message = $body;

            $title = 'GraphicsZoo';

        //SM_sendMail($receiver, $subject, $message, $title);
        }

        public function send_email_after_qa_approval_for_designer($file_id, $project_id, $status, $approveby, $email) {
            require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
            require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
            require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
            $data = array(
                'file_id' => $file_id,
                'project_id' => $project_id,
                'status' => $status,
                'appby' => $approveby
            );
            $body = $this->load->view('emailtemplate/qa_approvel_for_designer_template', $data, TRUE);
            $receiver = $email;

            $subject = 'Admin approved your design';

            $message = $body;

            $title = 'GraphicsZoo';

        }

        public function load_more($loadstatus="",$loadsearch="") {
            $this->myfunctions->checkloginuser("admin");
            $group_no = $this->input->post('group_no');
            $client_id = $this->input->post('client_id');
            $bkurl = $this->input->post('bkurl');
            $designer_id = $this->input->post('designer_id');
            $bucket_type = (!empty($_POST['bucket_type'])) ? $_POST['bucket_type'] : '';
            $dataStatusText = ($this->input->post('status') != "")?$this->input->post('status'):$loadstatus;
            $status = explode(",", $dataStatusText);
            $search = ($this->input->post('search') != "")?$this->input->post('search'):$loadsearch;
            $content_per_page = LIMIT_ADMIN_LIST_COUNT;
            $start = ceil($group_no * $content_per_page);
            $va_assignproject = $this->input->post('assigned_project');
            if($search != ""){
                $this->setsessionforbackbutton("dashboard",$dataStatusText,$search);
            }else{
               $this->setsessionforbackbutton("dashboard",$dataStatusText); 
            }   
            $count_load_more = $this->Request_model->admin_load_more($status, true, $start, $content_per_page, "", $search, $bucket_type,"",$va_assignproject,$client_id,$designer_id);
            if (is_array($status)) {
                $all_content = $this->Request_model->admin_load_more($status, "", $start, $content_per_page, "", $search, $bucket_type,"",$va_assignproject,$client_id,$designer_id);
            } else {
                $all_content = $this->Request_model->admin_load_more($status, "", $start, $content_per_page, "", $search, $bucket_type,"",$va_assignproject,$client_id,$designer_id);
            }
            for ($i = 0; $i < sizeof($all_content); $i++) {
                $all_content[$i]['category_data'] = $this->Request_model->getRequestCatbyid($all_content[$i]['id'], $all_content[$i]['category_id']);
                if (isset($all_content[$i]['category_data'][0]['category']) && $all_content[$i]['category_data'][0]['category'] != '') {
                    $all_content[$i]['category_name'] = $all_content[$i]['category_data'][0]['category'];
                } else {
                    $all_content[$i]['category_name'] = $all_content[$i]['category_data'][0]['cat_name'];
                }
                $all_content[$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($all_content[$i]['id'], $all_content[$i]['category_id'], $all_content[$i]['subcategory_id']);
                $all_content[$i]['total_chat'] = $this->Request_model->get_chat_number($all_content[$i]['id'], $all_content[$i]['customer_id'], $all_content[$i]['designer_id'], "admin");
                $all_content[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($all_content[$i]['id'], $_SESSION['user_id'], "admin");
                $getfileid = $this->Request_model->get_attachment_files($all_content[$i]['id'], "designer");
                $commentcount = 0;
                for ($j = 0; $j < sizeof($getfileid); $j++) {
                    $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
                }
                if ($all_content[$i]['status_admin'] == "active" || $all_content[$i]['status_admin'] == "disapprove") {
                    if ($all_content[$i]['expected_date'] == '' || $all_content[$i]['expected_date'] == NULL) {
                        $all_content[$i]['expected'] = $this->myfunctions->check_timezone($all_content[$i]['latest_update'], $all_content[$i]['current_plan_name']);
                    } else {
                        $all_content[$i]['expected'] = $this->onlytimezone($all_content[$i]['expected_date']);
                    }
                } elseif ($all_content[$i]['status_admin'] == "checkforapprove") {
                    $all_content[$i]['reviewdate'] = $this->onlytimezone($all_content[$i]['modified']);
                } elseif ($all_content[$i]['status_admin'] == "approved") {
                    $all_content[$i]['approvddate'] = $this->onlytimezone($all_content[$i]['approvaldate']);
                } elseif ($all_content[$i]['status_admin'] == "pendingrevision") {
                    $all_content[$i]['revisiondate'] = $this->onlytimezone($all_content[$i]['modified']);
                }
                if ($all_content[$i]['designer_id'] != "" && $all_content[$i]['designer_id'] != 0) {
                    $all_content[$i]['designer_project_count'] = $this->Request_model->get_designer_active_request($all_content[$i]['designer_id']);
                } else {
                    $all_content[$i]['designer_project_count'] = "";
                }
                $all_content[$i]['comment_count'] = $commentcount;
                $all_content[$i]['total_file'] = $this->Request_model->get_files_count($all_content[$i]['id'], $all_content[$i]['designer_id'], "admin");
                $all_content[$i]['total_files_count'] = $this->Request_model->get_files_count_all($all_content[$i]['id']);
                $all_content[$i]['usertimezone_date'] = $all_content[$i]['user_timezone'];
                $all_content[$i]['addClass'] = $this->adminCheckClassforDesignerAssign($all_content[$i]['status_admin']);

            }
            if (isset($all_content) && is_array($all_content) && count($all_content)) {
                for ($i = 0; $i < count($all_content); $i++) {
                            $data['projects'] = $all_content[$i];  
                            $data['projects']['bkurl'] = $bkurl; 
                            $this->load->view('admin/project_listing_temp',$data); 
                    }
                ?>
                <?php } if($count_load_more > 0 || $count_load_more != ''){ ?><span id="loadingAjaxCount" data-value="<?php echo $count_load_more; ?>"></span>
                <script> countTimer();</script>
                <?php
        }}

    public function load_more_customer() {
                $this->myfunctions->checkloginuser("admin");
                $isfullaccess = $this->hasAdminfullaccess('full_admin_access','admin');
               // echo "hii".$isfullaccess;exit;
                $group_no = $this->input->post('group_no');
                $search = $this->input->post('search');
                $status = $this->input->post('status');
                $va_assignproject = $this->input->post('assign_project');
                $isonetimeuser = $this->input->post('isonetime');
                $content_per_page = LIMIT_ADMIN_LIST_COUNT;
                $start = ceil($group_no * $content_per_page);
                if($search != ""){
                  $this->setsessionforbackbutton("view_vlients",$status,$search); 
                }else{
                  $this->setsessionforbackbutton("view_vlients",$status);   
                }
                $planlist = $this->Stripe->getallsubscriptionlist();
                if ($status == "paid") {
                    $status_pass = "paid";
                    $onetime = $isonetimeuser;
                    $cancelsubuser = "";
                }elseif($status == "free"){
                    $status_pass = "free";
                    $onetime = $isonetimeuser;
                    $cancelsubuser = "";
                }elseif($status == "request_based"){
                    $status_pass = ""; 
                    $onetime = "1";
                    $cancelsubuser = "";
                }else{
                    $status_pass = ""; 
                    $onetime = "";
                     $cancelsubuser = "1";
                }
                    $countcustomer = $this->Account_model->clientlist_load_more($status_pass, "", true, $start, $content_per_page, '', $search, $onetime,$cancelsubuser,$va_assignproject);
                    $activeClients = $this->Account_model->clientlist_load_more($status_pass, "", false, $start, $content_per_page, '', $search, $onetime,$cancelsubuser,$va_assignproject);
                    for ($i = 0; $i < sizeof($activeClients); $i++) {
                        $activeClients[$i]['total_rating'] = $this->Request_model->get_average_rating($activeClients[$i]['id']);
                        $activeClients[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($activeClients[$i]['profile_picture']);
                        $activeClients[$i]['designer'][0]['profile_picture'] = $this->customfunctions->getprofileimageurl($activeClients[$i]['designer'][0]['profile_picture']);
                        $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $activeClients[$i]['id']);
                        $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $activeClients[$i]['id']);
                        $revision = $this->Account_model->get_all_active_view_request_by_customer(array('disapprove'), $activeClients[$i]['id']);
                        $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $activeClients[$i]['id']);
                        $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $activeClients[$i]['id']);
                        $hold = $this->Account_model->get_all_active_view_request_by_customer(array('hold'), $activeClients[$i]['id']);
                        $cancelled = $this->Account_model->get_all_active_view_request_by_customer(array('cancel'), $activeClients[$i]['id']);
                        $user = $this->Account_model->get_all_active_request_by_customer($activeClients[$i]['id']);
                        $plan_data = $this->Request_model->getsubscriptionlistbyplanid($activeClients[$i]['plan_name']);
                        $activeClients[$i]['created'] = $this->onlytimezone($activeClients[$i]['created'],'M/d/Y');
                        if($activeClients[$i]['billing_end_date'] != ''){
                            $activeClients[$i]['billing_end_date'] = $this->onlytimezone($activeClients[$i]['billing_end_date'],'M/d/Y');
                        }else{
                            $activeClients[$i]['billing_end_date'] = 'N/A';
                        }
                        $activeClients[$i]['addClass'] = $status;
                        $activeClients[$i]['active'] = $active;
                        $activeClients[$i]['inque_request'] = $inque;
                        $activeClients[$i]['revision_request'] = $revision;
                        $activeClients[$i]['review_request'] = $review;
                        $activeClients[$i]['complete_request'] = $complete;
                        $activeClients[$i]['hold'] = $hold;
                        $activeClients[$i]['cancelled'] = $cancelled;
                        $activeClients[$i]['total_request'] = ($active + $inque + $revision + $review + $complete + $hold + $cancelled);
                        $activeClients[$i]['isfullaccess'] = $isfullaccess;
                        $activeClients[$i]['client_plan'] = (isset($plan_data[0]['plan_name']) && $plan_data[0]['plan_name'] != '') ? $plan_data[0]['plan_name'] : 'No Member';
                       if ($activeClients[$i]['designer_id'] != 0) {
                            $activeClients[$i]['designer'] = $this->Request_model->get_user_by_id($activeClients[$i]['designer_id']);
                        } else {
                            $activeClients[$i]['designer'] = "";
                        }
                        $activeClients[$i]['va'] = $this->Request_model->get_user_by_id($activeClients[$i]['va_id']);
                        $activeClients[$i]['am'] = $this->Request_model->get_user_by_id($activeClients[$i]['am_id']);
                        }
                    for ($i = 0; $i < sizeof($activeClients); $i++) {
                       $data['clients'] = $activeClients[$i];
                       $this->load->view('admin/view_clients_template', $data);
                    }if($countcustomer != 0 || $countcustomer != ''){
                    ?>
                        <span id="loadingAjaxCount" data-value="<?php echo $countcustomer; ?>"></span>
                <?php 
                    }
                    $data["all_designers"] = $this->Request_model->get_all_designers();
                    for ($i = 0; $i < sizeof($data['all_designers']); $i++) {
                        $user = $this->Account_model->get_all_active_request_by_designer($data['all_designers'][$i]['id']);
                        $data['all_designers'][$i]['active_request'] = sizeof($user);
                    }
                    $subscription_plan_list = array();
                    $j = 0;
                    for ($i = 0; $i < sizeof($planlist); $i++) {
                        if ($planlist[$i]['id'] == '30291' || $planlist[$i]['id'] == 'A21481D' || $planlist[$i]['id'] == 'A1900S' || $planlist[$i]['id'] == 'M99S' || $planlist[$i]['id'] == 'M199G' || $planlist[$i]['id'] == 'M1991D' || $planlist[$i]['id'] == 'M199S' || $planlist[$i]['id'] == 'A10683D' || $planlist[$i]['id'] == 'M993D') {
                            continue;
                        }
                        $subscription_plan_list[$j]['id'] = $planlist[$i]['id'];
                        $subscription_plan_list[$j]['name'] = $planlist[$i]['name'] . " - $" . $planlist[$i]['amount'] / 100 . "/" . $planlist[$i]['interval'];
                        $subscription_plan_list[$j]['amount'] = $planlist[$i]['amount'] / 100;
                        $subscription_plan_list[$j]['interval'] = $planlist[$i]['interval'];
                        $subscription_plan_list[$j]['trial_period_days'] = $planlist[$i]['trial_period_days'];
                        $j++;
                    }
            }

    public function load_more_clientprojects() {
            $this->myfunctions->checkloginuser("admin");
            $status_role = $this->input->post('status');
            $status = explode(',', $status_role);
            $group_no = $this->input->post('group_no');
            $search = $this->input->post('search');
            $datauserid = $this->input->post('id');
            $content_per_page = LIMIT_ADMIN_LIST_COUNT;
            $start = ceil($group_no * $content_per_page);
            $count_rows = $this->Account_model->customer_projects_load_more($status, $datauserid, true, $start, $content_per_page, '', $search);
            $rows = $this->Account_model->customer_projects_load_more($status, $datauserid, false, $start, $content_per_page, '', $search);
    //echo "<pre>";print_r($rows);
    //$rows = $this->Request_model->GetAutocomplete_for_admin(array('keyword' => $dataArray), $dataStatus, $datauserid, "admin");
            for ($i = 0; $i < sizeof($rows); $i++) {
                $rowobj = (object) $rows[$i];
        //echo "<pre>";print_r($rowobj);
                $rowobj->total_chat = $this->Request_model->get_chat_number($rowobj->id, $rowobj->customer_id, $rowobj->designer_id, "admin");
                $rowobj->total_chat_all = $this->Request_model->get_total_chat_number($rowobj->id, $_SESSION['user_id'], "admin");
                $getfileid = $this->Request_model->get_attachment_files($rowobj->id, "designer");
                $commentcount = 0;
                for ($j = 0; $j < sizeof($getfileid); $j++) {
                    $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
                }
                if ($rowobj->status == "active" || $rowobj->status == "disapprove") {
                    if ($rowobj->expected_date == '' || $rowobj->expected_date == NULL) {
                        $rowobj->expected = $this->myfunctions->check_timezone($rowobj->latest_update, $rowobj->current_plan_name);
                    } else {
                        $rowobj->expected = $this->onlytimezone($rowobj->expected_date);
                    }
                } elseif ($rowobj->status == "checkforapprove") {
            //$rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->latest_update);
                    $rowobj->reviewdate = $this->onlytimezone($rowobj->modified);
                } elseif ($rowobj->status == "hold" || $rowobj->status == "cancel") {
            //$rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->latest_update);
                    $rowobj->modified = $this->onlytimezone($rowobj->modified);
                } elseif ($rowobj->status == "approved") {
                    $rowobj->approvddate = $this->onlytimezone($rowobj->approvaldate);
                }
                if ($rowobj->status_admin == "pendingrevision") {
                    $rowobj->revisiondate = $this->onlytimezone($rowobj->modified);
                }
                $rowobj->comment_count = $commentcount;
                $rowobj->total_files = $this->Request_model->get_files_count($rowobj->id, $rowobj->designer_id, "admin");
                $rowobj->total_files_count = $this->Request_model->get_files_count_all($rowobj->id);
            }
            for ($i = 0; $i < sizeof($rows); $i++) {
                $rowobj = (object) $rows[$i];
                if ($rowobj->status == "disapprove" || $rowobj->status == "active" || $rowobj->status == "assign") {
                    ?>
                    <!--QA Active Section Start Here -->
                    <?php
                    for ($i = 0; $i < sizeof($rows); $i++) {
                        if ($rowobj->status == "disapprove" || $rowobj->status == "active") {
                            ?>
                            <!-- Start Row -->
                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1'" style="cursor: pointer;">
                                <div class="cli-ent-col td" style="width: 12%;">
                                    <div class="cli-ent-xbox">
                                        <p class="pro-a">
                                            <?php
                                            if ($rowobj->status == "active") {
                                                echo "Expected on";
                                            } elseif ($rowobj->status == "disapprove") {
                                                echo "Expected on";
                                            } elseif ($rowobj->status == "assign") {
                                                echo "In-Queue";
                                            }
                                            ?>
                                        </p>
                                        <p class="space-a"></p>
                                        <p class="pro-b">
                                            <?php
                                            if ($rowobj->status == "active") {
                                                echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                            } elseif ($rows[$i]->status == "disapprove") {
                                                echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 48%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">

                                            <div class="cell-col" >
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1"><?php echo $rowobj->title; ?></a></h3>
                                                <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                            </div>

                                            <div class="cell-col col-w1">
                                                <p class="neft text-center">
                                                    <?php if ($rowobj->status == "active" || $rowobj->status == "checkforapprove") { ?>
                                                        <span class="green text-uppercase">In-Progress</span>
                                                    <?php } elseif ($rowobj->status == "disapprove") { ?>
                                                        <span class="red orangetext text-uppercase">REVISION</span>
                                                    <?php } elseif ($rowobj->status == "assign") { ?>
                                                        <span class="gray text-uppercase">In-Queue</span>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 20%;">
                                    <div class="cli-ent-xbox text-left p-left1">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                        <figure class="pro-circle-img">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture; ?>" class="img-responsive">
                                                        </figure>
                                                    <?php } else { ?>
                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                            <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                        </figure>
                                                    <?php } ?>
                                                </a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h">
                                                    <?php
                                                    if ($rowobj->designer_first_name) {
                                                        echo $rowobj->designer_first_name;
                                                    } else {
                                                        echo "No Designer";
                                                    }
                                                    ?>  
                                                </p>
                                                <p class="pro-b">Designer</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rowobj->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                    <?php } ?></span>
                                                    <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                                </div>
                                            </div>
                                            <div class="cli-ent-col td" style="width: 10%;">
                                                <div class="cli-ent-xbox text-center">
                                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                            <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span></span>
                                                        <?php } ?>
                                                        <?php echo count($rowobj->total_files_count); ?></a></p>
                                                    </div>
                                                </div>
                                            </div> <!-- End Row -->
                                            <?php
                                        }
                                        if ($rowobj->status == "assign") {
                                            ?>
                                            <!-- Start Row -->
                                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                                                <div class="cli-ent-col td" style="width: 12%;">
                                                    <div class="cli-ent-xbox">
                                                        <p class="pro-a">
                                                            <?php
                                                            if ($rowobj->status == "active") {
                                                                echo "Expected on";
                                                            } elseif ($rowobj->status == "disapprove") {
                                                                echo "Expected on";
                                                            } elseif ($rowobj->status == "assign") {
                                                                echo "In-Queue";
                                                            }
                                                            ?>
                                                        </p>
                                                        <p class="space-a"></p>
                                                        <p class="pro-b">
                                                            <?php
                                                            if ($rowobj->status == "active") {
                                                                echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                                            } elseif ($rows[$i]->status == "disapprove") {
                                                                echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                                            }
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="cli-ent-col td" style="width: 48%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cell-row">

                                                            <div class="cell-col" >
                                                                <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1"><?php echo $rowobj->title; ?></a></h3>
                                                                <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                                            </div>

                                                            <div class="cell-col col-w1">
                                                                <p class="neft text-center">
                                                                    <?php if ($rowobj->status == "active" || $rowobj->status == "checkforapprove") { ?>
                                                                        <span class="green text-uppercase">In-Progress</span>
                                                                    <?php } elseif ($rowobj->status == "disapprove") { ?>
                                                                        <span class="red orangetext text-uppercase">REVISION</span>
                                                                    <?php } elseif ($rowobj->status == "assign") { ?>
                                                                        <span class="gray text-uppercase">In-Queue</span>
                                                                    <?php } ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 20%;">
                                                    <div class="cli-ent-xbox text-left p-left1">
                                                        <div class="cell-row">
                                                            <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                                <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                                        <figure class="pro-circle-img">
                                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture; ?>" class="img-responsive">
                                                                        </figure>
                                                                    <?php } else { ?>
                                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                            <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                                        </figure>
                                                                    <?php } ?>
                                                                </a>
                                                            </div>
                                                            <div class="cell-col">
                                                                <p class="text-h">
                                                                    <?php
                                                                    if ($rowobj->designer_first_name) {
                                                                        echo $rowobj->designer_first_name;
                                                                    } else {
                                                                        echo "No Designer";
                                                                    }
                                                                    ?>  
                                                                </p>
                                                                <p class="pro-b">Designer</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="cli-ent-col td" style="width: 10%;">
                                                    <div class="cli-ent-xbox text-center">
                                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                                                <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                                    <span class="numcircle-box">
                                                                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                                    </span>
                                                                    <?php } ?></span>
                                                                    <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                                                                </div>
                                                            </div>
                                                            <div class="cli-ent-col td" style="width: 10%;">
                                                                <div class="cli-ent-xbox text-center">
                                                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                                            <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span></span>
                                                                        <?php } ?>
                                                                        <?php echo count($rowobj->total_files_count); ?></a></p>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- End Row -->
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <!--QA Active Section End Here -->
                                                    <?php
                                                } elseif ($rowobj->status == "checkforapprove") {
                                                    ?>
                                                    <?php
                                                    for ($i = 0; $i < sizeof($rows); $i++) {
                                                        $rowobj = (object) $rows[$i];
                                                        ?>
                                                        <!-- Start Row -->
                                                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2'"  style="cursor: pointer;">
                                                            <div class="cli-ent-col td" style="width: 10%;">
                                                                <div class="cli-ent-xbox">
                                                                    <p class="pro-a">
                                                                        Delivered on
                                                                    </p>
                                                                    <p class="space-a"></p>
                                                                    <p class="pro-b">
                                                                        <?php echo date('M d, Y h:i A', strtotime($rowobj->reviewdate));
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="cli-ent-col td" style="width: 40%;">
                                                                <div class="cli-ent-xbox text-left">
                                                                    <div class="cell-row">
                                                                        <div class="cell-col">
                                                                            <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/2"><?php echo $rowobj->title; ?></a></h3>
                                                                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                                                        </div>

                                                                        <div class="cell-col col-w1">
                                                                            <p class="neft text-center">
                                                                                <span class="red bluetext text-uppercase text-wrap">Pending-approval</span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="cli-ent-col td" style="width: 16%;">
                                                                <div class="cli-ent-xbox text-left">
                                                                    <div class="cell-row">
                                                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                            <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/2">
                                                                                <?php if ($rowobj->customer_profile_picture != "") { ?>
                                                                                    <figure class="pro-circle-img">
                                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture; ?>" class="img-responsive">
                                                                                    </figure>
                                                                                <?php } else { ?>
                                                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                                        <?php echo ucwords(substr($rowobj->customer_first_name, 0, 1)) . ucwords(substr($rowobj->customer_last_name, 0, 1)); ?>
                                                                                    </figure>
                                                                                <?php } ?>
                                                                            </a>
                                                                        </div>
                                                                        <div class="cell-col">
                                                                            <p class="text-h text-wrap" title="<?php echo $rowobj->customer_first_name . " " . $rowobj->customer_last_name; ?>">
                                                                                <?php echo $rowobj->customer_first_name; ?>
                                                                                <?php
                                                                                if (strlen($rowobj->customer_last_name) > 5) {
                                                                                    echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                                                                } else {
                                                                                    echo $rowobj->customer_last_name;
                                                                                }
                                                                                ?>
                                                                            </p>
                                                                            <p class="pro-b">Client</p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="cli-ent-col td" style="width: 16%;">
                                                                <div class="cli-ent-xbox text-left">
                                                                    <div class="cell-row">
                                                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                            <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/2">
                                                                                <?php if ($rowobj->profile_picture != "") { ?>
                                                                                    <figure class="pro-circle-img">
                                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture; ?>" class="img-responsive">
                                                                                    </figure>
                                                                                <?php } else { ?>
                                                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                                        <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                                                    </figure>
                                                                                <?php } ?>
                                                                            </a>
                                                                        </div>
                                                                        <div class="cell-col">
                                                                            <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                                                <?php
                                                                                if ($rowobj->designer_first_name) {
                                                                                    echo $rowobj->designer_first_name;
                                                                                    if (strlen($rowobj->designer_last_name) > 5) {
                                                                                        echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                                                    } else {
                                                                                        echo $rowobj->designer_last_name;
                                                                                    }
                                                                                } else {
                                                                                    echo "No Designer";
                                                                                }
                                                                                ?>
                                                                            </p>
                                                                            <p class="pro-b">Designer</p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="cli-ent-col td" style="width: 9%;">
                                                                <div class="cli-ent-xbox text-center">
                                                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                                            <span class="numcircle-box">
                                                                                <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                                            </span>
                                                                        <?php } ?>
                                                                    </span>
                                                                    <?php //echo $rows[$i]->total_chat_all;   ?>
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="cli-ent-col td" style="width: 9%;">
                                                        <div class="cli-ent-xbox text-center">
                                                            <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                                <?php } ?>
                                                                <?php echo count($rows[$i]->total_files_count); ?>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div> <!-- End Row -->
                                        <?php } ?>
                                        <!--                </div>-->
                                        <!--QA Pending Approval Section End Here -->
                                    <?php } elseif ($rowobj->status == "approved") {
                                        ?>
                                        <?php
                                        for ($i = 0; $i < sizeof($rows); $i++) {
                                            $rowobj = (object) $rows[$i];
                                            ?>
                                            <!-- Start Row -->
                                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'" style="cursor: pointer;">
                                                <div class="cli-ent-col td" style="width: 13%;">
                                                    <div class="cli-ent-xbox">
                                                        <h3 class="app-roved green">Approved on</h3>
                                                        <p class="space-a"></p>
                                                        <p class="pro-b">
                                                            <?php echo date('M d, Y h:i A', strtotime($rowobj->approvaldate));
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="cli-ent-col td" style="width: 37%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cell-row">

                                                            <div class="cell-col">
                                                                <h3 class="pro-head space-b">
                                                                    <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3"><?php echo $rowobj->title; ?>

                                                                </a>
                                                            </h3>
                                                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                                        </div>

                                                        <div class="cell-col col-w1">
                                                            <p class="neft text-center">
                                                                <span class="green text-uppercase text-wrap">completed</span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 16%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cell-row">
                                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                                    <?php if ($rowobj->customer_profile_picture != "") { ?>
                                                                        <figure class="pro-circle-img">
                                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture; ?>" class="img-responsive">
                                                                        </figure>
                                                                    <?php } else { ?>
                                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                            <?php echo ucwords(substr($rowobj->customer_first_name, 0, 1)) . ucwords(substr($rowobj->customer_last_name, 0, 1)); ?>
                                                                        </figure>
                                                                    <?php } ?>
                                                                </a>
                                                            </div>
                                                            <div class="cell-col">
                                                                <p class="text-h text-wrap" title=" <?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                                    <?php echo $rowobj->customer_first_name; ?>
                                                                    <?php
                                                                    if (strlen($rowobj->customer_last_name) > 5) {
                                                                        echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                                                    } else {
                                                                        echo $rowobj->customer_last_name;
                                                                    }
                                                                    ?>
                                                                </p>
                                                                <p class="pro-b">Client</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 16%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cell-row">
                                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                                <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                                        <figure class="pro-circle-img">
                                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture; ?>" class="img-responsive">
                                                                        </figure>
                                                                    <?php } else { ?>
                                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                            <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                                        </figure>
                                                                    <?php } ?>
                                                                </a>
                                                            </div>
                                                            <div class="cell-col">
                                                                <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                                    <?php
                                                                    if ($rowobj->designer_first_name) {
                                                                        echo $rowobj->designer_first_name;
                                                                        if (strlen($rowobj->designer_last_name) > 5) {
                                                                            echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                                        } else {
                                                                            echo $rowobj->designer_last_name;
                                                                        }
                                                                    } else {
                                                                        echo "No Designer";
                                                                    }
                                                                    ?>
                                                                </p>
                                                                <p class="pro-b">Designer</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="cli-ent-col td" style="width: 9%;">
                                                    <div class="cli-ent-xbox text-center">
                                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                                <span class="numcircle-box">
                                                                    <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                                </span>
                                                            <?php } ?>
                                                        </span>
                                                        <?php //echo $rows[$i]->total_chat_all;   ?>
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 9%;">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rowobj->total_files) != 0) { ?>
                                                        <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span></span>
                                                    <?php } ?>
                                                    <?php echo count($rowobj->total_files_count); ?></a></p>
                                                </div>
                                            </div>
                                        </div><!-- End Row -->
                                    <?php } ?>
                                    <!--QA Completed Section End Here -->
                                    <?php
                                } elseif ($rowobj->status == "hold") {
                                    ?>
                                    <!--QA hold Section Start Here -->
                                    <?php
                                    for ($i = 0; $i < sizeof($rows); $i++) {
                                        $rowobj = (object) $rows[$i];
                                        ?>
                                        <!-- Start Row -->
                                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'" style="cursor: pointer;">
                                            <div class="cli-ent-col td" style="width: 13%;">
                                                <div class="cli-ent-xbox">
                                                    <h3 class="app-roved green">Hold on</h3>
                                                    <p class="space-a"></p>
                                                    <p class="pro-b">
                                                        <?php echo date('M d, Y h:i A', strtotime($rowobj->modified));
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="cli-ent-col td" style="width: 37%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="cell-row">

                                                        <div class="cell-col">
                                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3"><?php echo $rowobj->title; ?>

                                                        </a>
                                                    </h3>
                                                    <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                                </div>

                                                <div class="cell-col col-w1">
                                                    <p class="neft text-center">
                                                        <span class="gray text-uppercase text-wrap">On Hold</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cli-ent-col td" style="width: 16%;">
                                        <div class="cli-ent-xbox text-left">
                                            <div class="cell-row">
                                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                    <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                        <?php if ($rowobj->customer_profile_picture != "") { ?>
                                                            <figure class="pro-circle-img">
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture; ?>" class="img-responsive">
                                                            </figure>
                                                        <?php } else { ?>
                                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                <?php echo ucwords(substr($rowobj->customer_first_name, 0, 1)) . ucwords(substr($rowobj->customer_last_name, 0, 1)); ?>
                                                            </figure>
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="cell-col">
                                                    <p class="text-h text-wrap" title=" <?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                        <?php echo $rowobj->customer_first_name; ?>
                                                        <?php
                                                        if (strlen($rowobj->customer_last_name) > 5) {
                                                            echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                                        } else {
                                                            echo $rowobj->customer_last_name;
                                                        }
                                                        ?>
                                                    </p>
                                                    <p class="pro-b">Client</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="cli-ent-col td" style="width: 16%;">
                                        <div class="cli-ent-xbox text-left">
                                            <div class="cell-row">

                                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                    <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                        <?php if ($rowobj->profile_picture != "") { ?>
                                                            <figure class="pro-circle-img">
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture; ?>" class="img-responsive">
                                                            </figure>
                                                        <?php } else { ?>
                                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                                <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                            </figure>
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="cell-col">
                                                    <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                        <?php
                                                        if ($rowobj->designer_first_name) {
                                                            echo $rowobj->designer_first_name;
                                                            if (strlen($rowobj->designer_last_name) > 5) {
                                                                echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                            } else {
                                                                echo $rowobj->designer_last_name;
                                                            }
                                                        } else {
                                                            echo "No Designer";
                                                        }
                                                        ?>
                                                    </p>
                                                    <p class="pro-b">Designer</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 9%;">
                                        <div class="cli-ent-xbox text-center">
                                            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                    </span>
                                                <?php } ?>
                                            </span>
                                            <?php   ?>
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rowobj->total_files) != 0) { ?>
                                            <span class="numcircle-box"><?php echo count($rowobj->total_files); ?>

                                        </span>
                                    </span>
                                <?php } ?>
                                <?php echo count($rowobj->total_files_count); ?>
                            </a>
                        </p>
                    </div>
                </div>
            </div><!-- End Row -->
        <?php } ?>
        <!--QA Hold Section End Here -->
        <?php
    } elseif ($rowobj->status == "cancel") {
        ?>
        <!--QA Cancel Section Start Here -->
        <?php
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object) $rows[$i];
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 13%;">
                    <div class="cli-ent-xbox">
                        <h3 class="app-roved green">Cancelled on</h3>
                        <p class="space-a"></p>
                        <p class="pro-b">
                            <?php echo date('M d, Y h:i A', strtotime($rowobj->modified));
                            ?>
                        </p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 37%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">

                            <div class="cell-col">
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3"><?php echo $rowobj->title; ?></a></h3>
                                <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                            </div>

                            <div class="cell-col col-w1">
                                <p class="neft text-center"><span class="gray text-uppercase text-wrap">Cancelled</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 16%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                    <?php if ($rowobj->customer_profile_picture != "") { ?>
                                        <figure class="pro-circle-img">
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture; ?>" class="img-responsive">
                                        </figure>
                                    <?php } else { ?>
                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                            <?php echo ucwords(substr($rowobj->customer_first_name, 0, 1)) . ucwords(substr($rowobj->customer_last_name, 0, 1)); ?>
                                        </figure>
                                    <?php } ?>
                                </a>
                            </div>
                            <div class="cell-col">
                                <p class="text-h text-wrap" title=" <?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                    <?php echo $rowobj->customer_first_name; ?>
                                    <?php
                                    if (strlen($rowobj->customer_last_name) > 5) {
                                        echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                    } else {
                                        echo $rowobj->customer_last_name;
                                    }
                                    ?>
                                </p>
                                <p class="pro-b">
                                    Client
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 16%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                    <?php if ($rowobj->profile_picture != "") { ?>
                                        <figure class="pro-circle-img">
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture; ?>" class="img-responsive">
                                        </figure>
                                    <?php } else { ?>
                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                            <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                        </figure>
                                        <?php } ?></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                            <?php
                                            if ($rowobj->designer_first_name) {
                                                echo $rowobj->designer_first_name;
                                                if (strlen($rowobj->designer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->designer_last_name;
                                                }
                                            } else {
                                                echo "No Designer";
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                        <span class="numcircle-box">
                                            <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                        </span>
                                    <?php } ?>
                                </span>
                                <?php ?>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 9%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rowobj->total_files) != 0) { ?>
                                <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span></span>
                            <?php } ?>
                            <?php echo count($rowobj->total_files_count); ?></a></p>
                        </div>
                    </div>
                </div><!-- End Row -->
            <?php } ?>
            <!--QA Cancel Section End Here -->
            <?php
        }
    }
    ?>
    <span id="loadingAjaxCount" data-value="<?php echo $count_rows; ?>"></span>
    <?php
}
public function load_more_designer() {
    $this->myfunctions->checkloginuser("admin");
    $group_no = $this->input->post('group_no');
    $search = $this->input->post('search');
    $content_per_page = LIMIT_ADMIN_LIST_COUNT;
    $start = ceil($group_no * $content_per_page);
    $qa_assignproject = isset($_GET['assign'])?$_GET['assign']:"";
    $countdesigners = $this->Account_model->designerlist_load_more("", true, $start, $content_per_page, '', $search,$qa_assignproject);
    $designers = $this->Account_model->designerlist_load_more("", false, $start, $content_per_page, '', $search,$qa_assignproject);
    $isfullaccess = $this->hasAdminfullaccess('full_admin_access','admin');
    for ($i = 0; $i < sizeof($designers); $i++) {
        $designers[$i]['pending_requests'] = $this->Account_model->getcount_requests_fordesigners($designers[$i]['id'], array("checkforapprove"));
        $designers[$i]['active_requests'] = $this->Account_model->getcount_requests_fordesigners($designers[$i]['id'], array("active", "disapprove"));
        $designers[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($designers[$i]['profile_picture']);
        $designers[$i]['created'] = $this->onlytimezone($designers[$i]['created'],'M/d/Y');
        $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);
        $designers[$i]['qa'] = $this->Request_model->get_user_by_id($designers[$i]['va_id']);
        $designers[$i]['am'] = $this->Request_model->get_user_by_id($designers[$i]['am_id']);
        $designers[$i]['isfullaccess'] = $isfullaccess;
    }
    for ($i = 0; $i < sizeof($designers); $i++) {
        $data['designers'] = $designers[$i];
        $this->load->view('admin/view_designers_template', $data);
    } ?>
<span id="loadingAjaxCount" data-value="<?php echo $countdesigners; ?>"></span>
<?php
}

public function load_more_designerprojects() {
    $this->myfunctions->checkloginuser("admin");
    $status_role = $this->input->post('status');
    $status = explode(',', $status_role);
    $group_no = $this->input->post('group_no');
    $search = $this->input->post('search');
    $datauserid = $this->input->post('id');
    $content_per_page = LIMIT_ADMIN_LIST_COUNT;
    $start = ceil($group_no * $content_per_page);
    $count_rows = $this->Account_model->designer_projects_load_more($status, $datauserid, true, $start, $content_per_page, '', $search);
    $rows = $this->Account_model->designer_projects_load_more($status, $datauserid, false, $start, $content_per_page, '', $search);
    for ($i = 0; $i < sizeof($rows); $i++) {
        $rowobj = (object) $rows[$i];
        $rowobj->total_chat = $this->Request_model->get_chat_number($rowobj->id, $rowobj->customer_id, $rowobj->designer_id, "designer");
        $rowobj->total_chat_all = $this->Request_model->get_total_chat_number($rowobj->id, $_SESSION['user_id'], "qa");
        $rowobj->total_files = $this->Request_model->get_files_count($rowobj->id, $rowobj->designer_id, 'admin');
        $getfileid = $this->Request_model->get_attachment_files($rowobj->id, "designer");
        $commentcount = 0;
        for ($j = 0; $j < sizeof($getfileid); $j++) {

            $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
        }
        $rowobj->comment_count = $commentcount;
        if ($rowobj->status == "active" || $rowobj->status == "disapprove") {
            if ($rowobj->expected_date == '' || $rowobj->expected_date == NULL) {
                $rowobj->expected = $this->myfunctions->check_timezone($rowobj->latest_update, $rowobj->current_plan_name);
            } else {
                $rowobj->expected = $this->onlytimezone($rowobj->expected_date);
            }
        } elseif ($rowobj->status == "checkforapprove") {
            $rowobj->reviewdate = $this->onlytimezone($rowobj->latest_update);
        } elseif ($rowobj->status == "hold" || $rowobj->status == "cancel") {
            $rowobj->modified = $this->onlytimezone($rowobj->modified);
        } elseif ($rowobj->status == "approved") {
            $rowobj->approvddate = $this->onlytimezone($rowobj->approvaldate);
        }
        if ($rowobj->status_admin == "pendingrevision") {
            $rowobj->revisiondate = $this->onlytimezone($rowobj->modified);
        }
        $rowobj->total_files_count = $this->Request_model->get_files_count_all($rowobj->id);
    }
    for ($i = 0; $i < sizeof($rows); $i++) {
        $rowobj = (object) $rows[$i];
        if ($rowobj->status_designer == "disapprove" || $rowobj->status_designer == "active") {
            ?>
            <!--QA Active Section Start Here -->
            <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                <?php
                for ($i = 0; $i < sizeof($rows); $i++) {
                    $rowobj = (object) $rows[$i];
                    ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1'" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 12%;">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">
                                    <?php
                                    if ($rowobj->status == "active") {
                                        echo "Expected on";
                                    } elseif ($rowobj->status_admin == "disapprove") {
                                        echo "Expected on";
                                    }
                                    ?>
                                </p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php
                                    if ($rowobj->status == "active") {
                                        echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                    } elseif ($rowobj->status_admin == "disapprove") {
                                        echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b">
                                                <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1"><?php echo $rowobj->title; ?>

                                            </a>
                                        </h3>
                                        <p class="pro-b"><?php echo substr($rowobj->description, 0, 30); ?>

                                    </p>
                                </div>
                                <div class="cell-col col-w1">
                                    <p class="neft text-center">
                                        <?php if ($rowobj->status_admin == "active") { ?>
                                            <span class="green text-uppercase text-wrap">In-Progress</span>
                                        <?php } elseif ($rowobj->status_admin == "disapprove" && $rowobj->who_reject == 1) { ?>
                                            <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                        <?php } elseif ($rowobj->status_admin == "disapprove" && $rowobj->who_reject == 0) { ?>
                                            <span class="red text-uppercase text-wrap">Quality Revision</span>
                                        <?php } ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="cli-ent-col td" style="width: 20%;">
                        <div class="cli-ent-xbox text-left p-left1">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                    <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1">
                                        <figure class="pro-circle-img">
                                            <?php if ($rowobj->profile_picture != "") { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture ?>" class="img-responsive">
                                            <?php } else { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                            <?php } ?>
                                        </figure>
                                    </a>
                                </div>
                                <div class="cell-col">
                                    <p class="text-h" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                        <?php
                                        if ($rowobj->designer_first_name) {
                                            echo $rowobj->designer_first_name;
                                            if (strlen($rowobj->designer_last_name) > 5) {
                                                echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                            } else {
                                                echo $rowobj->designer_last_name;
                                            }
                                        } else {
                                            echo "No Designer";
                                        }
                                        ?>

                                    </p>
                                    <p class="pro-b">Designer</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 10%;">
                        <div class="cli-ent-xbox text-center">
                            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                    <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                        <span class="numcircle-box">
                                            <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                        </span>
                                    <?php } ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 10%;">
                        <div class="cli-ent-xbox text-center">
                            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_request/<?php echo $rowobj->id; ?>/1">
                                <span class="inline-imgsssx">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                        <span class="numcircle-box"><?php echo count($rowobj->total_files); ?>

                                    </span>
                                <?php } ?>
                            </span>
                            <?php echo count($rowobj->total_files_count); ?>
                        </a>
                    </p>
                </div>
            </div>
        </div> <!-- End Row -->
        <?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows; ?>">

        </span>
    </div>
    <!--QA Active Section End Here -->
    <?php
} elseif ($rowobj->status_designer == "assign" || $rowobj->status_designer == "pending") {
    ?>
    <!--QA Active Section Start Here -->
    <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
        <?php
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object) $rows[$i];
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2'" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 12%;">
                    <div class="cli-ent-xbox">
                        <p class="pro-a">In Queue</p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 48%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" >
                                <h3 class="pro-head space-b">
                                    <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2"><?php echo $rowobj->title; ?>

                                </a>
                            </h3>
                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 30); ?>
                        </p>
                    </div>
                    <div class="cell-col col-w1">
                        <p class="neft text-center">
                            <span class="gray text-uppercase">IN-queue
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="cli-ent-col td" style="width: 20%;">
            <div class="cli-ent-xbox text-left p-left1">
                <div class="cell-row">
                    <div class="cell-col" style="width: 36px; padding-right: 15px;">
                        <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2">
                            <figure class="pro-circle-img">
                                <?php if ($rowobj->profile_picture != "") { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                <?php } ?>
                            </figure>
                        </a>
                    </div>
                    <div class="cell-col">
                        <p class="text-h" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                            <?php echo $rowobj->designer_first_name; ?>
                            <?php
                            if (strlen($rowobj->designer_last_name) > 5) {
                                echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                            } else {
                                echo $rowobj->designer_last_name;
                            }
                            ?>

                        </p>
                        <p class="pro-b">Designer</p>
                        <p class="space-a"></p>
                        <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rowobj->id; ?>" data-designerid= "<?php echo $rowobj->designer_id; ?>">
                            <span class="sma-red">+</span> Add Designer
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="cli-ent-col td" style="width: 10%;">
            <div class="cli-ent-xbox text-center">
                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2">
                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                        <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                            <span class="numcircle-box">
                                <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                            </span>
                        <?php } ?>
                    </span>
                </a>
            </p>
        </div>
    </div>
    <div class="cli-ent-col td" style="width: 10%;">
        <div class="cli-ent-xbox text-center">
            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2">
                <span class="inline-imgsssx">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                        <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span>
                    <?php } ?>
                </span>
                <?php echo count($rowobj->total_files_count); ?>
            </a>
        </p>
    </div>
</div>
</div> <!-- End Row -->
<?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows; ?>"></span>  
</div>
<!--QA Active Section End Here -->
<?php
} elseif ($rowobj->status_designer == "pendingrevision") {
    ?>
    <!--QA Pending Approval Section Start Here -->
    <div class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel">
        <?php
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object) $rows[$i];
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
                    <div class="cli-ent-xbox">
                        <p class="pro-a">Expected on</p>
                        <p class="space-a"></p>
                        <p class="pro-b">
                            <?php echo date('M d, Y h:i A', strtotime($rowobj->revisiondate)); ?>
                        </p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" >
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3"><?php echo $rowobj->title; ?></a>
                                </h3>
                                <p class="pro-b"><?php echo substr($rowobj->description, 0, 30); ?></p>
                            </div>

                            <div class="cell-col col-w1">
                                <p class="neft text-center"><span class=" lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                <a href=""><figure class="pro-circle-img">
                                    <?php if ($rowobj->customer_profile_picture != "") { ?>
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture ?>" class="img-responsive">
                                    <?php } else { ?>
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                    <?php } ?>
                                </figure>
                            </a>
                        </div>
                        <div class="cell-col">
                            <p class="text-h text-wrap" title="<?php echo $rowobj->customer_first_name . " " . $rowobj->customer_last_name; ?>
                            ">
                            <?php echo $rowobj->customer_first_name; ?>
                            <?php
                            if (strlen($rowobj->customer_last_name) > 5) {
                               echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                           } else {
                               echo $rowobj->customer_last_name;
                           }
                           ?>

                       </p>
                       <p class="pro-b">Client</p>
                   </div>
               </div>

           </div>
       </div>

       <div class="cli-ent-col td" style="width: 16%;">
        <div class="cli-ent-xbox text-left">
            <div class="cell-row">
                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                    <a href="" class="<?php echo $rowobj->id; ?>"><figure class="pro-circle-img">
                        <?php if ($rowobj->profile_picture != "") { ?>
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture ?>" class="img-responsive">
                        <?php } else { ?>
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                        <?php } ?>
                    </figure></a>
                </div>
                <div class="cell-col <?php echo $rowobj->id; ?>">
                    <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                        <?php echo $rowobj->designer_first_name; ?>
                        <?php
                        if (strlen($rowobj->designer_last_name) > 5) {
                            echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                        } else {
                            echo $rowobj->designer_last_name;
                        }
                        ?>
                    </p>
                    <p class="pro-b">Designer</p>
                </div>
            </div>

        </div>
    </div>
    <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
        <div class="cli-ent-xbox text-center">
            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                    <span class="numcircle-box">
                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                    </span>
                <?php } ?>
            </span>
            <?php  ?>
        </a>
    </p>
</div>
</div>
<div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>'">
    <div class="cli-ent-xbox text-center">
        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span><?php } ?>
            </span>
            <?php echo count($rowobj->total_files_count); ?>
        </a>
    </p>
</div>
</div>
</div> 
<!-- End Row -->
<?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows; ?>"></span>  
</div>
<!--QA Pending Approval Section End Here -->
<?php
} elseif ($rowobj->status == "checkforapprove") {
    ?>
    <!--QA Pending Approval Section Start Here -->
    <div class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel">
        <?php
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object) $rows[$i];
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4'">
                    <div class="cli-ent-xbox">
                        <p class="pro-a">Delivered on</p>
                        <p class="space-a"></p>
                        <p class="pro-b">
                            <?php echo date('M d, Y h:i A', strtotime($rowobj->reviewdate)); ?>
                        </p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4'">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">

                            <div class="cell-col" >
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4"><?php echo $rowobj->title; ?></a></h3>
                                <p class="pro-b"><?php echo substr($rowobj->description, 0, 30); ?>

                            </p>
                        </div>

                        <div class="cell-col col-w1">
                            <p class="neft text-center">
                                <span class="red bluetext text-uppercase text-wrap">Pending-approval
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4'">
                <div class="cli-ent-xbox text-left">
                    <div class="cell-row">
                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                            <a href="#"><figure class="pro-circle-img">
                                <?php if ($rowobj->customer_profile_picture != "") { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                <?php } ?>
                            </figure>
                        </a>
                    </div>
                    <div class="cell-col">
                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rowobj->customer_last_name; ?>
                        ">
                        <?php echo $rowobj->customer_first_name; ?>
                        <?php
                        if (strlen($rowobj->customer_last_name) > 5) {
                           echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                       } else {
                           echo $rowobj->customer_last_name;
                       }
                       ?>
                   </p>
                   <p class="pro-b">Client</p>
               </div>
           </div>

       </div>
   </div>

   <div class="cli-ent-col td" style="width: 16%;">
    <div class="cli-ent-xbox text-left">
        <div class="cell-row">
            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                <a href="#" class="<?php echo $rowobj->id; ?>"><figure class="pro-circle-img">
                    <?php if ($rowobj->profile_picture != "") { ?>
                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture ?>" class="img-responsive">
                    <?php } else { ?>
                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                    <?php } ?>
                </figure>
            </a>
        </div>
        <div class="cell-col <?php echo $rowobj->id; ?>">
            <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                <?php echo $rowobj->designer_first_name; ?>
                <?php
                if (strlen($rowobj->designer_last_name) > 5) {
                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                } else {
                    echo $rowobj->designer_last_name;
                }
                ?>
            </p>
            <p class="pro-b">Designer</p>
        </div>
    </div>

</div>
</div>
<div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
    <div class="cli-ent-xbox text-center">
        <p class="pro-a inline-per">
            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4">
                <span class="inline-imgsssx">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                    <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                        <span class="numcircle-box">
                            <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                        </span>
                    <?php } ?>
                </span>
                <?php  ?>
            </a>
        </p>
    </div>
</div>
<div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>'">
    <div class="cli-ent-xbox text-center">
        <p class="pro-a inline-per">
            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                <span class="inline-imgsssx">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                        <span class="numcircle-box"><?php echo count($rowobj->total_files); ?>

                    </span>
                <?php } ?>
            </span>
            <?php echo count($rowobj->total_files_count); ?>
        </a>
    </p>
</div>
</div>
</div> 
<!-- End Row -->
<?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows; ?>"></span>  
</div>
<!--QA Pending Approval Section End Here -->
<?php
} elseif ($rowobj->status == "approved") {
    ?>
    <!--QA Completed Section Start Here -->
    <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
        <?php
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object) $rows[$i];
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5'"style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 13%;">
                    <div class="cli-ent-xbox">
                        <h3 class="app-roved green">Approved on
                        </h3>
                        <p class="pro-b">
                            <?php echo date('M d, Y h:i A', strtotime($rowobj->approvaldate)); ?>
                        </p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 37%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">

                            <div class="cell-col" >
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5"><?php echo $rowobj->title; ?></a></h3>
                                <p class="pro-b">
                                    <?php echo substr($rowobj->description, 0, 30); ?>
                                </p>
                            </div>

                            <div class="cell-col col-w1">
                                <p class="neft text-center"><span class="green text-uppercase text-wrap">completed
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cli-ent-col td" style="width: 16%;">
                <div class="cli-ent-xbox text-left">
                    <div class="cell-row">
                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                            <a href=""><figure class="pro-circle-img">
                                <?php if ($rowobj->customer_profile_picture != "") { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                <?php } ?>
                            </figure></a>
                        </div>
                        <div class="cell-col">
                            <p class="text-h text-wrap" title="<?php echo $rowobj->customer_first_name . " " . $rowobj->customer_last_name; ?>">
                                <?php echo $rowobj->customer_first_name; ?>
                                <?php
                                if (strlen($rowobj->customer_last_name) > 5) {
                                    echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                } else {
                                    echo $rowobj->customer_last_name;
                                }
                                ?>

                            </p>
                            <p class="pro-b">Client</p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="cli-ent-col td" style="width: 16%;">
                <div class="cli-ent-xbox text-left">
                    <div class="cell-row">
                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                            <a href=""><figure class="pro-circle-img">
                                <?php
                                if ($rowobj->profile_picture != "") {
                                    ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                <?php } ?>
                            </figure></a>
                        </div>
                        <div class="cell-col">
                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rowobj->designer_last_name; ?> ">
                                <?php echo $rowobj->designer_first_name; ?>
                                <?php
                                if (strlen($rowobj->designer_last_name) > 5) {
                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                } else {
                                    echo $rowobj->designer_last_name;
                                }
                                ?>

                            </p>
                            <p class="pro-b">Designer</p>

                        </div>
                    </div>

                </div>
            </div>
            <div class="cli-ent-col td" style="width: 9%;">
                <div class="cli-ent-xbox text-center">
                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5">
                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                            <span class="numcircle-box">
                                <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                            </span>
                        <?php } ?>
                    </span>
                    <?php ?>
                </a>
            </p>
        </div>
    </div>
    <div class="cli-ent-col td" style="width: 9%;">
        <div class="cli-ent-xbox text-center">
            <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5">
                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                    <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span><?php } ?></span>
                    <?php echo count($rowobj->total_files_count); ?></a></p>
                </div>
            </div>
        </div> <!-- End Row -->
    <?php } ?>
    <span id="loadingAjaxCount" data-value="<?php echo $count_rows; ?>">

    </span>  
</div>
<!--QA Completed Section End Here -->
<?php
} elseif ($rowobj->status == "hold") {
    ?>
    <!--QA Hold Section Start Here -->
    <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
        <?php
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object) $rows[$i];
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5'"style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 13%;">
                    <div class="cli-ent-xbox">
                        <h3 class="app-roved green">Hold on</h3>
                        <p class="pro-b">
                            <?php echo date('M d, Y h:i A', strtotime($rowobj->modified)); ?>
                        </p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 37%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" >
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5"><?php echo $rowobj->title; ?></a></h3>
                                <p class="pro-b">
                                    <?php echo substr($rowobj->description, 0, 30); ?>
                                </p>
                            </div>

                            <div class="cell-col col-w1">
                                <p class="neft text-center">
                                    <span class="gray text-uppercase text-wrap">On Hold</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 16%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                <a href=""><figure class="pro-circle-img">
                                    <?php if ($rowobj->customer_profile_picture != "") { ?>
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture ?>" class="img-responsive">
                                    <?php } else { ?>
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                    <?php } ?>
                                </figure>
                            </a>
                        </div>
                        <div class="cell-col">
                            <p class="text-h text-wrap" title="<?php echo $rowobj->customer_first_name . " " . $rowobj->customer_last_name; ?>">
                                <?php echo $rowobj->customer_first_name; ?>
                                <?php
                                if (strlen($rowobj->customer_last_name) > 5) {
                                    echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                } else {
                                    echo $rowobj->customer_last_name;
                                }
                                ?>

                            </p>
                            <p class="pro-b">Client</p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="cli-ent-col td" style="width: 16%;">
                <div class="cli-ent-xbox text-left">
                    <div class="cell-row">
                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                            <a href=""><figure class="pro-circle-img">
                                <?php
                                if ($rowobj->profile_picture != "") {
                                                    //echo "<pre/>";print_r($rowobj);
                                    ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                <?php } ?>
                            </figure>
                        </a>
                    </div>
                    <div class="cell-col">
                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rowobj->designer_last_name; ?> ">
                            <?php echo $rowobj->designer_first_name; ?>
                            <?php
                            if (strlen($rowobj->designer_last_name) > 5) {
                                echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                            } else {
                                echo $rowobj->designer_last_name;
                            }
                            ?>

                        </p>
                        <p class="pro-b">Designer</p>

                    </div>

                </div>

            </div>
        </div>
        <div class="cli-ent-col td" style="width: 9%;">
            <div class="cli-ent-xbox text-center">
                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5">
                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                        <span class="numcircle-box">
                            <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                        </span>
                    <?php } ?>
                </span>
                <?php  ?>
            </a>
        </p>
    </div>
</div>
<div class="cli-ent-col td" style="width: 9%;">
    <div class="cli-ent-xbox text-center">
        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5">
            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span><?php } ?></span>
                <?php echo count($rowobj->total_files_count); ?></a></p>
            </div>
        </div>
    </div> <!-- End Row -->
<?php } ?>
<span id="loadingAjaxCount" data-value="<?php echo $count_rows; ?>"></span>  
</div>
<!--QA Hold Section End Here -->
<?php
} elseif ($rowobj->status == "cancel") {
    ?>
    <!--QA Cancel Section Start Here -->
    <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
        <?php
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object) $rows[$i];
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5'"style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 13%;">
                    <div class="cli-ent-xbox">
                        <h3 class="app-roved green">Cancelled on</h3>
                        <p class="pro-b">
                            <?php echo date('M d, Y h:i A', strtotime($rowobj->modified)); ?>
                        </p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 37%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" >
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5"><?php echo $rowobj->title; ?></a></h3>
                                <p class="pro-b">
                                    <?php echo substr($rowobj->description, 0, 30); ?>
                                </p>
                            </div>

                            <div class="cell-col col-w1">
                                <p class="neft text-center"><span class="gray text-uppercase text-wrap">Cancelled</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 16%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                <a href=""><figure class="pro-circle-img">
                                    <?php if ($rowobj->customer_profile_picture != "") { ?>
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->customer_profile_picture ?>" class="img-responsive">
                                    <?php } else { ?>
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                    <?php } ?>
                                </figure>
                            </a>
                        </div>
                        <div class="cell-col">
                            <p class="text-h text-wrap" title="<?php echo $rowobj->customer_first_name . " " . $rowobj->customer_last_name; ?>">
                                <?php echo $rowobj->customer_first_name; ?>
                                <?php
                                if (strlen($rowobj->customer_last_name) > 5) {
                                    echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                } else {
                                    echo $rowobj->customer_last_name;
                                }
                                ?>

                            </p>
                            <p class="pro-b">Client</p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="cli-ent-col td" style="width: 16%;">
                <div class="cli-ent-xbox text-left">
                    <div class="cell-row">

                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                            <a href=""><figure class="pro-circle-img">
                                <?php
                                if ($rowobj->profile_picture != "") {
                                    ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rowobj->profile_picture ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                <?php } ?>
                            </figure>
                        </a>
                    </div>
                    <div class="cell-col">
                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rowobj->designer_last_name; ?> ">
                            <?php echo $rowobj->designer_first_name; ?>
                            <?php
                            if (strlen($rowobj->designer_last_name) > 5) {
                                echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                            } else {
                                echo $rowobj->designer_last_name;
                            }
                            ?>

                        </p>
                        <p class="pro-b">Designer</p>

                    </div>
                </div>

            </div>
        </div>
        <div class="cli-ent-col td" style="width: 9%;">
            <div class="cli-ent-xbox text-center">
                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5">
                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                        <span class="numcircle-box">
                            <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                        </span>
                        <?php } ?></span>
                        <?php //echo $rows[$i]->total_chat_all;     ?></a></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 9%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5">
                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                <span class="numcircle-box"><?php echo count($rowobj->total_files); ?>

                                </span><?php } ?>
                            </span>
                            <?php echo count($rowobj->total_files_count); ?>
                        </a>
                    </p>
                </div>
            </div>
        </div> <!-- End Row -->
        <?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows; ?>"></span>  
    </div>
    <!--QA Cancel Section End Here -->
    <?php
}
}
?> 
<?php
}

public function load_more_qa_va() {
    $this->myfunctions->checkloginuser("admin");
    $status_role = $this->input->post('status');
    $count_active_project = $this->Request_model->admin_load_more(array('active', 'disapprove'),true);
    $count_pending_project = $this->Request_model->admin_load_more(array("pendingforapprove", "checkforapprove"),true);
        $group_no = $this->input->post('group_no');
        $search = $this->input->post('search');
        $am_assignproject = isset($_GET['assign'])?$_GET['assign']:"";
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $countqa = $this->Account_model->qavalist_load_more('admin', true, $start, $content_per_page, '', $search,$am_assignproject);
        $qa = $this->Account_model->qavalist_load_more('admin', false, $start, $content_per_page, '', $search,$am_assignproject);
        for ($i = 0; $i < sizeof($qa); $i++) {
            
            $count_active_project = $this->Request_model->countVaProjects(array('active','disapprove'),$qa[$i]['id']);
            $count_pending_project = $this->Request_model->countVaProjects(array("pendingforapprove","checkforapprove"),$qa[$i]['id']);
            $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
            $qa[$i]['profile_picture'] = $this->customfunctions->getprofileimageurl($qa[$i]['profile_picture']);
            $qa[$i]['count_active_project'] = $count_active_project;
            $qa[$i]['count_pending_project'] = $count_pending_project;
            $qa[$i]['created'] = $this->onlytimezone($qa[$i]['created'],'M/d/Y');
            $qa[$i]['total_designer'] = sizeof($designers);
            $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);
            if ($mydesigner) {
                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                $qa[$i]['total_clients'] = sizeof($data);
            } else {
                $qa[$i]['total_clients'] = 0;
            }
            $qa[$i]['user_role'] = $this->getadminuserrole($qa[$i]['full_admin_access']);
            $qa[$i]['am'] = $this->Request_model->get_user_by_id($qa[$i]['am_id']);
            $data['allqa'] = $qa[$i];
            $data['count_active_project'] = $count_active_project;
            $data['count_pending_project'] = $count_pending_project;
            $this->load->view('admin/view_qa_template', $data);
        }?>
    <span id="loadingAjaxCount" data-value="<?php echo $countqa; ?>"></span>
    <?php }

// For Admin verification that designer skills matched or not
    public function designer_skills_matched() {
        $this->myfunctions->checkloginuser("admin");
        $data = array(
            'designer_skills_matched' => $_POST['designer_skills_matched'],
        );
        $success = $this->Admin_model->update_data('requests', $data, array('id' => $_POST['request_id']));
        if ($success) {
            echo "1";
        } else {
            echo "0";
        }
        exit;
    }

// Disable designer functionality for admin  
//    public function disable_designer() {
//        $this->myfunctions->checkloginuser("admin");
//        if ($_POST['designerid']) {
//            $success = $this->Admin_model->update_data('users', array('disable_designer' => 1), array('id' => $_POST['designerid']));
//            if ($success) {
//                $all_requests = $this->reassign_project_to_enable_designer($_POST['designerid']);
//                echo "1";
//            } else {
//                echo "0";
//            }
//        }
//    }

// Enable designer functionality for admin  
    public function enabledisable_designer() {
        $this->myfunctions->checkloginuser("admin");
        if ($_POST['designerid'] && $_POST['status']) {
            if($_POST['status'] == 'enable'){
                $success = $this->Admin_model->update_data('users', array('is_active' => 1), array('id' => $_POST['designerid']));
            }else{
                $success = $this->Admin_model->update_data('users', array('is_active' => 0), array('id' => $_POST['designerid']));
            }
            if ($success) {
                echo json_encode('success');
            }
        }
    }

// Delete Designer from Admin 
    public function delete_designer() {
        $this->myfunctions->checkloginuser("admin");
        $success = $this->Request_model->delete_user('users', $_POST['designerid']);
        if ($success) {
            $this->reassign_project_to_enable_designer($_POST['designerid']);
        }
    }

// Reassign disabled designer Projects
    public function reassign_project_to_enable_designer($id) {
        echo $id;exit;
        $this->myfunctions->checkloginuser("admin");
        $designer_request = $this->Request_model->get_request_by_designer_id($id);
//        echo "<pre/>";print_R($designer_request);exit;
        if (!empty($designer_request)) {
            foreach ($designer_request as $value) {
                $get_all_designers_info = $this->Request_model->get_designers_info($value['category']);
                $count_min_request = [];
                foreach ($get_all_designers_info as $designers) {
                    $project_count = $this->Request_model->get_request_count_by_designer_id($designers['user_id']);
                    $count_min_request[$designers['user_id']] = $project_count;
                }
                $count = array_keys($count_min_request, min($count_min_request));
                $success = $this->Admin_model->update_data('requests', array('designer_id' => $count[0]), array('id' => $value['id']));
                if ($success) {
                    echo "1";
                } else {
                    echo "0";
                }
            }
        }
    }

    public function change_customer_status_active_inactive() {
        $this->myfunctions->checkloginuser("admin");
        $cust_status = $_POST['status'];
        $cust_id = $_POST['data_id'];
        if ($_POST['status'] == 'true') {
            echo "1";
            $this->Request_model->update_customer_active_status("users", array("is_active" => 1), array("id" => $cust_id));
        } elseif ($_POST['status'] == 'false') {
            echo "0";
            $this->Request_model->update_customer_active_status("users", array("is_active" => 0), array("id" => $cust_id));
        }
    }

    public function testemail() {
        $config = $this->config->item('email_smtp');
        $from = $this->config->item('from');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config['smtp_sequre']);
        define('MAIL_HOST', $config['smtp_host']);
        define('MAIL_PORT', $config['smtp_port']);
        define('MAIL_USERNAME', $config['smtp_user']);
        define('MAIL_PASSWORD', $config['smtp_pass']);
        define('MAIL_SENDER', $from);

        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $data = array(
            'title' => 'Hello Test',
            'project_id' => 1
        );
        $body = $this->load->view('emailtemplate/qa_approved_template', $data, TRUE);
        $receiver = 'jatinderkumar0550@gmail.com';

        $subject = 'Test email';

        $message = $body;

        $title = 'GraphicsZoo';

        echo SM_sendMail($receiver, $subject, $message, $title);
    }

    public function getSubusers() {
        $this->myfunctions->checkloginuser("admin");
        $cust_id = isset($_POST['cust_id']) ? $_POST['cust_id'] : '';
        $subuserlist = $this->Request_model->getAllsubUsers($cust_id);
        $html = '';
        $html = '<div class="modal-dialog modal-lg modal-nose similar-prop " role="document">
        <div class="modal-content">
        <div class="cli-ent-model-box">
        <img class="cross_popup" data-dismiss="modal" src="' . FS_PATH_PUBLIC_ASSETS . 'img/default-img/cross.png">
        <div class="cli-ent-model">
        <header class="fo-rm-header">
        <h2 class="popup_h2 ">Sub Users</h2>
        </header>';
        if (sizeof($subuserlist) > 0) {
            $html .= '<div class="noti-listpopup">
            <div class="newsetionlist">
            <div class="cli-ent-row tr notificate">
            <div class="cli-ent-col td" style="width: 30%;">
            <div class="cli-ent-xbox text-left">
            <h3 class="pro-head space-b">Name</h3>
            </div>
            </div>

            <div class="cli-ent-col td" style="width: 45%;">
            <div class="cli-ent-xbox text-left">
            <h3 class="pro-head space-b">Email</h3>
            </div>
            </div>

            <div class="cli-ent-col td" style="width: 25%;">
            <div class="cli-ent-xbox text-left">
            <h3 class="pro-head space-b text-center">Phone</h3>
            </div>
            </div>
            </div>
            </div>
            <ul class="list-unstyled list-notificate">';
            foreach ($subuserlist as $value1) {
            //echo $value['first_name'];
                $html .= '<li>
                <a href="#">
                <div class="cli-ent-row tr notificate">
                <div class="cli-ent-col td" style="width: 30%;">
                <div class="cli-ent-xbox text-left">
                <div class="setnoti-fication">
                <div class="notifitext">
                <p class="ntifittext-z1"><strong>' . $value1['first_name'] . " " . $value1['last_name'] . '</strong></p>
                </div>
                </div>
                </div>
                </div>
                <div class="cli-ent-col td" style="width: 45%;">
                <div class="cli-ent-xbox text-left">
                <p class="pro-a">' . $value1['email'] . '</p>
                </div>
                </div>

                <div class="cli-ent-col td" style="width: 25%;">
                <div class="cli-ent-xbox text-left">
                <div class="cli-ent-xbox text-center">
                <p class="neft text-center"><span class="ph-no">' . $value1['phone'] . '</span></p>
                </div>
                </div>
                </div>
                </div>
                </a>
                </li>';
            }
        } else {
            $html .= 'No user found';
        }
        $html .= '</ul>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>';
        echo $html;
    }

    public function change_can_trialdownload() {
        $this->myfunctions->checkloginuser("admin");
        $is_download = $_POST['status'];
        $cust_id = $_POST['data_id'];
        if ($is_download == 'true') {
            $this->Request_model->update_customer_active_status("users", array("can_trialdownload" => 1), array("id" => $cust_id));
        } elseif ($is_download == 'false') {
            $this->Request_model->update_customer_active_status("users", array("can_trialdownload" => 0), array("id" => $cust_id));
        }
    }

    public function sub_user($cust_id) {
    // echo $cust_id;
        $this->myfunctions->checkloginuser("admin");
        $cust_id = isset($cust_id) ? $cust_id : '';
        $subuserlist = $this->Request_model->getAllsubUsers($cust_id);
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/sub_users', array('subuserlist' => $subuserlist, 'cust_id' => $cust_id, 'edit_profile' => $profile_data));
        $this->load->view('admin/admin_footer');
    }

    public function add_subuser($parent_id) {
        $this->myfunctions->checkloginuser("admin");
        $brandprofile = $this->Request_model->get_Allbrandprofile();
        $sub_user_permissions = $this->Request_model->get_sub_user_permissions($parent_id);
        if (isset($_POST['save_subuser'])) {
            $emailpassword = $this->myfunctions->random_password();
            $customer['is_active'] = 1;
            $customer['email'] = $_POST['email'];
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['phone'] = $_POST['phone'];
            $customer['role'] = "customer";
            $customer['last_update'] = date("Y:m:d H:i:s");
            $customer['total_inprogress_req'] = TOTAL_INPROGRESS_REQUEST;
            $customer['total_active_req'] = TOTAL_ACTIVE_REQUEST;
            $customer['parent_id'] = $parent_id;
            $customer['created'] = date("Y:m:d H:i:s");
            $customer['modified'] = date("Y:m:d H:i:s");
            if (isset($_POST['add_requests']) && $_POST['add_requests'] == 'on') {
                $add_requests = 1;
            }
            if (isset($_POST['view_only']) && $_POST['view_only'] == 'on') {
                $view_only = 1;
            }
            if (isset($_POST['del_requests']) && $_POST['del_requests'] == 'on') {
                $del_requests = 1;
            }
            if (isset($_POST['app_requests']) && $_POST['app_requests'] == 'on') {
                $app_requests = 1;
            }
            if (isset($_POST['downld_requests']) && $_POST['downld_requests'] == 'on') {
                $downld_requests = 1;
            }
            if (isset($_POST['comnt_requests']) && $_POST['comnt_requests'] == 'on') {
                $comnt_requests = 1;
            }
            if (isset($_POST['billing_module']) && $_POST['billing_module'] == 'on') {
                $billing_module = 1;
            }
            if (isset($_POST['add_brand_pro']) && $_POST['add_brand_pro'] == 'on') {
                $add_brand_pro = 1;
            }
            if (isset($_POST['access_brand_pro']) && $_POST['access_brand_pro'] == 'on') {
                $access_brand_pro = 1;
            }
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            if (!empty($checkemail)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);
                redirect(base_url() . "admin/dashboard/sub_user/" . $parent_id);
            } else {
                $customer['password'] = md5($emailpassword);
                $customer['new_password'] = md5($emailpassword);
                $customer['created'] = date("Y:m:d H:i:s");
                $id = $this->Welcome_model->insert_data("users", $customer);
                if ($id) {
                //$this->send_randompassword_email($customer['email'],$emailpassword);
                    $role['add_requests'] = isset($add_requests) ? $add_requests : 0;
                    $role['delete_req'] = isset($del_requests) ? $del_requests : 0;
                    $role['customer_id'] = $id;
                    $role['approve/revision_requests'] = isset($app_requests) ? $app_requests : 0;
                    $role['download_file'] = isset($downld_requests) ? $downld_requests : 0;
                    $role['comment_on_req'] = isset($comnt_requests) ? $comnt_requests : 0;
                    $role['billing_module'] = isset($billing_module) ? $billing_module : 0;
                    $role['add_brand_pro'] = isset($add_brand_pro) ? $add_brand_pro : 0;
                    $role['brand_profile_access'] = isset($access_brand_pro) ? $access_brand_pro : 0;
                    $role['view_only'] = isset($view_only) ? $view_only : 0;
                    $role['modified'] = date("Y:m:d H:i:s");
                    $this->Welcome_model->insert_data("user_permissions", $role);
                    if ($_POST['brandids']) {
                        foreach ($_POST['brandids'] as $val) {
                            $role1['user_id'] = $cust_id;
                            $role1['brand_id'] = $val;
                            $role1['created'] = date("Y:m:d H:i:s");
                            $this->Welcome_model->insert_data("user_brand_profiles", $role1);
                        }
                    }
                    $this->session->set_flashdata('message_success', 'User created successfully! An email is sent to the address with a temporary password.', 5);
                    redirect(base_url() . "admin/dashboard/sub_user/" . $parent_id);
                }
            }
        }
        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/add_subuser', array('brandprofile' => $brandprofile, 'parent_id' => $parent_id));
        $this->load->view('admin/admin_footer');
    }

    public function delete_sub_user($id) {
        $this->myfunctions->checkloginuser("admin");
        $delete_subuser = $this->Admin_model->getuser_data($id);
        $sub_user_permissions = $this->Request_model->delete_user_permission($id);
        if ($sub_user_permissions) {
            $sub_user_data = $this->Request_model->delete_user("users", $id);
        }
        $this->session->set_flashdata('message_success', 'User deleted Successfully', 5);
        redirect(base_url() . "admin/dashboard/sub_user/" . $delete_subuser[0]['parent_id']);
    }

    public function editsub_user($cust_id) {
        $this->myfunctions->checkloginuser("admin");
        $cust_id = isset($cust_id) ? $cust_id : '';
        $edit_subuser = $this->Admin_model->getuser_data($cust_id);
    //echo $edit_subuser[0]['parent_id'];
        $brandprofile = $this->Request_model->get_Allbrandprofile();
        $brandIDs = $this->Request_model->selectedBrandsforuser($cust_id);
        $BIDs = array_column($brandIDs, 'brand_id');
        $sub_user_permissions = $this->Request_model->get_sub_user_permissions($cust_id);
        if (isset($_POST['add_requests']) && $_POST['add_requests'] == 'on') {
            $add_requests = 1;
        }
        if (isset($_POST['view_only']) && $_POST['view_only'] == 'on') {
            $view_only = 1;
        }
        if (isset($_POST['del_requests']) && $_POST['del_requests'] == 'on') {
            $del_requests = 1;
        }
        if (isset($_POST['app_requests']) && $_POST['app_requests'] == 'on') {
            $app_requests = 1;
        }
        if (isset($_POST['downld_requests']) && $_POST['downld_requests'] == 'on') {
            $downld_requests = 1;
        }
        if (isset($_POST['comnt_requests']) && $_POST['comnt_requests'] == 'on') {
            $comnt_requests = 1;
        }
        if (isset($_POST['billing_module']) && $_POST['billing_module'] == 'on') {
            $billing_module = 1;
        }
        if (isset($_POST['add_brand_pro']) && $_POST['add_brand_pro'] == 'on') {
            $add_brand_pro = 1;
        }
        if (isset($_POST['access_brand_pro']) && $_POST['access_brand_pro'] == 'on') {
            $access_brand_pro = 1;
        }
        if (isset($_POST['save_subuser'])) {
            if ($_POST['new_password'] == '' && $_POST['confirm_password'] != '') {
                $this->session->set_flashdata('message_error', 'Please enter new password!', 5);
                redirect(base_url() . "admin/dashboard/editsub_user/" . $cust_id);
            } elseif ($_POST['confirm_password'] == '' && $_POST['new_password'] != '') {
                $this->session->set_flashdata('message_error', 'Please enter confirm password!', 5);
                redirect(base_url() . "admin/dashboard/editsub_user/" . $cust_id);
            } elseif ($_POST['confirm_password'] !== $_POST['new_password']) {
                $this->session->set_flashdata('message_error', 'New password and confirm password must match!', 5);
                redirect(base_url() . "admin/dashboard/editsub_user/" . $cust_id);
            } else {
                if ($_POST['new_password']) {
                    $newpassword = md5($_POST['new_password']);
                }
                $customer['first_name'] = $_POST['first_name'];
                $customer['last_name'] = $_POST['last_name'];
                $customer['email'] = $_POST['email'];
                $customer['phone'] = $_POST['phone'];
                $customer['new_password'] = $newpassword;
                $role['add_requests'] = isset($add_requests) ? $add_requests : 0;
                $role['delete_req'] = isset($del_requests) ? $del_requests : 0;
                $role['approve/revision_requests'] = isset($app_requests) ? $app_requests : 0;
                $role['download_file'] = isset($downld_requests) ? $downld_requests : 0;
                $role['comment_on_req'] = isset($comnt_requests) ? $comnt_requests : 0;
                $role['billing_module'] = isset($billing_module) ? $billing_module : 0;
                $role['add_brand_pro'] = isset($add_brand_pro) ? $add_brand_pro : 0;
                $role['brand_profile_access'] = isset($access_brand_pro) ? $access_brand_pro : 0;
                $role['view_only'] = isset($view_only) ? $view_only : 0;
                $role['modified'] = date("Y:m:d H:i:s");
                $success = $this->Welcome_model->update_data("users", $customer, array("id" => $cust_id));
                if ($success) {
                    if ($sub_user_permissions) {
                        $role_id = $this->Welcome_model->update_data("user_permissions", $role, array("customer_id" => $cust_id));
                    } else {
                        $this->Welcome_model->insert_data("user_permissions", $role);
                    }
                    $isbrandselected = $this->Request_model->isBrandselected($cust_id);
                    if ($_POST['brandids']) {
                        if ($isbrandselected && $brandIDs) {
                            $isdel = $this->Request_model->deleteAlluserbrands($cust_id);
                            if ($isdel) {
                                foreach ($_POST['brandids'] as $val) {
                                    $role1['user_id'] = $cust_id;
                                    $role1['brand_id'] = $val;
                                    $role1['created'] = date("Y:m:d H:i:s");
                                    $this->Welcome_model->insert_data("user_brand_profiles", $role1);
                                }
                            }
                        } else {
                            foreach ($_POST['brandids'] as $val) {
                                $role1['user_id'] = $cust_id;
                                $role1['brand_id'] = $val;
                                $role1['created'] = date("Y:m:d H:i:s");
                                $this->Welcome_model->insert_data("user_brand_profiles", $role1);
                            }
                        }
                    } else {
                        $this->Request_model->deleteAlluserbrands($cust_id);
                    }
                    $this->session->set_flashdata('message_success', 'User Info Updated successfully..!', 5);
                    redirect(base_url() . "admin/dashboard/sub_user/" . $edit_subuser[0]['parent_id']);
                }
            }
        }
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header', array('edit_profile' => $edit_subuser, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/edit_subuser', array('edit_sub_user' => $edit_subuser, 'brandprofile' => $brandprofile, 'sub_user_permsion' => $sub_user_permissions, 'selectedbrandIDs' => $BIDs, 'cust_id' => $edit_subuser[0]['parent_id']));
        $this->load->view('admin/admin_footer');
    }

    public function send_randompassword_email($email, $randompw) {

        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $body = "Your Account password: " . $randompw;
        $receiver = $email;
        $subject = 'GraphicsZoo Password';
        $message = $body;
        $title = 'GraphicsZoo';
        SM_sendMail($receiver, $subject, $message, $title);
    }
    
public function assign_designer_ajax() {
        $this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');
        $reqIDs = explode(',', $_POST['request_id']);
        $assign_designer =  $this->input->post("assign_designer");
        $output = array();
        if ((isset($_POST['request_id']) && $_POST['request_id'] != '')) {
            $requests_data = $this->Request_model->get_request_by_id('', '', $reqIDs);
            $succ = $this->Admin_model->assign_designer_by_admin($_POST['assign_designer'], $reqIDs);
            if ($succ) {
                foreach ($requests_data as $request) {
                    
                    $assign_id = $this->input->post('assign_designer'); 
                    $checkDraft_id = $this->Welcome_model->select_data("draft_id,designer_id","activity_timeline","request_id='".$request['id']."' AND slug ='assign_designer' AND action_perform_by ='".$login_user_id."'",$limit="",$start="","desc","draft_id");
                    $checkDraft_idForReas = $this->Welcome_model->select_data("draft_id,designer_id","activity_timeline","request_id='".$request['id']."' AND slug ='reassign_designer' AND action_perform_by ='".$login_user_id."' ",$limit="",$start="","desc","id");
  
                   if(!empty($checkDraft_id) ){ 
                    if(empty($checkDraft_idForReas) &&  $checkDraft_id[0]['draft_id'] == 0 && $checkDraft_id[0]['designer_id'] != 0){
                        $new_assign_id =$assign_id;
                        $assign_id =$checkDraft_id[0]['designer_id'];
                        $slug='reassign_designer';
                    }else if($checkDraft_idForReas[0]['draft_id'] != 0 && $checkDraft_idForReas[0]['designer_id'] != 0){
                        $new_assign_id =$assign_id;
                        $assign_id =$checkDraft_idForReas[0]['draft_id'];  
                        $slug='reassign_designer';
                    }
                   }else{
                       $new_assign_id = 0;
                       $assign_id = $assign_id; 
                       $slug='assign_designer';
                   } 
                   $this->myfunctions->capture_project_activity($request['id'],$new_assign_id, $assign_id, $slug, 'assign_designer_ajax', $login_user_id);   

                //                    if ($request['designer_id'] != '0') {
                //                        //$this->myfunctions->capture_project_activity($request['id'], '', $request['designer_id'], 'reassign_designer', 'assign_designer_ajax', $login_user_id);
                //                        $this->myfunctions->capture_project_activity($request['id'],$new_assign_id, $assign_id, $slug, 'assign_designer_ajax', $login_user_id);
                //                    } else {
                //                        $this->myfunctions->capture_project_activity($request['id'], '', '', 'assign_designer', 'assign_designer_ajax', $login_user_id);
                //                    }
                }
                $output['status'] = "success";
                $output['msg'] = "designer updated successfully...!";
            } else {
                $output['status'] = "error";
                $output['msg'] = "Please select atleat one request for assign designer..!";
            }
        } 
        echo json_encode($output);
        exit;
    }
    
public function delete_emailtemp_file() {
    $this->myfunctions->checkloginuser("admin");
    $id = isset($_POST['request_id']) ? $_POST['request_id'] : '';
    $filename = isset($_POST['filename']) ? $_POST['filename'] : '';
    $success = $this->Admin_model->delete_emailtemp_file('email_template', $id, $filename);
    if ($success) {
        $this->load->library('s3_upload');
        $dir = FCPATH . '/public/uploads/emailtemplate_img/';
        $this->s3_upload->delete_file_from_s3($dir . $filename);
    }
    $this->session->set_flashdata('message_success', "File is Deleted Successfully.!", 5);
}


    

    public function change_expectedDate() {
//     echo "<pre/>";print_r($_POST);exit;
    $this->myfunctions->checkloginuser("admin");
    $reqid = isset($_POST['edit_reqid']) ? $_POST['edit_reqid'] : '';
    $expected = isset($_POST['edit_expected']) ? $_POST['edit_expected'] : '';
    $expected_format = date('Y-m-d H:i:s', strtotime($expected));
    $gmttime = $this->myfunctions->onlytimezoneforall($expected_format, $_SESSION['timezone']);
//    echo $gmttime;exit;
    $gmt_format = date('Y-m-d H:i:s', strtotime($gmttime));
    $expectedutc = $this->myfunctions->onlytimezoneforall($gmt_format,'UTC');
    //echo $expectedutc;exit;
    $expectedsave = date('Y-m-d H:i:s', strtotime($expectedutc));
    $request = $this->Request_model->get_request_by_id($reqid);
    $role['expected_date'] = $expectedsave;
//    echo "<pre/>";print_R($role);exit;
    $role_id = $this->Welcome_model->update_data("requests", $role, array("id" => $reqid));
//        echo $role_id;exit;
    if ($role_id) {
        $this->session->set_flashdata('message_success', "Expected date updated successfully!");
        redirect(base_url() . "admin/dashboard");
    } else {
        die("error");
    }
}

public function request_verified_by_admin() {
    $this->myfunctions->checkloginuser("admin");
    $login_user_id = $this->load->get_var('login_user_id');
    $requestId = $this->input->post('data_id');
    $request = $this->Request_model->get_request_by_id_admin($requestId);
    $data = array();
    $ischecked = $this->input->post('ischecked');
    $data['is_verified_by_admin'] = date("Y:m:d H:i:s");
    $data['verified'] = $ischecked;
    //if($ischecked){
    $success = $this->Welcome_model->update_data("requests", $data, array("id" => $requestId));
    $this->myfunctions->capture_project_activity($requestId,'',$request[0]['designer_id'],'verified_by_admin','request_verified_by_admin',$login_user_id);
    if ($success) {
        echo json_encode($data);
    }
    // }
}



    public function add_taxesfor_refund() {
        $this->myfunctions->checkloginuser("admin");
       // echo "<pre/>";print_R($_POST);exit;
        $tax_id = isset($_POST['tax_id']) ? $_POST['tax_id'] : '';
        $add_tax = isset($_POST['add_tax']) ? $_POST['add_tax'] : '';
        $data['tax_amount'] = $add_tax;
        $success = $this->Welcome_model->update_data("sales_tax", $data, array("id" => $tax_id));
        if ($success) {
            $this->session->set_flashdata('message_success', "Tax amount added successfully!");
            redirect(base_url() . "admin/dashboard/reports#Refund_taxes");
        }
    //echo "<pre>";print_r($_POST['tax_id']);
    }

    public function hold_unhold_project_status() {
        $status_h = (isset($_POST['status']) && $_POST['status'] == 'true') ? "hold" : "unhold";
        $request_id = (isset($_POST['data_id'])) ? $_POST['data_id'] : '';
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->Admin_model->getuser_data($login_user_id);
        $request = $this->Request_model->get_request_by_id_admin($request_id);
        $customer_data = $this->Admin_model->getuser_data($request[0]['customer_id']);
        $pervious_status = array("active","disapprove","checkforapprove");
        $getsubscriptionplan = $this->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
        if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
            $turn_around_days = $customer_data[0]['turn_around_days'];
        } else {
            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
        }
        if ($status_h == "hold") {
            $check_status = $request[0]['status'];
            $check_priority = $request[0]['priority'];
            $subcheck_priority = $request[0]['sub_user_priority'];
            $statuses = ($request[0]['status']."_".$request[0]['status_admin']."_".$request[0]['status_designer']."_".$request[0]['status_qa']);
            $success = $this->Welcome_model->update_data("requests", array("previous_status" => $statuses, "status" => "hold", "status_admin" => "hold", "status_designer" => "hold", "status_qa" => "hold","priority" => 0,"sub_user_priority" => 0,"modified" => date("Y-m-d H:i:s")), array("id" => $request_id));
            if($check_status == "assign"){
                $priorfrom = $check_priority + 1;
                $this->Admin_model->update_priority_after_approved_qa(1, $priorfrom, $request[0]['customer_id']);
                if($subcheck_priority > 0){
                    $subpriorfrom = $subcheck_priority + 1;
                    $this->Admin_model->update_subuserpriority_after_approved_qa(1, $request[0]['customer_id'], $request[0]['created_by'],$subpriorfrom);
                }
            }
            $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
            if ($ActiveReq == 1) {
                $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','hold_unhold_project_status',$request_id);
            }
            $this->myfunctions->capture_project_activity($request_id,'',$request[0]['designer_id'],$request[0]['status'].'_to_hold','hold_unhold_project_status',$login_user_id);
        } else if ($status_h == "unhold") {
            $allpreviousstatuses = $this->customfunctions->allStatusafterUnhold($request[0]['previous_status']);       
            $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id'],'','','','','','from_hold',$allpreviousstatuses['status']);
            $subusersdata = $this->Request_model->getAllsubUsers($request[0]['customer_id'], "client", "active");
            if ((!empty($subusersdata) && $getsubscriptionplan[0]['is_agency'] == 1) || ($getsubscriptionplan[0]['is_agency'] == 1 && $customer_data[0]['parent_id'] == 0)) {
                if ($ActiveReq == 1) {
                    $project_user = ($request[0]['created_by'] == 0) ? $request[0]['customer_id'] : $request[0]['created_by'];
                    $pro_userdata = $this->Admin_model->getuser_data($project_user);
                    $checkslot = $this->myfunctions->getslotofallusers($customer_data, $pro_userdata);
                    $checkslot['project_user'] = $project_user;
                    if ($pro_userdata[0]['user_flag'] == 'client') {
                        $logn_usr_data = $pro_userdata;
                    }else{
                        $logn_usr_data = $customer_data;
                    }
                    $subActiveReq = $this->myfunctions->checkactiveslotaccuser($request[0]['customer_id'],$checkslot,$logn_usr_data);
//                    if ($login_user_data[0]['requests_type'] == 'one_time') {
//                        $subActiveReq = $this->myfunctions->CheckActiveRequest($login_user_id, 'created', '', 'one_time','','','from_hold',$allpreviousstatuses['status']);
//                    } else {
//                        $subActiveReq = $this->myfunctions->CheckActiveRequest($login_user_id, 'created','','','','','from_hold',$allpreviousstatuses['status']);
//                    }
                    if ($subActiveReq == 1) {
                        $status = $allpreviousstatuses['status'];
                        $status_admin = $allpreviousstatuses['status_admin'];
                        $status_designer = $allpreviousstatuses['status_designer'];
                        $status_qa = $allpreviousstatuses['status_qa'];
                        $moveabletoactive = 1;
                    } else {
                        $status = "assign";
                        $status_admin = "assign";
                        $status_designer = "assign";
                        $status_qa = "assign";
                        $moveabletoactive = 0;
                        $priority = $this->Welcome_model->priority($request[0]['customer_id']);
                        $priorityupdate = $priority[0]['priority'] + 1;
                        $sub_user_priority = $this->Welcome_model->sub_user_priority($login_user_id);
                        $sub_user_priority = $sub_user_priority[0]['sub_user_priority'] + 1;
                    }
                } else {
                    $status = "assign";
                    $status_admin = "assign";
                    $status_designer = "assign";
                    $status_qa = "assign";
                    $moveabletoactive = 0;
                    $priority = $this->Welcome_model->priority($request[0]['customer_id']);
                    $priorityupdate = $priority[0]['priority'] + 1;
                    $sub_user_priority = $this->Welcome_model->sub_user_priority($login_user_id);
                    $sub_user_priority = $sub_user_priority[0]['sub_user_priority'] + 1;
                }
            } else {
                if ($ActiveReq == 1) {
                    $status = $allpreviousstatuses['status'];
                    $status_admin = $allpreviousstatuses['status_admin'];
                    $status_designer = $allpreviousstatuses['status_designer'];
                    $status_qa = $allpreviousstatuses['status_qa'];
                    $moveabletoactive = 1;
                } else {
                    $status = "assign";
                    $status_admin = "assign";
                    $status_designer = "assign";
                    $status_qa = "assign";
                    $moveabletoactive = 0;
                    $priority = $this->Welcome_model->priority($request[0]['customer_id']);
                    $priorityupdate = $priority[0]['priority'] + 1;
                    $sub_user_priority = 0;
                }
            }
        if ($moveabletoactive == 1) {
            $subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
            $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $request[0]['category_bucket'], $subcat_data[0]['timeline'], $turn_around_days);
            $success = $this->Welcome_model->update_data("requests", array("status" => $status, "status_admin" => $status_admin, "status_designer" => $status_designer, "status_qa" => $status_qa, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "priority" => 0, "sub_user_priority" => 0, "modified" => date("Y-m-d H:i:s")), array("id" => $request_id));
            $this->myfunctions->capture_project_activity($request_id,'',$request[0]['designer_id'],'hold_to_'.$status,'hold_unhold_project_status',$login_user_id,'1');
        } else {
            $success = $this->Welcome_model->update_data("requests", array("status" => $status, "status_admin" => $status_admin, "status_designer" => $status_designer, "status_qa" => $status_qa, "modified" => date("Y-m-d H:i:s"), "priority" => $priorityupdate, "sub_user_priority" => $sub_user_priority), array("id" => $request_id));
            $this->myfunctions->update_priority_after_unhold_project($priorityupdate, '1', $request_id, $request[0]['customer_id']);
            $this->myfunctions->capture_project_activity($request_id,'',$request[0]['designer_id'],'hold_to_'.$status,'hold_unhold_project_status',$login_user_id,'1');
            if ($sub_user_priority > 1) {
                $this->myfunctions->update_sub_userpriority_after_unhold_project($sub_user_priority, '1', $request_id, $login_user_id);
            }
        }
    } else {

        }
    }

    public function update_priority_after_unhold_project($prioritfrom, $prioritto, $request_id, $customer_id) {
        $priorityfrom = $prioritfrom;
        $priorityto = $prioritto;
        $id = $request_id;
        $userid = $customer_id;
        if (isset($userid) && $userid != '') {
            if ($priorityfrom < $priorityto) {
                for ($i = $priorityfrom + 1; $i <= $priorityto; $i++) {
                    $this->Request_model->update_priority($i, $i - 1, '', $userid);
                }
            } else if ($priorityfrom > $priorityto) {
                for ($i = $priorityfrom - 1; $i >= $priorityto; $i--) {
                    $this->Request_model->update_priority($i, $i + 1, '', $userid);
                }
            }
        }
        $this->Request_model->update_priority($priorityfrom, $priorityto, $id);
    }

    public function update_sub_userpriority_after_unhold_project($prioritfrom, $prioritto, $request_id, $customer_id) {
        $priorityfrom = $prioritfrom;
        $priorityto = $prioritto;
        $id = $request_id;
        $userid = $customer_id;
        if (isset($userid) && $userid != '') {
            if ($priorityfrom < $priorityto) {
                for ($i = $priorityfrom + 1; $i <= $priorityto; $i++) {
                    $this->Request_model->update_sub_userpriority($i, $i - 1, '', $userid);
                }
            } else if ($priorityfrom > $priorityto) {
                for ($i = $priorityfrom - 1; $i >= $priorityto; $i--) {
                    $this->Request_model->update_sub_userpriority($i, $i + 1, '', $userid);
                }
            }
        }
        $this->Request_model->update_sub_userpriority($priorityfrom, $priorityto, $id);
    }

    /******dot comments*****/

    public function editComment() {
        $data = array();
        $data['id'] = isset($_POST['request_file_id']) ? $_POST['request_file_id'] : '';
        $data['message'] = isset($_POST['updated_msg']) ? $_POST['updated_msg'] : '';
        $success = $this->Welcome_model->update_data("request_file_chat", $data, array("id" => $data['id']));
        if ($success) {
        //die("update");
            echo json_encode($data);
        }
    }

    public function deleteRequestmsg() {
        $output = array();
        $request_file_id = isset($_POST['request_file_id']) ? $_POST['request_file_id'] : '';
        if ($request_file_id) {
            $success = $this->Request_model->deleteMsg($request_file_id);
            if ($success) {
                $output['status'] = "success";
                $output['msg'] = "message deleted successfully";
            }
        }
        echo json_encode($output);
    }

    public function downloadrequest_accstatus($dataStatusText, $buckettype = "") {
        $this->myfunctions->checkloginuser("admin");
        $status = explode("-", $dataStatusText);
        $active_project = $this->Request_model->admin_load_more($status, false, '', '', '', '', "", "download");
        $filename = 'requests_' . date('Ymd') . '.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
    // file creation 
        $file = fopen('php://output', 'w');

        $header = array("Id", "Title", "Description", "Category", "Status", "Created Date", "Expected Date", "Inprogress Date", "Approval Date", "Designer First Name", "Designer Last Name", "Customer First Name", "Customer Last Name", "Subuser First Name", "Subuser Last Name");
        fputcsv($file, $header);
        foreach ($active_project as $key => $line) {
            fputcsv($file, $line);
        }
        fclose($file);
        exit;
    }
    
    public function adminCheckClassforDesignerAssign($reqstatus) {
            if ($reqstatus == 'active' || $reqstatus == 'disapprove'){
                $addClass = 'in_active';
            }elseif($reqstatus == 'assign'){
                 $addClass = 'in_queue';
            }elseif($reqstatus == 'pendingrevision'){
                $addClass = 'pending_rev';
            }elseif($reqstatus == 'checkforapprove'){
                $addClass = 'pending_app';
            }elseif($reqstatus == 'approved'){
                $addClass = 'completed';
            }elseif($reqstatus == 'hold'){
                $addClass = 'hold';
            }elseif($reqstatus == 'cancel'){
                $addClass = 'cancel';
            }
            return $addClass;
        }
        
    function hasAdminfullaccess($key,$role){
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->Admin_model->getuser_data($login_user_id);
        $userkey = array(4,3,0,5); //key 4 for qa role and 3 for PRT role
       // echo "<pre/>";print_R($login_user_data);
        if((!in_array($login_user_data[0][$key], $userkey) || $login_user_data[0][$key] == NULL)&& $login_user_data[0]['role'] == $role){
            return true;
        }
    }
    
    function activateQa(){
        $data = array();
        $data['id'] = isset($_POST['qa_id'])?$_POST['qa_id']:'';
        $data['is_active'] = isset($_POST['is_active'])?$_POST['is_active']:'';
        $success = $this->Welcome_model->update_data("users", $data, array("id" => $data['id']));
        if($success){
           echo 'success'; 
        }else{
            echo 'fail';
        }
    }
    public function assign_va_tocustomer() {
        $this->myfunctions->checkloginuser("admin");
        $assign = $this->input->post('assign');
        $ids = $this->input->post('customer_id');
        $assign_id = $this->input->post('assign_va');
        $customerid = explode(',', $ids);
        $output = array();
        if ($ids != "") {
            $this->Admin_model->assign_va_touser($assign_id, $customerid,$assign);
            $output['status'] = "success";
            $output['msg'] = "designer updated successfully...!";
        } else {
            $output['status'] = "error";
            $output['msg'] = "Please select atleat one request for assign designer..!";
        }
        echo json_encode($output);
    }
    
    public function setsessionforbackbutton($from,$status,$search=""){
    //  $store_session = array();
      $store_session =  array(
        'previous_status' => $status,
        'previous_search' => $search);
         $_SESSION['page'][$from] = $store_session;
    }
    
    public function displaydataaccsession(){
        if(isset($_SESSION['page'])){
            if(array_key_exists("dashboard", $_SESSION['page'])){
                $status = $_SESSION['page']['dashboard']['previous_status'];
                $search = $_SESSION['page']['dashboard']['previous_search'];
            }
        }
    }
    
    public function deleteRequest() {
        $id = isset($_POST['id']) && $_POST['id'] != '';
        if($id){
            $success = $this->Admin_model->delete_request('requests',array('id' => $id));
            if ($success) {
                $this->session->set_flashdata('success', "request deleted successfully!");
                echo json_encode("success");
            }else{
                $this->session->set_flashdata('error', "request not deleted successfully!");
                 echo json_encode("fail");
            }
        }
        
       // redirect(base_url()."admin/dashboard");
    }
    
    public function filter_clients_show(){
        if((isset($_POST['search_client'])) && $_POST['search_client'] != ''){
           $dataclient = $this->Admin_model->serachFilterClients($_POST['search_client']);
           echo json_encode($dataclient);exit;
           //echo "<pre/>";print_R($dataclient);exit;
        }
    }
    
    public function filter_form_submit(){
        if(!empty($_POST)){
            $clients = (isset($_POST['client_name']) &&  $_POST['client_name'] != '') ? $_POST['client_name'] : '';
            $clientsid = (count($clients) > 1) ? implode(',',$clients) : $clients;
            $designers = (isset($_POST['des_name']) &&  $_POST['des_name'] != '') ? $_POST['des_name'] : '';
            $designersid =  (count($designers) > 1) ? implode(',',$designers) : $designers;
            $pro_status = (isset($_POST['pro_status']) &&  $_POST['pro_status'] != '') ? $_POST['pro_status'] : '';
            $status =  (count($pro_status) > 1) ? implode(',',$pro_status) : $pro_status;
            if($clientsid || $designersid || $statuses){
              $filterurl  = '?clients='.$clientsid.'&designers='.$designersid.'&status='.$status;
            }
            //$this->index('',$filterurl);
            //redirect(base_url()."admin/dashboard".$filterurl);
        }
    }
      public function cancel_request_users() {
        $this->myfunctions->checkloginuser("admin");
        $data = array();
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url() . 'admin/dashboard', "Cancel Request Users" => base_url() . 'admin/dashboard/cancel_request_users');
        $cancl_req_user = $this->Admin_model->cancelledRequest($_POST['search_client']);
        for ($i = 0; $i < sizeof($cancl_req_user); $i++) {
            $data[$i]['first_name'] = $cancl_req_user[$i]['first_name'];
            $data[$i]['last_name'] = $cancl_req_user[$i]['last_name'];
            $data[$i]['email'] = $cancl_req_user[$i]['email'];
             $data[$i]['coupon_code'] = $cancl_req_user[$i]['coupon_code'];
            $data[$i]['reason'] = $cancl_req_user[$i]['reason'];
            $data[$i]['why_reason'] = $cancl_req_user[$i]['why_reason'];
            $data[$i]['cancel_status'] = $cancl_req_user[$i]['cancel_status'];
            $data[$i]['cancel_feedback'] = $cancl_req_user[$i]['cancel_feedback'];
            $data[$i]['is_designer_switch'] = $cancl_req_user[$i]['is_designer_switch'];
             $data[$i]['cid'] = $cancl_req_user[$i]['cid'];
            $data[$i]['created'] = $this->onlytimezone($cancl_req_user[$i]['created'], 'M/d/Y');
        }
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/cancel_request_user', array('cancl_req_user' => $data));
        $this->load->view('admin/admin_footer');
    }
    public function download_projectfile($id, $filename="") {
        $filename = $_GET['imgpath'];
        $file_name1 = FS_PATH_PUBLIC_UPLOADS_REQUESTS . $id . "/" . $filename;
        //$file_name1 = str_replace(" ", "_", $file_name1);
        $this->load->helper('download');
        $path = file_get_contents($file_name1); // get file name
//        echo "<pre/>";print_R($path);exit;
        $name = $filename; // new name for your file
        force_download($name, $path);
        redirect(base_url() . "admin/dashboard/view_request/" . $id);
    }
    
    public function change_cancel_status_by_admin(){
        $data = array();
        $status = isset($_POST['status'])?$_POST['status']:'';
        $cancelid = isset($_POST['cancel_id'])? $_POST['cancel_id']:'';
        if($status == 'false'){
            $data['cancel_status'] = 'close';
        }else{
            $data['cancel_status'] =  'open';
        } 
        $success1 = $this->Welcome_model->update_data("cancelled_subscriptions", $data, array("id" => $cancelid));
        if ($success1) {
            echo 'success';
        } else {
            echo 'fail';
        }
    }

     public function CheckForFeedBack()
    {
        $draftid =  $this->input->post('draftid'); 
         $data = $this->Welcome_model->select_data("*","request_files","id=".$draftid."",$limit="",$start="",$order="",$orderby=""); 
         
            if($data){
                echo json_encode($data); exit; 
            }else{
                echo json_encode($data); exit; 

            } 
    }

    public function design_review_frm($flag="") {
    $login_userid = $this->load->get_var('login_user_id');
    $login_user_id = ($login_userid) ? $login_userid : '0';
    $userdata = $this->Admin_model->getuser_data($login_user_id,"full_admin_access,role");
    if($userdata[0]["full_admin_access"] == 0 && $userdata[0]["role"] == "admin"){
        $role = "va";
    }else{
        $role = $userdata[0]["role"];
    }
    if($login_user_id == 0){
        $requestcustomer_id = $this->Request_model->getRequestcustomerbyReqid($_POST['review_reqid']);
    }else{
       $requestcustomer_id =  $login_user_id;
    }
    $reqid = $this->input->post('review_reqid');
    $drftid = $this->input->post('review_draftid');
    $satisfied_with_design = $this->input->post('satisfied_with_design');
    $additional_notes = $this->input->post('additional_notes');
    $review_sidebr = $this->input->post('review_sidebr'); 
    $reviews = array(
        "draft_id" => $drftid,
        "request_id" => $reqid,
        "user_id" => $requestcustomer_id,
        "user_role" => ($role) ? $role : 'customer',
        "satisfied_with_design" => $satisfied_with_design,
        "additional_notes" => $additional_notes,
        "user_feedback_date" => date("Y-m-d H:i:s")
    );
     $datak = $this->Welcome_model->select_data("id","request_files_review","request_id = '".$reqid."' AND draft_id = '".$drftid."'");
    if(empty($datak)){
         $success = $this->Welcome_model->insert_data("request_files_review",$reviews);
    }else{
        $success =  $this->Welcome_model->update_data("request_files_review",$reviews,array("request_id" => $reqid,"draft_id" => $drftid));
    }
    //$success = $this->Welcome_model->update_data("request_files_review",$reviews,"draft_id=".$drftid.""); 
   // $success = $this->Welcome_model->insert_data("request_files_review", $reviews);
    if($success){
        $this->Welcome_model->update_data("request_files", array("modified" => date("Y:m:d H:i:s"), "recieve_feedback" => 1), array("id" => $drftid));
        if($review_sidebr == "sidebar"){
            $output['status'] = 'success';
            $output['msg'] = 'Thankyou for your valuable feedback!';
            $output['file_id'] = $drftid;
            echo json_encode($output);exit;
        }
        if($flag != "" && $flag == 1 && $login_user_id != 0 && $_POST['from_markascomplete'] != 1){
            redirect(base_url() . "customer/request/project-info/".$reqid);
        }
        elseif($flag != "" && $flag == 1 && $login_user_id == 0 && $_POST['from_markascomplete'] != 1){
             redirect(base_url().'project-info/'.$_POST['share_key']);
        }
        elseif($flag == "" && $login_user_id == 0 && $_POST['from_markascomplete'] != 1){
            if($_POST['share_key'] != ''){
            redirect(base_url() . "welcome/project_image_view/".$_POST['share_key'].'/'.$drftid);
            }else{
                redirect(base_url().'project_view/'.$drftid.'?id='.$requestcustomer_id);
            }
        }
        elseif($_POST['from_markascomplete'] == 1){
           redirect(base_url().'customer/request/markAsCompleted/'.$reqid); 
        }elseif($role == "customer" && $flag == ""){
            $output['status'] = 'success';
            $output['file_id'] = $drftid;
            echo json_encode($output);
        }
        else{
            if($role == "customer"){
                redirect(base_url() . "customer/request/project_image_view/".$drftid."?id=".$reqid);
            }else{
                redirect(base_url() . "admin/dashboard/view_files/".$drftid."?id=".$reqid);
            }
        }
    }else{
        if($review_sidebr == "sidebar"){
            $output['status'] = 'error';
            $output['msg'] = 'Feedback not submitted successfully!';
            $output['file_id'] = $drftid;
            echo json_encode($output);exit;
        }
    }
}

/**********file uploading in main chat*********/
public function uploadmainChatfiles($reqid){
        $arr = array();
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        if (!(isset($_SESSION['chatreqtemp_folder_names'])) && ($_SESSION['chatreqtemp_folder_names'] == '')) {
            if (@mkdir('./public/uploads/chatreqtemp/' . $reqid, 0777, TRUE)) {
                $_SESSION['chatreqtemp_folder_names'] = './public/uploads/chatreqtemp/' . $reqid;
            } else {
                $_SESSION['chatreqtemp_folder_names'] = './public/uploads/chatreqtemp/' . $reqid;
            }
        } else {
            if (!is_dir($_SESSION['chatreqtemp_folder_names'])) {
                if (@mkdir('./public/uploads/chatreqtemp/' . $reqid, 0777, TRUE)) {
                    $_SESSION['chatreqtemp_folder_names'] = './public/uploads/chatreqtemp/' . $reqid;
                } else {
                    $_SESSION['chatreqtemp_folder_names'] = './public/uploads/chatreqtemp/' . $reqid;
                }
            }
        }
        
        if ($_SESSION['chatreqtemp_folder_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['shre_file']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['shre_file']['name'] = str_replace(" ","_",$files['shre_file']['name'][$i]);
                $_FILES['shre_file']['type'] = $files['shre_file']['type'][$i];
                $_FILES['shre_file']['tmp_name'] = $files['shre_file']['tmp_name'][$i];
                $_FILES['shre_file']['error'] = $files['shre_file']['error'][$i];
                $_FILES['shre_file']['size'] = $files['shre_file']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['chatreqtemp_folder_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("shre_file")) {
                    
                   // echo $thumb. "-thumb";exit;
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                $result = $this->addMainchatFileRequest($reqid,$_FILES['shre_file']['name'],$_POST,$_FILES['shre_file']['tmp_name']);
                $arr[$i] = $result;
                } else {
//                    $data = array('error' => $this->upload->display_errors());
//                    $error_data = array();
//                    $error_data[0] = array();
//                    $error_data[0]['file_name'] = $files['shre_file']['name'][$i];
//                    $error_data[0]['error'] = true;
//                    $error_data[0]['error_msg'] = strip_tags($data['error']);
//                    $output['files'][] = $error_data;
//                    $output['status'] = true;
//                    $output['error'] = '';
                    $data = array('error' => $this->upload->display_errors());
                    $output['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $data;
                    $output['status'] = "error";
                    $output['error'] = true;
                    $arr[$i] = $output;
                }
            }
            $uploadPATH = $_SESSION['chatreqtemp_folder_names'];
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        rmdir($uploadPATH."/");
        unset($_SESSION['chatreqtemp_folder_names']);
        echo json_encode($arr);
        
    }
    
public function addMainchatFileRequest($success,$filename,$data,$tmp) {
//    echo $tmp."<br>";
//    echo $filename."<br>";
        $output = array();
        $uid = $this->load->get_var('main_user');
        if (!is_dir('public/uploads/requestmainchat/' . $success)) {
            mkdir('./public/uploads/requestmainchat/' . $success, 0777, TRUE);
        }
        $uploadPATH = $_SESSION['chatreqtemp_folder_names'];
        $uploadPATHs = str_replace("./", "", $uploadPATH);
        $files = scandir($uploadPATH);
        $source = $uploadPATH . '/';
        $destination = './public/uploads/requestmainchat/' . $success . '/';
        $st = $data['data'];
//        echo "<pre>";print_r($files);exit;
        if (UPLOAD_FILE_SERVER == 'bucket') {
//            foreach ($files as $file) {
//                if (in_array($file, array(".", "..")))
//                    continue;
//                echo $uploadPATHs."<br>";
                $staticName = base_url() . $uploadPATHs . '/' . $filename;
                $config = array(
                    'upload_path' => 'public/uploads/requestmainchat/' . $success.'/',
                    'file_name' => $filename
                );
                $this->s3_upload->initialize($config);
                $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticName);
//                echo "<pre>";print_r($is_file_uploaded);
                if ($is_file_uploaded['status'] == 1) {
                    
                    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                    $img_arry = array("jpeg","jpg","png","gif");
                    if(in_array($ext,$img_arry)){
                        if (!is_dir('public/uploads/chatreqtemp/' . $success . '/_thumb')) {
                            $data = mkdir('./public/uploads/chatreqtemp/' . $success . '/_thumb', 0777, TRUE);
                        }
                        $thumb_tmp_path = 'public/uploads/chatreqtemp/' . $success . '/_thumb/';
                        $thumb = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $filename, 600, $ext);
                        if($thumb){
                            $staticName1 = base_url() . $thumb_tmp_path . $filename;
                            $config = array(
                                'upload_path' => 'public/uploads/requestmainchat/' . $success . '/_thumb/',
                                'file_name' => $filename
                            );
                            $this->load->library('s3_upload');
                            $this->s3_upload->initialize($config);
                            $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName1);
                            if ($uplod_thumb['status'] == 1) {
                                $output['file_status'] = 1;
                                $deletee = $thumb_tmp_path . $filename;
                                unlink($deletee);
                                unlink('./tmp/tmpfile/' . basename($staticName1));
                            }else{
                                $output['file_status'] = 0;
                                $deletee = $thumb_tmp_path . $filename;
                                unlink($deletee);
                                unlink('./tmp/tmpfile/' . basename($staticName1));
                            }
                        }
                    }else{
                        $output['file_status'] = 0;
                    }
                    $delete[] = $source . $filename;
                    unlink('./tmp/tmpfile/' . basename($staticName)); 
                    $request_files_data = array("request_id" => $success,
                    "sender_id" => $uid,
                    "reciever_id" => $st['reciever_id'],
                    "reciever_type" => $st['reciever_role'],
                    "sender_type" => $st['sender_role'],
                    "message" => $filename,
                    "with_or_not_customer" => $st['withornot'],
                    "admin_seen" => ($st['seenby'] == 'admin_seen')?'1':'0',
                    "customer_seen" => ($st['seenby'] == 'customer_seen')?'1':'0',
                    "is_filetype" => 1,
                    "created" => date("Y-m-d H:i:s"));
//                    echo "<pre>";print_r($request_files_data);
                $succ_id = $this->Welcome_model->insert_data("request_discussions", $request_files_data);
                if($succ_id){
                    $output['status'] = "success";
                    $output['req_id'] = $success;
                    $output['file'] = $filename;
                    $output['id'] = $succ_id;
                }else{
                   $output['status'] = "error";
                }
                }
//            }
           // rmdir($thumb_tmp_path);
            
        } 
        else {
            foreach ($files as $file) {
                if (in_array($file, array(".", "..")))
                    continue;
                $filepath = APPPATH . $source . $file;
                if (copy($source . $file, $destination . $file)) {
                    $delete[] = $source . $file;
                    unlink($source.$file);
                }
                $is_file_uploaded['status'] = 1;
            }
        }
        foreach ($delete as $file) {
            unlink($file);
        }
        
        
     
        $config['image_library'] = 'gd2';
//        if ($is_file_uploaded['status'] == 1) {
          return $output;
//        }
        }
        
        
/**********file uploading in draft chat*********/
public function uploaddraftChatfiles(){
        $arr = array();
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        $reqid = isset($_POST['data']['request_id'])?$_POST['data']['request_id']:'';
        $reqfileid = isset($_POST['data']['request_file_id'])?$_POST['data']['request_file_id']:'';
        if (!(isset($_SESSION['chatreqdrafttemp_folder_names'])) && ($_SESSION['chatreqdrafttemp_folder_names'] == '')) {
              if(@mkdir('./public/uploads/chatreqdraftemp/' . $reqid. '/' .$reqfileid , 0777, TRUE)){
                $_SESSION['chatreqdrafttemp_folder_names'] = './public/uploads/chatreqdraftemp/' . $reqid.'/'.$reqfileid;
              } else {
                $_SESSION['chatreqdrafttemp_folder_names'] = './public/uploads/chatreqdraftemp/' . $reqid.'/'.$reqfileid;
              }
        } else {
              if(@mkdir('./public/uploads/chatreqdraftemp/' . $reqid. '/' .$reqfileid , 0777, TRUE)){
                $_SESSION['chatreqdrafttemp_folder_names'] = './public/uploads/chatreqdraftemp/' . $reqid.'/'.$reqfileid;
              } else {
                $_SESSION['chatreqdrafttemp_folder_names'] = './public/uploads/chatreqdraftemp/' . $reqid.'/'.$reqfileid;
              }
        }
        if ($_SESSION['chatreqdrafttemp_folder_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['shre_main_draft_file']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['shre_main_draft_file']['name'] = str_replace(" ","_",$files['shre_main_draft_file']['name'][$i]);
                $_FILES['shre_main_draft_file']['type'] = $files['shre_main_draft_file']['type'][$i];
                $_FILES['shre_main_draft_file']['tmp_name'] = $files['shre_main_draft_file']['tmp_name'][$i];
                $_FILES['shre_main_draft_file']['error'] = $files['shre_main_draft_file']['error'][$i];
                $_FILES['shre_main_draft_file']['size'] = $files['shre_main_draft_file']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['chatreqdrafttemp_folder_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("shre_main_draft_file")) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                $result = $this->adddarftchatFileRequest($reqid,$reqfileid,$_FILES['shre_main_draft_file']['name'],$_POST,$_FILES['shre_main_draft_file']['tmp_name']);
//                echo "<pre/>";print_R($result);exit;
                $arr[$i] = $result;
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $output['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $data;
                    $output['status'] = "error";
                    $output['error'] = true;
                    $arr[$i] = $output;
                }
            }
            $uploadPATH = $_SESSION['chatreqdrafttemp_folder_names'];
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        rmdir($uploadPATH."/");
        unset($_SESSION['chatreqdrafttemp_folder_names']);
        echo json_encode($arr);
        
    }
    
public function adddarftchatFileRequest($success,$reqfileid,$filename,$data,$tmp) {
    $output = array();
    $uid = $this->load->get_var('main_user');
    if (!is_dir('public/uploads/requestdraftmainchat/' . $success)) {
        if (!is_dir('public/uploads/requestdraftmainchat/' . $success.'/'.$reqfileid)) {
         mkdir('./public/uploads/requestdraftmainchat/' . $success.'/'.$reqfileid, 0777, TRUE);
        }
    }
    $uploadPATH = $_SESSION['chatreqdrafttemp_folder_names'];
    $uploadPATHs = str_replace("./", "", $uploadPATH);
    $files = scandir($uploadPATH);
    $source = $uploadPATH . '/';
    $destination = './public/uploads/requestdraftmainchat/' . $success . '/'.$reqfileid.'/';
//    if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES . $success . '/'.$reqfileid . '/_thumb')) {
//        mkdir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES . $success . '/'.$reqfileid . '/_thumb', 0777, TRUE);
//    }
    $st = $data['data'];
    $config['image_library'] = 'gd2';
    $img_arry = array("jpeg", "jpg", "png", "gif");
    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
//    echo "<pre/>";print_R($files);
    if (UPLOAD_FILE_SERVER == 'bucket') {
        $staticdraftName = base_url() . $uploadPATHs . '/' . $filename;
        $config = array(
            'upload_path' => 'public/uploads/requestdraftmainchat/' . $success . '/'.$reqfileid.'/',
            'file_name' => $filename
        );
        $this->s3_upload->initialize($config);
        $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticdraftName);
        if ($is_file_uploaded['status'] == 1) {
            if (in_array($ext, $img_arry)) {
                if (!is_dir('public/uploads/chatreqdraftemp/' . $success . '/'.$reqfileid.'/_thumb')) {
                    $data = mkdir('./public/uploads/chatreqdraftemp/' . $success . '/'.$reqfileid.'/_thumb', 0777, TRUE);
                }
                $thumb_tmp_path = 'public/uploads/chatreqdraftemp/' . $success . '/'.$reqfileid.'/_thumb/';
                $thumb_create = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $filename, 600, $ext);
                if($thumb_create){
                    $staticdraftName1 = base_url() . $thumb_tmp_path . $filename;
                    //echo $staticdraftName1;
                        $config = array(
                            'upload_path' => 'public/uploads/requestdraftmainchat/' . $success . '/'.$reqfileid.'/_thumb/',
                            'file_name' => $filename
                        );
                       // echo "<pre>";print_r($config);
                        $this->load->library('s3_upload');
                        $this->s3_upload->initialize($config);
                        $uplod_thumb = $this->s3_upload->upload_multiple_file($staticdraftName1);
                        if ($uplod_thumb['status'] == 1) {
                            $output['file_status'] = 1;
                            $deletee = $thumb_tmp_path . $filename;
                            unlink($deletee);
                            unlink('./tmp/tmpfile/' . basename($staticdraftName1));
                        } else {
                            $output['file_status'] = 0;
                            $deletee = $thumb_tmp_path . $filename;
                            unlink($deletee);
                            unlink('./tmp/tmpfile/' . basename($staticdraftName1));
                        }
                }
            }else{
                $output['file_status'] = 0;
            }
            $delete[] = $source . $filename;
            unlink('./tmp/tmpfile/' . basename($staticName));
            
            $request_files_data = array("request_file_id" => $reqfileid,
            "sender_role" => $st['sender_role'],
            "sender_id" => $uid,
            "receiver_role" => $st['reciever_role'],
            "parent_id" => $st['parentid'],
            "receiver_id" => $st['reciever_id'],
            "message" => $filename,
            "admin_seen" => ($st['seenby'] == 'admin_seen')?'1':'0',
            "customer_seen" => ($st['seenby'] == 'customer_seen')?'1':'0',
            "designer_seen" => ($st['seenby'] == 'designer_seen')?'1':'0',
            "is_filetype" => 1,
            "created" => date("Y-m-d H:i:s"));
        $succ_id = $this->Welcome_model->insert_data("request_file_chat", $request_files_data);
        if($succ_id){
            $output['status'] = "success";
            $output['req_id'] = $success;
            $output['file_id'] = $reqfileid;
            $output['file'] = $filename;
            $output['id'] = $succ_id;
        }else{
            $output['status'] = "error";
        }
        }
    }
    else {
        foreach ($files as $file) {
            if (in_array($file, array(".", "..")))
                continue;
            $filepath = APPPATH . $source . $file;
            if (copy($source . $file, $destination . $file)) {
                $delete[] = $source . $file;
                unlink($source.$file);
            }
            $is_file_uploaded['status'] = 1;
        }
      if($is_file_uploaded['status'] == 1){
            $this->load->library('image_lib', $config);
        if (in_array($ext, $img_arry)) {
            $thumb_tmp_path = './public/uploads/requestdraftmainchat/' . $success .'/'.$reqfileid.'/_thumb/';
            $thumb = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $filename, 600, $ext);
        }else{
            $filename = preg_replace('/\s+/', '_', $filename);
            $previewname = $filename;
        }
        $request_files_data = array("request_file_id" => $reqfileid,
            "sender_role" => $st['sender_role'],
            "sender_id" => $uid,
            "receiver_role" => $st['reciever_role'],
            "parent_id" => $st['parentid'],
            "receiver_id" => $st['reciever_id'],
            "message" => $filename,
            "admin_seen" => ($st['seenby'] == 'admin_seen')?'1':'0',
            "customer_seen" => ($st['seenby'] == 'customer_seen')?'1':'0',
            "designer_seen" => ($st['seenby'] == 'designer_seen')?'1':'0',
            "is_filetype" => 1,
            "created" => date("Y-m-d H:i:s"));
        $succ_id = $this->Welcome_model->insert_data("request_file_chat", $request_files_data);
        if($succ_id){
            $output['status'] = "success";
            $output['req_id'] = $success;
            $output['file_id'] = $reqfileid;
            $output['file'] = $filename;
            $output['id'] = $succ_id;
        }else {
            $output['status'] = "error";
        }
    }
        
    }
        return $output;
    } 
        
    public function get_brand_details() {
        $brandid = isset($_POST["brandid"])?$_POST["brandid"]:"";
        $branddata = $this->Request_model->get_brandprofile_by_id($brandid);
        $brand_materials_files = $this->Request_model->get_brandprofile_materials_files($branddata[0]['id']);
        for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
            if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
                $brand_materials_files['latest_logo'] = $brand_materials_files[$i]['filename'];
                break;
            }
        }
        $this->load->view('admin/brands_data', array("branddata" => $branddata,"brand_materials_files" => $brand_materials_files));
        
    }
    
    public function getadminuserrole($flag) {
        if($flag == 1){
            $role = "Admin";
        }else if($flag == 2){
            $role = "CST";
        }else if($flag == 3){
            $role = "PRT";
        }else if($flag == 4){
            $role = "QA";
        }else if($flag == 5){
            $role = "AM";
        }
        
        return $role;

    }
}



