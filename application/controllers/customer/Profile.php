<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('Myfunctions');
        $this->load->model('Admin_model');
        $this->load->model('Welcome_model');
        $this->load->model('Account_model');
        $this->load->model('Request_model');
        $this->load->model('Stripe');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
    }

//    public function checkloginuser() {
//        if (!$this->session->userdata('user_id')) {
//            redirect(base_url());
//        }
//        if ($this->session->userdata('role') != "customer") {
//            redirect(base_url());
//        }
//    }

    public function dashboard() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        redirect(base_url() . "customer/request/design_request");
        $today_customer = $this->Admin_model->get_today_customer();
        $all_customer = $this->Admin_model->get_total_customer();
        $today_requests = $this->Admin_model->get_today_request();
        $total_requests = $this->Admin_model->get_total_request();

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);

        $this->load->view('customer/customer_header_1', array("notification_number" => $notification_number, "notifications" => $notifications));
        $this->load->view('admin/index', array('today_customer' => $today_customer, 'all_customer' => $all_customer, 'today_requests' => $today_requests, 'total_requests' => $total_requests));
        $this->load->view('customer/footer_customer');
    }

	public function thank_you(){
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $uid = $_SESSION['user_id'];
        $customer_ques = array();
        if(isset($_POST['quest_submit'])){
            $designtype = isset($_POST['design_type'])? $_POST['design_type'] :'';
            $customer_ques['is_survey'] =  1;
            $customer_ques['business_detail'] =  isset($_POST['business_detail'])? $_POST['business_detail'] : '';
            $customer_ques['company_role'] =  isset($_POST['company_role'])? $_POST['company_role'] : '';
            $customer_ques['design_count'] =  isset($_POST['design_count'])? $_POST['design_count'] : '';
            $customer_ques['design_type'] =  implode(",", $designtype);
            $customer_ques['phone'] =  isset($_POST['mob_no'])? $_POST['mob_no'] : '';
            $success = $this->Welcome_model->update_data("users", $customer_ques, array('id'=>$uid));
            if($success){
                //$this->session->set_flashdata('message_success', "Thanks for providing the valuable info!", 5);
                redirect(base_url() . "customer/request/design_request");
            }
        }
        //$this->load->view('customer/customer_header_1', array("notification_number" => $notification_number, "notifications" => $notifications));
        $this->load->view('customer/thank_you', array(''));
        //$this->load->view('customer/footer_customer');
    }
    
    public function send_email_after_signup($activation_code, $id, $email) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $data = array(
            'activation' => $activation_code,
            'user_id' => $id,
        );
        $body = $this->load->view('emailtemplate/signup_template', $data, TRUE);
        $receiver = $email;

        $subject = 'GraphicsZoo Account confirmation.';

        $message = $body;

        $title = 'GraphicsZoo';

        SM_sendMail($receiver, $subject, $message, $title);
    }
    
    public function thank_you_freedesign(){
        $id = $_SESSION['user_id'];
        $email = $_SESSION['email'];
        $this->send_email_after_signup('abcdes',$id,$email);
        $this->session->set_flashdata('message_success', "Email is sent to you!", 5);
        redirect(base_url().'thank-you-free-design');
    }
	
    public function all_requests() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $requested_designs = $this->Admin_model->get_all_requested_designs(array("active", "disapprove"));

        $inprogressrequest = $this->Admin_model->get_all_requested_designs(array("assign", "pending"));
        $checkforapproverequests = $this->Admin_model->get_all_requested_designs(array("checkforapprove"));
        $approved_designs = $this->Admin_model->get_all_requested_designs(array("approved"));

        //$get_message = $this->Admin_model->get_unread_message();

        $admin_unread_message_count_array = array();
        $total_unread_message = 0;
        $total_assign_message = 0;
        $total_checkforapprove_message = 0;
        $total_approved_message = 0;

        for ($i = 0; $i < sizeof($requested_designs); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($requested_designs[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array[$requested_designs[$i]['id']] = $admin_unread_msg_count;
            if ($requested_designs[$i]['status_admin'] == "active" || $requested_designs[$i]['status_admin'] == "disapprove") {
                $total_unread_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "assign") {
                $total_assign_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "checkforapprove") {
                $total_checkforapprove_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "approved") {
                $total_approved_message += $admin_unread_msg_count;
            }
        }

        $admin_unread_message_count_array2 = array();
        $total_unread_message2 = 0;
        for ($i = 0; $i < sizeof($approved_designs); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($approved_designs[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array2[$approved_designs[$i]['id']] = $admin_unread_msg_count;
            $total_unread_message2 += $admin_unread_msg_count;
        }
        for ($i = 0; $i < sizeof($inprogressrequest); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($inprogressrequest[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array2[$inprogressrequest[$i]['id']] = $admin_unread_msg_count;
            $total_unread_message2 += $admin_unread_msg_count;
        }
        $customers = $this->Admin_model->get_total_customer();

        $customerarray = array();

        for ($i = 0; $i < sizeof($customers); $i++) {
            if ($customers[$i]['current_plan'] != "") {

                if ($customers[$i]['plan_turn_around_days'] == 1) {
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "red";
                } elseif ($customers[$i]['plan_turn_around_days'] !== 3) {
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "blue";
                } else {
                    $customerarray[$i]['current_plan_name'] = "";
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "";
                }
            } else {
                $customerarray[$i]['current_plan_name'] = "";
                $customerarray[$customers[$i]['id']]['current_plan_color'] = "";
            }
        }

        $this->load->view('admin/admin_header');
        $this->load->view('admin/all_requests', array("customerarray" => $customerarray, "customers" => $customers, "total_unread_message2" => $total_unread_message2, "admin_unread_message_count_array2" => $admin_unread_message_count_array2, "total_approved_message" => $total_approved_message, "total_checkforapprove_message" => $total_checkforapprove_message, "total_assign_message" => $total_assign_message, "total_unread_message" => $total_unread_message, "admin_unread_message_count_array" => $admin_unread_message_count_array, "requested_designs" => $requested_designs, "inprogressrequest" => $inprogressrequest, "checkforapproverequests" => $checkforapproverequests, "approved_designs" => $approved_designs));
       $this->load->view('customer/footer_customer');
    }

    public function chat() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $user_data = $this->Account_model->getuserbyid($_SESSION['user_id']);
        if ($user_data[0]['designer_id']) {
            $room_id = $this->Account_model->chat_data("customer_id", $_SESSION['user_id'], "designer_id", $user_data[0]['designer_id']);
            redirect(base_url() . "customer/profile/room_chat/" . $room_id);
        }
        $data['chat_customer'] = $this->Account_model->get_user_for_chat("customer_id", $_SESSION['user_id']);
         for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {

            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['designer_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);

        $this->load->view('customer/customer_header', array("notification_number" => $notification_number, "notifications" => $notifications));
        $this->load->view('customer/chat', $data);
        $this->load->view('customer/footer_customer');
    }

    public function chat_staff() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $user_data = $this->Account_model->getuserbyid($_SESSION['user_id']);
        if ($user_data[0]['designer_id']) {
            $room_id = $this->Account_model->chat_data("customer_id", $_SESSION['user_id'], "designer_id", $user_data[0]['designer_id']);
        }
        $data['chat_customer'] = $this->Account_model->get_user_for_chat("customer_id", $_SESSION['user_id']);
        for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {
            if ($data['chat_customer'][$i]['va_id'] != "") {
                redirect(base_url() . "customer/profile/room_chat/" . $data['chat_customer'][$i]['room_id']);
            }
        }
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);

        $this->load->view('customer/customer_header', array("notification_number" => $notification_number, "notifications" => $notifications));
        $this->load->view('customer/chat', $data);
        $this->load->view('customer/footer_customer');
    }

    public function room_chat($room_no = null) {
        if ($room_no == "") {
            redirect(base_url() . "customer/profile/chat");
        }
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("url" => "customer/profile/room_chat/".$room_no));
        $data['userdata'] = $this->Account_model->get_userdata_from_room_id($room_no);
        $data['chat'] = $this->Account_model->get_chat($room_no);

        $data['room_no'] = $room_no;

        $data['chat_customer'] = $this->Account_model->get_user_for_chat("customer_id", $_SESSION['user_id']);
        for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {

            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['designer_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $user = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $data['myname'] = $user[0]['first_name'] . " " . $user[0]['last_name'];

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('customer/customer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));

        $this->load->view('customer/chat', $data);
        $this->load->view('customer/footer_customer');
    }

    public function get_chat_ajax() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
		$this->Welcome_model->update_data("message_notification", array("shown" => 1), array("url" => "customer/profile/room_chat/".$_POST['roomid']));
        $chat = $this->Account_model->get_chat($_POST['roomid']);
        $data['room_no'] = $_POST['roomid'];
		$room_no = $_POST['roomid'];
        $data['chat_customer'] = $this->Account_model->get_user_for_chat("customer_id", $_SESSION['user_id']);
        $userdata = $this->Account_model->get_userdata_from_room_id($_POST['roomid']);

        for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {
            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['designer_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $user = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $data['myname'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
		$myname = $data['myname'];
		$chat_customer = $data['chat_customer']
        ?>

		<div class="chatpage-tit">
			<h4><?php
				if (isset($chat_customer[0]['chat_name'])) {
					echo $chat_customer[0]['chat_name'];
				}
				?></h4>
		</div>

		<div class="chat-main">

			<?php if (isset($chat)) { ?>

				<div class="myscroller2 roomchat messagediv_<?php echo $room_no; ?>" style="height:500px; overflow-y: scroll;">
					<?php for ($i = 0; $i < sizeof($chat); $i++) { ?>


						<!--<div>-->
						<?php if ($_SESSION['user_id'] == $chat[$i]['sender_id']) { ?>


							<div class="chatbox2 right">
								<!--<div class="avatar">
									&nbsp;
								</div>-->

								<div class="message-text-wrapper messageright">
									<p class="description">
										<?php 
										if($chat[$i]['isimage'] == "1"){
											echo '<img src="'.base_url()."uploads/requests/".$chat[$i]['message'].'" />'; 
										}else{ 
											 echo $chat[$i]['message']; 
										} ?>
									</p>
									<p class="posttime">
										<?php
										$date = strtotime($chat[$i]['created']);
										$diff = strtotime(date("Y:m:d H:i:s")) - $date;
										$minutes = floor($diff / 60);
										$hours = floor($diff / 3600);
										if ($hours == 0) {
											echo $minutes . " Minutes Ago";
										} else {
											$days = floor($diff / (60 * 60 * 24));
											if ($days == 0) {
												echo $hours . " Hours Ago";
											} else {
												echo $days . " Days Ago";
											}
										}
										?>
									</p>
								</div>
							</div>


						<?php } else { ?>

							<div class="chatbox left">
								<div class="avatar">
									<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
								</div>

								<div class="message-text-wrapper messageleft">

									<p class="title">
										<?php echo $userdata[$chat[$i]['sender_id']]; ?>
									</p>
									<p class="description">
										<?php 
										if($chat[$i]['isimage'] == "1"){
											echo '<img src="'.base_url()."uploads/requests/".$chat[$i]['message'].'" />'; 
										}else{ 
											 echo $chat[$i]['message']; 
										} ?>
									</p>
									<p class="posttime">
										<?php
										$date = strtotime($chat[$i]['created']);
										$diff = strtotime(date("Y:m:d H:i:s")) - $date;
										$minutes = floor($diff / 60);
										$hours = floor($diff / 3600);
										if ($hours == 0) {
											echo $minutes . " Minutes Ago";
										} else {
											$days = floor($diff / (60 * 60 * 24));
											if ($days == 0) {
												echo $hours . " Hours Ago";
											} else {
												echo $days . " Days Ago";
											}
										}
										?>
									</p>

								</div>
							</div>
						<?php } ?>
						<!--</div>-->

					<?php } ?>
				</div>

				<!--<div class="col-md-12" style="margin-top: 20px;">
					<input type='text' class='form-control myText room_chat_text text_<?php echo $room_no; ?>' style="border: 1px solid !important;border-radius: 6px !important;" placeholder="Type a message here" />
					<span style="position: absolute;top: 10px;right: 30px;font-size: 18px;" class="pinktext fa fa-send send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>"></span>
				</div>-->


				<!--<form class="myform" method="post" action="" enctype="multipart/form-data"  style="clear: both;position: absolute;bottom: -7px;z-index: 9999999999999;">

					<span class="chatcamera"><i class="fa fa-camera" id="openimageupload"></i></span>
					<div class="hiddenfile">
						<input name="upload" type="file" id="fileinput" onchange="validateAndUpload(this);"/>
					</div>
					<input type="hidden" value="<?php echo $room_no; ?>" class="hidden_room_no" name="room_no"/>
					<button type="submit" style="margin-top: -16px; margin-right: 15px;display: none;" class="myfilesubmit">Image Submit</button>

				</form>
				<input type='text' class='form-control myText room_chat_text text_<?php echo $room_no; ?>' style="border: 1px solid; border-radius: 0px !important; height: 60px; padding-right: 30px; padding-left: 60px; box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.2); margin-top: 20px; position: relative; bottom: -20px; left: -20px; right: -20px;" placeholder="Type a message here" />
				<span style="margin-top: -15px; margin-right: 15px;" class="chatsendbtn send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>">Send</span>-->



				<!--<input  type='text' class='form-control sendtext text_<?php echo $data[0]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px; height: 44px;" placeholder="Reply To Client" />
				<span class="chatsendbtn send_request_chat send"
					  data-requestid="<?php echo $data[0]['id']; ?>"
					  data-senderrole="customer"
					  data-senderid="<?php echo $_SESSION['user_id']; ?>"
					  data-receiverid="<?php echo $data[0]['designer_id']; ?>"
					  data-receiverrole="designer"
					  data-sendername="<?php echo $data[0]['customer_name']; ?>">Send
				</span>-->

			<?php } ?>

		</div>

		<div class="chatbtn-boxmain">
			<form onsubmit="myform_notsubmit(event,this);" class="myform" method="post" action="" enctype="multipart/form-data"  style="clear: both;position: absolute;bottom: -7px;z-index: 9999999999999;">
				<span class="chatcamera"><i class="fa fa-paperclip" id="openimageupload" onclick="$('#fileinput').click();"></i></span>
				<div class="hiddenfile">
					<input name="upload" type="file" id="fileinput" onchange="validateAndUpload(this);"/>
				</div>
				<input type="hidden" value="<?php echo $room_no; ?>" class="hidden_room_no" name="room_no"/>
				<button type="submit" style="margin-top: -16px; margin-right: 15px;display: none;" class="myfilesubmit">Image Submit</button>
			</form>
			<input type='text' class='form-control chatlargeinp myText room_chat_text text_<?php echo $room_no; ?>' placeholder="Type a message here" />
			<span style="margin-right: 15px;" class="chatsendbtn send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>"  onclick="send_room_chat();">Send</span>
	   </div>

        <?php
    }

    //chat ajex
    public function get_chat_customer_ajex()
    {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        
        $data['room_no'] = $_POST['roomid'];
        $room_no = $_POST['roomid'];
        
        $data['chat_customer'] = $this->Account_model->get_user_for_chat("customer_id", $_SESSION['user_id']);
        for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {

            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['designer_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $chat_customer = $data['chat_customer'];
        $myname = "";
        for ($i = 0; $i < sizeof($chat_customer); $i++) {
            ?>

            
			
			<div data-roomid="<?php echo $chat_customer[$i]['room_id']; ?>" onclick="chatlistbox('<?php echo $chat_customer[$i]['room_id']; ?>')" class="chatlistbox <?php
			if ($chat_customer[$i]['room_id'] == $room_no) {
				echo 'active';
			}
			?>">
				<div class="avatar">
					<?php if ($chat_customer[$i]['image']) { ?>
						<img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_customer[$i]['image']; ?>" class="img-responsive" />
					<?php } else { ?>
						<div class="myprofilebackground" style="width:50px; height:50px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 24px;padding-top: 7px;">
								<?php echo $chat_customer[$i]['sort_name']; ?></p></div>
					<?php } ?>
				</div>
				<div class="message-text-wrapper topspacen">
					<div class="posttime">Dec 11</div>
					<p class="title"><?php echo $chat_customer[$i]['title']; ?></p>
					<p class="message graytext"><?php echo substr($chat_customer[$i]['last_message'],0,20); ?></p>
					<!--<p class="<?php echo $chat_customer[$i]['status_class']; ?> status"><?php echo $chat_customer[$i]['status']; ?></p>-->
					<div class="<?php echo strtolower($chat_customer[$i]['status']); ?>"><i class="fa fa-circle"></i></div>
				</div>
				<div class="clear"></div>
			</div>
            <?php
        }
    }

    public function send_room_message() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            $_POST['created'] = date("Y-m-d H:i:s");
			
			$room_data = $this->Account_model->get_userdata_from_room_id($_POST['room_id']);
			$not_data['url']="designer/profile/room_chat/".$_POST['room_id'];
			$not_data['created']=date("Y:m:d H:i:s");
			$not_data['heading']="";
			$not_data['title']=substr($_POST['message'],0,30)."...";
			
			$ids = array();
			foreach($room_data as $id=>$name){
				if($id == $_SESSION['user_id']){
					$not_data['heading'] = $name;
				}else{
					$ids[] = $id; 
				}
			}
			for($i=0;$i<sizeof($ids);$i++){
				$not_data['user_id'] = $ids[$i];
				$mn = $this->Request_model->get_request_by_data($not_data['url'],$not_data['heading'],$not_data['user_id'],0);
				 if(empty($mn))
			   {
				 $this->Welcome_model->insert_data("message_notification", $not_data);  
			   }
			   else
			   {
				   $this->Welcome_model->update_data("message_notification",array("count"=>$mn[0]['count']+1,"created"=>date("Y:m:d H:i:s"),"title"=>$not_data['title']),array("url"=>$not_data['url'],"heading"=>$not_data['heading'],"user_id"=>$not_data['user_id'],"shown"=>0));
			   }
			}
            $success = $this->Welcome_model->insert_data("room_chat", $_POST);

            echo $success;
            exit;
        }
    }
    
    

    public function imgupload()
    {
		// print_R($_POST);
		// print_R($_FILES);
        if (!empty($_FILES)) {
            if ($_FILES['upload']['name'] != "") {
                    $config2 = array(
                        'upload_path' => './uploads/requests/',
                        'allowed_types' => "*",
                        'encrypt_name' => TRUE
                    );
                    $config2['preview_file'] = $_FILES['upload']['name'];

                    $_FILES['src_file']['name'] = $_FILES['upload']['name'];
                    $_FILES['src_file']['type'] = $_FILES['upload']['type'];
                    $_FILES['src_file']['tmp_name'] = $_FILES['upload']['tmp_name'];
                    $_FILES['src_file']['error'] = $_FILES['upload']['error'];
                    $_FILES['src_file']['size'] = $_FILES['upload']['size'];
                    $this->load->library('upload', $config2);

                    if ($this->upload->do_upload('upload')) {
                        $data = array($this->upload->data());
                        $data = $this->Welcome_model->insert_data("room_chat", array("room_id"=>$_POST['room_no'],"sender_id"=>$_SESSION['user_id'],"message"=>$data[0]['file_name'],"isimage"=>1,"created"=>date("Y:m:d H:i:s")));
                        echo $data;
                    } else {
                        echo '0';
                    }
                }
            } else {
                echo '0';
            }
    }

}
