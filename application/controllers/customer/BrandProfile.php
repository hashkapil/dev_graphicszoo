<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class BrandProfile extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Clients_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $this->load->library('zip');

        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    } 
    public function brand_profiles() {
        $this->myfunctions->checkloginuser("customer");
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $notifications = $this->Request_model->get_notifications($login_user_id);
        $notification_number = $this->Request_model->get_notifications_number($login_user_id);
        $messagenotifications = $this->Request_model->get_messagenotifications($login_user_id);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($login_user_id);
        $profile_data = $this->Admin_model->getuser_data($login_user_id);
        $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : "";
        if ($profile_data[0]['user_flag'] == 'client' && $client_id == '') {
            $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($login_user_id, 'created_by');
        }elseif($client_id != ''){
            $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($client_id, 'created_by');
        }else {
            $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($created_user);
        }
        $subusersdata = array();
        if($profile_data[0]['user_flag'] != 'client'){
          $subusersdata = $this->Request_model->getAllsubUsers($created_user,"client","","","id,first_name,last_name,profile_picture");
        }
        for($i=0; $i<sizeof($user_profile_brands);$i++){
            $brand_materials_files = $this->Request_model->get_brandprofile_materials_files($user_profile_brands[$i]['id']);
            for ($j = 0; $j < sizeof($brand_materials_files); $j++) {
                if ($brand_materials_files[$j]['file_type'] == 'logo_upload') {
                    $user_profile_brands[$i]['latest_logo'] = $brand_materials_files[$j]['filename'];
                    break;
                }
            }
            $user_profile_brands[$i]['client_name'] = $this->Request_model->getuserbyid($user_profile_brands[$i]['created_by'],'client');
        }
        $is_add_brandprofile = $this->is_add_brand_profile($created_user);
        
        //echo sizeof($user_profile_brands)."<pre>";print_r($user_profile_brands);exit;    
        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));

        $this->load->view('customer/brand_profiles', array("user_profile_brands" => $user_profile_brands,"is_add_brandprofile" => $is_add_brandprofile,'subusersdata' => $subusersdata,'profile_data' => $profile_data));
        $this->load->view('customer/footer_customer');
    }
    
     public function delete_brand_profile() {
         if($_POST['delete_brand'] != ''){
            $brnd_id = $_POST['brand_ids'];
            if($_POST['delete_brand'] == 1){
            $success = $this->Request_model->deleteAllrequestbybrandId($brnd_id); 
            }
          if($_POST['delete_brand'] == 2 && $_POST['assign_brand'] != ''){
            $assign_brand = $_POST['assign_brand'];
            $req_by_brnd_id = $this->Request_model->getAllrequestbybrandId($brnd_id);
            if(sizeof($req_by_brnd_id) > 0){
              //  echo sizeof($req_by_brnd_id);
            foreach ($req_by_brnd_id as $all_reqestsid) {
              $req_id[] =  $all_reqestsid['id']; 
            }
            $success = $this->Request_model->update_req_brand_id(array("brand_id"=>$assign_brand), $req_id);
            }
            else{
                 $success = 1;
            }
          }
          if($_POST['delete_brand'] == 3){
           //   echo $_POST['delete_brand'];
            $req_by_brnd_id = $this->Request_model->getAllrequestbybrandId($brnd_id);
           // echo "<pre>";print_r($req_by_brnd_id);exit;
            if(sizeof($req_by_brnd_id) > 0){
              //  echo sizeof($req_by_brnd_id);exit;
            foreach ($req_by_brnd_id as $all_reqestsid) {
              $req_id[] =  $all_reqestsid['id']; 
            }
            $success = $this->Request_model->update_req_brand_id(array("brand_id"=>0), $req_id);
            }
            else{
                $success = 1;
            }
            }
           // echo $success;
            if($success){ 
               // echo "hello";
            $del_brndpermission_by_brnd_id = $this->Request_model->delete_user_brand_profile_by_brand_id($brnd_id);
           // echo "<pre>";print_r($del_brndpermission_by_brnd_id);
            $del_brndmaterials_by_brnd_id = $this->Request_model->delete_brandprofile_materials_files($brnd_id);
            $delete_brand_profile = $this->Request_model->delete_user('brand_profile',$brnd_id); 
            $this->session->set_flashdata('message_success', 'Brand Profile Deleted Successfully', 5);
            redirect(base_url() . "customer/brand_profiles");
            }
         }else{
             $this->session->set_flashdata('message_error', 'Brand Profile not Deleted Successfully', 5);
            redirect(base_url() . "customer/brand_profiles");
         }
     }
     

    public function add_brand_profile($ajaxsubmit = "") {
        
        //echo "<pre>";prinr_r($this->input->post());exit;
        $created_user = $this->load->get_var('main_user');
        if($ajaxsubmit){
            parse_str($this->input->post('formData'), $params);
            $_POST = $params;
            $_POST['brand_profile'] = $this->input->post('brand_profile');
        }
        $editid = $this->input->get('editid');
        //echo "<pre>";prinr_r($_POST);exit;
        $this->myfunctions->checkloginuser("customer");
        if(!empty($_POST)){
            $add_brand = $this->customfunctions->add_brand_profiles($_POST,$ajaxsubmit,$_FILES,$editid);
            if($ajaxsubmit){
                echo json_encode($add_brand);exit;
            }else{
                $this->session->set_flashdata('message_success', $add_brand["msg"], 5); 
                redirect(base_url() . "customer/brand_profiles");
            }
        }
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        /**check user permissions to add brand**/
        $is_add_brandprofile = $this->is_add_brand_profile($created_user);
        
        /**check user permissions to add brand**/
        $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($created_user);
        $this->load->view('customer/customer_header_1', array('profile' => $profile_data, 'user_profile_brands' => $user_profile_brands));
        $this->load->view('customer/add_brand_profile', array('profile' => $profile_data, 'exist_prodile_data' => $exist_prodile_data, 'exist_prodile_material_data' => $exist_prodile_material_data,"is_add_brandprofile" => $is_add_brandprofile));
        $this->load->view('customer/footer_customer');
    }

    public function add_profile_files_request($success) {
        if (!is_dir('public/uploads/brand_profile/' . $success)) {
            $data = mkdir('./public/uploads/brand_profile/' . $success, 0777, TRUE);
        }
//        echo "<pre>";print_r($this->input->post("upload_file_name"));
//        echo "<pre>";print_r($this->input->post("upload_type"));
        //Move files from temp folder
        // Get array of all source files
//        echo $_SESSION['temp_folder_logo_names'];exit;
        $uploadPATH = $_SESSION['temp_folder_logo_names'];
        $uploadPATHs = str_replace("./", "", $uploadPATH);

        //echo $uploadPATHs;exit;
        $files = scandir($uploadPATH);
        // Identify directories
        $source = $uploadPATH . '/';
        $s3Staus = "";
//         echo UPLOAD_FILE_SERVER;exit;
        $destination = './public/uploads/brand_profile/' . $success . '/';
        $logo_name = isset($_POST['upload_file_name']) ? $_POST['upload_file_name'] : '';
        if (UPLOAD_FILE_SERVER == 'bucket') {
                
            foreach ($files as $file) {

                if (in_array($file, array(".", "..")))
                    continue;

                $staticName = base_url() . $uploadPATHs . '/' . $file;

                $config = array(
                    'upload_path' => 'public/uploads/brand_profile/' . $success . '/',
                    'file_name' => $file
                );

                $this->s3_upload->initialize($config);
                $s3status = $this->s3_upload->upload_multiple_file($staticName);

                //echo "<pre>"; print_r($s3status); 
                if($s3status['status'] == 1){
                    $key = array_search(basename($staticName), $this->input->post("upload_file_name"));
                    $upload_type = $this->input->post("upload_type");
                    unlink('./tmp/tmpfile/' . basename($staticName));
                    $delete[] = $source . $file;
                    $request_files_data = array(
                        "brand_id" => $success,
                        "filename" => $s3status['file_update_name'],
                        "file_type" => $upload_type[$key],
                        "created" => date("Y-m-d H:i:s"));
                    $result = $this->Welcome_model->insert_data("brand_profile_material", $request_files_data);
                }
               
            }
             exit;
      //      if ($_POST['upload_file_name'] != '') {
      //     // if($test['status'] == 1){
      //      for ($i = 0; $i < sizeof($logo_name); $i++) {
      //          $request_files_data = array("brand_id" => $success,
      //              "filename" => $_POST['upload_file_name'][$i],
      //               "file_type" => $_POST['upload_type'][$i],
      //              "created" => date("Y-m-d H:i:s"));
      //          $result = $this->Welcome_model->insert_data("brand_profile_material", $request_files_data);
      //          // echo "<pre>";print_r($result);
      //      }
      //     // exit;   
      // // }
      //  }
        }else {
            foreach ($files as $file) {
                if (in_array($file, array(".", "..")))
                    continue;
                $filepath = APPPATH . $source . $file;
                if (copy($source . $file, $destination . $file)) {
                    $delete[] = $source . $file;
                }
            }
        }
        foreach ($delete as $file) {
            unlink($file);
        }
        rmdir($source);
        unset($_SESSION['temp_folder_logo_names']);
//        if ($_POST['upload_file_name'] != '') {
//            for ($i = 0; $i < sizeof($logo_name); $i++) {
//                $request_files_data = array("brand_id" => $success,
//                    "filename" => $_POST['upload_file_name'][$i],
//                     "file_type" => $_POST['upload_type'][$i],
//                    "created" => date("Y-m-d H:i:s"));
//                $result = $this->Welcome_model->insert_data("brand_profile_material", $request_files_data);
//            }
//        }
//        echo "<pre/>";print_r($_POST['upload_file_name']);exit;
        
        return $s3status['status'];
    }
        public function upload_logo_process() {
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        $dateFolder = strtotime(date('Y-m-d H:i:s'));
        $folder_name = './public/uploads/temp/brand_profile/' . $dateFolder;
        //upload logo process
        if (isset($_FILES['logo_upload'])) {
            $filename = 'logo_upload';
        }
        if (isset($_FILES['materials_upload'])) {
            $filename = 'materials_upload';
        }
        if (isset($_FILES['additional_upload'])) {
            $filename = 'additional_upload';
        }
        if (!(isset($_SESSION['temp_folder_logo_names']) && $_SESSION['temp_folder_logo_names'] != '')) {
            if (@mkdir($folder_name, 0777, TRUE)) {
                $_SESSION['temp_folder_logo_names'] = $folder_name;
            } else {
                $_SESSION['temp_folder_logo_names'] = '';
            }
        } else {
            if (!is_dir($_SESSION['temp_folder_logo_names'])) {
                if (@mkdir($folder_name, 0777, TRUE)) {
                    $_SESSION['temp_folder_logo_names'] = $folder_name;
                } else {
                    $_SESSION['temp_folder_logo_names'] = '';
                }
            }
        }
        // echo $_SESSION['temp_folder_logo_names']; die; 
        if ($_SESSION['temp_folder_logo_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES[$filename]['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES[$filename]['name'] = $files[$filename]['name'][$i];
                $_FILES[$filename]['type'] = $files[$filename]['type'][$i];
                $_FILES[$filename]['tmp_name'] = $files[$filename]['tmp_name'][$i];
                $_FILES[$filename]['error'] = $files[$filename]['error'][$i];
                $_FILES[$filename]['size'] = $files[$filename]['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['temp_folder_logo_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload($filename)) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output[$filename][] = $data;
                    $output['status'] = true;
                    $output['type'] = $filename;
                    $output['error'] = '';
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $error_data = array();
                    $error_data[0] = array();
                    $error_data[0]['file_name'] = $files[$filename]['name'][$i];
                    $error_data[0]['error'] = true;
                    $error_data[0]['error_msg'] = strip_tags($data['error']);
                    $output[$filename][] = $error_data;
                    $output['status'] = true;
                    $output['type'] = $filename;
                    $output['error'] = '';
                }
            }
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
//        echo "<pre/>";print_R($output);exit;
        echo json_encode($output);
        exit;
    }
    
//    public function checkloginuser() {
//        if (!$this->session->userdata('user_id')) {
//            $protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
//            $base_url = $protocol . "://" . $_SERVER['HTTP_HOST'];
//            $complete_url = $_SERVER["REQUEST_URI"];
//            $requrl = ltrim($complete_url, '/');
//             redirect($url.'?url=' . $requrl);
//        }
//        if ($this->session->userdata('role') != "customer") {
//            redirect(base_url());
//        }
//    }
    
    public function del_brandprofile_files() {
        if (isset($_POST)) {
            
          $baseurl =  base_url();
          $filepath = $_POST['flpath'];
          $delfilename = $_POST['delfile'];
          $delfileid = $_POST['fileid'];
          $delfiledname = $_POST['fieldname'];
          $path = FCPATH.$filepath;
         // echo $baseurl.$filepath;
          $delete = $this->Admin_model->delete_data('brand_profile_material', $delfileid);
          if($delete){
               if (UPLOAD_FILE_SERVER == 'bucket') {
             $delfile = $this->s3_upload->delete_file_from_s3($baseurl.$filepath);
               }
               else{
                   
              $delfile = unlink($path);
               }
               if($delfile == 1){
                   echo 1;
               }
          }
        }
    
    }

    public function delete_brand_file_from_folder() {
        $folderPath = $_SESSION['temp_folder_logo_names'];
        $folderPaths = $_SESSION['temp_folder_logo_names'] . '/*';
        if (!empty($_POST)) {
            $data = $_POST['file_name'];
            $files = glob($folderPaths); // get all file names
            $dir = FCPATH . $folderPath;
            $dirHandle = opendir($dir);
            while ($file = readdir($dirHandle)) {
                if ($file == $data) {
                    unlink($dir . "/" . $file); //give correct path,
                } else {
                    
                }
            }
        }
    }
    
    public function is_add_brand_profile($created_user=""){
        $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($created_user,"","1");
        $users_data = $this->load->get_var('parent_user_data');
        $login_user_data = $this->load->get_var('login_user_data');
        $user_plan_data = $this->load->get_var('parent_user_plan');
        if ($users_data[0]['overwrite'] == 1) {
            if ($users_data[0]['brand_profile_count'] == -1 || ($users_data[0]['brand_profile_count'] != 0 && $user_profile_brands < $users_data[0]['brand_profile_count'])) {
                $return = 1;
            }else{
                $return = 0;
            }
        }else{
            if ($user_plan_data[0]['brand_profile_count'] == -1 || ($user_plan_data[0]['brand_profile_count'] != 0 && $user_profile_brands < $user_plan_data[0]['brand_profile_count'])) {
                $return = 1;
            }else{
                $return = 0;
            }
        }
        if($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client' && $return == 1){
            $loginuser_pln = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name'],"",$users_data[0]['id']);
          //  echo "<pre>";print_r($loginuser_pln);
            $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($login_user_data[0]['id'], "created_by","1");
          //  echo $user_profile_brands.$loginuser_pln[0]['brand_profile_count'];
            if ($login_user_data[0]['overwrite'] == 1) {
                if ($login_user_data[0]['brand_profile_count'] == -1 || ($login_user_data[0]['brand_profile_count'] != 0 && $user_profile_brands < $login_user_data[0]['brand_profile_count'])) {
                    $return = 1;
                } else {
                    $return = 0;
                }
            } else {
                if ($loginuser_pln[0]['brand_profile_count'] == -1 || ($loginuser_pln[0]['brand_profile_count'] != 0 && $user_profile_brands < $loginuser_pln[0]['brand_profile_count'])) {
                    $return = 1;
                } else {
                    $return = 0;
                }
            }
        }
        
        return $return;
        
    }

    public function get_extension($file) {
        $extension = end(explode(".", $file));
        return $extension ? $extension : false;
    }
    
    public function get_brandprofiles() {
        $created_user = $this->load->get_var('main_user');
        $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($created_user);
        echo json_encode($user_profile_brands);exit;
    }

}
