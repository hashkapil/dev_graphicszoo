<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MultipleUser extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $this->load->library('zip');

        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    } 

    public function sub_user_list(){
        
       $login_user_id = $this->load->get_var('login_user_id');
       $profile_data = $this->Admin_model->getuser_data($login_user_id);
       $sub_user_data = $this->Request_model->getuserbyparentid($login_user_id);
       
      // echo "<pre>";print_R($sub_user_data);
       $this->load->view('customer/customer_header_1',array('profile'=>$profile_data));
       $this->load->view('customer/sub_user_list',array('profile'=> $profile_data,'sub_user_data'=> $sub_user_data)); 
       $this->load->view('customer/footer_customer');
    }

    public function sub_user(){
        $output = array();
        $cust_url = $this->input->post('cust_url');
        $this->myfunctions->checkloginuser("customer");
        $main_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $edit_user_id = $this->input->post('cust_id');

        if(!empty($this->input->post())){
            $add_memeber = $this->customfunctions->addteammembers($this->input->post());
            //echo "<pre>";print_r($add_memeber);
            if($add_memeber["status"] == 1){
                $this->session->set_flashdata('message_success', $add_memeber["msg"], 5);
                redirect(base_url() ."customer/team-management");
            }else{
                $this->session->set_flashdata('message_error', $add_memeber["msg"], 5);
                redirect(base_url() ."customer/team-management");
            }
        }
        $sub_user_data = $this->Admin_model->getuser_data($edit_user_id);
        $sub_user_permissions = $this->Request_model->get_sub_user_permissions($edit_user_id);
        $brandprofile = $this->Request_model->get_brand_profile_by_user_id($main_user);
//        echo "<pre/>";print_r($brandprofile);exit;
        $profile_data = $this->Admin_model->getuser_data($login_user_id);
        $this->load->view('customer/customer_header_1',array('profile'=>$profile_data,'canaddbrandprofile' => $canaddbrandprofile));
        $this->load->view('customer/footer_customer');
        $this->load->view('customer/add_new_user',array('brandprofile' => $brandprofile,'sub_user_data' => $sub_user_data,'sub_user_permissions' =>$sub_user_permissions,'selectedbrandIDs' => $BIDs));
    }

    public function delete_sub_user($url,$id) {
        //echo $url.$id;exit;
        $sub_user_permissions = $this->Request_model->delete_user_permission($id);
        if ($sub_user_permissions) {
            $sub_user_data = $this->Request_model->delete_user("users", $id);
        }
        $this->session->set_flashdata('message_success', 'User deleted Successfully', 5);
        if($url == "setting_view"){
            redirect(base_url() . "customer/setting-view#management");
        }else{
            redirect(base_url() . "customer/client_management");
        }
//        header("Refresh:0");
    }
    
    public function add_edit_manager(){
     //   die("hello"); 
        if(!empty($this->input->post())){
            $add_memeber = $this->customfunctions->addteammembers($this->input->post());
            if($add_memeber["status"] == 1){
                $this->session->set_flashdata('message_success', $add_memeber["msg"], 5);
                redirect(base_url() ."customer/team-management");
            }else{
                $this->session->set_flashdata('message_error', $add_memeber["msg"], 5);
                redirect(base_url() ."customer/team-management");
            }
        }
    }
    
//    public function checkloginuser() {
//        if (!$this->session->userdata('user_id')) {
//            $protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
//            $base_url = $protocol . "://" . $_SERVER['HTTP_HOST'];
//            $complete_url = $_SERVER["REQUEST_URI"];
//            $requrl = ltrim($complete_url, '/');
//            if($_SERVER['HTTP_HOST'] == DOMAIN_NAME){ 
//                $url = base_url() . 'login';
//            }else{
//                $url = base_url();
//            }
//            redirect($url.'?url=' . $requrl);
//        }
//        if ($this->session->userdata('role') != "customer") {
//            redirect(base_url());
//        }
//    }
    

}
