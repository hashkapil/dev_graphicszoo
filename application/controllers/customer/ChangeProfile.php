<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ChangeProfile extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->model('Category_model');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        
        
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }


    public function index() {
        $this->myfunctions->checkloginuser("customer");
        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);

        if (!empty($_POST)) {
            unset($_POST['savebtn']);
            if (!empty($_FILES)) {
                if ($_FILES['profile']['name'] != "") {
                    if (!empty($_FILES)) {
                        $config2 = array(
                            'upload_path' => './uploads/profile_picture',
                            'allowed_types' => "*",
                            'encrypt_name' => TRUE
                        );
                        $config2['profile'] = $_FILES['profile']['name'];

                        $this->load->library('upload', $config2);

                        if ($this->upload->do_upload('profile')) {
                            $data = array($this->upload->data());
                            $_POST['profile_picture'] = $data[0]['file_name'];
                        } else {
                            $data = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message_error', $data, 5);
                        }
                    }
                }
            }
            $update_data_result = $this->Admin_model->update_data('users', $_POST, array("id" => $user_id));
            if ($update_data_result) {
                $this->session->set_flashdata('message_success', 'User Profile Updated Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Updated.!', 5);
            }
            redirect(base_url() . "customer/ChangeProfile/");
        }
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));

        $this->load->view('customer/setting-edit', array("data" => $data));
        $this->load->view('customer/footer_customer');
    }

    public function change_password_old() {
        $this->myfunctions->checkloginuser("customer");
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

            if ($user_data[0]['new_password'] != $_POST['old_password']) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "customer/ChangeProfile");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "customer/ChangeProfile");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "customer/ChangeProfile");
        }

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $this->load->view('customer/change_password');
        $this->load->view('customer/footer_customer');
    }

    public function billing_subscription() {
        $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        // echo "<pre>";print_r($user_data);exit;
        $planlist = $this->Stripe->getallsubscriptionlist();
        $current_plan = $this->Stripe->getcurrentplan($user_data[0]['current_plan']);
        $customerdetails = $this->Stripe->getcustomerdetail($user_data[0]['current_plan']);
        if ($current_plan != "") {
            $current_plan = $current_plan->items->data[0]->plan->id;
        }

        if (!empty($_POST)) {

            if ($_POST['plan_name'] == "") {
                $this->session->set_flashdata("message_error", "Please Choose Another Plan..!");
                redirect(base_url() . "customer/ChangeProfile/billing_subscription");
            }
            if ($_POST['plan_name'] == $current_plan) {
                $this->session->set_flashdata("message_error", "Please Choose Another Plan..!");
                redirect(base_url() . "customer/ChangeProfile/billing_subscription");
            }
            $data = $this->Stripe->updateuserplan($user_data[0]['current_plan'], $_POST['plan_name']);
            if ($data[0] == "error") {
                $this->session->set_flashdata("message_error", $data[1]);
                redirect(base_url() . "customer/ChangeProfile/billing_subscription");
            }
            if ($data[0] == "success") {
                $this->session->set_flashdata("message_success", "Plan Updated Successfully..!");
                redirect(base_url() . "customer/ChangeProfile/billing_subscription");
            }
        }
        $invoices = $this->Stripe->get_customer_invoices($user_data[0]['customer_id']);
        if (isset($invoices['data'])) {
            $invoices = $invoices['data'];
        } else {
            $invoices = array();
        }
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('customer/billing_subscription', array("planlist" => $planlist, "current_plan" => $current_plan, "invoices" => $invoices));
        $this->load->view('customer/customer_footer_1');
    }

    public function change_plan() {
        $main_user = $this->load->get_var('main_user');
        $user_data = $this->Admin_model->getuser_data($main_user);
        
        $expiry_month_date = isset($_POST['expiry-month-date'])?$_POST['expiry-month-date']:"";
        $expiry_date =  explode("/", $expiry_month_date);
        $_POST['expiry-month'] = $expiry_date[0];
        $_POST['expiry-year'] = $expiry_date[1];
        
        $customerdetails = $this->Stripe->getcustomerdetail($user_data[0]['current_plan'],$user_data[0]['customer_id']);
        $updatecard = $this->Stripe->change_user_card($customerdetails->id, $_POST);
        
        if ($updatecard[0] == "success") {
            $this->session->set_flashdata("message_success", $updatecard[1]);
        } else {
            $this->session->set_flashdata("message_error", $updatecard[1]);
        }
        redirect(base_url() . "customer/setting-view#billing");
    }

    public function change_current_plan() {

        $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $customer_id = $_SESSION['user_id'];
        $current_plan_price = isset($_POST['plan_price']) ? $_POST['plan_price'] : '';
        $display_name = isset($_POST['display_name']) ? $_POST['display_name'] : '';
        $current_plan_name = $user_data[0]['plan_name'];
        $next_plan_name = $user_data[0]['next_plan_name'];
        $subscription_plan_id = $_POST['plan_name'];
        $inprogress_request = isset($_POST['in_progress_request']) ? $_POST['in_progress_request'] : '';
        $userstate = $user_data[0]['state'];
        if (array_key_exists($userstate, STATE_TEXAS)) {
            $state_tax = STATE_TEXAS[$userstate];
        } else {
            $state_tax = 0;
        }
        //if user upgrade 49 plan
        if (in_array($subscription_plan_id, NEW_PLANS)) {
            $updateuserplan = $this->Stripe->create_invoice($user_data[0]['customer_id'], $state_tax, $current_plan_price);
            if ($updateuserplan['status'] != true) {
                $updateuserplan['message'] = $updateuserplan['message'];
                $updateuserplan[0] = 'error';
            } else {
                $updateuserplan[0] = 'success';
                $customer['plan_name'] = $subscription_plan_id;
                $customer['display_plan_name'] = $display_name;
                $customer['total_inprogress_req'] = 0;
                $customer['total_active_req'] = 0;
                $customer['total_requests'] = $user_data[0]['total_requests'] + $inprogress_request;
                $customer['plan_amount'] = $current_plan_price;
                $customer['invoice'] = $updateuserplan['id'];
                $customer['billing_start_date'] = date('Y-m-d H:i:s');
            }
        } else {
            if ($user_data[0]['invoice'] == '' || $user_data[0]['invoice'] == NULL || $user_data[0]['invoice'] == 1) {
                $current_plan_weight = $this->Request_model->getsubscriptionlistbyplanid($current_plan_name);
            } elseif ($user_data[0]['invoice'] == 0) {
                $current_plan_weight = $this->Request_model->getsubscriptionlistbyplanid($next_plan_name);
            }
            $post_plan_weight = $this->Request_model->getsubscriptionlistbyplanid($subscription_plan_id);
            if ($current_plan_weight[0]['plan_weight'] <= $post_plan_weight[0]['plan_weight']) {
                $invoice = 1;
            } else {
                $invoice = 0;
            }



            $updateuserplan = $this->Stripe->updateuserplan($user_data[0]['customer_id'], $_POST['plan_name'], $inprogress_request, $invoice);

            $subscription = $updateuserplan['data']['subscriptions'];
            $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
            $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
            //if invoice created
            if ($invoice == 1 || ($invoice != 1 && $user_data[0]['is_cancel_subscription'] == 1)) {
                if ($subscription_plan_id == SUBSCRIPTION_99_PLAN) {
                    $current_date = date("Y:m:d H:i:s");
                    $monthly_date = strtotime("+1 months", strtotime($current_date)); // returns timestamp
                    $billing_expire = date('Y:m:d H:i:s', $monthly_date);
                    $customer['billing_cycle_request'] = 3;
                } else {
                    $customer['billing_cycle_request'] = 0;
                }
                if ($invoice == 1) {
                    $this->Stripe->create_invoice($user_data[0]['customer_id'], $state_tax);
                }
                $customer['plan_name'] = $subscription_plan_id;
                $customer['current_plan'] = $subscription['data'][0]->id;
                $customer['billing_start_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_start);
                $customer['billing_end_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_end);
                $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                $customer['display_plan_name'] = $plandetails['name'];
                $customer['total_inprogress_req'] = $inprogress_request;
                $customer['total_active_req'] = $inprogress_request * TOTAL_ACTIVE_REQUEST;
                $customer['plan_amount'] = $current_plan_price;
                $customer['invoice'] = 1;
                $customer['plan_interval'] = $plandetails['interval'];
                if ($user_data[0]['is_cancel_subscription'] == 1) {
                    $customer['is_cancel_subscription'] = 0;
                    $flag = isset($_POST['reactivate_pro']) ? $_POST['reactivate_pro'] : '';
                    if ($flag == 'yes') {
                        $this->myfunctions->movecancelprojecttopreviousstatus($userid);
                    }
                }
                $this->myfunctions->moveinqueuereq_toactive_after_updrage_plan($customer_id,$inprogress_request);
                
            } else if ($invoice == 0) {
                //if invoice not created
                $customer['next_plan_name'] = $subscription_plan_id;
                $customer['invoice'] = 0;
                $customer['next_current_plan'] = $subscription['data'][0]->id;
                
            }
        }
        $customer['modified'] = date("Y:m:d H:i:s");
        if ($updateuserplan[0] != 'success') {
            $this->session->set_flashdata('message_error', $updateuserplan['1'], 5);
            redirect(base_url() . "customer/setting-view#billing");
        } else {
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $customer_id));
            if ($id) {

                $this->session->set_flashdata('message_success', $updateuserplan['1'], 5);

                redirect(base_url() . "customer/setting-view#billing");
            }
        }
    }

    public function setting_edit() {
        $this->myfunctions->checkloginuser("customer");
        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);

        if (!empty($_POST)) {
            unset($_POST['savebtn']);
            if (!empty($_FILES)) {
                if ($_FILES['profile']['name'] != "") {
                    if (!empty($_FILES)) {
                        $config2 = array(
                            'upload_path' => './uploads/profile_picture',
                            'allowed_types' => "*",
                            'encrypt_name' => TRUE
                        );
                        $config2['profile'] = $_FILES['profile']['name'];

                        $this->load->library('upload', $config2);

                        if ($this->upload->do_upload('profile')) {
                            $data = array($this->upload->data());
                            $_POST['profile_picture'] = $data[0]['file_name'];
                        } else {
                            $data = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message_error', $data, 5);
                        }
                    }
                }
            }
            $update_data_result = $this->Admin_model->update_data('users', $_POST, array("id" => $user_id));
            if ($update_data_result) {
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
            redirect(base_url() . "customer/setting-view/");
        }

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));

        $this->load->view('customer/setting-edit', array("data" => $data));
        $this->load->view('customer/footer_customer');
   
 }

    public function change_password() {
        $this->myfunctions->checkloginuser("customer");
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

            if ($user_data[0]['new_password'] != $_POST['old_password']) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "customer/setting-edit");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "customer/setting-edit");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "customer/setting-edit");
        }

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
        $this->load->view('customer/change_password');
        $this->load->view('customer/footer_customer');
    }

    public function change_password_front() {
        $this->myfunctions->checkloginuser("customer");
        if (!empty($_POST)) {
            $output = array();
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
            if ($user_data[0]['new_password'] != md5($_POST['old_password'])) {
                $output["msg"] = "Old Password is not correct..!";
                $output["status"] = 0;
                echo json_encode($output);exit;
//                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
//                redirect(base_url() . "customer/setting-view#password");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $output["msg"] = "New And Confirm Password is not match..!";
                $output["status"] = 0;
                echo json_encode($output);exit;
//                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
//                redirect(base_url() . "customer/setting-view#password");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
               // $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
                $output["msg"] = "Password is changed successfully..!";
                $output["status"] = 1;
                echo json_encode($output);exit;
            } else {
               // $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
                $output["msg"] = "Password is not changed successfully..!";
                $output["status"] = 0;
                echo json_encode($output);exit;
            }
//            redirect(base_url() . "customer/setting-view#password");
        }

//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
//        $this->load->view('customer/setting-view#password');
//        $this->load->view('customer/footer_customer');
    }

//    public function setting_view() {
//        $main_user = $this->load->get_var('main_user');
//        $canseebilling = $this->myfunctions->isUserPermission('billing_module');
//        $this->myfunctions->checkloginuser("customer");
//        $user_id = $this->load->get_var('login_user_id'); 
//        $data = $this->Admin_model->getuser_data($main_user);
//        $plan_data = $this->Request_model->getsubscriptionlistbyplanid($data[0]['plan_name']);
//        if($plan_data[0]['new_plan_id'] != ''){
//          $new_plan_data = $this->Request_model->getsubscriptionlistbyplanid($plan_data[0]['new_plan_id']);  
//        }
//        $url = "customer/setting-view?status=client#management";
//        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url,"user_id"=> $user_id));
//        $total_inprogress_req = $data[0]['total_inprogress_req'];
//        $notifications = $this->Request_model->get_notifications($user_id);
//        $notification_number = $this->Request_model->get_notifications_number($user_id);
//        $messagenotifications = $this->Request_model->get_messagenotifications($user_id);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($user_id);
//        $getalltimezone = $this->Request_model->getAlltimezone();
//        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $profile_data[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($profile_data[0]['profile_picture']);
//        $currentuser = $this->load->get_var('main_user');
//        $subusersdata = $this->Request_model->getAllsubUsers($currentuser,"manager");
//        foreach ($subusersdata as $userskey => $usersvalue) {
//           $subusersdata[$userskey]['profile_picture'] = $this->customfunctions->getprofileimageurl($usersvalue['profile_picture']); 
//        } 
//        $timezone = $this->Admin_model->get_timezone();
//        $brandprofile = $this->Request_model->get_brand_profile_by_user_id($main_user);
//        if ($data[0]['customer_id'] != "" || $data[0]['customer_id'] != null) {
//            $card_details = $this->get_customer_detail_from_strip($data[0]['current_plan'],$data[0]['customer_id']);
//            $plan_details = $this->getdetailplan($data[0]['current_plan']);
//        } else {
//            $card_details = [];
//            $plan_details = [];
//        }
//        $currentplan_details = $this->Request_model->getsubscriptionlistbyplanid($data[0]['plan_name']);
//        if($data[0]['next_plan_name'] != ''){
//        $nextplan_details = $this->Request_model->getsubscriptionlistbyplanid($data[0]['next_plan_name']);
//        }else{
//            $nextplan_details = [];
//        }
//        if(in_array($data[0]['plan_name'],NEW_PLANS)){
//        $all_request = $this->Request_model->getall_request_for_all_status($main_user);
//        $count_active_project = $this->Request_model->get_active_requestsbyuserid('active',$main_user);
//        $request['added_request'] = sizeof($all_request);
//        $request['active_project'] = sizeof($count_active_project);
//        $pending_req = $data[0]['total_requests'] - $request['added_request'];
//        $request['pending_req'] = ($pending_req > 0)?$pending_req:0;
//        }else{
//            $request = [];
//        }
//        $subusers = $this->Admin_model->sumallsubuser_requestscount($main_user);
//        $main_active_req = $data[0]['total_active_req'];
//        $main_inprogress_req = $data[0]['total_inprogress_req'];
//        $subtotal_active_req = $subusers['total_active_req'];
//        $subtotal_inprogress_req = $subusers['total_inprogress_req'];
//        $checksubuser_requests = array('main_active_req' => $main_active_req,
//                                'main_inprogress_req' => $main_inprogress_req,
//                                'subtotal_active_req' => isset($subtotal_active_req)?$subtotal_active_req:0,
//                                'subtotal_inprogress_req' => isset($subtotal_inprogress_req)?$subtotal_inprogress_req:0);
//        $isshow_subuser = $this->is_show_subuser($currentuser);
//        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
//        $this->load->view('customer/setting-view-new', array("data" => $data,"profile" => $profile_data,"plan_data"=> $plan_data,"new_plan_data" => $new_plan_data, "nextplan_details" => $nextplan_details, 'card' => $card_details, 'plan' => $plan_details, "timezone" => $timezone, 'getalltimezone' => $getalltimezone, 'subusersdata' => $subusersdata,'brandprofile' => $brandprofile, 'canseebilling' => $canseebilling,'currentplan_details'=>$currentplan_details,'request'=>$request,'checksubuser_requests' => $checksubuser_requests,'isshow_subuser'=>$isshow_subuser));
//        $this->load->view('customer/footer_customer',array('total_inprogress_req' => $total_inprogress_req));
//    }

    public function setting_view() {
        $main_user = $this->load->get_var('main_user');
        $canseebilling = $this->myfunctions->isUserPermission('billing_module');
        $this->myfunctions->checkloginuser("customer");
        $user_id = $this->load->get_var('login_user_id'); 
        $login_user_data = $this->load->get_var('login_user_data');
        $parent_user_data = $this->load->get_var('parent_user_data');


        
        /** get billing details of user **/
        $billinginfo = $this->getbillinginfobaseduser($login_user_data,$parent_user_data);
        
        $url = "customer/setting-view?status=client#management";
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url,"user_id"=> $user_id));
        $getalltimezone = $this->Request_model->getAlltimezone();
        $isalreadyrequested = $this->Account_model->isAlreadyCancelled($user_id);
        $checkcouponapplied =  $this->Account_model->isAlreadyCancelled($user_id,true);
        /**get requests to assign dedicated designer**/
        $allreq = $this->Request_model->getall_newrequest($main_user,array('active','checkforapprove','disapprove','approved'),'','','id');

        $profile_data = $this->Admin_model->getuser_data($user_id);
        $profile_data[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($profile_data[0]['profile_picture']);
        $currentuser = $this->load->get_var('main_user');
        $subusersdata = $this->Request_model->getAllsubUsers($currentuser,"manager");
        foreach ($subusersdata as $userskey => $usersvalue) {
           $subusersdata[$userskey]['profile_picture'] = $this->customfunctions->getprofileimageurl($usersvalue['profile_picture']); 
        } 
        $timezone = $this->Admin_model->get_timezone();
        $brandprofile = $this->Request_model->get_brand_profile_by_user_id($main_user);
        $subusers = $this->Admin_model->sumallsubuser_requestscount($main_user);
        $main_active_req = $parent_user_data[0]['total_active_req'];
        $main_inprogress_req = $parent_user_data[0]['total_inprogress_req'];
        $subtotal_active_req = $subusers['total_active_req'];
        $subtotal_inprogress_req = $subusers['total_inprogress_req'];
        $checksubuser_requests = array('main_active_req' => $main_active_req,
                                'main_inprogress_req' => $main_inprogress_req,
                                'subtotal_active_req' => isset($subtotal_active_req)?$subtotal_active_req:0,
                                'subtotal_inprogress_req' => isset($subtotal_inprogress_req)?$subtotal_inprogress_req:0);
        
        $isshow_subuser = $this->customfunctions->is_show_subuser($currentuser);
        $design_teams = $this->Request_model->forGettingUsersInfo();
        $design_teams[0]['d_profile_picture'] = $this->customfunctions->getprofileimageurl($design_teams[0]['d_profile_picture']); 
        $design_teams[0]['prt_profile_picture'] = $this->customfunctions->getprofileimageurl($design_teams[0]['prt_profile_picture']); 
        $design_teams[0]['am_profile_picture'] = $this->customfunctions->getprofileimageurl($design_teams[0]['am_profile_picture']); 
        $design_teams[0]['qa_profile_picture'] = $this->customfunctions->getprofileimageurl($design_teams[0]['qa_profile_picture']); 
        $change_designer_request = $this->Request_model->change_designer_request($user_id);
        $designer_skilldata = $this->Welcome_model->select_data("*","user_skills","user_id='".$login_user_data[0]['designer_id']."'");

        // echo "<pre>"; print_r($GettingUsersInfo); die; 

        $this->load->view('customer/customer_header_1');
        $this->load->view('customer/setting-view-new', array("data" => $billinginfo['userdata'],"profile" => $profile_data,"plan_data"=> $billinginfo['plan_data'],"nextplan_details" => $billinginfo['nextplan_details'], 'card' => $billinginfo['card_details'], 
            "timezone" => $timezone, 'getalltimezone' => $getalltimezone, 'subusersdata' => $subusersdata,
             'brandprofile' => $brandprofile,'canseebilling' => $canseebilling,'currentplan_details'=>$billinginfo['currentplan_details'],'request'=>$billinginfo['request'],
            'checksubuser_requests' => $checksubuser_requests,'allreq' => $allreq,'isshow_subuser'=>$isshow_subuser,'isalreadyrequested'=> $isalreadyrequested,'checkcouponapplied' => $checkcouponapplied,"design_teams"=>$design_teams,"change_designer_request"=>$change_designer_request,"designer_skilldata"=>$designer_skilldata));
        $this->load->view('customer/footer_customer',array('total_inprogress_req' => $billinginfo['total_inprogress_req']));
    }
    public function team_management()
    {
        $main_user = $this->load->get_var('main_user');
        $canseebilling = $this->myfunctions->isUserPermission('billing_module');
        $this->myfunctions->checkloginuser("customer");
        $user_id = $this->load->get_var('login_user_id'); 
        $login_user_data = $this->load->get_var('login_user_data');
        $parent_user_data = $this->load->get_var('parent_user_data');
        
        /** get billing details of user **/
        $billinginfo = $this->getbillinginfobaseduser($login_user_data,$parent_user_data);
        
        $url = "customer/setting-view?status=client#management";
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url,"user_id"=> $user_id));
        $getalltimezone = $this->Request_model->getAlltimezone();
        $isalreadyrequested = $this->Account_model->isAlreadyCancelled($user_id);
        $checkcouponapplied =  $this->Account_model->isAlreadyCancelled($user_id,true);
        /**get requests to assign dedicated designer**/
        $allreq = $this->Request_model->getall_newrequest($main_user,array('active','checkforapprove','disapprove','approved'),'','','id');

        $profile_data = $this->Admin_model->getuser_data($user_id);
        $profile_data[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($profile_data[0]['profile_picture']);
        $currentuser = $this->load->get_var('main_user');
        $subusersdata = $this->Request_model->getAllsubUsers($currentuser,"manager");
        foreach ($subusersdata as $userskey => $usersvalue) {
           $subusersdata[$userskey]['profile_picture'] = $this->customfunctions->getprofileimageurl($usersvalue['profile_picture']); 
        } 
        $timezone = $this->Admin_model->get_timezone();
        $brandprofile = $this->Request_model->get_brand_profile_by_user_id($main_user);
        
        $subusers = $this->Admin_model->sumallsubuser_requestscount($main_user);
        $main_active_req = $parent_user_data[0]['total_active_req'];
        $main_inprogress_req = $parent_user_data[0]['total_inprogress_req'];
        $subtotal_active_req = $subusers['total_active_req'];
        $subtotal_inprogress_req = $subusers['total_inprogress_req'];
        $checksubuser_requests = array('main_active_req' => $main_active_req,
                                'main_inprogress_req' => $main_inprogress_req,
                                'subtotal_active_req' => isset($subtotal_active_req)?$subtotal_active_req:0,
                                'subtotal_inprogress_req' => isset($subtotal_inprogress_req)?$subtotal_inprogress_req:0);
        
        $isshow_subuser = $this->customfunctions->is_show_subuser($currentuser);
        $this->load->view('customer/customer_header_1');
        $this->load->view('customer/team_management', array("data" => $billinginfo['userdata'],"profile" => $profile_data,"plan_data"=> $billinginfo['plan_data'],"nextplan_details" => $billinginfo['nextplan_details'], 'card' => $billinginfo['card_details'], 
            "timezone" => $timezone, 'getalltimezone' => $getalltimezone, 'subusersdata' => $subusersdata,
             'brandprofile' => $brandprofile,'canseebilling' => $canseebilling,'currentplan_details'=>$billinginfo['currentplan_details'],'request'=>$billinginfo['request'],
            'checksubuser_requests' => $checksubuser_requests,'allreq' => $allreq,'isshow_subuser'=>$isshow_subuser,'isalreadyrequested'=> $isalreadyrequested,'checkcouponapplied' => $checkcouponapplied));
        $this->load->view('customer/footer_customer',array('total_inprogress_req' => $billinginfo['total_inprogress_req']));
         
        //$this->load->view('customer/customer_footer'); team_management
    }
    
    public function edit_profile_image_form() {
        $output = array();
        $user_id = $this->load->get_var('login_user_id'); 
        $fname = explode(' ', $_POST['first_name']);
        $login_user_id = $this->load->get_var('main_user');
        if($this->input->post('request_layout') == 'on'){
            $request_layout = 1;
        }else{
            $request_layout = 0;
        }
        $update_data_result = $this->customfunctions->uploadprofilewiththumb($_FILES);
          if($update_data_result) {
            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
          }else{
            $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
          }
        if (!empty($this->input->post())) {
            $user_data = $this->Request_model->getuserbyid($user_id);
            $customer_id = isset($user_data[0]['customer_id'])? $user_data[0]['customer_id']: '';
            $checkemailexits = $this->Request_model->getuserbyemail($_POST['notification_email'], $user_id);
            if (!empty($checkemailexits)) {
                $this->session->set_flashdata('message_error', "Email address is already used!", 5);
            } else {
                $profile_pic = array(
                    'first_name' => $fname[0],
                    'last_name' => $fname[1],
                    'company_name' => $_POST['company'],
                    'email' => $_POST['notification_email'],
                    'title' => $_POST['title'],
                    'phone' => $_POST['phone'],
                    'url' => $_POST['url'],
                );
                $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                $this->Welcome_model->update_data("users", array('request_wizard' => $request_layout),array('id' => $login_user_id));
                $stripe_update_data = $this->Stripe->updateCustomer($customer_id,$_POST['notification_email']);
                if ($update_data_result) {
                    $output["msg"] = "Account info updated successfully!";
                    $output["status"] = 1;
                    echo json_encode($output);exit;
                    //$this->session->set_flashdata('message_success', 'Account info updated successfully!', 5);
                } else {
                    $output["msg"] = "Error occurred while updating account info!";
                    $output["status"] = 0;
                    echo json_encode($output);exit;
                   // $this->session->set_flashdata('message_error', 'Error occurred while updating account info!', 5);
                }
            }
        }
    }

    public function customer_edit_profile() {
        $output = array();
        $user_id = $_SESSION['user_id'];
        $profileUpdate = array(
            'address_line_1' => $_POST['adress'],
            'title' => $_POST['title'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'zip' => $_POST['zip'],
            'timezone' => $_POST['timezone'],
        );
        $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
        if ($update_data_result) {
            $this->session->set_userdata('timezone', $profileUpdate['timezone']);
            $output["msg"] = "User Profile Update Successfully.!";
            $output["status"] = 1;
            echo json_encode($output);exit;
//            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
        } else {
            $output["msg"] = "User Profile Not Update.!";
            $output["status"] = 0;
            echo json_encode($output);exit;
           // $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
        }
    }

    public function get_customer_detail_from_strip($plan_id,$customer_id="") {
        $card_detail = $this->Stripe->getcustomerdetail($plan_id,$customer_id);
      //  echo "<pre>";print_r($card_detail);exit; 
        $card = $card_detail->sources['data'];
       // echo "<pre>";print_r($card);exit; 
        return $card;
    }

    public function getdetailplan($plan_id) {
        $card_detail = $this->Stripe->getcurrentplan($plan_id);
        return $card_detail->items->data[0]->plan;
    }

    public function change_status_usersettings() {
        $cust_status = $_POST['status'];
        $cust_id = $_POST['data_id'];
        $datekey = $_POST['data_key'];
        if ($_POST['status'] == 'true') {
            $this->Request_model->update_customer_active_status("users", array($datekey => 1), array("id" => $cust_id));
        } elseif ($_POST['status'] == 'false') {
            $this->Request_model->update_customer_active_status("users", array($datekey => 0), array("id" => $cust_id));
        }
    }

    public function getSubuserData() {
        $edit_user_id = $_POST['data'];
        // echo $edit_user_id;exit;
        $sub_user_data = $this->Admin_model->getuser_data($edit_user_id);
       // echo "<pre>";print_r($sub_user_data);
        $sub_user_permissions = $this->Request_model->get_sub_user_permissions($edit_user_id);
        $brandIDs = $this->Request_model->selectedBrandsforuser($edit_user_id);
        $all_request = $this->Request_model->getall_request_for_all_status($edit_user_id,'','','created');  
        $totaladded_req = sizeof($all_request);
        $pendingreq = $sub_user_data[0]['total_requests'] - $totaladded_req; 
        if($pendingreq > 0){
            $pendingreqcount = $pendingreq;
        }else{
            $pendingreqcount = 0;
        }
        $result = array();
        $result['sub_user_data'] = $sub_user_data;
        $result['sub_user_permissions'] = $sub_user_permissions;
        $result['totaladded_req'] = $totaladded_req;
        $result['pendingreq'] = $pendingreqcount;
        $result['brandIDs'] = $this->Request_model->selectedBrandsforuser($edit_user_id);
        echo json_encode($result);
    }
    
    public function enable_disable_subuser_account(){
        $status = isset($_POST['status'])?$_POST['status']:'';
        $userid = isset($_POST['data_id'])?$_POST['data_id']:'';
        $userdata = $this->Admin_model->getuser_data($userid);
        $parent_userplan = $this->Admin_model->getuser_data($userdata[0]['parent_id']);
        $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($userdata[0]['parent_id'],$parent_userplan[0]['plan_name'],'login');
       
        $useremail = isset($_POST['data_email'])?$_POST['data_email']:'';
        $username = isset($_POST['data_name'])?$_POST['data_name']:'';
        if($status == "true"){
            $customer = array();
            if($userdata[0]['billing_start_date'] == NULL && $userdata[0]['billing_end_date'] == NULL && $userdata[0]['user_flag'] == 'client'){
                $time = strtotime(date("Y:m:d H:i:s"));
                $customer['billing_start_date'] = date("Y:m:d H:i:s");
                $customer['billing_end_date'] = date("Y:m:d H:i:s", strtotime("+1 month",$time));
            }
            $customer['is_active'] = 1;
           $update_user = $this->Welcome_model->update_data("users", $customer, array("id" => $userid));
        }elseif ($status == "false") {
           $update_user =  $this->Welcome_model->update_data("users", array("is_active" => 0), array("id" => $userid));
        }
        if($update_user && $status == "true"){
          $array = array('CUSTOMER_NAME' => $username,
                    'LOGIN_URL' => $subdomain_url);
        $this->myfunctions->send_email_to_users_by_template($userid,'notification_email_to_activate_user',$array,$useremail);
        
        }
    }
//     public function cancel_current_plan(){
//       require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
//       require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
//       require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
//       $login_user_id = $this->load->get_var('login_user_id'); 
//       $support_email = SUPPORT_EMAIL;
//       $data = array();
////       $support_email = "hashmonika@gmail.com";
//       $user_data = $this->Admin_model->getuser_data($login_user_id); 
//       $reason = isset($_POST['reason'])?$_POST['reason']:'';
//       if($reason == 'others'){
//       $reason = $_POST['reason_desc'];
//       }else{
//          $reason = isset($_POST['reason'])?$_POST['reason']:'';
//       }
//       $data['customer_id'] = $login_user_id;
//       $data['reason'] = $reason;
//       $data['created'] = date("Y:m:d H:i:s");
//       if($reason){
//        $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
//       }
//       //echo "<pre/>";print_R($user_data);exit;
//       $array = array('CUSTOMER_NAME' => ucwords($user_data[0]['first_name'].' '.$user_data[0]['last_name']),
//        'EMAIL' => $user_data[0]['email'],
//        'PLAN' => $user_data[0]['display_plan_name'],
//        'SIGNUPDATE' => date("d-m-Y", strtotime($user_data[0]['created'])),
//        'REASON' => $reason);
//        $this->myfunctions->send_email_to_users_by_template($login_user_id,'email_for_cancel_subscription',$array,$support_email);
//        $this->session->set_flashdata('message_success', "Your request for cancel subscription has been submitted successfully, we'll update you soon!", 5);
//        redirect(base_url().'customer/setting-view#billing');
//    }
    
    public function getbillinginfobaseduser($login_user_data,$mainusers_data) {
        $billinginfo = array();
        if($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client'){
            $total_inprogress_req = $login_user_data[0]['total_inprogress_req'];
            $billinginfo['plan_data'] = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name']);
            $billinginfo['currentplan_details'] = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name']);
            if ($login_user_data[0]['next_plan_name'] != '') {
                $billinginfo['nextplan_details'] = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['next_plan_name']);
            }
            if ($login_user_data[0]['customer_id'] != "" || $login_user_data[0]['customer_id'] != null) {
                $billinginfo['card_details'] = $this->get_customer_detail_from_strip($login_user_data[0]['current_plan'],$login_user_data[0]['customer_id']);
            }
            if($login_user_data[0]['requests_type'] == 'one_time'){
                $all_request = $this->Request_model->getall_request_for_all_status($login_user_data[0]['id'],"","","created_by");
                $count_active_project = $this->Request_model->get_active_requestsbyuserid('active',$mainusers_data[0]['id'],$login_user_data[0]['id']);
                $request['added_request'] = sizeof($all_request);
                $request['active_project'] = sizeof($count_active_project);
                $pending_req = $login_user_data[0]['total_requests'] - $request['added_request'];
                $request['pending_req'] = ($pending_req > 0)?$pending_req:0;
                $billinginfo['request'] = $request;
            }
            $billinginfo['userdata'] = $login_user_data;
        }else{
            $total_inprogress_req = $mainusers_data[0]['total_inprogress_req'];
            $billinginfo['plan_data'] = $this->Request_model->getsubscriptionlistbyplanid($mainusers_data[0]['plan_name']);
            $billinginfo['currentplan_details'] = $this->Request_model->getsubscriptionlistbyplanid($mainusers_data[0]['plan_name']);
            if ($mainusers_data[0]['next_plan_name'] != '') {
                $billinginfo['nextplan_details'] = $this->Request_model->getsubscriptionlistbyplanid($mainusers_data[0]['next_plan_name']);
            }
            if ($mainusers_data[0]['customer_id'] != "" || $mainusers_data[0]['customer_id'] != null) {
                $billinginfo['card_details'] = $this->get_customer_detail_from_strip($mainusers_data[0]['current_plan'],$mainusers_data[0]['customer_id']);
            }
            if(in_array($mainusers_data[0]['plan_name'],NEW_PLANS)){
                $all_request = $this->Request_model->getall_request_for_all_status($mainusers_data[0]['id']);
                $count_active_project = $this->Request_model->get_active_requestsbyuserid('active',$mainusers_data[0]['id']);
                $request['added_request'] = sizeof($all_request);
                $request['active_project'] = sizeof($count_active_project);
                $pending_req = $mainusers_data[0]['total_requests'] - $request['added_request'];
                $request['pending_req'] = ($pending_req > 0)?$pending_req:0;
                $billinginfo['request'] = $request;
            }
            $billinginfo['userdata'] = $mainusers_data;
        }
        if($billinginfo['currentplan_details'][0]['plan_type'] == "unlimited"){
            $billinginfo['currentplan_details'][0]['plan_type'] = $billinginfo['currentplan_details'][0]["global_inprogress_request"]." Request"; 
        }
        return $billinginfo;
    }
    
      /**********cancel plan******/
    public function cancel_current_plan(){
//        echo "<pre/>";print_R($_POST);exit;
       require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
       require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
       require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
       $login_user_id = $this->load->get_var('login_user_id'); 
       $user_data = $this->Admin_model->getuser_data($login_user_id);
       $agencyusers_data = $this->load->get_var('parent_user_data');
        if($reason == 'We are going with another service provider or in-house designer'){
        $reason = $_POST['reason_desc'];
        }else{
           $reason = isset($_POST['reason'])?$_POST['reason']:'';
        }
        $why_reason = isset($_POST['reason_desc'])?$_POST['reason_desc']:'';
       $data = array();
       /****designer switch*****/
        if(isset($_POST['yes_switch_cnfrm'])){
           $support_email = SUPPORT_EMAIL;
//            $support_email = "hashmonika@gmail.com";
            $reason = isset($_POST['reason_val'])?$_POST['reason_val']:'';
            $data['customer_id'] = $login_user_id;
            $data['reason'] = $reason;
            $data['is_designer_switch'] = 1;
            $data['cancel_status'] = 'open';
            $data['coupon_code'] = CANCEL_OFFER;
            $data['cancel_feedback'] = isset($_POST['feedback_val'])?$_POST['feedback_val']:'';
            $data['created'] = date("Y:m:d H:i:s");
            if($reason){
             $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
            }
           
            $array = array('CUSTOMER_NAME' => ucwords($user_data[0]['first_name'].' '.$user_data[0]['last_name']),
            'EMAIL' => $user_data[0]['email'],
            'PLAN' => $user_data[0]['display_plan_name'],
            'SIGNUPDATE' => date("d-m-Y", strtotime($user_data[0]['created'])),
            'REASON' => $reason);
//            echo "<pre/>";print_R($array);exit;
            $this->myfunctions->send_email_to_users_by_template('0','email_for_cancel_subscription',$array,$support_email);
            $this->Stripe->applyCouponToCustomer($user_data[0]['customer_id'],CANCEL_OFFER);
            $this->session->set_flashdata('message_success', "Your account manager will review this and find a better fit for your work. Also give you 20% off the next month for their inconvenience.", 5);
            redirect(base_url().'customer/setting-view#billing');
        }
        /****get 20% off*****/
        elseif(isset($_POST['yes_apply'])){
            $reason = isset($_POST['reason_val'])?$_POST['reason_val']:'';
            $data['customer_id'] = $login_user_id;
            $data['reason'] = $reason;
            $data['cancel_status'] = 'close';
            $data['coupon_code'] = CANCEL_OFFER;
            $data['why_reason'] = isset($_POST['why_reason'])?$_POST['why_reason']:'';
            $data['cancel_feedback'] = isset($_POST['feedback_val'])?$_POST['feedback_val']:'';
            $data['created'] = date("Y:m:d H:i:s");
            if($reason){
             $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
            }
            $this->Stripe->applyCouponToCustomer($user_data[0]['customer_id'],CANCEL_OFFER);
            $this->session->set_flashdata('message_success', "Congratulation! You have got 20% off.", 5);
            redirect(base_url().'customer/setting-view#billing');
        }
        /****cancel anyway*****/
        elseif(isset($_POST['no_apply'])){
            $reason = isset($_POST['reason_val'])?$_POST['reason_val']:'';
            $data['customer_id'] = $login_user_id;
            $data['reason'] = $reason;
            $data['cancel_status'] = 'open';
            $data['coupon_code'] = '';
            $data['why_reason'] = isset($_POST['why_reason'])?$_POST['why_reason']:'';
            $data['cancel_feedback'] = isset($_POST['feedback_val'])?$_POST['feedback_val']:'';
            $data['created'] = date("Y:m:d H:i:s");
            if($reason){
             $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
            }
            $this->session->set_flashdata('message_success', "Cancel request has been placed.", 5);
            redirect(base_url().'customer/setting-view#billing',array('isalreadyrequested' => $isalreadyrequested));
        }
        /***** for clients ******/
        elseif(isset($_POST['reason_submit_client'])){
            $agency_email = $agencyusers_data[0]['email'];
            $data['customer_id'] = $login_user_id;
            $data['reason'] = $reason;
            $data['why_reason'] = $why_reason;
            $data['cancel_status'] = '';
            $data['coupon_code'] = '';
            $data['is_agency'] = 1;
            $data['cancel_feedback'] = isset($_POST['feedback_val'])?$_POST['feedback_val']:'';
            $data['created'] = date("Y:m:d H:i:s");
//            echo "<pre/>";print_R($data);exit;
            if($reason){
             $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
            }
            $array = array('CUSTOMER_NAME' => ucwords($user_data[0]['first_name'].' '.$user_data[0]['last_name']),
            'EMAIL' => $user_data[0]['email'],
            'PLAN' => $user_data[0]['display_plan_name'],
            'SIGNUPDATE' => date("d-m-Y", strtotime($user_data[0]['created'])),
            'REASON' => $reason);
            $this->myfunctions->send_email_to_users_by_template($login_user_id,'email_for_cancel_subscription',$array,$agency_email);
            $this->session->set_flashdata('message_success', "Your account manager will review this and find a better fit for your work.", 5);
            redirect(base_url().'customer/setting-view#billing'); 
        }
        else{
            if(isset($_POST['is_copon_applied']) && $_POST['is_copon_applied'] != ''){
                $reason = isset($_POST['reason'])?$_POST['reason']:'';
                $data['customer_id'] = $login_user_id;
                $data['reason'] = $reason;
                $data['coupon_code'] = '';
                $data['cancel_status'] = 'open';
                $data['why_reason'] = isset($_POST['reason_desc'])?$_POST['reason_desc']:'';
                $data['cancel_feedback'] = isset($_POST['cancel_feebback'])?$_POST['cancel_feebback']:'';
                $data['created'] = date("Y:m:d H:i:s");
                if($reason){
                 $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
                }
                $this->session->set_flashdata('message_success', "Cancellation request has been submitted successfully.", 5);
                redirect(base_url().'customer/setting-view#billing');
            }
        }
    }

    public function change_designer_requestFromClient($ajax=""){
        // echo "<pre>"; print_r($_POST); die; 
        $user_id = $this->load->get_var('main_user');
        if($ajax=1 && !empty($ajax)){
           $data = $this->Welcome_model->select_data("status,user_id","change_designer_request",array("user_id" =>$user_id),"","","desc","id");
           //if(!empty($data)){
            $return["status"] =$data[0]["status"];
          // }else{
         //   $return["status"] = 0;

         //  }
            echo json_encode($return); die; 
        }
        $update_data = $this->input->post('update_data');
        $reason_selected = $this->input->post('reason_selected');
        $addtional_text = $this->input->post('addtional_text');
        $created = date("Y:m:d H:i:s");
        $modified = date("Y:m:d H:i:s");
        if($reason_selected == "other"){
            $add_txt = $addtional_text;
        }else{
            $add_txt = "";
        }
         $data = array("user_id"=>$user_id,"reason_selected"=>$reason_selected,"addtional_text"=>$add_txt,"status"=>1,"created" => $created,"modified"=>$modified);
         $success = $this->Welcome_model->insert_data("change_designer_request", $data);
        if($success){
            $user_data = $this->load->get_var('parent_user_data');
            $amdata = $this->Admin_model->getuser_data($user_data[0]["am_id"]);
            if(!empty($amdata)){ 
                $email = $amdata[0]["email"].",".SUPPORT_EMAIL;
            }else{
                $email = SUPPORT_EMAIL;
            }
            $reason = "<div class='reson_text'><h3> REASON - ".$reason_selected."<h3><p>".$addtional_text."</p></div>";
            $array = array('CUSTOMER_NAME' => ucwords($user_data[0]['first_name'].' '.$user_data[0]['last_name']),
            'REASON' => $reason);
            $this->myfunctions->send_email_to_users_by_template($user_id,'change_designer_request',$array,$email);
            $this->session->set_flashdata('message_success',"Request has been submitted..");
            redirect(base_url().'customer/setting-view#designer_team');

        }else{
            $this->session->set_flashdata('message_error',"issue with submitted your request try in some time");
            redirect(base_url().'customer/setting-view#designer_team');
        }
    }

}
