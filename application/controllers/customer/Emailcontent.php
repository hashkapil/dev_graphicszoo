<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailcontent extends CI_Controller {
    
    public function __construct(){
	  parent::__construct();
		$this->load->library('javascript');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('Admin_model');
		$this->load->model('Welcome_model');
		$this->load->model('Request_model');
		$this->load->model('Stripe');
                $this->load->library('Myfunctions');
                $config_email = $this->config->item('email_smtp');
                define('MAIL_AUTH', true);
                define('MAIL_SECURE', $config_email['sequre']);
                define('MAIL_HOST', $config_email['host']);
                define('MAIL_PORT', $config_email['port']);
                define('MAIL_USERNAME', $config_email['hostusername']);
                define('MAIL_PASSWORD', $config_email['hostpassword']);
                define('MAIL_SENDER', $config_email['sender']);
		
	}
        
    public function editemail() {
        $login_user_id = $this->load->get_var('login_user_id');
        $user_id = $login_user_id;
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $selecteddata = $this->Admin_model->get_emailtemplate_byID($id);
        if (isset($_POST['enable_disable']) && $_POST['enable_disable'] == 1) {
            $updatearray['email_name'] = $selecteddata[0]['email_name'];
            $updatearray['email_subject'] = $selecteddata[0]['email_subject'];
            $updatearray['email_text'] = $selecteddata[0]['email_text'];
            $updatearray['user_id'] = $login_user_id;
            if (isset($_POST['status']) && $_POST['status'] == 'false') {
                $updatearray['disable_email_to_user'] = 1;
            } else {
                $updatearray['disable_email_to_user'] = 0;
            }
        } elseif (isset($_POST['submit'])) {
           // echo "<pre>";print_r($selecteddata[0]['email_footer']);exit;
            if($selecteddata[0]['email_slug'] == 'email_header'){
                $doc = new DOMDocument();
                $doc->loadHTML($_POST['description']);
                $arr = $doc->getElementsByTagName("div");

                foreach ($arr as $item) {
                    $style = $item->getAttribute("style");
                    $item->setAttribute('style', 'padding:25px;');
                    $image = $item->getElementsByTagName("img");

                    foreach ($image as $imgvalue) {
                        $imgvalue->setAttribute('style', 'max-height: 150px;max-width:250px;text-align: center;');
                    }
                }
                $_POST['description'] = $doc->saveHTML();
            }elseif($selecteddata[0]['email_slug'] == 'email_footer'){
                
                $doc = new DOMDocument();
                $doc->loadHTML($_POST['description']);
                
                    $image = $doc->getElementsByTagName("img");
                    foreach ($image as $imgvalue) {
                        $imgvalue->setAttribute('src', FS_PATH_PUBLIC_ASSETS.'img/emailtemplate_img/client_footer-layer.jpg');
                    }

                $_POST['description'] = $doc->saveHTML();
            }
            $updatearray = array('email_name' => $_POST['email_name'], 'email_subject' => $_POST['subject'], 'email_text' => $_POST['description'], 'user_id' => $user_id);
        }
         $html = htmlspecialchars($_POST['description'],ENT_QUOTES);
//         $doc = new DOMDocument();
//         $doc->loadHTML($_POST['description']);
//         $element = $doc->getElementsByClassName('header_section');
//        // $doc->saveHTML();
////         $element = $doc->getElementsByClassName('header_section');
//        // $dom = file_get_html($html);
////         $tables = $html->find('header_section');
//         echo "<pre>";print_r($element);exit;
//            $doc = new DOMDocument();
//            $doc->loadHTML($_POST['description']);
//            $arr = $doc->getElementsByTagName("div");
//
//            foreach ($arr as $item) {
//                $style = $item->getAttribute("style");
//                $item->setAttribute('style', 'padding:25px;');
//                $image = $item->getElementsByTagName("img");
//
//                foreach ($image as $imgvalue) {
//                    $imgvalue->setAttribute('style', 'max-height: 44px;max-width:250px;text-align:center;');
//                }
//            }
//            $html1 = $doc->saveHTML();
//            echo $html."<pre>";print_r($html1);exit;

        if ($selecteddata[0]['user_id'] == 0) {
            $updatearray['email_slug'] = $selecteddata[0]['email_slug'];
            $updatearray['email_img'] = $selecteddata[0]['email_img'];
            $updatearray['identifier'] = $selecteddata[0]['identifier'];
            $updatearray['identifier_description'] = $selecteddata[0]['identifier_description'];
            $updatearray['email_description'] = $selecteddata[0]['email_description'];
            $updatearray['date_add'] = date("Y-m-d H:i:s");

            // echo "<pre>";print_r($updatearray);exit;
            $update_email_temp = $this->Welcome_model->insert_data('email_template', $updatearray);
        } else {
            $update_email_temp = $this->Admin_model->update_data('email_template', $updatearray, array("id" => $id));
        }
        if (isset($_POST['submit'])) {
            if ($update_email_temp) {
                $this->session->set_flashdata('message_success', "Update Successfully", 5);
                redirect(base_url() . "customer/user_setting#Admin_Email");
            } else {
                $this->session->set_flashdata('message_error', "Update Not Successfully", 5);
                redirect(base_url() . "customer/email_content/" . $id);
            }
        }
        echo json_encode($selecteddata);
    }

    public function getemail_content(){
         $id = isset($_POST['id'])?$_POST['id']:'';
         $selecteddata = $this->Admin_model->get_emailtemplate_byID($id);
         echo json_encode($selecteddata);
     }
    
    public function delete_emailtemp_file() {
        $id = isset($_POST['request_id']) ? $_POST['request_id'] : '';
        $filename = isset($_POST['filename']) ? $_POST['filename'] : '';
        $success = $this->Admin_model->delete_emailtemp_file('email_template', $id, $filename);
        if ($success) {
            $this->load->library('s3_upload');
            $dir = FCPATH . '/public/uploads/emailtemplate_img/';
            $this->s3_upload->delete_file_from_s3($dir . $filename);
        }
        $this->session->set_flashdata('message_success', "File is Deleted Successfully.!", 5);
    }
    
    public function delete_emailtemp($id) {
        //echo $id;exit;
        $success = $this->Admin_model->delete_data('email_template', $id);
        if ($success) {
            $this->session->set_flashdata('message_success', "Reset Template Successfully.!", 5);
            redirect(base_url() . "customer/user_setting#Admin_Email");
        }
    }
    
    public function send_agency_testemail(){
        $CI = & get_instance();
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        require_once(APPPATH . "views/identifier.php");
        $id = isset($_POST['template_id'])?$_POST['template_id']:'';
        $email = isset($_POST['user_email'])?$_POST['user_email']:'';
        $login_user_id = $this->load->get_var('login_user_id');
       // $customer_data = $CI->Admin_model->getuser_data($login_user_id);
        //echo "<pre/>";print_r($customer_data);exit;'main_user'
        $parent_user_id =  $this->load->get_var('main_user');
        $email_header = $CI->Admin_model->get_template_byslug_and_userid('email_header',$parent_user_id);
        $email_footer = $CI->Admin_model->get_template_byslug_and_userid('email_footer',$parent_user_id);
        $selecteddata = $this->Admin_model->get_emailtemplate_byID($id);
        $body = '<div class="container" style="box-shadow: 0px 0px 25px #d6dee4; border:1px solid #eee;max-width:650px;width: 100%;margin:20px auto 40px;">';
        $body .= $email_header[0]['email_text'];
        $body_html = $selecteddata[0]['email_text'];
        $email_identifiers = explode(',',$selecteddata[0]['identifier']);
        foreach($email_identifiers as $kid => $vid){
                $literal = trim(strtoupper($vid));
                $body_html = str_replace($literal, $identifiers[$vid],$body_html );
        }
        $body .= $body_html;
        $body .= $email_footer[0]['email_text'];
        $body .= '</div>';
        $title = 'GraphicsZoo';
        //if (strpos($email, 'hash') !== false) {
             $is_send = SM_sendMail($email, $selecteddata[0]['email_subject'], $body, $title,'','',$login_user_id);
       // }
        if($is_send){
                $this->session->set_flashdata('message_success', "Test email sent successfully!", 5);
                redirect(base_url() . "customer/user_setting#Admin_Email");
        }else{
                $this->session->set_flashdata('message_error', "Test email not sent successfully!", 5);
                redirect(base_url() . "customer/user_setting#Admin_Email");
        }
       }
}