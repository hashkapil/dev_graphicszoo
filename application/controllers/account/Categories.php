<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('account/S_myfunctions');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Welcome_model');
        $this->load->model('account/S_request_model');
        $this->load->model('account/S_category_model');
        $this->load->model('Stripe');
    }
	
        public function overWriteCat(){
//         echo "<pre/>";print_R($_POST);exit;   
        $output = $datap = $datac = array();
        $login_user_id = $_SESSION['user_id'];
        $created_user = $this->load->get_var('main_user');
//        echo $created_user;exit;
            if(($_POST['no_overwrite'] == 'no' && $_POST['overwrite'] == '') || ($_POST['no_overwrite'] == '' && $_POST['overwrite'] == 'no')){
                $success = $this->Welcome_model->update_data("agency_user_options", array("overwrite_category" => 1),array("user_id"=> $created_user));
                if($success){
                    $output['status'] =  'success';
                    $output['message'] =  'You can add your own categories!';
                }
            }
            if($_POST['overwrite'] == 'yes' && $_POST['no_overwrite'] == ''){
               $categories = $this->S_category_model->get_categories(0,1);
                $success = $this->Welcome_model->update_data("agency_user_options", array("overwrite_category" => 1),array("user_id"=> $created_user));
//               echo "<pre/>";print_R($categories);exit;
               foreach($categories as $pkey => $pval){
//                   echo "<pre/>";print_R($pval);exit;
                    $datap['parent_id'] = $pval['parent_id'];
                    $datap['user_id'] = $created_user;
                    $datap['bucket_type'] = $pval['bucket_type'];
                    $datap['name'] = $pval['name'];
                    $datap['image_url'] = $pval['image_url'];
                    $datap['agency_only'] = $pval['agency_only'];
                    $datap['position'] = $pval['position'];
                    $datap['timeline'] = $pval['timeline'];
                    $datap['is_active'] = $pval['is_active'];
                    $datap['created'] = date("Y-m-d H:i:s");
                    $datap['modified'] = date("Y-m-d H:i:s");
                    $inserted_id = $this->Welcome_model->insert_data("request_categories", $datap);
                    foreach($pval['child'] as $ckey => $cval){
                        $datac['parent_id'] = $inserted_id;
                        $datac['user_id'] = $created_user;
                        $datac['bucket_type'] = $cval['bucket_type'];
                        $datac['name'] = $cval['name'];
                        $datac['image_url'] = $cval['image_url'];
                        $datac['agency_only'] = $cval['agency_only'];
                        $datac['position'] = $cval['position'];
                        $datac['timeline'] = $cval['timeline'];
                        $datac['is_active'] = $cval['is_active'];
                        $datac['created'] = date("Y-m-d H:i:s");
                        $datac['modified'] = date("Y-m-d H:i:s"); 
                        $insertedc_id = $this->Welcome_model->insert_data("request_categories", $datac);
                    }
                    if($inserted_id && $insertedc_id){
                        $output['status'] =  'success';
                        $output['message'] =  'Congratulations you are using GZ categories and you can able to edit/disable them!';
                        $usercategories = $this->S_category_model->get_categories(0,1,$created_user);
                        $this->load->view('account/categories_listing_template',array('cat_data' => $usercategories)); 
                    }
               }
            }
            echo json_encode($output);
        }
	
	public function cat_listing(){
            $data = array();
            $login_user_id = $_SESSION['user_id'];
            $login_user_data = $this->load->get_var('login_user_data');
            $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
            $created_user = $this->load->get_var('main_user');
            $profile_data = $this->S_admin_model->getuser_data($login_user_id);
            $agency_users = $this->S_request_model->getAgencyuserinfo($created_user);
//            echo "<pre/>";print_R($agency_users[0]['overwrite_category']);exit;
            $is_overwrite_category = $agency_users[0]['overwrite_category'];
            $usercategories = $this->S_category_model->get_categories(0,1,$created_user);
//            echo "<pre/>";print_R($usercategories);exit;
            $data['cat_data'] = $usercategories;
            $data['is_overwrite_category'] = $is_overwrite_category;
            $this->load->view('account/customer_header_1');
            $this->load->view('account/categories/cat_listing', $data);
//             $this->load->view('account/customer_footer_1');
	}
        
//        public function add_category(){
//             $CI = & get_instance();
//            $this->s_myfunctions->checkloginuser("admin");
////            echo "<pre/>";print_R($_POST);exit;
//            if(isset($_POST['save'])){
//               $data['parent_id'] = isset($_POST['parent_cat'])? $_POST['parent_cat']:'';
//               $data['name'] = isset($_POST['cat_name'])? $_POST['cat_name']:'';
//               $data['position'] = isset($_POST['position'])? $_POST['position']:'';
//               $data['bucket_type'] = isset($_POST['bucket_type'])? $_POST['bucket_type']:'';
//               $data['timeline'] = isset($_POST['timeline'])? $_POST['timeline']:'';
//               $data['created'] = date("Y-m-d H:i:s");
//               if($_POST['active'] == 'on'){
//                   $data['is_active'] = 1;
//               }else{
//                   $data['is_active'] = 0;
//               }
//            if ($_FILES['cat_image']['name'] != "") {
//                $file = pathinfo($_FILES['cat_image']['name']);
//                $s3_file = $file['filename'] . '-' . rand(1000, 1) . '.' . $file['extension'];
//                $config = array(
//                    'upload_path' => FS_UPLOAD_PUBLIC.'assets/img/customer/customer-images/',
//                    'allowed_types' => array("jpg", "jpeg", "png"),
//                    'file_name' => $s3_file
//                );
//                $CI->load->library('upload');
//                $CI->upload->initialize($config);
//                if ($CI->upload->do_upload('cat_image')) {
//                    $output['status'] = true;
//                    $output['msg'] = 'uploaded file successfully';
//                } else {
//                    $output['status'] = false;
//                    $output['msg'] = $CI->upload->display_errors();
//                }
//                $data['image_url'] = $s3_file;
//            }
//            $id = $this->Welcome_model->insert_data("request_categories", $data);
//            if($id){
//                $this->session->set_flashdata('message_success', 'Request category added Successfully.!');
//                 redirect(base_url() . "admin/categories/cat_listing/");
//            }else{
//                $this->session->set_flashdata('message_error', 'Request category Not Added!');
//                 redirect(base_url() . "admin/categories/cat_listing/");
//            }
//
//        }
//}
//
//        public function edit_detailcat(){
//            $this->s_myfunctions->checkloginuser("admin");
//            $category_id = isset($_POST['category_id'])?$_POST['category_id']:'';
//            $cat_data = $this->Category_model->get_category_byID($category_id);
//            echo json_encode($cat_data);
//        }
//        
//        public function update_category(){ 
////            echo "<pre/>";print_R($_POST);exit;
//            $CI = & get_instance();
//            $this->s_myfunctions->checkloginuser("admin");
//            $catid = isset($_POST['cat_id'])?$_POST['cat_id']:'';
//            $getAllcat = $this->Category_model->get_category_byID($catid);
////            echo "subcat_id".$catid."<br/>";
////            echo "parent_id".$_POST['parent_cat']."<br/>";
////            exit;
//            if($getAllcat[0]['parent_id'] != 0){
//                if($getAllcat[0]['bucket_type'] != $_POST['bucket_type']){
//                    $allReq = $this->Category_model->getAllrequestsBysubcatID($catid);
////                    echo "<pre/>";print_R($allReq);exit;
//                    foreach($allReq as $kk => $vv){
//                        if($vv['latest_update'] == '' || $vv['latest_update'] == NULL){
//                            $latest_update = date("Y-m-d H:i:s");
//                        }else{
//                            $latest_update = $vv['latest_update'];
//                        }
//                    $expctddate = $this->s_myfunctions->getexpected_datefromtimezone($latest_update,$vv['plan_name'],$_POST['bucket_type'],$vv['timeline']);
//                    $success = $this->Welcome_model->update_data("requests", array("expected_date" => $expctddate,"category_bucket" => $_POST['bucket_type']),array("id"=> $vv['request_id']));
//                    }
//                }
//            }
//            $parentID = ($catid !== $_POST['parent_cat']) ? $_POST['parent_cat'] : '0';
//            $data['parent_id'] = $parentID;
//            $data['name'] = isset($_POST['cat_name'])? $_POST['cat_name']:'';
//            $data['position'] = isset($_POST['position'])? $_POST['position']:'';
//            $data['bucket_type'] = isset($_POST['bucket_type'])? $_POST['bucket_type']:'';
//            $data['timeline'] = isset($_POST['timeline'])? $_POST['timeline']:'';
//            $data['created'] = date("Y-m-d H:i:s");
//            if($_POST['active'] == 'on'){
//                $data['is_active'] = 1;
//            }else{
//                $data['is_active'] = 0;
//            }
//            if ($_FILES['cat_image']['name'] != "") {
//                $file = pathinfo($_FILES['cat_image']['name']);
//                $s3_file = $file['filename'] . '-' . rand(1000, 1) . '.' . $file['extension'];
//                $config = array(
//                    'upload_path' => FS_UPLOAD_PUBLIC.'assets/img/customer/customer-images/',
//                    'allowed_types' => array("jpg", "jpeg", "png"),
//                    'file_name' => $s3_file
//                );
//                $CI->load->library('upload');
//                $CI->upload->initialize($config);
//                if ($CI->upload->do_upload('cat_image')) {
//                    $output['status'] = true;
//                    $output['msg'] = 'uploaded file successfully';
//                } else {
//                    $output['status'] = false;
//                    $output['msg'] = $CI->upload->display_errors();
//                }
//                $data['image_url'] = $s3_file;
//            }
//            
//            $success = $this->Welcome_model->update_data("request_categories",$data, array("id" => $catid));
//            if($success){
//                $this->session->set_flashdata('message_success', 'Request category updated Successfully.!');
//                redirect(base_url() . "admin/categories/cat_listing/");
//            }else{
//                $this->session->set_flashdata('message_error', 'Request category Not updated!');
//                redirect(base_url() . "admin/categories/cat_listing/");
//            }
//            
//        }
//
//
//        public function delete_category(){
//            $this->s_myfunctions->checkloginuser("admin");
//            $category_id = isset($_POST['category_id'])?$_POST['category_id']:'';
//            $delete = $this->Admin_model->delete_data('request_categories', $category_id);
//            if($delete){
//                $this->session->set_flashdata('message_success', 'Request category deleted Successfully.!');
//            }else{
//            $this->session->set_flashdata('message_error', 'Request category Not Deleted!');
//            }
//           redirect(base_url() . "admin/categories/cat_listing/");
//        }
}
