<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ChangeProfile extends CI_Controller {

    /**
     * Index Page for this controller.
     * 
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->model('Category_model');
       // $this->load->library('Myfunctions');
        $this->load->library('account/S_myfunctions');
        $this->load->library('account/S_customfunctions');
        $this->load->helper('form');
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('account/S_request_model');
        $this->load->model('account/S_users_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Clients_model');
        $this->load->model('Stripe');
        $this->load->model('account/S_admin_model');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }

    public function index() {
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);
        if (!empty($_POST)) {
            unset($_POST['savebtn']);
            if (!empty($_FILES)) {
                if ($_FILES['profile']['name'] != "") {
                    if (!empty($_FILES)) {
                        $config2 = array(
                            'upload_path' => './uploads/profile_picture',
                            'allowed_types' => "*",
                            'encrypt_name' => TRUE
                        );
                        $config2['profile'] = $_FILES['profile']['name'];

                        $this->load->library('upload', $config2);

                        if ($this->upload->do_upload('profile')) {
                            $data = array($this->upload->data());
                            $_POST['profile_picture'] = $data[0]['file_name'];
                        } else {
                            $data = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message_error', $data, 5);
                        }
                    }
                }
            }
            $update_data_result = $this->Admin_model->update_data('users', $_POST, array("id" => $user_id));
            if ($update_data_result) {
                $this->session->set_flashdata('message_success', 'User Profile Updated Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Updated.!', 5);
            }
            redirect(base_url() . "account/ChangeProfile/");
        }
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));

        $this->load->view('account/setting-edit', array("data" => $data));
        $this->load->view('account/footer_customer');
    }

    public function change_password_old() {
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

            if ($user_data[0]['new_password'] != $_POST['old_password']) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "account/ChangeProfile");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "account/ChangeProfile");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "account/ChangeProfile");
        }

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $this->load->view('account/change_password');
        $this->load->view('account/footer_customer');
    }

    public function billing_subscription() {
        $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        // echo "<pre>";print_r($user_data);exit;
        $planlist = $this->Stripe->getallsubscriptionlist();
        $current_plan = $this->Stripe->getcurrentplan($user_data[0]['current_plan']);
        $customerdetails = $this->Stripe->getcustomerdetail($user_data[0]['current_plan']);
        if ($current_plan != "") {
            $current_plan = $current_plan->items->data[0]->plan->id;
        }
        if (!empty($_POST)) {
            if ($_POST['plan_name'] == "") {
                $this->session->set_flashdata("message_error", "Please Choose Another Plan..!");
                redirect(base_url() . "account/ChangeProfile/billing_subscription");
            }
            if ($_POST['plan_name'] == $current_plan) {
                $this->session->set_flashdata("message_error", "Please Choose Another Plan..!");
                redirect(base_url() . "account/ChangeProfile/billing_subscription");
            }
            $data = $this->Stripe->updateuserplan($user_data[0]['current_plan'], $_POST['plan_name']);
            if ($data[0] == "error") {
                $this->session->set_flashdata("message_error", $data[1]);
                redirect(base_url() . "account/ChangeProfile/billing_subscription");
            }
            if ($data[0] == "success") {
                $this->session->set_flashdata("message_success", "Plan Updated Successfully..!");
                redirect(base_url() . "account/ChangeProfile/billing_subscription");
            }
        }
        $invoices = $this->Stripe->get_customer_invoices($user_data[0]['customer_id']);
        if (isset($invoices['data'])) {
            $invoices = $invoices['data'];
        } else {
            $invoices = array();
        }
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('account/billing_subscription', array("planlist" => $planlist, "current_plan" => $current_plan, "invoices" => $invoices));
        $this->load->view('account/customer_footer_1');
    }

    public function change_plan() {
        $main_user = $this->load->get_var('main_user');
        $user_data = $this->Admin_model->getuser_data($main_user);
        
        $expiry_month_date = isset($_POST['expiry-month-date'])?$_POST['expiry-month-date']:"";
        $expiry_date =  explode("/", $expiry_month_date);
        $_POST['expiry-month'] = $expiry_date[0];
        $_POST['expiry-year'] = $expiry_date[1];
        
        $customerdetails = $this->Stripe->getcustomerdetail($user_data[0]['current_plan'],$user_data[0]['customer_id']);
        $updatecard = $this->Stripe->change_user_card($customerdetails->id, $_POST);
        
        if ($updatecard[0] == "success") {
            $this->session->set_flashdata("message_success", $updatecard[1]);
        } else {
            $this->session->set_flashdata("message_error", $updatecard[1]);
        }
        redirect(base_url() . "account/setting-view#billing");
    }

    public function change_current_plan() {

        $userdata = $this->S_admin_model->getuser_data($_SESSION['user_id']);
        
        if($userdata[0]["role"] == "manager" || $userdata[0]["user_flag"] == "manager"){
            $us_id = $userdata[0]["parent_id"];
        }else{
            $us_id = $userdata[0]["id"];
        }
        $customer_id = $us_id;
        $user_data = $this->S_admin_model->getuser_data($us_id);
        $current_plan_price = isset($_POST['plan_price']) ? $_POST['plan_price'] : '';
        $display_name = isset($_POST['display_name']) ? $_POST['display_name'] : '';
        $current_plan_name = $user_data[0]['plan_name'];
        $next_plan_name = $user_data[0]['next_plan_name'];
        $subscription_plan_id = $_POST['plan_name'];
        $inprogress_request = isset($_POST['in_progress_request']) ? $_POST['in_progress_request'] : '';
        $userstate = $user_data[0]['state'];
        if (array_key_exists($userstate, STATE_TEXAS)) {
            $state_tax = STATE_TEXAS[$userstate];
        } else {
            $state_tax = 0;
        }
        //if user upgrade 49 plan
        if (in_array($subscription_plan_id, NEW_PLANS)) {
            //echo "dskjgkfhijk";exit;
            $updateuserplan = $this->Stripe->create_invoice($user_data[0]['customer_id'], $state_tax, $current_plan_price);
            // echo "<pre>";print_r($updateuserplan);exit;
            if ($updateuserplan['status'] != true) {
                $updateuserplan['message'] = $updateuserplan['message'];
                $updateuserplan[0] = 'error';
            } else {
                $updateuserplan[0] = 'success';
                $customer['plan_name'] = $subscription_plan_id;
                $customer['display_plan_name'] = $display_name;
                $customer['total_inprogress_req'] = 0;
                $customer['total_active_req'] = 0;
                $customer['total_requests'] = $user_data[0]['total_requests'] + $inprogress_request;
                $customer['plan_amount'] = $current_plan_price;
                $customer['invoice'] = $updateuserplan['id'];
                $customer['billing_start_date'] = date('Y-m-d H:i:s');
            }
        } else {
            if ($user_data[0]['invoice'] == '' || $user_data[0]['invoice'] == NULL || $user_data[0]['invoice'] == 1) {
                $current_plan_weight = $this->S_request_model->getsubscriptionlistbyplanid($current_plan_name);
            } elseif ($user_data[0]['invoice'] == 0) {
                $current_plan_weight = $this->S_request_model->getsubscriptionlistbyplanid($next_plan_name);
            }
            $post_plan_weight = $this->S_request_model->getsubscriptionlistbyplanid($subscription_plan_id);
            if ($current_plan_weight[0]['plan_weight'] <= $post_plan_weight[0]['plan_weight']) {
                $invoice = 1;
            } else {
                $invoice = 0;
            }



            $updateuserplan = $this->Stripe->updateuserplan($user_data[0]['customer_id'], $_POST['plan_name'], $inprogress_request, $invoice);

            $subscription = $updateuserplan['data']['subscriptions'];
            $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
            $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
            //if invoice created
            if ($invoice == 1 || ($invoice != 1 && $user_data[0]['is_cancel_subscription'] == 1)) {
                if ($subscription_plan_id == SUBSCRIPTION_99_PLAN) {
                    $current_date = date("Y:m:d H:i:s");
                    $monthly_date = strtotime("+1 months", strtotime($current_date)); // returns timestamp
                    $billing_expire = date('Y:m:d H:i:s', $monthly_date);
                    $customer['billing_cycle_request'] = 3;
                } else {
                    $customer['billing_cycle_request'] = 0;
                }
                if ($invoice == 1) {
                    $this->Stripe->create_invoice($user_data[0]['customer_id'], $state_tax);
                }
                $customer['plan_name'] = $subscription_plan_id;
                $customer['current_plan'] = $subscription['data'][0]->id;
                $customer['billing_start_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_start);
                $customer['billing_end_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_end);
                $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                $customer['display_plan_name'] = $plandetails['name'];
                $customer['total_inprogress_req'] = $inprogress_request;
                $customer['total_active_req'] = $inprogress_request * TOTAL_ACTIVE_REQUEST;
                $customer['plan_amount'] = $current_plan_price;
                $customer['invoice'] = 1;
                $customer['plan_interval'] = $plandetails['interval'];
                if ($user_data[0]['is_cancel_subscription'] == 1) {
                    $customer['is_cancel_subscription'] = 0;
                    $flag = isset($_POST['reactivate_pro']) ? $_POST['reactivate_pro'] : '';
                    if ($flag == 'yes') {
                        $this->s_myfunctions->movecancelprojecttopreviousstatus($userid);
                    }
                }
                $this->s_myfunctions->moveinqueuereq_toactive_after_updrage_plan($customer_id,$inprogress_request);
                
            } else if ($invoice == 0) {
                //if invoice not created
                $customer['next_plan_name'] = $subscription_plan_id;
                $customer['invoice'] = 0;
                $customer['next_current_plan'] = $subscription['data'][0]->id;
                
            }
        }
        $customer['modified'] = date("Y:m:d H:i:s");
        if ($updateuserplan[0] != 'success') {
            $this->session->set_flashdata('message_error', $updateuserplan['1'], 5);
            redirect(base_url() . "account/setting-view#billing");
        } else {
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $customer_id));
            if ($id) {

                $this->session->set_flashdata('message_success', $updateuserplan['1'], 5);

                redirect(base_url() . "account/setting-view#billing");
            }
        }
    }

    public function setting_edit() {
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);

        if (!empty($_POST)) {
            unset($_POST['savebtn']);
            if (!empty($_FILES)) {
                if ($_FILES['profile']['name'] != "") {
                    if (!empty($_FILES)) {
                        $config2 = array(
                            'upload_path' => './uploads/profile_picture',
                            'allowed_types' => "*",
                            'encrypt_name' => TRUE
                        );
                        $config2['profile'] = $_FILES['profile']['name'];

                        $this->load->library('upload', $config2);

                        if ($this->upload->do_upload('profile')) {
                            $data = array($this->upload->data());
                            $_POST['profile_picture'] = $data[0]['file_name'];
                        } else {
                            $data = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message_error', $data, 5);
                        }
                    }
                }
            }
            $update_data_result = $this->Admin_model->update_data('users', $_POST, array("id" => $user_id));
            if ($update_data_result) {
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
            redirect(base_url() . "account/setting-view/");
        }

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));

        $this->load->view('account/setting-edit', array("data" => $data));
        $this->load->view('account/footer_customer');
    }

    public function change_password() {
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

            if ($user_data[0]['new_password'] != $_POST['old_password']) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "account/setting-edit");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "account/setting-edit");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "account/setting-edit");
        }

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
        $this->load->view('account/change_password');
        $this->load->view('account/footer_customer');
    }

    public function change_password_front() {
        //echo "sad"; exit; 
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
        $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($_SESSION['user_id']); 

        // echo "<pre>"; print_r($CheckForMainORsub); die; 
        if (!empty($_POST)) {
            $user_data = $this->S_admin_model->getuser_data($_SESSION['user_id']);
            if ($user_data[0]['new_password'] != md5($_POST['old_password'])) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "account/setting-view#password");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "account/setting-view#password");
            }
            
                      
            if($CheckForMainORsub['is_saas'] == 1){
                if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                    $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
                } else {
                    $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
                }

            }else{
                if ($this->Admin_model->update_data("s_users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                    $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
                } else {
                    $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
                }
            }
            
            redirect(base_url() . "account/setting-view#password");
        }

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
        $this->load->view('account/setting-view#password');
        $this->load->view('account/footer_customer');
    }

    public function setting_view() {
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
//        die("vcb");
        $main_user = $this->load->get_var('main_user');
        $seebilling = $this->s_myfunctions->isUserPermission('billing_module');
        $canseebilling = $this->canseebilling_sec($seebilling);
        $permission = array("seebilling" => $canseebilling,
            "add_team_member" => $this->s_myfunctions->isUserPermission('add_team_member'),
            "manage_clients" => $this->s_myfunctions->isUserPermission('manage_clients'),
            "assigndes_to_client" => $this->s_myfunctions->isUserPermission('assign_designer_to_client'));
        $user_id = $this->load->get_var('login_user_id'); 
 
        $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($user_id);
        $parent_user_data = $this->load->get_var('parent_user_data');
        $allData['designer'] = $this->S_users_model->get_Susers("designer","",$main_user);
        $allData['customer'] = $this->S_users_model->get_Susers("customer","",$main_user);
        $allData['manager'] = $this->S_users_model->get_Susers("manager","",$main_user);
//        echo "<pre/>";print_R($allData);
         for ($i = 0; $i < sizeof($allData['customer']); $i++) { 
            if($allData['customer'][$i]['designer_id']!= 0){
               
             $allData['customer'][$i]['is_designer_assign'] = $allData['customer'][$i]['designer_id'];
             
             $allData['customer'][$i]['designer_info'][] = $this->Welcome_model->select_data("first_name","s_users","id='".$allData['customer'][$i]['designer_id']."'");
             $allData['customer'][$i]['designer_info'][] = $this->Welcome_model->select_data("last_name","s_users","id='".$allData['customer'][$i]['designer_id']."'");
             
            }else{
                 $allData['customer'][$i]['is_designer_assign'] = 0;
                 $allData['customer'][$i]['designer_first_name'] = "";
                 $allData['customer'][$i]['designer_last_name'] = "";
                  
            }

            if($allData['customer'][$i]['va_id']!= 0){
                $allData['customer'][$i]['is_Va_assign'] = $allData['customer'][$i]['va_id'];
                $allData['customer'][$i]['va_info'][] = $this->Welcome_model->select_data("first_name","s_users","id='".$allData['customer'][$i]['va_id']."'");
                $allData['customer'][$i]['va_info'][] = $this->Welcome_model->select_data("last_name","s_users","id='".$allData['customer'][$i]['va_id']."'");
            }else{
                $allData['customer'][$i]['is_Va_assign'] = 0;
                $allData['customer'][$i]['va_first_name'] = "";
                $allData['customer'][$i]['va_last_name']  = "";
            }

            for ($k=0; $k < sizeof($allData['customer'][$i]['designer_info']) ; $k++) { 

                $allData['customer'][$i]['designer_first_name'] = $allData['customer'][$i]['designer_info'][0][0]
                ['first_name'];
                $allData['customer'][$i]['designer_last_name'] =  $allData['customer'][$i]['designer_info'][1][0]
                ['last_name'];

                unset($allData['customer'][$i]['designer_info']); 
            }

            for ($k=0; $k < sizeof($allData['customer'][$i]['va_info']) ; $k++) { 

                $allData['customer'][$i]['va_first_name'] = $allData['customer'][$i]['va_info'][0][0]
                ['first_name'];
                $allData['customer'][$i]['va_last_name'] =  $allData['customer'][$i]['va_info'][1][0]
                ['last_name'];

                unset($allData['customer'][$i]['va_info']); 
            }
            $allData['customer'][$i]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($allData['customer'][$i]['profile_picture'],1); 

         }
 
        for ($i = 0; $i < sizeof($allData['designer']); $i++) {
            $user = $this->Account_model->get_all_active_request_by_designer($allData['designer'][$i]['id']);
            $allData['designer'][$i]['active_request'] = sizeof($user);
            $allData['designer'][$i]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($allData['designer'][$i]['profile_picture'],1);  
        }
        for ($i = 0; $i < sizeof($allData['manager']); $i++) {
            $user = $this->Account_model->get_all_active_request_by_designer($allData['manager'][$i]['id']);
            $allData['manager'][$i]['active_request'] = sizeof($user);
            $allData['manager'][$i]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($allData['manager'][$i]['profile_picture'],1);  
        }

       // echo "<pre>"; print_r($allData['customer']); die; 
        
        // $this->S_admin_model->getuser_data($login_user_id);
        /** get billing details of user **/
        $billinginfo = $this->getbillinginfobaseduser($login_user_data,$parent_user_data);
        $url = "account/setting-view?status=client#management";
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url,"user_id"=> $user_id));
        $getalltimezone = $this->Request_model->getAlltimezone();
        $isalreadyrequested = $this->Account_model->isAlreadyCancelled($user_id);
        $checkcouponapplied =  $this->Account_model->isAlreadyCancelled($user_id,true);
        /**get requests to assign dedicated designer**/
        $allreq = $this->Request_model->getall_newrequest($main_user,array('active','checkforapprove','disapprove','approved'),'','','id');
        $profile_data = $this->S_admin_model->getuser_data($user_id);
//        echo "<pre>";print_R($profile_data);
        if($login_user_data[0]["is_saas"] == 1 || ($login_user_data[0]["role"] == "manager" && $permission["add_team_member"] != 0)){
            $s_plans = $this->getsaasplans($main_user);
            $SaaSPlan = $this->S_request_model->getsaasuserplan($main_user); 
            if(!empty($SaaSPlan)){
               $saasPlanDetails = $this->S_request_model->getsubscriptionlistbyplanid($SaaSPlan[0]['plan_id'],"1");
              }
              $totalSubUserunderSAAS['customer'] = $this->S_admin_model->totalSubUserunderSAAS($main_user,"customer"); 
              $totalSubUserunderSAAS['designer'] = $this->S_admin_model->totalSubUserunderSAAS($main_user,"designer"); 
              $totalSubUserunderSAAS['manager'] = $this->S_admin_model->totalSubUserunderSAAS($main_user,"manager"); 
        }
        $profile_data[0]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($profile_data[0]['profile_picture']);
 
        // echo "<pre>"; print_r($saasPlanDetails);die;
        $currentuser = $this->load->get_var('main_user');
        $subusersdata = $this->S_request_model->getAll_S_subUsers($currentuser,"manager");
        foreach ($subusersdata as $userskey => $usersvalue) {
           $subusersdata[$userskey]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($usersvalue['profile_picture']); 
        } 
        $timezone = $this->Admin_model->get_timezone();
        $brandprofile = $this->Request_model->get_brand_profile_by_user_id($main_user);
        $subusers = $this->Admin_model->sumallsubuser_requestscount($main_user);
        $main_active_req = $parent_user_data[0]['total_active_req'];
        $main_inprogress_req = $parent_user_data[0]['total_inprogress_req'];
        $subtotal_active_req = $subusers['total_active_req'];
        $subtotal_inprogress_req = $subusers['total_inprogress_req'];
        $checksubuser_requests = array('main_active_req' => $main_active_req,
            'main_inprogress_req' => $main_inprogress_req,
            'subtotal_active_req' => isset($subtotal_active_req)?$subtotal_active_req:0,
            'subtotal_inprogress_req' => isset($subtotal_inprogress_req)?$subtotal_inprogress_req:0);
        $isshow_subuser = $this->is_show_subuser($currentuser);

        $active_subscriptions = $this->Clients_model->getsubscriptionbaseduser($main_user,'',1); 

        foreach ($active_subscriptions as $key => $userplan){
            $active_subscriptions[$key]['client_slot'] = $this->customfunctions->checkclientaddornot($main_userdata,$userplan);
            if($userplan['plan_type'] == "unlimited"){
               $active_subscriptions[$key]['plan_type'] = $userplan["global_inprogress_request"]." Request"; 
            }
        }

                // echo "<pre>"; print_r($active_subscriptions); echo "</pre>"; 
                // die(); 

        $PaymentpermiSsion = $this->S_admin_model->getuserPaymentpermiSsion($main_user); 
        

        $this->load->view('account/customer_header_1');
        $this->load->view('account/setting-view-new', array("data" => $billinginfo['userdata'],"profile" => $profile_data,"plan_data"=> $billinginfo['plan_data'],"nextplan_details" => $billinginfo['nextplan_details'], 'card' => $billinginfo['card_details'], 
            "timezone" => $timezone, 'getalltimezone' => $getalltimezone, 'subusersdata' => $subusersdata,
             'brandprofile' => $brandprofile,'permission' => $permission,'currentplan_details'=>$billinginfo['currentplan_details'],'request'=>$billinginfo['request'],
            'checksubuser_requests' => $checksubuser_requests,'allreq' => $allreq,'isshow_subuser'=>$isshow_subuser,'isalreadyrequested'=> $isalreadyrequested,'checkcouponapplied' => $checkcouponapplied,'CheckForMainORsub'=>$CheckForMainORsub,'alluserData'=>$allData ,'active_subscriptions' =>$active_subscriptions,'PaymentpermiSsion' =>$PaymentpermiSsion,"s_plans" => $s_plans,"totalSubUserunderSAAS" =>$totalSubUserunderSAAS,"saasPlanDetails"=>$saasPlanDetails,'SaaSPlan'=>$SaaSPlan));
        $this->load->view('account/footer_customer',array('total_inprogress_req' => $billinginfo['total_inprogress_req']));
    }
    
    public function edit_profile_image_form() {
         
         $fname = explode(' ', $_POST['first_name']);
          $update_data_result = $this->s_customfunctions->uploadprofilewiththumb($_FILES);
          if ($update_data_result) {
               $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
          } else {
               $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
          }
          
        if (!empty($this->input->post())) {
            $user_data = $this->Request_model->getuserbyid($user_id);
            $customer_id = isset($user_data[0]['customer_id'])? $user_data[0]['customer_id']: '';
            $checkemailexits = $this->Request_model->getuserbyemail($_POST['notification_email'], $user_id);
            if (!empty($checkemailexits)) {
                $this->session->set_flashdata('message_error', "Email address is already used!", 5);
            } else {
                $profile_pic = array(
                    'first_name' => $fname[0],
                    'last_name' => $fname[1],
                    'company_name' => $_POST['company'],
                    'email' => $_POST['notification_email'],
                    // 'title' => $_POST['title'],
                    'phone' => $_POST['phone'],
                    'url' => $_POST['url'],
                );
                if($CheckForMainORsub['is_saas'] == 1){
                    // echo "1";
                    $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                    $stripe_update_data = $this->Stripe->updateCustomer($customer_id,$_POST['notification_email']);
                }else{

                    // echo "2";
                    $update_data_result = $this->S_admin_model->designer_update_profile($profile_pic, $user_id);
                    $stripe_update_data = $this->Stripe->updateCustomer($customer_id,$_POST['notification_email']);

                }
                if ($update_data_result) {
                    $this->session->set_flashdata('message_success', 'Account info updated successfully!', 5);
                } else {
                    $this->session->set_flashdata('message_error', 'Error occurred while updating account info!', 5);
                }
            }
        }
    }

    public function customer_edit_profile() {
        $user_id = $_SESSION['user_id'];
        $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($user_id); 
        $profileUpdate = array(
            'address_line_1' => $_POST['adress'],
            // 'title' => $_POST['title'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'zip' => $_POST['zip'],
            'timezone' => $_POST['timezone'],
        );
        if($CheckForMainORsub['is_saas'] == 1){

            $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
        }else{
            $update_data_result = $this->S_admin_model->designer_update_profile($profileUpdate, $user_id);

        }
        if ($update_data_result) {
            $this->session->set_userdata('timezone', $profileUpdate['timezone']);
            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
        } else {
            $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
        }
    }

    public function get_customer_detail_from_strip($plan_id,$customer_id="") {
        $card_detail = $this->Stripe->getcustomerdetail($plan_id,$customer_id);
      //  echo "<pre>";print_r($card_detail);exit; 
        $card = $card_detail->sources['data'];
       // echo "<pre>";print_r($card);exit; 
        return $card;
    }

    public function getdetailplan($plan_id) {
        $card_detail = $this->Stripe->getcurrentplan($plan_id);
        return $card_detail->items->data[0]->plan;
    }

    public function change_status_usersettings() {

        $cust_status = $_POST['status'];
        $cust_id = $_POST['data_id'];
       // echo $cust_id; exit; 
        $datekey = $_POST['data_key'];
        $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($cust_id); 
        if($CheckForMainORsub['is_saas'] == 1){

             if ($_POST['status'] == 'true') {
                $this->Request_model->update_customer_active_status("users", array($datekey => 1), array("id" => $cust_id));
            } elseif ($_POST['status'] == 'false') {
                $this->Request_model->update_customer_active_status("users", array($datekey => 0), array("id" => $cust_id));
            }
        }else{
            if($_POST['status'] == 'true') {
          //  echo "<pre>"; print_r($cust_id); die; 
                $this->S_request_model->update_customer_active_status("s_users", array($datekey => 1), array("id" => $cust_id));
            } elseif ($_POST['status'] == 'false') {
                $this->S_request_model->update_customer_active_status("s_users", array($datekey => 0), array("id" => $cust_id));
            }

        }
        
    }

    public function getSubuserData() {
        $edit_user_id = $_POST['data'];
        // echo $edit_user_id;exit;
        $sub_user_data = $this->S_admin_model->getuser_data($edit_user_id);
       // echo "<pre>";print_r($sub_user_data);
        $sub_user_permissions = $this->S_request_model->get_sub_user_permissions($edit_user_id);
        $brandIDs = $this->S_request_model->selectedBrandsforuser($edit_user_id);
        $all_request = $this->S_request_model->getall_request_for_all_status($edit_user_id,'','','created');  
        $totaladded_req = sizeof($all_request);
        $pendingreq = $sub_user_data[0]['total_requests'] - $totaladded_req; 
        if($pendingreq > 0){
            $pendingreqcount = $pendingreq;
        }else{
            $pendingreqcount = 0;
        }
        $result = array();
        $result['sub_user_data'] = $sub_user_data;
        $result['sub_user_permissions'] = $sub_user_permissions;
        $result['totaladded_req'] = $totaladded_req;
        $result['pendingreq'] = $pendingreqcount;
        $result['brandIDs'] = $this->S_request_model->selectedBrandsforuser($edit_user_id);
//        echo "<pre/>";print_R($result);exit;
        echo json_encode($result);
    }
    
    public function enable_disable_subuser_account(){
        $status = isset($_POST['status'])?$_POST['status']:'';
        $userid = isset($_POST['data_id'])?$_POST['data_id']:'';
        $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($userid);  
        // echo "<pre>"; print_r($CheckForMainORsub)   ; exit; 
        $userdata = $this->Admin_model->getuser_data($userid);
        $parent_userplan = $this->Admin_model->getuser_data($userdata[0]['parent_id']);
        $subdomain_url = $this->s_myfunctions->dynamic_urlforsubuser_inemail($userdata[0]['parent_id'],$parent_userplan[0]['plan_name'],'login');
       
        $useremail = isset($_POST['data_email'])?$_POST['data_email']:'';
        $username = isset($_POST['data_name'])?$_POST['data_name']:'';
        if($status == "true"){
            $customer = array();
            if($userdata[0]['billing_start_date'] == NULL && $userdata[0]['billing_end_date'] == NULL && $userdata[0]['user_flag'] == 'client'){
                $time = strtotime(date("Y:m:d H:i:s"));
                $customer['billing_start_date'] = date("Y:m:d H:i:s");
                $customer['billing_end_date'] = date("Y:m:d H:i:s", strtotime("+1 month",$time));
            }
            $customer['is_active'] = 1;
            if($CheckForMainORsub['is_saas'] == 1){
           $update_user = $this->Welcome_model->update_data("users", $customer, array("id" => $userid));
            }else{
             $update_user = $this->Welcome_model->update_data("s_users", $customer, array("id" => $userid));   
            }
        }elseif ($status == "false") {
            if($CheckForMainORsub['is_saas'] == 1){
           $update_user =  $this->Welcome_model->update_data("users", array("is_active" => 0), array("id" => $userid));
         }else{
             $update_user =  $this->Welcome_model->update_data("s_users", array("is_active" => 0), array("id" => $userid));
         }
        }
        if($update_user && $status == "true"){
          $array = array('CUSTOMER_NAME' => $username,
                    'LOGIN_URL' => $subdomain_url);
          $this->s_myfunctions->send_email_to_users_by_template($userid,'notification_email_to_activate_user',$array,$useremail);

        }
    }
    
    public function is_show_subuser($currentuser=""){
        $subusersdata = $this->Request_model->getAllsubUsers($currentuser,"manager","","1");
        $users_data = $this->load->get_var('parent_user_data');
        $user_plan_data = $this->load->get_var('parent_user_plan');
        $subuser = array();
        if ($users_data[0]['overwrite'] == 1) {
            if ($users_data[0]['sub_user_count'] == -1 || $users_data[0]['sub_user_count'] != 0) {
                $subuser['add'] = 1;
                $subuser['count'] = $users_data[0]['sub_user_count'];
            }else{
                $subuser['add'] = 0;
            }
        }else{
            if ($user_plan_data[0]['total_sub_user'] == -1 || $user_plan_data[0]['total_sub_user'] != 0 ) {
                $subuser['add'] = 1;
                $subuser['count'] = $user_plan_data[0]['total_sub_user'];
            }else{
                $subuser['add'] = 0;
            }
        }
        return $subuser;
    }
    
    public function getbillinginfobaseduser($login_user_data,$mainusers_data) {
        $billinginfo = array();
        if($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client'){
            $total_inprogress_req = $login_user_data[0]['total_inprogress_req'];
            $billinginfo['plan_data'] = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name']);
            $billinginfo['currentplan_details'] = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name']);
            if ($login_user_data[0]['next_plan_name'] != '') {
                $billinginfo['nextplan_details'] = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['next_plan_name']);
            }
            if ($login_user_data[0]['customer_id'] != "" || $login_user_data[0]['customer_id'] != null) {
                $billinginfo['card_details'] = $this->get_customer_detail_from_strip($login_user_data[0]['current_plan'],$login_user_data[0]['customer_id']);
            }
            if($login_user_data[0]['requests_type'] == 'one_time'){
                $all_request = $this->Request_model->getall_request_for_all_status($login_user_data[0]['id'],"","","created_by");
                $count_active_project = $this->Request_model->get_active_requestsbyuserid('active',$mainusers_data[0]['id'],$login_user_data[0]['id']);
                $request['added_request'] = sizeof($all_request);
                $request['active_project'] = sizeof($count_active_project);
                $pending_req = $login_user_data[0]['total_requests'] - $request['added_request'];
                $request['pending_req'] = ($pending_req > 0)?$pending_req:0;
                $billinginfo['request'] = $request;
            }
            $billinginfo['userdata'] = $login_user_data;
        }else{
            $total_inprogress_req = $mainusers_data[0]['total_inprogress_req'];
            $billinginfo['plan_data'] = $this->Request_model->getsubscriptionlistbyplanid($mainusers_data[0]['plan_name']);
            $billinginfo['currentplan_details'] = $this->Request_model->getsubscriptionlistbyplanid($mainusers_data[0]['plan_name']);
            if ($mainusers_data[0]['next_plan_name'] != '') {
                $billinginfo['nextplan_details'] = $this->Request_model->getsubscriptionlistbyplanid($mainusers_data[0]['next_plan_name']);
            }
            if ($mainusers_data[0]['customer_id'] != "" || $mainusers_data[0]['customer_id'] != null) {
                $billinginfo['card_details'] = $this->get_customer_detail_from_strip($mainusers_data[0]['current_plan'],$mainusers_data[0]['customer_id']);
            }
            if(in_array($mainusers_data[0]['plan_name'],NEW_PLANS)){
                $all_request = $this->Request_model->getall_request_for_all_status($mainusers_data[0]['id']);
                $count_active_project = $this->Request_model->get_active_requestsbyuserid('active',$mainusers_data[0]['id']);
                $request['added_request'] = sizeof($all_request);
                $request['active_project'] = sizeof($count_active_project);
                $pending_req = $mainusers_data[0]['total_requests'] - $request['added_request'];
                $request['pending_req'] = ($pending_req > 0)?$pending_req:0;
                $billinginfo['request'] = $request;
            }
            $billinginfo['userdata'] = $mainusers_data;
        }
        if($billinginfo['currentplan_details'][0]['plan_type'] == "unlimited"){
            $billinginfo['currentplan_details'][0]['plan_type'] = $billinginfo['currentplan_details'][0]["global_inprogress_request"]." Request"; 
        }
        return $billinginfo;
    }
    
      /**********cancel plan******/
    public function cancel_current_plan(){
//        echo "<pre/>";print_R($_POST);exit;
       require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
       require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
       require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
       $login_user_id = $this->load->get_var('login_user_id'); 
       $user_data = $this->Admin_model->getuser_data($login_user_id);
       $agencyusers_data = $this->load->get_var('parent_user_data');
        if($reason == 'We are going with another service provider or in-house designer'){
        $reason = $_POST['reason_desc'];
        }else{
           $reason = isset($_POST['reason'])?$_POST['reason']:'';
        }
        $why_reason = isset($_POST['reason_desc'])?$_POST['reason_desc']:'';
       $data = array();
       /****designer switch*****/
        if(isset($_POST['yes_switch_cnfrm'])){
           $support_email = SUPPORT_EMAIL;
            $reason = isset($_POST['reason_val'])?$_POST['reason_val']:'';
            $data['customer_id'] = $login_user_id;
            $data['reason'] = $reason;
            $data['is_designer_switch'] = 1;
            $data['cancel_status'] = 'open';
            $data['coupon_code'] = CANCEL_OFFER;
            $data['cancel_feedback'] = isset($_POST['feedback_val'])?$_POST['feedback_val']:'';
            $data['created'] = date("Y:m:d H:i:s");
            if($reason){
             $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
            }
           
            $array = array('CUSTOMER_NAME' => ucwords($user_data[0]['first_name'].' '.$user_data[0]['last_name']),
            'EMAIL' => $user_data[0]['email'],
            'PLAN' => $user_data[0]['display_plan_name'],
            'SIGNUPDATE' => date("d-m-Y", strtotime($user_data[0]['created'])),
            'REASON' => $reason);
            $this->s_myfunctions->send_email_to_users_by_template('0','email_for_cancel_subscription',$array,$support_email);
            $this->Stripe->applyCouponToCustomer($user_data[0]['customer_id'],CANCEL_OFFER);
            $this->session->set_flashdata('message_success', "Your account manager will review this and find a better fit for your work. Also give you 20% off the next month for their inconvenience.", 5);
            redirect(base_url().'account/setting-view#billing');
        }
        /****get 20% off*****/
        elseif(isset($_POST['yes_apply'])){
            $reason = isset($_POST['reason_val'])?$_POST['reason_val']:'';
            $data['customer_id'] = $login_user_id;
            $data['reason'] = $reason;
            $data['cancel_status'] = 'close';
            $data['coupon_code'] = CANCEL_OFFER;
            $data['why_reason'] = isset($_POST['why_reason'])?$_POST['why_reason']:'';
            $data['cancel_feedback'] = isset($_POST['feedback_val'])?$_POST['feedback_val']:'';
            $data['created'] = date("Y:m:d H:i:s");
            if($reason){
             $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
            }
            $this->Stripe->applyCouponToCustomer($user_data[0]['customer_id'],CANCEL_OFFER);
            $this->session->set_flashdata('message_success', "Congratulation! You have got 20% off.", 5);
            redirect(base_url().'account/setting-view#billing');
        }
        /****cancel anyway*****/
        elseif(isset($_POST['no_apply'])){
            $reason = isset($_POST['reason_val'])?$_POST['reason_val']:'';
            $data['customer_id'] = $login_user_id;
            $data['reason'] = $reason;
            $data['cancel_status'] = 'open';
            $data['coupon_code'] = '';
            $data['why_reason'] = isset($_POST['why_reason'])?$_POST['why_reason']:'';
            $data['cancel_feedback'] = isset($_POST['feedback_val'])?$_POST['feedback_val']:'';
            $data['created'] = date("Y:m:d H:i:s");
            if($reason){
             $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
            }
            $this->session->set_flashdata('message_success', "Cancel request has been placed.", 5);
            redirect(base_url().'account/setting-view#billing',array('isalreadyrequested' => $isalreadyrequested));
        }
        /***** for clients ******/
        elseif(isset($_POST['reason_submit_client'])){
            $agency_email = $agencyusers_data[0]['email'];
            $data['customer_id'] = $login_user_id;
            $data['reason'] = $reason;
            $data['why_reason'] = $why_reason;
            $data['cancel_status'] = '';
            $data['coupon_code'] = '';
            $data['is_agency'] = 1;
            $data['cancel_feedback'] = isset($_POST['feedback_val'])?$_POST['feedback_val']:'';
            $data['created'] = date("Y:m:d H:i:s");
//            echo "<pre/>";print_R($data);exit;
            if($reason){
             $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
            }
            $array = array('CUSTOMER_NAME' => ucwords($user_data[0]['first_name'].' '.$user_data[0]['last_name']),
            'EMAIL' => $user_data[0]['email'],
            'PLAN' => $user_data[0]['display_plan_name'],
            'SIGNUPDATE' => date("d-m-Y", strtotime($user_data[0]['created'])),
            'REASON' => $reason);
            $this->s_myfunctions->send_email_to_users_by_template($login_user_id,'email_for_cancel_subscription',$array,$agency_email);
            $this->session->set_flashdata('message_success', "Your account manager will review this and find a better fit for your work.", 5);
            redirect(base_url().'account/setting-view#billing'); 
        }
        else{
            if(isset($_POST['is_copon_applied']) && $_POST['is_copon_applied'] != ''){
                $reason = isset($_POST['reason'])?$_POST['reason']:'';
                $data['customer_id'] = $login_user_id;
                $data['reason'] = $reason;
                $data['coupon_code'] = '';
                $data['cancel_status'] = 'open';
                $data['why_reason'] = isset($_POST['reason_desc'])?$_POST['reason_desc']:'';
                $data['cancel_feedback'] = isset($_POST['cancel_feebback'])?$_POST['cancel_feebback']:'';
                $data['created'] = date("Y:m:d H:i:s");
                if($reason){
                 $success = $this->Welcome_model->insert_data("cancelled_subscriptions", $data);
                }
                $this->session->set_flashdata('message_success', "Cancellation request has been submitted successfully.", 5);
                redirect(base_url().'account/setting-view#billing');
            }
        }
    }

     public function assign_designer_for_customer() {
                // echo "<pre>"; print_r($_POST); echo "</pre>"; 
                // die(); 
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
            $login_user_id = $this->load->get_var('login_user_id');
            $success_add = $this->S_admin_model->update_data("s_users", array('designer_id' => $_POST['assign_designer']), array("id" => $_POST['customer_id']));

            $this->s_myfunctions->capture_project_activity('', '', $_POST['assign_designer'], 'assign_paramanent_designer', 'assign_designer_for_customer', $login_user_id, '', '', '', $_POST['customer_id']);
            $all_request = $this->S_request_model->get_request_by_customer_id($_POST['customer_id']);
            $assign_id = $this->input->post('assign_designer'); 
            //echo "<pre>"; print_r($all_request);  exit; 

            if(empty($all_request)){

            }
           foreach ($all_request as $request) {
                $request_id[] = $request['id'];
             $checkDraft_id = $this->Welcome_model->select_data("draft_id,designer_id","activity_timeline","request_id='".$request['id']."' AND slug ='assign_designer' AND action_perform_by ='".$login_user_id."'",$limit="",$start="","desc","draft_id");
             //exit; 
             $checkDraft_idForReas = $this->Welcome_model->select_data("draft_id,designer_id","activity_timeline","request_id='".$request['id']."' AND slug ='reassign_designer' AND action_perform_by ='".$login_user_id."' ",$limit="",$start="","desc","id");

           if(!empty($checkDraft_id) ){ 
            if(empty($checkDraft_idForReas) &&  $checkDraft_id[0]['draft_id'] == 0 && $checkDraft_id[0]['designer_id'] != 0){
                $new_assign_id =$assign_id;
                $assign_id =$checkDraft_id[0]['designer_id'];
                $slug='reassign_designer';
            }else if($checkDraft_idForReas[0]['draft_id'] != 0 && $checkDraft_idForReas[0]['designer_id'] != 0){
                $new_assign_id =$assign_id;
                $assign_id =$checkDraft_idForReas[0]['draft_id'];  
                $slug='reassign_designer';
            }
           }else{
               $new_assign_id = 0;
               $assign_id = $assign_id; 
               $slug='assign_designer';
           }
       $this->s_myfunctions->capture_project_activity($request['id'],$new_assign_id, $assign_id, $slug, 'assign_designer_for_customer', $login_user_id);
       }
        $this->S_request_model->update_request_admin($request_id, array('designer_id' => $_POST['assign_designer']));
        if ($success_add) {
            $this->session->set_flashdata('message_success', "Assigned designer Successfully", 5);
            redirect(base_url() . "account/setting-view#management");
        }
    }

    public function assign_va_tocustomer() {
        //$this->s_myfunctions->checkloginuser("customer");
        $customerid = explode(',', $_POST['customer_id']);
        $output = array();
        if (isset($_POST['customer_id']) && $_POST['customer_id'] != "") {
            $this->S_admin_model->assign_va_touser($_POST['assign_va'], $customerid);
            $output['status'] = "success";
            $output['msg'] = "designer updated successfully...!";
        } else {
            $output['status'] = "error";
            $output['msg'] = "Please select atleat one request for assign designer..!";
        }
        echo json_encode($output);
    }
    
    public function getsaasplans($user_id) {
        $plans =array();
        $plan_name = $this->S_request_model->getsaasuserplan($user_id);
        if(!empty($plan_name)){
         $plans['current'] = $this->S_request_model->getsubscriptionlistbyplanid($plan_name[0]["plan_id"],"s_subscription_plan");
        }
        $plans["all"] = $this->S_users_model->getalsaasubscription();
        foreach ($plans["all"] as $key => $value) {
            if($plan_name[0]["plan_id"] == $value['plan_id']){
                $plans["all"][$key]['info']['target'] = "currentplan";
                $plans["all"][$key]['info']['class'] = "currentPlan f_actv_pln";
                $plans["all"][$key]['info']['text'] = "Current Plan";
            } else if ($plans['current'][0]["plan_weight"] > $value["plan_weight"]) {
                $plans["all"][$key]['info']['target'] = "canntdowngrade";
                $plans["all"][$key]['info']['class'] = "canntdowngrade";
                $plans["all"][$key]['info']['text'] = "Change Plan";
            }else if ($plan_name[0]["plan_id"] == "") {
                $plans["all"][$key]['info']['target'] = "";
                $plans["all"][$key]['info']['class'] = "s_upgrd";
                $plans["all"][$key]['info']['text'] = "Choose Plan";
            } else {
                $plans["all"][$key]['info']['target'] = "s_cnfrmPopup";
                $plans["all"][$key]['info']['class'] = "ChngPln";
                $plans["all"][$key]['info']['text'] = "Change Plan";
            }
        }
        return $plans;
    }
    
    public function change_s_userplan() {
        
        $login_user_data = $this->load->get_var('login_user_data');
        $plan_price = isset($_POST['plan_price']) ? $_POST['plan_price'] : '';
        $display_name = isset($_POST['display_name']) ? $_POST['display_name'] : '';
        $plan_id = isset($_POST['plan_name'])?$_POST['plan_name']:'';
        $created = strtotime($login_user_data[0]["created"]);
        $today  = strtotime(date("Y:m:d H:i:s"));
        $datediff = $today - $created;
        $day_diff = round($datediff / (60 * 60 * 24));
        $trial_end = "";
        if($day_diff < 14){
            $trial_left = 14 - $day_diff;
            $trial_end = strtotime("+".$trial_left." day");
        }
     //   echo $trial_left." - trial end";exit;
        $updateuserplan = $this->Stripe->updateuserplan($login_user_data[0]['customer_id'], $plan_id,"","",$trial_end);

            $subscription = $updateuserplan['data']['subscriptions'];
            $plandetails = $this->Stripe->retrieveoneplan($plan_id);
            //if invoice created
                $customer['plan_id'] = $plan_id;
                $customer['subscription_id'] = $subscription['data'][0]->id;
                $customer['billing_start_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_start);
                $customer['billing_end_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_end);
                $customer['plan_name'] = ($plandetails['name'])?$plandetails['name']:$display_name;
                $customer['plan_amount'] = $plan_price;
                $customer['modified'] = date("Y:m:d H:i:s");
                if ($updateuserplan[0] != 'success') {
                    $this->session->set_flashdata('message_error', $updateuserplan['1'], 5);
                    redirect(base_url() . "account/setting-view#s_subscription");
                } else {
                    $id = $this->Welcome_model->update_data("s_users_setting", $customer, array("user_id" => $login_user_data[0]["id"]));
                    if ($id) {
                        $this->session->set_flashdata('message_success', $updateuserplan['1'], 5);
                        redirect(base_url() . "account/setting-view#s_subscription");
                    }
                }
    }
    
    public function createsaaspln() {
        $result = array();
        $plan_name = isset($_POST['plan_name'])?$_POST['plan_name']:"";
        $inprogress_request = isset($_POST['in_progress_request'])?$_POST['in_progress_request']:"";
        $plan_price = isset($_POST['plan_price'])?$_POST['plan_price']:"";
        $email = isset($_POST['email'])?$_POST['email']:"";
        $discount = isset($_POST['discount'])?$_POST['discount']:"";
        $expirdate = isset($_POST['expir_date'])?$_POST['expir_date']:"";
        $cardnumber = isset($_POST['card_number'])?$_POST['card_number']:"";
        $cvc = isset($_POST['cvc'])?$_POST['cvc']:"";
        $customerid = isset($_POST['customer_id'])?$_POST['customer_id']:"";
        $s_plns = $this->S_request_model->getsaasuserplan($customerid);
       // $user_data = $this->S_admin_model->getuser_data($_POST['customer_id']);
        if (!empty($_POST)) {
            $state_tax = 0;
            $stripe_customer = $this->Stripe->createnewCustomer($email, trim($discount)); 
            if (!$stripe_customer['status']) {
                $this->session->set_flashdata('message_error', "Error occurred while upgrading plan!", 5);
                $result['error'] = $stripe_customer['message'];
                echo json_encode($result);exit;
            }

            $expir_date = explode("/", $expirdate);
            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($cardnumber);
            $expiry_month = $expir_date[0];
            $expiry_year = $expir_date[1];
            $cvc = $cvc;
            $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
            if (!$stripe_card['status']):
                $this->session->set_flashdata('message_error', $stripe_card['message'], 5);
                $result['error'] = $stripe_card['message'];
		echo json_encode($result);exit;
            endif;

            $stripe_subscription = '';
            if ($plan_name) {
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $plan_name,$inprogress_request,$state_tax);
                $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
            }

            if (!$stripe_subscription['status']) {
                $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
                $result['error'] = $stripe_subscription['message'];
		echo json_encode($result);exit;
            }
            
            $s_customer['subscription_id'] = $stripe_subscription['data']['subscription_id'];
            $s_customer['billing_start_date'] = $current_date;
            $s_customer['billing_end_date'] = $billing_expire;
            $s_customer['plan_id'] = $plan_name;
            $s_customer['plan_amount'] = $plan_price;
            $plandetails = $this->Stripe->retrieveoneplan($plan_name);
            $s_customer['plan_name'] = $plandetails['name'];
            $s_customer['modified'] = date("Y:m:d H:i:s");
            
            $customer['customer_id'] = $stripe_customer['data']['customer_id'];
            
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $customerid));
            if(!empty($s_plns)){
                $success = $this->Welcome_model->update_data("s_users_setting", $s_customer, array("user_id" => $customerid));
            }else{
                $s_customer['user_id'] = $customerid;
                $s_customer['created'] = date("Y:m:d H:i:s");
                $success = $this->Welcome_model->insert_data("s_users_setting", $s_customer);
            }
            if ($id && $success) {
                $result['success'] = "Account upgraded successfully!";
            } else {
                $result['error'] = "Error occurred while upgrading account, please try again or contact us!";
                $this->session->set_flashdata('message_error', "Error occurred while upgrading account, please try again or contact us!", 5);
                //redirect(base_url() . "account/request/design_request");
            }
        }
        echo json_encode($result);
        exit;
    }
    
    public function canseebilling_sec($seebilling="") {
        $login_user_data = $this->load->get_var('login_user_data');
        $login_user_plan = $this->load->get_var('login_user_plan');
        $agency_info = $this->load->get_var('agency_info');
         if($login_user_data[0]['role'] == 'customer' && $login_user_data[0]['is_saas'] != 1){
            if(($login_user_plan[0]["payment_mode"] == 0 && $agency_info[0]['show_billing'] == 1) || $login_user_plan[0]["payment_mode"] != 0){
                $canseebilling = 1;
            }else{
                $canseebilling = $seebilling;
            }    
        }else{
                $canseebilling = $seebilling;
        }
        return $canseebilling;
    }

     

}
