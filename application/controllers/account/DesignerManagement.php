<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DesignerManagement extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('account/S_myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('Category_model');
        $this->load->model('Clients_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('account/S_admin_model');
        $this->load->model('account/S_users_model');
        $this->load->model('account/S_request_model');
        $this->load->library('Stripeauth');
        $this->load->library('account/S_customfunctions');
        $this->load->model('Stripe');
        $this->load->library('zip');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_SENDER', $config_email['sender']);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
    }
    
    public function index($id=""){ 
       $alldesigners = $this->S_users_model->get_Susers('designer');
     // echo "<pre/>";print_R($alldesigners);
       $this->load->view('account/customer_header_1');
       $this->load->view('account/agency_user/designer_management',array('designerdata' => $alldesigners)); 
       $this->load->view('account/footer_customer');
    }
    
    public function add_designer($from){
        $output = array();
        $login_user_id = $this->load->get_var('main_user');
        $parent_user_data = $this->load->get_var('parent_user_data');
        $userdata = $this->load->get_var('login_user_data');
        $pass = isset($_POST['password'])?$_POST['password']:"";
        $updateid  = isset($_POST['cid'])?$_POST['cid']:"";

        $email = isset($_POST['email']) ? $_POST['email']:"";
        $checkemail = $this->S_request_model->getuserbyemail($email);
        $subdomain_url = $this->s_myfunctions->dynamic_urlforsubuser_inemail($login_user_id,$parent_user_data[0]['plan_name'],'login');
        if (!empty($checkemail) && $from =="save") {
           $output['msg'] = 'Email Address is already available.!';
           $output['status'] = 0;
           echo json_encode($output);exit;
        } 
        
        
        if($from == "save"){
            $output = $this->s_customfunctions->addteammembers($this->input->post());
            echo json_encode($output);exit;

    }else{
        $designer = array("parent_id" => $login_user_id,
            "first_name" => isset($_POST['first_name'])?$_POST['first_name']:"",
            "last_name" => isset($_POST['last_name'])?$_POST['last_name']:"",
            "email" => $email,
            "phone" => isset($_POST['phone'])?$_POST['phone']:"",
            "new_password" => md5($password),
            "ip_address" => $_SERVER['REMOTE_ADDR'],
            "is_active" => 1,
            "is_logged_in" => 0,
            "role" => $_POST['user_role'],
            "modified" => date("Y:m:d H:i:s")
            );

        if($_POST['user_role'] == "manager"){  
                $user_permission = array(
                    'customer_id' => $this->input->post('cid'),
                    'add_requests' => $this->customfunctions->checkInputCheCkbox($this->input->post('add_requests')),
                    'add_brand_pro' => $this->customfunctions->checkInputCheCkbox($this->input->post('add_brand_pro')),
                    'view_only' => $this->customfunctions->checkInputCheCkbox($this->input->post('view_only')),
                    'delete_req' => $this->customfunctions->checkInputCheCkbox($this->input->post('del_requests')),
                    'comment_on_req' => $this->customfunctions->checkInputCheCkbox($this->input->post('comnt_requests')),
                    'billing_module' => $this->customfunctions->checkInputCheCkbox($this->input->post('billing_module')),
                    'white_label' => $this->customfunctions->checkInputCheCkbox($this->input->post('white_label')),
                    'file_management' => $this->customfunctions->checkInputCheCkbox($this->input->post('file_management')),
                    'show_all_project' => $this->customfunctions->checkInputCheCkbox($this->input->post('show_all_project')),
                    'approve_reject' => $this->customfunctions->checkInputCheCkbox($this->input->post('approve_reject')),
                    'upload_draft' => $this->customfunctions->checkInputCheCkbox($this->input->post('upload_draft')),
                    'assign_designer_to_client' => $this->customfunctions->checkInputCheCkbox($this->input->post('assign_designer_to_client')),
                    'assign_designer_to_project' => $this->customfunctions->checkInputCheCkbox($this->input->post('assign_designer_to_project')),
                    'approve_revise' => $this->customfunctions->checkInputCheCkbox($this->input->post('approve_revise')),
                    'add_team_member' => $this->customfunctions->checkInputCheCkbox($this->input->post('add_team_member')),
                    'manage_clients' => $this->customfunctions->checkInputCheCkbox($this->input->post('manage_clients')),
                    'change_project_status' => $this->customfunctions->checkInputCheCkbox($this->input->post('change_project_status')),
                    'review_design' => $this->customfunctions->checkInputCheCkbox($this->input->post('review_design')),
                    'download_file' => $this->customfunctions->checkInputCheCkbox($this->input->post('download_file')),
                    'brand_profile_access' => $this->customfunctions->checkInputCheCkbox($this->input->post('brand_profile_access')),
                    'modified' => date("Y:m:d H:i:s")
                     
                );
                   $p_id = $this->Welcome_model->update_data("s_user_permissions", $user_permission,array("customer_id" =>$updateid));
              
            }
        $id = $this->Welcome_model->update_data("s_users", $designer,array("id" =>$updateid));
       // update_data($table,$data,$where) 
        if($id){
                $this->session->set_flashdata('message_success', ''.$_POST['user_role'].' updated successfully!', 5);
                $output['msg'] = "".$_POST['user_role']." updated successfully!";
                $output['status'] = 1;
               echo json_encode($output);exit; 
            }else{
                $this->session->set_flashdata('message_success', ''.$_POST['user_role'].' not updated successfully!', 5);
                $output['msg'] = "".$_POST['user_role']." not updated successfully!";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
    }
    }

    public function EmailCheckerValidation(){
        $email = $this->input->post("email"); 
        $userid = $this->input->post("editid"); 
        $checkemail = $this->S_request_model->getuserbyemail($email,$userid);
        if (!empty($checkemail)) {
           echo 'false'; 
        }else{
            echo 'true';
          
        } 

    }

    

public function addclientwithsubscription($input){

        //echo "<pre>"; print_r($input); die("here"); 
        $output = array();
        
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        $userdata = $this->load->get_var('login_user_data');
        $parent_name = isset($userdata[0]['first_name']) ? $userdata[0]['first_name']:'';
        $pass = isset($input['password'])?$input['password']:"";
        $bypas_paymnt = (isset($input['f_bypass_payment']) && $input['f_bypass_payment'] == "on")?1:0;
        if(isset($input['genrate_password']) && $input['genrate_password'] == "on"){
            $genrate_password = 1;
        }
        
        if($genrate_password != 1 && $pass != ""){
            $password = $pass;
        }else{
            $password = $this->s_myfunctions->random_password(); 
        }
        $email = isset($input['email'])?$input['email']:"";
        $planname = isset($input['requests_type'])?$input['requests_type']:"";
        $subscriptions = $this->Clients_model->getsubsbaseduserbyplanid($planname,"plan_id,plan_name,global_inprogress_request,global_active_request,plan_price,plan_type,plan_type_name,shared_user_type,payment_mode",$login_user_id);


   //     $shared_user = $subscriptions[0]['shared_user_type'];
        
        $checkemail = $this->S_request_model->getuserbyemail($email);

        if (!empty($checkemail)) {
           $output['msg'] = 'Email Address is already available.!';
           $output['status'] = 0;
           return $output;exit;
        }
//        $this->Stripe->setapikey($useroptions_data[0]['stripe_api_key']); 
//        $stripe_customer = $this->Stripe->createnewCustomer($email); 
//
//        if (!$stripe_customer['status']) {
//           $output['msg'] = $stripe_customer['message'];
//           $output['status'] = 0;
//          return $output; exit; 
//        }
//        $customer_id = $stripe_customer['data']['customer_id'];
        
        $customer = array("parent_id" => $login_user_id,
            "first_name" => isset($input['first_name'])?$input['first_name']:"",
            "last_name" => isset($input['last_name'])?$input['last_name']:"",
            "email" => $email,
            "phone" => isset($input['phone'])?$input['phone']:"",
            "new_password" => md5($password),
            "ip_address" => $_SERVER['REMOTE_ADDR'],
            "plan_name" => $planname,
            "plan_amount" => $subscriptions[0]['plan_price'],
            "display_plan_name" => $subscriptions[0]['plan_name'],
            "requests_type" => $subscriptions[0]['plan_type_name'],
            "is_active" => 1,
            "is_logged_in" => 0,
            "user_flag" => "client",
            "role" => "customer",
            "modified" => date("Y:m:d H:i:s"),
            "created" => date("Y:m:d H:i:s"),
            "total_active_req" => isset($subscriptions[0]['global_active_request'])?$subscriptions[0]['global_active_request']:0,
            "total_inprogress_req" => isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0
            );
//        echo "<pre>";print_r($customer);
//        echo "<pre>";print_r($subscriptions);exit;
        if ($subscriptions[0]['payment_mode'] != 0) {
            $this->Stripe->setapikey($useroptions_data[0]['stripe_api_key']);
            $stripe_customer = $this->Stripe->createnewCustomer($email);
            if (!$stripe_customer['status']) {
                $output['msg'] = $stripe_customer['message'];
                $output['status'] = 0;
                 return $output; 
                exit;
            }
            $customer_id = $stripe_customer['data']['customer_id'];
            if ($bypas_paymnt == 0) {
                $customer = $this->notbypasspayment($customer_id, $subscriptions, $planname, $customer);
            } else {
                $customer['payment_status'] = 0;
            }
        } else {
            $customer = $this->s_customfunctions->offlinepayment_process($subscriptions, $planname, $customer);
            $customer_id = "";
        }
        $customer['customer_id'] = $customer_id;
        $subdomain_url = $this->s_myfunctions->dynamic_urlforsubuser_inemail($login_user_id,$userdata[0]['plan_name'],'login');
        //echo "<pre>"; print_r($subdomain_url); exit;
        if (empty($checkemail)) {
//              echo "<pre>"; print_r($customer); exit;
            $id = $this->Welcome_model->insert_data("s_users", $customer);
            if($id){
                $this->Stripe->setapikey(API_KEY);
                /** user permissions **/
                $role['brand_profile_access'] = 0;   
                $role['add_requests'] =  1; 
                $role['delete_req'] = 1;
                // $role['approve/revision_requests'] = 1;
                $role['download_file'] = 1;
                $role['comment_on_req'] = 1;
                $role['billing_module'] = 0;
                $role['add_brand_pro'] = 1;
                $role['file_management'] = 1;
                $role['white_label'] = 0;
                $role['show_all_project'] = 0;
                $role['upload_draft'] = 0;
                $role['approve_reject'] = 1;
                $role['download_file'] = 1;
                $role['assign_designer_to_client'] = 0;
                $role['assign_designer_to_project'] = 0;
                // $role['manage_priorities'] = 1;
                $role['customer_id'] = $id; 
                $role['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("s_user_permissions", $role); 
                // echo "<pre>"; print_r($role); exit; 
                /** end user permissions **/
                
                /** extra user info **/
                if($subscriptions[0]['payment_mode'] != 0){
                    $user_info['user_id'] = $id;
                    $user_info["bypass_payment"] = $bypas_paymnt;
                    $user_info['created'] = date("Y:m:d H:i:s");
                    $this->Welcome_model->insert_data("s_users_info", $user_info); 
                }
                /** end extra user info **/
                $array = array('CUSTOMER_NAME' => $customer['first_name'],
                        'CUSTOMER_EMAIL' => $customer['email'],
                        'CUSTOMER_PASSWORD' => $password,
                            'PARENT_USER' => $parent_name,
                        'LOGIN_URL' => $subdomain_url);
                $this->s_myfunctions->send_email_to_users_by_template($login_user_id, 'welcome_email_to_sub_user', $array, $customer['email']);
                
                if($genrate_password == 1){
                    $this->session->set_flashdata('message_success', ''.$input['user_role'].' created successfully! An email is sent to the address with a temporary password.', 5);
                    $output['msg'] = "User created successfully! An email is sent to the address with a temporary password.";
                    $output['status'] = 1;
                     return $output; exit;
                }else{
                    $this->session->set_flashdata('message_success', ''.$input['user_role'].' created successfully! An email is sent to the address with password.', 5);
                    $output['msg'] = "'User created successfully! An email is sent to the address with password.";
                    $output['status'] = 1;
                    return $output; exit;
                 }
            }
        }
    }


    public function notbypasspayment($customer_id,$subscriptions,$planname,$customer) {
       
        $u_data = $this->S_request_model->get_userdata_bycustomerid($customer_id);
        $expir_date = explode("/", $_POST['expir_date']);
        $expiry_month = $expir_date[0];
        $expiry_year = $expir_date[1];
        $card_number = trim($_POST['card_number']);
        $cvc = $_POST['cvc'];
        $output = array();
        
        $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
        if (!$stripe_card['status']){
           $output['msg'] = $stripe_card['message'];
           $output['status'] = 0;
           echo json_encode($output);exit;
        }
        
        if($subscriptions[0]['plan_type_name'] == 'one_time'){
            $customer['total_requests'] = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            $customer['total_active_req'] = 0;
            $customer['total_inprogress_req'] = 0; 
            $customer['billing_start_date'] = date('Y-m-d H:i:s');
            $invoice_created = $this->Stripe->create_invoice($customer_id,0,$subscriptions[0]['plan_price']);
            if($invoice_created['status'] != true){
                 $output['message'] = $invoice_created['message'];
                 $output['status'] = 0;
                 echo json_encode($output);exit;
            }
        }
        else{
            $customer['total_requests'] = 0;
            $billing_start = ($u_data[0]['billing_start_date'] != "")?strtotime($u_data[0]['billing_start_date']):"";
            $customer['total_active_req'] = isset($subscriptions[0]['global_active_request'])?$subscriptions[0]['global_active_request']:0;
            $customer['total_inprogress_req'] = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            $stripe_subscription = "";
            if ($planname) {
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($customer_id, $planname, $subscriptions[0]['global_inprogress_request'],"",$billing_start);
                if ($stripe_subscription['status'] == 0) {
                    $output['msg'] = $stripe_subscription['message'];
                    $output['status'] = 0;
                    echo json_encode($output);exit;
                }else{
                    $customer['billing_start_date'] = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                    $customer['billing_end_date'] = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
                    $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
                    $plandetails = $this->Stripe->retrieveoneplan($planname);
                    $customer['plan_interval'] = $plandetails['interval'];
                    $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                    $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
                }
            }
            
        }
        $customer["requests_type"] = $subscriptions[0]['plan_type_name'];
        $customer["plan_amount"] = $subscriptions[0]['plan_price'];
        $customer["plan_name"] = $subscriptions[0]['plan_id']; 
        $customer["display_plan_name"] = $subscriptions[0]['plan_name'];  
        return $customer;
    }


 

}
