<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subdomain404controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Myfunctions');
    }

    public function index() {
        $this->load->view('errors/html/subdomain404');
       
    }

}
