<?php
if (!function_exists('helpr')) {
        function helpr() {
                $ci = &get_instance();
                $urlopen = parse_url(current_url());
                $getuser = $ci->Request_model->getuserinfobydomain($urlopen['host']);
                    if (!empty($getuser) && !isset($_SERVER["HTTPS"]) && $getuser[0]['ssl_or_not'] == 1) {
                        redirect(str_replace("http://", "https://", current_url()));
                    }
                    
                $notifications = $ci->Request_model->get_notifications($_SESSION['user_id']);
                foreach ($notifications as $key => $value) {
                    $notifications[$key]['profile_picture'] = $ci->customfunctions->getprofileimageurl($value['profile_picture']);
                }
                $notification_number = $ci->Request_model->get_notifications_number($_SESSION['user_id']);

                $messagenotifications = $ci->Request_model->get_messagenotifications($_SESSION['user_id']);
                foreach ($messagenotifications as $key => $value) {
                    $messagenotifications[$key]['profile_picture'] = $ci->customfunctions->getprofileimageurl($value['profile_picture']);
                }

                $messagenotification_number = $ci->Request_model->get_messagenotifications_number($_SESSION['user_id']);

                $profile_data = $ci->Admin_model->getuser_data($_SESSION['user_id']);
                $profile_data[0]['profile_picture'] = $ci->customfunctions->getprofileimageurl($profile_data[0]['profile_picture']);
                $role_permissions = array();
                if($profile_data[0]["role"] == "admin"){
                    $role_permissions['remove_clients'] = $ci->myfunctions->admin_role_permissions("remove_clients");
                    $role_permissions['remove_reports'] = $ci->myfunctions->admin_role_permissions("remove_reports");
                    $role_permissions['remove_designer'] = $ci->myfunctions->admin_role_permissions("remove_designer");
                    $role_permissions['remove_qa_va'] = $ci->myfunctions->admin_role_permissions("remove_qa_va");
                    $role_permissions['remove_cancel_requests'] = $ci->myfunctions->admin_role_permissions("remove_cancel_requests");
                    $role_permissions['remove_additional_module'] = $ci->myfunctions->admin_role_permissions("remove_additional_module");
                    $role_permissions['remove_add_module_except_fdbk'] = $ci->myfunctions->admin_role_permissions("remove_add_module_except_fdbk");
                }
                return array('notifications' => $notifications, 'notification_number' => $notification_number, 'messagenotifications' => $messagenotifications, 'messagenotification_number' => $messagenotification_number, 'profile_data' => $profile_data,"role_permissions" => $role_permissions);
        }
}