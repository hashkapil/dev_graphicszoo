<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|----------------------------------- ---------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|saas
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/**************hash-new****************************************/

define('DOMAIN_NAME','localhost');
define('MAIN_DOMAIN_NAME','gz_graphicszoo');
define('HOST_NAME',$_SERVER['HTTP_HOST']); 
define('BASEURL','http://localhost/gz_graphicszoo/');
define('REQUEST_SCHEME',$_SERVER['REQUEST_SCHEME']); 
define('LAST_ASSEST_UPDATE',strtotime('2020-01-11 12:49:00'));
define('UPLOAD_FILE_SERVER','server'); //server //bucket
//define('FS_PATH','https://s3.us-east-2.amazonaws.com/graphics-zoo-ohio/');
    define('FS_PATH','http://localhost/gz_graphicszoo/'); 
 
// define('FS_PATH','https://s3.us-east-2.amazonaws.com/graphics-zoo-ohio/');
  // define('FS_PATH','http://localhost/gz_graphicszoo/'); 
 
//  define('FS_PATH','http://localhost/gz_graphicszoo/'); 
 
define('FS_PATH_SAAS','https://s3.us-east-2.amazonaws.com/graphics-zoo-ohio/'); 
define('BATCH_MESSAGE_TIMELIMIT',2);
define('LIMIT_CUSTOMER_LIST_COUNT',12); 
define('LIMIT_DESIGNER_LIST_COUNT',10);
define('LIMIT_QA_LIST_COUNT',10);
define('LIMIT_ADMIN_LIST_COUNT',20);
define('LIMIT_ALL_LIST_COUNT',20);
define('LIMIT_MESSAGE_AND_NOTIFICATION',3);
define('BASE64KEY','Gglq5B8DS0EylVTg8n4FLi1GAFhdKQ9MwckZN4LQfgM=');
define('TOTAL_REQUESTS_WITH_DEDICATED_DESIGNER','3');

/**stripe auth info**/
define('CLIENT_ID', 'ca_Feq9KZiLMta2j2RtQOerxDJU4QK8z7Ai');
define('API_KEY', 'sk_test_xhhUB83ZheRy8jrMzthlb1Ty');
define('TOKEN_URI', 'https://connect.stripe.com/oauth/token');
define('AUTHORIZE_URI', 'https://connect.stripe.com/oauth/authorize');


//define test email
define('TEST_EMAIL','hashrajpreet@gmail.com');
define('SUPPORT_EMAIL','support@graphicszoo.com');

define('PROMO_TEXT','Promo');
//for live stripe api 
define('SUBSCRIPTION_99_PLAN','plan_E9aIpVOGGrCARO');
//define('AGENCY_PLAN','plan_Em9LEdGkZiJEMA');
define('ANNUAL_AGENCY_PLAN','annual_agency');
define('ANNUAL_COMPANY_PLAN','annual_company');
define('ANNUAL_BUSINESS_PLAN','annual_business');
define('AGENCY_PLAN','agency_plans');
define('COMPANY_PLAN','company_plan');
define('SUBSCRIPTION_MONTHLY','plan_Ep18e05fgIVFcF');
define('FORTYNINE_REQUEST_PLAN','plan_49REQ');
define('UPGRADE_TO_FIVE_PLAN','plan_149REQ');
define('UPGRADE_TO_THREE_PLAN','plan_99REQ');

//define new plans in array
define('NEW_PLANS',array("plan_49REQ","plan_149REQ","plan_99REQ"));  
define('UNLIMITED_REQUESTS_PLANS',array("plan_Ep18e05fgIVFcF","BP349","M299G","M1991D","plan_Em9LEdGkZiJEMA","enterprise-plan","agency_plans","company_plan"));

//plan price for subscription
define('ANNUAL_AGENCY_PLAN_PRICE','722');
define('ANNUAL_COMPANY_PLAN_PRICE','382');
define('ANNUAL_BUSINESS_PLAN_PRICE','297');
define('AGENCY_PLAN_PRICE','849');
define('COMPANY_PLAN_PRICE','449');
define('SUBSCRIPTION_99_PLAN_PRICE','99');
define('SUBSCRIPTION_MONTHLY_PRICE','349');
define('FORTYNINE_REQUEST_PRICE','49');
define('UPGRADE_TO_FIVE_PRICE','149');
define('UPGRADE_TO_THREE_PRICE','99');

//tax for taxas state of usa
define('STATE_TEXAS',array('texas'=> 8.25,'test' => 5.3));

//inprogress and total active request
define('TOTAL_INPROGRESS_REQUEST','1');
define('TOTAL_ACTIVE_REQUEST','2');
define('FORTYNINE_REQUEST','1');
define('UPGRADE_TO_FIVE_REQUEST','5');
define('UPGRADE_TO_THREE_REQUEST','3');

//define display name for new plans
define('FORTYNINE_REQUEST_PLAN_NAME','1 active request');
define('UPGRADE_TO_THREE__PLAN_NAME','3 active requests');
define('UPGRADE_TO_FIVE__PLAN_NAME','5 active requests');

//define discount code for 349plan to single request user
define('DISCOUNT_CODE','off20');

//working time
define('WORKING_TIME_START','06:00:00');
define('WORKING_TIME_END','17:00:00');

//QA default message
define('QA_MESSAGE','The designer is currently working on your project. Send messages here to share updates about the overall project. If you want to share comments about an individual design you can use the comments section within each design draft.');


//for test stripe api 
//define('SUBSCRIPTION_99_PLAN','plan_E9Bt4b8rFYm9UI'); 
//define('AGENCY_PLAN','plan_Em9LEdGkZiJEMA');
//define('SUBSCRIPTION_MONTHLY','plan_Ep18e05fgIVFcF');

define('SUBSCRIPTION_ANNUAL','A2870G');
define('FAV_ICON_PATH','http://localhost/gz_graphicszoo/favicon.ico');

define('FS_PATH_PUBLIC',FS_PATH.'public/');

define('FS_PATH_PUBLIC_UPLOADS',FS_PATH_PUBLIC.'uploads/');
define('FS_PATH_PUBLIC_UPLOADS_PROFILE',FS_PATH_PUBLIC_UPLOADS.'profile_picture/');
//define('FS_PATH_PUBLIC_UPLOADS_PROFILE','https://graphicszoo.com/stage.graphicszoo.com/uploads/profile_picture/');
//define('FS_PATH_PUBLIC_ASSETS',FS_PATH_PUBLIC.'assets/');
//define('FS_PATH_PUBLIC_ASSETS','https://www.graphicszoo.com/public/assets/');
 define('FS_PATH_PUBLIC_ASSETS','http://localhost/gz_graphicszoo/public/assets/');
define('FS_PATH_PUBLIC_UPLOADS_BLOGS',FS_PATH_PUBLIC_UPLOADS.'blogs/');
define('FS_PATH_PUBLIC_UPLOADS_PORTFOLIOS',FS_PATH_PUBLIC_UPLOADS.'portfolios/');
define('FS_PATH_PUBLIC_UPLOADS_REQUESTS',FS_PATH_PUBLIC_UPLOADS.'requests/');
// define('FS_PATH_PUBLIC_UPLOADS_REQUESTS','http://localhost/gz_saas/public/uploads/requests/');
define('FS_PATH_PUBLIC_UPLOADS_USER_LOGO',FS_PATH_PUBLIC_UPLOADS.'user_logos/');
define('FS_PATH_PUBLIC_UPLOADS_REQUESTMAINCHATFILES',FS_PATH_PUBLIC_UPLOADS.'requestmainchat/');
define('FS_PATH_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES',FS_PATH_PUBLIC_UPLOADS.'requestdraftmainchat/');

define('SET_REQUEST_EMAIL','newrequest@graphicszoo.com');
define('SET_REQUEST_PASSWORD','abcdef12345678');

define('FS_UPLOAD_PUBLIC','public/');
define('FS_UPLOAD_PUBLIC_UPLOADS',FS_UPLOAD_PUBLIC.'uploads/');
define('FS_UPLOAD_PUBLIC_UPLOADS_PROFILE',FS_UPLOAD_PUBLIC_UPLOADS.'profile_picture/');
define('FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS',FS_UPLOAD_PUBLIC_UPLOADS.'requests/');
define('FS_UPLOAD_PUBLIC_UPLOADS_USER_LOGO',FS_UPLOAD_PUBLIC_UPLOADS.'user_logos/');
define('FS_UPLOAD_PUBLIC_UPLOADS_REQUESTMAINCHATFILES',FS_UPLOAD_PUBLIC_UPLOADS.'requestmainchat/');
define('FS_UPLOAD_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES',FS_UPLOAD_PUBLIC_UPLOADS.'requestdraftmainchat/');
define('AVG_REVISION_NUMBER','2');
define('COMMISION_TYPE','percentage');
define('COMMISION_FEES',10);
define('CANCEL_OFFER','qmvHgPnn');

/********** start arrays for chat files*************/
define('DOCARROFFICE',array('doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt', 'ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps'));
define('FONT_FILE',array('ttf', 'otf', 'woff', 'eot', 'svg'));
define('VEDIOARR',array('mp4', 'Mov'));
define('AUDIOARR',array('mp3'));
define('IMAGEARR',array('jpg', 'png', 'gif', 'jpeg', 'bmp'));
define('ZIPARR',array('zip', 'rar'));
/********** end arrays for chat files*************/

/***************For controller check files****************/
define('ALLOWED_FILE_TYPES', 
        array('doc','docx','psd','odt','ods','ai','zip','mp4','mov','pdf','jpg','png','jpeg','tiff','ttf','eps',
            'emb','art','amt','jan','emx','u??','exp','dst','dsb','10o','arx','pes','pec','emd','sew','jef',
            'jpx','jef+','hus','shv','vip','vp3','pcd','pcm','pcq','pcs','csd','xxx','xlsx','xls','ttf','svg'));

/***************For frontend document file type files****************/
define('ALLOWED_DOCFILE_TYPES', array("pdf","odt","ods","zip","mov","mp4","doc","docx","psd","rar","ai","tiff","ttf","eps",
        'emb','art','amt','jan','emx','u??','exp','dst','dsb','10o','arx','pes','pec','emd','sew','jef',
            'jpx','jef+','hus','shv','vip','vp3','pcd','pcm','pcq','pcs','csd','xxx','xlsx','cad','ppt','pptx','pps','ppsx',
            'txt','text','xls','mp3','wav','m4v','wmv','ttf'
    ));

define('ALLOWED_SAMPLE_TYPES',array('jpg','png','jpeg'));

 //taxes calculation 
define('USA_STATES',array("Alabama","Alaska","Alaska","Arizona","Arkansas","California","Colorado",
    "Connecticut","Delaware","District Of Columbia","Florida","Georgia","Hawaii","Idaho","Illinois",
    "Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi",
    "Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota",
    "Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","texas","Utah","Vermont",
    "Virginia","Washington","est Virginia","Wisconsin","Wyoming"
));

define('SUBSCRIPTION_DATA',array("2"=> array(
        "quantity"=>"2", 
        "amount"=>"84900"
    ),
    "3"=>array(
        "quantity"=>"3", 
        "amount"=>"124900"
    ),
    "4"=>array(
        "quantity"=>"4", 
        "amount"=>"164900"
    ),
    "5"=>array(
        "quantity"=>"5", 
        "amount"=>"204900"
    ),
    "6"=>array(
        "quantity"=>"6", 
        "amount"=>"244900"
    ),
    "7"=>array(
        "quantity"=>"7", 
        "amount"=>"284900"
    ),
    "8"=>array(
        "quantity"=>"8", 
        "amount"=>"324900"
    ),
    "9"=>array(
        "quantity"=>"9", 
        "amount"=>"364900"
    ),
    "10"=>array(
        "quantity"=>"10", 
        "amount"=>"404900"
    ),));

define('ANNUAL_SUBSCRIPTION_DATA',array("2"=> array(
        "quantity"=>"2", 
        "amount"=>"72200"
    ),
    "3"=>array(
        "quantity"=>"3", 
        "amount"=>"106200"
    ),
    "4"=>array(
        "quantity"=>"4", 
        "amount"=>"140200"
    ),
    "5"=>array(
        "quantity"=>"5", 
        "amount"=>"174200"
    ),
    "6"=>array(
        "quantity"=>"6", 
        "amount"=>"208200"
    ),
    "7"=>array(
        "quantity"=>"7", 
        "amount"=>"242200"
    ),
    "8"=>array(
        "quantity"=>"8", 
        "amount"=>"276700"
    ),
    "9"=>array(
        "quantity"=>"9", 
        "amount"=>"310200"
    ),
    "10"=>array(
        "quantity"=>"10", 
        "amount"=>"344200"
    ),));
 
//agency plan array
define('AGENCY_USERS',array("enterpriseplan600","plan_EBSdcn9L47Unsr","enterprise-plan","plan_Em9LEdGkZiJEMA","agency_plans","company_plan","agencyplan800","annualagencyplan800","plan_GIT9TihnOr5XAA"));
define('BUISNESS_USERS',array("M299G","M1991D","BP349"));
define('BUISNESS_WITH_WHITE_LABEL',array("company_plan"));
define('NITYNINE_USERS',array("M993D","M99S","plan_E9aIpVOGGrCARO"));
define('RESTRICT_COUPON',array("*"));
define('RESTRICT_COUPON_FOR_SPECIFIC_PLAN',array("plan_49REQ","plan_149REQ","plan_99REQ","annual_business","annual_company","annual_agency"));
define("RESTEICTED_DOMAIN",array("graphicszoo","newdesign","newplans","mailgz","dev","demo","live"));
/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*********SAAS CONSTANTS**********************/
/*******customer statuses******/
define('IN_PROGRESS','In Progress');
define('REVISION','Revision');
define('INQUEUE','In Queue');
define('REVIEW_YOUR_DESIGN','Review your design');
define('DRAFT','draft');
/********add new request default questions ids*********/
define('DESIGN_DIMENSION','1');
define('DESCRIPTION','2');
define('LET_DESIGNER_CHOOSE','3');
define('PROJECT_DELIVERABLE','4');
define('TEXT_INCLUDING','5');

define('FS_PATH_PUBLIC_SAAS',FS_PATH.'design_requests/public/');
define('FS_PATH_PUBLIC_UPLOADS_SAAS',FS_PATH_PUBLIC_SAAS.'uploads/');
define('FS_PATH_PUBLIC_UPLOADS_PROFILE_SAAS',FS_PATH_PUBLIC_UPLOADS_SAAS.'profile_picture/');
define('FS_UPLOAD_PUBLIC_SAAS','design_requests/public/');
define('FS_UPLOAD_PUBLIC_UPLOADS_SAAS',FS_UPLOAD_PUBLIC_SAAS.'uploads/');

define('FS_UPLOAD_PUBLIC_UPLOADS_PROFILE_SAAS',FS_UPLOAD_PUBLIC_UPLOADS_SAAS.'profile_picture/');
define('FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS',FS_UPLOAD_PUBLIC_UPLOADS_SAAS.'requests/');
define('FS_UPLOAD_PUBLIC_UPLOADS_USER_LOGO_SAAS',FS_UPLOAD_PUBLIC_UPLOADS_SAAS.'user_logos/');
define('FS_UPLOAD_PUBLIC_UPLOADS_REQUESTMAINCHATFILES_SAAS',FS_UPLOAD_PUBLIC_UPLOADS_SAAS.'requestmainchat/');
define('FS_UPLOAD_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES_SAAS',FS_UPLOAD_PUBLIC_UPLOADS_SAAS.'requestdraftmainchat/');

define('FS_PATH_PUBLIC_UPLOADS_REQUESTMAINCHATFILES_SAAS',FS_PATH_PUBLIC_UPLOADS_SAAS.'requestmainchat/');
define('FS_PATH_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES_SAAS',FS_PATH_PUBLIC_UPLOADS_SAAS.'requestdraftmainchat/');
define('FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS',FS_PATH_PUBLIC_UPLOADS_SAAS.'requests/');
define('FS_PATH_PUBLIC_UPLOADS_CATEGORIES',FS_PATH_PUBLIC_UPLOADS.'categories_images/');
define('FS_PATH_PUBLIC_UPLOADS_SEO',FS_PATH_PUBLIC.'uploads/Seo_material/');
