<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clients_model extends CI_Model {
    
    public function getsubscriptionbaseduser($userid="",$id="",$active="",$parameter="",$pay_mode="") {
        if($parameter != ""){
          $this->db->select($parameter); 
        }else{
          $this->db->select("*");  
        }
        if($userid != ""){
            $this->db->where("user_id", $userid);
            if($active != ""){
               $this->db->where("is_active", $active); 
            }
        }else{
         $this->db->where("id", $id);   
        }
        if($pay_mode != ""){
            $this->db->where("payment_mode", $pay_mode);
        }
        $this->db->from("agency_subscription_plan");
        $data = $this->db->get();
        // echo $this->db->last_query();exit; 
        $result = $data->result_array();
        return $result;
    }
    
    public function getsubsbaseduserbyplanid($planid,$parameter="",$user_id="") {
        if($parameter != ""){
            $this->db->select($parameter);
        }else{
            $this->db->select("*");
        }
        if($user_id != ""){
            $this->db->where("user_id", $user_id);
        }
        $this->db->where("plan_id", $planid);   
        $this->db->from("agency_subscription_plan");
        $data = $this->db->get();
       // echo $this->db->last_query();exit; 
        $result = $data->result_array();
        return $result; 
    }
    
    public function getsharedsubscription($user_id) {
        $this->db->select("id");
        $this->db->where("user_id", $user_id);   
        $this->db->where("shared_user_type", 1); 
        $this->db->from("agency_subscription_plan");
        $data = $this->db->get();
        $plans = $data->result_array();
        $result['id'] = array_column($plans,'id');
        return $result; 
    }
    
    public function count_requests_basedon_customer($customer_id){
        $this->db->where('customer_id', $customer_id);
        $this->db->where("dummy_request != 1");
        $num_rows = $this->db->count_all_results('requests');
        //echo $this->db->last_query();exit;
        return $num_rows;
    }
    
    public function get_req_not_created_bydedicated($status,$customer_id,$loginuser_id,$shared=""){
        
        if($shared == ""){
            $this->db->where_not_in('created_by', $loginuser_id);
        }else{
            $this->db->select("count(id) as count,created_by,priority");
            $this->db->where_in('created_by', $loginuser_id);
            $this->db->group_by("created_by");
        }
        $this->db->where('customer_id', $customer_id);
        $this->db->where_in('status', $status);
        $this->db->order_by("priority","ASC");
        $data = $this->db->get('requests');
        
        $result = $data->result_array();
       // echo $this->db->last_query();
        return $result;
    }
    
    public function get_sharedusers($customer_id,$shared_flag){
        $this->db->select("u.id");
        $this->db->where("u.parent_id",$customer_id);
        $this->db->where("asp.shared_user_type",$shared_flag);
        $this->db->join("agency_subscription_plan asp","asp.plan_id = u.plan_name","inner");
        $data = $this->db->get("users u");
        $result = $data->result_array();
        return $result;
    }
    public function get_priority_forshared($parameter,$status,$customer_id,$loginuser_id,$priority="") {
        $this->db->select($parameter);
        $this->db->where('customer_id', $customer_id);
        $this->db->where_in('status', $status);
        $this->db->where_in('created_by', $loginuser_id);
        if($priority != ""){
            $this->db->where('priority >', $priority);
        }
        $data = $this->db->get('requests');
//        echo $this->db->last_query();
        $result = $data->result_array();
        
            return $result;
    }
    
    public function one_time_users_project($status,$created_user,$one_timeuser,$parameter) {
        if($parameter != ""){
            $this->db->select($parameter);
        }else{
            $this->db->select("max(priority) as priority");
        }
        $this->db->where_in('status', $status);
        $this->db->where("((requests.created_by IN (".implode(',',$one_timeuser).")) or (requests.created_by = 0 and requests.created_by='".$created_user."'))");
        $data = $this->db->get('requests');
       // echo $this->db->last_query();
        $result = $data->result_array();

        return $result;
    }
    public function updatepriorityforagancyproject($created_user,$priority) {
        $this->db->where('customer_id', $created_user);
        $this->db->where('priority >=', $priority);
        $this->db->set('priority', 'priority+1', FALSE);
        $update = $this->db->update('requests');
//        echo $this->db->last_query()."<br>";
        return $update;
    }
    
    public function get_all_shareduserreq($status = array(), $customer_id, $shareduser = array()){
		$this->db->select("*");
		$this->db->where_in('status', $status);
                $this->db->where('customer_id', $customer_id);
                $this->db->where_in("created_by", $shareduser);
                $this->db->order_by("priority","ASC");
		$query = $this->db->get('requests');
		$result = $query->result_array();
		return $result;
    }
    public function updateclientusers($data,$ids) {
        $this->db->where_in('id', $ids);
        $this->db->set($data);
        $update = $this->db->update('agency_subscription_plan');
        return $update;
        
    }
}