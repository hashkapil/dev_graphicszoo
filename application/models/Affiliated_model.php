<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Affiliated_model extends CI_Model {
    
    public function IsAffiliated_keyexists($key = '',$userID = '') {
        $this->db->select("*");
        if($key){
        $this->db->where("affiliated_key = '" . $key . "'");
        }
        if($userID){
          $this->db->where("user_id = '" . $userID . "'"); 
        }
        $data = $this->db->get("affiliate_user_info");
//         echo $this->db->last_query();exit;
        $result = $data->result_array();
      
        return $result;
    }
    
     public function get_totalAffiliatedCommision($user_id,$currentdate='',$available=''){
        $this->db->select('sum(amount) as comm_earned');
        $this->db->where('user_id',$user_id);
        $this->db->where('type','credit');
        if($currentdate){
          $this->db->where('Date(created)', $currentdate);
        }
        $query = $this->db->get('affiliate_transaction_history');
        $result = $query->result_array();
        $credited_amnt = $result[0]['comm_earned'];
        
        $this->db->select('sum(amount) as comm_debited');
        $this->db->where('user_id',$user_id);
        $this->db->where('type','debit');
        if($currentdate){
          $this->db->where('Date(created)', $currentdate);
        }
        $query1 = $this->db->get('affiliate_transaction_history');
//        echo $this->db->last_query()."<br/>";
        $result_deb = $query1->result_array();
        $debited_amnt = $result_deb[0]['comm_debited'];
        if($available){
        return round(($credited_amnt - $debited_amnt),2);
        }else{
          return round(($credited_amnt + $debited_amnt),2);  
        }
    }
    
    
    public function get_monthlyAffiliatedCommision($user_id,$fromdate='',$todate='',$isavailable=""){
        $this->db->select('sum(amount) as comm_earned_amt');
        $this->db->where('user_id',$user_id);
        $this->db->where('type','credit');
        if($fromdate){
           $this->db->where('Date(created) >= ',$fromdate);
           $this->db->where('Date(created) <= ',$todate);
        }
        $query = $this->db->get('affiliate_transaction_history');
        $result = $query->result_array();
        $credited_amnt = $result[0]['comm_earned_amt'];
        $this->db->select('sum(amount) as comm_debited_amt');
        $this->db->where('user_id',$user_id);
        $this->db->where('type','debit');
        if($fromdate){
           $this->db->where('Date(created) >= ',$fromdate);
           $this->db->where('Date(created) <= ',$todate);
        }
        $query = $this->db->get('affiliate_transaction_history');
        $resultdeb = $query->result_array();
        $debited_amnt = $resultdeb[0]['comm_debited_amt'];
        if($isavailable){
        return round(($credited_amnt - $debited_amnt),2);
        }else{
          return round(($credited_amnt + $debited_amnt),2);
        }
        
    }
    
    public function paymentHistory($userID) {
        $this->db->select("u.first_name as fname,u.last_name as lname,u.email as email,ath.amount as com_amnt,ath.type,u.created as signup_date,ath.created as debited_date");
        $this->db->where("ath.user_id = '" . $userID . "'");
        $this->db->join('users as u','u.id = ath.signup_user_id','left');
        $this->db->order_by("ath.created","DESC");
        $data = $this->db->get("affiliate_transaction_history as ath");
        $result = $data->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }
    
    public function captureAffiliatedfund($signupuserID){
        $this->db->select('af.user_id,af.signup_user_id,af.type');
        $this->db->where("af.signup_user_id = '" . $signupuserID . "'");
        $this->db->join('users as u','u.id = af.user_id','left');
        $data = $this->db->get("affiliate_transaction_history as af");
//         return $this->db->last_query();
        $result = $data->result_array();
//        echo "<pre/>";print_R($result);exit;
        return $result;
    }
    
    public function getRecordsofaffiliatedsignup($uid,$currentDate="",$fromdate="",$todate=""){
        $this->db->select("id");
        $this->db->where("user_id = '" . $uid . "'");
        $this->db->where('type','credit');
        if($currentDate){
            $this->db->where('Date(created)', $currentDate); 
        }
        if($fromdate && $todate){
           $this->db->where('Date(created) >= ',$fromdate);
           $this->db->where('Date(created) <= ',$todate);
        }
        $this->db->group_by('signup_user_id');
        $data = $this->db->get("affiliate_transaction_history");
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
        return count($result);
    }
    
    public function checkAlreadyCredit($signupid,$userid){
        $this->db->select("id");
        $this->db->where("signup_user_id = '" . $signupid . "'"); 
        $this->db->where("user_id = '" . $userid . "'");
        $this->db->where("type = 'credit'");
        $data = $this->db->get("affiliate_transaction_history");
        $result = $data->result_array();
        if($result){
            return true;
        }
    }
    
    public function affiliatedusers_load_more($client_id = "", $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="",$cancelsubscriptionuser="") {
        $login_user_id = $this->load->get_var('login_user_id');
        $this->db->select("u.*");
        $this->db->from('users as u');
        $where_query = "(u.role = '" . 'customer' . "' and u.is_affiliated = '1' or u.is_affiliated = '2')";
        if($search){
            $where_query.= " AND   (
                `u`.`first_name` LIKE '%$search%' ESCAPE '!'
                OR  `u`.`last_name` LIKE '%$search%' ESCAPE '!'
                OR  `u`.`email` LIKE '%$search%' ESCAPE '!'
            )";
        }
//        $where_query.= ")";
        if($cancelsubscriptionuser != ""){
           $this->db->where('u.is_cancel_subscription', 1); 
        }else{
           $this->db->where('u.is_cancel_subscription != 1'); 
        }
        $this->db->where($where_query);
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                $this->db->order_by("created", "DESC");
            } else {
                $this->db->order_by($orderby, "DESC");
            }
        }
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return count($result); 
            }else{
                return $result;
            }
        }
    }
    
    public function Checkvisitexist($userID,$ip){
        $this->db->select("id");
        $this->db->where("user_id = '" . $userID . "'"); 
        $this->db->where("ip_address = '" . $ip . "'");
        $data = $this->db->get("affiliation_visits");
        $result = $data->result_array();
        if($result){
            return true;
        }
    }
    
    public function getaffuserinfobyuserid($user_id){
        $this->db->select("*");
        $this->db->where("user_id = '" . $user_id . "'"); 
        $data = $this->db->get("affiliate_user_info");
        $result = $data->result_array();
        return $result;
    }
}
