<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stripe extends CI_Model {	
		
	public function getallsubscriptionlist(){
		require_once APPPATH."third_party/stripe/init.php";
		try {
			$getplanlist =  \Stripe\Plan::all();
			$i=0;
			foreach ($getplanlist['data'] as $plan) {
				
				$allplan[$i]['id'] = $plan['id'];
				$allplan[$i]['amount'] = $plan['amount'];
				$allplan[$i]['interval'] = $plan['interval'];
				$allplan[$i]['name'] = $plan['name'];
                                $allplan[$i]['tiers'] = $plan['tiers'];
				$allplan[$i]['trial_period_days'] = $plan['trial_period_days'];
				$i++;
				
			}
		 }catch (Exception $ex){
			 $allplan[0]['id'] = "";
			$allplan[0]['amount'] = "";
			$allplan[0]['interval'] = "";
			$allplan[0]['name'] = "";
                        $allplan[0]['tiers'] = "";
			$allplan[0]['trial_period_days'] = "";
		 }
		// to get the monthly plan first instead of annualy
		$allplan = array_reverse($allplan);
		return $allplan;
	}
	
	public function getcurrentplan($id){

		require_once APPPATH."third_party/stripe/init.php";
		
		try{
			$currentplan =  \Stripe\Subscription::retrieve($id);
		 } catch (Exception $ex) {
            $currentplan = "";
			
			return $currentplan;
        }
		return $currentplan;
	}
	
	public function getcustomerdetail($id,$customerid=""){
		require_once APPPATH."third_party/stripe/init.php";
		$customerdata = "";
		try{    
                        if($customerid != ""){
                            $customerdata = \Stripe\Customer::retrieve($customerid);
                        }else{
			$currentplan =  \Stripe\Subscription::retrieve($id);
			$customerdata = \Stripe\Customer::retrieve($currentplan->customer);
                        }
		}catch(Exception $ex){
			return $customerdata;
		}
		return $customerdata;
	}
	
	public function getcustomerInfoUsingCustomerId($id){
		require_once APPPATH."third_party/stripe/init.php";
		$customerdata = "";
		try{
			$customerdata = \Stripe\Customer::retrieve($id);
		}catch(Exception $ex){
			return $customerdata;
		}
		return $customerdata;
	}
        
	public function getalldetailsofcustomer($id,$invoice){
          //  echo $invoice;
		require_once APPPATH."third_party/stripe/init.php";
		$customerdata = "";
                $result = array();
		try{
			$customerdata = \Stripe\Customer::retrieve($id);
                        $result['lastest_invoice'] = $customerdata->subscriptions['data'][0]['latest_invoice'];
                        if($result['lastest_invoice'] == '' || $result['lastest_invoice'] == NULL){
                            $invoice_id = $invoice;
                        }else{
                           $invoice_id = $result['lastest_invoice'];
                        }
                        $result['subscriptions'] = $customerdata->subscriptions['data'][0]['items']['data'][0]['plan']['id'];
                        $result['plan_name'] = $customerdata->subscriptions['data'][0]['items']['data'][0]['plan']['name'];
                        $result['plan_amount'] = $customerdata->subscriptions['data'][0]['items']['data'][0]['plan']['amount']/100;
                        $invoicedata = \Stripe\Invoice::retrieve($invoice_id);
                        if($invoicedata['amount_paid'] == 0){
                        $result['discount_amount'] = $invoicedata['amount_paid']/100;
                        }else{
                        $result['discount_amount'] = $invoicedata['amount_due']/100;   
                        }
                        $result['coupen_id'] = $invoicedata['discount']['coupon']['id'];
                        $result['tax_amount'] = $invoicedata['tax']/100;
                        $result['tax_percent'] = $invoicedata['tax_percent'];
                        $result['final_amount'] = $invoicedata['total']/100;
                        $coupontest = Stripe\Coupon::retrieve($result['coupen_id']);
                        if ($coupontest) {
                            $result['percent_off'] = $coupontest->percent_off;
                        }
		}catch(Exception $ex){
			return $result;
		}
		return $result;
	}
	
	public function createCustomer($customer_email) {
        //require ROOT . DS . 'vendor' . DS . 'stripe-php-4.13.0' . DS . 'init.php';
		require_once APPPATH."third_party/stripe/init.php";
		
        $status = 0;
        $message = '';
        $data = [];
        try {
            $customer_data = [];
//            $customer_data['email'] = 'abc@def.com';
            $customer_data['email'] = $customer_email;
            $customer = \Stripe\Customer::create($customer_data);
            if ($customer):
                $status = 1;
                $message = 'Customer created successfully.';
                $data['customer_id'] = $customer->id;
            else:
                $message = 'Customer cannot be created.';
            endif;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $return_array['status'] = $status;
        $return_array['message'] = $message;
        $return_array['data'] = $data;

        return $return_array;
    }
	
	public function createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc) {
		require_once APPPATH."third_party/stripe/init.php";
        $status = 0;
        $message = '';
        $data = [];

        try {

            $stripe_token_data = [
                "card" => [
                    "number" => $card_number,
                    "exp_month" => $expiry_month,
                    "exp_year" => $expiry_year,
                    "cvc" => $cvc
                ]
            ];
            $stripe_token = \Stripe\Token::create($stripe_token_data);


//          $stripe_customer_id = 'cus_AtdxOMdvcnghr5';
            $stripe_customer_id = $customer_id;
            $customer = \Stripe\Customer::retrieve($stripe_customer_id);
            $card = $customer->sources->create(array("source" => $stripe_token->id));
            if ($card):
                $status = 1;
                $message = 'Card created successfully.';
                $data['card_id'] = $card->id;
            else:
                $message = 'Card cannot be created';
            endif;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }
        $return_array['status'] = $status;
        $return_array['message'] = $message;
        $return_array['data'] = $data;

        return $return_array;
    }
	
    public function create_cutomer_subscribePlan($customer_id, $planid, $quantity="", $state_tax = null,$start_billing = "") {
        require_once APPPATH . "third_party/stripe/init.php";
       //echo $state_tax;
        $status = 0;
        $message = '';
        $data = [];

        try {
            if($start_billing != ""){
                $subscription = \Stripe\Subscription::create([
                            "customer" => $customer_id,
                            "plan" => $planid,
                            "quantity" => ($quantity != "")?$quantity:1,
                            'tax_percent' => $state_tax,
                            'billing_cycle_anchor' => $start_billing,
                            'prorate' => false
                ]);
            }else{
                $subscription = \Stripe\Subscription::create([
                            "customer" => $customer_id,
                            "plan" => $planid,
                            "quantity" => ($quantity != "")?$quantity:1,
                            'tax_percent' => $state_tax
                ]);
            }
            if ($subscription):
                $status = 1;
                $message = 'Plan subscribed successfully.';
                $data['subscription_id'] = $subscription->id;
                $data['period_start'] = $subscription->current_period_start;
                $data['period_end'] = $subscription->current_period_end;
            else:
                $message = 'Plan not subscribed.';
            endif;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $return_data['status'] = $status;
        $return_data['message'] = $message;
        $return_data['data'] = $data;

        return $return_data;
    }

    public function retrieveoneplan($id){
		require_once APPPATH."third_party/stripe/init.php";
		
		$getplanlist =  \Stripe\Plan::retrieve($id);
		return $getplanlist;
	}
	
//	public function updateuserplan($id,$planid){
//		require_once APPPATH."third_party/stripe/init.php";
//		try{
//			$currentplan =  \Stripe\Subscription::retrieve($id);
//			// echo '<pre>';
//			// print_R($currentplan->items->data[0]->plan); echo '</pre>'; exit;
//			$currentplan->plan = $planid;
//			$currentplan->save();
//		 } catch (Exception $ex) {
//            $message = $ex->getMessage();
//			$data[0] = "error";
//			$data[1] = $message;
//			return $data;
//        }
//		
//		$data[0] = "success";
//		$data[1] = "Plan Update Succsessfully !!";
//		return $data;
//	}
	public function updateuserplan($id,$planid,$quantity="",$invoice="",$trial_end=""){
         //   echo $trial_end;exit();
		require_once APPPATH."third_party/stripe/init.php";
		try{
                            $currentplan =  \Stripe\Customer::retrieve($id);
                            $currentplan->plan = $planid;
                            $currentplan->quantity = ($quantity != "")?$quantity:1;;
                            if($invoice == 0){
                                $currentplan->prorate = false;   
    //                          
                            }
                            if($trial_end != ""){
                                $currentplan->trial_end = $trial_end;
                            }
			$data = $currentplan->save();
		 } catch (Exception $ex) {
            $message = $ex->getMessage();
			$data[0] = "error";
			$data[1] = $message;
//                        $this->session->set_flashdata('message_error', $message, 5);
//                        redirect(base_url()."customer/setting-view#billing");
			return $data;
        }
		
		$data[0] = "success";
		$data[1] = "Plan Update Succsessfully !!";
                $data['data'] = $data;
		return $data;
	}
        
    public function create_invoice($id,$state_tax=NULL,$amount=NULL){
             require_once APPPATH . "third_party/stripe/init.php";
            // If Invoice is not generated
            //echo $id;
                try {
                        if($amount == NULL || $amount == ''){
                     //       echo "amount null".$amount;exit;
                        $invoicedata = \Stripe\Invoice::create([
                                "customer" => $id,
                                'tax_percent' => $state_tax
                            ]);
                        
                        }else{
                            $total_amount = $amount*100;
                          // echo "amount - '".$id."'".'<br>'.$state_tax.'<br>'.$amount.'<br>';
//                           $invoicedata = \Stripe\InvoiceItem::create([
//                            'customer' => $id,
//                            'amount' => $amount,
//                            'currency' => 'usd',
//                            'description' => 'One-time setup fee'
//                            ]);
                           $invoiceitem = \Stripe\InvoiceItem::create([
                                'amount' => $total_amount,
                                'currency' => 'usd',
                                'customer' => $id,
                               'discountable' =>  true,
                                'description' => 'Design Request',
                            ]);
                          // echo "<pre>";print_r($invoiceitem);exit;
                            $invoicedata = \Stripe\Invoice::create([
                            'customer' => $id,
                            'tax_percent' => $state_tax
                                    
                           ]);
                           // echo "<pre>";print_r($invoicedata);exit;
                           // $invoice = \Stripe\Invoice::retrieve($invoicedata->id);
                          //  $invoicedata->sendInvoice();
                           // echo "test <pre>";print_R($invoicedata);
                           // echo '<br>invoice'.$invoicedata->id;exit;
                           //$invoicedata->sendInvoice();
                       }
                            
                        if($invoicedata){
                            $invoice = \Stripe\Invoice::retrieve($invoicedata->id);
                            $invoice_finalize = $invoice->pay();
                            $invoicedata['status'] = true;
                            $invoicedata['message'] = "Create Invoice successfully";
                        }else{
                            $invoicedata['status'] = false;
                            $invoicedata['message'] = "Invoice Not Created";
                        }
                    }catch (\Stripe\Error\Base $e) {
                        // Code to do something with the $e exception object when an error occurs
                       // echo($e->getMessage());
                        $invoicedata['message'] = $e->getMessage();
                      }
                    catch (Exception $ex) {
                        // If Invoice is generated 
                        $invoices = \Stripe\Invoice::all(['customer' => $id, 'limit' => 1]);
                        $invoicedata = $invoices['data'][0];
                        $invoicedata['message'] = $e->getMessage();
                    }
                    return $invoicedata;
        }
        
    public function change_user_card($id, $params) {
        require_once APPPATH . "third_party/stripe/init.php";

        try {
            //$expiry = explode('/', $params['exp_month']);
            $expiry_month = $params['expiry-month'];
            $expiry_year = $params['expiry-year'];

            $customerdata = \Stripe\Customer::retrieve($id);
            //echo $customerdata->sources->data[0]->id;
            //echo '<pre>'; print_r($customerdata);echo $id; echo '</pre>'; exit;


            $stripe_token_data = [
                "card" => [
                    "number" => $params['card-number'],
                    "exp_month" => $expiry_month,
                    "exp_year" => $expiry_year,
                    "cvc" => $params['cvc']
                ]
            ];

            $stripe_token = \Stripe\Token::create($stripe_token_data);

            if ($customerdata->id) {
                //echo "available";

                /* 	$card = $customerdata->sources->retrieve($customerdata->sources->data[0]->id);
                  $card->exp_month = $expiry_month;
                  $card->exp_year = $expiry_year;
                  $card->save();
                 */
                $new_card = $customerdata->sources->create(array("source" => $stripe_token->id));
                $customerdata->default_source = $new_card->id;
                $customerdata->save();
                if ($customerdata):
                    $data[0] = "success";
                    $data[1] = "Billing info updated successfully.";
                    return $data;
                else:
                    $data[0] = "error";
                    $data[1] = "Error occurred while updating billing info, try again.";
                    return $data;
                    //print_r($card);
                    //exit;
                    //$message = 'Card cannot be created';
                endif;
            }else {


                //            $stripe_customer_id = 'cus_AtdxOMdvcnghr5';

                $card = $customerdata->sources->create(array("source" => $stripe_token->id));
                if ($card):
                    $data[0] = "success";
                    $data[1] = "Billing info added successfully.";
                    //$status = 1;
                    //$message = 'Billing info added successfully.';
                    $data['card_id'] = $card->id;
                    return $data;
                else:
                    $data[0] = "error";
                    $data[1] = "Error occurred while adding billing info, try again.";
                    return $data;
                    //$message = 'Error occurred while adding billing info, try again.';
                endif;
            }
            // echo '<pre>'; print_r($customerdata); echo '</pre>';  exit;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $data[0] = "error";
            $data[1] = $message;
            return $data;
        }

        //$data[0] = "success";
        //$data[1] = "Card Create Succsessfully !!";
        //return $data;
    }

    public function get_customer_invoices($customer_id){
		require_once APPPATH."third_party/stripe/init.php";
		
		try{
			$invoices = \Stripe\Invoice::all(array("limit" => 10,"customer"=>$customer_id));
			return $invoices;
			} catch (Exception $ex) {
            $message = $ex->getMessage();
			
        }
	}
	
	public function createnewCustomer($customer_email,$discountcode="") {
        require_once APPPATH."third_party/stripe/init.php";
		
        $status = 0;
        $message = '';
        $data = [];
        try {
            $customer_data = [];
//            $customer_data['email'] = 'abc@def.com';
            $customer_data['email'] = $customer_email;
			$customer_data['coupon'] = $discountcode;
            
            $customer = \Stripe\Customer::create($customer_data);
                   //   echo "<pre>"; print_r($customer); exit;
			
            if ($customer):
                $status = 1;
                $message = 'Customer created successfully.';
                $data['customer_id'] = $customer->id;
            else:
                $message = 'Customer cannot be created.';
            endif;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $return_array['status'] = $status;
        $return_array['message'] = $message;
        $return_array['data'] = $data;

        return $return_array;
    }
    
	public function checkcouponvalid_ornot($coupon) {
        require_once APPPATH . "third_party/stripe/init.php";
        $status = 0;
        $message = '';
        $data = [];
        try {
            $coupontest = Stripe\Coupon::retrieve($coupon);
            if ($coupontest) {
                $status = 1;
                $message = 'Coupon applied successfully';
                $data['id'] = $coupontest->id;
                $data['amount_off'] = $coupontest->amount_off;
                $data['name'] = $coupontest->name;
                $data['valid'] = $coupontest->valid;
                $data['percent_off'] = $coupontest->percent_off;
                $data['percent_off_precise'] = $coupontest->percent_off_precise;
                $data['duration'] = $coupontest->duration;
                $data['duration_in_months'] = $coupontest->duration_in_months;
            }
        } catch (Exception $ex) {
            //$message = $ex->getMessage();
            $message = "Invalid coupon code";
        }
        $return_array['status'] = $status;
        $return_array['message'] = $message;
        $return_array['data'] = $data;
        return $return_array;
    }
    
	public function access_connectuserwebhook(){
		require_once APPPATH."third_party/stripe/init.php";
                \Stripe\Stripe::setApiKey(API_KEY);
                $arrContextOptions = array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false));  
                //$fileContents = file_get_contents($file_name,false, stream_context_create($arrContextOptions));
                $input = file_get_contents("php://input",false, stream_context_create($arrContextOptions));
                $action = json_decode($input, true);
                $data = [];
                
                $data['start_period'] = date('Y-m-d H:i:s',$action['data']['object']['current_period_start']);
                $data['end_period'] = date('Y-m-d H:i:s',$action['data']['object']['current_period_end']);
                $data['customer_id'] = $action['data']['object']['customer'];
                $data['plan_name'] = $action['data']['object']['plan']['id'];
                $data['display_plan_name'] = $action['data']['object']['plan']['name'];
                $data['plan_quantity'] = $action['data']['object']['plan']['quantity'];
                $data['trial_period_days'] = $action['data']['object']['plan']['trial_period_days'];
                $data['interval'] = $action['data']['object']['plan']['interval'];
                $data['plan_amount'] = $action['data']['object']['plan']['amount']/100;
                $data['sub_id'] = $action['data']['object']['id'];
                $data['payment_status'] = $action['data']['object']['paid'];
                $data['amount_paid'] = $action['data']['object']['amount_paid']/100;
                $data['event_type'] = $action['type'];
                $data['previous_start_period'] = $action['data']['previous_attributes']['current_period_start'];
                $data['previous_end_period'] = $action['data']['previous_attributes']['current_period_end'];
                $data['previous_plan_name'] = $action['data']['previous_attributes']['items']['data']['0']['plan']['id'];
                $data['previous_plan_amount'] = $action['data']['previous_attributes']['items']['data']['0']['plan']['amount']/100;
                
                
            $invoice = [];   
            $invoice['event_type'] = $action['type'];
            $invoice['tax'] = $action['data']['object']['tax']/100;
            $invoice['amount'] = $action['data']['object']['amount_due']/100;
            $invoice['refund_amount'] = $action['data']['object']['amount_refunded']/100;
            $invoice['billing_reason'] = $action['data']['object']['billing_reason'];
            $invoice['customer_id'] = $action['data']['object']['customer'];
                
                ob_start();
                echo "<pre>";
                print_r($data);
                echo "</pre>";
                echo "<pre>";
                print_r($invoice);
                echo "</pre>";
                $result = ob_get_clean();
                $return_array['data'] = $data;
                $return_array['result'] = $result;
                $return_array['invoice'] = $invoice;
                return $return_array;
       
    }
	
	public function accesswebhook_event(){
		require_once APPPATH."third_party/stripe/init.php";
                \Stripe\Stripe::setApiKey("API_KEY");
                $arrContextOptions = array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false));  
                //$fileContents = file_get_contents($file_name,false, stream_context_create($arrContextOptions));
                $input = file_get_contents("php://input",false, stream_context_create($arrContextOptions));
                $action = json_decode($input, true);
                $data = [];
                
                $data['start_period'] = date('Y-m-d H:i:s',$action['data']['object']['current_period_start']);
                $data['end_period'] = date('Y-m-d H:i:s',$action['data']['object']['current_period_end']);
                $data['customer_id'] = $action['data']['object']['customer'];
                $data['plan_name'] = $action['data']['object']['plan']['id'];
                $data['display_plan_name'] = $action['data']['object']['plan']['name'];
                $data['plan_quantity'] = $action['data']['object']['plan']['quantity'];
                $data['trial_period_days'] = $action['data']['object']['plan']['trial_period_days'];
                $data['interval'] = $action['data']['object']['plan']['interval'];
                $data['plan_amount'] = $action['data']['object']['plan']['amount']/100;
                $data['sub_id'] = $action['data']['object']['id'];
                $data['payment_status'] = $action['data']['object']['paid'];
                $data['amount_paid'] = $action['data']['object']['amount_paid']/100;
                $data['event_type'] = $action['type'];
                $data['previous_start_period'] = $action['data']['previous_attributes']['current_period_start'];
                $data['previous_end_period'] = $action['data']['previous_attributes']['current_period_end'];
                $data['previous_plan_name'] = $action['data']['previous_attributes']['items']['data']['0']['plan']['id'];
                $data['previous_plan_amount'] = $action['data']['previous_attributes']['items']['data']['0']['plan']['amount']/100;
                
                
            $invoice = [];   
            $invoice['event_type'] = $action['type'];
            $invoice['tax'] = $action['data']['object']['tax']/100;
            $invoice['amount'] = $action['data']['object']['amount_due']/100;
            $invoice['refund_amount'] = $action['data']['object']['amount_refunded']/100;
            $invoice['billing_reason'] = $action['data']['object']['billing_reason'];
            $invoice['customer_id'] = $action['data']['object']['customer'];
                
                ob_start();
                echo "<pre>";
                print_r($data);
                echo "</pre>";
                echo "<pre>";
                print_r($invoice);
                echo "</pre>";
                $result = ob_get_clean();
                $return_array['data'] = $data;
                $return_array['result'] = $result;
                $return_array['invoice'] = $invoice;
                return $return_array;
       
    }
    
    public function updateCustomer($customer_id,$customer_email="",$discount="") {
        require_once APPPATH."third_party/stripe/init.php";
        $status = 0;
        $message = '';
        $data = [];
        try {
            $customer_data = [];
//            $customer_data['email'] = 'abc@def.com';
            if($discount !=  ''){
            $customer_data['coupon'] = $discount;    
            }else{
            $customer_data['email'] = $customer_email;
            }
            $customer = \Stripe\Customer::update($customer_id,$customer_data);
            if ($customer):
                $status = 1;
                $message = 'Customer updated successfully.';
                $data['customer_id'] = $customer->id;
            else:
                $message = 'Customer cannot be updated.';
            endif;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }
        $return_array['status'] = $status;
        $return_array['message'] = $message;
        $return_array['data'] = $data;
        return $return_array;
    }
    
    public function cancel_subscription($subscription_id) {
         //die($subscription_id);
         require_once APPPATH."third_party/stripe/init.php";
         try {
            $subscription = \Stripe\Subscription::retrieve($subscription_id);
            $cancel_sub = $subscription->cancel();
            if($cancel_sub){
                $result['status'] = 1;
                $result['data'] = $cancel_sub;
            }
            else{
                 $result['status'] = 0;
                 $result['msg'] = "Subscription is not cancelled";
            }
         } catch (Exception $ex) {
           $result['msg'] = $ex->getMessage();
           $result['status'] = 0;
         }
         
         return $result;
         
     }
     
    public function setapikey($api) {
        require_once APPPATH . "third_party/stripe/init.php";
        \Stripe\Stripe::setApiKey($api);
        // echo \Stripe\Stripe::getApiKey();
        return $api;
    }

    public function getallcharge($stripe_key) {
        require_once APPPATH . "third_party/stripe/init.php";
        $result = array();
        try {
            \Stripe\Stripe::setApiKey($stripe_key);
            $data = \Stripe\Charge::all();
            $result['charge'] = $data->data;
        } catch (Exception $ex) {
            $result['msg'] = $ex->getMessage();
        }
        //echo "<pre>";print_r($result);
        return $result;
    }

    public function stripe_account_info($stripe_key,$acc_id) {
         require_once APPPATH."third_party/stripe/init.php";
         $result = array();
            try {
            \Stripe\Stripe::setApiKey($stripe_key);
             $data =  \Stripe\Account::retrieve($acc_id);
             $result['account'] = $data;
            } catch (Exception $ex) {
              $result['msg'] = $ex->getMessage();
            }
            return $result;
     }
     
     public function getclientplans() {
         require_once APPPATH."third_party/stripe/init.php";
         $result = array();
         try{ 
            $plandata = \Stripe\Plan::all();
          //  echo "<pre>";print_r($plandata);
            $result['plan'] = $plandata->data;
            //echo "<pre>";print_r($result['plan']);
         } catch (Exception $ex) {
             $result['msg'] = $ex->getMessage();
         }
         return $result; 
     }
     
     public function applyCouponToCustomer($cus_id,$discountcode) {
        require_once APPPATH."third_party/stripe/init.php";
        $status = 0;
        $message = '';
        $data = [];
        try {
            $customer_data = [];
            $customer_data['coupon'] = $discountcode;
            $customer = \Stripe\Customer::update($cus_id,$customer_data);
            if ($customer):
                $status = 1;
                $message = 'Coupon applied to customer successfully.';
            else:
                $message = 'Error occurred while appling coupon. Please try again.';
            endif;
        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $return_array['status'] = $status;
        $return_array['message'] = $message;
        $return_array['data'] = $data;

        return $return_array;
    }

}
