<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account_model extends CI_Model {	
    
    /*****************start my load-more functions****************/
    public function clientlist_load_more($is_trial,$client_id = "", $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="",$isonetimeuser="",$cancelsubscriptionuser="",$va_projects="") {
        $login_user_data = $this->load->get_var('login_user_data');
        $login_user_id = $login_user_data[0]["id"];
        $show_active_clients = $this->myfunctions->admin_role_permissions("show_active_clients");
        $this->db->select("u.*");
        $this->db->from('users as u');
        //$this->db->where("u.role = '" . 'customer' . "'");
        //$this->db->where("u.parent_id = '" . '0' . "'");
        $where_query = "(u.role = '" . 'customer' . "' ";
        $where_query.= " and u.parent_id = '" . '0' . "' ";
        if($is_trial){
            if($is_trial == 'free'){
                //$this->db->where("is_trail='1'");
                $where_query.= " and is_trail='1' ";
            } elseif($is_trial == 'paid'){
               //$this->db->where("is_trail='0'");
               $where_query.= " and is_trail='0' ";
            }
        }
        if($search){
            $where_query.= " AND   (
                `u`.`first_name` LIKE '%$search%' ESCAPE '!'
                OR  `u`.`last_name` LIKE '%$search%' ESCAPE '!'
                OR  `u`.`email` LIKE '%$search%' ESCAPE '!'
            )";
            /*
            $this->db->group_start();
            $this->db->or_like('u.first_name', $search, 'both');
            $this->db->or_like('u.last_name', $search, 'both');
            $this->db->or_like('u.email', $search, 'both');
            $this->db->group_end();*/
        }
        $where_query.= ")";
        if($isonetimeuser != '' && $isonetimeuser != 0){
            $this->db->where_in('u.plan_name', NEW_PLANS);
            $this->db->where('u.is_single_request_signup', '1');
        }
        // for sub users
        if($search){
            $sub_user_search_query = "(select u2.parent_id from users as u2 where parent_id != '0' and u2.role = 'customer' and ";
            $sub_user_search_query.= " (u2.first_name like '%$search%' or u2.last_name like '%$search%'  or u2.email like '%$search%' ))"; 
            //$this->db->or_where("u.id IN ".$sub_user_search_query);
            $where_query.= " or u.id IN ".$sub_user_search_query;
        }
        if($va_projects == 1 && $login_user_data[0]["full_admin_access"] == 5){ //flag 5 for AM
            $this->db->where('u.am_id',$login_user_id); 
        }else if($va_projects == 1){
            $this->db->where('u.va_id',$login_user_id); 
        }
        if($cancelsubscriptionuser != ""){
           $this->db->where('u.is_cancel_subscription', 1); 
        }else{
           $this->db->where('u.is_cancel_subscription != 1'); 
        }
        if($show_active_clients == 1){
            $this->db->where('u.is_active',1);
        }
        $this->db->where($where_query);
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                $this->db->order_by("created", "DESC");
            } else {
                $this->db->order_by($orderby, "DESC");
            }
        }
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return count($result); 
            }else{
                return $result;
            }
        }
    }

    public function designerlist_load_more($designer_id = "", $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="",$qa_assignproject="") {
        $login_user_data = $this->load->get_var('login_user_data');
        $login_user_id = $login_user_data[0]["id"];
        $show_active_designers = $this->myfunctions->admin_role_permissions("show_active_designers");
        $this->db->select("u.*");
        $this->db->from('users as u');
       // $this->db->where('u.status', '1');
        $this->db->where("u.role = '" . 'designer' . "'");
        if($search){
            $this->db->group_start();
            $this->db->or_like('u.first_name', $search, 'both');
            $this->db->or_like('u.last_name', $search, 'both');
            $this->db->or_like('u.email', $search, 'both');
            $this->db->group_end();
        }
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                $this->db->order_by("DATE(created)", "ASC");
            } else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        if($qa_assignproject == 1 && $login_user_data[0]["full_admin_access"] == 5){ //flag 5 for AM
            $this->db->where('u.am_id',$login_user_id); 
        }else if($qa_assignproject == 1){
            $this->db->where('u.va_id',$login_user_id); 
        }
        if($show_active_designers == 1){
            $this->db->where('u.is_active',1);
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return count($result); 
            } else{
                return $result;
            }
        }
    }

    public function qavalist_load_more($role, $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="",$assign_am="") {
        $login_user_data = $this->load->get_var('login_user_data');
        $login_user_id = $login_user_data[0]["id"];
        $this->db->select("u.*");
        $this->db->from('users as u');
        //$this->db->where('u.status', '1');
        $this->db->where("u.role = '" . $role . "'");
        $this->db->where("u.full_admin_access != 1");
        if($search){
            $this->db->group_start();
            $this->db->or_like('u.first_name', $search, 'both');
            $this->db->or_like('u.last_name', $search, 'both');
            $this->db->or_like('u.email', $search, 'both');
            $this->db->group_end();
            //echo $this->db->last_query();
        }
        if($assign_am == 1 && $login_user_data[0]["full_admin_access"] == 5){ //flag 5 for AM
            $this->db->where('u.am_id',$login_user_id); 
        }else if($assign_am == 1){
            $this->db->where('u.va_id',$login_user_id); 
        }
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                $this->db->order_by("DATE(created)", "ASC");
            } else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return count($result); 
            } else{
                return $result;
            }
        }
    }
    
    public function customer_projects_load_more($status, $customer_id, $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="") {
        $this->db->select("r.*,d.first_name as designer_first_name, d.last_name as designer_last_name, d.profile_picture as profile_picture,"
                . "c.first_name as customer_first_name, c.last_name as customer_last_name,c.plan_name as current_plan_name, c.profile_picture as customer_profile_picture, c.plan_turn_around_days as plan_turn_around_days");
        $this->db->from('requests as r');
        $this->db->join('users as d', 'd.id = r.designer_id', 'LEFT');
        $this->db->join('users as c', 'c.id = r.customer_id', 'LEFT');
        $this->db->where_in('r.status', $status);
        $this->db->where("r.customer_id ='$customer_id'");
        $this->db->where('r.dummy_request', '0');
        $this->db->group_by('r.id');
        if($search){
            $this->db->group_start();
            $this->db->or_like('r.title', $search, 'both');
            $this->db->or_like('r.description', $search, 'both');
            $this->db->or_like('d.first_name', $search, 'both');
            $this->db->or_like('d.last_name', $search, 'both');
            $this->db->group_end();
        }
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                $this->db->order_by("DATE(r.created)", "ASC");
            } else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return count($result); 
            } else{
                return $result;
            }
        }
    }

    public function designer_projects_load_more($status, $designer_id, $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="") {
        $this->db->select("r.*,d.first_name as designer_first_name, d.last_name as designer_last_name, d.profile_picture as profile_picture,"
                . "c.first_name as customer_first_name, c.last_name as customer_last_name,c.plan_name as current_plan_name, c.profile_picture as customer_profile_picture, c.plan_turn_around_days as plan_turn_around_days");
        $this->db->from('requests as r');
        $this->db->join('users as d', 'd.id = r.designer_id', 'LEFT');
        $this->db->join('users as c', 'c.id = r.customer_id', 'LEFT');
        // $this->db->where_in('r.status', $status);
        $this->db->where_in('r.status_admin', $status);
        $this->db->where("r.designer_id ='$designer_id'");
        $this->db->where("r.dummy_request", "0");
        $this->db->group_by('r.id');
        if($search){
            $this->db->group_start();
            $this->db->or_like('r.title', $search, 'both');
            $this->db->or_like('r.description', $search, 'both');
            $this->db->or_like('d.first_name', $search, 'both');
            $this->db->or_like('d.last_name', $search, 'both');
            $this->db->group_end();
        }
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                $this->db->order_by("DATE(r.created)", "ASC");
            } else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        $data = $this->db->get();
        //echo $this->db->last_query();
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return count($result); 
            } else{
                return $result;
            }
        }
    }

    public function designerlistfor_qa_load_more($qa_id = "", $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="") {
        $this->db->select("u.*");
        $this->db->from('users as u');
        $this->db->where("u.role = '" . 'designer' . "'");
        $this->db->where('u.qa_id', $qa_id);
        if($search){
            $this->db->group_start();
            $this->db->or_like('u.first_name', $search, 'both');
            $this->db->or_like('u.last_name', $search, 'both');
            $this->db->or_like('u.email', $search, 'both');
            $this->db->group_end();
        }
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_QA_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                $this->db->order_by("DATE(created)", "ASC");
            } else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        $data = $this->db->get();
        //echo $this->db->last_query();
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return count($result); 
            } else{
                return $result;
            }
        }
    }

    /*****************end my load-more functions****************/		
    public function getall_designer()
	{
		$this->db->select("*");
		$this->db->where("role='designer'");
		//$this->db->where("status='1'");
		$data = $this->db->get("users");
		$result = $data->result_array();
		
		for($i=0;$i<sizeof($result);$i++){
			$user = $this->get_all_request_by_designer($result[$i]['id']);
			$result[$i]['handled'] = sizeof($user);
			
			$user = $this->get_all_active_request_by_designer($result[$i]['id']);
			$result[$i]['active_request'] = sizeof($user);
		}
		return $result;
	}	
        
	public function getall_designer_pagination($start,$limit)
	{
		$this->db->select("*");
		$this->db->where("role='designer'");
		//$this->db->where("status='1'");
		$this->db->limit($limit,$start);
		$data = $this->db->get("users");
		$result = $data->result_array();
		
		for($i=0;$i<sizeof($result);$i++){
			$user = $this->get_all_request_by_designer($result[$i]['id']);
			$result[$i]['handled'] = sizeof($user);
			
			$user = $this->get_all_active_request_by_designer($result[$i]['id']);
			$result[$i]['active_request'] = sizeof($user);
		}
		return $result;
	}
        
	public function get_exist_user($email)
	{
		$this->db->select("*");
		$this->db->where("email",$email);
		$data = $this->db->get("users");
		$result = $data->result_array();
		return $result;

	}
        
        public function getcount_requests_fordesigners($designer_id,$status = array()){
                $this->db->where('designer_id',$designer_id);
		$this->db->where_in('status', $status);
		$num_rows = $this->db->count_all_results('requests');
		return $num_rows;
        }
        
	public function getall_customer_count()
	{
		$this->db->select("*");
		$this->db->where("role='customer'");
		//$this->db->where("status='1'");
		$num_rows = $this->db->count_all_results('users');
		return $num_rows;
	}
	public function getall_customer_load_pagin($start,$limit)
	{
		$this->db->select("*");
		$this->db->where("role='customer'");
		//$this->db->where("status='1'");
		$this->db->limit($limit, $start);
		$data = $this->db->get("users");
		$result = $data->result_array();
		for($i=0;$i<sizeof($result);$i++){
			$user = $this->get_all_request_by_customer($result[$i]['id']);
			$result[$i]['no_of_request'] = sizeof($user);
			
			$user = $this->get_all_active_request_by_customer($result[$i]['id']);
			$result[$i]['active_request'] = sizeof($user);
			
			$result[$i]['designer_name'] = "";
			
			$designer_name = $this->getuserbyid($result[$i]['designer_id']);
			if(!empty($designer_name)){
				$result[$i]['designer_name'] = $designer_name[0]['first_name'];
			}
		}
		return $result;
	}
	public function getall_customer($status=null)
	{
		$this->db->select("*");
		$this->db->where("role='customer'");
			if($status == "assigned"){
				$this->db->where("designer_id!=''");
			}
			if($status == "unassigned"){
				$this->db->where("designer_id=''");
			}
		//$this->db->where("status='1'");
		$data = $this->db->get("users");
		$result = $data->result_array();
		for($i=0;$i<sizeof($result);$i++){
			$user = $this->get_all_request_by_customer($result[$i]['id']);
			$result[$i]['no_of_request'] = sizeof($user);
			
			$user = $this->get_all_active_request_by_customer($result[$i]['id']);
			$result[$i]['active_request'] = sizeof($user);
			
			$result[$i]['designer_name'] = "";
			
			$designer_name = $this->getuserbyid($result[$i]['designer_id']);
			if(!empty($designer_name)){
				$result[$i]['designer_name'] = $designer_name[0]['first_name'];
			}
		}
		return $result;
	}

	
	public function getuserbyid($id)
	{
		$this->db->select("*");
		$this->db->where("id = '".$id."'");
		$data = $this->db->get("users");
		$result = $data->result_array();
//                echo "<pre>";print_r($result);
//die();
		for($i=0;$i<sizeof($result);$i++){
			$user = $this->get_all_request_by_designer($result[$i]['id']);
			$result[$i]['handled'] = sizeof($user);
			
			$user = $this->get_all_active_request_by_designer($result[$i]['id']);
			$result[$i]['active_request'] = sizeof($user);
			
			$user = $this->get_all_pending_request_by_designer($result[$i]['id']);
			$result[$i]['pending_request'] = sizeof($user);
		}
		return $result;
	}



	public function getqa_member()
	{
		$this->db->select("*");
		$this->db->where("role = 'qa'");
		//$this->db->where("status='1'");
		$data = $this->db->get("users");
		$result = $data->result_array();
		// for($i=0;$i<sizeof($result);$i++){
		// 	$user = $this->get_all_request_by_qa($result[$i]['id']);
		// 	$result[$i]['no_of_request'] = sizeof($user);
			
		// 	$user = $this->get_all_active_request_by_qa($result[$i]['id']);
		// 	$result[$i]['active_request'] = sizeof($user);
			
		// 	$result[$i]['qa_name'] = "";
			
		// 	$designer_name = $this->getuserbyid($result[$i]['qa_id']);
		// 	if(!empty($designer_name)){
		// 		$result[$i]['qa_name'] = $designer_name[0]['first_name'];
		// 	}
		// }
		return $result;

		//return $result;
	}
/* new  code */
	// public function get_all_request_by_qa($qa_id)
	// {
	// 	$this->db->select("*");
	// 	$this->db->where("qa_id = '".$qa_id."'");
	// 	$data = $this->db->get("requests");
	// 	$result = $data->result_array();
	// 	if(empty($result)){
	// 		return array();
	// 	}
	// 	return $result;
	// }
	// public function get_all_active_request_by_qa($qa_id)
	// {
	// 	$this->db->select("*");
	// 	$this->db->where("customer_id = '".$qa_id."'");
	// 	$this->db->where("status != 'approved'");
	// 	$data = $this->db->get("requests");
	// 	$result = $data->result_array();
	// 	if(empty($result)){
	// 		return array();
	// 	}
	// 	return $result;
	// }





	public function getva_member()
	{
		$this->db->select("*");
		$this->db->where("role = 'va'");
		//$this->db->where("status='1'");
		$data = $this->db->get("users");
		$result = $data->result_array();
		return $result;
	}
	
	public function getuserbyemail($email)
	{
		$this->db->select("*");
		$this->db->where("email = '".$email."'");
		$data = $this->db->get("users");
              // echo $this->db->last_query();exit;
		$result = $data->result_array();
		for($i=0;$i<sizeof($result);$i++){
			$user = $this->get_all_request_by_designer($result[$i]['id']);
			$result[$i]['handled'] = sizeof($user);
			
			$user = $this->get_all_active_request_by_designer($result[$i]['id']);
			$result[$i]['active_request'] = sizeof($user);
			
			$user = $this->get_all_pending_request_by_designer($result[$i]['id']);
			$result[$i]['pending_request'] = sizeof($user);
		}
		return $result;
	}
	
	public function get_request_by_id($id)
	{
		$this->db->select("*");
		$this->db->where("id = '".$id."'");
		$data = $this->db->get("requests");
		$result = $data->result_array();
                //echo "<pre>";print_r($result);
		
		for($i=0;$i<sizeof($result);$i++){
			$designer = $this->getuserbyid($result[$i]['designer_id']);
			$customer = $this->getuserbyid($result[$i]['customer_id']);
			if(!empty($designer)){
				$result[$i]['designer_first_name'] = $designer[0]['first_name'];
				$result[$i]['designer_last_name'] = $designer[0]['last_name'];
			}else{
				$result[$i]['designer_first_name'] = "";
				$result[$i]['designer_last_name'] = "";
			}
			if(!empty($customer)){
				$result[$i]['customer_first_name'] = $customer[0]['first_name'];
				$result[$i]['customer_last_name'] = $customer[0]['last_name'];
			}else{
				$result[$i]['customer_first_name'] = "";
				$result[$i]['customer_last_name'] = "";
			}
			
			$result[$i]['customer_attachment'] = $this->get_attachment_files($id,"customer");
			$result[$i]['designer_attachment'] = $this->get_attachment_files($id,"designer");
			$result[$i]['admin_attachment'] = $this->get_attachment_files($id,"admin");
			
			$style_suggestion = $this->get_style_suggestion($id);
			if(empty($style_suggestion)){
				$result[$i]['additional_description'] = "";
				$result[$i]['color'] = "";
				$result[$i]['image'] = "";
			}else{
				$result[$i]['additional_description'] = $style_suggestion[0]['additional_description'];
				$result[$i]['color'] = $style_suggestion[0]['color'];
				$result[$i]['image'] = $style_suggestion[0]['image'];
			}
			
		}
		
		return $result;
	}
	
	public function get_attachment_files($request_id,$role)
	{
		$this->db->select("*");
		$this->db->where("request_id = '".$request_id."'");
		$this->db->where("user_type = '".$role."'");
		$data = $this->db->get("request_files");
		$result = $data->result_array();
		if(empty($result)){
			return array();
		}
		return $result;
	}
	
	public function get_style_suggestion($request_id)
	{
		$this->db->select("*");
		$this->db->where("request_id = '".$request_id."'");
		$data = $this->db->get("style_suggestion");
		$result = $data->result_array();
		if(empty($result)){
			return array();
		}
		return $result;
	}
	
	public function get_all_request_by_designer($designer_id)
	{
		$this->db->select("*");
		$this->db->where("designer_id = '".$designer_id."'");
		$data = $this->db->get("requests");
		$result = $data->result_array();
		if(empty($result)){
			return array();
		}
		return $result;
	}
	
	public function get_all_active_request_by_designer($designer_id)
	{
		$this->db->select("id");
		$this->db->where_in("designer_id",$designer_id);
		$this->db->where("status != 'approved'");
		$data = $this->db->get("requests");
		$result = $data->result_array();
		if(empty($result)){
			return array();
		}
		return $result;
	}
	
	public function get_all_pending_request_by_designer($designer_id)
	{
		$this->db->select("*");
		$this->db->where("designer_id = '".$designer_id."'");
		$this->db->where("status = 'pending'");
		$data = $this->db->get("requests");
		$result = $data->result_array();
		if(empty($result)){
			return array();
		}
		return $result;
	}
	
	public function get_all_request_by_customer($customer_id)
	{
		$this->db->select("*");
		$this->db->where("customer_id = '".$customer_id."'");
		$data = $this->db->get("requests");
		$result = $data->result_array();
		if(empty($result)){
			return array();
		}
		return $result;
	}
	public function get_all_active_view_request_by_customer($status=array(),$customer_id,$checkstatus = NULL)
	{	
		//echo $status."<br>";
		$this->db->where('customer_id',$customer_id);
                if($checkstatus){
                    $this->db->where_in('status_admin', $status);
                }else{
                    $this->db->where_in('status', $status);
                }
                $this->db->where('dummy_request', '0');
		$num_rows = $this->db->count_all_results('requests');
		return $num_rows;
	}
        public function get_all_active_view_request_by_customer_for_qa($status=array(), $customer_id)
	{	
		//echo $status."<br>";
		$this->db->where('customer_id',$customer_id);
		$this->db->where_in('status', $status);
                $this->db->where('dummy_request', '0');
		$num_rows = $this->db->count_all_results('requests');
                //echo $this->db->last_query();exit;
		return $num_rows;
	}
	public function get_all_active_request_by_customer($customer_id)
	{
		$this->db->select('a.*','b.plan_name as current_plan_name');
                $this->db->from('requests as a');
                $this->db->join('users as b', 'b.id = a.customer_id','LEFT');
		$this->db->where("a.customer_id = '".$customer_id."'");
		$this->db->where("a.status != 'approved'");
		$this->db->where("a.status != 'checkforapprove'");
		$this->db->where("a.status != 'draft'");
		$this->db->where("a.status != 'pendingrevision'");
                $this->db->where('a.dummy_request', '0');
//                if("status == 'active' || status == 'disapprove'"){
//		$this->db->order_by("latest_update","ASC");
//                }
                if("status == 'assign' || status == 'pending'"){
                $this->db->order_by("a.priority","ASC");
                }elseif("status == 'active' || status == 'disapprove'"){
		$this->db->order_by("a.latest_update","ASC");
                }
                $this->db->order_by("a.modified","DESC");
		$data = $this->db->get();
               // echo $this->db->last_query();
		$result = $data->result_array();
		if(empty($result)){
			return array();
		}
		return $result;
	}
	
	public function getallqa()
	{
		$this->db->select("*");
		$this->db->where("role = 'qa'");
		$data = $this->db->get("users");
		$result = $data->result_array();
		return $result;
	}
	
	public function chat_data($first,$first_id,$second,$second_id)
	{
		$this->db->select("*");
		$this->db->where($first,$first_id);
		$this->db->where($second,$second_id);
		$data = $this->db->get("room");
		$result = $data->result_array();
		if(!empty($result)){
			return $result[0]['room_id'];
		}
		$success = $this->Welcome_model->insert_data("room",array($first=>$first_id,$second=>$second_id,"room_id"=>"room_".$first_id.$second_id));
		return "room_".$first_id.$second_id;
	}
	
	public function get_userdata_from_room_id($room_no)
	{
		$this->db->select("*");
		$this->db->where("room_id='".$room_no."'");
		$data = $this->db->get("room");
		$result = $data->result_array();
		$data_user = array();
		if($result[0]['customer_id'] != ""){
			$data = $this->getuserbyid($result[0]['customer_id']);
			$data_user[$data[0]['id']] = $data[0]['first_name']." ".$data[0]['last_name'];
		}
		if($result[0]['designer_id'] != ""){
			$data = $this->getuserbyid($result[0]['designer_id']);
			$data_user[$data[0]['id']] = $data[0]['first_name']." ".$data[0]['last_name'];
		}
		if($result[0]['qa_id'] != ""){
			$data = $this->getuserbyid($result[0]['qa_id']);
			$data_user[$data[0]['id']] = $data[0]['first_name']." ".$data[0]['last_name'];
		}
		if($result[0]['va_id'] != ""){
			$data = $this->getuserbyid($result[0]['va_id']);
			$data_user[$data[0]['id']] = $data[0]['first_name']." ".$data[0]['last_name'];
		}
		return $data_user;
	}
	
	public function get_chat($room_no)
	{
		$this->db->select("*");
		$this->db->where("room_id='".$room_no."'");
		$this->db->order_by("id","ASC");
		$data = $this->db->get("room_chat");
		$result = $data->result_array();
		return $result;
	}
	
	public function get_user_for_chat($type,$user_id)
	{
		$this->db->select("*");
		$this->db->where($type,$user_id);
		$data = $this->db->get("room");
		$result = $data->result_array();
		
		for($i=0;$i<sizeof($result);$i++)
		{
			$data_user = array();
			if($result[$i]['customer_id'] != $_SESSION['user_id']){
				$data = $this->getuserbyid($result[$i]['customer_id']);
				if(!empty($data)){
					$data_user[] = $data[0]['first_name']." ".$data[0]['last_name'];
				}
			}
			if($result[$i]['designer_id'] != $_SESSION['user_id']){
				$data = $this->getuserbyid($result[$i]['designer_id']);
				if(!empty($data)){
					$data_user[] = $data[0]['first_name']." ".$data[0]['last_name'];
				}
			}
			if($result[$i]['qa_id'] != $_SESSION['user_id']){
				$data = $this->getuserbyid($result[$i]['qa_id']);
				if(!empty($data)){
					$data_user[] = $data[0]['first_name']." ".$data[0]['last_name'];
				}
			}
			if($result[$i]['va_id'] != $_SESSION['user_id']){
				$data = $this->getuserbyid($result[$i]['va_id']);
				if(!empty($data)){
					$data_user[] = $data[0]['first_name']." ".$data[0]['last_name'];
				}
			}
			
			$result[$i]['title'] = implode(",",$data_user);
		}
		return $result;
	}
	
	
	public function get_user_chat_for_designer($type,$type2,$user_id)
	{
		
		$sql = "SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE '".$type."' = ".$user_id;
		$sql = "SELECT group_concat(id) as ids FROM `users` WHERE `".$type."` = ".$user_id;
		$query = $this->db->query($sql);
		$result = $query->result_array();
		
		if($result[0]['ids'])
                {
                    $sql_query = "select * from room where ".$type."='".$user_id."' and ".$type2." in(".$result[0]['ids'].")"; 
					$query = $this->db->query($sql_query);
                    $result = $query->result_array();
                    
                }
		for($i=0;$i<sizeof($result);$i++)
		{
			$data_user = array();
			if($result[$i]['customer_id'] != $_SESSION['user_id']){
				$data = $this->getuserbyid($result[$i]['customer_id']);
				if(!empty($data)){
					$data_user[] = $data[0]['first_name']." ".$data[0]['last_name'];
				}
			}
			if($result[$i]['designer_id'] != $_SESSION['user_id']){
				$data = $this->getuserbyid($result[$i]['designer_id']);
				if(!empty($data)){
					$data_user[] = $data[0]['first_name']." ".$data[0]['last_name'];
				}
			}
			if($result[$i]['qa_id'] != $_SESSION['user_id']){
				$data = $this->getuserbyid($result[$i]['qa_id']);
				if(!empty($data)){
					$data_user[] = $data[0]['first_name']." ".$data[0]['last_name'];
				}
			}
			if($result[$i]['va_id'] != $_SESSION['user_id']){
				$data = $this->getuserbyid($result[$i]['va_id']);
				if(!empty($data)){
					$data_user[] = $data[0]['first_name']." ".$data[0]['last_name'];
				}
			}
			
			$result[$i]['title'] = implode(",",$data_user);
		}
		return $result;
	}
	
	public function get_last_message_room($room_id)
	{
		$sql = "SELECT message FROM `room_chat` WHERE room_id = '".$room_id."' order by id DESC LIMIT 1";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		if(!empty($result[0]['message'])){
			return $result[0]['message'];
		}else{
			return "";
		}
	}
	
	public function addSubscriptionPlan($table,$value)
	{
		$data = $this->db->insert($table,$value);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	public function logout() 
	{
        $this->session->sess_destroy();
	}
        
         /**************cancel subscription********/
       public function isAlreadyCancelled($user_id,$couponapplied = NULL){
            $this->db->select("*");
//            $this->db->where('created >= NOW() - INTERVAL 3 MONTH');
            if($couponapplied){
                $this->db->where('coupon_code != " "');
                $this->db->where('coupon_code = "open"');
            }else{
                $this->db->where('cancel_status = "open"');
            }
            $this->db->where('customer_id',$user_id);
            $data = $this->db->get("cancelled_subscriptions");
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
            if($result){
                return true;
            }
        }
        
        
        public function isCancelCouponApplicable(){
            $this->db->select("cs.customer_id as user_id,u.customer_id as customer_id");
            $this->db->from('cancelled_subscriptions as cs');
            $this->db->where('(cancel_status = "open" or cancel_status = "close") and coupon_code != " "');
            $this->db->join('users as u', 'u.id = cs.customer_id','LEFT');
            $data = $this->db->get();
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
//            echo "<pre/>";print_R($result);exit;
            if($result){
                return true;
            }
        }
}
