<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class S_customfunctions {

    public function __construct() {
        $CI = &get_instance();
        $CI->load->library('session');
        $CI->load->model('Admin_model', '', TRUE);
        $CI->load->model('account/S_admin_model', '', TRUE);
        $CI->load->model('Account_model', '', TRUE);
        $CI->load->model('Request_model', '', TRUE);
        $CI->load->model('account/S_request_model', '', TRUE);
        $CI->load->model('S_admin_model', '', TRUE);
        $CI->load->model('Clients_model', '', TRUE);
        $CI->load->model('Category_model', '', TRUE);
        $CI->load->library('Myfunctions');
        $CI->load->library('s3_upload');
        $agencytier_price = $this->agencytierprice($login_user_id);
        $CI->load->vars($agencytier_price);
        $billing_plans = $this->getplansofcurrentusers();
        $CI->load->vars($billing_plans);

    }

    public function getprofileimageurl($img="",$proof="") {
         $CI = &get_instance();
         $login_user_data = $CI->load->get_var('login_user_data');
            if($login_user_data[0]['is_saas']==1 && $proof != 1){
                if(trim($img) == ""){
                 $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."user-admin.png";
                }
                else{
                    if(file_get_contents(FS_PATH. FS_UPLOAD_PUBLIC_UPLOADS_PROFILE ."_thumb/". $img)){
                        $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."_thumb/".$img;
                    }else{
                        $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE.$img;
                    }
                }
            }else{
                if(trim($img) == ""){
                 $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."user-admin.png";
             }else{
                 if(file_get_contents(FS_PATH. FS_UPLOAD_PUBLIC_UPLOADS_PROFILE_SAAS ."_thumb/". $img)){
                     $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE_SAAS ."_thumb/".$img;
                 }else{
                     $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE_SAAS .$img;
                 }
             }
         }
       return $path;
    } 
    
    public function getprojectteammembers($customer_id,$designer_id="") {
        $CI = &get_instance();
        $data = array();
        $customer = $CI->S_admin_model->getuser_data($customer_id);
        $data[0]['customer_image'] = $CI->s_customfunctions->getprofileimageurl($customer[0]['profile_picture']);
        $qa = array();
        if (!empty($customer)) {
            $data[0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
            $qa = $CI->S_admin_model->getuser_data($customer[0]['qa_id']);
            $data[0]['qa_image'] = $CI->s_customfunctions->getprofileimageurl($qa[0]['profile_picture']);
            if (!empty($qa)) {
                $data[0]['qa_name'] = $qa[0]['first_name'] . " " . $qa[0]['last_name'];
            }else{
                $data[0]['qa_name'] = '';
            }
        }else{
            $data[0]['customer_name'] = '';
        }
        $designer = $CI->S_admin_model->getuser_data($designer_id);
        $data[0]['designer_image'] = $CI->s_customfunctions->getprofileimageurl($designer[0]['profile_picture']);
        if (!empty($designer)) {
            $data[0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
        } else {
            $data[0]['designer_name'] = "";
        }
//        echo "<pre/>";print_R($data);exit;
        return $data;
    }
    
    public function getfolderfilesofusers() {
        $CI = &get_instance();
        $login_user_id = $CI->load->get_var('login_user_id');
        $main_user = $CI->load->get_var('main_user');
        $userprojects  = $CI->S_file_management->getRequestfilesofUser($main_user);

        $folderstructure = array();
        foreach($userprojects as $uk => $uv){
           $folderstructure['projects'][$uk]['name'] = $uv['name'];
           $folderstructure['projects'][$uk]['request_id'] = $uv['request_id'];
        }    
        $userbrands = $CI->S_request_model->get_brand_profile_by_user_id($login_user_id);
        foreach($userbrands as $k => $v){
           $folderstructure['brands'][$k]['id'] = $v['id'];
           $folderstructure['brands'][$k]['brand_name'] = $v['brand_name'];
        }  
        $allfolders = $CI->S_file_management->getFolderStructure('0');
        foreach($allfolders['folder'] as $k => $v){
           $folderstructure['folder'][$k]['folder_id'] = $v['id'];
           $folderstructure['folder'][$k]['folder_name'] = $v['folder_name'];
           $folderstructure['folder'][$k]['folder_type'] = $v['folder_type'];
        }
        
        foreach($allfolders['folder'] as $k => $v){
            $folderstructure['main_folder'][$k]['folder_id'] = $v['id'];
           $folderstructure['main_folder'][$k]['folder_name'] = $v['folder_name'];
           $folderstructure['main_folder'][$k]['folder_type'] = $v['folder_type'];
           $subfolder = $CI->S_file_management->getSubFolderbyID($v['id']);
           foreach($subfolder as $sk => $sv){
               $folderstructure['subfolder'][$v['id']][$sk]['folder_id'] = $sv['id'];
               $folderstructure['subfolder'][$v['id']][$sk]['folder_name'] = $sv['folder_name'];
               $folderstructure['subfolder'][$v['id']][$sk]['folder_type'] = $sv['folder_type'];
               
           }
        }
       // echo "<pre>";print_r($folderstructure);exit;
        return $folderstructure;
    }
    
    public function agencytierprice() {
        $CI = &get_instance();
        $tier_price = array();
        $tier_price['monthly_plans'] = $CI->S_request_model->getactiveagencyplanid('monthly');
        $tier_price['yearly_plans'] = $CI->S_request_model->getactiveagencyplanid('yearly');
        
        $tier_price['montly_tier_price'] = $CI->S_request_model->getagencytierprice('monthly');
        $key = 1;
        $planid = $tier_price['montly_tier_price'][0]['plan_id'];
        foreach ($tier_price['montly_tier_price'] as $value) {
            if($planid != $value['plan_id']){
                $key = 1;
                $planid = $value['plan_id'];
            }else{
               $key++; 
            }
            $tier_price['subscription_data'][$planid][$key] = $value;
        }
        $tier_price['yearly_tier_price'] = $CI->S_request_model->getagencytierprice('yearly');
        $keys = 1;
        $plan_id = $tier_price['yearly_tier_price'][0]['plan_id'];
        foreach ($tier_price['yearly_tier_price'] as $value) {
            if($plan_id != $value['plan_id']){
                $keys = 1;
                $plan_id = $value['plan_id'];
            }else{
               $keys++; 
            }
            $tier_price['annual_subscription_data'][$plan_id][$keys] = $value;
        }
        
        $tier_price['all_tier_prices'] = array_merge($tier_price['subscription_data'],$tier_price['annual_subscription_data']);
        return $tier_price;
    }
    
//    public function getplansofcurrentusers() {
//    $CI = &get_instance();
//     $main_user = $CI->load->get_var('main_user');
//     $data = $CI->S_admin_model->getuser_data($main_user);
//     $billinginfo = array();
//     if (in_array($data[0]['plan_name'], NEW_PLANS)) {
//            $billinginfo['userplans'] = $CI->S_request_model->getplansforuser('request_based');
//            $billinginfo['type_ofuser'] = 'fortynine_plans';
//        } else {
//            $billinginfo['userplans'] = $CI->S_request_model->getplansforuser();
//            $billinginfo['type_ofuser'] = 'subscription_based';
//        }
//        //echo "<pre>";print_r($billinginfo['userplans']);exit;
//        return $billinginfo;
//    }
    
    public function getplansofcurrentusers() {
    $CI = &get_instance();
     $login_user_data = $CI->load->get_var('login_user_data');
     if($login_user_data[0]['parent_id'] == 0){
         $main_user = $login_user_data[0]['id'];
     }else{
         $main_user = $login_user_data[0]['parent_id'];
     }
     $data = $CI->S_admin_model->getuser_data($main_user);
     if($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client'){
         
         $billinginfo['type_ofuser'] = $login_user_data[0]['requests_type'];
         $current_plan = $CI->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name'],"plan_id,plan_name,global_inprogress_request,global_active_request,plan_price,plan_type,plan_type_name,features,payment_mode");
         $billinginfo['userplans'] = $CI->Clients_model->getsubscriptionbaseduser($main_user,"",1,"",$current_plan[0]["payment_mode"]);
         
         
         foreach($billinginfo['userplans'] as $key => $userplans){
            $pln_feature = explode("_", $userplans['features']);
            $plnfeature = "<ul>";
            if(!empty($pln_feature)){
                foreach ($pln_feature as $text) {
                    $plnfeature .= "<li>".$text."</li>";
                }
            }
           $plnfeature .= "<ul>";
           if($userplans['plan_type'] == "unlimited"){
               $billinginfo['userplans'][$key]['plan_type'] = $userplans["global_inprogress_request"]." Request"; 
           }
          $canaddclient = $this->checkclientaddornot($data,$userplans);
          $billinginfo['userplans'][$key]['features'] = $plnfeature; 
          $billinginfo['userplans'][$key]['info'] = $this->billingsubsbasedcond($current_plan,$userplans,'client',$login_user_data[0]['plan_name'],$canaddclient,$login_user_data[0]['customer_id']);
          $billinginfo['userplans'][$key]['requests'] = $canaddclient;
         }
         
     }else{
        $current_plan = $CI->S_request_model->getsubscriptionlistbyplanid($data[0]['plan_name']);
        $billinginfo = array();
        if (in_array($data[0]['plan_name'], NEW_PLANS)) {
               $billinginfo['userplans'] = $CI->S_request_model->getplansforuser('request_based');
               $billinginfo['type_ofuser'] = 'fortynine_plans';
        } else {
            $billinginfo['userplans'] = $CI->S_request_model->getplansforuser();
            $billinginfo['type_ofuser'] = 'subscription_based';
        }
        foreach($billinginfo['userplans'] as $key => $userplans){
          $billinginfo['userplans'][$key]['info'] = $this->billingsubsbasedcond($current_plan,$userplans,'main',$data[0]['plan_name']);
          if($userplans['plan_type'] == "unlimited"){
               $billinginfo['userplans'][$key]['plan_type'] = $userplans["global_inprogress_request"]." Request"; 
           }
        }
     }
        return $billinginfo;
    }
    
    public function allStatusafterUnhold($previousstatus){
        if($previousstatus){
            if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $previousstatus)){
                $status_arr = explode('_',$previousstatus);
                $i = 0;
                $keys_array=array("0"=>"status","1"=>"status_admin","2"=>"status_designer","3"=>"status_qa");
                $keys = array_keys($keys_array);
                for($i=0;$i<count($keys);$i++) {
                    $keys_array[$keys_array[$i]]=$status_arr[$i];
                    unset($keys_array[$i]);
                }
            }else{
               $keys_array = array("status"=>$previousstatus,"status_admin"=>$previousstatus,"status_designer"=>$previousstatus,"status_qa"=>$previousstatus);
            }
            return $keys_array;
    }
    }
    
//    public function getdesignerlistforassign() {
//        $CI = &get_instance();
//        $designers = array();
//        $designers['designer_list'] = $CI->S_admin_model->get_total_customer("designer");
//        
//        for ($dl = 0; $dl < count($designers['designer_list']); $dl++) {
//            $user = $CI->Account_model->get_all_active_request_by_designer($designers['designer_list'][$dl]['id']);
//            
//            $designers['designer_list'][$dl]['profile_picture'] = $this->getprofileimageurl($designers['designer_list'][$dl]['profile_picture']);
//           
//            $designers['designer_list'][$dl]['active_request'] = sizeof($user);
//        }
//        return $designers;
//        
//    }
    
    /*********samples upload**********/
    public function movefilestos3($success,$path,$sampleflag = NULL) {
        $CI = &get_instance();
        if (!is_dir($path . $success)) {
            mkdir('./'. $path . $success, 0777, TRUE);
        }
        //Move files from temp folder
        // Get array of all source files
        if($sampleflag != ''){
            $uploadPATH = $_SESSION['temp_attachfolder_name'];
        }else{
            $uploadPATH = $_SESSION['temp_folder_names'];
        }
        $uploadPATHs = str_replace("./", "", $uploadPATH);
        $files = scandir($uploadPATH);
//        echo "uploadpatj".$uploadPATHs;exit;
        // Identify directories
        $source = $uploadPATH . '/';
        $destination = './' .$path. $success . '/';

        if (UPLOAD_FILE_SERVER == 'bucket') {
//            echo "<pre/>";print_R($files);exit;
            foreach ($files as $file) {

                if (in_array($file, array(".", "..")))
                    continue;

                $staticName = base_url() . $uploadPATHs . '/' . $file;
               // echo "staticName".$staticName."<br/>";

                $config = array(
                    'upload_path' => $path . $success . '/',
                    'file_name' => $file
                );
               // echo "<pre/>";print_R($config);

                $CI->s3_upload->initialize($config);
                $is_file_uploaded = $CI->s3_upload->upload_multiple_file($staticName);
                if ($is_file_uploaded['status'] == 1) {
                    $delete[] = $source . $file;
                    unlink('./tmp/tmpfile/' . basename($staticName));
                }
            }
        } else {
            foreach ($files as $file) {
                if (in_array($file, array(".", "..")))
                    continue;
                $is_file_uploaded['filepath'] = APPPATH . $source . $file;
                if (copy($source . $file, $destination . $file)) {
                    $delete[] = $source . $file;
                }
            }
        }
//        echo "<pre>";print_r($files);exit;
        foreach ($delete as $file) {
            unlink($file);
        }
        rmdir($source);
        unset($_SESSION['temp_folder_name']);
        unset($_SESSION['temp_attachfolder_name']);
        if($sampleflag){
            return $files;
        }else{
            return $is_file_uploaded;
        }
        
    }
    
    public function checkclientaddornot($main_user,$userplans="") {
         $CI = &get_instance();
         $useroptions_data = $CI->S_request_model->getAgencyuserinfo($main_user[0]['id']);
         $sharedusers = $CI->S_admin_model->sumallsubuser_requestscount($main_user[0]['id'],1);
         $sharedusers_count = $CI->S_admin_model->sumallsubuser_requestscount($main_user[0]['id'],1,1);
         if(!isset($useroptions_data[0]['reserve_count']) || $useroptions_data[0]['reserve_count'] == -1){
             $allcount = -1;
             $reserve_slot = $main_user[0]['total_inprogress_req'] ;
         }else{
             $reserve_slot = $useroptions_data[0]['reserve_count'];
         }
         
         $dedicatedusers = $CI->S_admin_model->sumallsubuser_requestscount($main_user[0]['id'],0);
         $dedicateddesignercount = $dedicatedusers['total_inprogress_req']*$useroptions_data[0]['no_of_assign_user'];
         $reserve_count = $reserve_slot*$useroptions_data[0]['no_of_assign_user'];
         $subusers = $dedicateddesignercount+$sharedusers['total_inprogress_req'];
         $subusers_reqcount = $subusers+$reserve_count;
         $main_dedicated_designr = $main_user[0]['total_inprogress_req'];
         $main_inprogress_req = $main_user[0]['total_inprogress_req']*$useroptions_data[0]['no_of_assign_user'];
         $pending_req = $main_inprogress_req - $subusers_reqcount;
         
         $sharedratio = (int) ($sharedusers['total_inprogress_req']/$useroptions_data[0]['no_of_assign_user']);
         $shared_dedicated = $dedicatedusers['total_inprogress_req']+$sharedratio;
         $pendreserve_count = $main_dedicated_designr - $shared_dedicated;
         $f_reserve_count = ($pendreserve_count > 0)?$pendreserve_count:0;
         $clientslot =  array('main_dedicated_designr' => $main_dedicated_designr,'main_inprogress_req' => $main_inprogress_req,'addedsbrqcount' => $subusers_reqcount,'pending_req' => $pending_req,'shared_ratio' => $useroptions_data[0]['no_of_assign_user'],"reserve_count" => $reserve_slot,"dedicatedusers" => $dedicatedusers['total_inprogress_req'],"pendreserve_count" => $f_reserve_count,"allslot" => $allcount,"sharedusers" => $sharedusers_count['total_inprogress_req']);
         if(!empty($userplans)){   
           if ($userplans["shared_user_type"] == 0) {
                $inprogress_req = $userplans['global_inprogress_request'] * $useroptions_data[0]["no_of_assign_user"];
                $clientslot['msg'] = "You can't add more dedicated designer";
            } else {
                $inprogress_req = $userplans['global_inprogress_request'];
                $clientslot['msg'] = "You can't add more shared designer";
            }
            $canadd_req = $subusers_reqcount + $inprogress_req;
            if($canadd_req > $main_inprogress_req && $userplans['plan_type_name'] != 'one_time'){
               $clientslot['slot'] = 0; 
            }else{
              $clientslot['slot'] = 1;   
            }
          }
        return $clientslot;
           
    }
    
    public function billingsubsbasedcond($current_plan,$userplans,$logggeduser,$planname="",$canaddclient="",$customerid=""){
        $info = array();
        $CI = &get_instance();
        $parent_user_data = $CI->load->get_var('parent_user_data');
        if($logggeduser == 'client'){
            if ($current_plan[0]['plan_type'] == 'yearly' && $userplans['plan_type'] == 'monthly') { 
                $info['target'] = "planpermissionspopup";
                $info['class'] = "nfChngPln";
                $info['text'] = "Change Plan";
            }else if($current_plan[0]['plan_type_name'] == 'all' && $userplans['plan_type_name'] == 'one_time'){
                $info['target'] = "sbusrplnprmsnspopup";
                $info['class'] = "nfChngPln";
                $info['text'] = "Change Plan";
            }else if($canaddclient['slot'] == 0){
                $info['target'] = "cantaddsubs";
                $info['class'] = "cantaddclnt";
                $info['text'] = "Change Plan";
            }else{   
                if ($userplans['payment_mode'] == 1 && ($planname == "" || $customerid == "")) {
                    $info['target'] = "Upgrdsubuseracount";
                    $info['class'] = "upgrdclntacnt";
                    $info['text'] = "Choose Plan";
                }else if($userplans['plan_type'] != 'unlimited' && $planname == $userplans['plan_id']){
                    $info['target'] = "currentplan";
                    $info['class'] = "currentPlan f_actv_pln";
                    $info['text'] = "Current Plan";
                }else{
                    $info['target'] = "chnagesubuserpln";
                    $info['class'] = "ChngclntPln";
                    $info['text'] = " Change Plan";
                }
            }
        }else{
            if($current_plan[0]['plan_type'] == 'yearly' && $userplans['plan_type'] == 'monthly'){ 
               $info['target'] = "planpermissionspopup";
                $info['class'] = "ChngPln"; 
                $info['text'] = " Change Plan";
            }else if($current_plan[0]['plan_type'] == 'quarterly' && $userplans['plan_type'] == 'monthly'){ 
               $info['target'] = "planpermissionspopup";
                $info['class'] = "ChngPln"; 
                $info['text'] = " Change Plan";
            }else if($userplans['plan_type'] != 'unlimited' && $planname == $userplans['plan_id']){
                    $info['target'] = "currentplan";
                    $info['class'] = "currentPlan f_actv_pln";
                    $info['text'] = "Current Plan";
            }else{
                if ($parent_user_data[0]['is_cancel_subscription'] == 1 && $parent_user_data[0]['parent_id'] == 0 && $planname != "") {
                    $info['target'] = "CnfrmPopup";
                    $info['class'] = "ChngPln";
                    $info['text'] = "Choose Plan";
                }else if($planname == ""){
                    $info['target'] = "";
                    $info['class'] = "upgrd";
                    $info['text'] = "Choose Plan";
                }else{
                    $info['target'] = "CnfrmPopup";
                    $info['class'] = "ChngPln";
                    $info['text'] = "Change Plan";
                }
            }
        }
        
        return $info;
        
    }
    
    public function offlinepayment_process($subscriptions,$planname,$customer,$current_req="") {
        $start = date('Y-m-d H:i:s');
        if($subscriptions[0]['plan_type_name'] == 'one_time'){
            $post_req = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            $customer['total_requests'] = $current_req+$post_req;
            $customer['total_active_req'] = 0;
            $customer['total_inprogress_req'] = 0; 
            $customer['billing_start_date'] = $start;
        }
        else{
            $customer['total_requests'] = 0;
            $customer['total_active_req'] = isset($subscriptions[0]['global_active_request'])?$subscriptions[0]['global_active_request']:0;
            $customer['total_inprogress_req'] = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            if ($planname) {
                if($subscriptions[0]['plan_type'] == "monthly"){
                    $enddate = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($start)) . " +1 month"));
                }else{
                    $enddate = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($start)) . " + 1 year"));
                }
                    $customer['billing_start_date'] = $start;
                    $customer['billing_end_date'] = $enddate;
            }
            
        }
        $customer['plan_interval'] = $subscriptions[0]['plan_type'];
        $customer['payment_status'] = 1;
        $customer['requests_type'] = $subscriptions[0]['plan_type_name'];
        return $customer;
    }
    public function CheckIsSaasUser(){
        $Check = $CI->S_admin_model->CheckForMainORsub($_SESSION['user_id']); 
        if($Check['is_saas']==1){
            $ci = $CI->S_file_management; 
        }else{
            $ci = $CI->S_S_file_management; 
        }
        return $ci; 
      }
      
        public function add_custom_subscription($post_d) {
        $CI = &get_instance();
        $result = array();
            $login_user_id = $CI->load->get_var('main_user');
            $paymode = ($post_d['subs_payment_mode'] == "on")?1:0;
            $subs_id = $post_d['f_subs_id'];
            if (!empty($post_d['subs_plan_features'])) {
                $subs_plan_features = implode('_', $post_d['subs_plan_features']);
            }
            if ($post_d['subs_plan_name'] == "") {
                $result['status'] = 0;
                $result['msg'] = 'Please enter plan name';
                return $result;
            }
            if ($post_d['subs_plan_price'] == "") {
                $result['status'] = 0;
                $result['msg'] = 'Please enter plan price';
                return $result;
            }
            if ($post_d['subs_plan_price'] !="" && !is_numeric($post_d['subs_plan_price'])) {
                $result['status'] = 0;
                $result['msg'] = 'Please enter numeric value for price';
                return $result;
            }
            $plan_type_name = $post_d['subs_plan_type_name'];
            if($plan_type_name == "one_time"){
                $global_inprogress_request = ($post_d['total_requests'] != "")?$post_d['total_requests']:0;
                $global_active_request = 0;
                $plan_type = 'unlimited';
                $user_type = 2; //for one_time users
                if($subs_id == ""){
                    $plan_id = uniqid();
                }else{
                    $id = $CI->Clients_model->getsubscriptionbaseduser("",$subs_id,"","plan_id");
                    $plan_id = $id[0]['plan_id'];
                }
            }else{
                if ($paymode == 1) {
                    $plan_id = $post_d['subs_planid'];
                } else {
                    if($subs_id == ""){
                        $plan_id = uniqid();
                    }else{
                        $id = $CI->Clients_model->getsubscriptionbaseduser("",$subs_id,"","plan_id");
                        $plan_id = $id[0]['plan_id'];
                    }
                }
                //$plan_id = isset($_POST['subs_plan_id'])?$_POST['subs_plan_id']:'';
                $user_type = $post_d['subs_plan_user_type'];
                $global_inprogress_request = 1;
                $global_active_request = $global_inprogress_request*TOTAL_ACTIVE_REQUEST;
                $plan_type = $post_d['subs_plan_type'];
                
            }
            if($subs_id == ""){
                $isplanexist = $CI->Clients_model->getsubsbaseduserbyplanid($plan_id,"id",$login_user_id);
                if(!empty($isplanexist)){
                    $result['status'] = 0;
                    $result['msg'] = 'This stripe plan id is already exist. Please use unique ID';
                    return $result;
                }
            }
            $subscrption = array("user_id" => $login_user_id,
                "plan_id" => $plan_id,
                "plan_name" => $post_d['subs_plan_name'],
                "features" => $subs_plan_features,
                "plan_type" => $plan_type,
                "shared_user_type" => $user_type,
                "plan_type_name" => $post_d['subs_plan_type_name'],
                "plan_price" => $post_d['subs_plan_price'],
                "global_inprogress_request" => $global_inprogress_request,
                "global_active_request" => $global_active_request,
                "is_active" => ($post_d['f_subs_plan_active'] == "on")?1:0,
                "file_sharing" => ($post_d['f_file_sharing'] == "on")?1:0,
                "file_management" => ($post_d['f_file_management'] == "on")?1:0,
                "apply_coupon" => ($post_d['f_apply_coupon'] == "on")?1:0,
                "payment_mode" => $paymode);
            if($subs_id == ""){
                $subscrption["created"] = date("Y:m:d H:i:s");
                $id = $CI->Welcome_model->insert_data("agency_subscription_plan", $subscrption);
                $result['id'] = $id;
                $msg = "Added Subscription successfully";
            }else{
                $subscrption["modified"] = date("Y:m:d H:i:s");
                $id = $CI->Welcome_model->update_data("agency_subscription_plan", $subscrption,array("id" => $subs_id));
                $msg = "Edit Subscription successfully";
            }
            if($id) {
               $CI->session->set_flashdata('message_success', $msg, 5);
               $result['status'] = 1;
            }else{
                $result['status'] = 0;
                $result['msg'] = 'Something went wrong,Please Try again ..!';
            }
            
            return $result;
    }
    
    public function addteammembers($post,$flag="") {
        $CI = &get_instance();
//        echo "<pre>"; print_r($post); die("here"); 
        $total = array();
        $login_user_id = $CI->load->get_var('main_user');
        $parent_user_data = $CI->load->get_var('parent_user_data');
        $parent_user_plan = $CI->load->get_var('parent_user_plan');
        $checkemail = $CI->S_request_model->getuserbyemail($post['email']);
        $total['designer'] = $CI->S_admin_model->totalSubUserunderSAAS($login_user_id,"designer"); 
        $total['manager'] = $CI->S_admin_model->totalSubUserunderSAAS($login_user_id,"manager");
        
        if ($post['email'] == "") {
           $output['msg'] = 'Please Enter email!';
           $output['status'] = 0;
           return $output;
        }
        if ($post['first_name'] == "") {
           $output['msg'] = 'Please Enter First Name!';
           $output['status'] = 0;
           return $output;
        }
        if ($post['last_name'] == "") {
           $output['msg'] = 'Please Enter Last Name!';
           $output['status'] = 0;
           return $output;
        }
        if (!empty($checkemail) && $flag != 1) {
           $output['msg'] = 'Email Address is already available.!';
           $output['status'] = 0;
           return $output;
        } 
        $subdomain_url = $CI->s_myfunctions->dynamic_urlforsubuser_inemail($login_user_id,$parent_user_data[0]['plan_name'],'login');
        if(isset($post['genrate_password']) && $post['genrate_password'] == "on"){
                $genrate_password = 1;
            }
            if($genrate_password != 1 && $post["password"] != ""){
                $password = $post["password"];
            }else{
                $password = $CI->s_myfunctions->random_password(); 
            } 
            if($post['user_role'] == "customer"){
                $input = $post; 
                $result = $CI->customfunctions->addclientwithsubscription($input,$flag);
                return $result;
               // echo "<pre>"; print_r($result); exit; 
  //            
            } 
            $designer = array("parent_id" => $login_user_id,
                "first_name" => $post['first_name'],
                "last_name" => $post['last_name'],
                "email" => $post['email'],
                "phone" => $post['phone'],
                "new_password" => md5($password),
                "ip_address" => $_SERVER['REMOTE_ADDR'],
                "is_active" => 1,
                "is_logged_in" => 0,
                "role" => $post['user_role']
            );
        if (empty($checkemail) || (!empty($checkemail) && $flag == 1)) {
                if(!empty($checkemail)){
                   $CI->Welcome_model->update_data("s_users", $designer, array("id" => $checkemail[0]["id"])); 
                   $id = $checkemail[0]["id"];
                }else{
                    if ($post['user_role'] == "designer" || $post['user_role'] == "manager") {
                        $mangageUser = $total['manager'][0]['total_users'] + $total['designer'][0]['total_users'];
                        if ($mangageUser >= $parent_user_plan[0]['total_sub_user'] && $parent_user_plan[0]['total_sub_user'] != "-1") {
                            $output['msg'] = "You don't have access to add more " . $post['user_role'].", Please remove this section or change role.";
                            $output['status'] = 0;
                            $output['role'] = $post['user_role'];
                            return $output;
                        }
                    }
                    $designer["created"] = date("Y:m:d H:i:s");
                    $id = $CI->Welcome_model->insert_data("s_users", $designer);
                    if ($id) {
                    $array = array('CUSTOMER_NAME' => $designer['first_name'],
                        'CUSTOMER_EMAIL' => $designer['email'],
                        'CUSTOMER_PASSWORD' => $password,
                        'PARENT_USER' => $parent_user_data[0]["first_name"]." ".$parent_user_data[0]["last_name"],
                        'LOGIN_URL' => $subdomain_url);
                    $CI->s_myfunctions->send_email_to_users_by_template($login_user_id, 'welcome_email_to_sub_user', $array, $designer['email'],1);
                    }
                }
            }

        if($post['user_role'] == "manager"){  
            $user_permission = array(
                'customer_id' => $id,
                'add_requests' => $CI->customfunctions->checkInputCheCkbox($post['add_requests']),
                'add_brand_pro' => $CI->customfunctions->checkInputCheCkbox($post['add_brand_pro']),
                'view_only' => $CI->customfunctions->checkInputCheCkbox($post['view_only']),
                'delete_req' => $CI->customfunctions->checkInputCheCkbox($post['del_requests']),
                'comment_on_req' => $CI->customfunctions->checkInputCheCkbox($post['comnt_requests']),
                'billing_module' => $CI->customfunctions->checkInputCheCkbox($post['billing_module']),
                'white_label' => $CI->customfunctions->checkInputCheCkbox($post['white_label']),
                'file_management' => $CI->customfunctions->checkInputCheCkbox($post['file_management']),
                'show_all_project' => $CI->customfunctions->checkInputCheCkbox($post['show_all_project']),
                'approve_reject' => $CI->customfunctions->checkInputCheCkbox($post['approve_reject']),
                'upload_draft' => $CI->customfunctions->checkInputCheCkbox($post['upload_draft']),
                'assign_designer_to_client' => $CI->customfunctions->checkInputCheCkbox($post['assign_designer_to_client']),
                'assign_designer_to_project' => $CI->customfunctions->checkInputCheCkbox($post['assign_designer_to_project']),
                'approve_revise' => $CI->customfunctions->checkInputCheCkbox($post['approve_revise']),
                'add_team_member' => $CI->customfunctions->checkInputCheCkbox($post['add_team_member']),
                'manage_clients' => $CI->customfunctions->checkInputCheCkbox($post['manage_clients']),
                'change_project_status' => $CI->customfunctions->checkInputCheCkbox($post['change_project_status']),
                'review_design' => $CI->customfunctions->checkInputCheCkbox($post['review_design']),
                'download_file' => $CI->customfunctions->checkInputCheCkbox($post['download_file']),
                'brand_profile_access' => $CI->customfunctions->checkInputCheCkbox($post['brand_profile_access'])
                
            );
                $existpermission = $CI->S_request_model->getuserpermissions($id);
                if(!empty($existpermission)){
                    $p_id = $CI->Welcome_model->update_data("s_user_permissions", $user_permission, array("customer_id" => $id));
                }else{
                    $user_permission['created'] = date("Y:m:d H:i:s");
                    $p_id = $CI->Welcome_model->insert_data("s_user_permissions", $user_permission); 
                }
        }

         if(!empty($id) ||  !empty($p_id)){
                if($genrate_password == 1){
                  $CI->session->set_flashdata('message_success', $post['user_role'].' created successfully! An email is sent to the address with a temporary password.', 5);
                 }else{
                   $CI->session->set_flashdata('message_success', $post['user_role'].' created successfully! An email is sent to the address with password.', 5);  
                 }
                //$this->session->set_flashdata('message_success', ''.$_POST['user_role'].' created successfully!', 5);
                $output['msg'] = "".$post['user_role']." created successfully!";
                $output['status'] = 1;
                return $output;
            }else{
                $CI->session->set_flashdata('message_success', ''.$post['user_role'].' not created successfully!', 5);
                $output['msg'] = "".$post['user_role']." not created successfully!";
                $output['status'] = 0;
            }
            return $output;
            
    }
    
    public function checkInputCheCkbox($value){
        if($value =="on"){
            $return = 1;
        }else{
            $return = 0;
        }
        return $return; 
    }
    
    public function addclientwithsubscription($input,$flag=""){
        $CI = &get_instance();
        
        $output = array();
        $login_user_id = $CI->load->get_var('main_user');
        $useroptions_data = $CI->S_request_model->getAgencyuserinfo($login_user_id,'');
        $userdata = $CI->load->get_var('login_user_data');
        $parent_user_plan = $CI->load->get_var('parent_user_plan');
        $total_customer = $CI->S_admin_model->totalSubUserunderSAAS($login_user_id,"customer"); 
        $parent_name = isset($userdata[0]['first_name']) ? $userdata[0]['first_name']:'';
        $pass = isset($input['password'])?$input['password']:"";
        $bypas_paymnt = (isset($input['f_bypass_payment']) && $input['f_bypass_payment'] == "on")?1:0;
        if(isset($input['genrate_password']) && $input['genrate_password'] == "on"){
            $genrate_password = 1;
        }
        
        if($genrate_password != 1 && $pass != ""){
            $password = $pass;
        }else{
            $password = $CI->s_myfunctions->random_password(); 
        }
        $email = isset($input['email'])?$input['email']:"";
        $planname = isset($input['requests_type'])?$input['requests_type']:"";
        $subscriptions = $CI->Clients_model->getsubsbaseduserbyplanid($planname,"plan_id,plan_name,global_inprogress_request,global_active_request,plan_price,plan_type,plan_type_name,shared_user_type,payment_mode",$login_user_id);
       // echo $planname."<pre>";print_r($subscriptions);exit;

   //     $shared_user = $subscriptions[0]['shared_user_type'];
        
        $checkemail = $CI->S_request_model->getuserbyemail($email);

        if (!empty($checkemail) && $flag != 1) {
           $output['msg'] = 'Email Address is already available.!';
           $output['status'] = 0;
           return $output;
        }
//        $this->Stripe->setapikey($useroptions_data[0]['stripe_api_key']); 
//        $stripe_customer = $this->Stripe->createnewCustomer($email); 
//
//        if (!$stripe_customer['status']) {
//           $output['msg'] = $stripe_customer['message'];
//           $output['status'] = 0;
//          return $output; exit; 
//        }
//        $customer_id = $stripe_customer['data']['customer_id'];
        
        $customer = array("parent_id" => $login_user_id,
            "first_name" => isset($input['first_name'])?$input['first_name']:"",
            "last_name" => isset($input['last_name'])?$input['last_name']:"",
            "email" => $email,
            "phone" => isset($input['phone'])?$input['phone']:"",
            "new_password" => md5($password),
            "ip_address" => $_SERVER['REMOTE_ADDR'],
            "plan_name" => $planname,
            "plan_amount" => $subscriptions[0]['plan_price'],
            "display_plan_name" => $subscriptions[0]['plan_name'],
            "requests_type" => $subscriptions[0]['plan_type_name'],
            "is_active" => 1,
            "is_logged_in" => 0,
            "user_flag" => "client",
            "role" => "customer",
            "modified" => date("Y:m:d H:i:s"),
            "created" => date("Y:m:d H:i:s"),
            "total_active_req" => isset($subscriptions[0]['global_active_request'])?$subscriptions[0]['global_active_request']:0,
            "total_inprogress_req" => isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0
            );
//        echo "<pre>";print_r($customer);
//        echo "<pre>";print_r($subscriptions);exit;
        if ($subscriptions[0]['payment_mode'] != 0) {
            $CI->Stripe->setapikey($useroptions_data[0]['stripe_api_key']);
            $stripe_customer = $CI->Stripe->createnewCustomer($email);
            if (!$stripe_customer['status']) {
                $output['msg'] = $stripe_customer['message'];
                $output['status'] = 0;
                 return $output; 
            }
            $customer_id = $stripe_customer['data']['customer_id'];
            if ($bypas_paymnt == 0) {
                $customer = $CI->customfunctions->notbypasspayment($customer_id, $subscriptions, $planname, $customer);
            } else {
                $customer['payment_status'] = 0;
            }
        } else {
            $customer = $CI->customfunctions->offlinepayment_process($subscriptions, $planname, $customer);
            $customer_id = "";
        }
        $customer['customer_id'] = $customer_id;
        $subdomain_url = $CI->s_myfunctions->dynamic_urlforsubuser_inemail($login_user_id,$userdata[0]['plan_name'],'login');
        //echo "<pre>"; print_r($subdomain_url); exit;
        if (empty($checkemail) || (!empty($checkemail) && $flag == 1)) {
//              echo "<pre>"; print_r($customer); exit;
            if(!empty($checkemail)){
                   $CI->Welcome_model->update_data("s_users", $customer, array("id" => $checkemail[0]["id"])); 
                   $id = $checkemail[0]["id"];
                   $user_insert = 0;
            }else{
                   if ($input['user_role'] == "customer") {
                        $t_users = $total_customer[0]['total_users'];
                        if ($t_users >= $parent_user_plan[0]['total_client_allow'] && $parent_user_plan[0]['total_client_allow'] != "-1") {
                            $output['msg'] = "You don't have access to add more " + $input['user_role']+", Please remove this section or change role.";
                            $output['status'] = 0;
                            $output['role'] = $input['user_role'];
                            return $output;
                        }
                    }
                    $id = $CI->Welcome_model->insert_data("s_users", $customer);
                    $user_insert = 1;
            }
            if($id){
                $CI->Stripe->setapikey(API_KEY);
                /** user permissions **/
                $role['brand_profile_access'] = 0;   
                $role['add_requests'] =  1; 
                $role['delete_req'] = 1;
                // $role['approve/revision_requests'] = 1;
                $role['download_file'] = 1;
                $role['comment_on_req'] = 1;
                $role['billing_module'] = 0;
                $role['add_brand_pro'] = 1;
                $role['file_management'] = 1;
                $role['white_label'] = 0;
                $role['show_all_project'] = 0;
                $role['upload_draft'] = 0;
                $role['approve_reject'] = 1;
                $role['download_file'] = 1;
                $role['assign_designer_to_client'] = 0;
                $role['assign_designer_to_project'] = 0;
                // $role['manage_priorities'] = 1;
                
                $existpermission = $CI->S_request_model->getuserpermissions($id);
                if(!empty($existpermission)){
                    $CI->Welcome_model->update_data("s_user_permissions", $role, array("customer_id" => $id));
                }else{
                    $role['customer_id'] = $id; 
                    $role['created'] = date("Y:m:d H:i:s");
                    $CI->Welcome_model->insert_data("s_user_permissions", $role); 
                }
                // echo "<pre>"; print_r($role); exit; 
                /** end user permissions **/
                
                /** extra user info **/
                if($subscriptions[0]['payment_mode'] != 0 && $user_insert == 1){
                    $user_info['user_id'] = $id;
                    $user_info["bypass_payment"] = $bypas_paymnt;
                    $user_info['created'] = date("Y:m:d H:i:s");
                    $CI->Welcome_model->insert_data("s_users_info", $user_info); 
                }
                if($user_insert == 1){
                /** end extra user info **/
                $array = array('CUSTOMER_NAME' => $customer['first_name'],
                        'CUSTOMER_EMAIL' => $customer['email'],
                        'CUSTOMER_PASSWORD' => $password,
                            'PARENT_USER' => $parent_name,
                        'LOGIN_URL' => $subdomain_url);
                $CI->s_myfunctions->send_email_to_users_by_template($login_user_id, 'welcome_email_to_sub_user', $array, $customer['email']);
                }
                if($genrate_password == 1){
                    $CI->session->set_flashdata('message_success', ''.$input['user_role'].' created successfully! An email is sent to the address with a temporary password.', 5);
                    $output['msg'] = "User created successfully! An email is sent to the address with a temporary password.";
                    $output['status'] = 1;
                     return $output; 
                }else{
                    $CI->session->set_flashdata('message_success', ''.$input['user_role'].' created successfully! An email is sent to the address with password.', 5);
                    $output['msg'] = "'User created successfully! An email is sent to the address with password.";
                    $output['status'] = 1;
                    return $output;
                 }
                
            }
        }
    }
    
    public function notbypasspayment($customer_id,$subscriptions,$planname,$customer) {
        $CI = &get_instance();
        $u_data = $CI->Request_model->get_userdata_bycustomerid($customer_id);
        $expir_date = explode("/", $_POST['expir_date']);
        $expiry_month = $expir_date[0];
        $expiry_year = $expir_date[1];
        $card_number = trim($_POST['card_number']);
        $cvc = $_POST['cvc'];
        $output = array();
        
        $stripe_card = $CI->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
        if (!$stripe_card['status']){
           $output['msg'] = $stripe_card['message'];
           $output['status'] = 0;
           echo json_encode($output);exit;
        }
        
        if($subscriptions[0]['plan_type_name'] == 'one_time'){
            $customer['total_requests'] = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            $customer['total_active_req'] = 0;
            $customer['total_inprogress_req'] = 0; 
            $customer['billing_start_date'] = date('Y-m-d H:i:s');
            $invoice_created = $CI->Stripe->create_invoice($customer_id,0,$subscriptions[0]['plan_price']);
            if($invoice_created['status'] != true){
                 $output['message'] = $invoice_created['message'];
                 $output['status'] = 0;
                 echo json_encode($output);exit;
            }
        }
        else{
            $customer['total_requests'] = 0;
            $billing_start = ($u_data[0]['billing_start_date'] != "")?strtotime($u_data[0]['billing_start_date']):"";
            $customer['total_active_req'] = isset($subscriptions[0]['global_active_request'])?$subscriptions[0]['global_active_request']:0;
            $customer['total_inprogress_req'] = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            $stripe_subscription = "";
            if ($planname) {
                $stripe_subscription = $CI->Stripe->create_cutomer_subscribePlan($customer_id, $planname, $subscriptions[0]['global_inprogress_request'],"",$billing_start);
                if ($stripe_subscription['status'] == 0) {
                    $output['msg'] = $stripe_subscription['message'];
                    $output['status'] = 0;
                    echo json_encode($output);exit;
                }else{
                    $customer['billing_start_date'] = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                    $customer['billing_end_date'] = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
                    $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
                    $plandetails = $CI->Stripe->retrieveoneplan($planname);
                    $customer['plan_interval'] = $plandetails['interval'];
                    $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                    $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
                }
            }
            
        }
        $customer["requests_type"] = $subscriptions[0]['plan_type_name'];
        $customer["plan_amount"] = $subscriptions[0]['plan_price'];
        $customer["plan_name"] = $subscriptions[0]['plan_id']; 
        $customer["display_plan_name"] = $subscriptions[0]['plan_name'];  
        return $customer;
    }
      
    public function uploadprofilewiththumb($files) {
        $CI = &get_instance();
        $res = array();
        $user_id = $CI->load->get_var('login_user_id');
        $CheckForMainORsub = $CI->S_admin_model->CheckForMainORsub($user_id);
        if ($CheckForMainORsub['is_saas'] == 1) {
            $uploadpath = FS_UPLOAD_PUBLIC_UPLOADS_PROFILE;
        } else {
            $uploadpath = FS_UPLOAD_PUBLIC_UPLOADS_PROFILE_SAAS;
        }
        //echo $_FILES['profile']['name']; die; 
        if ($files['profile']['name'] != "") {
            $file = pathinfo($files['profile']['name']);
            $ext = strtolower(pathinfo($files['profile']['name'], PATHINFO_EXTENSION));
            $s3_file = $file['filename'] . '-' . rand(1000, 1) . '.' . $ext;
            if (!is_dir($uploadpath . '_thumb')) {
                 mkdir('./' . $uploadpath . '_thumb', 0777, TRUE);
            }
            $config = array(
                'upload_path' => $uploadpath,
                'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                'max_size' => '5000000',
                'file_name' => $s3_file
            );
            $check = $CI->s_myfunctions->CustomFileUpload($config, 'profile');
            if ($check['status'] == 1) {

                $thumb_tmp_path = $uploadpath . '_thumb/';
                $tmp = $files['profile']['tmp_name'];
                $CI->s_myfunctions->make_thumb($tmp, $thumb_tmp_path . $s3_file, '', $ext, 150);
                if (UPLOAD_FILE_SERVER == 'bucket') {
                    $staticName = base_url() . $thumb_tmp_path . $s3_file;
                    $config = array(
                        'upload_path' => $uploadpath . '_thumb/',
                        'file_name' => $s3_file
                    );
                    $CI->load->library('s3_upload');
                    $CI->s3_upload->initialize($config);
                    $uplod_thumb = $CI->s3_upload->upload_multiple_file($staticName);
                    if ($uplod_thumb['status'] == 1) {
                        $delete = $thumb_tmp_path . $s3_file;
                        unlink($delete);
                        unlink('./tmp/tmpfile/' . basename($staticName));
                        rmdir($thumb_tmp_path);
                    }
                }
                $profile_pic = array(
                    'profile_picture' => $s3_file,
                );
                if ($CheckForMainORsub['is_saas'] == 1) {
                    $update_data_result = $CI->Admin_model->designer_update_profile($profile_pic, $user_id);
                } else {
                    $update_data_result = $CI->S_admin_model->designer_update_profile($profile_pic, $user_id);
                }
                $res['status'] = 1;
                $res['msg'] = "Profile updated successfully";
            }else{
                $res['status'] = $check['status'];
                $res['msg'] = $check['msg'];
            }
            return $res;
        }
    }

}
