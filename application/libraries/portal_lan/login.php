<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Front_controller {

	public function __construct(){
		parent::__construct();

		$this->load->library('encryption');
		$this->load->model(array('db_country_m', 'db_state_m', 'ct_account_m', 'db_portal_info_m'));

		$this->load->library('email');			
	}

	public function index()
	{
		//let us see if a login code is set then log user in automatically
		$ccode=$this->input->get('ccode');
		if($ccode==$this->config->item('ccv_autologinCode'))
		{			
			//we need to have a refferer or a direct access code
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$ref=$this->agent->referrer();
				$allowed_refs=$this->config->item('ccv_autologinRefferer');
				foreach($allowed_refs as $aref){
					$pos = strpos($ref, $aref);
					if($pos!==false)
						break;
				}				
				if ($pos !== false) {				
				////***************************************************//
					$email=$this->input->get('email');
					
					$user_info_array = $this->ct_account_m->get_account_info("Login = '" . $email . "'");

					if(is_array($user_info_array) && count($user_info_array)){
						if($user_info_array['Activated'] == '1'){
							/*****************************/
							//Login session creation
							/*****************************/
							$arr_user_login_info['user_account_number'] = $user_info_array['Account_Number'];
							$arr_user_login_info['user_name'] = $user_info_array['Contact_FirstName'] . ' ' . $user_info_array['Contact_LastName'];
							$arr_user_login_info['user_email_address'] = $user_info_array['Login'];
							$arr_user_login_info['user_group_id'] = $user_info_array['Group'];
							$arr_user_login_info['user_active_status'] = $user_info_array['Activated'];
							$arr_user_login_info['user_dp'] = $user_info_array['member_facebook_photo'];

							$this->userauth->create_login_session($arr_user_login_info);
							redirect(site_url('home'));
						}
					}
				}
			}
		}
		/***********************************************************************/
		$user_profile = array();
		if($this->input->get('a') == 'fb_reg_process'){
			require_once APPPATH . 'third_party/fb-sdk/facebook.php';

			$facebook = new Facebook(array(
				'appId'  => $this->config->item('fb_app_id'),
				'secret' => $this->config->item('fb_app_secret_key')
			));

			$user = $facebook->getUser();
			if($user){
				try{
					$user_profile = $facebook->api('/'.$user);
				}catch(FacebookApiException $e){
					$user = null;
					$this->out();
				}
			}
		}

		$data['user_profile'] = $user_profile;

		$data['country_array'] = $this->db_country_m->get_countries("", FALSE);
		$data['state_array'] = $this->db_state_m->get_states("", FALSE);
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		//getting failed attempts for captcha purposes//
		$data['failed_attempts']=$this->session->userdata('failed_attempts');
		///////////////////////////////////////////////////
		$this->load->view($this->config->item('portal_folder') . '/login', $data);
	}

	public function create() {
		$data['country_array'] = $this->db_country_m->get_countries("", FALSE);
		$data['state_array'] = $this->db_state_m->get_states("", FALSE);
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$this->load->view($this->config->item('portal_folder') . '/register', $data);
	}

	public function forgot_password() {
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);
        
		$this->load->view($this->config->item('portal_folder') . '/forgot_password', $data);
	}

	public function create_captcha() {
		$this->load->library('captcha_code');

		$objCaptcha = new captcha_code();
		$objCaptcha->create_captcha();
	}

	public function fb_process($type=''){
		require_once APPPATH . 'third_party/fb-sdk/facebook.php';

		$facebook = new Facebook(array(
			'appId'  => $this->config->item('fb_app_id'),
			'secret' => $this->config->item('fb_app_secret_key')
		));

		$user = $facebook->getUser();
		$extra_params = '';

		if($user){
			try{
				$auto_login = FALSE;
				$first_login = FALSE;

				$user_profile = $facebook->api('/'.$user);
				if(is_array($user_profile) && count($user_profile)){
					$member_detail_array = $this->ct_account_m->get_account_info("Login = '" . $user_profile['email'] . "'");
					if(!count($member_detail_array)){
						if($type != 'create_user'){
							redirect(site_url('login') . '?a=fb_reg_process');
						}

						$random_password = GenerateRandom(8);
						$account_values_array = array(
							'Activated' => '1',
							'Login' => $user_profile['email'],
							'Password' => $random_password,'eBilling'=>$user_profile['email'],
							'Contact_FirstName' => $user_profile['first_name'],
							'Contact_LastName' => $user_profile['last_name'],
							'Address' => '',
							'City' => '',
							'State' => '',
							'Province' => '',
							'ZipCode' => '','Status_update'=>date("Y-m-d H:i:s"),
							'Contact_Number' => '',
							'Account_Name' => $user_profile['name'],
							'Country' => '',
							'member_facebook_id' => $user_profile['id'],
                            'IP' => get_ip(),
                            'SingupPG' => 1
						);

						//Add user to DB
						$account_number = $this->ct_account_m->add_account($account_values_array);
						$first_login = $auto_login = TRUE;

						//Add user image to DB
						get_facebook_profile_photo($account_number);

						//Email setup
						$site_name = $this->config->item('site_name');
						$email_from = $this->config->item('email_from');
						$email_from_name = $this->config->item('email_from_name');
						$site_url = $this->config->item('site_url');

						$this->email->to($email);
						$this->email->from($email_from, $email_from_name);
						$this->email->subject('Activate your account with ' . $site_name);
						
						$encryptioned_email = $this->encryption->encode($user_profile['email']);

						$msg = '<html>
							<head>
								<title>Thank you for registration with eTollfree.net</title>
							</head>
							<body>
								<p>Dear <b>' . $user_profile['first_name'] . ' ' . $user_profile['last_name'] . '</b>,</p>
								<p>Thanks for signing-up to eTollfree.net. You may login using the details below.</p>
								<p>Your account details are:</p>
								<p>E-mail Id:<b> ' . $user_profile['email'] . ' </b><br/>
								Password:<b> ' . $random_password . ' </b><br></p>
								<p>Regards,</p>
								<p>eTollfree.net Team<br/>
								<a href="' . base_url() . '" target="_blank">eTollfree.net</a></p>
							</body>
						</html>';

						$this->email->message($msg);
						$this->email->send();
					}else{
						if($member_detail_array['Activated'] == '1'){
							$auto_login = TRUE;
						}else{
							$extra_params = $this->encryption->encode($member_detail_array['email']);
						}
					}

					if($auto_login == TRUE){
						$user_info_array = $this->ct_account_m->get_account_info("Login = '" . $user_profile['email'] . "'");
						/*****************************/
						//Login session creation
						/*****************************/
						$arr_user_login_info['user_account_number'] = $user_info_array['Account_Number'];
						$arr_user_login_info['user_name'] = $user_info_array['Contact_FirstName'] . ' ' . $user_info_array['Contact_LastName'];
						$arr_user_login_info['user_email_address'] = $user_info_array['Login'];
						$arr_user_login_info['user_group_id'] = $user_info_array['Group'];
						$arr_user_login_info['user_active_status'] = $user_info_array['Activated'];
						$arr_user_login_info['user_dp'] = $user_info_array['member_facebook_photo'];

						$this->userauth->create_login_session($arr_user_login_info);

						if($first_login == TRUE){
							$landing = site_url('my_account');
						}else{
							$landing = site_url('home');
						}

						redirect($landing);
					}

					if($auto_login == FALSE){
						$user = null;
						$this->out();
					}
				}
			}catch(FacebookApiException $e) {
				$user = null;
				$this->out();
			}
		}

		redirect(site_url('login') . ($extra_params != '' ? '?p=' . $extra_params : ''));
	}

	public function login_process(){
		$action_type = $this->input->post('action_type');

		switch($action_type){
			case 'login':
				//captcha for failed attempts lets see here //
				$failed_attempts=$this->session->userdata('failed_attempts');
				if($failed_attempts>=3){
					$failed_attempts++;
					$this->session->set_userdata('failed_attempts',$failed_attempts);
					$security_code = $this->input->post('security_code');
					if($security_code!=$this->session->userdata('security_code')){
						$this->session->set_flashdata('error', 'Please fill captcha properly.');
						redirect(site_url('login') . ($extra_params != '' ? '?p=' . $extra_params : ''));
						exit;
					}
				}
				/*************************************************/
				
				$extra_params = '';
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				$remember_me = $this->input->post('remember_me');

				$user_info_array = $this->ct_account_m->get_account_info("Login = " . $this->db->escape($username) . " and AES_Decrypt(Password,'".$this->config->item('ccv_passEncryptKey')."') = " . $this->db->escape($password) );

				if(is_array($user_info_array) && count($user_info_array)){
					if($user_info_array['Activated'] == '1'){
						/*****************************/
						//Login session creation
						/*****************************/
						$arr_user_login_info['user_account_number'] = $user_info_array['Account_Number'];
						$arr_user_login_info['user_name'] = $user_info_array['Contact_FirstName'] . ' ' . $user_info_array['Contact_LastName'];
						$arr_user_login_info['user_email_address'] = $user_info_array['Login'];
						$arr_user_login_info['user_group_id'] = $user_info_array['Group'];
						$arr_user_login_info['user_active_status'] = $user_info_array['Activated'];
						$arr_user_login_info['user_dp'] = $user_info_array['member_facebook_photo'];

						$this->userauth->create_login_session($arr_user_login_info);

						/*****************************/
						//Remember me
						/*****************************/
						if(isset($remember_me) && $remember_me == '1'){
							$remember_cookie = time()+30*24*60*60;
							$cookie_domain = '/';

							setcookie("remember_me", "1", $remember_cookie, $cookie_domain);
							setcookie("remember_username", $username, $remember_cookie, $cookie_domain);
							setcookie("remember_password", $password, $remember_cookie, $cookie_domain);
						}else{
							if(!isset($_COOKIE['remember_me']) || $_COOKIE['remember_me'] == ''){
								setcookie("remember_me", "", 1, $cookie_domain);
								setcookie("remember_username", "", 1, $cookie_domain);
								setcookie("remember_password", "", 1, $cookie_domain);
							}
						}
                        
                        $this->session->set_flashdata('action', 'login');

						$redirect_url='https://numbersearch.etollfree.net/';
						$baseurl=base_url();
						
						if(strpos($baseurl,'portaltest')){
						      $redirect_url='https://numbersearchtest.etollfree.net/';
						}
						if($this->input->post('login_from')=="numbersearch") 
						    	  redirect($redirect_url);
						else
						    redirect(site_url('home'));
					}else{
						//for captcha //
						if($failed_attempts==''){
							$failed_attempts=0;
						}
						$failed_attempts++;
						$this->session->set_userdata('failed_attempts',$failed_attempts);
						//////////////////////////////////////////////////////////////////////
						$extra_params = $this->encryption->encode($username);
						//$this->session->set_flashdata('error', 'Please make active your account. If you have not got activation mail, <a href="' . site_url('login/resend_activation') . '?ee=' . $extra_params . '">Click here</a> for get new activation email.');
					}
				}else{
					//for captcha //
					if($failed_attempts==''){
						$failed_attempts=0;
					}
					$failed_attempts++;
					$this->session->set_userdata('failed_attempts',$failed_attempts);
					//////////////////////////////////////////////////////////////////////
					$this->session->set_flashdata('error', 'Please check your username and password.');
				}

				redirect(site_url('login') . ($extra_params != '' ? '?p=' . $extra_params : ''));
			break;

			case 'create_account':
				$email = $this->input->post('email');
				$password = $this->input->post('confirm_password');
				$account_name = $this->input->post('account_name');
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$contact_number = $this->input->post('contact_number');
				$country = $this->input->post('country');
				$address = $this->input->post('address');
				$city = $this->input->post('city');
				$state = $this->input->post('state');
				$province = $this->input->post('province');
				$zip_code = $this->input->post('zip_code');

				$security_code = $this->input->post('security_code');

				$account_values_array = array(
					'Activated' => 0,
					'Login' => $email,
					'Password' => $password,
					'Contact_FirstName' => $first_name,
					'Contact_LastName' => $last_name,
					'Address' => $address,
					'City' => $city,
					'State' =>$state,
					'Province' => $province,
					'ZipCode' =>$zip_code,
					'Contact_Number' => $contact_number,
					'Account_Name' => $account_name,
					'eBilling' => $email,
					'Status_Update' =>date('Y/m/d'),
					'Agent' =>'NULL',
 					'Country' =>$country,
                    'IP' => get_ip(),
                    'SingupPG' => 1
				);

				if($security_code == $this->session->userdata('security_code')){
					$user_info_array = $this->ct_account_m->get_account_info("Login = '" . $email . "'");

					if(count($user_info_array) <= 0){
						//Add user to DB
						
						
						//$this->ct_account_m->add_account($account_values_array);
						$this->ct_account_m->add_account($account_values_array);
					
						//Email setup
						$site_name = $this->config->item('site_name');
						$email_from = $this->config->item('email_from');
						$email_from_name = $this->config->item('email_from_name');
						$site_url = $this->config->item('site_url');

						$this->email->to($email);
						$this->email->from($email_from, $email_from_name);
						$this->email->subject('Activate your account with ' . $site_name);
						
						$encryptioned_email = $this->encryption->encode($email);

						$msg = '<html>
							<head>
								<title>Thank you for registration with eTollfree.net1</title>
							</head>
							<body>
								<p>Dear <b>' . $first_name . ' ' . $last_name . '</b>,</p>
								<p>Thanks for signing-up to eTollfree.net. You may login using the details below.</p>
								<p>Your account details are:</p>
								<p>E-mail Id:<b> ' . $email . ' </b><br/>
								Password:<b> ' . $password . ' </b><br></p>
								<p>Please click <a href="' . site_url('login/activate_account/' . $encryptioned_email) . '">ACTIVATE MY ACCOUNT</a> link to activate your account with us.</p>
								<p>Regards,</p>
								<p>eTollfree.net Team<br/>
								<a href="' . base_url() . '" target="_blank">eTollfree.net</a></p>
							</body>
						</html>';

						$this->email->message($msg);
						$this->email->send();

						$this->email->to('robert@etollfree.net');
						$this->email->from($email_from, $email_from_name);
						$this->email->subject('User registered with ' . $email_from_name);

						$state_array = $this->config->item('state_array');
						
						$msg = '<html>
									<body>
										<p>Hi,</p>
										<p>User has been registered with ' . $site_name . '</p>
										<p>Account details are:<br><br></p>
										<p>First Name:<b>' . $first_name . ' </b></p>
										<p>Last Name:<b>' . $last_name . ' </b></p>
										<p>Address:<b>' . $address . ' </b></p>
										<p>City:<b>' . $city . ' </b></p>
										<p>State:<b>' . $state_array[$state] . ' </b></p>
										<p>Zipcode:<b>' . $zip_code . ' </b></p>
										<p>Country:<b>' . ucwords(strtolower($country)) . ' </b><br><br></p>
										<p>E-mail Id:<b> ' . $email . ' </b><br></p>
										<p>Password:<b>' . $password . ' </b><br><br></p>
										<p>Regards,</p>
										<p>' . $site_name . ' Team</p>
										<p><a href="' . base_url() . '" target="_blank">' . str_replace('http://','',$site_url) . '</a></p>
									</body>
								</html>';

						$this->email->message($msg);
						//$this->email->send();

						$this->session->set_flashdata('success', 'Your have registered with us successfully. Please check your email for activate your account.');
					}else{
						$this->session->set_flashdata('error', 'Your email address has been already registered with us.');
					}
				}else{
					$this->session->set_flashdata('error', 'Please check the security code.');
				}

				redirect(site_url('login'));
			break;

			case 'create_fb_account':
				require_once APPPATH . 'third_party/fb-sdk/facebook.php';

				$facebook = new Facebook(array(
					'appId'  => $this->config->item('fb_app_id'),
					'secret' => $this->config->item('fb_app_secret_key')
				));

				$user = $facebook->getUser();

				$user_profile = array();
				if($user){
					try{
						$user_profile = $facebook->api('/'.$user);

						if(is_array($user_profile) && count($user_profile)){
							$email = $user_profile['email'];
							$password = $this->input->post('confirm_password');
							$account_name = $this->input->post('account_name');
							$first_name = $this->input->post('first_name');
							$last_name = $this->input->post('last_name');
							$contact_number = $this->input->post('contact_number');
							$country = $this->input->post('country');
							$address = $this->input->post('address');
							$city = $this->input->post('city');
							$state = $this->input->post('state');
							$province = $this->input->post('province');
							$zip_code = $this->input->post('zip_code');

							$security_code = $this->input->post('security_code');

							$account_values_array = array(
								'Activated' => '1',
								'Login' => $email,
								'Password' => $password,
								'Contact_FirstName' => $first_name,
								'Contact_LastName' => $last_name,
								'Address' => $address,
								'City' => $city,
								'State' => $state,
								'Province' => $province,
								'ZipCode' => $zip_code,
								'Contact_Number' => $contact_number,
								'Account_Name' => $account_name,
								'Country' => $country,
								'member_facebook_id' => $user_profile['id'],
                                'IP' => get_ip(),
                                'SingupPG' => 1
							);

							if($security_code == $this->session->userdata('security_code')){
								$user_info_array = $this->ct_account_m->get_account_info("Login = '" . $email . "'");

								if(count($user_info_array) <= 0){
									//Add user to DB
									$account_number = $this->ct_account_m->add_account($account_values_array);
									get_facebook_profile_photo($account_number);

									//Email setup
									$site_name = $this->config->item('site_name');
									$email_from = $this->config->item('email_from');
									$email_from_name = $this->config->item('email_from_name');
									$site_url = $this->config->item('site_url');

									$this->email->to($email);
									$this->email->from($email_from, $email_from_name);
									$this->email->subject('Thank you for registration with ' . $site_name);
									
									$encryptioned_email = $this->encryption->encode($email);

									$msg = '<html>
										<head>
											<title>Thank you for registration with eTollfree.net</title>
										</head>
										<body>
											<p>Dear <b>' . $first_name . ' ' . $last_name . '</b>,</p>
											<p>Thanks for signing-up to eTollfree.net. You may login using the details below.</p>
											<p>Your account details are:</p>
											<p>E-mail Id:<b> ' . $email . ' </b><br/>
											Password:<b> ' . $password . ' </b><br></p>
											<p>Regards,</p>
											<p>eTollfree.net Team<br/>
											<a href="' . base_url() . '" target="_blank">eTollfree.net</a></p>
										</body>
									</html>';

									$this->email->message($msg);
									$this->email->send();

									$user_info_array = $this->ct_account_m->get_account_info("Login = '" . $email . "'");
									/*****************************/
									//Login session creation
									/*****************************/
									$arr_user_login_info['user_account_number'] = $user_info_array['Account_Number'];
									$arr_user_login_info['user_name'] = $user_info_array['Contact_FirstName'] . ' ' . $user_info_array['Contact_LastName'];
									$arr_user_login_info['user_email_address'] = $user_info_array['Login'];
									$arr_user_login_info['user_group_id'] = $user_info_array['Group'];
									$arr_user_login_info['user_active_status'] = $user_info_array['Activated'];
									$arr_user_login_info['user_dp'] = $user_info_array['member_facebook_photo'];

									$this->userauth->create_login_session($arr_user_login_info);

									$response_array = array(
										'status' => 'SUCCESS',
										'redirect' => site_url('home')
									);

									//redirect(site_url('home'));
									//$this->session->set_flashdata('success', 'Your have registered with us successfully.');
								}else{
									$response_array = array(
										'status' => 'ERROR',
										'message' => 'Your email address has been already registered with us.'
									);
									//$this->session->set_flashdata('fb_error', 'Your email address has been already registered with us.');
								}
							}else{
								//$this->session->set_flashdata('fb_error', 'Please check the security code.');
								$response_array = array(
									'status' => 'ERROR',
									'message' => 'Please check the security code.'
								);
							}
						}

					}catch(FacebookApiException $e){
						$user = null;
						$this->out();
					}

					echo json_encode($response_array); exit;

					//redirect(site_url('login') . '?a=fb_reg_process');
				}

				redirect(site_url('login'));
			break;

			case 'forgot_password':


				$email = $this->input->post('email');
        $email = $this->db->escape( $email );

				//adding captcha here so we can prune sql injection attempts //
				$security_code = $this->input->post('security_code');
				if($security_code != $this->session->userdata('security_code')){
					$this->session->set_flashdata('error', 'Please fill the captcha properly.');
					redirect(site_url('login'));
					exit;
				}
				///////////////////////////////////////////////
				
				$user_info_array = $this->ct_account_m->get_account_info("Login = " . $this->db->escape($email) . "");

				if(is_array($user_info_array) && count($user_info_array)){

					//set password to new random one and also update database for password
					$user_info_array['Password']=$this->random_num(15);
					$this->ct_account_m->update_account($user_info_array['Account_Number'],array('Password'=>$user_info_array['Password'])); //updating password to random number so no one can login
					///////////////////////////
					
					$site_name = $this->config->item('site_name');
					$email_from = $this->config->item('email_from');
					$email_from_name = $this->config->item('email_from_name');
					$site_url = $this->config->item('site_url');

					$this->email->to($user_info_array['Login']); 
					$this->email->from($email_from, $email_from_name);
					$this->email->subject('Forgot password - ' . $site_name);
					
					$message = '<html>
									<head>
										<title>Forgot Password - ' . $site_name . '</title>
									</head>
								<body>
									<p>Dear <b>' . $user_info_array['Contact_FirstName'] . ' ' . $user_info_array['Contact_LastName'] . '</b>,</p>
									<p>Your account details are:<br></p>
									<p>E-mail Id:<b> ' . $user_info_array['Login'] . '</b></p>
									<p>Password:<b> ' . $user_info_array['Password'] . '</b><br></p>
									<p>Regards,</p>
									<p>' . $site_name . ' Team</p>
									<p><a href="' . base_url() . ' " target="_blank">' . str_replace('http://','',$site_url) . '</a></p>
								</body>
							</html>';

					$this->email->message($message);
					$this->email->send();					
					$this->session->set_flashdata('success', 'Your login details has been sent to your registered email address.');
				}else{
					$this->session->set_flashdata('error', 'Your email address has not been registered with us.');
				}

				redirect(site_url('login'));
			break;
		}
	}

	public function resend_activation() {
		$enc_email = $this->input->get('ee');
		if(isset($enc_email) && php_not_null($enc_email)){
			$email = $this->encryption->decode($enc_email);
			$user_info_array = $this->ct_account_m->get_account_info("Login = '" . $email . "'");

			if(count($user_info_array)){
				//Email setup
				$site_name = $this->config->item('site_name');
				$email_from = $this->config->item('email_from');
				$email_from_name = $this->config->item('email_from_name');
				$site_url = $this->config->item('site_url');

				$this->email->to($email);
				$this->email->from($email_from);
				$this->email->subject('eTollfree Account Activation');
				
				$encryptioned_email = $this->encryption->encode($email);
				
				$msg = '<html>
							<head>
								<title>Thank you for registration with eTollfree.net</title>
							</head>
							<body>
								<p>Dear <b>' . $user_info_array['Contact_FirstName'] . ' ' . $user_info_array['Contact_FirstName'] . '</b>,</p>
								<p>Thanks for signing-up with eTollfree. You may login using the details below.</p>
								<p>Your account details are:</p>
								<p>E-mail Id:<b> ' . $user_info_array['Login'] . ' </b><br/>
								Password:<b> ' . $user_info_array['Password'] . ' </b><br></p>
								<p>Please click <a href="' . site_url('login/activate_account/' . $encryptioned_email) . '">ACTIVATE MY ACCOUNT</a> link to activate your account with us.</p>
								<p>Regards,</p>
								<p>eTollfree Support Team<br/>
								<a href="' . base_url() . '" target="_blank">eTollfree.net</a></p>
							</body>
						</html>';

				$this->email->message($msg);
				$this->email->send();

				$this->session->set_flashdata('success', 'Activation email has been sent to your registered email address. ');
			}
		}

		redirect(site_url('login'));
	}

	public function activate_account($enc_email){
		if($enc_email != ''){
			$email = $this->encryption->decode($enc_email);

			$user_info_array = $this->ct_account_m->get_account_info("Login = '" . $email . "'");
			if(is_array($user_info_array) && count($user_info_array)){

				$account_values_array = array('Activated' => '1');

				$this->ct_account_m->update_account($user_info_array['Account_Number'], $account_values_array);

				$this->session->set_flashdata('success', 'You have activated successfully.');
			}else{
				$this->session->set_flashdata('error', 'There is some problem with your activation.');
			}
		}

		redirect(site_url('login'));
	}

	public function out(){
		$this->userauth->logout();
	}

	public function test_email() {
			$site_name = $this->config->item('site_name');
			$email_from = $this->config->item('email_from');
			$site_url = $this->config->item('site_url');

			$this->email->to('raju.rajpurohit@gmail.com');
			$this->email->from($email_from, $site_name);
			$this->email->subject('Test email from Raju');

			$encryptioned_email = $this->encryption->encode('raju.rajpurohit@gmail.com');
			
			$msg = '<html>
				<head>
					<title>Thank you for registration with eTollfree.net</title>
				</head>
				<body>
					<p>Dear <b>Raju Singh</b>,</p>
					<p>Thanks for signing-up to eTollfree.net. You may login using the details below.</p>
					<p>Your account details are:</p>
					<p>E-mail Id:<b> raju.rajpurohit@gmail.com </b><br/>
					Password:<b> 123456 </b><br></p>
					<p>Please click <a href="' . site_url('login/activate_account/' . $encryptioned_email) . '">ACTIVATE MY ACCOUNT</a> link to activate your account with us.</p>
					<p>Regards,</p>
					<p>eTollfree.net Team<br/>
					<a href="' . base_url() . '" target="_blank">eTollfree.net</a></p>
				</body>
			</html>';

			$this->email->message($msg);
			$this->email->send();

			echo $this->email->print_debugger();
	}
	//function to generate random string
	private function random_num($size) {
		$alpha_key = '';
		$keys = range('A', 'Z');

		for ($i = 0; $i < 2; $i++) {
			$alpha_key .= $keys[array_rand($keys)];
		}

		$length = $size - 2;

		$key = '';
		$keys = range(0, 9);

		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}

		return $alpha_key . $key;
	}
}
