<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends Front_controller {
	var $array_csv_column = array();
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->array_csv_column = array('Order_Notes'=>'Note','Order_Type'=>'Order Type', 'Service_Type'=>'Service Type', 'DID'=>'DID', 'Resporg_ID'=>'Resporg ID', 'RingToType'=>'Ring To Type', 'Ring_To_Data'=>'Ring To Data', 'Area_of_Service'=>'Area of Service', 'Order_Template'=>'Order Template');

		$this->load->model('portal_orders_m');
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
		$this->history();
	}

	public function history(){
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Order History";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
		$data['current_page'] = 'ORDERS';
		$sess_user_account_number = $this->session->userdata('sess_user_account_number');
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$this->load->view($this->config->item('portal_folder') . '/order_history', $data);
	}

	public function import(){
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Order Import";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
		$data['current_page'] = 'ORDERS';

		$this->load->model('ct_orders_m');

		$import_heading_data_array = array();
		$import_data_array = array();

		$action_type = $this->input->post('action_type');

		if(isset($action_type) && !empty($action_type)){
			switch($action_type){
				case 'import_data':
					if(is_array($_FILES['import_file']) && count($_FILES['import_file'])){
						$source_import_file = $_FILES['import_file']['tmp_name'];
						$fp = fopen($source_import_file, 'r');

						$cnt_data = 0;
						while($csv_data = fgetcsv($fp, 5000, ',')){
							if($cnt_data == 0){
								$import_heading_data_array = $csv_data;
							}else{
								$import_data_array[] = $csv_data;
							}
							$cnt_data++;
						}
					}
				break;
				case 'import_data_final':
					$data_import_array = $this->input->post('data_import');
					
					if(is_array($data_import_array) && count($data_import_array)){
						foreach($data_import_array as $data_import_info){
							$db_order_values = array(
								'Order_Notes' => $data_import_info[0],
								'Order_Type' => $data_import_info[1],
								'Service_Type' => $data_import_info[2],
								'DID' => $data_import_info[3],
								'Resporg_ID' => $data_import_info[4],
								'RingToType' => $data_import_info[5],
								'Ring_To_Data' => $data_import_info[6],
								'Area_of_Service' => $data_import_info[7],
								'Order_Template' => $data_import_info[8],
								'Account_Number' => $this->session->userdata('sess_user_account_number')
							);

							$this->ct_orders_m->add_order($db_order_values);

							//echo $this->db->last_query(); exit;
						}

						$this->session->set_flashdata('success', 'Your orders has been imported successfully.');
					}

					redirect(site_url('orders/import'));
				break;
			}
		}

		$data['import_heading_data_array'] = $import_heading_data_array;
		$data['import_data_array'] = $import_data_array;
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$this->load->view($this->config->item('portal_folder') . '/order_import', $data);
	}

	public function generate_order_csv(){
		$this->load->helper('download');

		ob_start();
		$f = fopen('php://output', 'w') or show_error("Can't open php://output");

		if (! fputcsv($f, $this->array_csv_column))show_error("Can't write CSV");

		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();

		$filename = 'Batch_order_import_' . date("YmdHis").'.csv';

		force_download($filename, $str);
	}

	public function history_listing(){
		$this->portal_orders_m->get_listing();
	}
}