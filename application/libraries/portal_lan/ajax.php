<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends Front_controller {
    public function process()
    {		
        $this->load->model(array('portal_number_list_m', 'portal_ct_extensions_m', 'ct_conference_rooms_m', 'portal_voicemail_m', 'ct_call_screen_m', 'ct_orders_fax_m', 'ct_tn_m', 'auto_attendant_m', 'db_portal_info_m'));

        $sess_user_account_number = $this->session->userdata('sess_user_account_number');

        $action_type = $this->input->post('action_type');		
        if(isset($action_type) && $action_type != ''){
            switch($action_type){				
                case 'get_number_info':				
                    $did = $this->input->post('did');
					
                    $number_info = $this->portal_number_list_m->get_number_list_info("Account_Number = '" . $sess_user_account_number . "' AND DID = " . $this->db->escape($did) . "");

                    echo json_encode($number_info);
                    exit;
                break;
                case 'get_extension_info':
                    $extention = $this->input->post('extention');

                    $extension_info = $this->portal_ct_extensions_m->get_extension_info("`Extention` = '" . $extention ."'");

                    echo json_encode($extension_info);
                    exit;
                break;
                case 'get_conference_info':
                    $conference = $this->input->post('conference');

                    $conference_info = $this->portal_ct_extensions_m->get_extension_info("`Extention` = '" . $extention ."'");

                    echo json_encode($conference_info);
                    exit;
                break;
                case 'get_conf_bridge_info':
                    $key = $this->input->post('key');

                    $conf_bridge_info = $this->ct_conference_rooms_m->get_conf_room_info("`Key` = '" . $key ."'");    
                    echo json_encode($conf_bridge_info);
                    exit;
                break;
                case 'get_voice_mail_info':
                    $uniqueid = $this->input->post('uniqueid');

                    $voice_mail_info = $this->portal_voicemail_m->get_voice_mail_info("`uniqueid` = '" . $uniqueid ."' AND Account_Number = '" . $sess_user_account_number . "'");    
                    echo json_encode($voice_mail_info);
                    exit;
                break;
                case 'get_billing_other':
                    $this->load->model('portal_billing_other_m');

                    $billing_period = $this->input->post('billing_period');

                    $other_billing_list = $this->portal_billing_other_m->get_other_billing_list("Account_Number = '" . $sess_user_account_number . "' AND BillPeriod = '" . $billing_period . "'");

                    $str_html = '<div class="table-responsive">';
                    $str_html .= '<table class="table table-bordered table-striped">
                        <thead>
                            <tr class="active">
                                <th>Bill Period</th>
                                <th>Service Type</th>
								<th>Details</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Charge</th>
                                <th>MSF</th>
                                <th>MST</th>
                            </tr>
                        </thead>
                        <tbody>
                    ';

                    if(is_array($other_billing_list) && count($other_billing_list)){
                        foreach($other_billing_list as $billing_other_info){
                            $str_html .= '<tr>
                                <td>' . $billing_other_info['BillPeriod'] . '</td>
                                <td>' . $billing_other_info['Service Type'] . '</td>
								<td>' . $billing_other_info['Details'] . '</td>
                                <td>' . $billing_other_info['Quantity'] . '</td>
                                <td>' . $billing_other_info['Rate'] . '</td>
                                <td>' . display_price($billing_other_info['Charge']) . '</td>
                                <td>' . efree_date_format($billing_other_info['MSF']) . '</td>
                                <td>' . efree_date_format($billing_other_info['MST']) . '</td>
                            </tr>
                            ';
                        }
                    }else{
                        $str_html .= '<tr><td align="center" colspan="8">No other usage available.</td></tr>';
                    }

                    $str_html .= '
                        </tbody>
                    </table></div>';
                    echo $str_html;
                break;
                case 'get_billing_rcc':
                    $this->load->model('portal_billing_rcc_m');

                    $billing_period = $this->input->post('billing_period');

                    $rcc_billing_list = $this->portal_billing_rcc_m->get_rcc_billing_list("Account_Number = '" . $sess_user_account_number . "' AND BillPeriod = '" . $billing_period . "'");

                    $str_html = '<div class="table-responsive">';
                    $str_html .= '<table class="table table-bordered table-striped">
                        <thead>
                            <tr class="active">
                                <th>Bill Period</th>
                                <th>Service Type</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Charge</th>
                                <th>MSF</th>
                                <th>MST</th>
                            </tr>
                        </thead>
                        <tbody>
                    ';

                    if(is_array($rcc_billing_list) && count($rcc_billing_list)){
                        foreach($rcc_billing_list as $rcc_billing_info){
                            $str_html .= '<tr>
                                <td>' . $rcc_billing_info['BillPeriod'] . '</td>
                                <td>' . $rcc_billing_info['Service Type'] . '</td>
                                <td>' . $rcc_billing_info['Quantity'] . '</td>
                                <td>' . $rcc_billing_info['Rate'] . '</td>
                                <td>' . display_price($rcc_billing_info['Charge']) . '</td>
                                <td>' . efree_date_format($rcc_billing_info['MSF']) . '</td>
                                <td>' . efree_date_format($rcc_billing_info['MST']) . '</td>
                            </tr>
                            ';
                        }
                    }else{
                        $str_html .= '<tr><td align="center" colspan="8">No MRC usage available.</td></tr>';
                    }

                    $str_html .= '
                        </tbody>
                    </table></div>';
                    echo $str_html;
                break;
                case 'get_billing_usage':
                    $this->load->model('portal_billing_usage_m');

                    $billing_period = $this->input->post('billing_period');

                    $usage_billing_list = $this->portal_billing_usage_m->get_usage_billing_list("Account_Number = '" . $sess_user_account_number . "' AND BillPeriod = '" . $billing_period . "'");

                    $str_html = '<div class="table-responsive">';
                    $str_html .= '<table class="table table-bordered table-striped">
                        <thead>
                            <tr class="active">
                                <th>Bill Period</th>
                                <th>Service Type</th>
                                <th>Destype</th>
                                <th>Total MOU</th>
                                <th>Rate</th>
                                <th>Charge</th>
                                <th>MSF</th>
                                <th>MST</th>
                            </tr>
                        </thead>
                        <tbody>
                    ';

                    if(is_array($usage_billing_list) && count($usage_billing_list)){
                        foreach($usage_billing_list as $usage_billing_info){
                            $str_html .= '<tr>
                                <td>' . $usage_billing_info['BillPeriod'] . '</td>
                                <td>' . $usage_billing_info['Service Type'] . '</td>
                                <td>' . $usage_billing_info['Destype'] . '</td>
                                <td>' . $usage_billing_info['Total MOU'] . '</td>
                                <td>' . $usage_billing_info['Rate'] . '</td>
                                <td>' . display_price($usage_billing_info['Charge']) . '</td>
                                <td>' . efree_date_format($usage_billing_info['MSF']) . '</td>
                                <td>' . efree_date_format($usage_billing_info['MST']) . '</td>
                            </tr>
                            ';
                        }
                    }else{
                        $str_html .= '<tr><td align="center" colspan="8">No usage available.</td></tr>';
                    }

                    $str_html .= '
                        </tbody>
                    </table></div>';
                    echo $str_html;
                break;
                case 'check_email_availablity':    
                    $this->load->model('ct_account_m');

                    $email = $this->input->post('email');
                    $email = $this->db->escape( $email );

                    $response = '0';
                    $user_info_array = $this->ct_account_m->get_account_info("Login = '" . $email . "'");

                    if(is_array($user_info_array) && count($user_info_array)){
                        $response = '1';
                    }

                    echo $response;
                    exit;
                break;
                case 'check_dup_moderator_pin':    
                    $moderator_pin = $this->input->post('moderator_pin');
                    $form_action = $this->input->post('form_action');
                    $key = $this->input->post('key');

                    $str_where = "Account_Number = '" . $sess_user_account_number . "' AND Moderator_PIN = " . $this->db->escape($moderator_pin);
                    if($form_action != 'new_conf_rooms'){
                        $str_where .= " AND `Key` != '" . $key . "'";
                    }

                    $response = 'true';
                    $conf_room_info = $this->ct_conference_rooms_m->get_conf_room_info($str_where);

                    if(is_array($conf_room_info) && count($conf_room_info)){
                        $response = 'false';
                    }

                    echo $response;
                    exit;
                break;
                case 'check_dup_participant_pin':    
                    $participant_pin = $this->input->post('participant_pin');
                    $form_action = $this->input->post('form_action');
                    $key = $this->input->post('key');

                    $str_where = "Account_Number = '" . $sess_user_account_number . "' AND Participant_PIN = " . $this->db->escape($participant_pin);
                    if($form_action != 'new_conf_rooms'){
                        $str_where .= " AND `Key` != '" . $key . "'";
                    }

                    $response = 'true';
                    $conf_room_info = $this->ct_conference_rooms_m->get_conf_room_info($str_where);

                    if(is_array($conf_room_info) && count($conf_room_info)){
                        $response = 'false';
                    }

                    echo $response;
                    exit;
                break;
                case 'check_dup_mailbox':
					$new_number =  $this->input->post('mailbox_number');
                    $mailbox_number = intval($new_number);
                    $form_action = $this->input->post('form_action');
                    $current_mailbox_number = $this->input->post('current_int_id');

                    $str_where = "Account_Number = '" . $sess_user_account_number . "' AND `Mailbox Number` = " . $this->db->escape($mailbox_number);
                    if($form_action != 'new_voicemail'){
                        $str_where .= " AND `Mailbox Number` != '" . $current_mailbox_number . "'";
                    }

                    $response = 'true';
                    $voice_mail_info = $this->portal_voicemail_m->get_voice_mail_info($str_where);    

                    if(is_array($voice_mail_info) && count($voice_mail_info)){
                        //$response = 'false';
                    }

                    echo $response;
                    exit;
                break;
                case 'get_call_screen_info':
                    $key = $this->input->post('key');

                    $call_screen_info = $this->ct_call_screen_m->get_call_screen_info("`Key` = '" . $key ."'");

                    echo json_encode($call_screen_info);
                    exit;
                break;
                case 'check_call_screen_status':
                    $key = $this->input->post('key');

                    $call_screen_info = $this->ct_call_screen_m->get_call_screen_info("`Key` = '" . $key ."'");

                    $response_array = array();

                    if($call_screen_info['Status'] != 'Updating Switch'){
                        $response_array['status'] = show_status_color($call_screen_info['status_name'], $key);
                    }else{
                        $response_array['status'] = $call_screen_info['Status'];
                    }

                    echo json_encode($response_array);
                    exit;
                break;
                // code by heng
                case 'get_video_link':
                    $this->load->model('video_popup_m');
                
                    $video_title = $this->input->post('video_title');

                    $video_link = $this->video_popup_m->get_video_link("video_title = '" . $video_title . "'");

                    echo json_encode($video_link);
                    exit;
                break;
                // end
                // get auto attendant
                case 'get_auto_attendant_info':
                    $this->load->model('auto_attendant_m');
                    
                    $key = $this->input->post('key');
                    
                    $auto_attendant_info = $this->auto_attendant_m->get_auto_attendant_info("`Key` = '" . $key ."'");

                    echo json_encode($auto_attendant_info);
                    exit;
                break;
				case 'get_ring_group_info':
					$this->load->model('ct_ring_groups_m');
                    
                    $key = $this->input->post('key');
                    
                    $ring_group_info = $this->ct_ring_groups_m->get_ring_group_info("`Key` = '" . $key ."'");

					$ring_group_details = $this->ct_ring_groups_m->get_ring_group_detail_info("`Group_Key` = '" . $key ."'", FALSE);
					$ring_group_info['ring_group_details'] = $ring_group_details;

                    echo json_encode($ring_group_info);
                    exit;
				break;
                case 'resend_fax':
                    $order_number = $this->input->post('int_id');
                    $this->ct_orders_fax_m->update_order($order_number, array('Status' => '1'));
                    $this->session->set_flashdata('success', 'Fax order has been resent successfully.');
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Fax order has been resent successfully.</div>';
                    
                    echo json_encode($response);
                    exit;
                break;
                case 'cancel_fax':
                    $order_number = $this->input->post('int_id');
                    $this->ct_orders_fax_m->update_order($order_number, array('Status' => '4'));
                    $this->session->set_flashdata('success', 'Fax order has been cancelled successfully.');
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Fax order has been cancelled successfully.</div>';
                    
                    echo json_encode($response);
                    exit;
                break;
                case 'delete_fax':
                    $order_number = $this->input->post('int_id');
                    $this->ct_orders_fax_m->delete_order($order_number);
                    $this->session->set_flashdata('success', 'Fax order has been deleted successfully.');
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Fax order has been deleted successfully.</div>';
                    
                    echo json_encode($response);
                    exit;
                break;
                case 'edit_fax_name':
                    $order_number = $this->input->post('int_id');
                    $fax_name = $this->input->post('fax_name');
                    $this->ct_orders_fax_m->update_order($order_number, array('fax_receiver_name' => $fax_name));
                    $this->session->set_flashdata('success', 'Fax order has been updated successfully.');
                    $response['result'] = "success";
                    $response['fax_name'] = $fax_name;
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Fax order has been updated successfully.</div>';
                    
                    echo json_encode($response);
                    exit;
                break;
                case 'check_ring_group_extension':
                    $this->load->model('ct_ring_groups_m');
                    
					$new_extension =  $this->input->post('extension');
                    $extension = intval($new_extension);
                    
                    $ring_group_info = $this->ct_ring_groups_m->get_ring_group_info("`Group_Extension` = '" . $extension ."' and Account_Number = '".$sess_user_account_number."'");

                    echo json_encode($ring_group_info);
                    exit;
                break;
				case 'new_ring_group':
					$this->load->model('ct_ring_groups_m');

					$new_extension =  $this->input->post('extension');
                    $extension = intval($new_extension);
					$name = $this->input->post('name');
					$time_out = $this->input->post('time_out');
					$timeout = $this->input->post('timeout');
					$ring_back = $this->input->post('ring_back');

					$ring_type_array = $this->input->post('ring_type_option');
					$ring_to_data_array = $this->input->post('ring_to_data_option');

					$timeout_route = $this->input->post('ring_type_timeout');
					$timeout_data = $this->input->post('ring_to_data_timeout');
					
					$rg_values_array = array(
						'Group_Extension' => $extension,
						'Group_Name' => $name,
						'TimeOut' => $time_out,
						'TimeOutRoute' => $timeout_route,
						'TimeOutData' => $timeout_data,
						'Ringback' => $ring_back,
						'Status' => 11
					);
					
					$rg_values_array['Account_Number'] = $sess_user_account_number;
					$int_id = $this->ct_ring_groups_m->add_ring_group($rg_values_array);
					
					if(is_array($ring_type_array) && count($ring_type_array)){
						foreach($ring_type_array as $kRingType => $RingType){
							$RingToData = $ring_to_data_array[$kRingType];
							$rg_detail_db_array = array(
								'Group_Key' => $int_id,
								'RingType' => $RingType,
								'RingToData' => $RingToData
							);

							$this->ct_ring_groups_m->add_ring_group_detail($rg_detail_db_array);
						}
					}
					
					$response['result'] = "success";
					$response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ring Group has been added successfully.</div>';
					echo json_encode($response);
                    exit;
				break;
				case 'update_ring_group':
					$this->load->model('ct_ring_groups_m');
					
					$int_id = $this->input->post('int_id');

					$new_extension =  $this->input->post('extension');
                    $extension = intval($new_extension);
					$name = $this->input->post('name');
					$time_out = $this->input->post('time_out');
					$timeout = $this->input->post('timeout');
					$ring_back = $this->input->post('ring_back');

					$ring_type_array = $this->input->post('ring_type_option');
					$ring_to_data_array = $this->input->post('ring_to_data_option');

					$timeout_route = $this->input->post('ring_type_timeout');
					$timeout_data = $this->input->post('ring_to_data_timeout');
					
					$rg_values_array = array(
						'Group_Extension' => $extension,
						'Group_Name' => $name,
						'TimeOut' => $time_out,
						'TimeOutRoute' => $timeout_route,
						'TimeOutData' => $timeout_data,
						'Ringback' => $ring_back,
						'Status' => 11
					);
					
					$this->ct_ring_groups_m->update_ring_group($int_id, $rg_values_array);
					
					$this->ct_ring_groups_m->delete_ring_group_details("", $int_id);
					if(is_array($ring_type_array) && count($ring_type_array)){
						foreach($ring_type_array as $kRingType => $RingType){
							$RingToData = $ring_to_data_array[$kRingType];
							$rg_detail_db_array = array(
								'Group_Key' => $int_id,
								'RingType' => $RingType,
								'RingToData' => $RingToData
							);

							$this->ct_ring_groups_m->add_ring_group_detail($rg_detail_db_array);
						}
					}
					
					$response['result'] = "success";
					$response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ring Group has been updated successfully.</div>';
					echo json_encode($response);
                    exit;
				break;
				case 'delete_ring_group':
					$this->load->model('ct_ring_groups_m');
					
					$int_id = $this->input->post('int_id');
					
					$this->ct_ring_groups_m->delete_ring_group($int_id);
					
					$this->ct_ring_groups_m->delete_ring_group_details("", $int_id);
					
					$response['result'] = "success";
					$response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ring Group has been deleted successfully.</div>';
					echo json_encode($response);
                    exit;
				break;
				
				
				case 'delete_cdr_download':
				$this->load->model('cdr_download_m');
				 $int_id = $this->input->post('int_id');
                   $this->cdr_download_m->delete_cdr_down($int_id);
					
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>CDR Download box has been deleted successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
				
				
				
				
				
				
				
				
				
                case 'change_name':
					$this->load->model('ct_ring_groups_m');
					
					$int_id = $this->input->post('int_id');
					$name = $this->input->post('name');
					
                    $rg_values_array = array(
                        'Group_Name' => $name
                    );
                    $this->ct_ring_groups_m->update_ring_group($int_id, $rg_values_array);
					
					$response['result'] = "success";
					$response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ring Group Name has been changed successfully.</div>';
					echo json_encode($response);
                    exit;
                break;
                case 'update_number_management':
                    $int_id = $this->input->post('int_id');
                    $note = $this->input->post('note');
                    $number_values_array = array(
                        'Note' => $note
                    );

                    $this->ct_tn_m->update_note($int_id, $number_values_array);
                    
                    $response['result'] = "success";
                    $response['note'] = $note;
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Your number has been updated successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'update_number_route':
                    $int_id = $this->input->post('int_id');
                    $ring_type = $this->input->post('ring_type');
                    switch($ring_type){
                        case '3':
                            $ring_to_data = $this->input->post('ip_address');
                        break;
                        case '4':
                            $ring_to_data = $this->input->post('voice_mail');
                        break;
                        case '8':
                            $ring_to_data = $this->input->post('auto_attendant');
                        break;
                        case '9':
                            $ring_to_data = $this->input->post('conference_bridge');
                        break;
                        case '10':
                            $ring_to_data = $this->input->post('phone_extension');
                        break;
                        case '12':
                            $ring_to_data = $this->input->post('ring_group');
                        break;
                        case '14':
                            $ring_to_data = $this->input->post('call_screen');
                        break;
                        default:
                            $ring_to_data = $this->input->post('ring_to_data');
                        break;
                    }    
                    $db_route_values = array(
                        'RingToType' => $ring_type,
                        'RingToData' => $ring_to_data,
                        'Status' => '11'
                    );

                    $this->ct_tn_m->update_tn($int_id, $db_route_values);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Your number routing has been updated successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'disconnect_number':
                    $did = $this->input->post('did');
                    $number_action_result_array = $this->portal_number_list_m->disconnect_number($did);

                    $number_action_result = $number_action_result_array['SP_Message1'];

                    $response['message'] = '';
                    if(!empty($number_action_result)){
                        $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$did.' '.$number_action_result.'</div>';
                    }
                    
                    $response['result'] = "success";
                    echo json_encode($response);
                    exit;
                break;
                case 'reconnect_number':
                    $did = $this->input->post('did');

                    $number_action_result_array = $this->portal_number_list_m->reconnect_number($did);

                    $number_action_result = $number_action_result_array['SP_Message1'];

                    $response['message'] = '';
                    if(!empty($number_action_result)){
                        $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$number_action_result.'</div>';
                    }
                    
                    $response['result'] = "success";
                    echo json_encode($response);
                    exit;
                break;
                case 'update_auto_attendant':
                    $int_id = $this->input->post('int_id');
                    
                    $name = $this->input->post('name');
                    $new_extension =  $this->input->post('extension');
                    $extension = intval($new_extension);
                    $recording_type = $this->input->post('recording_type');
                    if($recording_type == 'UPLOAD'){
                        $recording_file = $_FILES['recording_file'];
                        if($recording_file['size'] == 0){
                            $contents = "";
                        }else{
                            $recording_name = $recording_file['name'];
                            $name_array = explode(".", $recording_name);
                            $ext = end($name_array);
                            $valid_formats = array("mp3", "wav", "gsm");
                            if(in_array($ext,$valid_formats)){
                                $contents = file_get_contents($recording_file['tmp_name']);
                                /*$handle = fopen($recording_file['tmp_name'], "r");
                                $contents = fread($handle, $recording_file['size']);
                                $contents = addslashes($contents);
                                fclose($handle);*/
                            }else{
                                $contents = "";
                            }
                        }
                        
                        $talent = 0;
                    }else{
                        $contents = "";
                        $talent = $this->input->post('talent');
                    }
                    $attendant_message = $this->input->post('attendant_message');
                    $allow_ext_dial = $this->input->post('allow_ext_dial');
                    $ring_back = $this->input->post('ring_back');
                    $ring_type_timeout = $this->input->post('ring_type_timeout');
                    $ring_to_data_timeout = $this->input->post('ring_to_data_timeout');
                    
                    $ring_type = array();
                    $ring_to_data = array();
                    for($i=1; $i<=11; $i++){
                        if(isset($_POST['caller_option_'.$i])){
                            $ring_type[$i] = $_POST['ring_type_'.$i];
                            $ring_to_data[$i] = $_POST['ring_to_data_'.$i];
                        }
                    }
                    
                    //$status = $this->input->post('status');
                    $status = '11';//Updating switch

                    $main_array = array(
                        'IVR_Name' => $name,
                        'Extension' => $extension,
                        'talent_key' => $talent,
                        'Message' => $attendant_message,
                        'Extension_Dial' => $allow_ext_dial,
                        'Ringback' => $ring_back,
                        'TimeOutRoute' => $ring_type_timeout,
                        'TimeOut_Data' => $ring_to_data_timeout,
                        'Status' => $status,
                    );
                    
                    if($contents != ""){
                        $main_array = array_merge($main_array, array('Greeting' => $contents, 'Greeting_format' => $ext, 'Greeting_name' => $recording_name));
                    }
                    
                    if(count($ring_type)){
                        $detail_array = array(
                            'RingToType' => $ring_type,
                            'RingToData' => $ring_to_data,
                        );
                    }else{
                        $detail_array = array();
                    }
                    
                    $main_array['Status'] = '11';
                    $this->auto_attendant_m->update_auto_attendant($int_id, $main_array, $detail_array);
                    
                    $response['result'] = "success";
                    $response['extension_dial'] = $allow_ext_dial;
                    $response['player'] = '';
                    if($contents != ""){
                        $file_path = base_url()."uploads/greeting/".$recording_name;
            
                        if($ext == "mp3"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }elseif($ext == "wav"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/wav">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }elseif($ext == "gsm"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/wav">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }
                        $response['player'] = $player;
                    }
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Auto Attendant has been updated successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'delete_auto_attendant':
                    $int_id = $this->input->post('int_id');
                    
                    $this->auto_attendant_m->delete_auto_attendant($int_id);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Auto Attendant has been deleted successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'new_call_screen':
                   $new_extension =  $this->input->post('extension');
                    $extension = intval($new_extension);
                    $name = $this->input->post('name');
                    $voice_mail = $this->input->post('voice_mail');
                    $play_screen_message = $this->input->post('play_screen_message');
                    $play_screen_message = (isset($play_screen_message) && $play_screen_message == 1 ? '1' : '0');
                    $status = $this->input->post('status');
                    $status = '11';//Updating switch
                    $ring_back = $this->input->post('ring_back');
                    $timeout = $this->input->post('timeout');

                    $ext_values_array = array(
                        'Extension' => $extension,
                        'Name' => $name,
                        'VoiceMail' => $voice_mail,
                        'PlayScreenMessage' => $play_screen_message,
                        'Status' => $status,
                        'Ringback' => $ring_back,
                        'TimeOut' => $timeout
                    );
                    $ext_values_array['Account_Number'] = $sess_user_account_number;
                    $this->ct_call_screen_m->add_call_screen($ext_values_array);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Call Screen has been added successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'update_call_screen':
                    $int_id = $this->input->post('int_id');

                    $new_extension =  $this->input->post('extension');
                    $extension = intval($new_extension);
                    $name = $this->input->post('name');
                    $voice_mail = $this->input->post('voice_mail');
                    $play_screen_message = $this->input->post('play_screen_message');
                    $play_screen_message = (isset($play_screen_message) && $play_screen_message == 1 ? '1' : '0');
                    $status = $this->input->post('status');
                    $status = '11';//Updating switch
                    $ring_back = $this->input->post('ring_back');
                    $timeout = $this->input->post('timeout');
                    
                    $ext_values_array = array(
                        'Extension' => $extension,
                        'Name' => $name,
                        'VoiceMail' => $voice_mail,
                        'PlayScreenMessage' => $play_screen_message,
                        'Status' => $status,
                        'Ringback' => $ring_back,
                        'TimeOut' => $timeout
                    );
                    
                    $ext_values_array['Status'] = '11';
                    $this->ct_call_screen_m->update_call_screen($int_id, $ext_values_array);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Call Screen has been updated successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'delete_call_screen':
                    $int_id = $this->input->post('int_id');
                    
                    $this->ct_call_screen_m->delete_call_screen($int_id);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Call Screen has been deleted successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'new_phone_extension':
                    $new_extension =  $this->input->post('extension');
                    $extension = intval($new_extension);
                    $password = $this->input->post('password');
                    $caller_id = $this->input->post('caller_id');
                    $enabled = $this->input->post('enabled');

                    $ext_values_array = array(
                        'Extention' => $extension,
                        'Password' => $password,
                        'Caller_ID' => $caller_id,
                        'Enabled' => $enabled
                    );
                    
                    $ext_values_array['Account_Number'] = $sess_user_account_number;
                    $this->portal_ct_extensions_m->add_extension($ext_values_array);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Phone extension has been added successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'update_phone_extension':
                    $int_id = $this->input->post('int_id');

                    $new_extension =  $this->input->post('extension');
                    $extension = intval($new_extension);
                    $password = $this->input->post('password');
                    $caller_id = $this->input->post('caller_id');
                    $enabled = $this->input->post('enabled');

                    $ext_values_array = array(
                        'Extention' => $extension,
                        'Password' => $password,
                        'Caller_ID' => $caller_id,
                        'Enabled' => $enabled
                    );
                    
                    $ext_values_array['Status'] = '11';
                    $this->portal_ct_extensions_m->update_extension($int_id, $ext_values_array);
                    
                    $response['result'] = "success";
                    $response['caller_id'] = display_phone_number($caller_id);
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Phone extension has been updated successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'delete_phone_extension':
                    $int_id = $this->input->post('int_id');
                    $this->portal_ct_extensions_m->delete_extension($int_id);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Phone extension has been deleted successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'new_conf_rooms':
                    $name = $this->input->post('name');
                    $moderator_pin = $this->input->post('moderator_pin');
                    $participant_pin = $this->input->post('participant_pin');
                    $record = $this->input->post('record');
                    $secure = $this->input->post('secure');
                    $announce = $this->input->post('announce');
                    $mute_on_entry = $this->input->post('mute_on_entry');
                    $sounds = $this->input->post('sounds');
                    $max_memebers = $this->input->post('max_memebers');
                    $enabled = $this->input->post('enabled');

                    $ext_values_array = array(
                        'Name' => $name,
                        'Moderator_PIN' => $moderator_pin,
                        'Participant_PIN' => $participant_pin,
                        'Record' => $record,
                        'Secure' => $secure,
                        'Announce' => $announce,
                        'Mute_on_Entry' => $mute_on_entry,
                        'Sounds' => $sounds,
                        'Max_Memebers' => $max_memebers,
                        'Enabled' => $enabled,
                    );
                    
                    $ext_values_array['Account_Number'] = $sess_user_account_number;
                    $this->ct_conference_rooms_m->add_conf_room($ext_values_array);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Conference Bridge has been added successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'update_conf_rooms':
                    $int_id = $this->input->post('int_id');

                    $name = $this->input->post('name');
                    $moderator_pin = $this->input->post('moderator_pin');
                    $participant_pin = $this->input->post('participant_pin');
                    $record = $this->input->post('record');
                    $secure = $this->input->post('secure');
                    $announce = $this->input->post('announce');
                    $mute_on_entry = $this->input->post('mute_on_entry');
                    $sounds = $this->input->post('sounds');
                    $max_memebers = $this->input->post('max_memebers');
                    $enabled = $this->input->post('enabled');

                    $ext_values_array = array(
                        'Name' => $name,
                        'Moderator_PIN' => $moderator_pin,
                        'Participant_PIN' => $participant_pin,
                        'Record' => $record,
                        'Secure' => $secure,
                        'Announce' => $announce,
                        'Mute_on_Entry' => $mute_on_entry,
                        'Sounds' => $sounds,
                        'Max_Memebers' => $max_memebers,
                        'Enabled' => $enabled,
                    );

                    $this->ct_conference_rooms_m->update_conf_room($int_id, $ext_values_array);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Conference Bridge has been updated successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'delete_conf_rooms':
                    $int_id = $this->input->post('int_id');
                    
                    $this->ct_conference_rooms_m->delete_conf_room($int_id);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Conference Bridge has been deleted successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'new_voicemail':
                   $new_number =  $this->input->post('mailbox_number');
                    $mailbox_number = intval($new_number);
                    $password = $this->input->post('password');
                    $box_name = $this->input->post('box_name');
                    $email_notification = $this->input->post('email_notification');
                    $email_address = $this->input->post('email_address');
                    $delete_sent_message = $this->input->post('delete_sent_message');
                    $mute_on_entry = $this->input->post('mute_on_entry');
                    $sounds = $this->input->post('sounds');
                    $max_memebers = $this->input->post('max_memebers');
                    $enabled = $this->input->post('enabled');
                    // code by heng
                    $recording_upload = $_FILES['recording_upload'];
                    if($recording_upload['size'] == 0){
                        $contents = "";
                    }else{
                        $name = $recording_upload['name'];
                        $name_array = explode(".", $name);
                        $ext = end($name_array);
                        $valid_formats = array("mp3", "wav", "gsm");
                        if(in_array($ext,$valid_formats)){
                            /*$handle = fopen($recording_upload['tmp_name'], "r");
                            $contents = fread($handle, $recording_upload['size']);
                            $contents = addslashes($contents);
                            fclose($handle);*/
                            $contents = file_get_contents($recording_upload['tmp_name']);
                        }else{
                            $contents = "";
                        }
                    }
                    
                    $db_values_array = array(
                        '`mailbox`' => $mailbox_number,
                        '`password`' => $password,
                        '`fullname`' => $box_name,
                        '`sendvoicemail`' => $email_notification,
                        '`email`' => $email_address,
                        '`delete`' => $delete_sent_message,
                        '`Status`' => 11,
                    );
                    
                    if($contents != ""){
                        $db_values_array = array_merge($db_values_array, array('`Recording`' => $contents, '`Recording_format`' => $ext, '`Recording_name`' => $name));
                    }
                    
                    $db_values_array = array_merge($db_values_array, array('`customer_id`' => $sess_user_account_number));

                    $this->portal_voicemail_m->add_voice_mail($db_values_array);
                    
                    $response['result'] = "success";
                    $response['player'] = '';
                    if($contents != ""){
                        $file_path = base_url()."uploads/recording/".$name;
            
                        if($ext == "mp3"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }elseif($ext == "wav"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/wav">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }elseif($ext == "gsm"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/wav">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }
                        
                        $response['player'] = $player;
                    }
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Voicemail box has been added successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'update_voicemail':
                    $int_id = $this->input->post('int_id');

                    $new_number =  $this->input->post('mailbox_number');
                    $mailbox_number = intval($new_number);
                    $password = $this->input->post('password');
                    $box_name = $this->input->post('box_name');
                    $email_notification = $this->input->post('email_notification');
                    $email_address = $this->input->post('email_address');
                    $delete_sent_message = $this->input->post('delete_sent_message');
                    $mute_on_entry = $this->input->post('mute_on_entry');
                    $sounds = $this->input->post('sounds');
                    $max_memebers = $this->input->post('max_memebers');
                    $enabled = $this->input->post('enabled');
                    // code by heng
                    $recording_upload = $_FILES['recording_upload'];
                    $name = '';
                    if($recording_upload['size'] == 0){
                        $contents = "";
                    }else{
                        $name = $recording_upload['name'];
                        $name_array = explode(".", $name);
                        $ext = end($name_array);
                        $valid_formats = array("mp3", "wav", "gsm");
                        if(in_array($ext,$valid_formats)){
                            /*$handle = fopen($recording_upload['tmp_name'], "r");
                            $contents = fread($handle, $recording_upload['size']);
                            $contents = addslashes($contents);
                            fclose($handle);*/
                            $contents = file_get_contents($recording_upload['tmp_name']);
                        }else{
                            $contents = "";
                        }
                    }
                    
                    $db_values_array = array(
                        '`mailbox`' => $mailbox_number,
                        '`password`' => $password,
                        '`fullname`' => $box_name,
                        '`sendvoicemail`' => $email_notification,
                        '`email`' => $email_address,
                        '`delete`' => $delete_sent_message,
                        '`Status`' => 11,
                    );
                    
                    if($contents != ""){
                        $db_values_array = array_merge($db_values_array, array('`Recording`' => $contents, '`Recording_format`' => $ext, '`Recording_name`' => $name));
                    }
                    
                    $voice_mail_info = $this->portal_voicemail_m->get_ct_voice_mail_info("`uniqueid` = '" . $int_id ."'", TRUE);
                    
                    $this->portal_voicemail_m->update_voice_mail($int_id, $db_values_array);
                    
                    $response['result'] = "success";
                    $response['player'] = '';
                    if($contents == '' && $voice_mail_info['Recording_name'] != ''){
                        $name = $voice_mail_info['Recording_name'];
                        $ext = $voice_mail_info['Recording_format'];
                    }
                    
                    if($name != ''){
                        $file_path = base_url()."uploads/recording/".$name;
            
                        if($ext == "mp3"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }elseif($ext == "wav"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/wav">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }elseif($ext == "gsm"){
                            $player = '<audio controls>
                                        <source src="'.$file_path.'" type="audio/wav">
                                        Your browser does not support the audio element.
                                       </audio>';
                        }
                        
                        $response['player'] = $player;
                    }
                    
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Voicemail box has been updated successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'delete_voicemail':
                    $int_id = $this->input->post('int_id');
                    
                    $this->portal_voicemail_m->delete_voice_mail($int_id);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Voicemail box has been deleted successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'get_paypal_info':
                    $portal_info = $this->db_portal_info_m->get_version("", TRUE);
                    $response['result'] = "success";
                    $response['PayPalRate'] = $portal_info['PayPalRate'];
                    echo json_encode($response);
                    exit;
                break;
				case 'fb_profile_update':
					$this->load->model('ct_account_m');

					$member_facebook_id = $this->input->post('id');
					$account_number = $this->session->userdata('sess_user_account_number');

					$response = array();
					if($account_number > 0 && $account_number != ''){
						$this->ct_account_m->update_account($account_number, array('member_facebook_id' => $member_facebook_id));

						//Add user image to DB
						$member_image_name = get_facebook_profile_photo($account_number);

						$this->session->set_userdata('sess_user_photo', $member_image_name);

						$response = array("status" => 'SUCCESS');
					}

					echo json_encode($response);
                    exit;
				break;
                case 'download_request':
                    $this->load->model('cdr_download_m');
                    
                    $start_date = date("Y-m-d H:i:s",strtotime($this->input->post('start_date')));
                    $end_date = date("Y-m-d H:i:s",strtotime($this->input->post('end_date')));
                    $created_time = date("Y-m-d H:i:s",strtotime($this->input->post('created_time')));
                    $download = GenerateRandom(15);
                    
                    $request_array = array(
                        'Account_Number' => $sess_user_account_number,
                        'Start_Date' => $start_date,
                        'End_Date' => $end_date,
                        'Created_Time' => $created_time,
                        'Download' => $download
                    );
                    
                    $this->cdr_download_m->add_dropdown_request($request_array);
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>CDR Download has been requested successfully.</div>';
                    echo json_encode($response);
                    exit;
                break;
                case 'get_download_link':
                    $this->load->model('cdr_download_m');
                    
                    $key = $this->input->post('key');
                    
                    $cdr_download_info = $this->cdr_download_m->get_cdr_download_info("`ID` = '" . $key ."'");
                    
                    $response['download_link'] = site_url('cdr_download/cdrs/'.$cdr_download_info['Download']);

                    echo json_encode($response);
                    exit;
                break;
                case 'email_download_link':
                    
                    $this->load->library('email');
                    $this->load->model(array('ct_account_m', 'cdr_download_m'));
                    
                    $key = $this->input->post('key');
                    
                    $cdr_download_info = $this->cdr_download_m->get_cdr_download_info("`ID` = '" . $key ."'");
                    
                    $download_link = site_url('cdr_download/cdrs/'.$cdr_download_info['Download']);
                    
                    $user_info_array = $this->ct_account_m->get_account_info("Account_Number = '" . $sess_user_account_number . "'");
                    $email = $user_info_array['Login'];

                    if(count($user_info_array)){
                        //Email setup
                        $site_name = $this->config->item('site_name');
                        $email_from = $this->config->item('email_from');
                        $email_from_name = $this->config->item('email_from_name');
                        $site_url = $this->config->item('site_url');

                        $this->email->to($email);
                        $this->email->from($email_from, $email_from_name);
                        $this->email->subject('eTollfree CDR Download');
                        
                        $msg = '<html>
                                    <head>
                                        <title>CDR Download Link</title>
                                    </head>
                                    <body>
                                        <p>Dear <b>' . $user_info_array['Contact_FirstName'] . '</b>,</p>
                                        <p>CDR Download Link:</p>
                                        <p><a href="' . $download_link . '" target="_blank">'. $download_link .'</a></p>
                                    </body>
                                </html>';

                        $this->email->message($msg);
                        $this->email->send();
                        
                    }
                    
                    $response['result'] = "success";
                    $response['message'] = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Email has been sent to your email address.</div>';

                    echo json_encode($response);
                    exit;
                break;
                case 'reactivate':
                    $this->load->model('ct_account_m');

                    $response = array();
                    if($sess_user_account_number > 0 && $sess_user_account_number != ''){
                        $this->ct_account_m->update_account($sess_user_account_number, array('Status' => 10));
                    }
                    $response['result'] = "success";
                    echo json_encode($response);
                    exit;
                break;
                case 'disconnect_account':
                    $this->load->model('ct_orders_m');

                    $response = array();
                    
                    $order_db_values = array(
                        "Account_Number" => $sess_user_account_number,
                        "Order_Type" => 15
                    );

                    $order_id = $this->ct_orders_m->add_order($order_db_values);
                    
                    $response['result'] = "success";
                    echo json_encode($response);
                    exit;
                break;
                case 'feature_code_list':
                    $this->load->model('featurecode_list_m');
                    
                    $featurecode_list = $this->featurecode_list_m->get_featurecode_list();

                    $str_html = '<div class="table-responsive">';
                    $str_html .= '<table data-sortable class="table">
                        <thead>
                            <tr class="active">
                                <th>Feature</th>
                                <th>Feature Code</th>
                                <th>Instructions</th>
                            </tr>
                        </thead>
                        <tbody>
                    ';

                    if(is_array($featurecode_list) && count($featurecode_list)){
                        foreach($featurecode_list as $featurecode_info){
                            $str_html .= '<tr>
                                <td>' . $featurecode_info['Feature'] . '</td>
                                <td>' . $featurecode_info['Feature Code'] . '</td>
                                <td>' . $featurecode_info['Instructions'] . '</td>
                            </tr>
                            ';
                        }
                    }else{
                        $str_html .= '<tr><td align="center" colspan="3">No usage available.</td></tr>';
                    }

                    $str_html .= '
                        </tbody>
                    </table></div>';
                    echo $str_html;
                break;
                case 'submit_portal_message':
                    $this->load->model('ct_account_message_m');
                    
                    $response = array();
                    $ID = $this->input->post('id');
                    
                    $main_array = array(
                        'Display' => 0,
                    );
                    
                    $this->ct_account_message_m->update_account_messages_portal($ID, $main_array);
                    
                    $message_content = "";
                    
                    $messages_array = $this->ct_account_message_m->get_account_messages_portal("Account_Number = '" . $sess_user_account_number . "' AND Display = 1");
                    
                    if(is_array($messages_array) && count($messages_array)){
                        $message_num = count($messages_array);
                        $message_width = round(100 / $message_num, 5);
                        
                        $message_content .= '<div class="message-content text-center" id="customerMessage-content" style="width:'. $message_num*100 .'%;">';
                        $index = 1;
                        foreach($messages_array as $messages_info){
                            $message_content .= '<div class="message-content-item" style="width:'. $message_width .'%;" message-id="'. $messages_info['ID'] .'" data-slide="'. $index .'">';
                            $message_content .= $messages_info['Message1'];
                            $message_content .= '</div>';
                        $index++;
                        }
                        $message_content .= '</div>';
                    }

                    $response['result'] = "success";
                    $response['content'] = $message_content;
                    echo json_encode($response);
                    exit;
                break;
            }
        }
    }
}
