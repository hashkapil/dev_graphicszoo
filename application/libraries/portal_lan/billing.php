<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model('portal_billing_invoices_m');
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Billing";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
		$data['current_page'] = 'BILLING';
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		$this->load->view($this->config->item('portal_folder') . '/billing', $data);
	}

	//adding export functions for other here to download on 14/9/2017
	public function download_usage(){
		$this->load->model('portal_billing_usage_m');
		$sess_user_account_number = $this->session->userdata('sess_user_account_number');
		$billing_period = $this->input->get('billing_period');

		$usage_billing_list = $this->portal_billing_usage_m->get_usage_billing_list("Account_Number = '" . $sess_user_account_number . "' AND BillPeriod = '" . $billing_period . "'");

		 // Redirect output to a clients web browser (CSV)
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment;filename="' . $billing_period . '_usage_charge_download.csv"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings		
		$field_names_array = array("id","Bill Period", "Service Type", "Dest Type",  "Rate", "Charge", "MSF", "MST", "Total MOU",);
		fputcsv($output, $field_names_array);
		
		foreach($usage_billing_list as $cdr_list_info){
			fputcsv($output, $cdr_list_info);
		}
		exit;
	}
	
	public function download_MRC(){
		$this->load->model('portal_billing_rcc_m');
		$sess_user_account_number = $this->session->userdata('sess_user_account_number');
		$billing_period = $this->input->get('billing_period');

		$rcc_billing_list = $this->portal_billing_rcc_m->get_rcc_billing_list("Account_Number = '" . $sess_user_account_number . "' AND BillPeriod = '" . $billing_period . "'");

		 // Redirect output to a clients web browser (CSV)
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment;filename="' . $billing_period . '_MRC_Downloads.csv"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings		
		$field_names_array = array("id","Bill Period", "Service Type", "Quantity", "Rate", "Charge", "MSF", "MST");
		fputcsv($output, $field_names_array);
		
		foreach($rcc_billing_list as $cdr_list_info){
			fputcsv($output, $cdr_list_info);
		}
		exit;
	}
	
	public function download_other(){
		$this->load->model('portal_billing_other_m');
		$sess_user_account_number = $this->session->userdata('sess_user_account_number');
		$billing_period = $this->input->get('billing_period');

		$other_billing_list = $this->portal_billing_other_m->get_other_billing_list("Account_Number = '" . $sess_user_account_number . "' AND BillPeriod = '" . $billing_period . "'");
		
		 // Redirect output to a clients web browser (CSV)
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment;filename="' . $billing_period . '_Other_Charge_Downloads.csv"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings		
		$field_names_array = array("id","Bill Period", "Service Type","Details", "Quantity", "Rate", "Charge", "MSF", "MST");
		fputcsv($output, $field_names_array);
		
		foreach($other_billing_list as $cdr_list_info){
			fputcsv($output, $cdr_list_info);
		}
		exit;
	}
	////////////////////////////////////////////////////////////////
	public function invoice_listing(){
		$this->portal_billing_invoices_m->get_invoice_listing();
	}
}