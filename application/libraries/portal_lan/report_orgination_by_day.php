<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_orgination_by_day extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model('report_orgination_by_day_m');
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
        //code by heng
        $this->load->model('video_popup_m');
        // die( $this->config->item( 'portal_folder' ) ); // portal_lan
        
        $data['selected_video'] = "Report Origination";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
		$data['current_page'] = 'REPORTS';

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$this->load->view($this->config->item('portal_folder') . '/report_orgination_by_day', $data);
	}

	public function listing(){
		$this->report_orgination_by_day_m->get_listing();
	}
  public function telephone_listing()
  {
    $this->report_orgination_by_day_m->get_telephone_listing();
  }
  public function telephone_monthly_summary()
  {
        $this->load->model('video_popup_m');
        $data['selected_video'] = "Report Origination";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        $data['current_page'] = 'REPORTS';

        $sess_user_account_number = $this->session->userdata('sess_user_account_number');
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

    $this->load->view($this->config->item('portal_folder') . '/telephone_monthly_summary', $data);
  }
}