<?php
	class Payment extends Front_controller
	{
		public function __construct(){
			parent::__construct();

			$this->load->model(array('ct_account_m', 'ct_cc_m', 'ct_payments_m', 'ct_ivr_m', 'ct_payment_confirm_message_m'));
			$this->config->load('paypal');
			
		}

		public function payment_process($payment_type){
			$this->userauth->is_login();

			if($payment_type == 'paypal'){
				$amount = $this->input->post('amount');

				if($amount > 0){
					$data['amount'] = $amount;

					$sess_user_account_number = $this->session->userdata('sess_user_account_number');
					$portal_account_info = $this->ct_account_m->get_account_info("Account_Number = '" . $sess_user_account_number . "'");
					$data['portal_account_info'] = $portal_account_info;

					$paypal_log_data = array(
						'paypal_trans_type' => 'PORTAL_PAYMENT',
						'cust_account_number' => $sess_user_account_number,
						'cust_email_address' => (isset($portal_account_info['Login']) ? $portal_account_info['Login'] : ''),
						'cust_first_name' => (isset($portal_account_info['Contact_FirstName']) ? $portal_account_info['Contact_FirstName'] : ''),
						'cust_last_name' => (isset($portal_account_info['Contact_LastName']) ? $portal_account_info['Contact_LastName'] : ''),
						'cust_address' => (isset($portal_account_info['Address']) ? $portal_account_info['Address'] : ''),
						'cust_city' => (isset($portal_account_info['City']) ? $portal_account_info['City'] : ''),
						'cust_state' => (isset($portal_account_info['State']) ? $portal_account_info['State'] : ''),
						'cust_zip_code' => (isset($portal_account_info['ZipCode']) ? $portal_account_info['ZipCode'] : ''),
						'cust_ip_address' => $this->input->ip_address(),
						'paypal_trans_amount' => $amount,
						'paypal_trans_id' => '',
						'paypal_trans_status' => 'CHECKOUT'
					);

					$paypal_log_id = $this->ct_cc_m->add_paypal_log($paypal_log_data);
					$data['paypal_log_id'] = $paypal_log_id;

					$paypal_invoice_number = date("HisdmY") . '_' . $paypal_log_id;
					$data['paypal_invoice_number'] = $paypal_invoice_number;

					$this->ct_cc_m->update_paypal_log($paypal_log_id, array('paypal_invoice_number' => $paypal_invoice_number));
					

					$this->load->view($this->config->item('portal_folder') . '/paypal_payment_process', $data);
				}else{
					redirect(site_url('my_account'));
				}
			}
		}

		public function payment_return_process(){
			$this->load->library('email');

			if(isset($_POST) && is_array($_POST) && count($_POST)){
				$req = 'cmd=_notify-validate';
				foreach($_POST as $key => $value)
				{
					$value = urlencode(stripslashes($value));
					$req .= "&$key=$value";
				}

				//mail('raju.rajpurohit@gmail.com', 'paypal_test', 'Paypal Params : ' . $req);

				// post back to PayPal system to validate
				$header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
				//$header .= "Host: www.paypal.com\r\n";
				$header .= "Host: www.sandbox.paypal.com\r\n";
				$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
				$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

				// If testing on Sandbox use:
				if($this->config->item('paypal_status') == 'live'){
					$fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);
				}else{
					$fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
				}

				$account_number = $_POST['custom'];
				$invoice_array = explode('_', $_POST['invoice']);
				$paypal_log_id = $invoice_array[1];

				$txn_id = $_POST['txn_id'];

				if((int)$paypal_log_id > 0){
					$paypal_db_log_array = array('paypal_trans_status' => 'CONFIRMED', 'paypal_trans_id' => $txn_id);
					$this->ct_cc_m->update_paypal_log($paypal_log_id, $paypal_db_log_array);

					$paypal_log_info = $this->ct_cc_m->get_paypal_log_info("paypal_log_id = '" . $paypal_log_id . "'");

					$payment_db_array = array(
						'Account_Number' => $account_number,
						'Amount' => $paypal_log_info['paypal_trans_amount'],
						'Transaction_ID' => $txn_id,
						'PaymentType' => 'Paypal',
						'Ip' => $paypal_log_info['cust_ip_address']
					);
					
					$this->ct_payments_m->add_payment($payment_db_array);

					if($paypal_log_info['paypal_trans_type'] == 'NS_PAYMENT'){
						//Loading etollfree Lib functions
						$this->load->library('etollfree');
						$etoll = new etollfree;

						$extra_params = unserialize($paypal_log_info['extra_params']);
						if(is_array($extra_params) && count($extra_params)){

							if($extra_params['product'] == 'toll_free' || $extra_params['product'] == 'vanity_toll_free'){
								$reserve = $etoll->reserveTollFree($extra_params['number'], $account_number, $extra_params['order_id']);
							}else{
								if($extra_params['country_id'] == 211){//local
									$reserve = $etoll->reserveLocal($extra_params['number'], $extra_params['provider'], $extra_params['lata'], $account_number, $extra_params['vendor_id'], $extra_params['order_id']);
								}else{//international
									$reserve = $etoll->reserveInternational($extra_params['number'], $extra_params['vendor_id'], $account_number, $extra_params['order_id']);
								}
							}

							/*if($extra_params['country_id'] == 211){//local
								$reserve = $etoll->reserveLocal($extra_params['number'], $extra_params['provider'], $extra_params['lata'], $extra_params['state_code'], $account_number, $extra_params['rc'], $extra_params['immdkey'], $extra_params['pg'], $extra_params['vendor_id'], $extra_params['ring_data'], $extra_params['ring_type'], $extra_params['setup_fee'], $extra_params['monthly_fee'], $extra_params['per_minute_fee']);
							}else{//international
								$reserve = $etoll->reserveInternational($extra_params['number'], $extra_params['vendor_id'], $account_number, $extra_params['ring_data'], $extra_params['ring_type'], $extra_params['setup_fee'], $extra_params['monthly_fee'], $extra_params['per_minute_fee']);
							}*/
						}

						//Update trial
						//$this->db->query("call `OR_NewOrder`('" . $txn_id . "');");

					}else if($paypal_log_info['paypal_trans_type'] == 'AUTO_QUES_PAYMENT'){
						$extra_params = unserialize($paypal_log_info['extra_params']);
						if(is_array($extra_params) && count($extra_params)){
							$ivr_data = array(
								'Account_Number' => $account_number,
								'Status' => '11',
								'Extension' => $extra_params['ivr_extension'],
								'IVR_Name' => $extra_params['attend_name'],
								'Ringback' => $extra_params['ringback'],
								'Extension_Dial' => $extra_params['allow_ext_dial'],
								'SetupFee' => $extra_params['setup_fee'],
								'talent_key' => $extra_params['talent'],
								'Message' => $extra_params['attendant_message'],
								'Greeting_format' => $extra_params['recording_file_ext'],
								'Greeting_name' => $extra_params['org_recording_file']
							);

							if($extra_params['recording_type'] == 'UPLOAD'){
								$temp_path = $this->config->item('upload_path') . 'record_temp/';
								
								$ffp = fopen($temp_path . $extra_params['recording_file'], 'r');
								$content = fread($ffp, filesize($temp_path . $extra_params['recording_file']));
								$content = addslashes($content);
								fclose($ffp);

								unlink($temp_path . $extra_params['recording_file']);

								$ivr_data['Greeting'] = $content;
							}

							$key = $this->ct_ivr_m->add_ivr($ivr_data);

							$selected_caller_options = unserialize($extra_params['selected_caller_options']);

							if(is_array($selected_caller_options) && count($selected_caller_options)){
								foreach($selected_caller_options as $caller_option_info){
									$ivr_detail_db_values = array(
										'IVR_Key' => $key,
										'Option' => $caller_option_info['caller_option'],
										'RingToType' => $caller_option_info['ring_type'],
										'RingToData' => $caller_option_info['ring_to_data']
									);

									$this->ct_ivr_m->add_ivr_details($ivr_detail_db_values);
								}
							}
						}
					}
				}

				if ($fp)
				{
					fputs($fp, $header . $req);
					while (!feof($fp))
					{
						$res = fgets ($fp, 1024);
						if (strcmp ($res, "VERIFIED") == 0){}
					}
				}

				fclose ($fp);
			}

			exit;
		}

		function payment_success() {
            $this->session->set_flashdata('action','after_payment');
			
			$txn_id = $_REQUEST['txn_id'];
			
			//add record to check if success message has shown or not
			$payment_confirm_message_db_values = array(
				'pcm_txn_id' => $txn_id, 
				'pcm_ip_address' => $this->input->ip_address(),
				'pcm_payment_created' => date("Y-m-d H:i:s") 
			);

			//INSERT IN DB_CT_payment_confirm_message_log TABLE
			$order_id = $this->ct_payment_confirm_message_m->add_payment_confirm_message_log($payment_confirm_message_db_values);
			
			$this->session->set_flashdata('success', 'Your payment has been approved');
			redirect(site_url('my_account'));
		}
	}
?>
