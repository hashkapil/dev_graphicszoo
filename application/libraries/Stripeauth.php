<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stripeauth {

    public function __construct() {
        $CI = &get_instance();
        $CI->load->library('session');
        $CI->load->model('Admin_model', '', TRUE);
        $CI->load->model('Welcome_model', '', TRUE);
        $CI->load->model('Request_model', '', TRUE);
        $CI->load->model('Admin_model', '', TRUE);
        $CI->load->model('Category_model', '', TRUE);
        $CI->load->library('Myfunctions');

    }
    
    public function strip_auth($login_user_id,$action,$redirect="") {
        if($redirect == ""){
            $redirect = base_url().'customer/client_management';
        }
        $CI = &get_instance();
        $output = array();
        if (isset($_GET['code'])) { // Redirect w/ code
            $code = $_GET['code'];
            $token_request_body = array(
                'client_secret' => API_KEY,
                'grant_type' => 'authorization_code',
                'client_id' => CLIENT_ID,
                'code' => $code,
            );
            $req = curl_init(TOKEN_URI);
            curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($req, CURLOPT_POST, true);
            curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
            // TODO: Additional error handling
            $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
            $resp = json_decode(curl_exec($req), true);
            curl_close($req);
            if(isset($resp['stripe_user_id'])){
                $user_options = array("stripe_account_id" => $resp['stripe_user_id'],
                                "stripe_api_key" => $resp['access_token'],
                                "user_id" => $login_user_id);
                if($action == "update"){
                    $success = $CI->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));
                }else{
                    $success = $CI->Welcome_model->insert_data("agency_user_options", $user_options);
                }
                    if($success){
                     $CI->session->set_flashdata('message_success', "Your stripe account is created successfully",5);
                        redirect($redirect);
                    }else{
                        $CI->session->set_flashdata('message_error', "Your stripe account is not created successfully",5);
                        redirect($redirect);
                    }
            }else{
//                die("hello");
                $CI->session->set_flashdata('message_error', $resp['error_description'],5);
                redirect($redirect);
            }
            $output['data'] = $resp;
            $output['status'] = 'code';
          // echo "<pre>";print_r($resp);
        } else if (isset($_GET['error'])) { // Error
            $output['data'] = $_GET['error_description'];
            $output['status'] = 'error';
        } else {
            $output['status'] = 'no response';
        } 
        return $output;
}
    public function strip_auth_link() {
            $authorize_request_body = array(
                'response_type' => 'code',
                'scope' => 'read_write',
                'client_id' => CLIENT_ID
            );
            $url = AUTHORIZE_URI . '?' . http_build_query($authorize_request_body);
            return $url;
    }
    
    public function setapikeyforstripe($api,$autoupgrade) {
        $CI = &get_instance();
        if($autoupgrade != ""){
            $apikey = $api;
        }else{
            if($api != "" && API_KEY != $api){

                $apikey = $api;
            }else{
                $getuser = $CI->Request_model->getuserinfobydomain(HOST_NAME);
                $login_user_id = $CI->load->get_var('login_user_id'); 
                if(!empty($getuser) && $getuser[0]['stripe_api_key'] !== "" && $login_user_id != $getuser[0]['user_id']){
                   $apikey = $getuser[0]['stripe_api_key']; 
                }else{
                    $apikey = API_KEY;
                }
            }
        }
       // echo $api.' - '.$apikey;
       // die($apikey);
        return $apikey;
    }


}