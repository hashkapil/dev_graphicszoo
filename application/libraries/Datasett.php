<?php

class Datasett implements \JsonSerializable
{
	protected $label = false;
	protected $data  = [];

	public function __construct($label = NULL, $data = NULL)
	{
		$this->label = $label;
		$this->data  = $data;
	}

	public function jsonSerialize() {
        return [
        	$this->label => [
        		$this->data
        	]
        ];
    }

}