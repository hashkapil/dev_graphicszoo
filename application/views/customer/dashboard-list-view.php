<style type="text/css">
	.list-header-blog > li.active a, .list-header-blog > li a:hover{
		color: #e52344;
    border-bottom: 2px solid #e52344;
	}
	h3.app-roved.gray {
    text-align: center;
    font: normal 15px/25px 'GothamPro-Medium', sans-serif;
}

p.green {
	    font: normal 13px/20px 'GothamPro', sans-serif;
	    color: #43c572;
	}
	h4.head-c.draft_no {
	    font: normal 14px/50px 'GothamPro', sans-serif;
	}
	.select-pro-a select::-ms-expand {
	    display: none;
	}
		button.btn.btn-y {
		background: #37c473;
		color: #fff;
		width: 30%;
	}
	button.btn.btn-n {
	    background: #e73250;
	    color: #fff;
	    width: 30%;
	}
	#myModal .modal-dialog {
	    width: 480px;
	    margin: 50px auto;
	}
	#myModal .modal-content{
		height: 200px;
	}
	#myModal .cli-ent-model {
		padding: 60px;
	}
	a.reddelete i {
	    font-size: 20px;
	    color: #e52344;
	}
	a.reddelete {
	    position: absolute;
	    right: -40px;
	    top: -45px;
	    background: #dddddd80;
	    padding: 3px;
	    border-radius: 50%;
	    width: 29px;
	    text-align: center;
	    height: 29px;
	}
	p.pro-a.inline-per {
	    position: relative;
	}
	.pro-desh-row {
		display: table;
		overflow: auto;
	}
</style>
<?php //echo "<pre>"; print_r($complete_project); exit;?>
<section class="con-b">
		<div class="container">
			<div class="header-blog">
				<?php if ($this->session->flashdata('message_error') != '') { ?>
						<div class="alert alert-danger alert-dismissable">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
							<strong>
								<?php echo $this->session->flashdata('message_error'); ?>
							</strong>
						</div>
						<?php } ?>

						<?php if ($this->session->flashdata('message_success') != '') { ?>
						<div class="alert alert-success alert-dismissable">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
							<strong>
								<?php echo $this->session->flashdata('message_success'); ?>
							</strong>
						</div>
						<?php } ?>
				<div class="row flex-show">
					<div class="col-md-7">
						<ul id="status_check" class="list-unstyled list-header-blog">
							<li class="nav-item active" id="1"><a data-status="active,disapprove,assign,pending,checkforapprove" data-toggle="tab" href="#designs_request_tab" role="tab">Active (<?php echo (count($active_project)); ?>)</a></li>

							<li class="nav-item" id="2"><a class="nav-link tabmenu" data-status="draft" data-toggle="tab" href="#inprogressrequest" role="tab">Draft (<?php echo count($draft_project); ?>)</a></li>

							<li class="nav-item" id="3"><a class="nav-link tabmenu" data-status="approved" data-toggle="tab" href="#approved_designs_tab" role="tab">Completed (<?php echo count($complete_project); ?>) </a></li>        
						</ul>
					</div>
					<div class="col-md-4">
						<div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata" id="search_text">
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <!--<img src="images/icon-search.png" class="img-responsive">-->
                                    <svg class="icon">
                                        <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                    </svg>
                                </button>
                            </form>
                        </div>
					</div>
					<div class="col-md-1">
						<div class="show-blog-xx2">
							<ul class="list-unstyled show-blog-list-xx2">
								<li class="active"><a href="<?php echo base_url() ?>customer/request/dashboard-list-view">
									<svg class="icon list">
										<use xlink:href="<?php echo base_url() ?>theme/customer-assets/images/symbol-defs.svg#icon-menu-list"></use>
									</svg>
								</a></li>
								<li ><a href="<?php echo base_url() ?>customer/request/design_request">
									<svg class="icon">
										<use xlink:href="<?php echo base_url() ?>theme/customer-assets/images/symbol-defs.svg#icon-menu-grid"></use>
									</svg>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<p class="space-c"></p>

			<div class="tab-content">
				<!-- Design Request Tab -->
				<div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
					<div class="pro-deshboard-list">
						<div class="cli-ent-add-row">
							<a href="<?php echo base_url() ?>customer/request/new_request/category" class="cli-ent-add-col" >
								<span class="cli-ent-add-icon">+</span>
							</a>
						</div>
						<?php $activeTab = 0;
							foreach ($active_project as $project) {
								if($project['status']=="assign"){
									$activeTab++;
								}
							}
							 ?>
						<!-- For Active Projects -->
						<div id="prior_data">
						<?php for ($i=0; $i < count($active_project); $i++) {
							if($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "checkforapprove" || $active_project[$i]['status'] == "disapprove" ){
							?>
							<!-- List Desh Product -->
								<div class="pro-desh-row">
									<div class="pro-desh-box delivery-desh">
										<p class="pro-a">
											<?php if($active_project[$i]['status'] == "active") { 
												echo "Expected on";
											}elseif ($active_project[$i]['status'] == "checkforapprove") {
												echo "Delivered on";
											}elseif ($active_project[$i]['status'] == "disapprove") {
												echo "Expected on";
											} ?>
										</p>
										<p class="pro-b">
										<?php if($active_project[$i]['status'] == "active") {
												echo date('m/d/Y', strtotime($active_project[$i]['expected']));
											}elseif ($active_project[$i]['status'] == "checkforapprove") {
												echo date('m/d/Y', strtotime($active_project[$i]['deliverydate']));
											}elseif ($active_project[$i]['status'] == "disapprove") {
												echo date('m/d/Y', strtotime($active_project[$i]['expected']));
											} ?>						
										</p>
										<div class="pro-inrightbox">
											<?php if($active_project[$i]['status'] != "assign") {?>
												<p class="green">Active</p>
											<?php } ?>
										</div>
									</div>
									
									<div class="pro-desh-box dcol-1" style="cursor:pointer;" onclick="window.location= '<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>/1' ">
										<div class="desh-head-wwq">
											<div class="desh-inblock">
												<h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>/1"><?php echo $active_project[$i]['title']; ?></a></h3>
												<p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
											</div>
											<div class="desh-inblock">
												<p class="neft pull-right">
												<?php 
				                  if ($active_project[$i]['status'] == "checkforapprove") {
				                      $status = "Review your design";
				                      $color = "bluetext";
				                  } elseif ($active_project[$i]['status'] == "active") {
				                      $status = "Design In Progress";
													$color = "blacktext";
				                  } elseif ($active_project[$i]['status'] == "disapprove") {
				                      $status = "Revision In Progress";
				                      $color = "orangetext ";
				                  } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
				                      $status = "In Queue";
				                      $color = "greentext ";
				                  } else {
				                      $status = "";
				                      $color = "greentext ";
				                  }
				                  
				                ?>
				                <?php if ($status == 'In Queue'): ?>
			                    	<span class="gray text-uppercase">
			                    		<?php echo $status; ?>
									</span>
								<?php else: ?>
									<span class="green <?php echo $color; ?> text-uppercase">
        								<?php echo $status; ?>
									</span>
			                    <?php endif ?>
				                 </p>
											</div>
										</div>
									</div>
									
									<div class="pro-desh-box dcol-2">
										<div class="pro-circle-list clearfix">
											<div class="pro-circle-box">
												<a href=""><figure class="pro-circle-img">
													<?php if($active_project[$i]['profile_picture']){ ?>
							                <img src="<?php echo base_url()."uploads/profile_picture/".$active_project[$i]['profile_picture']; ?>" class="img-responsive" />
							            <?php } else{ ?>
							            	<img src="<?php echo base_url()."uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg" ?>" class="img-responsive" />
							            <?php } ?>
												</figure>
												<p class="pro-circle-txt text-center">
													<?php echo $active_project[$i]['designer_first_name']; ?>
												</p></a>
											</div>
										</div>
									</div>
									
									<div class="pro-desh-box">
										<div class="pro-desh-r1">
											<div class="pro-inleftbox">
											<p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>/1"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0){ ?>
																	<span class="numcircle-box">
																		<?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
																	</span>
																<?php } ?></span></a></p>
											</div>
											<div class="pro-inrightbox">
												<p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>/1"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
													<?php if(count($active_project[$i]['files_new']) != 0){ ?>
														<span class="numcircle-box">
															<?php echo count($active_project[$i]['files_new']); ?>
														</span>
													<?php } ?>
												</span>
												<?php echo count($active_project[$i]['files']) ;?>
												</a>
											 <a href="<?php echo base_url();?>customer/request/deleteproject/<?php echo $active_project[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a></p>
											</div>
										</div>
									</div>
								</div> 
							<!-- -->
						<?php } } ?>
						<?php for ($i=0; $i < count($active_project); $i++) {
							if($active_project[$i]['status'] != "active" && $active_project[$i]['status'] != "checkforapprove" && $active_project[$i]['status'] != "disapprove" ){
							?>
							<!-- List Desh Product -->
								<div class="pro-desh-row">
									<div class="pro-desh-box delivery-desh" style="padding-right: 0px; padding-left: 15px;">
										<!-- <p class="pro-a">Delivery</p>
										<p class="pro-b">
										<?php
											$created_date =  date($active_project[$i]['dateinprogress']);
											$expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
											if ( $active_project[ $i ][ 'modified' ] == "" ) {
												echo $expected_date;
											} else {
												echo date( "m/d/Y", strtotime( $active_project[ $i ][ 'modified' ] ) );
											}
										?>							
										</p> -->
										<p class="pro-a">Priority
														<span class="select-pro-a">
															<select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?> ,<?php echo $active_project[$i]['id'] ?>)">
																<?php for($j=1;$j<=$activeTab;$j++){ ?>
																<option value="<?php echo $j; ?>"
																	<?php if($j == $active_project[$i]['priority']){echo "selected";} ?>
																	><?php echo $j; ?></option>												
															<?php } ?>
															</select>
														</span>
														</p>
									</div>
									
									<div class="pro-desh-box dcol-1" style="cursor:pointer;" onclick="window.location= '<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>/1' ">
										<div class="desh-head-wwq">
											<div class="desh-inblock">
												<h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>/1"><?php echo $active_project[$i]['title']; ?></a></h3>
												<p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
											</div>
											<div class="desh-inblock">
												<p class="neft pull-right">
												<?php 
				                  if ($active_project[$i]['status'] == "checkforapprove") {
				                      $status = "Review your design";
				                      $color = "greentext";
				                  } elseif ($active_project[$i]['status'] == "active") {
				                      $status = "Design In Progress";
													$color = "blacktext";
				                  } elseif ($active_project[$i]['status'] == "disapprove") {
				                      $status = "Revision In Progress";
				                      $color = "orangetext ";
				                  } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
				                      $status = "In Queue";
				                      $color = "greentext ";
				                  } else {
				                      $status = "";
				                      $color = "greentext ";
				                  }
				                  
				                ?>
				                <?php if ($status == 'In Queue'): ?>
			                    	<span class="gray text-uppercase">
			                    		<?php echo $status; ?>
									</span>
								<?php else: ?>
										<span class="green text-uppercase">
            								<?php echo $status; ?>
										</span>
			                    <?php endif ?>
				                 </p>
											</div>
										</div>
									</div>
									
									<div class="pro-desh-box dcol-2">
										<div class="pro-circle-list clearfix">
											<div class="pro-circle-box">
												 <h4 class="head-c draft_no">No Designer assigned yet</h4>
												<!-- <a href=""><figure class="pro-circle-img">
													<?php if($active_project[$i]['profile_picture']){ ?>
							                <img src="<?php echo base_url()."uploads/profile_picture/".$active_project[$i]['profile_picture']; ?>" class="img-responsive" />
							            <?php } else{ ?>
							            	<img src="<?php echo base_url()."uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg" ?>" class="img-responsive" />
							            <?php } ?>
												</figure>
												<p class="pro-circle-txt text-center">
													<?php echo $active_project[$i]['designer_first_name']; ?>
												</p></a> -->
											</div>
										</div>
									</div>
									
									<div class="pro-desh-box">
										<div class="pro-desh-r1">
											<div class="pro-inleftbox">
												<p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>/1"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0){ ?>
																	<span class="numcircle-box">
																		<?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
																	</span>
																<?php } ?></span></a></p>
											</div>
											<div class="pro-inrightbox">
												<p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>/1"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
													<?php if(count($active_project[$i]['files_new']) != 0){ ?>
														<span class="numcircle-box">
															<?php echo count($active_project[$i]['files_new']); ?>
														</span>
													<?php } ?></span>
												<?php echo count($active_project[$i]['files']) ;?></a>
											<a href="<?php echo base_url();?>customer/request/deleteproject/<?php echo $active_project[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a></p>
											</div>
										</div>
									</div>
								</div> 
							<!-- -->
						<?php } } ?>
						</div>
					</div>

					
					<p class="space-e"></p>
					
					<!-- <div class="loader">
						<a class="loader-link text-uppercase" href=""><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
					</div> -->
				</div>

				<!-- In Progress Request Tab-->
				<div class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
					<div class="cli-ent-add-row">
							<a href="<?php echo base_url() ?>customer/request/new_request/category" class="cli-ent-add-col" >
								<span class="cli-ent-add-icon">+</span>
							</a>
						</div>
					<!-- For Drafted Projects -->
					<?php for ($i=0; $i < count($draft_project); $i++) { ?>
						<!-- List Desh Product -->
							<div class="pro-desh-row">
								<div class="pro-desh-box delivery-desh">
									<h3 class="app-roved gray">DRAFT</h3>
								</div>
								
								<div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location= '<?php echo base_url(); ?>customer/request/project-info/<?php echo $draft_project[$i]['id']; ?>/2' ">
									<div class="desh-head-wwq">
										<div class="desh-inblock">
											<h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $draft_project[$i]['id']; ?>/2"><?php echo $draft_project[$i]['title']; ?></a></h3>
												<?php if (!empty($draft_project[$i]['category'])): ?>
													<?php $cat = $draft_project[$i]['category']; ?>
												<?php else: ?>
													$cat = "Not Categorized";
												<?php endif; ?>
											<!-- <p class="pro-b"><?php echo $cat; ?></p> -->
										</div>
										<div class="desh-inblock">
											<p class="neft pull-right"><span class="gray text-uppercase">
											<?php 
			                  echo $draft_project[$i]['status'];
			                ?>
			                  </span></p>
										</div>
									</div>
								</div>
								
								<div class="pro-desh-box dcol-2">
									<div class="pro-circle-list clearfix">
										<?php if ($draft_project[$i]['designer_first_name']): ?>
											<div class="pro-circle-box">
											<a href=""><figure class="pro-circle-img">
												<?php if($draft_project[$i]['profile_picture']){ ?>
						                <img src="<?php echo base_url()."uploads/profile_picture/".$draft_project[$i]['profile_picture']; ?>" class="img-responsive" />
						            <?php } else{ ?>
						            	<img src="<?php echo base_url()."uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg" ?>" class="img-responsive" />
						            <?php } ?>
											</figure>
											<p class="pro-circle-txt text-center">
												<?php echo $draft_project[$i]['designer_first_name']; ?>
											</p></a>
										</div>

										<?php else: ?>
											<p class="text-muted">No designer assigned yet</p>

										<?php endif ?>
										
									</div>
								</div>
								
								<div class="pro-desh-box">
									<div class="pro-desh-r1">
										<div class="pro-inleftbox">
											<p class="pro-a inline-per"><a href=""><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php echo $draft_project[$i]['total_chat_all']; ?></a></p>
										</div>
										<div class="pro-inrightbox">
											<p class="pro-a inline-per"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
												<?php  echo count($draft_project[$i]['files']); ?>
												
										</p>
										</div>
									</div>
								</div>
							</div> 
						<!-- -->
					<?php } ?>

					<p class="space-e"></p>
					
					<!-- <div class="loader">
						<a class="loader-link text-uppercase" href=""><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
					</div> -->
				</div>

				<!-- Approved Designs Tab -->
				<div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
					<div class="cli-ent-add-row">
							<a href="<?php echo base_url() ?>customer/request/new_request/category" class="cli-ent-add-col" >
								<span class="cli-ent-add-icon">+</span>
							</a>
						</div>
					<!-- For Completed Projects -->
					<?php for ($i=0; $i < count($complete_project); $i++) { ?>
						<!-- List Desh Product -->
							<div class="pro-desh-row">
								<div class="pro-desh-box delivery-desh">
									<p class="pro-a">Approved on</p>
									<p class="pro-b">
									<?php
										echo date('m/d/Y', strtotime($complete_project[$i]['approvddate']));
									?>						
									</p>
								</div>
								
								<div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location= '<?php echo base_url(); ?>customer/request/project-info/<?php echo $complete_project[$i]['id']; ?>/3' ">
									<div class="desh-head-wwq">
										<div class="desh-inblock">
											<h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $complete_project[$i]['id']; ?>/3"><?php echo $complete_project[$i]['title']; ?></a></h3>
											<p class="pro-b"><?php echo $complete_project[$i]['category']; ?></p>
										</div>
										<div class="desh-inblock">
											<p class="neft pull-right"><span class="green text-uppercase">
											<?php 
			                  if ($complete_project[$i]['status'] == " checkforapprove") {
			                      $status = "Review your design";
			                      $color = "greentext";
			                  } elseif ($complete_project[$i]['status'] == "active") {
			                      $status = "Design In Progress";
												$color = "blacktext";
			                  } elseif ($complete_project[$i]['status'] == "disapprove") {
			                      $status = "Revision In Progress";
			                      $color = "orangetext ";
			                  } elseif ($complete_project[$i]['status'] == "pending" || $complete_project[$i]['status'] == "assign") {
			                      $status = "In Queue ";
			                      $color = "greentext ";
			                  } else {
			                      $status = "Completed";
			                      $color = "greentext ";
			                  }
			                  echo $status;
			                ?>
			                  </span></p>
										</div>
									</div>
								</div>
								
								<div class="pro-desh-box dcol-2">
									<div class="pro-circle-list clearfix">
										<div class="pro-circle-box">
											<a href=""><figure class="pro-circle-img">
												<?php if($complete_project[$i]['profile_picture']){ ?>
						                <img src="<?php echo base_url()."uploads/profile_picture/".$complete_project[$i]['profile_picture']; ?>" class="img-responsive" />
						            <?php } else{ ?>
						            	<img src="<?php echo base_url()."uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg" ?>" class="img-responsive" />
						            <?php } ?>
											</figure>
											<p class="pro-circle-txt text-center">
												<?php echo $complete_project[$i]['designer_first_name']; ?>
											</p></a>
										</div>
									</div>
								</div>
								
								<div class="pro-desh-box">
									<div class="pro-desh-r1">
										<div class="pro-inleftbox">
											<p class="pro-a inline-per"><a style="position: relative;" href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
												<?php if($complete_project[$i]['total_chat'] + $complete_project[$i]['comment_count'] != 0){ ?>
													<span class="numcircle-box">
														<?php echo $complete_project[$i]['total_chat'] + $complete_project[$i]['comment_count']; ?>
													</span>
												<?php } ?>
											</span>
											</a></p>
										</div>
										<div class="pro-inrightbox">
											<p class="pro-a inline-per"><a style="position: relative;" href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13"><?php if(count($complete_project[$i]['files_new']) != 0){ ?>
												<span class="numcircle-box">
													<?php echo count($complete_project[$i]['files_new']); ?>
												</span>
											<?php } ?>
										</span>
										<?php echo count($complete_project[$i]['files']) ;?>
												</a>
											</p>
										</div>
									</div>
								</div>
							</div> 
						<!-- -->
					<?php } ?>

					<p class="space-e"></p>
					
					<!-- <div class="loader">
						<a class="loader-link text-uppercase" href=""><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
					</div> -->
				</div>	
			</div>
		</div>
	</section>
<!-- Modal -->
<button style="display: none;" id="confirmation" data-toggle="modal" data-target="#myModal">click here</button>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<input type="hidden" name="priorityFrom" id="priorityFrom" value=""/>
				<input type="hidden" name="priorityto" id="priorityto" value=""/>
				<input type="hidden" name="id" id="id" value=""/>
				<div class="cli-ent-model-box">
					<div class="cli-ent-model">
						<header class="fo-rm-header">
							<h3 class="head-c text-center">Are you sure you want to change the priority of this project?</h3>
						</header>
						<div class="confirmation_btn text-center">
							<button class="btn btn-y" data-dismiss="modal" aria-label="Close">Yes</button>
							<button class="btn btn-n" data-dismiss="modal" aria-label="Close">No</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- Modal -->
	<!-- <script>
		$(document).ready(function() {

    $('#profile-click tr').click(function() {
        var href = $(this).find("a").attr("href");
        if(href) {
            window.location = <?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>;
        }
    });

    $('#profile-click tr').hover(function() {
        $(this).css('cursor','pointer');
    });

});
	</script> -->
    <!-- jQuery (necessary for JavaScript plugins) -->
    <script src="<?php echo base_url() ?>theme/customer-assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>theme/customer-assets/js/bootstrap.min.js"></script>
    <script>
	function prioritize(priorityto,priorityfrom, id){
		$('#confirmation').click();
		$('#priorityFrom').val(priorityfrom);
		$('#priorityto').val(priorityto);
		$('#id').val(id);
	}
	$('.btn-y').click(function(){
		var priorityfrom = $('#priorityFrom').val();
		var priorityto = $('#priorityto').val();
		var id = $('#id').val();
		var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("prior_data").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET", "prioritySetforlistview/"+priorityto+"/"+priorityfrom+"/"+id,true);
        xmlhttp.send();

	});
	
</script>
    <script type="text/javascript">	
    	$(document).ready(function(){
          var urlParams = new URLSearchParams(window.location.search);
          var status = urlParams.get('status');
          $('#status_check #'+status+" a").click();
        });

        $('#status_check li a').click(function(){
            var status = $(this).data('status');
            $('#status').val(status);
            $('#search_text').val("");
            $('.search_data_ajax').click();
        });
        

        $('.searchdata').keyup(function(e){
            var search = $('#search_text').val();
            var status = $('#status').val();
            $.get( "<?php echo base_url();?>customer/request/get_ajax_all_request_list?title="+search+"&status="+status, function( data ){
                var content_id = $('a[data-status="'+status+'"]').attr('href');
               $( content_id ).html( data );  
            });
           
        } );
        $('.search_data_ajax').click(function(e){
            e.preventDefault();
             $('.searchdata').keyup();
        });

    </script>
  </body>
</html>

<?php 
	 //echo 'Active_Project Array Details<br><pre>'; print_r($active_project); echo '</pre>';
	// echo 'check_approve_project Array Details<br><pre>'; print_r($check_approve_project); echo '</pre>';
	// echo 'disapprove_project Array Details<br><pre>'; print_r($disapprove_project); echo '</pre>';
	// echo 'pending_project Array Details<br><pre>'; print_r($pending_project); echo '</pre>';
	// echo 'draft_project Array Details<br><pre>'; print_r($draft_project); echo '</pre>';
	// echo 'complete_project Array Details<br><pre>'; print_r($complete_project); echo '</pre>';
	// echo 'notifications Array Details<br><pre>'; print_r($notifications); echo '</pre>';

 // echo 'draft_project Array Details<br><pre>'; print_r($draft_project); echo '</pre>';
 // echo 'complete_project Array Details<br><pre>'; print_r($complete_project); echo '</pre>';
?> 

