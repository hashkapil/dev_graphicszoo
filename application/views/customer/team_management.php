<section class="edit-profile-sec" id="edit-profile-sec">
    <div class="container">
        <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') {  ?>
             <div class="alrt alert-danger">
                <a href="javascript:void(0)" class="close" data-dismiss="alrt" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_error'); ?></p>
            </div>
            <?php } if ($this->session->flashdata('message_success') != '') { ?>
            <div class="alrt alert-success">
                <a href="javascript:void(0)" class="close" data-dismiss="alrt" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_success'); ?></p>
            </div>
            <?php } ?>
        </div>

        <div class="tab-pane xyz" id="management">
            <div class="row">
                <div class="col-md-12">
                    <div class="white-boundries" id="sub-userlist">
                        <div class="headerWithBtn">
                            <h2 class="main-info-heading">Team Management</h2>
                            <p class="fill-sub">Add and Update additional users for your account.</p>
                            <div class="addPlusbtn">
                                <?php
 
                                $addedsubuser = $this->load->get_var('total_sub_users');
                                $subscription_plan_user = $this->load->get_var('add_user_limit');
                                if($user_plan_name == ""){ ?>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#SelectPackForUP" class="new-subuser">+ Add Team Member</a>
                                <?php }else{
                                    if ($isshow_subuser['add'] == 1) {
                                        if ($addedsubuser < $isshow_subuser['count'] || $isshow_subuser['count'] == -1) { ?>
                                            <a href="javascript:void(0)" class="new-subuser add_subuser_main " id="add_subuser_mains" data-user_type="manager">+ Add Team Member</a>
                                        <?php } else{ ?>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#ChangePlan" class="new-subuser">+ Add Team Member</a>
                                        <?php } 
                                    } else { ?>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#accessdenieduser" class="new-subuser">+ Add Team Member</a>
                                    <?php }
                                } ?>
                            </div>
                        </div>
                        <?php  
                        if ($user_plan_name == '') { ?>
                            <div class=""> 
                                <div id="no-more-tables" class="manageuserprmsnblk"> You don't have access to the team management section. Please  
                                    <a href="javascript:void(0);" class="stayhere upgrade_color f_upgrade-link"> 
                                        upgrade your plan 
                                    </a>
                                    for team management access.
                                </div>
                            </div>
                        </div>
                    <?php }else{ 
                        if ($isshow_subuser['add'] == 1) {
                            if (($addedsubuser > $isshow_subuser['count'] && $isshow_subuser['count'] != -1) && $user_plan_name != '') { ?>
                                <div class=""> 
                                    <div id="no-more-tables" class="manageuserprmsnblk">
                                        You don't have access to the team management section. Please 
                                        <a href="javascript:void(0);" class="stayhere upgrade_color f_upgrade-link"> 
                                            upgrade your plan 
                                        </a> 
                                        for team management access.
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="account-tab" style="margin-top: 0">
                                <div id="no-more-tables">
                                    <div class="managment-list">   

                                        <?php foreach ($subusersdata as $userkey => $userval) { ?>
                                            <ul id="user_<?php echo $userval['id']; ?>">
                                                <li class="usericon">
                                                    <img src="<?php echo $userval['profile_picture']; ?>" class="img-responsive">
                                                </li>
                                                <li class="name">
                                                    <?php 
                                                    echo isset($userval['first_name']) ? $userval['first_name'] : '';
                                                    echo ' ';
                                                    echo isset($userval['last_name']) ? $userval['last_name'] : ''; ?></li>
                                                    <li class="email"><?php echo isset($userval['email']) ? $userval['email'] : ''; ?></li>
                                                    <li class="lbl">
                                                        <div class="switch-custom-usersetting-check activate-user">
                                                            <input type="checkbox" name="enable_disable_set" data-userid="<?php echo $userval['id']; ?>" data-email="<?php echo $userval['email'];?>" data-name="<?php echo $userval['first_name'];?>" id="enable_disable_set_<?php echo $userval['id']; ?>"
                                                            <?php
                                                            if ($userval['is_active'] == 1) {
                                                                echo 'checked';
                                                            } else {

                                                            }
                                                            ?>
                                                            >
                                                            <label for="enable_disable_set_<?php echo $userval['id']; ?>"></label> 
                                                            <span class="checkstatus_<?php echo $userval['id']; ?>">
                                                                <?php echo ($userval['is_active'] == 1) ? 'Active' : 'Inactive'; ?>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <li class="action">
                                                        <a href="javascript:void(0)" data-id="<?php echo $userval['id']; ?>" class="edit_subuser" data-user_type="manager">
                                                            <i class="icon-gz_edit_icon"></i></a>
                                                            <a href="<?php echo base_url(); ?>customer/MultipleUser/delete_sub_user/setting_view/<?php echo $userval['id']; ?>" class="delete_subuser">
                                                               <i class="icon-gz_delete_icon"></i>
                                                           </a>
                                                       </li>
                                                   </ul>
                                               <?php } ?>


                                           </div>

                                       </div>                              
                                   </div>  
                               </div> 
                               <?php
                               $editvar = '';
                               $edit_id = isset($_GET['editid']) ? $_GET['editid'] : '';
                               if ($edit_id && $edit_id != '') {
                                $edit_sub_user = $sub_user_data[0];
                                $sub_user_permsion = $sub_user_permissions[0];
                            }if ($edit_id == '' && $edit_id == '') {
                                $edit_sub_user = '';
                                $sub_user_permsion = '';
                            }
                            ?>
                            <div id="new-subuser" style="display: none;">

                             <div class="edit-submt">

                                <form action="<?php echo base_url() . 'customer/MultipleUser/add_edit_manager'; ?>" method="post" role="form" class="sub_user_form" id="sub_user_frm1">
                                    <div  class="white-boundries">
                                        <div class="headerWithBtn">
                                           <h2 class="main-info-heading">New User</h2>
                                           <p class="fill-sub">Complete the form below to add new user.</p>
                                           <div class="addPlusbtn">
                                             <a class="backlist backlist-go">
                                              <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">Back
                                          </a>
                                      </div>
                                  </div>
                                  <input type="hidden" name="cust_id" value="" id="cust_id"/>
                                  <input type="hidden" name="user_flag" value="" id="user_flag"/>
                                  <input type="hidden" name="cust_url" value="customer/team-management" id="cust_url"/>

                                  <div class="row">
                                    <div class="col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt">First Name <span>*</span></p>
                                            <input type="text" name="first_name" class="input" id="fname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                            <div class="line-box">
                                              <div class="line"></div>
                                          </div>
                                      </label>
                                  </div>
                                  <div class="col-md-6">
                                   <label class="form-group">
                                    <p class="label-txt">Last Name <span>*</span></p>
                                    <input type="text" name="last_name" class="input" id="lname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                    <div class="line-box">
                                      <div class="line"></div>
                                  </div>
                              </label>
                          </div>
                          <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt">Email Address <span>*</span></p>
                                <input type="email" class="input" name="email" id="user_email" data-rule="email" data-msg="Please enter a valid email" value="" required="">
                                <div class="line-box">
                                  <div class="line"></div>
                              </div>
                          </label>
                      </div>
                      <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">Phone Number <span>*</span></p>
                            <input type="tel" name="phone" class="input" id="phone" value="" required="">
                            <div class="line-box">
                              <div class="line"></div>
                          </div>
                      </label>
                  </div>
                  <div class="col-md-6 password_toggle" style="display:none">
                    <div class="notify-lines">
                        <p>Randomly generated password</p>
                        <label class="form-group switch-custom-usersetting-check">
                            <input type="checkbox" name="genrate_password" id="password_ques" checked/>
                            <label for="password_ques"></label> 
                        </label>
                    </div>
                </div>
                <div class="col-md-6 create_password" style="display:none">
                    <label class="form-group">
                        <p class="label-txt">Password<span>*</span></p>
                        <input type="password" name="password" class="input" id="password" value=""/>
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
            </div> 
        </div>
        <div class="row">
            <?php //if(!empty($brandprofile)){ ?>
                <div class="col-md-12">
                   <div class="white-boundries">
                    <div class="access-brand">
                        <h3>Brands Selection</h3>
                        <div class="notify-lines finish-line">
                            <label class="switch-custom-usersetting-check">
                                <div class="switch-custom-usersetting-check">
                                    <input type="checkbox" name="access_brand_pro" id="switch_access"<?php echo ($sub_user_permsion['add_requests'] == 1) ? 'checked' : ''; ?>/>
                                    <label for="switch_access"></label> 
                                </div>
                            </label>
                            <h3>Access All brands</h3>
                        </div>
                    </div>
                    <div class="" id="brandshowing">
                        <label>Select Brands <span>*</span></label>
                        <select name="brandids[]" id="brandcheck"  class="chosen-select" style='display:none' data-live-search="true" multiple>
                            <?php  ?>
                            <option value="" disabled>Select Brand</option>
                            <?php
                            foreach ($brandprofile as $brand) {
                                ?> 
                                <option value="<?php echo $brand['id'] ?>" <?php echo in_array($brand['id'], $selectedbrandIDs) ? 'selected' : '' ?>><?php echo $brand['brand_name'] ?></option>
                            <?php } ?>
                        </select>                                                            
                    </div>
                </div>
            </div>
        </div>
        <?php// } ?>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div  class="white-boundries">
                <div class="view_only_prmsn">
                    <div class="access-brand">
                        <h3>Permissions</h3>
                        <div class="notify-lines finish-line">
                            <label class="switch-custom-usersetting-check">
                                <input type="checkbox" name="view_only" class="form-control" id="view_only" <?php echo ($sub_user_permsion['view_only'] == 1) ? "checked" : ""; ?>>
                                <label for="view_only"></label> 
                            </label>
                            <h3>View Only</h3>
                        </div>
                    </div>
                </div>
                <div class="row permissions_for_subuser_client">
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="add_requests" class="form-control" id="add_requests" <?php echo ($sub_user_permsion['add_requests'] == 1) ? "checked" : ""; ?>>
                                <label for="add_requests"></label> 
                            </label>
                            <p>Add Request</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="comnt_requests" class="form-control" id="comnt_requests" <?php echo ($sub_user_permsion['comment_on_req'] == 1) ? "checked" : ""; ?>>
                                <label for="comnt_requests"></label> 
                            </label>
                            <p>Comment on Request</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="add_brand_pro" class="form-control" id="add_brand_pro" <?php echo ($sub_user_permsion['add_brand_pro'] == 1) ? "checked" : ""; ?>>
                                <label for="add_brand_pro"></label> 
                            </label>
                            <p>Add/Edit Brand Profile</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="del_requests" class="form-control" id="del_requests" <?php echo ($sub_user_permsion['delete_req'] == 1) ? "checked" : ""; ?>>
                                <label for="del_requests"></label> 
                            </label>
                            <p>Delete Request</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="billing_module" class="form-control" id="billing_module" <?php echo ($sub_user_permsion['billing_module'] == 1) ? "checked" : ""; ?>>
                                <label for="billing_module"></label> 
                            </label>
                            <p>Billing Module</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="manage_priorities" class="form-control" id="manage_priorities" <?php echo ($sub_user_permsion['manage_priorities'] == 1) ? "checked" : ""; ?>>
                                <label for="manage_priorities"></label> 
                            </label>
                            <p>Manage Request Priorities<br/><span style="font-size:12px;display:none;font-style: italic;margin-top: 5px;">*Applicable only if all brand access is enable</span></p>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="app_requests" class="form-control" id="app_requests" <?php echo ($sub_user_permsion['approve/revision_requests'] == 1) ? "checked" : ""; ?>>
                                <label for="app_requests"></label> 
                            </label>
                            <p>Approve/Revision Request</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="downld_requests" class="form-control" id="downld_requests" <?php echo ($sub_user_permsion['download_file'] == 1) ? "checked" : ""; ?>>
                                <label for="downld_requests"></label> 
                            </label>
                            <p>Download File</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="file_management" class="form-control" id="file_management" <?php echo ($sub_user_permsion['file_management'] == 1) ? "checked" : ""; ?>>
                                <label for="file_management"></label> 
                            </label>
                            <p>File Management</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="notify-lines">
                            <label class="switch-custom-usersetting-check uncheckview">
                                <input type="checkbox" name="white_label" class="form-control" id="white_label" <?php echo ($sub_user_permsion['white_label'] == 1) ? "checked" : ""; ?>>
                                <label for="white_label"></label> 
                            </label>
                            <p>White Label</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12 mb-t30">
            <div class="btn-here">
                <input type="submit" name="save_subuser" class="btn-red center-block" value="SAVE">
            </div>
        </div>
    </form>
</div>
</div>
</div>
<?php }
    } else {  if ($subscription_plan_user == $isshow_subuser['count']) { ?>
                                <div class=""> 
                                    <div id="no-more-tables" class="manageuserprmsnblk">
                                        You don't have access to the team management section. Please 
                                        <a href="javascript:void(0);" class="stayhere upgrade_color f_upgrade-link"> 
                                            upgrade your plan 
                                        </a> 
                                        for team management access.
                                    </div>
                                </div>
                            </div>
                        <?php }else{?>
                    <div class=""> 
                        <div id="no-more-tables" class="manageuserprmsnblk">
                            You don't have access to the team management section. Please contact us from more info. 
                        </div>
                    </div>
                    </div>

    <?php } 
    }
    }?>

</div>
</div>
</section>