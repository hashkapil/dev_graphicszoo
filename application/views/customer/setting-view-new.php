<?php
if (isset($data[0]['first_name'])) {
    $first_name = $data[0]['first_name'];
}if (isset($data[0]['last_name'])) {
    $last_name = $data[0]['last_name'];
}
$currentuser = $this->load->get_var('main_user');
$loggedinuser = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';
$CI = & get_instance();
$CI->load->library('myfunctions');
$this->load->view('front_end/variable_css');

// echo "<pre>"; 
// print_r($GettingUsersInfo); 
// die;
?>

<section class="edit-profile-sec" id="edit-profile-sec">
    <div class="container">
        <div class="row mobile_menu_mngmnt">
            <div class="col-md-12"  data-step="7" data-intro="Go to settings to manage users, billing, notifications, password, and your profile." data-position='right' data-scrollTo='tooltip'>
                <div class="account-tab">
                    <div class="tab-toggle togglemenuforres"><p><span></span><span></span><span></span></p></div>
                    <ul class="list-header-blog">
                        <li class="active"><a href="#general" class="stayhere" data-toggle="tab">General</a></li>
                        <li><a href="#personal" class="stayhere" data-toggle="tab">Personal Info</a></li>
                        <li><a href="#notification" class="stayhere" data-toggle="tab">Notification</a></li>
                        <?php if (($canseebilling == 1 || $canseebilling == 2) || ($login_user_data[0]['user_flag'] == 'client' && $login_user_plan[0]["payment_mode"] == 0 && $agency_info[0]['show_billing'] == 1) || ($login_user_data[0]['user_flag'] == 'client' && $login_user_plan[0]["payment_mode"] != 0)) { ?>
                            <li><a href="#billing" class="stayhere" data-toggle="tab">Billing & Subscription</a></li>
                        <?php } ?>
                        <li><a href="#password" class="stayhere" data-toggle="tab">Password</a></li>
                        <?php if ($currentuser == $loggedinuser) { ?>
                            <!--  <li class="usrmangment"><a href="#management" class="stayhere" data-toggle="tab">Team Management</a> </li>  -->

                        <?php } ?>
                        <?php if ($parent_user_id == 0 && $parent_user_plan[0]['is_agency'] == 1) { ?>
                            <li class="for_agency_user stayhere">
                                <a href="<?php echo base_url(); ?>customer/user_setting"><i class="fas fa-cog"></i> White Label Settings</a>
                                <ul class="white_label_nd_email" style="display:none">
                                    <li><a href="#Admin_Email" class="stayhere" data-toggle="tab">Email Templates</a></li>
                                    <li class=""><a href="<?php echo base_url(); ?>customer/user_setting"><span><i class="fas fa-cogs"></i> </span> White label Setting <span class="sr-only">(current)</span></a></li> 

                                </ul>
                            </li>
                        <?php } ?>
                        <li><a href="#designer_team" class="stayhere" data-toggle="tab">Designer Team</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div class="alrt alert-danger">
                    <a href="javascript:void(0)" class="close" data-dismiss="alrt" aria-label="close">×</a>
                    <p class="head-c"><?php echo $this->session->flashdata('message_error'); ?></p>
                </div>
            <?php } ?>

            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div class="alrt alert-success">
                    <a href="javascript:void(0)" class="close" data-dismiss="alrt" aria-label="close">×</a>
                    <p class="head-c"><?php echo $this->session->flashdata('message_success'); ?></p>
                </div>
            <?php } ?>

            <div class="alrt alert-danger" style="display:none">
                <a href="javascript:void(0)" class="close" data-dismiss="alrt" aria-label="close">×</a>
                <p class="head-c"></p>
            </div>
            <div class="alrt alert-success" style="display:none">
                <a href="javascript:void(0)" class="close" data-dismiss="alrt" aria-label="close">×</a>
                <p class="head-c"></p>
            </div>

        </div>

        <div class="tab-content">
            <div class="tab-pane active" id="general">    
                <div class="row">
                    <div class="col-md-12">
                        <form enctype="multipart/form-data" method="post" action="" id="edit_form_data">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="user-area">
                                        <div class="display-info">
                                            <div class="dp">
                                                <img src="<?php echo $profile[0]['profile_picture']; ?>" class="img-responsive telset33">
                                            </div>
                                            <a href="javascript:void(0)">
                                                <span class="setimgcaps">
                                                    <i class="icon-gz_edit_icon" id="edit_button" aria-hidden="true"></i>
                                                </span>
                                            </a>
                                            <div class="setimg-row33 changeprofilepage" style="display: none;">
                                                <div class="imagemain2" style="padding-bottom: 20px; display: none;">
                                                    <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="profile" data-plugin="dropify" id="inFile" style="display: none"/>
                                                </div>
                                                <div class="clearfix profile_image_sec">
                                                    <div class="imagemain">
                                                        <figure class="setimg-box33">
                                                            <img src="<?php echo $profile[0]['profile_picture']; ?>" class="img-responsive telset33">
                                                            <a class="setimg-blog33 link1 font18 bold" href="javascript:void(0)"  onclick="$('.dropify').click();" class="link1 font18 bold" for="profile">
                                                                <span class="setimgblogcaps">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-camera.png" class="img-responsive"><br>
                                                                    <span class="setavatar33">Change <br>Avatar</span>
                                                                </span>
                                                            </a>
                                                        </figure>
                                                        <style>
                                                            .dropify-wrapper{ display:none; height: 200px !important; }
                                                        </style> 
                                                    </div>

                                                    <div class="imagemain3" style="display: none;">
                                                        <figure class="setimg-box33">
                                                            <img id="image3" src="" class="img-responsive telset33">
                                                            <a class="setimg-blog33 link1 font18 bold" href="javascript:void(0)"  onclick="$('.dropify').click();" class="link1 font18 bold" for="profile">
                                                                <span class="setimgblogcaps">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-camera.png" class="img-responsive"><br>
                                                                    <span class="setavatar33">Change <br>Avatar</span>
                                                                </span>
                                                            </a>
                                                        </figure>
                                                    </div>
                                                </div>
                                            </div> 
                                            <?php
                                            $last_name = "";
                                            if (isset($profile[0]['last_name'])) {
                                                $last_name = substr($profile[0]['last_name'], 0, 1);
                                            }
                                            ?> 
                                            <h3><?php echo $profile[0]['first_name'] . ' ' . $last_name; ?></h3>
                                            <p><?php echo $profile[0]['email']; ?></p>
                                            <p><?php echo $profile[0]['phone']; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="content-edition">
                                        <h2 class="main-info-heading">General Details</h2>
                                        <p class="fill-sub">Fill out the information below to tell us more about yourself.</p>
                                        <div class="edit-submt">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['first_name']) && $profile[0]['first_name'] != "") ? "label-active" : ""; ?>"> Full Name <span>*</span> </p>
                                                <input type="text" name="first_name" class="input" value="<?php
                                            if (isset($profile[0]['first_name'])) {
                                                echo $profile[0]['first_name'];
                                            }
                                            ?> <?php
                                                if (isset($profile[0]['last_name'])) {
                                                    echo $profile[0]['last_name'];
                                                }
                                                ?>" required>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['company_name']) && $profile[0]['company_name'] != "") ? "label-active" : ""; ?>"> Company Name </p>
                                                <input type="text" name="company" class="input"  value="<?php echo isset($profile[0]['company_name']) ? $profile[0]['company_name'] : ''; ?>">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <label class="form-group">
                                                <p class="label-txt <?php echo ($profile[0]['title'] && $profile[0]['title'] != "") ? "label-active" : ""; ?>">Title </p>
                                                <input type="text" name="title" class="input" value="<?php echo isset($profile[0]['title']) ? $profile[0]['title'] : ''; ?>">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['email']) && $profile[0]['email'] != "") ? "label-active" : ""; ?>">Email Address <span>*</span> </p>
                                                <input type="email" class="input" name="notification_email"  value="<?php echo isset($profile[0]['email']) ? $profile[0]['email'] : ''; ?>" required>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['phone']) && $profile[0]['phone'] != "") ? "label-active" : ""; ?>">Phone Number</p>
                                                <input type="tel" name="phone" class="input" value="<?php echo isset($profile[0]['phone']) ? $profile[0]['phone'] : ''; ?>">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['url']) && $profile[0]['url'] != "") ? "label-active" : ""; ?>">Website URL </p>
                                                <input type="text" name="url" class="input" value="<?php echo isset($profile[0]['url']) ? $profile[0]['url'] : ''; ?>" >
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <div class="notify-lines">
                                                <div class="switch-custom-usersetting-check">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" name="request_layout" id="switch_1" <?php echo (isset($profile[0]['request_wizard']) && $profile[0]['request_wizard'] == 1) ? "checked" : ""; ?>>
                                                    <label for="switch_1"></label> 
                                                </div>
                                                <p>Enable one-page add request form</p>
                                            </div>
                                            <div class="form-group">
                                                <div class="loader"></div>
                                                <div class="btn-here">
                                                    <input type="submit" id="savebtn1" name="savebtn" class="btn-e save_info btn-red" value="Save">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                        </form>
                    </div> 
                </div>
            </div> 

            <div class="tab-pane" id="personal"> 
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="white-boundries">
                            <h2 class="main-info-heading"> Personal Information</h2>
                            <p class="fill-sub">Enter your personal details below.</p>
                            <div class="edit-submt">
                                <form action="javascript:void(0)" method="post" accept-charset="utf-8" id="about_info_form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['address_line_1']) && $profile[0]['address_line_1'] != "") ? "label-active" : ""; ?>">Address</p>
                                                <input type="text" name="adress" class="input" value="<?php echo isset($profile[0]['address_line_1']) ? $profile[0]['address_line_1'] : '' ?>" >
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['city']) && $profile[0]['city'] != "") ? "label-active" : ""; ?>">City</p>
                                                <input type="text" name="city" class="input" value="<?php echo isset($profile[0]['city']) ? $profile[0]['city'] : '' ?>" >
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['state']) && $profile[0]['state'] != "") ? "label-active" : ""; ?>">State</p>
                                                <input type="text" name="state" class="input" value="<?php echo isset($profile[0]['state']) ? $profile[0]['state'] : '' ?>"  readonly="">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($profile[0]['zip']) && $profile[0]['zip'] != "") ? "label-active" : ""; ?>">Zip Code</p>
                                                <input type="text" name="zip" class="input" value="<?php echo isset($profile[0]['zip']) ? $profile[0]['zip'] : '' ?>" >
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-group">
                                                <p class="label-txt label-active">Select Time Zone </p>
                                                <select name="timezone" class="form-control select-a input">
                                                    <option value="">Select timezone</option>
                                                    <?php foreach ($getalltimezone as $kk => $vv) { ?>
                                                        <option value="<?php echo $vv['zone_name']; ?>" <?php echo ($vv['zone_name'] == $profile[0]['timezone']) ? 'selected' : ''; ?>><?php echo $vv['zone_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>                                   
                                    <div class="form-group">
                                        <div class="loader"></div>
                                        <div class="btn-here">
                                            <input type="submit" id="about_info_btn" name="savebtn" class="btn-e save_info btn-red" value="SAVE"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!---------------------Start notifications------------------------------------>
            <div class="tab-pane" id="notification"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-boundries">
                            <h2 class="main-info-heading"> Notification</h2>
                            <p class="fill-sub">Choose your notification preferences below.</p>
                            <div class="form-group">
                                <div class="notify-lines">
                                    <div class="switch-custom-usersetting-check remind_mail">
                                        <span class="checkstatus"></span>
                                        <input type="checkbox" data-cid="<?php echo $profile[0]['id']; ?>" data-key="DRAFT_APPROVED_EMAIL_TO_USER" id="switch_1"
                                        <?php
                                        if ($profile[0]['DRAFT_APPROVED_EMAIL_TO_USER'] == 1) {
                                            echo 'checked';
                                        } else {
                                            
                                        }
                                        ?>/>
                                        <label for="switch_1"></label> 
                                    </div>
                                    <p>Send email to review new design draft</p>
                                </div>
                                <div class="notify-lines">
                                    <div class="switch-custom-usersetting-check remind_mail">
                                        <span class="checkstatus"></span>
                                        <input type="checkbox" data-cid="<?php echo $profile[0]['id']; ?>" data-key="SEND_CRON_EMAIL_ON_MESSAGE" id="switch_2"
                                        <?php
                                        if ($profile[0]['SEND_CRON_EMAIL_ON_MESSAGE'] == 1) {
                                            echo 'checked';
                                        } else {
                                            
                                        }
                                        ?>/>
                                        <label for="switch_2"></label> 
                                    </div>
                                    <p>Remind me if I have not seen messages within 30 minutes</p>
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---------------------End notifications------------------------------------>

            <div class="tab-pane" id="billing"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-boundries">
                            <?php if ($parent_user_data[0]['is_cancel_subscription'] == 1 && $parent_user_data[0]['parent_id'] == 0) { ?>
                                <p class="next_plan_active cancel_sub_msg">Payment is past due. Subscription has been cancelled. Please reactivate your subscription for more designs.
                                </p>
                            <?php } else if ($data[0]['invoice'] == '0' && $data[0]['next_plan_name'] != "") {
                                ?>
                                <p class="next_plan_active">Your plan "<?php echo $nextplan_details[0]['plan_name']; ?>" will be active from <?php echo $data[0]['billing_end_date']; ?> 
                                </p>
                            <?php } ?> 
                            <h2 class="main-info-heading">Billing and Subscription</h2>
                            <p class="fill-sub">Update your Billing or Change your Subscription Plan below.</p>

                            <?php if ($type_ofuser == 'fortynine_plans') { ?>
                                <div class="purchase_req" style="display:none">
                                    <a href="javascript:void(0)" data-value="<?php echo FORTYNINE_REQUEST_PLAN; ?>" data-price="<?php echo FORTYNINE_REQUEST_PRICE; ?>" data-inprogress="<?php echo FORTYNINE_REQUEST; ?>" data-display_name="<?php echo FORTYNINE_REQUEST_PLAN_NAME; ?>" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p red-theme-btn">
                                        Purchase 1 Request
                                    </a>
                                </div>
                            <?php } ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="project-row-qq1 settingedit-box">
                                        <div class="settrow">
                                            <div class="settcol left">
                                                <h3 class="sub-head-b">Card Details</h3>
                                            </div>
                                            <div class="settcol right">
                                                <a id="about_infoform" class="review-circleww" href="javascript:void(0)">
                                                    <?php if ($card) { ?><i class="icon-gz_edit_icon"></i><?php } else { ?>
                                                        <i class="icon-gz_plus_icon"></i>
                                                    <?php } ?>

                                                </a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <?php if ($card) { //echo "<pre>";print_r($card);  ?>

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="label-x2">Card Number</label>
                                                        <p class="space-a"></p>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="text-f">************<?php echo $card['0']->last4; ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="label-x2">Expiry Month/Year</label>
                                                        <p class="space-a"></p>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="text-f"><?php echo $card['0']->exp_month . '/' . $card['0']->exp_year; ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="label-x2">CVV</label>
                                                        <p class="space-a"></p>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="text-f">***</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <h4 style="text-align:center">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/card-bg.svg" class="img-responsive">
                                                    No Card Detail Found!</h4>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="carddetailsedit edit-submt" style="display:none">
                                        <div class="settcol left">
                                            <h3 class="sub-head-b">Card Details</h3>
                                        </div>
                                        <form class="card-verifection" action="<?php echo base_url(); ?>customer/ChangeProfile/change_plan" method="post" accept-charset="utf-8">
                                            <div class="card-js">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label class="form-group">
                                                            <p class="label-txt <?php echo isset($editvar['title']) ? "label-active" : ""; ?>">Card Number <span>*</span></p>
                                                            <input type="text" name="card-number" class="card-number input" required="" maxlength="16">
                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="form-group">
                                                            <p class="label-txt label-active fixedlabelactive">Expiry Datesdsds <span>*</span></p>
                                                            <input class="input expiry-month expir_date" id="card_expir_date" name="expiry-month-date" required="" placeholder="MM  /  YY" onkeyup="dateFormat(this, this.value);">

                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="form-group">
                                                            <p class="label-txt <?php echo isset($editvar['title']) ? "label-active" : ""; ?>">CVV <span>*</span></p>
                                                            <input type="text" name="cvc" class="cvc input"  maxlength="3" required="">
                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <div class="btn-here">
                                                    <input type="hidden" name="current_plan" value="<?php echo $current_plan; ?>" />
                                                    <input type="submit" value="Save" class="btn btn-e site-btn btn-red" name="change_my_card_deatils"/>
                                                    <input type="submit" id="cancelcard" name="cancel" class="theme-cancel-btn" value="Cancel"></div>
                                            </div>
                                        </form>
                                    </div>          
                                </div>
                                <div class="col-md-12">
                                    <div class="row billing_plan_view">
                                        <p class="btn-x">
                                            <a href="javascript:void(0)" class="btn-f Change_Plan chossbpanss"><?php echo ($data[0]['plan_name'] != '') ? "Change your plan" : "Select your plan"; ?></a>
                                        </p>

                                        <div class="col-md-6 col-sm-6 current_actv_pln billing_plan">
                                            <div class="flex-grid">
                                                <div class="price-card best-offer">
                                                    <div class ="chooseplan active f_main_clnt">
                                                        <?php
                                                        if ($data[0]['plan_name'] != '') {
                                                            $pln_feature = explode("_", $currentplan_details[0]['features']);
                                                            $plnfeature = "<ul>";
                                                            if (!empty($pln_feature)) {
                                                                foreach ($pln_feature as $text) {
                                                                    $plnfeature .= "<li>" . $text . "</li>";
                                                                }
                                                            }
                                                            $plnfeature .= "<ul>";
                                                            $agency_priceclass = "<span class='" . $currentplan_details[0]['plan_type'] . " agency_price'> " . $currentplan_details[0]['plan_price'] . "</span>";
                                                            ?>
                                                            <h2><?php echo $currentplan_details[0]['plan_name']; ?></h2>
                                                            <h3><font>$</font><?php echo $agency_priceclass; ?><span>/<?php echo $currentplan_details[0]['plan_type']; ?></span></h3>
                                                            <div class="p-benifite">
                                                                <?php if ($type_ofuser == 'fortynine_plans' || $type_ofuser == 'one_time') { ?>
                                                                    <ul>
                                                                        <li><p>Total Requests: <?php echo $data[0]['total_requests']; ?></p></li>
                                                                        <li><p>Added Requests: <?php echo $request['added_request']; ?></p></li>
                                                                        <li><p>Pending Requests: <?php echo $request['pending_req']; ?></p></li>
                                                                    </ul>
                                                                <?php
                                                                } else {
                                                                    if (!empty($currentplan_details[0]['tier_prices'])) {
                                                                        if ($currentplan_details[0]['plan_type'] == 'yearly' || $currentplan_details[0]['plan_type'] == 'quarterly') {
                                                                            $amount = 'annual_price';
                                                                        } else {
                                                                            $amount = 'amount';
                                                                        }
                                                                        $options = "";
                                                                        foreach ($currentplan_details[0]['tier_prices'] as $tier_prices) {
                                                                            $options .= '<option value ="' . $tier_prices['quantity'] . '" data-amount="' . $tier_prices[$amount] . '" data-annual_price="' . $tier_prices["amount"] . '">' . $tier_prices['quantity'] . ' Dedicated Designer</option>';
                                                                        }
                                                                        $features = str_replace('{{QUANTTY}}', $options, $plnfeature);
                                                                        echo $features;
                                                                    } else {
                                                                        echo $plnfeature;
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="price-sign-up">
                                                                <a type="button" class="ud-dat-p currentPlan" disabled>
                                                                    Current Plan
                                                                </a>
                                                            </div>
                                                        <?php
                                                        } else {
                                                            echo "no selected Plan";
                                                        }
                                                        ?>
                                                        <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="btn-x bckbtntoback" style="display:none">
                                        <a href="javascript:void(0)" class="btn-g Change_Plan backfromnewplan">Back</a>
                                    </p>
                                    <div class="f_chng_ugrd_blpln" style="display:none">
                                    <?php $this->load->view('customer/plan_listing', array("data" => $data)); ?>
                                    </div>
<!--                                    <div class="billing_plan new_plan_billingfr_chng two-can <?php //echo $type_ofuser; ?> owl-carousel" style="display:none">
<?php
//                                        foreach ($userplans as $userplan) {
//                                        $agencyclass = $userplan['plan_type']." upgrd_agency_pln" ;
//                                        $agency_priceclass = "<span class='".$userplan['plan_type']." agency_price'> ".$userplan['plan_price']."</span>" ; 
?>
                                        <div class="flex-grid">
                                            <div class="price-card">
                                                <div class="chooseplan">
                                                    <h2><?php //echo $userplan['plan_name']; ?></h2>
                                                    <h3><font>$</font><?php //echo $agency_priceclass; ?><span>/<?php //echo $userplan['plan_type']; ?></span></h3>
                                                    <div class="p-benifite">
                                                        <div class="price-details">
                                    <?php
//                                                            if($userplan['plan_type'] == 'yearly'){
//                                                                $amount = 'annual_price';
//                                                            }else{
//                                                                $amount = 'amount';
//                                                            }
//                                                            $options = "";
//                                                             foreach ($userplan['tier_prices'] as $tier_prices){
//                                                                 $options .= '<option value ="'.$tier_prices['quantity'].'" data-amount="'.$tier_prices[$amount].'">'.$tier_prices['quantity'].' Dedicated Designer</option>';
//                                                             }
//                                                            $features = str_replace('{{QUANTTY}}', $options, $userplan['features']);
//                                                            echo $features; 
                                    ?>
                                                        </div>
                                                    </div>
                                                    <div class="price-sign-up">
                                    <?php // if($userplan['info']['target'] != '' && $userplan['info']['target'] == 'cantaddsubs'){  ?>
                                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#<?php echo $userplan['info']['target']; ?>" data-value="<?php echo $userplan['plan_id']; ?>" data-assignreq="<?php echo $userplan['requests']['main_inprogress_req']; ?>" data-alrdyassignreq="<?php echo $userplan['requests']['addedsbrqcount']; ?>" data-pndngrq="<?php echo $userplan['requests']['pending_req']; ?>" type="button" class="<?php echo $userplan['info']['class']; ?> ud-dat-p <?php echo $agencyclass; ?>">
                                    <?php //echo $userplan['info']['text'];  ?>
                                                                </a>
                                    <?php //}else if($userplan['info']['target'] != ''){  ?>
                                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#<?php echo $userplan['info']['target']; ?>" data-value="<?php echo $userplan['plan_id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-inprogress="<?php echo $userplan['global_inprogress_request']; ?>" data-display_name="<?php echo $userplan['plan_name']; ?>" data-clientid="<?php echo $data[0]['id']; ?>" data-applycoupn="<?php echo $userplan['apply_coupon']; ?>"  type="button" class="<?php echo $userplan['info']['class']; ?> ud-dat-p <?php echo $agencyclass; ?>">
                                    <?php // echo $userplan['info']['text'];  ?>
                                                                </a>
<?php //}else{  ?>
                                                               <a href="javascript:void(0)" data-value="<?php echo $userplan['plan_id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-inprogress="<?php echo $userplan['global_inprogress_request']; ?>" data-display_name="<?php echo $userplan['plan_name']; ?>" data-clientid="<?php echo $data[0]['id']; ?>" data-applycoupn="<?php echo $userplan['apply_coupon']; ?>"  type="button" class="<?php echo $userplan['info']['class']; ?> ud-dat-p <?php echo $agencyclass; ?>">
<?php //echo $userplan['info']['text'];  ?>
                                                                </a> 
                                    <?php //}  ?>
                                                    </div>
                                                    <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
                                                </div>
                                            </div>
                                        </div>
<?php // } ?>
                                    </div>-->
<?php if ($data[0]['is_cancel_subscription'] != 1 && $parent_user_data[0]['parent_id'] == 0 && !in_array($data[0]['plan_name'], NEW_PLANS)) { ?>
                                        <a href="javascript:void(0)" class="btn-f cancel_Plan" data-toggle="modal" data-target="#CancelPopup" style="color: var(--theme-primary-color);text-decoration: underline;">Cancel Plan</a>
<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!---------------------Start Change Password------------------------------------>
            <div class="tab-pane" id="password"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-boundries">
                            <h2 class="main-info-heading">Change Password</h2>
                            <p class="fill-sub">Use the fields below to update your account password</p>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="edit-submt">   
                                        <form action="javascript:void(0)" method="post" accept-charset="utf-8" id="change_psswrd">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo isset($editvar['title']) ? "label-active" : ""; ?>">Current Password *</p>
                                                <input type="password" class="input" name="old_password" required>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <label class="form-group">
                                                <p class="label-txt <?php echo isset($editvar['title']) ? "label-active" : ""; ?>">New Password  *</p>
                                                <input type="password" class="input" name="new_password" required>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <label class="form-group">
                                                <p class="label-txt <?php echo isset($editvar['title']) ? "label-active" : ""; ?>">Confirm Password *</p>
                                                <input type="password" class="input" name="confirm_password" required>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <div class="form-group">
                                                <div class="loader"></div>
                                                <div class="btn-here">
                                                    <input type="submit" name="save" class="btn-red" value="SAVE">
                                                </div>
                                            </div>
                                        </form>
                                    </div>               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="designer_team">
<?php echo $this->load->view('customer/designer_team', array("design_team" => $design_teams, "change_designer_request" => $change_designer_request)); ?>
            </div>
            <!---------------------END Change Password------------------------------------>
            <div class="tab-pane xyz" id="management">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-boundries" id="sub-userlist">
                            <div class="headerWithBtn">
                                <h2 class="main-info-heading">Team Management</h2>
                                <p class="fill-sub">Add and Update additional users for your account.</p>
                                <div class="addPlusbtn">
                                    <?php
                                    $addedsubuser = $this->load->get_var('total_sub_users');
                                    $subscription_plan_user = $this->load->get_var('add_user_limit');
                                    if ($user_plan_name == "") {
                                        ?>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#SelectPackForUP" class="new-subuser">+ Add Team Member</a>
                                    <?php
                                    } else {
                                        if ($isshow_subuser['add'] == 1) {
                                            if ($addedsubuser < $isshow_subuser['count'] || $isshow_subuser['count'] == -1) {
                                                ?>
                                                <a href="javascript:void(0)" class="new-subuser add_subuser_main " id="add_subuser_mains" data-user_type="manager">+ Add Team Member</a>
        <?php } else { ?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#ChangePlan" class="new-subuser">+ Add Team Member</a>
                                    <?php }
                                } else {
                                    ?>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#accessdenieduser" class="new-subuser">+ Add Team Member</a>
    <?php }
}
?>
                                </div>
                            </div>
<?php if ($user_plan_name == '') { ?>
                                <div class=""> 
                                    <div id="no-more-tables" class="manageuserprmsnblk"> You don't have access to the team management section. Please  
                                        <a href="javascript:void(0);" class="stayhere upgrade_color f_upgrade-link"> 
                                            upgrade your plan 
                                        </a>
                                        for team management access.
                                    </div>
                                </div>
                            </div>
<?php
} else {
    if ($isshow_subuser['add'] == 1) {
        if (($addedsubuser > $isshow_subuser['count'] && $isshow_subuser['count'] != -1) && $user_plan_name != '') {
            ?>
                                    <div class=""> 
                                        <div id="no-more-tables" class="manageuserprmsnblk">
                                            You don't have access to the team management section. Please 
                                            <a href="javascript:void(0);" class="stayhere upgrade_color f_upgrade-link"> 
                                                upgrade your plan 
                                            </a> 
                                            for team management access.
                                        </div>
                                    </div>
                                </div>
                                                <?php } else { ?>
                                <div class="account-tab" style="margin-top: 0">
                                    <div id="no-more-tables">
                                        <div class="managment-list">   

            <?php foreach ($subusersdata as $userkey => $userval) { ?>
                                                <ul id="user_<?php echo $userval['id']; ?>">
                                                    <li class="usericon">
                                                        <img src="<?php echo $userval['profile_picture']; ?>" class="img-responsive">
                                                    </li>
                                                    <li class="name">
                                                            <?php
                                                            echo isset($userval['first_name']) ? $userval['first_name'] : '';
                                                            echo ' ';
                                                            echo isset($userval['last_name']) ? $userval['last_name'] : '';
                                                            ?></li>
                                                    <li class="email"><?php echo isset($userval['email']) ? $userval['email'] : ''; ?></li>
                                                    <li class="lbl">
                                                        <div class="switch-custom-usersetting-check activate-user">
                                                            <input type="checkbox" name="enable_disable_set" data-userid="<?php echo $userval['id']; ?>" data-email="<?php echo $userval['email']; ?>" data-name="<?php echo $userval['first_name']; ?>" id="enable_disable_set_<?php echo $userval['id']; ?>"
                <?php
                if ($userval['is_active'] == 1) {
                    echo 'checked';
                } else {
                    
                }
                ?>
                                                                   >
                                                            <label for="enable_disable_set_<?php echo $userval['id']; ?>"></label> 
                                                            <span class="checkstatus_<?php echo $userval['id']; ?>">
                                                <?php echo ($userval['is_active'] == 1) ? 'Active' : 'Inactive'; ?>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <li class="action">
                                                        <a href="javascript:void(0)" data-id="<?php echo $userval['id']; ?>" class="edit_subuser" data-user_type="manager">
                                                            <i class="icon-gz_edit_icon"></i></a>
                                                        <a href="<?php echo base_url(); ?>customer/MultipleUser/delete_sub_user/setting_view/<?php echo $userval['id']; ?>" class="delete_subuser">
                                                            <i class="icon-gz_delete_icon"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                            <?php } ?>


                                        </div>

                                    </div>                              
                                </div>  
                            </div> 
            <?php
            $editvar = '';
            $edit_id = isset($_GET['editid']) ? $_GET['editid'] : '';
            if ($edit_id && $edit_id != '') {
                $edit_sub_user = $sub_user_data[0];
                $sub_user_permsion = $sub_user_permissions[0];
            }if ($edit_id == '' && $edit_id == '') {
                $edit_sub_user = '';
                $sub_user_permsion = '';
            }
            ?>
                            <div id="new-subuser" style="display: none;">

                                <div class="edit-submt">

                                    <form action="<?php echo base_url() . 'customer/sub_user?editid=' . $edit_id; ?>" method="post"  role="form" class="sub_user_form" id="sub_user_frm_12">
                                        <div  class="white-boundries">
                                            <div class="headerWithBtn">
                                                <h2 class="main-info-heading">New User</h2>
                                                <p class="fill-sub">Complete the form below to add new user.</p>
                                                <div class="addPlusbtn">
                                                    <a class="backlist backlist-go">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">Back
                                                    </a>
                                                </div>
                                            </div>
                                            <input type="hidden" name="cust_id" value="" id="cust_id"/>
                                            <input type="hidden" name="user_flag" value="" id="user_flag"/>
                                            <input type="hidden" name="cust_url" value="customer/setting-view#management" id="cust_url"/>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="form-group">
                                                        <p class="label-txt">First Name <span>*</span></p>
                                                        <input type="text" name="first_name" class="input" id="fname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-group">
                                                        <p class="label-txt">Last Name <span>*</span></p>
                                                        <input type="text" name="last_name" class="input" id="lname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-group">
                                                        <p class="label-txt">Email Address <span>*</span></p>
                                                        <input type="email" class="input" name="email" id="user_email" data-rule="email" data-msg="Please enter a valid email" value="" required="">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-group">
                                                        <p class="label-txt">Phone Number <span>*</span></p>
                                                        <input type="tel" name="phone" class="input" id="phone" value="" required="">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-md-6 password_toggle" style="display:none">
                                                    <div class="notify-lines">
                                                        <p>Randomly generated password</p>
                                                        <label class="form-group switch-custom-usersetting-check">
                                                            <input type="checkbox" name="genrate_password" id="password_ques" checked/>
                                                            <label for="password_ques"></label> 
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 create_password" style="display:none">
                                                    <label class="form-group">
                                                        <p class="label-txt">Password<span>*</span></p>
                                                        <input type="password" name="password" class="input" id="password" value=""/>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="row">
            <?php //if(!empty($brandprofile)){  ?>
                                            <div class="col-md-12">
                                                <div class="white-boundries">
                                                    <div class="access-brand">
                                                        <h3>Brands Selection</h3>
                                                        <div class="notify-lines finish-line">
                                                            <label class="switch-custom-usersetting-check">
                                                                <div class="switch-custom-usersetting-check">
                                                                    <input type="checkbox" name="access_brand_pro" id="switch_access"<?php echo ($sub_user_permsion['add_requests'] == 1) ? 'checked' : ''; ?>/>
                                                                    <label for="switch_access"></label> 
                                                                </div>
                                                            </label>
                                                            <h3>Access All brands</h3>
                                                        </div>
                                                    </div>
                                                    <div class="" id="brandshowing">
                                                        <label>Select Brands <span>*</span></label>
                                                        <select name="brandids[]" id="brandcheck"  class="chosen-select" style='display:none' data-live-search="true" multiple>
                                        <?php ?>
                                                            <option value="" disabled>Select Brand</option>
            <?php
            foreach ($brandprofile as $brand) {
                ?> 
                                                                <option value="<?php echo $brand['id'] ?>" <?php echo in_array($brand['id'], $selectedbrandIDs) ? 'selected' : '' ?>><?php echo $brand['brand_name'] ?></option>
            <?php } ?>
                                                        </select>                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            <?php // }  ?>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div  class="white-boundries">
                                            <div class="view_only_prmsn">
                                                <div class="access-brand">
                                                    <h3>Permissions</h3>
                                                    <div class="notify-lines finish-line">
                                                        <label class="switch-custom-usersetting-check">
                                                            <input type="checkbox" name="view_only" class="form-control" id="view_only" <?php echo ($sub_user_permsion['view_only'] == 1) ? "checked" : ""; ?>>
                                                            <label for="view_only"></label> 
                                                        </label>
                                                        <h3>View Only</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row permissions_for_subuser_client">
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="add_requests" class="form-control" id="add_requests" <?php echo ($sub_user_permsion['add_requests'] == 1) ? "checked" : ""; ?>>
                                                            <label for="add_requests"></label> 
                                                        </label>
                                                        <p>Add Request</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="comnt_requests" class="form-control" id="comnt_requests" <?php echo ($sub_user_permsion['comment_on_req'] == 1) ? "checked" : ""; ?>>
                                                            <label for="comnt_requests"></label> 
                                                        </label>
                                                        <p>Comment on Request</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="add_brand_pro" class="form-control" id="add_brand_pro" <?php echo ($sub_user_permsion['add_brand_pro'] == 1) ? "checked" : ""; ?>>
                                                            <label for="add_brand_pro"></label> 
                                                        </label>
                                                        <p>Add/Edit Brand Profile</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="del_requests" class="form-control" id="del_requests" <?php echo ($sub_user_permsion['delete_req'] == 1) ? "checked" : ""; ?>>
                                                            <label for="del_requests"></label> 
                                                        </label>
                                                        <p>Delete Request</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="billing_module" class="form-control" id="billing_module" <?php echo ($sub_user_permsion['billing_module'] == 1) ? "checked" : ""; ?>>
                                                            <label for="billing_module"></label> 
                                                        </label>
                                                        <p>Billing Module</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="manage_priorities" class="form-control" id="manage_priorities" <?php echo ($sub_user_permsion['manage_priorities'] == 1) ? "checked" : ""; ?>>
                                                            <label for="manage_priorities"></label> 
                                                        </label>
                                                        <p>Manage Request Priorities<br/><span style="font-size:12px;display:none;font-style: italic;margin-top: 5px;">*Applicable only if all brand access is enable</span></p>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="app_requests" class="form-control" id="app_requests" <?php echo ($sub_user_permsion['approve/revision_requests'] == 1) ? "checked" : ""; ?>>
                                                            <label for="app_requests"></label> 
                                                        </label>
                                                        <p>Approve/Revision Request</p>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="downld_requests" class="form-control" id="downld_requests" <?php echo ($sub_user_permsion['download_file'] == 1) ? "checked" : ""; ?>>
                                                            <label for="downld_requests"></label> 
                                                        </label>
                                                        <p>Download File</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="file_management" class="form-control" id="file_management" <?php echo ($sub_user_permsion['file_management'] == 1) ? "checked" : ""; ?>>
                                                            <label for="file_management"></label> 
                                                        </label>
                                                        <p>File Management</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="notify-lines">
                                                        <label class="switch-custom-usersetting-check uncheckview">
                                                            <input type="checkbox" name="white_label" class="form-control" id="white_label" <?php echo ($sub_user_permsion['white_label'] == 1) ? "checked" : ""; ?>>
                                                            <label for="white_label"></label> 
                                                        </label>
                                                        <p>White Label</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-t30">
                                        <div class="btn-here">
                                            <input type="submit" name="save_subuser" class="btn-red center-block" value="SAVE">
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>                        
                <?php }
            } else {
                if ($subscription_plan_user == $isshow_subuser['count']) {
                    ?>
                        <div class=""> 
                            <div id="no-more-tables" class="manageuserprmsnblk">
                                You don't have access to the team management section. Please 
                                <a href="javascript:void(0);" class="stayhere upgrade_color f_upgrade-link"> 
                                    upgrade your plan 
                                </a> 
                                for team management access.
                            </div>
                        </div>
                    </div>
        <?php } else { ?>
                    <div class=""> 
                        <div id="no-more-tables" class="manageuserprmsnblk">
                            You don't have access to the team management section. Please contact us from more info. 
                        </div>
                    </div>
                </div>

        <?php
        }
    }
}
?>

</div>
</div>
</section>
<div class="modal fade similar-prop" id="accessdenieduser" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Access Denied</h2>
                <div class="cross_popup" data-dismiss="modal"> x </div>
            </header>
            <div class="cli-ent-model">
                <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
                <h3 class="head-c text-center">You don't have access to add user. Please contact us for more info. </h3>
            </div>
        </div>
    </div>
</div>


<!-- Confirm popup for change plan -->
<div class="modal fade similar-prop" id="CancelPopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Cancel Subscription?</h2>
                    <div class="cross_popup" data-dismiss="modal">x </div>
                </header>

                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
                    <h3 class="head-c text-center">Why you want to cancel your subscription?</h3>
                    <form action="<?php echo base_url(); ?>customer/ChangeProfile/cancel_current_plan" method="post" role="form" id="form_cancel_pop">
                        <select name="reason" class="form-control select-a" id="change_reason" required>
                            <option value="">Please choose one</option>
                            <option value="Designs are taking too long to complete">Designs are taking too long to complete</option>
                            <option value="Designers are not understanding my request">Designers are not understanding my request</option>
                            <option value="The design quality is not good">The design quality is not good</option>
                            <option value="The cost is too expensive for me">The cost is too expensive for me</option>
                            <option value="I don't need the service anymore">I don't need the service anymore</option>
                            <option value="My business has closed and I no longer need the service">My business has closed and I no longer need the service</option>
                            <option value="The platform is too difficult to use">The platform is too difficult to use</option>
                            <option value="Customer service is unsatisfactory">Customer service is unsatisfactory</option>
                            <option value="The system lacks the integrations we need">The system lacks the integrations we need</option>
                            <option value="This was a test account">This was a test account</option>
                            <option value="We are going with another service provider or in-house designer">We are going with another service provider or in-house designer</option>
                        </select>
                        <input type="hidden" name="is_copon_applied" value="<?php echo $checkcouponapplied; ?>"/>
                        <div class="error_sel"></div>
                        <div class="switch_designer_box" style="display:none">
                            <label>Do you want to switch your designer ?</label>
                            <div class="yes-radio">
                                <input id="d_yes" type="radio" name="radio_des" class="yes_rad" value="yes">
                                <label for="d_yes">Yes</label>
                            </div>
                            <div class="yes-radio">
                                <input id="d_no" type="radio" name="radio_des" class="no_rad" value="no">
                                <label for="d_no">No</label>
                            </div>
                       <!--<div class="chage-offr-desinr"><p>We will give <span> 20% off </span> the next month for this inconvenience.</p></div>-->
                        </div>
                        <div class='give_reason ur-exp' style='display:none'>
                            <label>Which one and why ? 
                                <textarea name='reason_desc' id="reason_desc"></textarea>
                            </label>
                        </div>
                        <div class="form-radion ur-exp">
                            <label class="form-group">
                                <p class=""> Please tell us how could have made this service better and if there is anything else you would like to share with us. </p>
                                <textarea name='cancel_feebback' id="cancel_feebback"></textarea></label>
                        </div>
                        <a class="btn btn-red close-cancel" href='javascript:void(0)' data-dismiss="modal">Close</a>
                            <?php if ($login_user_data[0]['user_flag'] != 'client') { ?>
                            <button class="btn btn-red reason_submit reason_submit_disable" data-is_already_cancelled="<?php echo $isalreadyrequested; ?>" data-is_coupn_applied="<?php echo $checkcouponapplied; ?>" name="reason_submit" type="submit">Proceed</button>
                            <?php } else { ?>
                            <button class="btn btn-red" name="reason_submit_client" type="submit">Submit</button>
                            <?php } ?>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal popup -->

<!--------start 20% off popup *****-->
<div class="modal fade slide-3 similar-prop model-close-button in" id="prompt_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Stay with us</h2>
                <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
            </header>
            <div class="modal-body">
                <div class="stay-offer"><p>Stay with us and we will give you <span> 20% <small>off </small></span> for the next month.</p></div>
                <div class="congrts">
                    <form action="<?php echo base_url(); ?>customer/ChangeProfile/cancel_current_plan" method="post">
                        <input type="hidden" name="customer_id" class="customer_id" value=""/>
                        <input type="hidden" name="reason_val" class="reason_val" value=""/>
                        <input type="hidden" name="is_switch_designer" class="is_switch_designer" value=""/>
                        <input type="hidden" name="feedback_val" class="feedback_val" value=""/>
                        <input type="hidden" name="why_reason" class="why_reason" value=""/>
                        <button class="btn btn-red yes_apply" name="yes_apply" type="submit">Get 20% Off!</button>
                        <button class="no_apply" name="no_apply">Cancel anyway  
        <!--                    <small>I don't want to claim this offer</small>-->
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--------end 20% off popup *****-->
<!--------Switch designer popup start *****-->
<div class="modal fade slide-3 similar-prop model-close-button in" id="switch_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Confirmation</h2>
                <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
            </header>
            <div class="modal-body">
                <div class="stay-offer sure-switch">
                    <p>
                        We will switch your designer & give 20% off the next month for this inconvenience.
                        <span> 20% off</span>  
                    </p>
                </div>
                <div class="congrts">
                    <form action="<?php echo base_url(); ?>customer/ChangeProfile/cancel_current_plan" method="post">
                        <input type="hidden" name="customer_id" class="customer_id" value=""/>
                        <input type="hidden" name="reason_val" class="reason_val" value=""/>
                        <input type="hidden" name="is_switch_designer" class="is_switch_designer" value=""/>
                        <input type="hidden" name="feedback_val" class="feedback_val" value=""/>
                        <button class="btn btn-red yes_switch_cnfrm" name="yes_switch_cnfrm" type="submit">Confirm</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--------Switch designer popup end *****-->

<!--------start 20% off popup *****-->
<div class="modal fade slide-3 similar-prop model-close-button in" id="sorry_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt"></h2>
                <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
            </header>
            <div class="modal-body">
                <div class="sorry-subscr">
                    <p>Your cancellation request is already in process,<br/> Your account manager will contact you <br/> within 1-2 business days.<br/>Thanks!</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--------end 20% off popup *****-->
<div class="modal fade slide-3 similar-prop model-close-button in" id="changedes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt"> Why you want to change your designer </h2>
                <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
            </header>
            <div class="modal-body">
                <form action="<?php echo base_url(); ?>customer/ChangeProfile/change_designer_requestFromClient" method="post">

                    <div id="clientmsg" class="msg">
                        <p></p>
                    </div>


                    <label>Choose Reason:</label>
                    <select name="reason_selected" id="cars" class="form-control" required>
                        <option value="">--Select Reason --</option>
                        <option value="volvo" >Volvo</option>
                        <option value="saab">Saab</option>
                        <option value="mercedes">Mercedes</option>
                        <option value="audi">Audi</option>
                        <option value="other" >Other</option>
                    </select>
                    <label>Reason Mentoined:</label>
                    <textarea name="addtional_text" placeholder="enter your addtional_text(optional)" class="form-control"><?php echo $addtional_text; ?>
                    </textarea>
                    <div class="butnsubmit" style="display: none; ">
                        <button class="btn-info" type="submit">Change</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<style>
    div#clientmsg p {
        color: red;
    }
    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 300px;
        margin: auto;
        text-align: center;
        font-family: arial;
    }

    .title {
        color: grey;
        font-size: 18px;
    }

    button {
        border: none;
        outline: 0;
        display: inline-block;
        padding: 8px;
        color: white;
        background-color: #000;
        text-align: center;
        cursor: pointer;
        width: 100%;
        font-size: 18px;
    }

    a {
        text-decoration: none;
        font-size: 22px;
        color: black;
    }

    button:hover, a:hover {
        opacity: 0.7;
    }
</style>
