<section>
    <div class="container">
        <div class="header-blog">
            <?php
            if ($this->session->flashdata('message_error') != '') {
                ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable success_msg">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="account-tab">
                    <ul class="list-header-blog">
                        <li class="active"><a href="#general" class="stayhere" data-toggle="tab">Dashboard</a></li>
                        <li><a href="#payment_history" class="stayhere" data-toggle="tab">Payment History</a></li>
                        <li><a href="#payment_settings" class="stayhere" data-toggle="tab">Payment Setting</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-content affiliated-wrap">
            <div class="tab-pane active" id="general">
                
                 <div class="row">
                    <div class="col-md-12">
                        <div class="commission-records">
                            <div class="white-boundries">
                                <h2 class="main-info-heading">Records</h2>
                                <div class="inner-record">
                                    <div class="record-item record_total">
                                        <h3>Lifetime Earning</h3>
                                        <div class="cust-nmr">
                                           <span> <?php echo ($totalusers > 1) ? 'Users' : 'User'; ?></span> <span><?php echo $totalusers;?></span>
                                        </div>
                                        <div class="total-pnt">
                                            <span>Total Points</span><span> <?php echo '$' . $totalcomm; ?></span>
                                        </div>
                                    </div>
                                    <div class="record-item record_month">
                                        <h3>Current Month</h3>
                                        <div class="cust-nmr">
                                            <span> <?php echo ($totalmonthusers > 1) ? 'Users' : 'User'; ?></span><span><?php echo $totalmonthusers;?></span>
                                        </div>
                                        <div class="total-pnt">
                                            <span>Total Points</span><span> <?php echo '$' . $monthcomm; ?></span>
                                        </div>
                                    </div>
                                    <div class="record-item record_week">
                                        <h3>Current Week</h3>
                                        <div class="cust-nmr">
                                            <span> <?php echo ($totalweeklyusers > 1) ? 'Users' : 'User'; ?></span><span><?php echo $totalweeklyusers;?></span>
                                        </div>
                                        <div class="total-pnt">
                                            <span>Total Points</span><span> <?php echo '$' . $weekcomm; ?></span>
                                        </div>
                                    </div>
                                    <div class="record-item record_today">
                                        <h3>Today</h3>
                                        <div class="cust-nmr">
                                            <span> <?php echo ($totaltodayusers > 1) ? 'Users' : 'User'; ?></span><span><?php echo $totaltodayusers;?></span>
                                        </div>
                                        <div class="total-pnt">
                                            <span>Total Points</span><span> <?php echo '$' . $todaycomm; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <?php //echo "<pre/>";print_R($aff_info);exit; ?>
                        <div class="white-boundries">
                            <div class="inner-copy-link">   
                                <h2 class="main-info-heading">Referral Code</h2>
                                <label class="form-group top-link">
                                    <span class="copiedaffone"></span>
                                    <p class="label-txt label-active">your link</p>
                                    <input type="text" class="input show_aff" name="link" value="<?php echo $aff_info[0]['shared_url']; ?>" readonly>
                                    <span class="link-copy-img copy_affiliated_link">
                                        <img src="<?php echo base_url(); ?>public/assets/img/customer/copy-link.svg<?php echo $chat_customer[$i]['image']; ?>" class="img-responsive" />
                                    </span>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                                <button class="generate_shorten" data-url="<?php echo $aff_info[0]['shared_url']; ?>" style="<?php echo ($aff_info[0]['shared_shorturl'] != '') ? 'display:none' : 'display:block' ?>">Generate Shorten Link</button>
                                <img src="<?php echo base_url(); ?>public/assets/img/customer/gz_customer_loader.gif" class="img-responsive generate_loader" style="display:none"/>
                                <label class="form-group shortn_url" style="<?php echo ($aff_info[0]['shared_shorturl'] != '') ? 'display:block' : 'display:none' ?>">
                                    <span class="copiedshortaff"></span>
                                    <p class="label-txt label-active">Shorten link</p>
                                    <input type="text" class="input show_aff_short" name="link" value="<?php echo $aff_info[0]['shared_shorturl']; ?>" readonly>
                                    <span class="link-copy-img copy_short_affiliated_link">
                                        <img src="<?php echo base_url(); ?>public/assets/img/customer/copy-link.svg<?php echo $chat_customer[$i]['image']; ?>" class="img-responsive" />
                                    </span>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>   
                            <div class="inner-copy-link share-via-item">   
                                <h2 class="main-info-heading">Share this via</h2>
                                <div class="share-this">
                                    <ul>
                                        <li class="shr-mail">
                                            <a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo $aff_info[0]['shared_url']; ?>">
                                                <i class="fas fa-envelope"></i> email
                                            </a>
                                        </li>
                                        <li class="shr-fb">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $aff_info[0]['shared_url']; ?>&redirect_uri=https://facebook.com&title=<?php echo $aff_info[0]['shared_url']; ?>" target="_blank">
                                                <i class="fab fa-facebook-f"></i> facebook
                                            </a>
                                        </li>
                                        <li class="shr-whtsapp">
                                            <a href="https://api.whatsapp.com/send?phone=whatsappphonenumber&text=<?php echo $aff_info[0]['shared_url']; ?>" target="_blank">
                                                <i class="fab fa-whatsapp"></i> whatsapp
                                            </a>
                                        </li>
                                        <li class="shr-twt">
                                            <a href="https://twitter.com/intent/tweet?text=<?php echo $aff_info[0]['shared_url']; ?>" target="_blank">
                                                <i class="fab fa-twitter"></i> twitter
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="white-boundries">
                            <div class="gz_points">
                                <h2 class="main-info-heading">Earned Commision</h2>
                                <div class="gz_points_tbl">
                                    <div class="gz_points_left">
                                        <span>
                                            <sup>Available Balance</sup>
                                            <strong><?php echo '$' . $totalavailablecomm; ?></strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="additional-terms">
                                    <h3>Additional terms</h3>
                                    <ul>
                                        <li>Commissions will be paid by the 15th of the following month.</li>
                                        <li>All the Commissions earned will be <?php echo COMMISION_FEES; ?>% of the monthly bill for the customer that you have reffered to Graphics Zoo.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="payment_history">
                <?php $this->load->view('customer/payment_history_temp', $payment_history); ?>
            </div>
            <div class="tab-pane" id="payment_settings">
                <div class="white-boundries">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="affliated_payment_setting">
                                <form action="<?php echo base_url() . 'customer/Affiliate/UpdatePaypalID' ?>" method="post">
                                    <div class="inner-copy-link">
                                        <h2 class="main-info-heading">Payment Setting</h2>   
                                        <label class="form-group">
                                            <p class="label-txt label-active">paypal id <span>*</span></p>
                                            <input type="text" class="input" name="paypal_id" value="<?php echo (isset($aff_info[0]['paypal_id']) && $aff_info[0]['paypal_id'] != '') ? $aff_info[0]['paypal_id'] : ''; ?>" required="">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                        <input type="submit" class="affiliated-btn" href="#" value="save changes">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="affliated_content">
                                <p> Please enter your PayPal ID where we can send you the commission you have earned by referring clients to Graphics Zoo. Your commission will be sent to you once per month between the 15th and 20th for the previous months' commission.</p>
                                <p> i.e. If you earned $525 in commissions for the month of June, that will be sent to you between July 15-20.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
