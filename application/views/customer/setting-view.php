<style type="text/css">
    body.deshboard-bgnone {
        padding-right: 0px !important;
    }
    body{
        height: auto !important;
    }
    @font-face {
        font-family: 'Lato';
        font-style: normal;
        font-weight: 400;
        src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v14/S6uyw4BMUTPHjx4wWw.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Lato';
        font-style: normal;
        font-weight: 700;
        src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh6UVSwiPHA.ttf) format('truetype');
    }
    /* {
      position: relative;
      box-sizing: border-box;
    }*/
    html {
        /*font-size: 62.5%;*/
        background: #ebebeb;
    }
    html,
    body { 
        height: 100%;
    }

    /* 
      Standard ID-1 cards are 85.60 × 53.98 mm   
    */
    .credit-card {    position: relative;
                      padding: 20px;
                      font-family: "Lato", sans-serif;
                      border: #eeeff1;
                      border-top-color: #333;
                      border-radius: 18px;
                      background: #eeeff1;
                      box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.28);
                      transition: background 0.28s ease-in-out;
    }
    .credit-card.credit-card--with-stripe:before {
        content: "";
        position: absolute;
        bottom: 15px;
        left: 0;
        width: 100%;
        height: 50px;
        background: rgba(0, 0, 0, 0.25);
    }
    .credit-card.credit-card--visa {
        /* Aesthetics */
    }
    .credit-card .credit-card__title {
        margin-bottom: 20px;
        font-size: 20px;
        font-weight: 700;
        letter-spacing: 0.12rem;
        color: #e73250;
        text-shadow: 0 1px rgba(0, 0, 0, 0.18);
    }
    .credit-card .credit-card__form input {
        width: 100%;
        padding: 0.7rem;
        font-size: 2rem;
        line-height: 1.2;
        font-family: "Lato", sans-serif;
        /* Aesthetics */
        color: #666;
        border: 1px solid #222;
        border-bottom: 1px solid #333;
        border-radius: 3px;
        box-shadow: 0 0 0 1px #505050;
    }

    .credit-card .credit-card__form fieldset {
        width: 100%;
        padding: 0;
        margin: 0;    position: relative;
        border: 0;
        margin-bottom: 5px;
    }
    .credit-card .credit-card__form .credit-card__number {
        background: #fff;
        margin-bottom: 2rem;
        padding: 10px;
        font: normal 17px/25px 'GothamPro', sans-serif;
        color: #596375;
    }
    .credit-card .credit-card__form .credit-card__expiry-month,
    .credit-card .credit-card__form .credit-card__expiry-year,
    .credit-card .credit-card__form .credit-card__cv-code {
        display: inline-block;
        margin-right: 1rem;
        width: 63px;
        text-align: center;
        background: #fff;
        padding: 10px;    
        font: normal 14px/10px 'GothamPro', sans-serif;
        color: #596375;
    }
    /* End Credit Card */
    .pull-right {
        float: right;
        margin: 0 !important;
    }
    .l-centered {
        margin: 20px auto;
    }

    /* css for change current plan */
    .modal-header .close {
        margin-top: -2px;
    }
    .model-tour::before {
        content: '';
        background: url(../uploads/images/save.png);
        width: 120px;
        height: 130px;
        position: absolute;
        right: 13px;
        top: 3px;
        z-index: 1;
    }
    button.close {
        -webkit-appearance: none;
        padding: 0;
        cursor: pointer;
        background: 0 0;
        border: 0;
    }
    .model-close-button .close {
        background: #cecece !important;
        opacity: 1;
        text-shadow: 0 0 0;
        border-radius: 50%;
        width: 30px;
        height: 30px;
        color: white;
    }


    .out-project {
        margin: 0;
        line-height: 1.42857143;
        text-align: center;
        font-size: 26px;
        padding: 0px 0 0;
    }
    .plan {
        text-align: center;
        font-size: 16px;
        color: #898989;
        padding: 11px 0 0;
    }
    .model-tour {
        display: inline-block;
        width: 100%;
    }
    .model-tour::before {
        content: '';
        background: url(../uploads/images/save.png);
        width: 120px;
        height: 130px;
        position: absolute;
        right: 13px;
        top: 3px;
        z-index: 1;
    }
    .tour {
        box-shadow: 0 0px 10px #cecece;
        padding: 15px 0 0;
        border-radius: 9px;
    }
    .tour h3 {
        font-size: 17px;
        font-weight: 500;
        color: #e8304d;
        padding-bottom: 9px;
    }
    .tour h4 {
        color: #253858;
        font-size: 14px;
        font-weight: 500;
        padding-bottom: 4px;
    }
    .tour h2 {
        color: #616a7a;
        font-size: 24px;
        font-weight: 500;
        padding-top: 0px; 
    }
    .mon {
        font-size: 16px;
        text-transform: uppercase;
    }
    .ud-dat-p {
        background: #e8304d;
        color: white;
        padding: 10px 36px;
        border-radius: 50px;
        display: inline-block;
        font-size: 14px;
        font-weight: 600;
        margin: 12px 0 17px;
        border: 0;
    }
    .ud-dat-p:hover {
        background: #ab162f;
        color: white;
        text-decoration: none;
    }
    .tour h5 {
        font-size: 16px;
        color: #616a7a;
        font-weight: 600;
        padding-bottom: 8px;
        border-bottom: 1px solid #cac8c8;
    }
    .list-unstyled {
        padding-left: 0;
        list-style: none;
    }
    .tour ul li {
        font-size: 12px;
        line-height: 33px;
        border-bottom: 1px solid #cac8c8;
        font-weight: 500;
        color: #616a7a;
    }


    .fotter-mdl-p {
        text-align: center;
    }
    .cancel-p {
        background: #e8304d;
        color: white;
        border: 0;
        padding: 13px 45px;
        border-radius: 50px;
        font-size: 15px;
        font-weight: 600;
        margin: 25px 0 0;
    }
    #CnfrmPopup .modal-dialog {
        width: 480px;
        margin: 50px auto;
    }
/*    #CnfrmPopup .modal-content{
        height: 200px;
    }*/
    #CnfrmPopup .cli-ent-model {
        padding: 60px;
    }
    button.btn.btn-CnfrmChange{
        background: #37c473;
        color: #fff;
        width: 30%;
    }
    button.btn.btn-ndelete {
        background: #e73250;
        color: #fff;
        width: 30%;
    }
    .ChangeBillInfo {
        text-align: right;
        float: right;
        z-index: 1;
    }
    label.inplabel {
        margin-bottom: 5px;
    }
    button.close {
        z-index: 999;
    }
    .profile-page{
        padding-top: 0px; 
    }
</style>
<?php
if (isset($data[0]['first_name'])) {
    $first_name = $data[0]['first_name'];
}
if (isset($data[0]['last_name'])) {
    $last_name = $data[0]['last_name'];
}
?>

<p class="space-b"></p>
<section class="con-b">
    <div class="container">
        <h2 class="head-d text-left profile-page">My Profile</h2>   

        <p class="space-d"></p>
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_error'); ?></ps>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_success'); ?></ps>
            </div>
        <?php } ?>
        <div class="row" id="profile_form">
            <!-- Saved Form Start -->
            <div class="saved_form">
                <div class="col-md-4">
                    <div class="project-row-qq1 settingedit-box">
                        <div class="settrow">

                            <div class="settcol right">
                                <a id="edit_button" class="review-circleww" href="javascript:void(0)"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-edit.png" class="img-responsive"></a> 
                            </div>
                        </div>

                        <div class="setimg-row33">
                            <figure class="setimg-box33 dispa">
                                <?php if ($data[0]['profile_picture']): ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$data[0]['profile_picture']; ?>" class="img-responsive telset33">
                                <?php else: ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive telset33">
                                    <?php /* $first_name = ""; $last_name="";
                                      if(isset($data[0]['first_name'])){
                                      $first_name = substr($data[0]['first_name'],0,1);
                                      }
                                      if(isset($data[0]['last_name'])){
                                      $last_name = substr($data[0]['last_name'],0,1);
                                      }
                                      echo $first_name.$last_name; */ ?>

                                <?php endif ?>
                            </figure>
                        </div>

                        <p class="space-a"></p>

                        <div class="form-group">
                            <h3 class="sub-head-h text-center">
                                <?php echo $first_name . ' ' . $last_name; ?>
                            </h3>
                        </div>

                        <div class="form-group">
                            <h3 class="head-c text-center">
                                <?php if (isset($data[0]['notification_email'])): ?>
                                    <?php echo $data[0]['notification_email']; ?>
                                <?php endif ?>
                            </h3>
                        </div>

                        <p class="seter"><span>Administrative Email</span></p>
                        <p class="space-e"></p>
                        <p class="chngpasstext">
                            <a href="" class="change_pass" name="savebtn" value="Change Password" data-toggle="modal" data-target="#myModal"  ><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/change-password.png" class="img-responsive"> <span>Change Password</span></a></p>
                    </div>	
                </div>
            </div>
            <!-- Saved Form End -->

            <!-- End Form Start -->


            <div class="edit_form" style="display: none;">
                <form enctype="multipart/form-data" method="post" action="" id="edit_form_data">
                    <div class="col-md-4">
                        <div class="project-row-qq1 settingedit-box">
                            <div class="settrow">
                                <div class="settcol right">
                                    <p class="btn-x"><input type="submit" id="savebtn1" name="savebtn" class="btn-e save_info" value="Save"/></p> 
                                </div>
                            </div>

                            <div class="setimg-row33 changeprofilepage">
                                <div class="imagemain2" style="padding-bottom: 20px; display: none;">
                                    <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="profile" data-plugin="dropify" id="inFile" style="display: none"/>
                                </div>
                                <div class="clearfix profile_image_sec">
                                    <div class="imagemain">
                                        <figure class="setimg-box33">
                                            <?php if ($data[0]['profile_picture']): ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$data[0]['profile_picture']; ?>" class="img-responsive telset33">
                                            <?php else: ?>
                                                <div class="myprofilebackground" style="width:50px;height:50px;border-radius:50%;background: #0190ff;text-align: center;padding-top: 15px;font-size: 15px;color: #fff; margin: 0 auto;">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive telset33">
                                                    <?php /* $first_name = ""; $last_name="";
                                                      if(isset($data[0]['first_name'])){
                                                      $first_name = substr($data[0]['first_name'],0,1);
                                                      }
                                                      if(isset($data[0]['last_name'])){
                                                      $last_name = substr($data[0]['last_name'],0,1);
                                                      }
                                                      echo $first_name.$last_name; */ ?>

                                                </div>
                                            <?php endif; ?>

                                            <a class="setimg-blog33 link1 font18 bold" href="#"  onclick="$('.dropify').click();" class="link1 font18 bold" for="profile">
                                                <span class="setimgblogcaps">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-camera.png" class="img-responsive"><br>
                                                    <span class="setavatar33">Change <br>Avatar</span>
                                                </span>
                                            </a>
                                        </figure>
                                        <style>
                                            .dropify-wrapper{ display:none; height: 200px !important; }
                                        </style> 
                                    </div>

                                    <div class="imagemain3" style="display: none;">
                                        <figure class="setimg-box33">

                                            <img id="image3" src="" class="img-responsive telset33">


                                            <a class="setimg-blog33 link1 font18 bold" href="#"  onclick="$('.dropify').click();" class="link1 font18 bold" for="profile">
                                                <span class="setimgblogcaps">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-camera.png" class="img-responsive"><br>
                                                    <span class="setavatar33">Change <br>Avatar</span>
                                                </span>
                                            </a>
                                        </figure>
                                        <style>
                                            .dropify-wrapper{ display:none; height: 200px !important; }
                                        </style> 
                                    </div>
                                </div>
                            </div>
                            <p class="space-a"></p>
                            <div class="form-group setinput">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="first_name" class="form-control input-c" value="<?php
                                            if (isset($data[0]['first_name'])) {
                                                echo $data[0]['first_name'];
                                            }
                                            ?> <?php
                                            if (isset($data[0]['last_name'])) {
                                                echo $data[0]['last_name'];
                                            }
                                            ?>" placeholder="First Name" id="user_name"/>
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-6">
                                            <div class="form-group">
                                                    <input type="text" name="last_name" class="form-control input-c" value="<?php
                                    if (isset($data[0]['last_name'])) {
                                        echo $data[0]['last_name'];
                                    }
                                    ?>" placeholder="Last Name"/>
                                            </div>
                                    </div> -->
                                </div>
                            </div>

                            <div class="form-group setinput">
                                <input type="text" class="form-control input-b" value="<?php echo $data[0]['email']; ?>" name="notification_email" placeholder="Notification Email"/>
                                <p class="seter"><span>Administrative Email</span></p>
                            </div>
                            <p class="space-b"></p>
                            <p class="chngpasstext">
                                <a href="" class="change_pass" name="savebtn" value="Change Password" data-toggle="modal" data-target="#myModal"  ><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/change-password.png" class="img-responsive"> <span>Change Password</span></a></p>
                            <!-- name="savebtn"  -->
                        </div>	
                    </div>
                </form>
            </div>
            <!-- Edit Form End -->

            <div class="about_info">
                <div class="col-md-4">
                    <div class="project-row-qq1 settingedit-box">
                        <div class="settrow">
                            <div class="settcol left">
                                <h3 class="sub-head-b">About Info</h3>
                            </div>
                            <div class="settcol right">
                                <a id="about_infoform" class="review-circleww" href="javascript:void(0)">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-edit.png" class="img-responsive">
                                </a>
                            </div>
                        </div>
                        <p class="space-c"></p>
                        <div class="form-group">
                            <label class="label-x2">Company Name</label>
                            <p class="space-a"></p>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-f"><?php
                                        if (isset($data[0]['company_name'])) {
                                            echo $data[0]['company_name'];
                                        }
                                        ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="label-x2">Title</label>
                                    <p class="space-a"></p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-f"><?php
                                                if (isset($data[0]['title'])) {
                                                    echo $data[0]['title'];
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="label-x2">Phone</label>
                                    <p class="space-a"></p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-f"><?php
                                                if (isset($data[0]['phone'])) {
                                                    echo $data[0]['phone'];
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="label-x2">Address</label>
                                    <p class="space-a"></p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-f"><?php
                                                if (isset($data[0]['address_line_1'])) {
                                                    echo $data[0]['address_line_1'];
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label class="label-x2">City</label>
                                    <p class="space-a"></p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-f"><?php
                                                if (isset($data[0]['city'])) {
                                                    echo $data[0]['city'];
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="label-x2">Zip</label>
                                    <p class="space-a"></p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-f"><?php
                                                if (isset($data[0]['zip'])) {
                                                    echo $data[0]['zip'];
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="label-x2">State</label>
                                    <p class="space-a"></p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-f"><?php
                                                if (isset($data[0]['state'])) {
                                                    echo $data[0]['state'];
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="label-x2">Timezone</label>
                                    <p class="space-a"></p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-f"><?php
                                                if (isset($data[0]['timezone'])) {
                                                    echo $data[0]['timezone'];
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>	
                </div>
            </div>
            <div class="about_info_edit" style="display: none;">
                <form method="post" id="about_info_form">
                    <div class="col-md-4">
                        <div class="project-row-qq1 settingedit-box">
                            <div class="settrow">
                                <div class="settcol left">
                                    <h3 class="sub-head-b">About Info</h3>
                                </div>
                                <div class="settcol right">
                                    <p class="btn-x">
                                            <!-- <a href="<?php echo base_url() ?>customer/request/setting-view" class="btn-e save_info" name="savebtn" value="Save Info">Save</a> -->
                                        <input type="submit" id="about_info_btn" name="savebtn" class="btn-e save_info" value="Save"/>
                                    </p>
                                </div>
                            </div>
                            <p class="space-d"></p>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-c" value="<?php
                                        if (isset($data[0]['company_name'])) {
                                            echo $data[0]['company_name'];
                                        }
                                        ?>" placeholder="Company Name" name="company_name">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-c" value="<?php
                                        if (isset($data[0]['title'])) {
                                            echo $data[0]['title'];
                                        }
                                        ?>" placeholder="Title" name="title">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-c" value="<?php
                                        if (isset($data[0]['phone'])) {
                                            echo $data[0]['phone'];
                                        }
                                        ?>" name="phone" placeholder="Phone">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea class="form-control textarea-b" name="address_line_1" placeholder="<?php echo $data[0]['address_line_1']; ?>"><?php
                                            if (isset($data[0]['address_line_1'])) {
                                                echo $data[0]['address_line_1'];
                                            }
                                            ?></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-c" value="<?php
                                        if (isset($data[0]['address_line_1'])) {
                                            echo $data[0]['city'];
                                        }
                                        ?>" name="city" placeholder="City">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-c" value="<?php
                                        if (isset($data[0]['address_line_1'])) {
                                            echo $data[0]['state'];
                                        }
                                        ?>" name="state" placeholder="State">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-c" value="<?php
                                        if (isset($data[0]['address_line_1'])) {
                                            echo $data[0]['zip'];
                                        }
                                        ?>" name="zip" placeholder="Zip">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <select class="form-control input-c" name="timezone" placeholder="Timezone">
                                            <?php for ($i = 0; $i < sizeof($timezone); $i++) { ?>
                                                <option value="<?php echo $timezone[$i]['zone_name']; ?>" <?php
                                                if ($data[0]['timezone'] == $timezone[$i]['zone_name']) {
                                                    echo 'selected';
                                                }
                                                ?> ><?php echo $timezone[$i]['zone_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            </div>


                        </div>	
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="project-row-qq1 settingedit-box">
                    <div class="settrow">
                        <div class="settcol left">
                            <h3 class="sub-head-b">Billing and Subscription</h3>
                        </div>
                    </div>
                    <p class="space-b"></p>
                    <div class="settrow yss1">
                        <div class="settcol left">
                            <h3 class="sub-head-f">Current Plan</h3>
                        </div>
                        <!-- <div class="settcol right">
                                <p class="setrightstst">Cycle: May 01,2018-June 01, 2018</p> 
                        </div> -->
                    </div>
                    <p class="space-a"></p>
                    <?php if ($plan) { //echo "<pre>";print_r($plan);  ?>
                        <div class="cyclebox25">
                            <div class="settrow">
                                <div class="settcol left">
                                    <h3 class="sub-head-f"><?php if ($plan->interval == "year") { ?>Annual Golden Plan<?php
                                        } elseif ($plan->interval == "month") {
                                            echo "Monthly Golden Plan";
                                        }
                                        ?></h3>
                                    <p class="setpricext25">$<?php echo $plan->amount / 100; ?></p>
                                </div>
                                <div class="settcol right">
                                    <p class="btn-x">
                                        <a href="" data-toggle="modal" data-target="#ChangePlan" class="btn-f Change_Plan chossbpanss">Change Plan</a></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="settrow">
                        <?php
                        if ($card) {
                            // echo "<pre>";  Print_r($card['0']);exit();
//							foreach($card as $cd){ 
                            ?>
                            <div class="credit-card credit-card--with-stripe credit-card--visa l-centered">
                                <div class="ChangeBillInfo">
                                    <p class="btn-x"><a href="" data-toggle="modal" data-target="#ChangeBillInfo" class="btn-f Change_Plan chossbpanss">Edit Billing Info</a></p>
                                </div>
                                <div class="credit-card__title">
                                    Card Details
                                </div>
                                <form class="credit-card__form">
                                    <div class="credit-card__number">************<?php echo $card['0']->last4; ?></div>
                                    <fieldset>
                                        <div class="credit-card__expiry-month"><?php echo $card['0']->exp_month; ?></div>
                                        <div class="credit-card__expiry-year"><?php echo $card['0']->exp_year; ?></div>
                                        <div class="credit-card__cv-code pull-right">***</div>

                                    </fieldset>
                                </form>
                                <div class="dtlBillingInfo"></div>
                            </div>
                            <?php
                            // }
                        }
                        ?>
                    </div>
                    <p class="space-b"></p>						
                </div>
            </div>	
        </div>

    </div>

</div>
<!-- Change Current plan  -->
<!--<div class="modal fade" id="ChangePlan" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title out-project" id="myModalLabel"> You’re out of projects</h4>
                <p class="plan">Upgrade your plan to enjoy unlimited projects and much more!</p>
            </div>
            <div class="modal-body model-tour">
                <?php
//  echo "<pre>";
// print_r($data);
                ?>

                <div class="col-md-6 col-sm-6 text-center">
                    <div class="tour">
                        <h3>Golden Plan</h3>   
                        <h4>MONTHLY</h4>     
                        <h2>$299 <span class="mon">/Mon </span></h2>
                        <?php if ($data[0]['plan_name'] != 'M299G') { ?>
                            <a href="javascript:void(0)" data-value="M299G" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p">
                                Change Plan
                            </a>
                        <?php } else {
                            ?>
                            <a type="button" class="ud-dat-p currentPlan" disabled>
                                Current Plan
                            </a>
                        <?php } ?>
                         <a href="" type="button" class="ud-dat-p" data-dismiss="modal">UPGRADE NOW</a> 
                        <h5>This Includes</h5>
                        <ul class="list-unstyled">
                            <li>1-Day Turnaround</li>
                            <li>14 Day Risk Free Guarantee</li>
                            <li>No Contract</li>
                            <li> Unlimited Design Requests</li>
                            <li> 1 Active Request</li>
                            <li> Unlimited Revisions</li>
                            <li> Quality Designers</li>
                            <li> Dedicated Support</li>
                            <li>Unlimited free stock images from</li> 
                            <li>Pixabay and Unsplash</li>
                        </ul>
                    </div>
                </div>



                <div class="col-md-6 col-sm-6 text-center">
                    <div class="tour">
                        <h3>Golden Plan</h3>   
                        <h4>ANNUAL</h4>     
                        <h2>$239 <span class="mon">/Mon </span></h2>
                        <?php if ($data[0]['plan_name'] != 'A2870G') { ?>
                            <a href="javascript:void(0)" data-value="A2870G" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p">
                                Change Plan
                            </a>
                        <?php } else {
                            ?>
                            <a type="button" class="ud-dat-p currentPlan" disabled>
                                Current Plan
                            </a>
                        <?php } ?>
                        <h5>This Includes</h5>
                        <ul class="list-unstyled">
                            <li>1-Day Turnaround</li>
                            <li>14 Day Risk Free Guarantee</li>
                            <li>No Contract</li>
                            <li> Unlimited Design Requests</li>
                            <li> 1 Active Request</li>
                            <li> Unlimited Revisions</li>
                            <li> Quality Designers</li>
                            <li> Dedicated Support</li>
                            <li>Unlimited free stock images from</li> 
                            <li>Pixabay and Unsplash</li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="modal-footer fotter-mdl-p">


            </div>
        </div>
    </div>
</div>

 Confirm popup for change plan 
<div class="modal fade" id="CnfrmPopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c text-center">Are you sure you want to Change Current Plan? </h3>
                    </header>
                    <form action="<?php echo base_url(); ?>customer/ChangeProfile/change_current_plan" method="post" role="form" class="ChangeCurrent_Plan" id="ChangeCurrent_Plan">
                        <input type="hidden" name="current_user_id" value="<?php echo $data[0]['id']; ?>">
                        <input type="hidden" class="form-control" name="plan_name" id="plan_name" value="" >
                        <div class="confirmation_btn text-center">
                            <button class="btn btn-CnfrmChange" type="submit">Yes</button>
                            <button class="btn btn-ndelete" data-dismiss="modal" aria-label="Close">No</button>

                        </div>

                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>-->
<!-- Change Billing Info --> 
<div class="modal fade" id="ChangeBillInfo" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-d text-left" style="margin-top:40px;">Billing Method</h3>
                    </header>
                    <div class="clearfix billing_subs_right">
                        <!--    <div class="title_box">
                        <h2 class="head-d text-left">Subscription <span class="update_subs">
                                <p class="btn-x"> <input  id="savebtn" type="button" name="savebtn" value="Update Subscription" class="form-control btn-e"/></p></span></h2>
                        </div>-->

                        <div class="clearfix billing_subs_left">
                            <form method="post" action="<?php echo base_url(); ?>customer/ChangeProfile/change_plan">
                                <div class="payment-method">

                                    <div class="pmformmain">

                                        <div class="card-js">
                                            <div class="form-group col-md-12">
                                                <label class="inplabel">Card Number</label>
                                                <input type="text" name="card-number" class="card-number form-control" placeholder="Card Number"  required="" maxlength="16"></div>

                                            <div class="form-group col-md-7">
                                                <label class="inplabel">Expiry Date</label><br>
                                                <input class="expiry-month" placeholder="Expiry Month" name="expiry-month" required="" maxlength="2">
                                                <input class="expiry-year" placeholder="Expiry Year" name="expiry-year" required="" maxlength="2">
                                            </div>

                                            <div class="form-group col-md-5">
                                                <label class="inplabel">CVC</label>
                                                <input type="text" name="cvc" id="cvc" class="form-control" placeholder="CVV" maxlength="4" required="">
                                            </div>

                                        </div>
                                        <div class="form-group change_card_details btn-x" style="margin-top: 10px;">
                                            <input type="hidden" name="current_plan" value="<?php echo $current_plan; ?>" />
                                            <input type="submit" value="Edit Billing Info" class="btn btn-e site-btn" name="change_my_card_deatils"/>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Change Password</h3>
                    </header>
                    <div class="fo-rm-body">
                        <form method="post" action="<?php echo base_url(); ?>customer/ChangeProfile/change_password_front">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Current Password</label>
                                        <p class="space-a"></p>
                                        <input type="password" class="form-control input-c" name="old_password" required>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">New Password</label>
                                        <p class="space-a"></p>
                                        <input type="password" class="form-control input-c"  name="new_password" required>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Confirm Password</label>
                                        <p class="space-a"></p>
                                        <input type="password" class="form-control input-c" name="confirm_password" required>
                                    </div>
                                </div>
                            </div>
                            <p class="btn-x"><button class="btn-g" name="savebtn">Save</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</section>
<button style="display: none;" id="CnfrmPopup_msg" data-toggle="modal" data-target="#CnfrmPopup">click here</button>
<!-- jQuery (necessary for JavaScript plugins) -->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>theme/customer/bootstrap.min.js"></script>
<script>
                                                $(document).ready(function () {

                                                    $('#edit_form_data').submit(function (e) {
                                                        e.preventDefault();
                                                        var formData = new FormData($(this)[0]);
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: '<?php echo base_url(); ?>/customer/ChangeProfile/edit_profile_image_form',
                                                            data: formData,
                                                            async: false,
                                                            cache: false,
                                                            contentType: false,
                                                            processData: false,
                                                            success: function (data) {
                                                                location.reload(true);
                                                            }
                                                        });
                                                    });

                                                    $('#about_info_form').submit(function (e) {
                                                        e.preventDefault();
                                                        var formData = new FormData($(this)[0]);
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: '<?php echo base_url(); ?>/customer/ChangeProfile/customer_edit_profile',
                                                            data: formData,
                                                            async: false,
                                                            cache: false,
                                                            contentType: false,
                                                            processData: false,
                                                            success: function (data) {
                                                                //alert(data);
                                                                location.reload(true);
                                                            }
                                                        });
                                                    });

                                                    $('#togal_form').click(function () {
                                                        $("#password_form").toggle();
                                                    });
                                                    $('#edit_button').click(function () {
                                                        $('.saved_form').hide();
                                                        $('.edit_form').css('display', 'block');
                                                    });

                                                    $('#about_infoform').click(function () {
                                                        $('.about_info').hide();
                                                        $('.about_info_edit').css('display', 'block');
                                                    });
                                                });



                                                var $fileInput = $('.file-input');
                                                var $droparea = $('.file-drop-area');

                                                // highlight drag area
                                                $fileInput.on('dragenter focus click', function () {
                                                    $droparea.addClass('is-active');
                                                });

                                                // back to normal state
                                                $fileInput.on('dragleave blur drop', function () {
                                                    $droparea.removeClass('is-active');
                                                });

                                                // change inner text
                                                $fileInput.on('change', function () {
                                                    var filesCount = $(this)[0].files.length;
                                                    var $textContainer = $(this).prev();

                                                    if (filesCount === 1) {
                                                        // if single file is selected, show file name
                                                        var fileName = $(this).val().split('\\').pop();
                                                        $textContainer.text(fileName);
                                                    } else {
                                                        // otherwise show number of files
                                                        $textContainer.text(filesCount + ' files selected');
                                                    }
                                                });
                                                function validateAndUpload(input) {
                                                    //alert("In Function"); 
                                                    var URL = window.URL || window.webkitURL;
                                                    var file = input.files[0];

                                                    if (file) {
                                                        console.log(URL.createObjectURL(file));
                                                        // console.log(file);
                                                        $(".imagemain").hide();

                                                        $(".imagemain2").show();
                                                        $(".imagemain3").show();
                                                        $("#image3").attr('src', URL.createObjectURL(file));

                                                        $(".dropify-wrapper").show();
                                                    }
                                                }
// $(document).on('click', ".ChngPln", function (e) {
//     e.preventDefault();
//    var url = $(this).attr('data-url');
//    $('.btn-CnfrmChange').attr('href', url);
//    $("#ChangePlan").css("display","none");
//    $(".modal-backdrop.fade.in").remove();
//    $('#CnfrmPopup_msg').trigger('click');
// });
//                $(document).on('click', ".Change_Plan", function (e) {
//                   $.ajax({
//        type : 'POST',
//        url : '<?php echo base_url(); ?>customer/request/upgrADE_account',
//        dataType: "json",
//        data: { customer_id: customer_id, plan_name : plan_name,card_number: card_number, expir_date : expir_date,cvc: cvc,term: term},
//        success : function(response){
//         
//         if(response.success){
//         console.log(response.success);
//         console.log('response');
//         $("#Upgradeacoount").css("display","none");
//         $(".modal-backdrop.fade.in").remove();
//         $('#subscribed_USR').trigger('click');
//         
//        }
//        else{
//             console.log(response.error);
//            console.log('User can not Subscribed successfully ..!');
//        }
//        },
//        error:function(response){
//        console.log('User can not Subscribed successfully ..!');
//        $('.bill-infoo .error-msg').append('User can not Subscribed successfully ..!');
//        console.log(response.error);
//       }
//    });
//                });

                                                /*$(document).on('click', ".ChngPln", function (e) {
                                                    e.preventDefault();
                                                    var data_id = $(this).attr('data-value');
                                                    $(this).attr('data-url');
                                                    //$('#plan_name').append(data_id);
                                                    $("#plan_name").val(function () {
                                                        return this.value + data_id;
                                                    });
                                                });*/


                                                function dateFormat(val) {
                                                    if (val.length == 2) {
                                                        $('#expir_date').val($('#expir_date').val() + '/');
                                                    }
                                                }
                                                $('#right_nav > li ').click(function(){
                                                    $(this).toggleClass('open');
                                                });
</script>   
</body>
</html>