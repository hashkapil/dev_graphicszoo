<style type="text/css">
    .check-main-xx3{
            height:150px;
    }
    .select-a::-ms-expand {
        display: none;
    }
</style>
<section class="con-b">
		<div class="container">
                    <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php }
            ?>
        </div>
			<div class="flex-show uplink-row clearfix">
				<span class="uplink-col left"><h3 class="sub-head-d">
					<?php 
					if(isset($_GET['rep']) && $_GET['rep'] == 1){ 
						$stat = "Project Update";	
					} 
					else { 
						$stat = 'Create Project' ;
					} 
					echo $stat;
					?>
				</h3></span>
				<span class="uplink-col right">
					<ul class="list-unstyled list-bread-crumb">
						<li><a href="<?php echo base_url();?>/customer/request/design_request">Dashboard</a></li>
						<li><?php echo $stat; ?></li>
					</ul>
				</span>
			</div>

			<?php $category = array(''=>'Select Design Type','Popular Design' => 'Popular Design', 'Branding & Logos' => 'Branding & Logos','Social Media' => 'Social Media','Marketing' => 'Marketing','Email' => 'Email','Ads & Banners' => 'Ads & Banners','Web Design' => 'Web Design','App Design' => 'App Design');
			?>
			<!-- ,'Custom Request' => 'Custom Request' -->

			

			<form method="post" action="<?php echo base_url(); ?>customer/request/new_request_brief">

			<div class="header-bxx2">
				<ul class="list-unstyled list-tab-xx5">
					<li class="acitve"><a href="<?php echo base_url(); ?>customer/request/new_request/category">Category</a></li>			
					<li><a href="javascript:void(0)">Brief</a></li> 			
					<li><a href="javascript:void(0)">Review</a></li>         
				</ul>
			</div>
			
			<p class="space-d"></p>
			<div class="cate-main-boxx3">
				<div class="row">
					<!-- <div class="col-md-5">
						<div class="cat-box-xx122">
							<h3 class="sub-head-a">Choose Category</h3>
							<p class="space-c"></p>
							<p class="text-b">vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
						</div>
					</div> -->
				
					<div class="col-md-10 col-md-offset-1">
						<div class="cat-form-xx123">
							<div class="form-group">
								<label class="label-x1">Select a Category</label>
								<p class="space-c"></p>
								<select id="cat_page" class="form-control select-a" required name="category" onchange="design(this.value);">
                <?php foreach($category as $key=>$value){ ?>
									<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
								<?php } ?>
								</select>
							</div>
							<p class="space-c"></p>

							<!-- Popular Design -->
							<div class="form-group" id="Popular Design" style="display: none;">
									<div class="plan-boxex-xx5 clearfix">
										
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Facebook post" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Facebook post.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Facebook Post</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Infographic" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Infographic.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Infographic</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
									  <input name="logo-brand" value="Custom Request" type="radio" checked>
									  <span class="checkmark"></span>
									  <div class="check-main-xx3">
										<figure class="chkimg">
											<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Custom Request.jpg" class="img-responsive">
										</figure>										
										<h3 class="sub-head-b text-center">Custom Request</h3>
									  </div>
									</label>

									</div>
							</div>

							<!-- Branding & Logos -->
							<div class="form-group" id="Branding & Logos" style="display: none;">
									<div class="plan-boxex-xx5 clearfix">
										
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Logo" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Logo.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Logo</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Business Card" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Business Card.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Business Card</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="T-Shirts" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/T-Shirts.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">T-Shirts</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
									  <input name="logo-brand" value="Custom Request" type="radio" checked>
									  <span class="checkmark"></span>
									  <div class="check-main-xx3">
										<figure class="chkimg">
											<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Custom Request.jpg" class="img-responsive">
										</figure>										
										<h3 class="sub-head-b text-center">Custom Request</h3>
									  </div>
									</label>

									</div>
							</div>

							<!-- Social Media -->
							<div class="form-group" id="Social Media" style="display: none;">
									<div class="plan-boxex-xx5 clearfix">
										
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Twitter Post" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Twitter Post.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Twitter Post</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Social Media Post" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Social Media Post.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Social Media Post</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Pinterest" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Pinterest.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Pinterest</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Facebook Post" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Facebook Post.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Facebook Post</h3>
										  </div>
										</label>
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Instagram Post" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Instagram Post.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Instagram Post</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Facebook Cover Photo" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Facebook Cover Photo.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Facebook Cover Photo</h3>
										  </div>
										</label>
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Youtube Channel Art" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Youtube Channel Art.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Youtube Channel Art</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Twitter Header" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Twitter Header.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Twitter Header</h3>
										  </div>
										</label>
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Google+ Header" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Google+ Header.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Google+ Header</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Linkedin Post Header" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Linkedin Post Header.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Linkedin Post Header</h3>
										  </div>
										</label>
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Linkedin Banner" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Linkedin Banner.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Linkedin Banner</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
									  <input name="logo-brand" value="Custom Request" type="radio" checked>
									  <span class="checkmark"></span>
									  <div class="check-main-xx3">
										<figure class="chkimg">
											<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Custom Request.jpg" class="img-responsive">
										</figure>										
										<h3 class="sub-head-b text-center">Custom Request</h3>
									  </div>
									</label> 

									</div>
							</div>

							<!-- Marketing -->
							<div class="form-group" id="Marketing" style="display: none;">
									<div class="plan-boxex-xx5 clearfix">
										
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Letterhead" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Letterhead.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Letterhead</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Sales Sheet" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Sales Sheet.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Sales Sheet</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="One Pagers" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/One Pagers.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">One Pagers</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Presentations" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Presentations.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Presentations</h3>
										  </div>
										</label>
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Posters" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Posters.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Posters</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Flyers" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Flyers.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Flyers</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
									  <input name="logo-brand" value="Custom Request" type="radio" checked>
									  <span class="checkmark"></span>
									  <div class="check-main-xx3">
										<figure class="chkimg">
											<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Custom Request.jpg" class="img-responsive">
										</figure>										
										<h3 class="sub-head-b text-center">Custom Request</h3>
									  </div>
									</label>

									</div>
							</div>
							
							<!-- Email -->
							<div class="form-group" id="Email" style="display: none;">
									<div class="plan-boxex-xx5 clearfix">
										
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Email Templates" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Email Templates.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Email Templates</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Email Header" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Email Header.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Email Header</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
									  <input name="logo-brand" value="Custom Request" type="radio" checked>
									  <span class="checkmark"></span>
									  <div class="check-main-xx3">
										<figure class="chkimg">
											<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Custom Request.jpg" class="img-responsive">
										</figure>										
										<h3 class="sub-head-b text-center">Custom Request</h3>
									  </div>
									</label>

									</div>
							</div>

							<!-- Ads & Banners -->
							<div class="form-group" id="Ads & Banners" style="display: none;">
									<div class="plan-boxex-xx5 clearfix">
										
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Facebook Ads" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Facebook Ads.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Facebook Ads</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Skyscraper Ads" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Skyscraper Ads.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Skyscraper Ads</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="300 X 250 Ads" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/300 X 250 Ads.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">300 X 250 Ads</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="728 X 90 Leader Board" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/728 X 90 Leader Board.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">728 X 90 Leader Board</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
									  <input name="logo-brand" value="Custom Request" type="radio" checked>
									  <span class="checkmark"></span>
									  <div class="check-main-xx3">
										<figure class="chkimg">
											<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Custom Request.jpg" class="img-responsive">
										</figure>										
										<h3 class="sub-head-b text-center">Custom Request</h3>
									  </div>
									</label>

									</div>
							</div>

							<!-- Web Design -->
							<div class="form-group" id="Web Design" style="display: none;">
									<div class="plan-boxex-xx5 clearfix">
										
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Landing Page" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Landing Page.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Landing Page</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Multipage Website" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Multipage Website.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Multipage Website</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Slider Images" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Slider Images.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Slider Images</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="Web Forms" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Web Forms.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">Web Forms</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
									  <input name="logo-brand" value="Custom Request" type="radio" checked>
									  <span class="checkmark"></span>
									  <div class="check-main-xx3">
										<figure class="chkimg">
											<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/Custom Request.jpg" class="img-responsive">
										</figure>										
										<h3 class="sub-head-b text-center">Custom Request</h3>
									  </div>
									</label>

									</div>
							</div>

							<!-- App Design -->
							<div class="form-group" id="App Design" style="display: none;">
									<div class="plan-boxex-xx5 clearfix">
										
										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="App Design" type="radio" >
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/App Design.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">App Design</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
										  <input name="logo-brand" value="App Icon" type="radio">
										  <span class="checkmark"></span>
										  <div class="check-main-xx3">
											<figure class="chkimg">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/customer-images/App Icon.jpg" class="img-responsive">
											</figure>										
											<h3 class="sub-head-b text-center">App Icon</h3>
										  </div>
										</label>

										<label class="radio-box-xx2"> 
									  <input name="logo-brand" value="Custom Request" type="radio" checked>
									  <span class="checkmark"></span>
									  <div class="check-main-xx3">
										<figure class="chkimg">
											<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/Custom Request.jpg" class="img-responsive">
										</figure>										
										<h3 class="sub-head-b text-center">Custom Request</h3>
									  </div>
									</label>

									</div>
							</div>

							<div id = "design_patterns">
								<!-- To be filled by javascript -->
							</div>


							
							<p class="space-d"></p>
							
							<div class="uplinkflexs clearfix">
								
									
										<p class="savecols-col left btn-x">
											<input type="submit" value="Next" class="btn btn-a text-uppercase min-250 text-center" name="Next">
										</p>
								
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
			</form>
		</div>
	</section>
    <!-- jQuery (necessary for JavaScript plugins) -->
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/bootstrap.min.js"></script>
	
	<script type="text/javascript">
	function design(str){
		document.getElementById("design_patterns").innerHTML = ""
		document.getElementById("design_patterns").innerHTML = document.getElementById(str).innerHTML
	}
	</script>
  </body>
</html>