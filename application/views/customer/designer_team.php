<?php
$activeList = $change_designer_request[0]['reason_selected']; 
$addtional_text = $change_designer_request[0]['addtional_text']; 
?>
<div class="gz_support_wrap">
    <div class="support_header">
        <h2>GZ Support Team
        </h2>
        <span>GZ team assigned to your account, full time available for your support.</span>
    </div>
    <div class="support_team">
             <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <?php  if($design_teams[0]["d_id"] != ""){ ?>
    <li role="presentation" class="gz_designer active">
          <a href="#gz_designer" aria-controls="gz_designer" role="tab" data-toggle="tab" class="team_member">
    <div class="gz_team_item">
        <div class="gz_team_info">
            <figure>
            <img class="" src="<?php echo $design_teams[0]["d_profile_picture"]; ?>">
            </figure>
            <div class="gz_sp_name">
            <h3><?php echo $design_teams[0]["d_first_name"]." ".$design_teams[0]["d_last_name"]; ?></h3>
            <span>designer</span>
            </div>
        </div>
        <div class="change_d"><i class="icon-gz_edit_icon" id="changedesbtn" aria-hidden="true" title="Change Designer"></i></div>
        <div class="gz_bio">
        <p><?php echo $design_teams[0]["d_about_info"]; ?></p>
        </div>
    </div>
      </a><i class="fas fa-angle-down"></i>
    </li>
    <?php } if($design_teams[0]["qa_id"] != ""){ ?>
    <li class="gz_qa" role="presentation">
         <a href="#gz_qa" aria-controls="gz_qa" role="tab" data-toggle="tab" class="team_member">
         <div class="gz_team_item">
        <div class="gz_team_info">
            <figure>
            <img class="" src="<?php echo $design_teams[0]["qa_profile_picture"]; ?>">
            </figure>
            <div class="gz_sp_name">
            <h3><?php echo $design_teams[0]["qa_first_name"]." ".$design_teams[0]["qa_last_name"]; ?></h3>
            <span>QA</span>
            </div>
        </div>
        <div class="gz_bio">
        <p><?php echo $design_teams[0]["qa_about_info"]; ?></p>
        </div>
    </div>
   </a><i class="fas fa-angle-down"></i>
    </li>
    <?php } if($design_teams[0]["prt_id"] != ""){ ?>
    <li class="gz_prt" role="presentation">
        <a href="#gz_prt" aria-controls="gz_prt" role="tab" data-toggle="tab" class="team_member">
         <div class="gz_team_item">
        <div class="gz_team_info">
            <figure>
            <img class="" src="<?php echo $design_teams[0]["prt_profile_picture"]; ?>">
            </figure>
            <div class="gz_sp_name">
            <h3><?php echo $design_teams[0]["prt_first_name"]." ".$design_teams[0]["prt_last_name"]; ?></h3>
            <span>PRT</span>
            </div>
        </div>
        <div class="gz_bio">
        <p><?php echo $design_teams[0]["prt_about_info"]; ?></p>
        </div>
    </div>
    </a><i class="fas fa-angle-down"></i>
    </li>
    <?php } if($design_teams[0]["am_id"] != ""){ ?>
    <li class="gz_am" role="presentation">
    <a href="#gz_am" aria-controls="gz_am" role="tab" data-toggle="tab" class="team_member">
         <div class="gz_team_item">
        <div class="gz_team_info">
            <figure>
            <img class="" src="<?php echo $design_teams[0]["am_profile_picture"]; ?>">
            </figure>
            <div class="gz_sp_name">
            <h3><?php echo $design_teams[0]["am_first_name"]." ".$design_teams[0]["am_last_name"]; ?></h3>
            <span>AM</span>
            </div>
        </div>
        <div class="gz_bio">
        <p><?php echo $design_teams[0]["am_about_info"]; ?></p>
        </div>
    </div>

    </a><i class="fas fa-angle-down"></i>
    </li>
    <?php } ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?php if($design_teams[0]["d_id"] != ""){ echo "active";} ?> gz_designer" id="gz_designer">
        <div class="gz_support_details ">
            <div class="row">
                <div class="col-md-12">
                    <div class="gz_details">
                        <h3>About <?php echo $design_teams[0]["d_first_name"]." ".$design_teams[0]["d_last_name"]; ?></h3>
                        <p><?php echo $design_teams[0]["d_about_info"]; ?></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="gz_details">
                        <h3>Skills</h3>
                        <ul class="skill_list">
                            <li><h5>Branding & Logos</h5> <span><?php echo $design_teams[0]["d_brandin_logo"]; ?></span></li>
                            <li><h5>Ads & Banners</h5> <span><?php echo $design_teams[0]["d_ads_banner"]; ?></span></li>
                            <li><h5>Email & Marketing</h5> <span><?php echo $design_teams[0]["d_email_marketing"]; ?></span></li>
                            <li><h5>Web Design</h5> <span><?php echo $design_teams[0]["d_webdesign"]; ?></span></li>
                            <li><h5>App Design</h5> <span><?php echo $design_teams[0]["d_appdesign"]; ?></span></li>
                            <li><h5>Graphics</h5> <span><?php echo $design_teams[0]["d_graphics"]; ?></span></li>
                            <li><h5>Illustration</h5> <span><?php echo $design_teams[0]["d_illustration"]; ?></span></li>
                        </ul>   
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="gz_details">
                        <h3>Hobbies</h3>
                        <ul>
                          <?php foreach (explode(",",$design_teams[0]["d_hobbies"]) as $key => $value) {
                                echo "<li>".$value."</li>";
                            }?>
                            </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane gz_qa <?php if($design_teams[0]["d_id"] == "" && $design_teams[0]["qa_id"] != ""){ echo "active";} ?>" id="gz_qa">
    
    <div class="gz_support_details ">
            <div class="row">
                <div class="col-md-12">
                    <div class="gz_details">
                        <h3>About <?php echo $design_teams[0]["qa_first_name"]." ".$design_teams[0]["qa_last_name"]; ?></h3>
                        <p><?php echo $design_teams[0]["qa_about_info"]; ?></p>
                    </div>
                </div>
            <?php if(!empty($design_teams[0]["qa_hobbies"])){ ?>
                <div class="col-md-6">
                    <div class="gz_details">
                        <h3>Hobbies</h3>
                        <ul>
                          <?php foreach (explode(",",$design_teams[0]["qa_hobbies"]) as $key => $value) {
                                echo "<li>".$value."</li>";
                            }?>
                            </ul>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>

    </div>
    <div role="tabpanel" class="tab-pane gz_prt <?php if($design_teams[0]["d_id"] == "" && $design_teams[0]["qa_id"] == "" && $design_teams[0]["prt_id"] != ""){ echo "active";} ?>" id="gz_prt">
    
    <div class="gz_support_details ">
            <div class="row">
                <div class="col-md-12">
                    <div class="gz_details">
                        <h3>About <?php echo $design_teams[0]["prt_first_name"]." ".$design_teams[0]["prt_last_name"]; ?></h3>
                        <p><?php echo $design_teams[0]["prt_about_info"]; ?></p>
                    </div>
                </div>
            <?php if(!empty($design_teams[0]["prt_hobbies"])){ ?>
                <div class="col-md-6">
                    <div class="gz_details">
                        <h3>Hobbies</h3>
                        <ul>
                          <?php foreach (explode(",",$design_teams[0]["prt_hobbies"]) as $key => $value) {
                                echo "<li>".$value."</li>";
                            }?>
                            </ul>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div> 

    </div>
    <div role="tabpanel" class="tab-pane gz_am <?php if($design_teams[0]["d_id"] == "" && $design_teams[0]["qa_id"] == "" && $design_teams[0]["prt_id"] == "" && $design_teams[0]["am_id"] != ""){ echo "active";} ?>" id="gz_am">
            
    <div class="gz_support_details ">
            <div class="row">
                <div class="col-md-12">
                    <div class="gz_details">
                        <h3>About <?php echo $design_teams[0]["am_first_name"]." ".$design_teams[0]["am_last_name"]; ?></h3>
                        <p><?php echo $design_teams[0]["am_about_info"]; ?></p>
                    </div>
                </div>
            <?php if(!empty($design_teams[0]["am_hobbies"])){ ?>
                <div class="col-md-6">
                    <div class="gz_details">
                        <h3>Hobbies</h3>
                        <ul>
                          <?php foreach (explode(",",$design_teams[0]["am_hobbies"]) as $key => $value) {
                                echo "<li>".$value."</li>";
                            }?>
                            </ul>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>

    </div>
  </div>

    </div>
</div>
<div class="modal fade slide-3 similar-prop model-close-button in change_designer_popup" id="changedes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt"> Why you want to change your designer </h2>
        <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
      </header>
      <div class="modal-body">
        <form action="<?php echo base_url(); ?>customer/ChangeProfile/change_designer_requestFromClient" method="post">

            <div id="clientmsg" class="msg">
                <p></p>
            </div>
           
            <div class="form-group">
                <label>Choose Reason:</label>
                <select name="reason_selected" id="select_reason" class="form-control reason_selected" required>
                  <option value="">--Select Reason --</option>
                  <option value="Designer style doesn't match my needs" <?php echo ($activeList = "Directions weren't followed")?"selected":""; ?>>Designer style doesn't match my needs</option>
                  <option value="Designer skill level is not what I need" <?php echo ($activeList = "Designer skill level is not what I need")?"selected":""; ?>>Designer skill level is not what I need</option>
                  <option value="Instructions are not followed" <?php echo ($activeList = "Instructions are not followed")?"selected":""; ?>>Instructions are not followed</option>
                  <option value="I would like to see a different designer" <?php echo ($activeList = "I would like to see a different designer")?"selected":""; ?>>I would like to see a different designer</option>
                  <option value="Designs are taking too long to complete" <?php echo ($activeList = "Designs are taking too long to complete")?"selected":""; ?>>Designs are taking too long to complete.</option>
                  <option value="other" <?php echo ($activeList = "other")?"selected":""; ?>>Other</option>
                </select>
            </div>
            <div class="form-group other_option">
                <label>Reason Mentoined:</label>
                <textarea name="addtional_text" placeholder="enter your addtional_text(optional)" class="form-control"><?php echo $addtional_text; ?>
                </textarea>
            </div>
        
          <div class="butnsubmit" style="display: none; ">
               <button class="btn-info" type="submit">Change</button>
          </div>
    </form>
      </div>
    </div>
  </div>
</div>