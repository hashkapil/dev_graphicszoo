<?php
//echo $allqust['id'] ."!=". LET_DESIGNER_CHOOSE;
//echo "<pre/>";print_R($allqust);
//echo LET_DESIGNER_CHOOSE;exit;
if($allqust['id'] != LET_DESIGNER_CHOOSE && $allqust['id'] != PROJECT_DELIVERABLE && $allqust['id'] != TEXT_INCLUDING){
        if ($allqust['question_type'] == 'text') {
            if ($allqust['id'] == DESIGN_DIMENSION || $allqust['question_label'] == "DESIGN DIMENSIONS") {
                if ($allqust['answer'] != '') {
                    $allqust['answer'] = $allqust['answer'];
                } else {
                    $allqust['answer'] = $editvar['design_dimension'];
                }
                $patern = "[a-z A-Z 0-9 *']{1,}";
            } else {
                $allqust['answer'] = $allqust['answer'];
                 $patern = "[a-z A-Z 0-9]{1,}";
            }
            ?>
            <label class="form-group">
                <div class="title_class_1">
                <p class="label-txt <?php echo (isset($allqust['answer']) && $allqust['answer'] != '') ? 'label-active' : ''; ?>"> <?php echo $allqust['question_label']; ?> <?php echo ($allqust['is_required'] == 1) ? '*' : ''; ?> </p>
                <input type="text" class="input title" name="quest[<?php echo $allqust['id']; ?>]" value="<?php echo (isset($allqust['answer']) && $allqust['answer'] != 'null' && $allqust['answer'] != '') ? $allqust['answer'] : ''; ?>" <?php echo ($allqust['is_required'] == 1) ? 'required' : ''; ?>  pattern="<?php echo $patern; ?>" maxlength="55">
                <div class="line-box">
                    <div class="line"></div>
                </div>
                <p class="discrption"><?php echo (isset($allqust['question_placeholder']) && $allqust['question_placeholder'] != '') ? $allqust['question_placeholder'] : ''; ?></p>
                </div> 
            </label>
            <?php
                }elseif($allqust['question_type'] == 'textarea'){
                    if($allqust['id'] == DESCRIPTION){
                        $html = '';
                        if ($allqust['answer'] != '') {
                            $desc_ans = explode('__', $allqust['answer']);
                           foreach($desc_ans as $dkk => $dvv){
                               $html .= $dvv."\n";
                           }
//                           echo $html;
                           $allqust['answer'] =  $html; 
                        } else {
                            $allqust['answer'] = $editvar['description'];
                        }
                    }else{
                        $allqust['answer'] = $allqust['answer']; 
                    } 
            ?>
            <div class="com_class">  
                <label class="form-group">
                    <p class="label-txt <?php echo (isset($allqust['answer']) && $allqust['answer'] != '') ? 'label-active' : ''; ?>"><?php echo $allqust['question_label']; ?> <?php echo ($allqust['is_required'] == 1) ? '*' : ''; ?></p>
                    <textarea class="input" rows="5" name="quest[<?php echo $allqust['id']; ?>]" pattern="[a-z A-Z 0-9]{1,}" id="comment" placeholder="" <?php echo ($allqust['is_required'] == 1) ? 'required' : ''; ?>><?php echo (isset($allqust['answer']) && $allqust['answer'] != 'null' && $allqust['answer'] != '') ? $allqust['answer'] : ''; ?></textarea>
                    <div class="line-box">
                        <div class="line"></div>
                    </div>
                    <p class="discrption">
                        <?php echo (isset($allqust['question_placeholder']) && $allqust['question_placeholder'] != '') ? $allqust['question_placeholder'] : ''; ?></p>
                <div class="error_msg"></div>
                </label>
            </div>
        <?php } elseif ($allqust['question_type'] == 'radio') {
            $rad_options = explode(',', $allqust['question_options']);
            ?>
            <label class="form-group">
            <div class="color-inside catagry-heading">
                <h3><?php echo $allqust['question_label']; ?><?php echo ($allqust['is_required'] == 1) ? '*' : ''; ?>
                </h3>
            </div>
            <div class="sound-signal">
                <?php
                $ans = explode(',', $allqust['answer']);
                foreach ($rad_options as $ok => $ov) {
                    ?>
                    <div class="form-radion radio_qust">
                        <input type="radio" name="quest[<?php echo $allqust['id']; ?>]" class="preference" id="soundsignalredio_<?php echo $ov; ?>" value="<?php echo $ov; ?>" <?php echo (in_array($ov, $ans)) ? 'checked' : ''; ?>>
                        <label for="soundsignalredio_<?php echo $ov; ?>"><?php echo $ov; ?></label>
                    </div>
                <?php } ?>
            </div>
                <div class="error_msg"></div>
            </label>
            <?php
        } elseif ($allqust['question_type'] == 'selectbox') {
            $selected_options = explode(',', $allqust['question_options']);
            ?>
            <label class="form-group selreq">
                <p class="label-txt <?php echo (isset($allqust['answer']) && $allqust['answer'] != '') ? 'label-active' : ''; ?>"><?php echo $allqust['question_label']; ?><?php echo ($allqust['is_required'] == 1) ? '*' : ''; ?></p>
                <select name="quest[<?php echo $allqust['id']; ?>]" class="input select-a" <?php echo ($allqust['is_required'] == 1) ? 'required' : ''; ?>>
                    <option value=""></option>
                    <?php
                    $ans = explode(',', $allqust['answer']);
                    foreach ($selected_options as $so_k => $so_v) {
                        ?>
                        <option value="<?php echo $so_v; ?>" <?php echo (in_array($so_v, $ans)) ? 'selected' : ''; ?>><?php echo $so_v; ?></option>
                    <?php } ?>
                </select>
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
            <?php
        } else {
            $checkbox_options = explode(',', $allqust['question_options']);
            ?>
            <label class="form-group chckrq_req">
            <div class="color-inside catagry-heading">
                <h3><?php echo $allqust['question_label']; ?><?php echo ($allqust['is_required'] == 1) ? '*' : ''; ?>
                </h3>
            </div>
            <?php
            $ans = explode(',', $allqust['answer']);
            foreach ($checkbox_options as $ck => $cv) {
                ?>
                <div class="form-radion2 chckrq">
                    <label for="soundsignal_<?php echo $cv; ?>" class="containerr"><?php echo $cv; ?>
                        <input type="checkbox" name="quest[<?php echo $allqust['id']; ?>][]" class="preference" id="soundsignal_<?php echo $cv; ?>" value="<?php echo $cv; ?>" <?php echo (in_array($cv, $ans)) ? 'checked' : ''; ?>>
                        <span class="checkmark"></span>
                    </label>
                </div>
                <?php
            }
        }
    //}
//}
}?> <div class="error_msg"></div>
    </label>