<style>
    .dropify-wrapper{ display:none; height: 200px !important; }
    /*the container must be positioned relative:*/
    .custom-select {
        position: relative;
        display: inline-block;
        width: 100%;
        height: auto;
        padding: 0;
        line-height: 34px;
        color: #495057;
        vertical-align: middle;
        background-size: 8px 10px;
        border: 1px solid #d7d7d7;
        border-radius: 8px;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        max-width: 300px;
        border: 1px solid #ced4da;
    }
    .custom-select select {
        display: none; /*hide original SELECT element:*/
    }
    .custom-select .select-selected {
        width: 100%;
        padding: 9px 14px;
        background: #fff url(<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/sign-nep.png) right center no-repeat;
        background-repeat: no-repeat;
        background-position:95% 50%;
        border-radius: 8px;
        font-size: 15px;
        color: #0f3148;
        font-weight: 500;

    }

    /*style items (options):*/
    .select-items {
        position: absolute;
        background-color:#fff;
        top: 100%;
        left: 0;
        right: 0;border: 1px solid #ddd;
        z-index: 99;
    }
    .custom-select .select-items div {
        padding:4px 23px;    font-size: 12px;
        font-weight: 600;
    }
    /*hide the items when the select box is closed:*/
    .select-hide {
        display: none;
    }
    .select-items div:hover, .same-as-selected {
        background-color: rgb(232, 46, 77);
        color: #fff;
    }
    .colorpicker.dropdown-menu .colorpicker-alpha {
        display: none !important;
    }

    .colorpicker.dropdown-menu 
    .colorpicker-saturation {
        width: 190px !important;    background-size: cover;
    }
</style> 
<link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/style-web.css">
<link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/custom.css">
<link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>plugins/fancybox/jquery.fancybox.css"/>
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/chosen.css" rel="stylesheet">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/chosen.jquery.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/init.js"></script>
<!--<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>plugins/owl/jquery.min.js"></script>-->
<?php 
//$CI = & get_instance();
//$CI->load->library('myfunctions');
if ($agency_info[0]['domain_name'] != "") {
    if ($agency_info[0]['ssl_or_not'] == "0") {
        $url = "http://" . $agency_info[0]['domain_name'];
    } else {
        $url = "https://" . $agency_info[0]['domain_name'];
    }
    $info = '<a href="' . $url . '" target="_blank" class="redrct_lnk" data-toggle="tooltip" title="' . $url . '"><i class="fas fa-external-link-alt"></i></a>';
} else {
    $info = "";
}
$profilepicture = (isset($login_user_data[0]['profile_picture']) && $login_user_data[0]['profile_picture'] != '') ? FS_PATH_PUBLIC_UPLOADS_PROFILE.'_thumb/'.$login_user_data[0]['profile_picture'] : '';
if($profilepicture == ""){
    $profile_css = 'style="display: none;"';
    $main_img = 'style="display: flex;"';
}else{
    $profile_css = "";
    $main_img = 'style="display: none;"';
}
$brand_logo = (isset($agency_info[0]['logo']) && $agency_info[0]['logo'] != '') ? FS_PATH_PUBLIC_UPLOADS_USER_LOGO.$login_user_data[0]['id'].'/'.$agency_info[0]['logo'] : '';
if($brand_logo == ""){
    $brnd_css = 'style="display: none;"';
    $brndmain_img = 'style="display: block;"';
}else{
    $brnd_css = "";
    $brndmain_img = 'style="display: none;"';
}

$design_type = explode(",",$login_user_data[0]["design_type"]);
$business = array("Marketing Agency","Print Shop","E-commerce","Retailer","Professional Services","Real Estate","Merch Seller","Design Agency");
$other_company = array("CEO/Owner","VP","Marketing Director","Manager"); 

$permission = array(
    "1" => array(
        "slug"=>"add_requests",
        "name" => "Add Request",
        "info" => "User can add/edit projects."),
    "2" => array(
        "slug"=>"add_brand_pro",
        "name" => "Add/Edit Brand Profile",
        "info" => "User can add/edit brand profiles."),
    "3" => array(
        "slug"=>"billing_module",
        "name" => "Billing Module",
        "info" => "User can manage billing modules."),
    "4" => array(
        "slug"=>"file_management",
        "name" => "File Management",
        "info" => "User can create new folder and copy/upload any files here in it using file management feature."),
    "5" => array(
        "slug"=>"white_label",
        "name" => "White Label",
        "info" => "User can manage branding setting."),
    "6" => array(
        "slug"=>"app_requests",
        "name" => "Approve/Revise",
        "info" => "User can approve or reject projects."),
    "7" => array(
        "slug"=>"comnt_requests",
        "name" => "Comment on Request",
        "info" => "User can comment on projects."),
    "8" => array(
        "slug"=>"del_requests",
        "name" => "Delete Request",
        "info" => "User can delete projects."),
    "9" => array(
        "slug"=>"downld_requests",
        "name" => "Download File",
        "info" => "User can download designs."),
    "10" => array(
        "slug"=>"manage_priorities",
        "name" => "Manage Priorities",
        "info" => "User can manage priorities."),);
        ?>
        <section class="welcome_wizard">

            <form id="subdomainwiz" action="javascript:void(0)" method="post" enctype="multipart/form-data" data-isagency="<?php echo $is_agency;?>">
                <div class="container">
                    <a class="back_wiz_step save_next back" id="back" style="display:none;">
                        <i class="fas fa-arrow-left" data-toggle="tooltip" title="Back"></i>back
                    </a>
<!--            <ul class="welcome_progress">
                <li class="active cstm_primary_colr" id="1_bar"></li>
                <li id="2_bar"></li>
                <li id="3_bar"></li>
                <li id="4_bar"></li>
                <li id="5_bar"></li>
                <li id="6_bar"></li>
                <li id="7_bar"></li>
                <li id="8_bar"></li>
            </ul>-->
            <a class="wel_wiz_logo" href="<?php echo base_url(); ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>front_end/Updated_Design/img/logo.svg"></a>

            <fieldset class="gen_sett active" id="1_field" data-chck="1" style="display: block;">
               <h1 class="welcome_to_head"> Welcome to GraphicsZoo </h1>
               <div class="welcome_header text-center">
                <h2 class="welcome_heading">Letʼs get your account set up</h2>
            </div>

            <div class="val_err"></div>
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <input type="hidden" name="step" value="1">
                    <div class="welcome_step_ouetr">
                        <div class="col-md-12 form-group company_name_input">
                            <label>Company Name*</label> 
                            <input class="form-control entr_compny_name" tabindex="1" autofocus type="text" pattern="[a-z A-Z 0-9]{1,}" minlength="2" maxlength="55" name="company_name" placeholder="Enter your company name" value="<?php echo isset($login_user_data[0]["company_name"])?$login_user_data[0]["company_name"]:""; ?>">
                            <em class="welcome_sub_info"><strong>Ex</strong> (google,apple)</em>
                        </div>
                        <div class="col-md-12 form-group text-center upload_profile_pic">
                            <input type="file" onChange="validateAndUpload(this,'','welcome');" tabindex="2"  accept="image/*" class="form-control dropify" name="profile" data-plugin="dropify" id="onboardingpic" style="display: none" hidden/>
                            <div class="welcome_profile_pic">
                             <a class="setimg-box" href="javascript:void(0)"  onclick="$('.dropify').click();" for="profile">
                                <div class="custom_pic_file imagemain" <?php echo $main_img; ?>>
                                    <span>Add Your <br> Photo</span>
                                    <i class="fas fa-plus"></i>
                                </div>
                                <div class="imagemain3" <?php echo $profile_css; ?>>
                                    <figure class="setimg-box33">
                                        <img id="image3" src="<?php echo $profilepicture; ?>" class="img-responsive telset33">
                                        <span class="change_profile_image">
                                            Change <br>Logo
                                        </span>
                                    </figure>
                                </div>
                            </a>
                        </div>
                        <div class="validation_err"></div>
                    </div>
                        <div class="row">
                    <div class="col-md-6 form-group phone_setting">
                        <label>Phone Number</label>
                        <input type="text" pattern="\d*" tabindex="3" oninvalid="this.setCustomValidity('Please enter valid phone number ex: 802256674')" oninput="this.setCustomValidity('')" maxlength="10" minlength="10" placeholder="8002256674" name="phone" class="form-control" value="<?php echo isset($login_user_data[0]["phone"])?$login_user_data[0]["phone"]:""; ?>">
                    </div>
                    <div class="col-md-6 form-group web_setting">
                        <label>Website URL</label>
                        <input type="text" tabindex="4" oninvalid="this.setCustomValidity('Please enter valid website url. Ex: www.graphicszoo.com')" oninput="this.setCustomValidity('')" placeholder="Ex: www.graphicszoo.com" maxlength="100" name="website" pattern="[A-Z a-z 0-9 -._:/]+\.[A-Z a-z]{2,}$" class="form-control" value="<?php echo isset($login_user_data[0]["url"])?$login_user_data[0]["url"]:""; ?>">
                    </div>
                        </div>
                         <div class="row">
                    <div class="col-md-6 form-group describe-sec des_business_detail">
                        <label>Which best describes your business?*</label>
                        <div class="custom-select">
                   
                           
                            <select name="business_detail" class="form-control" id="des_business_detail">
                                <option value="">Please select</option>
                                <option value="Marketing Agency" <?php echo ($login_user_data[0]["business_detail"] == "Marketing Agency") ? 'selected' : '' ?>>Marketing Agency</option>
                                <option value="Print Shop" <?php echo ($login_user_data[0]["business_detail"] == "Print Shop") ? 'selected' : '' ?>>Print Shop</option>
                                <option value="E-commerce" <?php echo ($login_user_data[0]["business_detail"] == "E-commerce") ? 'selected' : '' ?>>E-commerce  </option>
                                <option value="Retailer" <?php echo ($login_user_data[0]["business_detail"] == "Retailer") ? 'selected' : '' ?>>Retailer  </option>
                                <option value="Professional Services" <?php echo ($login_user_data[0]["business_detail"] == "Professional Services") ? 'selected' : '' ?>>Professional Services  </option>
                                <option value="Real Estate" <?php echo ($login_user_data[0]["business_detail"] == "Real Estate") ? 'selected' : '' ?>>Real Estate </option>
                                <option value="Merch Seller" <?php echo ($login_user_data[0]["business_detail"] == "Merch Seller") ? 'selected' : '' ?>>Merch Seller </option>
                                <option value="Design Agency" <?php echo ($login_user_data[0]["business_detail"] == "Design Agency") ? 'selected' : '' ?>>Design Agency </option>
                                <option value="Other" <?php echo (!in_array($login_user_data[0]["business_detail"], $business) && $login_user_data[0]["business_detail"] != "") ? 'selected' : '' ?>>Other</option>
                            </select> <div class="other_business" <?php echo (!in_array($login_user_data[0]["business_detail"], $business) && $login_user_data[0]["business_detail"] != "") ? 'style="display:block"' : 'style="display:none"' ?>>
                                <input type="text" tabindex="4" oninvalid="this.setCustomValidity('Please describes your business')" oninput="this.setCustomValidity('')" placeholder="Describes your business" maxlength="100" name="industry" pattern="[A-Z a-z 0-9 ]+[A-Z a-z]{2,}$" class="form-control" value="<?php echo (!in_array($login_user_data[0]["business_detail"], $business))?$login_user_data[0]["business_detail"]:""; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-group describe-sec company_describe">
                        <label> What is your role at the company?* </label>

                        <div class="custom-select">
                  <select name="company_role" class="form-control">
                                <option value="">Please select</option>
                                <option value="CEO/Owner" <?php echo ($login_user_data[0]["company_role"] == "CEO/Owner") ? 'selected' : '' ?>>CEO/Owner  </option>
                                <option value="VP" <?php echo ($login_user_data[0]["company_role"] == "VP") ? 'selected' : '' ?>>VP</option>
                                <option value="Marketing Director" <?php echo ($login_user_data[0]["company_role"] == "Marketing Director") ? 'selected' : '' ?>>Marketing Director </option>
                                <option value="Manager" <?php echo ($login_user_data[0]["company_role"] == "Manager") ? 'selected' : '' ?>>Manager</option>
                                <option value="Other" <?php echo (!in_array($login_user_data[0]["company_role"], $other_company) && $login_user_data[0]["company_role"] != "") ? 'selected' : '' ?>>Other</option>

                            </select>
                            <div class="other_company" <?php echo (!in_array($login_user_data[0]["company_role"], $other_company) && $login_user_data[0]["company_role"] != "") ? 'style="display:block"' : 'style="display:none"' ?>>
                                <input type="text" tabindex="4" oninvalid="this.setCustomValidity('Enter your company role')" oninput="this.setCustomValidity('')" placeholder="Enter your company role" maxlength="100" name="other_company" pattern="[A-Z a-z 0-9 ]+[A-Z a-z]{2,}$" class="form-control" value="<?php echo (!in_array($login_user_data[0]["company_role"], $other_company))?$login_user_data[0]["company_role"]:""; ?>">
                            </div>
                        
                        </div>
                    </div>
                         </div>
                        <div class="row">
                    <div class="col-md-6 form-group describe-sec top_select">
                        <label>  How many designs do you need  per month?*</label>
                        <div class="custom-select">
                            <select name="design_count" class="form-control">
                                <option value="">Please select</option>
                                <option value="Less than 10" <?php echo ($login_user_data[0]["design_count"] == "Less than 10") ? 'selected' : '' ?>>Less than 10</option>
                                <option value="10-20" <?php echo ($login_user_data[0]["design_count"] == "10-20") ? 'selected' : '' ?>>10-20 </option>
                                <option value="20-30" <?php echo ($login_user_data[0]["design_count"] == "20-30") ? 'selected' : '' ?>>20-30</option>
                                <option value="30-50" <?php echo ($login_user_data[0]["design_count"] == "30-50") ? 'selected' : '' ?>>30-50 </option>
                                <option value="More than 50" <?php echo ($login_user_data[0]["design_count"] == "More than 50") ? 'selected' : '' ?>>More than 50</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 form-group top_select">
                        <label> What kind of designs do you usually need?*  <p class="red-info">(you can select more than one)</p></label>
                        <select name="design_type[]" id="design_type"  class="chosen-select" data-live-search="true" multiple>
                            <option name="design_type[]" value="Logo & Branding" class="form-control" <?php echo in_array("Logo & Branding", $design_type) ? 'selected' : '' ?>>Logos & Branding</option>
                            <option name="design_type[]" value="Digital Media" class="form-control" <?php echo in_array("Digital Media", $design_type) ? 'selected' : '' ?>>Digital Media</option>
                            <option name="design_type[]" value="Print Material" class="form-control" <?php echo in_array("Print Material", $design_type) ? 'selected' : '' ?>>Print Material</option>
                            <option name="design_type[]" value="T-Shirts" class="form-control" <?php echo in_array("T-Shirts", $design_type) ? 'selected' : '' ?>>T-Shirts</option>
                            <option name="design_type[]" value="UI/UX" class="form-control" <?php echo in_array("UI/UX", $design_type) ? 'selected' : '' ?>>UI/UX</option>
                            <option name="design_type[]" value="Vectorize/Digitize" class="form-control" <?php echo in_array("Vectorize/Digitize", $design_type) ? 'selected' : '' ?>>Vectorize/Digitize</option>
<!--                                        <option name="design_type[]" value="Posters & Flyers" class="form-control" <?php echo in_array("Posters & Flyers", $design_type) ? 'selected' : '' ?>>Posters & Flyers</option>
    <option name="design_type[]" value="Marketing Ads" class="form-control" <?php echo in_array("Marketing Ads", $design_type) ? 'selected' : '' ?>>Marketing Ads</option>-->
    <option name="design_type[]" value="Other" class="form-control" <?php echo in_array("Other", $design_type) ? 'selected' : '' ?>>Other</option>
</select> 
</div>
 </div>
                            
</div>
</div>
</div>
</fieldset>
<fieldset class="brand_sett" id="2_field" data-chck="2" style="display: none;">
    <div class="welcome_header text-center">

        <h2 class="welcome_heading">Branding</h2>
        <span>Customize the platform to make it look and feel like your Company</span>
    </div>
    <div class="val_err"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="welcome_step_ouetr">    
                <div class="welcome_branding">
                    <div class="col-md-4">
                        <input type="hidden" name="step" value="2">
                        <div class="left_branding_bar">
                            <div class="form-group">
                                <label>Upload your logo:</label>
                                <input type="file" onChange="validateAndUpload(this,'brand_logo','welcome');"  accept="image/*" class="form-control dropify_brand" name="brand_logo" data-plugin="dropify" id="brand_logo" style="display: none" hidden/>
                                <div class="upload_brand_logo">
                                    <a class="setimg-box" href="javascript:void(0)"  onclick="$('.dropify_brand').click();" for="brand_logo">
                                        <div class="brand_logo_icon" <?php echo $brndmain_img; ?>>
                                            <i class="fas fa-plus"></i>
                                            <span>Drop your file here </span>
                                        </div>
                                        <div class="uploaded_brnd_fl" <?php echo $brnd_css; ?>>
                                            <figure class="setimg-box33">
                                                <img id="brand_logoimg" src="<?php echo $brand_logo; ?>" class="img-responsive telset33">
                                                <span class="setimgblogcaps">Change <br>Logo
                                                </span>
                                            </figure>
                                        </div>
                                    </a>
                                </div>
                                <div class="validation_err"></div>
                            </div><!--form-group -->
                            <input type="hidden" name="confirmed_arecord" id="confirmed_arecord" class="confirmed_arecord" value="1"/>
                            <div class="form-group choose_domain">
<!--                                <div class="domain_subdomain">
                                    <div class="form-group-b">
                                        <p class="toggle-text">Customize domain: <span class="domain_info" data-toggle="modal" data-target="#setmaindomain" id="domain_info_pop"><i class="fas fa-info-circle" data-toggle="tooltip" title="Domain Info"></i></span>
                                            <label class="form-group switch-custom-usersetting-check">
                                                <input type="checkbox" name="slct_domain_subdomain" class="slct_domain_subdomain" id="slct_domain_subdomain" <?php echo (isset($agency_info[0]['is_main_domain']) && $agency_info[0]['is_main_domain'] == "1") ? "checked" : "" ?>/>
                                                <label for="slct_domain_subdomain"></label> 
                                            </label>
                                        </p>
                                    </div>
                                </div>-->
                                <div class="alternate-domain">

                                    <div class="subdomain_nm"> 

                                        <label class="form-group">
                                            <p class="domain_https">https://</p>
                                            <div class="label-txt label-active clint_cll">Sub Domain Name*: <?php echo $info; ?></div>
                                            <input type="text" class="form-control domain_url" name="sub_domain_name" minlength="1"  maxlength="30" oninvalid="this.setCustomValidity('Please enter valid sub domain name. Ex: xyz.graphicszoo.com')" oninput="this.setCustomValidity('')" pattern="[a-z A-Z 0-9]+[a-z 0-9]{1,}" id="sub_domain_name" value="<?php echo (isset($agency_info[0]['domain_name']) && $agency_info[0]['is_main_domain'] == "0") ? strstr($agency_info[0]['domain_name'], '.', true) : '' ?>">
                                            <p class="domain_ex">.graphicszoo.com</p> 
                                        </label>
                                        <div class="validation"></div>
                                    </div>
<!--                                    <div class="domain_nm" style="display:none"> 
                                        <p class="domain_https"><?php echo (isset($agency_info[0]['ssl_or_not']) && $agency_info[0]['ssl_or_not'] == 1) ? "https://" : "http://" ?></p>
                                        <label class="form-group">
                                            <div class="label-txt label-active clint_cll">Domain Name*: <?php echo $info; ?></div>
                                            <input type="text" class="form-control domain_url" name="domain_name" id="domain_name" oninvalid="this.setCustomValidity('Please enter valid domain url like: graphicszoo.com')" oninput="this.setCustomValidity('')" minlength="1"  maxlength="30" pattern="[A-Z a-z 0-9]+\.[A-Z a-z]{2,}$"  value="<?php echo (isset($agency_info[0]['domain_name']) && $agency_info[0]['is_main_domain'] == "1") ? $agency_info[0]['domain_name'] : '' ?>">
                                        </label>
                                        <div class="validation"></div>
                                    </div>-->

                                </div>
                            </div><!--form-group -->

                            <div class="form-group brand-color">
                                <label>Primary Brand Color</label>
                                <input type="text" id="d_primary_color" class="form-control sel_colorpicker" minlength="4"  maxlength="20" name="primary_color" value="<?php echo (isset($agency_info[0]['primary_color']) && $agency_info[0]['primary_color'] != '') ? $agency_info[0]['primary_color'] : '#e42547' ?>">
                                <label class="errspan" for="d_primary_color"><span class="bgcolor_primary cstm_primary_colr" style="background-color:<?php echo $themePrimaryColor; ?>;"></span></label>
                            </div><!--form-group -->
                            <div class="form-group brand-color">
                                <label>secondary Brand Color</label>
                                <input type="text" id="d_secondary_color" class="form-control sel_colorpicker" minlength="4"  maxlength="20" name="secondary_color"  value="<?php echo (isset($agency_info[0]['secondary_color']) && $agency_info[0]['secondary_color'] != '') ? $agency_info[0]['secondary_color'] : '#1a3148' ?>">
                                <label class="errspan" for="d_secondary_color"><span class="bgcolor_secondary cstm_sec_btn" style="background-color:<?php echo $secondary_color; ?>;"></span></label>
                            </div><!--form-group -->
                            <!--                                        <div class="up-later-text">You have the option to change or update later.</div>-->
                        </div>
                    </div><!--col-md-4 -->
                    <div class="col-md-8">
                        <div class="welcome_screen">
                            <div class="screen_outer">
                                <div class="screen_head">
                                    <div class="col-md-3">
                                        <div class="window_left_head">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/window-dot.svg" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="window_url">
                                            <div class="url_txt"><?php echo ($url != "")?$url:"https://graphicszoo.com"; ?></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 hidden-xs">
                                        <div class="window_right_head text-right">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/window_tabs.svg" class="img-responsive">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="browser_out">
                                        <div class="web_header">
                                            <div class="col-md-3">
                                                <div class="web_logo">
                                                    <img src="<?php echo ($brand_logo != "")?$brand_logo:FS_PATH_PUBLIC_ASSETS."front_end/Updated_Design/img/logo.svg"; ?>" class="img-responsive">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="web_nav">
                                                    <ul>
                                                        <li class="cstm_primary_colr"></li>
                                                        <li class="cstm_primary_colr"></li>
                                                        <li class="cstm_primary_colr"></li>
                                                        <li class="cstm_primary_colr"></li>
                                                        <li class="cstm_primary_colr"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="web_user">
                                                    <span><?php echo $login_user_data[0]["first_name"]; ?>
                                                    <small>Customer</small>
                                                </span>
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/web_user.svg" class="img-responsive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="web_page">
                                        <div class="web_top_bar">
                                            <div class="col-md-6 text-left">
                                                <h2 class="agecy_cmpny_name"><?php echo (isset($login_user_data[0]["company_name"]) && $login_user_data[0]["company_name"] != "")?$login_user_data[0]["company_name"]:"Company Name"; ?></h2>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button class="cstm_primary_btn cstm_primary_colr" type="button"><span></span></button>
                                                <button class="cstm_sec_btn" type="button"><span></span></button>
                                            </div>
                                        </div>
                                        <div class="web_project_list">
                                            <div class="col-xs-6 col-sm-3 col-md-3">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/web_add_new.jpg" class="img-responsive">
                                            </div>
                                            <?php for($i=0; $i<=6; $i++){ ?>
                                                <div class="col-xs-6 col-sm-3 col-md-3">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/web_project_img.jpg" class="img-responsive">
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="text-center">
                                            <button class="cstm_primary_btn cstm_primary_colr" type="button"><span></span></button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--col-md-8 -->
            </div>
        </div>
    </div>
</div>
</fieldset>
<fieldset class="brand_profile" id="3_field" data-chck="3" style="display: none;">
    <div class="welcome_header text-center">
      <h2 class="welcome_heading"> Add New Brand Profile</h2> <span>Please fill out the information below to create a brand profile</span>
  </div>
  <div class="val_err"></div>
  <input type="hidden" name="step" value="3">
  <input type="hidden" name="brand_id" value="" id="brand_id">
  <div class="want_brand">
  <button type="button" class="want_create" id="want_create" data-flag="0" style="display: inline-block;"><i class="fas fa-check"></i> Do you want to create brand profile?</button>
<!--  <button type="button" class="skip_without_save" id="skip_without_save12" style="display: inline-block;">Skip >></button>-->
  </div>
  <div class="welcome_brand_outline" style="display:none">
      <div class="col-md-offset-2 col-md-8">
          <div class="welcome_step_ouetr">
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Brand Name <span>*</span></label>
                          <input placeholder="Example: graphicszoo" type="text" class="form-control" name="brand_name" oninvalid="this.setCustomValidity('Please enter valid brand name. Ex: graphicszoo')" oninput="this.setCustomValidity('')" pattern="[A-Z a-z 0-9 -_]+[A-Z a-z 0-9]{2,}$" id="exampleInputName" aria-describedby="NameHelp" value="">
                          <div class="error_field"></div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Website URL</label>
                          <input placeholder="https://www.graphicszoo.com" type="text" class="form-control" oninvalid="this.setCustomValidity('Please enter valid website url. Ex: www.graphicszoo.com')" oninput="this.setCustomValidity('')" placeholder="Ex: www.graphicszoo.com" maxlength="100" name="website" pattern="[A-Z a-z 0-9 -._:/@]+\.[A-Z a-z]{2,}$" name="website_url" id="exampleInputwebsite_url" value="">
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Fonts</label>
                          <input placeholder="Arial, times new roman, open sans" type="text" oninvalid="this.setCustomValidity('Please enter valid font. Ex: Arial,times new roman')" oninput="this.setCustomValidity('')" pattern="[A-Z a-z 0-9]{2,}$" class="form-control" name="fonts" id="web_font" value="">
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Color Preferences</label>
                          <select name="color_preference" class="form-control">
                              <option value="No Preference">No Preference</option>
                              <option value="CMYK Files">CMYK Files</option>
                              <option value="RGB Files">RGB Files</option>
                          </select>

                      </div>
                  </div>
                  <div class="des_brand_sec" style="display:none">
                      <div class="col-md-6">
                          <div class="form-group brand_description">
                              <label>Description <span>*</span> <i class="fa fa-info-circle" aria-hidden="true"  data-toggle="tooltip" title="" data-original-title="Please provide us as much detail as possible. Include things such as color preferences, styles you like, text you want written on the graphic, and anything else that may help the designer."></i></label>
                              <textarea class="form-control" rows="5" name="description" id="comment" pattern="[A-Z a-z 0-9 ]+[A-Z a-z 0-9]{2,}$" oninvalid="this.setCustomValidity('Please enter valid description')" oninput="this.setCustomValidity('')"></textarea>
                              <div class="error_field"></div>
                          </div>
                      </div>
                      <div class="col-md-6 additional_ref_links">
                          <div class="form-group">
                              <label>additional reference link</label>
                              <div class="added_brand_item">
                                  <input type="text" name="google_link[]" class="form-control google_link f_cls_nm" oninvalid="this.setCustomValidity('Please enter valid reference link')" oninput="this.setCustomValidity('')" pattern="[A-Z a-z 0-9 ]+[A-Z a-z 0-9]{2,}$">
                              </div>
                              <div class="added_brand_item">
                                  <input type="text" name="google_link[]" class="form-control google_link f_cls_nm" oninvalid="this.setCustomValidity('Please enter valid reference link')" oninput="this.setCustomValidity('')" pattern="[A-Z a-z 0-9 ]+[A-Z a-z 0-9]{2,}$">
                                  <a class="add_more_reference"><i class="icon-gz_plus_icon"></i></a>
                              </div>
                              <div class="reference_field"></div>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
          <div class="upload_for_brand" style="display:none">
              <div class="col-md-4">
                  <label for="exampleInputlogo">Brand Logo</label>
                  <div class="upload-card">
                      <div class="file-drop-area file-upload">
                          <span class="fake-img">
                              <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/upload_brand_icon.svg" class="img-responsive">
                          </span>
                          
                          <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="logo_upload[]" id="logo_file_input">
                      </div>
                  </div>
                  <div class="uploadFileListContainer row logo_uploadFileListContain">                      
                  </div>
                  <div class="uploadFileListContainer row">   
                  </div>
              </div>
              <div class="col-md-4">
                  <label for="exampleInputlogo">Marketing Materials</label>
                  <div class="upload-card">

                      <div class="file-drop-area file-upload">
                          <span class="fake-img">
                              <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/upload_brand_icon.svg" class="img-responsive">
                          </span>
<!--                          <span class="file-msg">Select files to upload or Drag and drop file</span>-->
                          <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="materials_upload[]" id="materials_file_input">
                      </div>
                  </div>
                  <div class="uploadFileListContainer row materials_uploadFileListContain">                      
                  </div>
                  <div class="uploadFileListContainer row">   
                  </div>

              </div>
              <div class="col-md-4">
                  <label for="exampleInputlogo">Additional Images </label>
                  <div class="upload-card">

                      <div class="file-drop-area file-upload">
                          <span class="fake-img">
                              <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/upload_brand_icon.svg" class="img-responsive">
                          </span>
                          
                          <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="additional_upload[]" id="additional_file_input">
                      </div>
                  </div>
                  <div class="uploadFileListContainer row additional_uploadFileListContain">                      
                  </div>
                  <div class="uploadFileListContainer row">   
                  </div>
              </div>
          </div>
      </div>
  </div>
</fieldset>
<fieldset class="team_sett" id="4_field" data-chck="4" style="display: none;">
    <div class="welcome_header text-center">
        <h2 class="welcome_heading">Add Team Members</h2>
        <span>Register your team members now or do it later. Itʼs free!</span>
    </div>
    <div class="val_err"></div>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <input type="hidden" name="step" value="4">
            <div class="welcome_step_ouetr">
<!--                            <div class="com_web_heading">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/add_user.png" class="img-responsive">
                                <h3 class="wel_sub_heading">Add New User
                                    <em>Complete the form below to add new user.</em>
                                </h3>
                            </div>-->

                            <div class="add_user_table">
                                <div class="adduser_item">
                                    <div class="adduser_membrr">
                                        <div class="not_allow"></div>
                                        <div class="user_field_row">
                                            <input type="hidden" name="invalid_email" class="invalid_email" value="0">
                                            <input type="hidden" name="user_role" class="user_role" value="manager">
<!--                                        <div class="form-group user_fields">
                                            <label>Select The User Role</label>
                                            <select class="form-control member_role" name="user_role[]">
                                                <option value="manager">manager</option>
                                            <?php //if($parent_user_plan[0]["is_agency"] == 1){ ?>
                                                <option value="customer">client</option>
                                            <?php //} ?>
                                            </select>
                                        </div>-->
                                        <div class="form-group user_fields">
                                            <label>First Name*</label>
                                            <input class="form-control" type="text" name="first_name[]" oninvalid="this.setCustomValidity('Please enter valid first name')" oninput="this.setCustomValidity('')" pattern="[a-z A-Z 0-9]{1,}" maxlength="30" placeholder="Enter the first name">
                                        </div>
                                        <div class="form-group user_fields">
                                            <label>Last Name*</label>
                                            <input class="form-control" type="text" name="last_name[]" oninvalid="this.setCustomValidity('Please enter valid last name')" oninput="this.setCustomValidity('')" pattern="[a-z A-Z 0-9]{1,}" maxlength="30" placeholder="Enter the last name">
                                        </div>
                                        <div class="form-group user_fields">
                                            <label>Email*</label>
                                            <input class="form-control chk_usr_mail" type="email" name="email[]" oninvalid="this.setCustomValidity('Please enter valid email. Ex: hello@example.com')" oninput="this.setCustomValidity('')" pattern="[a-z 0-9 -_.]+[a-z 0-9]+@[a-z 0-9]+\.[a-z]{2,}$" maxlength="30" placeholder="Enter the email">
                                            <div class="validation_err"></div>
                                        </div>
<!--                                        <div class="form-group user_fields">
                                            <label>Password*<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="User will receive an email with further instructions"></i></label> 
                                            <div class="gen_pwd"><span>Randomly generated password</span>
                                                <div class="switch-custom-usersetting-check">
                                                    <input type="checkbox" name="genrate_password[]" id="password_gen" class="password_ques user_prms_switch" checked="" disabled="">
                                                    <label for="password_gen">
                                                    </label>
                                                    <input type="hidden" value="0" name="genrate_password[]">
                                                </div>
                                                <p>User will receive an email with further instructions</p>
                                                <div class="form-group create_password" style="display:none">
                                                <label class="form-group">
                                                    <input type="password" name="password[]" class="form-control password-val" value="" minlength="8" maxlength="16" placeholder="Enter the password"/>
                                                    <div class="change_pss_mode"><i class="fa fa-eye" aria-hidden="true"></i></div>
                                                    <div class="validation_err"></div>
                                                </label>
                                            </div>
                                            </div>
                                            
                                        </div>-->
                                    </div>
                                        
                                    
                                    <div class="users_permission">
                                        <div class="password_note">
                                            <p><strong>Note: </strong>An email will be sent to this address with login instructions.</p>
                                            </div>
                                        <div class="com_web_heading form-group">
<!--                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/permission_ico.png" class="img-responsive">-->
                                            <label class="wel_sub_heading">Permissions</label>
                                            <div class="switch-custom-usersetting-check view_membr_prmsn_only">
                                                <input type="checkbox" name="view_only[]" id="view_only" class="view_only user_prms_switch" checked="">
                                                <label for="view_only"></label><span>View Only <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Switch toggle off to customize permissions"></i></span>
                                            </div>
                                        </div>
                                        <div class="permission_list" style="display:none">
                                            <ul>
                                                <?php foreach ($permission as $k => $per){  ?>
                                                    <li>
                                                        <div class="permission-checked">
                                                            <input type="checkbox" name="<?php echo $per["slug"].'[]';?>" id="<?php echo $per["slug"];?>" class="user_prms_switch">
                                                            <label  for="<?php echo $per["slug"];?>"><?php echo $per["name"]; ?><i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-original-title="<?php echo $per["info"]; ?>"></i>
                                                             <span class="checked_mark"></span>
                                                         </label>
                                                         <input type='hidden' value='0' name="<?php echo $per["slug"].'[]';?>">
                                                     </div>
                                                 </li>
                                             <?php } ?>
                                         </ul>
                                            <div class="row">
                                                <div class="col-md-12">
                                                <div class="white-boundries brand_selections">
                                                    <div class="access-brand com_web_heading form-group">
<!--                                                         <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/permission_ico.png" class="img-responsive">-->
                                                        <label>Brands Selection</label>
                                                        <div class="notify-lines finish-line">
                                                            <label class="switch-custom-usersetting-check">
                                                                <div class="switch-custom-usersetting-check">
                                                                    <input type="checkbox" name="access_brand_pro" class="switch_brand_access" id="switch_brand_access" checked/>
                                                                    <label for="switch_brand_access"></label> 
                                                                </div>
                                                            </label>
                                                            <h3>Access All brands</h3>
                                                        </div>
                                                    </div>
                                                    <div class="sel_brandshowing form-group" style="display:none">
                                                        <label>Select Brands <span>*</span></label>
                                                        <select name="brandids[0][]" style='display:none'  class="chosen-select sel_brandcheck" data-live-search="true" multiple>
                                                            <?php ?>
                                                            <option value="">Select Brand</option>
                                                            <?php
                                                           // foreach ($brandprofile as $brand) {
                                                                ?> 
<!--                                                                <option value="<?php //echo $brand['id'] ?>" <?php //echo in_array($brand['id'], $selectedbrandIDs) ? 'selected' : '' ?>><?php //echo $brand['brand_name'] ?></option>-->
                                                            <?php //} ?>
                                                        </select>                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                     </div>
                                 </div>
<!--                                    <div class="client_subscription form-group" style="display:none">
                                        <label>
                                                Subscription Plan
                                        </label>
                                            <select class="form-control clintsubscription" name="requests_type[]">
                                                <option value="">Select Subscription Plan</option>
                                            </select>
                                            <p class="note_text fill-sub"><span>Please choose subscription plan for this user. Manage your plans in subscription module.</span></p>
                                            
                                        </div>-->
                                    </div>
                                </div>
                                <div class="input_team_container"></div>
                                <button class="add_member_btn add_more_member"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/add_user.png" class="img-responsive"> Add New Member</button>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
<fieldset class="email_sett" id="5_field" data-chck="5" style="display: none;">
    <div class="welcome_header text-center">
        <h2 class="welcome_heading">Email Settings</h2>
        <span>Make sure all email communication to your clients look like its coming from your brand.</span>
    </div>
    <div class="val_err"></div>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <input type="hidden" name="step" value="5">
            <div class="welcome_step_ouetr">
                <div class="subscription_row">
<!--                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/mail-setting.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Email Settings
                                        <em>Please fill the complete form before sending test email,email settings you might need from your email provider</em>
                                    </h3>
                                </div>-->
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Reply Name</label>
                                        <input class="form-control" type="text" name="reply_name" oninvalid="this.setCustomValidity('Please enter valid reply name')" oninput="this.setCustomValidity('')" pattern="[a-z A-Z 0-9 @/.]+[a-z 0-9]{1,}" maxlength="30" placeholder="Enter the name" value="<?php echo $login_user_data[0]["first_name"]." ".$login_user_data[0]["last_name"]; ?>">
                                        <em>Your name as you would like as reply address</em>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Reply Email*</label>
                                        <input class="form-control" type="email" name="reply_email" oninvalid="this.setCustomValidity('Please enter valid reply email. Ex: hello@example.com')" oninput="this.setCustomValidity('')" pattern="[a-z 0-9-_.]+[a-z 0-9]+@[a-z 0-9]+\.[a-z]{2,}$" maxlength="30" placeholder="Enter the email" value="<?php echo $login_user_data[0]["email"]; ?>">
                                        <em>Email address where users can reply </em>
                                    </div>
                                </div>
                            </div>
                            <div class="subscription_row">
                                <div class="com_web_heading smtp_heading show-hidelabel">
                                    <h3 class="wel_sub_heading">Use your own email address  <div class="switch-custom-usersetting-check is_smtp_enable">
                                        <input type="checkbox" name="is_smtp_enable" id="is_smtp_enable" class="is_smtp">
                                        <label for="is_smtp_enable"></label>
                                    </div>
                                    <em>You may use your own email to send notifications to your users.</em>
                                </h3>
                            </div>
                            <div class='port_settings' style='display:none'>
                                <div class="col-md-12">
                                    <p  class="fill-sub">
<!--                                            You can send your users email through your own SMTP service. For more details about custom SMTP <a class="theme-color" href="https://www.youtube.com/watch?v=5SbC2fCf5qo" target="_blank">check cpanel instruction's</a> OR <a class="theme-color" href="https://in.godaddy.com/help/configuring-mail-clients-with-cpanel-email-8861" target="_blank">godaddy instruction's</a>
    according your hosting. If you're using Gmail for your email services, enable the switch for less secure apps in <a href="https://myaccount.google.com/lesssecureapps" target="_blank">your account</a>.-->
    You can email your users using your own SMTP service. For more details about custom SMTP <a class="theme-color" href="https://www.youtube.com/watch?v=5SbC2fCf5qo" target="_blank">check cpanel instruction's</a> OR <a class="theme-color" href="https://in.godaddy.com/help/configuring-mail-clients-with-cpanel-email-8861" target="_blank">godaddy instruction's</a> according to your hosting. If you're using Gmail for your email services, enable the switch for less secure apps in <a href="https://myaccount.google.com/lesssecureapps" target="_blank">your account</a>.</p>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label>From Name*</label>
        <input class="form-control" name="from_name" type="text" oninvalid="this.setCustomValidity('Please enter valid from name. Ex: John')" oninput="this.setCustomValidity('')" pattern="[a-z A-Z 0-9 @/.]+[a-z 0-9]{1,}" maxlength="30" placeholder="Enter the name">
        <em>Choose your sender name as you would like it to appear in messages that you send. Ex: John</em>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label>From Email*</label>
        <input class="form-control" name="from_email" type="email" oninvalid="this.setCustomValidity('Please enter valid from email. Ex: hello@example.com')" oninput="this.setCustomValidity('')" pattern="[a-z 0-9_-.]+[a-z 0-9]+@[a-z 0-9]+\.[a-z]{2,}$" maxlength="30" placeholder="Enter the email">
        <em>Your email where user want to reply your message.</em>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label>Port*</label>
        <input class="form-control" name="port" oninvalid="this.setCustomValidity('Please enter valid port. Ex: 143,465,587,2525 or 25')" oninput="this.setCustomValidity('')" pattern="[0-9]{1,}" type="text" maxlength="4" placeholder="Enter the port">
        <em> The port number used by the incoming mail server. Ex: 143,465,587,2525 or 25.</em>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label>Host Name*</label>
        <input class="form-control" name="host" oninvalid="this.setCustomValidity('Please enter valid host name. Ex: mail.example.com')" oninput="this.setCustomValidity('')" pattern="[a-z A-Z 0-9/.]+[a-z]{1,}" type="text" maxlength="30" placeholder="Enter the host name">
        <em>The host name of the incoming mail server, such as mail.example.com.</em>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label>Email Address*</label>
        <input class="form-control" name="host_username" oninvalid="this.setCustomValidity('Please enter valid email address. Ex: hello@example.com')" oninput="this.setCustomValidity('')" pattern="[a-z0-9_-.]+[a-z 0-9]+@[a-z 0-9]+\.[a-z]{2,}$" type="email" maxlength="30" placeholder="Enter the email">
        <em>Please Enter your email address.</em>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label>Password*</label>
        <input class="form-control" name="host_password" oninvalid="this.setCustomValidity('Please enter valid email password.')" oninput="this.setCustomValidity('')" pattern="[a-z A-Z 0-9 @!#$%^&*()/-_.]{1,}" type="password" placeholder="Enter the password">
        <em>Please Enter your email password.</em>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label>Email Secure * </label>
        <select class="form-control" name="mail_secure">
            <option value="ssl">SSL</option>
            <option value="tls">TLS</option>
        </select>
        <em>Does the incoming mail server support SSL (Secure Sockets Layer) or TLS (Transport Layer Security) encryption?</em>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</fieldset>
<fieldset class="syst_sett" id="6_field" data-chck="6" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Additional Steps</h2>
                    <!--                    <span>Lets walk through the final steps of getting your account ready to go.</span>-->
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="6">
                        <div class="welcome_step_ouetr">
                            <div class="usage_field">
                                <h3 class="text-center">Select how you plan on using GraphicsZoo</h3>
                                <div class="choose_purpose">
                                    <div class="choose_item">
                                        <input id="for_client" type="radio" name="client" value="1" class="clnt_typ">
                                        <span class="wlcom_check"></span>
                                        <label for="for_client">Sell design services to my clients</label>
                                    </div>                            
                                    <div class="choose_item">
                                        <input id="for_own" type="radio" name="client" value="2" class="clnt_typ">
                                        <span class="wlcom_check"></span>
                                        <label for="for_own">Using this for internal company tasks</label>
                                    </div>                            
                                </div>
                            </div>                         
                        </div>
                    </div>
                </div>
                <div class="row pay_mode_blk" style="display:none">
                    <div class="col-md-offset-2 col-md-8">
                        <!--                        <input type="hidden" name="step" value="6">-->
                        <div class="welcome_step_ouetr">
                            <div class="payment_mode">
<!--                                <div class="form-group com_web_heading">
                                    <img src="<?php //echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/wel_pmt.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Payment Mode
                                        <em>Do you want to accept payments online directly through Graphicszo</em>
                                    </h3>
                                </div>-->
                                <div class="form-group text-center">
                                    <label>Do you want to accept payments online directly through Graphicszoo from your customers?</label>
                                    <div class="pmt_check">
                                        <div class="pmt_check_item">
                                            <input type="radio" name="payment_mode" value="1">
                                            <label><i class="fas fa-check"></i><span>Yes</span></label>
                                        </div>  
                                        <div class="pmt_check_item">
                                            <input type="radio" name="payment_mode" value="0">
                                            <label><i class="fas fa-times"></i><span>No, I will accept payments manually</span></label>
                                        </div>  
                                    </div>
                                </div>
                                <div class="form-group text-center strp_acnt" style="display:none">
                                    You can connect your Stripe account and subscription options from the white label module in your settings.
<!--                                    <label>Click below to connect your Stripe Account</label>
                                    <a class="usr_connect_stripe" target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&scope=read_write&client_id=ca_Feq9KZiLMta2j2RtQOerxDJU4QK8z7Ai">
                                     <i class="fab fa-stripe-s"></i> Connect with stripe
                                    </a>
                                    <div class="loading_connect_procs"></div>-->
<!--                                    <div class="pmt_check">
                                        <div class="pmt_check_item">
                                            <input type="radio" name="stripe_acc" value="yes">
                                            <label><i class="fas fa-check"></i><span>yes</span></label>
                                        </div>  
                                        <div class="pmt_check_item">
                                            <input type="radio" name="stripe_acc" value="no">
                                            <label><i class="fas fa-times"></i><span>no</span></label>
                                        </div>  
                                    </div>-->
                                </div>
                            </div>
                        </div>
                         <div class="row subscription_blk text-center" style="display:none">
                    You can create subscription options from the white label module in your settings.
<!--                    <div class="col-md-offset-2 col-md-8">
                                                <input type="hidden" name="step" value="7">
                        <input type="hidden" name="f_subs_id" value="" id="f_subs_id">
                        <div class="welcome_step_ouetr">
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/Subscription_ico.png" class="img-responsive">
                                    <h3 class="wel_sub_heading subs_text">New Subscription
                                        <em>Complete the form below to add new subscription.</em>
                                    </h3>
                                <div class="preview_subs">Preview<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> To see preview of subscription, please fill required fields. </div></div></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group switch_tog">
                                        <label class="payment_mod_txt">Offline Payment</label>
                                        <div class="form-group switch-custom-usersetting-check">
                                            <input type="checkbox" name="subs_payment_mode" class="form-control" id="f_subs_payment_mode" disabled="">
                                            <label for="f_subs_payment_mode">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="subs_payment_mode" value="0" id="f_subs_payment_mode">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan Type</label>
                                        <select class="form-control f_subs_plan_type_name" name="subs_plan_type_name" id="f_subs_plan_type_name">
                                            <option value="one_time">Request Based</option>
                                            <option value="all">Subscription Based</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if ($agency_info[0]['shared_designer'] == 1) { ?>
                                    <div class="col-md-6 f_pln_user_type" style="display:none">
                                        <div class="form-group">
                                            <label>User Type</label>
                                            <select class="form-control" name="subs_plan_user_type" id="f_subs_user_type">
                                                <option value="1">Shared</option>
                                                <option value="0">Dedicated</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <input type="hidden" name="subs_plan_user_type" value="0" id="f_subs_user_type">
                                <?php } ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan ID</label>
                                        <input class="form-control" name="plan_id" autofocus type="text" placeholder="Enter the ID name">
                                    </div>
                                </div>
                                <div class="col-md-6 stripe_is_sec" style="display:none">
                                    <div class="stripe_pln_sec" style="display:none">
                                        <div class="form-group">
                                            <label>Stripe ID<span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> <strong>Note:</strong> <span>You need to create this product for subscription based plans in stripe first and use the plan ID here. For more info <a href="https://drive.google.com/file/d/1vcaVS_NB0msCWbfkI22aOIrpj-UZqZqC/view" target="_blank"class="demo_stripe_link">click here </a></span></div></div></label>
                                            <select class="form-control" name="subs_planid" id="f_subs_planid" data-pay="1" value="">
                                                <?php
                                                if (!empty($clientplans)) {
                                                    foreach ($clientplans as $plan) {
                                                        ?>
                                                        <option value="<?php echo $plan['id']; ?>"><?php echo $plan['plan_name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mannuly_subs_pln_sec" style="display:none">
                                        <div class="form-group">
                                            <label>Plan ID<span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"><strong>Note:</strong> <span>Plan id should be unique.</span></div></div></label>
                                            <input type="text" name="subs_plan_id" class="form-control" id="f_subs_plan_id" data-pay="1" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan Name*<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> This will be show to users when they signup.</div></div></label>
                                        <input class="form-control" name="subs_plan_name" type="text" pattern="[a-z A-Z 0-9 $-()/]+[a-z]{1,}" maxlength="30" placeholder="Plan Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group currency_icon">
                                        <label>Plan Price*<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Users will pay this price for subscription when they signup.</div></div></label>
                                        <span class="plan_prc_currncy">$</span><input class="form-control" type="text" pattern="[0-9]{1,}" name="subs_plan_price" maxlength="8" placeholder="Plan Price">
                                    </div>
                                </div>
                                <div class="col-md-6 onetimepln">
                                    <div class="form-group">
                                        <label>Number of Requests <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Total Number of requests user can add.</div></div></label>
                                        <input type="number" name="total_requests" class="form-control" id="f_subs_numof_req" value="" min="1" max="4">
                                    </div>
                                </div> 
                                <div class="col-md-6 f_amount_cycle" style="display:none">
                                    <div class="form-group">
                                        <label>Payment Circle</label>
                                        <select class="form-control" name="subs_plan_type">
                                            <option value="monthly">Monthly</option>
                                            <option value="yearly">Yearly</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group switch_tog">
                                        <label>Activate Plan<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> User can see only activated subscription when they signup.</div></div></label>
                                        <div class="form-group switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_subs_plan_active" id="actv_plan_1" class="active_plan">
                                            <label for="actv_plan_1">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                 <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/plan_ftr.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Plan Features<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> These features will be show to users when they signup. </div></div></h3>
                                </div>
                                 <div class="col-md-12">
                                <div class="form-group">
                                    <label>Plan Features<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> These features will be show to users when they signup. </div></div></label>
                                    <div class="plan_feature_field">
                                        <input class="form-control plan_features" type="text" name="subs_plan_features[]" maxlength="55" pattern="[a-z A-Z 0-9 $@!#%^/&*()]+[a-z]{1,}" placeholder="Enter the plan features">
                                        <a class="subs-btn add_more_subscription"><i class="fas fa-plus"></i></a>
                                    </div>
                                    <div class="input_fields_container"></div>
                                </div>
                            </div>
                            </div>
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/permission_ico.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Permissions<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> User can access system according to permissions after signup</div></div></h3>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_file_sharing" id="file_sharing_1" class="f_file_sharing">
                                            <label for="file_sharing_1">
                                            </label>
                                            <span>File Sharing Capabilities</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_file_management" id="file_mngmnt" class="f_file_management">
                                            <label for="file_mngmnt">
                                            </label>
                                            <span>File Management</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_apply_coupon" id="acpt_cpn" class="acpt_cpn">
                                            <label for="acpt_cpn">
                                            </label>
                                            <span>Accept Coupons</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>-->
                </div>
                    </div>
                </div>
               
            </fieldset>
<!--            <fieldset class="paymnt_sett" id="6_field" data-chck="6" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Payment</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="6">
                        <div class="welcome_step_ouetr">
                            <div class="payment_mode">
                                <div class="form-group com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/wel_pmt.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Payment Mode
                                        <em>Enable these settings to sync stripe payments and auto upgrade your account for a complete streamlined system.</em>
                                    </h3>
                                </div>
                                <div class="form-group text-center">
                                    <label>Do you want to receive payment online or you want to manage this manually?</label>
                                    <div class="pmt_check">
                                        <div class="pmt_check_item">
                                            <input type="radio" name="payment_mode" value="1">
                                            <label><i class="fas fa-check"></i><span>yes</span></label>
                                        </div>  
                                        <div class="pmt_check_item">
                                            <input type="radio" name="payment_mode" value="0">
                                            <label><i class="fas fa-times"></i><span>no</span></label>
                                        </div>  
                                    </div>
                                </div>
                                <div class="form-group text-center strp_acnt" style="display:none">
                                    <label>You Need To Connect Your Account With Stripe. After completing on-boarding process you will get the button to connect with stripe.</label>
                                    <div class="pmt_check">
                                        <div class="pmt_check_item">
                                            <input type="radio" name="stripe_acc" value="yes">
                                            <label><i class="fas fa-check"></i><span>yes</span></label>
                                        </div>  
                                        <div class="pmt_check_item">
                                            <input type="radio" name="stripe_acc" value="no">
                                            <label><i class="fas fa-times"></i><span>no</span></label>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="subs_sett" id="7_field" data-chck="7" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Subscription</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="7">
                        <input type="hidden" name="f_subs_id" value="" id="f_subs_id">
                        <div class="welcome_step_ouetr">
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/Subscription_ico.png" class="img-responsive">
                                    <h3 class="wel_sub_heading subs_text">New Subscription
                                        <em>Complete the form below to add new subscription.</em>
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group switch_tog">
                                        <label class="payment_mod_txt">Offline Payment</label>
                                        <div class="form-group switch-custom-usersetting-check">
                                            <input type="checkbox" name="subs_payment_mode" class="form-control" id="f_subs_payment_mode" disabled="">
                                            <label for="f_subs_payment_mode">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="subs_payment_mode" value="0">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan Type</label>
                                        <select class="form-control f_subs_plan_type_name" name="subs_plan_type_name" id="f_subs_plan_type_name">
                                            <option value="one_time">Request Based</option>
                                            <option value="all">Subscription Based</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if ($agency_info[0]['shared_designer'] == 1) { ?>
                                    <div class="col-md-6 f_pln_user_type">
                                        <div class="form-group">
                                            <label>User Type</label>
                                            <select class="form-control" name="subs_plan_user_type" id="f_subs_user_type">
                                                <option value="1">Shared</option>
                                                <option value="0">Dedicated</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <input type="hidden" name="subs_plan_user_type" value="0" id="f_subs_user_type">
                                <?php } ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan ID</label>
                                        <input class="form-control" name="plan_id" autofocus type="text" placeholder="Enter the ID name">
                                    </div>
                                </div>
                                <div class="col-md-6 stripe_is_sec" style="display:none">
                                    <div class="stripe_pln_sec" style="display:none">
                                        <div class="form-group">
                                            <label>Stripe ID<span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> <strong>Note:</strong> <span>You need to create this product for subscription based plans in stripe first and use the plan ID here. For more info <a href="https://drive.google.com/file/d/1vcaVS_NB0msCWbfkI22aOIrpj-UZqZqC/view" target="_blank"class="demo_stripe_link">click here </a></span></div></div></label>
                                            <select class="form-control" name="subs_planid" id="f_subs_planid" data-pay="1" value="">
                                                <?php
                                                if (!empty($clientplans)) {
                                                    foreach ($clientplans as $plan) {
                                                        ?>
                                                        <option value="<?php echo $plan['id']; ?>"><?php echo $plan['plan_name']; ?></option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mannuly_subs_pln_sec" style="display:none">
                                        <div class="form-group">
                                            <label>Plan ID<span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"><strong>Note:</strong> <span>Plan id should be unique.</span></div></div></label>
                                            <input type="text" name="subs_plan_id" class="form-control" id="f_subs_plan_id" data-pay="1" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan Name</label>
                                        <input class="form-control" name="subs_plan_name" autofocus type="text" pattern="[a-z A-Z 0-9 $-()/]+[a-z]{1,}" maxlength="30" placeholder="Plan Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group currency_icon">
                                        <label>Plan Price</label>
                                        <span class="plan_prc_currncy">$</span><input class="form-control" autofocus type="text" pattern="[0-9]{1,}" name="subs_plan_price" maxlength="8" placeholder="Plan Price">
                                    </div>
                                </div>
                                <div class="onetimepln">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Number of Requests <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Total Number of requests user can add.</div></div></label>
                                            <input type="number" name="total_requests" class="form-control" id="f_subs_numof_req" value="" min="1">
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-6 f_amount_cycle" style="display:none">
                                    <div class="form-group">
                                        <label>Payment Circle</label>
                                        <select class="form-control" name="subs_plan_type">
                                            <option value="monthly">Monthly</option>
                                            <option value="yearly">Yearly</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group switch_tog">
                                        <label>Activate Plan</label>
                                        <div class="form-group switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_subs_plan_active" id="actv_plan_1" class="active_plan">
                                            <label for="actv_plan_1">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/permission_ico.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Permissions</h3>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_file_sharing" id="file_sharing_1" class="f_file_sharing">
                                            <label for="file_sharing_1">
                                            </label>
                                            <span>File Sharing Capabilities</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_file_management" id="file_mngmnt" class="f_file_management">
                                            <label for="file_mngmnt">
                                            </label>
                                            <span>File Management</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_apply_coupon" id="acpt_cpn" class="acpt_cpn">
                                            <label for="acpt_cpn">
                                            </label>
                                            <span>Accept Coupons</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/plan_ftr.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Plan Features</h3>
                                </div>
                                <div class="form-group">
                                    <div class="input_fields_container"></div>
                                    <div class="plan_feature_field">
                                        <input class="form-control plan_features" type="text" name="subs_plan_features[]" maxlength="55" pattern="[a-z A-Z 0-9 $@!#%^/&*()]+[a-z]{1,}" placeholder="Enter the plan features">
                                        <a class="subs-btn add_more_subscription"><i class="fas fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="stripe_sett" id="8_field" data-chck="8" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Create new stripe account</h2>
                    <span>You can  update these settings in prefrecnces ares any time</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="8">
                        <div class="welcome_step_ouetr">
                            <div class="strip_mode text-center">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/strip_logo.png" class="img-responsive">
                                <p>Do you want to receive payment online or you want to manage this manually?</p>
                                <a class="usr_connect_stripe" target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&scope=read_write&client_id=ca_Feq9KZiLMta2j2RtQOerxDJU4QK8z7Ai"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/strip_btn.png" class="img-responsive"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>-->
            <fieldset class="getstat_sett" id="7_field" data-chck="7" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading"> HOORAY! You have setup your account!</h2>
                    <span>Keep in mind you can update any of the settings in the Settings page at any time</span>
                </div>
                <div class="get_started_sec">   

                    <div class="get_started_img">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/started_img.svg" class="img-responsive">
                    </div>
                    <div class="get_started_info">
                        <p>We are so excited to have you as a part of the GraphicsZoo family. Now lets get your first project started so we can get some amazing designs made for you.</p>
                        <a class="lets_go_started" href="<?php echo base_url(); ?>customer/request/add_new_request">Create New Project</a>
                    </div>
                    
                </div>  
            </fieldset>
            <div class="save_btn text-center">
                <!--                <button type="button" class="save_next back" id="back" style="display:none;">Back</button>-->
                <button type="submit" class="save_next save" id="next">save & next</button>
                <button type="button" class="skip_without_save" id="skip_without_save" style="display:none;">Skip >></button>
                <!--                <button type="button" class="save_next finish" id="finish" style="display:none;">Finish</button>-->
                <div class="setng_txt"><span>You can update this information in accounts settings.</span></div>
            </div>
            
        </div>
    </form>
</section>
<div class="modal fade slide-3 model-close-button in similar-prop" id="setmaindomain_msg" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
       <div class="cli-ent-model-box">
        <header class="fo-rm-header">
         <h2 class="popup_h2 del-txt">Point your domain</h2>
     </header>
     <div class="cli-ent-model">
         <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/dns.png">
         <p><br/></p>
         <p>You can use your own domain or subdomain and the complete application with run there. For this, you will have to point your domain to <b>A records</b> to our server which is <b>3.17.33.76</b>. We will install the SSL certificate within 24 hours of domain creation.
          For further instructions <a href="javascript:void(0)" class="domain_inst site_btn" id="domain_inst">click here</a> or you can contact us and we will be here to support you.</p>
      </div>
      <div class="onborng_cnfrm">
        <button type="button" class="btn arec_ntcnfrm_btn" data-dismiss="modal" style="color:#652121">Cancel</button>
        <button type="button" class="btn arec_cnfrm_btn" data-dismiss="modal" style="color:#652121">Confirm</button>
    </div>
</div>
</div>
</div>
</div>
<link href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/bootstrap-colorpicker.min.css" rel="stylesheet">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/bootstrap-colorpicker.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/agency_setting.js"></script>
<script>
    var is_agency = "<?php echo $parent_user_plan[0]["is_agency"]; ?>";
    $(function () {
        $('.sel_colorpicker').colorpicker( {
            customClass: 'colorpicker-2x',
            sliders: {
                saturation: {
                    maxLeft: 150,
                    maxTop: 150
                },
                hue: {
                    maxTop: 150
                },
                alpha: {
                    maxTop: 150
                }
            }

        });
    });
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];

//            console.log("selElmnt",selElmnt);
//            console.log("sel",x);
/*for each element, create a new DIV that will act as the selected item:*/
a = document.createElement("DIV");
a.setAttribute("class", "select-selected");
a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
x[i].appendChild(a);

/*for each element, create a new DIV that will contain the option list:*/
b = document.createElement("DIV");
b.setAttribute("class", "select-items select-hide");
for (j = 0; j < selElmnt.length; j++) {
                /*for each option in the original select element,
                create a new DIV that will act as an option item:*/
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function (e) {
                    /*when an item is clicked, update the original select box,
                    and the selected item:*/
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function (e) {
                /*when the select box is clicked, close any other select boxes,
                and open/close the current select box:*/
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }
        function closeAllSelect(elmnt) {
            /*a function that will close all select boxes in the document,
            except the current select box:*/
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }
        /*if the user clicks anywhere outside the select box,
        then close all select boxes:*/
        document.addEventListener("click", closeAllSelect);
        /*
         Dropdown with Multiple checkbox select with jQuery - May 27, 2013
         (c) 2013 @ElmahdiMahmoud
         license: https://www.opensource.org/licenses/mit-license.php
         */


         function getSelectedValue(id) {
            return $("#" + id).find("dt a span.value").html();
        }

//        $(document).bind('click', function (e) {
//            console.log("click");
//            var $clicked = $(e.target);
//            if (!$clicked.parents().hasClass("dropdown"))
//                $(".dropdown dd ul").hide();
//        });




</script>
