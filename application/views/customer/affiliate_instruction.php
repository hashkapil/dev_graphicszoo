<section class="affiliate-wrap">
<section class="become-affiliate-section">
    <div class="container">
        <div class="row">
        <div class="col-md-12">
    <h1>Become Affiliate</h1>
<!--    <p>It is a long established fact that a reader will be distracted by the readable content of a page
when looking at its layout. The point of using Lorem Ipsum is that it has a</p>-->
        </div>
        </div>
    </div>    
</section>

<section class="affiliate-section affiliate-progaram become-fact">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="left-earn-sec">
                    <h2 class="affiliate-sub-heading">Earn Flawlessly with the GraphicZoo Affiliate Program!</h2>
                    <p>Share and refer to earn huge! Join the Graphiczoo affiliates today!</p>
                    <a class="join_today" href="<?php echo base_url().'customer/affiliate/BecomeaffiliatedUser'; ?>">Become an affiliate</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="right-earn-image">
                    <img alt="affiliat-program" src="https://www.graphicszoo.com/public/assets/img/customer/become-affiliate/beacomaffiliate.jpg" class="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="affiliate-section program-benifit">
    <div class="container">
    <h2 class="affiliate-sub-heading">Affiliate program Benefits</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="program-item">
                    <div class="program-aff-image">
                    <img alt="affiliat-program" src="https://www.graphicszoo.com/public/assets/img/customer/become-affiliate/affiliate-program-a.svg" class="">
                    </div>
                    <h3>Personalised Tool Kit </h3>
                    <p>Quick and easy access to a marketing and promotional tool kit,personalised according to your choice and needs.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="program-item">
                <div class="program-aff-image">
                    <img alt="affiliat-program" src="https://www.graphicszoo.com/public/assets/img/customer/become-affiliate/affiliate-program-a.svg" class="">
                    </div>
                    <h3>Manage Earnings </h3>
                    <p>Keep track of all your earnings and reffrral through our custom built dashboard.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="program-item">
                <div class="program-aff-image">
                    <img alt="affiliat-program" src="https://www.graphicszoo.com/public/assets/img/customer/become-affiliate/affiliate-program-a.svg" class="">
                    </div>
                    <h3>No Enrollment Fees </h3>
                    <p>Yes, you herd me right! it’s free and all your money is your to keep!</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="affiliate-section affiliate_instruction_row">
    <div class="container">
        <h2 class="affiliate-sub-heading">Instructions</h2>
        <div class="white-boundries">
        <div class="row">
            <div class="col-md-8">
                <div class="instruction_list">
                    <ul>
                        <li>- Once you register, you will get a unique referral link.</li>
                        <li>- Promote us on your website or in your blogs, newsletters, etc using the referral link.</li>
                        <li>- You can also choose from the promotions already provided in the marketing tool kit.</li>
                        <li>- For each guest click, signup and purchase, you earn commissions.</li>
                    </ul>
                    <a class="join_today" href="<?php echo base_url().'customer/affiliate/BecomeaffiliatedUser'; ?>">Become an affiliate</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="instruc-image">
                <img alt="graph-image" src="https://www.graphicszoo.com/public/assets/img/customer/become-affiliate/graph-image.png" class="">
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<section class="affiliate-section affiliate-talk">
    <div class="container">
        <h2 class="affiliate-sub-heading">Our Affiliates Talk!</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="talk-item">
                    <div class="talker-header">             
                        <div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-a.png" class=""></div>
                        <div class="talker-info">
                            <h4>Erica</h4>
                            <span>New York, USA</span>
                        </div>
                        
                    </div>
                    <div class="talker-say">
                            <p>Graphiczoo hands-down provides the best affiliate program in the industry with a mixture of unlimited offers for the affiliates. I have referred it to a lot of friends and acquaintances and they have been more than just pleased with the graphic designs and services. Earning has never been more fun for me.</p>
                        </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="talk-item">
                    <div class="talker-header">             
                        <div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-b.png" class=""></div>
                        <div class="talker-info">
                            <h4>Adam Chris</h4>
                            <span>New York, USA</span>
                        </div>
                        
                    </div>
                    <div class="talker-say">
                            <p>I have been an affiliate for years now and have collaborated with all types of brands. The experience with Graphiczoo was rather different and better in a lot of aspects. I love their team. They’re so friendly, dedicated and hard-working. This is an awesome option for some passive income. Thank you Graphiczoo!</p>
                        </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="talk-item">
                    <div class="talker-header">             
                        <div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/danny.jpg" class=""></div>
                        <div class="talker-info">
                            <h4>Danny</h4>
                            <span>New York, USA</span>
                        </div>
                        
                    </div>
                    <div class="talker-say">
                            <p>Responsive staff, cool designs, simple payout options, and great other services. Graphiczoo has it all! My references have been extremely satisfied and hence I would definitely urge all of you to join the impeccable affiliate program of the Graphiczoo!</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>


