<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>plugins/owl/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>plugins/owl/owl.theme.default.min.css">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>plugins/fancybox/jquery.fancybox.css"/>
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/style-web.css">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/custom.css">
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>plugins/owl/jquery.min.js"></script>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>plugins/owl/owl.carousel.js"></script>
        <title>Graphics Zoo </title>
        <style>
            /*the container must be positioned relative:*/
            .custom-select {
                position: relative;
                display: inline-block;
                width: 100%;
                height: auto;
                padding: 0;
                line-height: 34px;
                color: #495057;
                vertical-align: middle;
                background-size: 8px 10px;
                border: 1px solid #d7d7d7;
                border-radius: 8px;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                max-width: 300px;
                border: 1px solid #ced4da;
            }
            .custom-select select {
                display: none; /*hide original SELECT element:*/
            }
            .custom-select .select-selected {
                width: 100%;
                padding: 9px 14px;
                background: #fff url(<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/sign-nep.png) right center no-repeat;
                background-repeat: no-repeat;
                background-position:95% 50%;
                border-radius: 8px;
                font-size: 15px;
                color: #0f3148;
                font-weight: 500;

            }

            /*style items (options):*/
            .select-items {
                position: absolute;
                background-color:#fff;
                top: 100%;
                left: 0;
                right: 0;border: 1px solid #ddd;
                z-index: 99;
            }
            .custom-select .select-items div {
                padding:4px 23px;    font-size: 12px;
                font-weight: 600;
            }
            /*hide the items when the select box is closed:*/
            .select-hide {
                display: none;
            }
            .select-items div:hover, .same-as-selected {
                background-color: rgb(232, 46, 77);
                color: #fff;
            }
        </style>
    </head>
    <body>
        <!-- header -->
        <div class="container">
            <div class="">
                <div class="col-md-12">
                    <a href="http://newdesign.graphicszoo.com/" class="gzthank">
                        <img class="img-fluid" src="<?php echo FS_PATH_PUBLIC_ASSETS ?>front_end/Updated_Design/img/logo.png">
                    </a>
                </div>
                <div class="col-lg-12">
                    <div class="right-sec3">
                        <div class="main-thanks">
                            <h2>Thank you for Signing up!</h2>
                            <p> Tell us about yourself so we can personalize your experience</p>
                        </div>
                        <form method="POST" id="questionaries" name="questionaries">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 describe-sec">
                                    <h4><span class="words">1</span>Which best describes your business?	</h4>
                                    <div class="custom-select">
                                        <select name="business_detail">
                                            <option value="Marketing Agency">Marketing Agency</option>
                                            <option value="Start-up">Start-up</option>
                                            <option value="Print Shop">Print Shop</option>
                                            <option value="E-commerce">E-commerce	</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 describe-sec">
                                    <h4><span class="words">2</span> What is your role at the company? </h4>

                                    <div class="custom-select">
                                        <select name="company_role">
                                            <option value="CEO/Owner">CEO/Owner  </option>
                                            <option value="VP">VP</option>
                                            <option value="Marketing Director">Marketing Director </option>
                                            <option value="Manager">Manager</option>
                                            <option value="Other">Other</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 describe-sec">
                                    <h4><span class="words">3</span>  How many designs do you need  per month?</h4>
                                    <div class="custom-select">
                                        <select name="design_count">
                                            <option value="Less than 10">Less than 10</option>
                                            <option value="10-20">10-20 </option>
                                            <option value="20-30">20-30</option>
                                            <option value="30-50">30-50 </option>
                                            <option value="More than 50">More than 50</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 describe-sec">
                                    <h4><span class="words">4</span>  What kind of designs do you usually need?  <p class="red-info">(you can select more then one)</p></h4>
                                    <p class="multiSel"></p>  
                                    <dl class="dropdown"> 
                                        <dt>
                                        <a href="javascript:void(0)">
                                            <span>Select</span>    
                                        </a>
                                        </dt>
                                        <dd>
                                            <div class="mutliSelect" >
                                                <ul style="display:none">
                                                    <li>
                                                        <label><input name="design_type[]" type="checkbox" value="Logo & Branding" />Logos & Branding</label></li>
                                                    <li>
                                                        <label><input name="design_type[]" type="checkbox" value="Posters & Flyers " />Posters & Flyers</label></li>
                                                    <li>
                                                        <label><input name="design_type[]" type="checkbox" value="Marketing Ads" />Marketing Ads</label></li>
                                                    <li>
                                                        <label><input name="design_type[]" type="checkbox" value="T-shirts & Print " />T-shirts & Print </label></li>
                                                    <li>
                                                        <label><input name="design_type[]" type="checkbox" value="UI/UX" />UI/UX</label></li>
                                                    <li>
                                                        <label><input name="design_type[]" type="checkbox" value="Other" />Other</label></li>
                                                </ul>
                                            </div>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 describe-sec email-valid-text">
                                    <h4><span class="words">5</span>Enter Your Mobile Number </h4>
                                    <div class="custom-select">
                                        <input type="number" class="select-selected spec" id="phone" name="mob_no" placeholder="Mobile Number" required>
                                    </div>
                                    <div class="validation"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 submit-after text-center">
                                    <input type="submit" class="submit-btn" name="quest_submit" value="Get Started">                           
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->


    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/bootstrap.min.js" ></script>

    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/jquery.fancybox.js"></script>


    <script>
        var x, i, j, selElmnt, a, b, c;
        /*look for any elements with the class "custom-select":*/
        x = document.getElementsByClassName("custom-select");
        console.log("x",x);
        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            /*for each element, create a new DIV that will act as the selected item:*/
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            /*for each element, create a new DIV that will contain the option list:*/
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 0; j < selElmnt.length; j++) {
                /*for each option in the original select element,
                 create a new DIV that will act as an option item:*/
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function (e) {
                    /*when an item is clicked, update the original select box,
                     and the selected item:*/
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function (e) {
                /*when the select box is clicked, close any other select boxes,
                 and open/close the current select box:*/
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }
        function closeAllSelect(elmnt) {
            /*a function that will close all select boxes in the document,
             except the current select box:*/
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }
        /*if the user clicks anywhere outside the select box,
         then close all select boxes:*/
        document.addEventListener("click", closeAllSelect);
    </script>
    <script>
        /*
         Dropdown with Multiple checkbox select with jQuery - May 27, 2013
         (c) 2013 @ElmahdiMahmoud
         license: https://www.opensource.org/licenses/mit-license.php
         */

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');
        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });


        function getSelectedValue(id) {
            return $("#" + id).find("dt a span.value").html();
        }

        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown"))
                $(".dropdown dd ul").hide();
        });

        $('.mutliSelect input[type="checkbox"]').on('click', function () {

            var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
                    title = "," + $(this).val();

            if ($(this).is(':checked')) {
                var html = '<span title="' + title + '">' + title + '</span>';
                $('.multiSel').append(html);
                $(".hida").hide();
            } else {
                $('span[title="' + title + '"]').remove();
                var ret = $(".hida");
                $('.dropdown dt a').append(ret);

            }

        });

        $("#questionaries").submit(function (e) {
            var phone = $('#phone').val();
            $(".email-valid-text .validation").html("");
            if (phone.length <= 7 ) {
                $(".email-valid-text .validation").html("<span>Phone number should be atleast 6-7 digits.</span>");
                return false;
            } else {
                $(".email-valid-text .validation").html("");
                return true;
            }
        });
    </script>

</body>
</html>