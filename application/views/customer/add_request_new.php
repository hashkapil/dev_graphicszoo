<style>
.grid {
    display: grid;
    grid-gap: 10px;
    grid-template-columns: repeat(auto-fill, minmax(250px,1fr));
    grid-auto-rows: 20px;
}
.upload-gallery ul.thumbnil-img li .content img{
    width: 100% !important;
    height: inherit !important;

}
.upload-gallery ul.thumbnil-img li {
    width: 100%;
}
.error,.error_search_msg {
    color: red;
    font-size: 15px;
}
.select2-dropdown {top: 22px !important; left: 8px !important;}
</style>
<link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS .'css/nice-select.css';?>">
<link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS .'css/select2.min.css';?>">

<?php
$get_method = $this->router->fetch_method();
$currenturl =  explode('/',$_SERVER['REQUEST_URI']);
if(in_array('add_new_request',$currenturl)){
  $formlayout =  str_replace("add_new_request","add_request_new",$_SERVER['REQUEST_URI']);
}else{
  $formlayout =  str_replace("add_request_new","add_new_request",$_SERVER['REQUEST_URI']);
}
//echo "<pre/>";print_R($allcategories);exit;
?>
<div class="wizard_add_request">
    <div class="container">
        <div class="row">
            <div class="add_wizard_head">
                <div class="catagry-heading" data-step="8" data-intro="Use this form to add details about the design you need created." data-position='right' data-scrollTo='tooltip'>
                <h3>
                    Add New Project<span>Provide details of your new design project.</span>
                </h3>
                    <div class="switch_rq_layout"> <a href="<?php echo $formlayout;?>">Switch to old</a></div>
                 </div>
                <div class="add_wizard_body">
                    <div class="row">
                        <div class="col-sm-2 col-md-2">
                            <div class="add_left_wizard">
                                <div class="add_step_count 1_leftbar current_step ">
                                    <i class="add_step_icon"></i>
                                    <span>Basic info</span>
                                </div>
                                <div class="add_step_count 2_leftbar">
                                    <i class="add_step_icon"></i>
                                    <span>Appearance</span>
                                </div>
                                <div class="add_step_count 3_leftbar">
                                    <i class="add_step_icon"></i>
                                    <span>Questions</span>
                                </div>
                                <div class="add_step_count 4_leftbar">
                                    <i class="add_step_icon"></i>
                                    <span>Photos</span>
                                </div>
                                <div class="add_step_count 5_leftbar">
                                    <i class="add_step_icon"></i>
                                    <span>Summary</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10">
                            <div class="add_right_wizard">
                                <form action="javascript:void(0)" method="post" id="addRequestform">
                                    <input type="hidden" name="is_sample_exist" id="is_sample_exist" value="0">
                                    <input type="hidden" name="is_quest_loaded_once" id="is_quest_loaded_once" value="0">
                                    <input type="hidden" name="is_quest_loaded_onback" id="is_quest_loaded_onback" value="0">
                                    <input type="hidden" name="drafted_id" id="drafted_id" value="0">
                                    <?php 
//                                    echo "<pre/>";print_R($profile_data);exit;
                                    $is_trial = $profile_data[0]['is_trail'];
                                    $editvar = '';
                                    $editID = isset($_GET['reqid']) ? $_GET['reqid'] : '';
                                    $dupID = isset($_GET['did']) ? $_GET['did'] : '';
                                    if ($editID && $editID != '') {
                                        $editvar = $existeddata[0];
                                    }if ($dupID && $dupID != '') {
                                        $editvar = $duplicatedata[0];
                                    }if ($editID == '' && $dupID == '') {
                                        $editvar = '';
                                    }
//                                     echo "<pre/>";print_R($editvar);exit;
//                                     echo "<pre/>";print_R($allqust);exit;
                                    ?>
                                    <input type="hidden" name="edit_id" id="edit_id" value="<?php echo (isset($editID) && $editID != '')?$editID:'0'; ?>">
                                    <input type="hidden" name="dup_id" id="dup_id" value="<?php echo (isset($dupID) && $dupID != '')?$dupID:'0'; ?>">
                                    <fieldset class="bsc_info active" id="1_field" data-chck="1" style="display: block;">
                                        <h3>Step <span class="count_step_wiz_bas">1</span> : <span>Basic Info</span></h3>
                                        <div class="form-group">
                                            <label>Enter the title of your project *</label>
                                            <input class="form-control pro_name" name="project_name"  type="text" id="pro_name" placeholder="Project Name" autofocus value="<?php echo isset($editvar['title']) ? $editvar['title'] : ""; ?>">
                                            <span class="sub_exp">Example: <em>Banner, Logo, Facebook Cover</em></span>
                                        </div>
                                        <!--<input type="text" class="searchcat" value=""/>-->
                                        <div class="form-group">
                                            <label>Choose Category *</label>
                                            <select class="form-control" name="category_id" id="cat_req" onchange="getsubcat(this);">
                                                <option value="">Select Category</option>
                                                <?php foreach ($allcategories as $key => $value) { ?>
                                                    <option value="<?php echo $value['id']; ?>"
                                                        <?php echo ($value['id'] == $editvar['category_id'] || $value['name'] == $editvar['category']) ? 'selected' : ''; ?>>
                                                        <?php echo $value['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group subcat_div" style="display: none">
                                            <label>Choose Sub-category that best suits your project. *</label>
                                            <div class="show_err"></div>
                                            <div class="row">
                                                <?php foreach ($allcategories as $kp => $vp) { ?>
                                                    <div class="form-group hide-prev" id="<?php echo "child_" . $vp['id']; ?>" style="display:none">
                                                        <?php foreach ($vp['child'] as $kc => $vc) { ?>
                                                            <div class="col-xs-6 col-md-2">
                                                                <div class="wizard_sub_catg">
                                                                    <input type="radio" name="subcategory_id" id="subct_id" class="subcategory_class"  value="<?php echo $vc['id']; ?>" <?php echo ($editvar['subcategory_id'] == $vc['id'] || $editvar['logo_brand'] == $vc['name']) ? 'checked' : ''; ?>>
                                                                    <div class="sub_catg_img">
                                                                        <div class="wiz_cat_check">
                                                                            <i class="fas fa-check"></i>
                                                                        </div>
                                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/<?php echo $vc['image_url']; ?>" class="img-responsive">
                                                                        <span><?php echo $vc['name']; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <p class="saved_msg"></p>
                                    </fieldset>
                                    <fieldset class="smpl_info" id="2_field" data-chck="2" data-min_val="" style="display: none;">
                                        <h3>Step <span class="count_step_wiz_samp">2</span> : <span>sample</span></h3>
                                        <div class="form-group">
                                            <label>Choose samples related to your requirement</label>
                                            <div class="wiz_sample samples_categories"></div>
                                            <h4 class="choose_samp_info text-center">You can choose max <strong class="max_des">12 designs</strong> and min <strong class="min_des">8 designs</strong></h4>
                                        </div>
                                          <p class="saved_msg"></p>
                                    </fieldset>
                                    <fieldset class="app_info" id="3_field" data-chck="3" style="display: none;">
                                        <h3>Step <span class="count_step_wiz_apper">2</span> : <span>Appearance</span></h3>
                                        <div class="form-group">
                                            <label>Brand Profile</label> <a href="javascript:void(0)" class="add_brands">Add new brand profile</a>
<!--                                            <button class="add_brands">Add Brands</button>-->
                                            <select name="brand_name" id="brand_sell" class="form-control">
                                                <option value="">Choose brand profile</option>
                                                <?php
//                                                echo "<pre>";print_R($user_profile_brands);
                                                if ($user_profile_brands != '') {
                                                    for ($i = 0; $i < sizeof($user_profile_brands); $i++) {
                                                ?>
                                                <option value="<?php echo $user_profile_brands[$i]['id']; ?>" <?php
                                                    if ($user_profile_brands[$i]['id'] == $editvar['brand_id']) {
                                                        echo "selected";
                                                    }
                                                ?>><?php echo $user_profile_brands[$i]['brand_name']; ?></option>
                                                <?php
                                                }
                                                ?>
                                                <option class="none" value="N/A">none</option>
                                                <?php 
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <?php
                                        foreach ($allqust as $qk => $qv) {
                                            if ($qv['id'] == DESIGN_DIMENSION) { ?>
                                                <div class="form-group">
                                                    <label><?php
                                                        echo $qv['question_label'];
                                                        echo ($qv['is_required'] == '1') ? ' *' : '';
                                                        ?></label>
                                                    <input class="form-control des_dimnsn questionsup" id="des_dimnsn" type="text" name="quest[<?php echo $qv['id']; ?>]" value="<?php echo (isset($qv['answer']) && $qv['answer'] != 'null' && $qv['answer'] != '') ? $qv['answer'] : ''; ?>" placeholder="Enter the dimension">
                                                    <span class="sub_exp"><?php echo $qv['question_info']; ?></span>
                                                </div>
                                            <?php } ?>
                                            <div class="row">
                                                <?php if ($qv['id'] == LET_DESIGNER_CHOOSE) { ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo $qv['question_label']; ?></label>
                                                            <span class="sub_exp"><?php echo $qv['question_placeholder']; ?></span>
                                                            <div class="wiz_prefrence">
                                                                <?php
                                                                $color_pref =  (isset($editvar['color_pref']) && $editvar['color_pref'] !== "Let Designer Choose for me") ? "block" : "none"; 
                                                                $qustnoptions = explode(',', $qv['question_options']);
                                                                foreach ($qustnoptions as $opk => $opv) {
                                                                    if($editvar){
                                                                    if($editvar['color_pref'] == "Let Designer Choose for me"){
                                                                        $checkkked =  ($opk == 0); 
                                                                    }else{
                                                                        $checkkked = ($opk == 1);  
                                                                    }
                                                                    ?>
                                                                    <label for="soundsignal<?php echo $opk; ?>">
                                                                        <input type="radio" name="color_pref[]" class="preference color_codrr" id="soundsignal<?php echo $opk; ?>" value="<?php echo $opv; ?>" <?php echo ($checkkked) ? 'checked' : ''; ?>>
                                                                        <span><?php echo $opv; ?></span>
                                                                    </label>
                                                                    <?php }else{ ?>
                                                                       <label for="soundsignal<?php echo $opk; ?>">
                                                                        <input type="radio" name="color_pref[]" class="preference color_codrr" id="soundsignal<?php echo $opk; ?>" value="<?php echo $opv; ?>" <?php echo ($opk == 0) ? 'checked' : ''; ?>>
                                                                        <span><?php echo $opv; ?></span>
                                                                        </label>  
                                                                   <?php }} ?>
                                                            </div>
                                                            <div class="">
                                                                <div class="col-md-6 choose_color_pref_type" style="display:<?php echo $color_pref;?>;">
                                                                    <div class="hax_code">
                                                                        <select class="form-control color_drpdn " id="color_pref_sel" name="color_pref[]">
                                                                            <option value="">Select color code</option>
                                                                            <option value="hex" <?php echo ($editvar['color_pref'] == 'hex') ? 'selected' : '';?>>Hex Code</option>
                                                                            <option value="pantone" <?php echo ($editvar['color_pref'] == 'pantone') ? 'selected' : '';?>>Pantone Color</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 ">
                                                                    <div class="enter_hax_code" style="display:<?php echo $color_pref;?>">
                                                                        <?php if(!empty($editvar['color_values'])){
                                                                            $links_arr = explode(',',$editvar['color_values']);
                                                                            foreach($links_arr as $lk => $lv){ ?>
                                                                            <input placeholder="Enter Color Code" type="text" name="color_values[]" class="form-control f_color_nm" value="<?php echo (isset($lv) && $lv !== "Let Designer Choose for me") ? $lv : '' ?>">
                                                                        <?php }}else { ?>
                                                                            <input placeholder="Enter Color Code" type="text" name="color_values[]" id="color_val" class="form-control f_color_nm" value="">
                                                                        <?php } ?>
                                                                        <button type="button" class="add_more_colors"><i class="fas fa-plus"></i></button>
                                                                        <div class="input_color_container"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }if ($qv['id'] == PROJECT_DELIVERABLE) { ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo $qv['question_label']; ?></label>
                                                            <span class="sub_exp"><?php echo $qv['question_placeholder']; ?></span>
                                                            <div class="wiz_pref_check">
                                                                <?php
                                                                $deliverableop = explode(',', $qv['question_options']);
                                                                $deliverableans = explode(',', $editvar['deliverables']);
//                                                                echo "<pre/>";print_R($deliverableop);
//                                                                echo "<pre/>";print_R($deliverableans);
                                                                foreach ($deliverableop as $dk => $dv) {
                                                                    if($editvar != ''){ ?>
                                                                    <label for="wiz_src_<?php echo $dv; ?>">
                                                                        <?php echo $dv; ?>
                                                                        <input id="wiz_src_<?php echo $dv; ?>" class="deliverables_up" name="filetype[]" value="<?php echo $dv; ?>" type="checkbox" <?php echo in_array(strtolower($dv), array_map('strtolower', $deliverableans)) ? 'checked' : ''; ?>>
                                                                        <span class="wiz_checkmark"></span>
                                                                    </label>  
                                                                    <?php }else{
                                                                    ?>
                                                                    <label for="wiz_src_<?php echo $dv; ?>">
                                                                        <?php echo $dv; ?>
                                                                        <input id="wiz_src_<?php echo $dv; ?>" class="deliverables_up" name="filetype[]" value="<?php echo $dv; ?>" type="checkbox" <?php echo ($dk == 0 || $dk == 1) ? 'checked' : ''; ?>>
                                                                        <span class="wiz_checkmark"></span>
                                                                    </label>
                                                                    <?php }} ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                         <p class="saved_msg"></p>
                                    </fieldset>
                                    <fieldset class="qstn_info" id="4_field" data-chck="4" style="display: none;">
                                        <h3>Step <span class="count_step_wiz_qust">3</span> : <span>Questions</span></h3>
                                        <?php
                                        foreach ($allqust as $deskey => $desval) {
                                            if ($desval['id'] == DESCRIPTION) {
                                                ?>
                                                <div class="form-group add_question_text">
                                                    <label><?php echo $desval['question_label']; ?>*</label>
                                                    <div class="add_wrapper">
                                                        <?php if($editvar && $desval['answer'] != ''){ 
                                                            $description_ans = explode('__',$desval['answer']);
                                                            foreach($description_ans as $dkk => $dvv){
                                                        ?>
                                                        <div class="input_wrapper">
                                                            <textarea class="form-control design_reqntt reqclass questionsup" name="quest[<?php echo $desval['id']; ?>][]" placeholder="Enter Design Requirements"><?php echo $dvv;?></textarea>
                                                        <button class="enter_btn" type="submit"><i class="fas fa-level-down-alt"></i></button>
                                                        </div>
                                                        <?php }}else{ ?>
                                                        <div class="input_wrapper">
                                                            <textarea class="form-control design_reqntt getinsumry reqclass questionsup" name="quest[<?php echo $desval['id']; ?>][]" placeholder="Enter Design Requirements"></textarea>
                                                        </div>
                                                        <div class="input_wrapper">
                                                            <textarea class="form-control design_reqntt getinsumry reqclass questionsup" name="quest[<?php echo $desval['id']; ?>][]" placeholder="Enter Design Requirements"></textarea>
                                                        </div>
                                                        <div class="input_wrapper">
                                                            <textarea class="form-control design_reqntt getinsumry reqclass questionsup" name="quest[<?php echo $desval['id']; ?>][]" placeholder="Press Enter for new line items"></textarea>
                                                            <button class="enter_btn" type="submit"><i class="fas fa-level-down-alt"></i></button>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <div class="question_based_on_catsubcat">
                                        </div>
                                        <?php
                                        foreach ($allqust as $textkey => $textval) {
                                            if ($textval['id'] == TEXT_INCLUDING) {
                                                ?>
                                                <div class="form-group additional_wiz add_question_text">
                                                    <label><?php echo $textval['question_label']; ?></label>
                                                    <?php if($editvar){ ?>
                                                    <div class="switch-custom-usersetting-check">
                                                    <input type="checkbox" name="text_included" id="text_included" class="text_included" <?php echo ($editvar['text_include'] != '') ? 'checked' : '';?>>
                                                    <label for="text_included"></label> 
                                                    </div><span class="answer_toggle"><?php echo ($editvar['text_include'] != '') ? 'YES' : 'NO';?></span>
<!--                                                    <input class="form-control include_text" name="include_text" type="text" value="<?php echo (isset($editvar['text_include']) && $editvar['text_include'] != '') ? $editvar['text_include'] : ''; ?>" placeholder="Enter you answer" style="display:<?php echo ($editvar['text_include'] != '') ? 'block' : 'none';?>;" pattern="[a-z A-Z 0-9]{1,}">-->
                                                    <textarea class="form-control include_text getinsumry questionsup" name="include_text" type="text" value="<?php echo (isset($editvar['text_include']) && $editvar['text_include'] != '') ? $editvar['text_include'] : ''; ?>" placeholder="Enter you answer" style="display:<?php echo ($editvar['text_include'] != '') ? 'block' : 'none';?>;" pattern="[a-z A-Z 0-9]{1,}"></textarea>
                                                    <?php }else { ?>
                                                    <div class="switch-custom-usersetting-check">
                                                    <input type="checkbox" name="text_included" id="text_included" class="text_included">
                                                    <label for="text_included"></label> 
                                                    </div><span class="answer_toggle">NO</span>
                                                     <textarea class="form-control include_text getinsumry questionsup" name="include_text" type="text" value="<?php echo (isset($editvar['text_include']) && $editvar['text_include'] != '') ? $editvar['text_include'] : ''; ?>" placeholder="Enter you answer" style="display:<?php echo ($editvar['text_include'] != '') ? 'block' : 'none';?>;" pattern="[a-z A-Z 0-9]{1,}"></textarea>
<!--                                                    <input class="form-control include_text" name="include_text" type="text" placeholder="Enter you answer" style="display:none;" pattern="[a-z A-Z 0-9]{1,}">-->
                                                    <?php } ?>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                         <p class="saved_msg"></p>
                                    </fieldset>
                                    <fieldset class="stock_info" id="5_field" data-chck="5" style="display: none;">
                                        <h3>Step <span class="count_step_wiz_pht">4</span> : <span>photos</span></h3>
                                        <div class="form-group">
                                            <label>Attach your own images or choose from the stock library.</label>
                                            <div class="wiz_upload_file">
                                                <input class="form-control" type="file" multiple="" name="file-upload[]" id="add_files_req">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/cloud-computing.svg" class="img-responsive">
                                                <strong>Drag and drop file here or</strong>
                                                <span>Click Here To Upload</span>
                                            </div>
                                            <span class="sub_exp">Example: Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov, tiff, ttf, eps</span>
                                        </div>
                                        <div class="form-group search_wiz_stock">
                                            <label>Search stock images to your project request.</label> 
                                            <input class="form-control" id="search_keywrd" type="text" placeholder="Enter your keyword eg:apple,books etc">
                                            <button type="submit" class="serach_stock_btn">Search</button>
                                            <span class="error_search_msg"></span>
                                        </div>
                                        <?php
                                        if(!empty($editvar['customer_attachment'])){
                                        for ($i = 0; $i < count($editvar['customer_attachment']); $i++) {
                                            $type = substr($editvar['customer_attachment'][$i]['file_name'], strrpos($editvar['customer_attachment'][$i]['file_name'], '.') + 1);
                                            ?>
                                            <div class="uploadFileRow" id="file<?php echo $editvar['customer_attachment'][$i]['id']; ?>">
                                                <div class="col-md-6">
                                                    <div class="extnsn-lst">
                                                        <p class="text-mb">
                                                            <?php echo $editvar['customer_attachment'][$i]['file_name']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }} ?>
                                        <div class="uploadFileListContain row">
                                        </div>
                                    </fieldset>
                                    <fieldset class="smry_info" id="6_field" data-chck="6" style="display: none;">
                                        <h3>Step <span class="count_step_wiz_smry">5</span> : <span>Summary</span></h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class=" summary_label">
                                                    <label>Project Title</label>
                                                    <span class="proj_value"></span>
                                                    <a class="summary_edit title_edit" href="javascript:void(0)"><i class="icon-gz_edit_icon"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="summary_label">
                                                    <label>Category and Sub category</label>
                                                    <span class="selectedcat_name"></span>
                                                    <a class="summary_edit cat_edit" href="javascript:void(0)"><i class="icon-gz_edit_icon"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class=" summary_label">
                                                    <label>Design dimensions</label>
                                                    <span class="des_dmmn"></span>
                                                    <a class="summary_edit des_edit" href="javascript:void(0)"><i class="icon-gz_edit_icon"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class=" summary_label">
                                                    <label>Color preferences</label>
                                                    <span class="color_preee"></span>
                                                    <a class="summary_edit color_edit" href="javascript:void(0)"><i class="icon-gz_edit_icon"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class=" summary_label">
                                                    <label>Project Deliverables</label>
                                                    <span class="deleverable"></span>
                                                    <a class="summary_edit del_edit" href="javascript:void(0)"><i class="icon-gz_edit_icon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add_question_summary">
                                            <h3><span>Additional questions</span>
                                                <a class="summary_edit adqst_edit" href="javascript:void(0)"><i class="icon-gz_edit_icon"></i></a>
                                            </h3>
                                            <div class="additional_divss">
                                            </div>
                                        </div>
                                        <div class="add_question_summary">
                                            <h3><span>Attachments</span>
                                                <a class="summary_edit attchmnt_edit" href="javascript:void(0)"><i class="icon-gz_edit_icon"></i></a>
                                            </h3>
                                            <?php if($editvar){ //echo "<pre/>";print_R($editvar); ?>
                                            <div class="wiz_summry_list appndfile">
                                                <?php 
                                                if(!empty($editvar['customer_attachment'])){
                                                foreach($editvar['customer_attachment'] as $kk => $vv){?>
                                                    <span><img src='<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS.$vv['request_id'].'/'.$vv['file_name']; ?>'></span>
                                                <?php }}else{ ?>
                                                    <p>No data found</p>
                                                <?php } ?>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="wiz_summry_list appndfile">
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </fieldset>
                                    <div class="prev_next_btn text-center">
                                        <button class="prev_step wiz_step_btn back_req" style="display:none;" type="submit">Previous Step</button> 
                                        <button class="next_step wiz_step_btn next_req" type="submit">Next Step</button>
                                        <button class="next_step wiz_step_btn save_submit" name="save_submit" type="submit" value="submit" style="display:none;">Submit</button>
                                        <!--<input type="submit" class="next_step wiz_step_btn save_submit" id="request_submit" value="Submit" name="save_submit" style="display:none;"/>-->
                                    </div>
                                    <div class="save_as_draft text-center">
                                        <button class="save_as_draft_btn draft_req" type="submit" style="display:none;">save as draft</button>
                                    </div>
                                </form>
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal upload-gallery fade" id="uploadstock" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Images </h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-lg-12 col-sm-12">
                    <div class="search-container col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <!--                        <div class="form-group search_wiz_stock">
                                                        <label>Search stock images to your project request.</label> 
                                                        <input class="form-control" id="search_keywrd" type="text" placeholder="Enter your keyword eg:apple,books etc">
                                                        <button type="submit" class="serach_stock_btn">Search</button>
                                                    </div>-->
                            </div>
                            <div class="uploadAnDsave">
                                <div class="left-up-btn">
                                    <form id="uploadForm" action="<?php echo base_url(); ?>customer/request/download_images" method="post">
                                        <input type="hidden" name="req_fromPage" value="<?php echo $get_method; ?>">
                                        <div class="selected_images"></div>
                                        <button type="submit" data-loading-text="<i class='fas fa-circle-notch fa-spin'></i></i> Downloading..." class="DownloadBtn" style="display:none;"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                                    </form>
                                    <input type="hidden" name="req_fromPage" value="<?php echo $get_method; ?>">
                                    <input type="hidden" name="search_keyword" value="" id="search_keyword_hid">
                                    <button type="button" data-loading-text="<i class='fas fa-circle-notch fa-spin'></i></i> Uploading..." class="upload_btn" style="display: none;"><i class="fa fa-paperclip" aria-hidden="true"></i> Attach</button> 
                                </div>
                                <div class="uploaded-items">
                                    <div class="content_Container"></div>
                                </div>
                            </div>
                            <div class="search_head-commeneted"></div>
                            <div class="col-sm-12">
                                <div class="ajax_searchload" style="display:none;">
                                    <img src="<?php echo base_url(); ?>public/assets/img/ajax-loader.gif">
                                </div>
                                <div class="img-gallery-grid">
                                    <ul class="thumbnil-img grid" id="image_gallery"></ul>
                                    <div class="loader_button">
                                        <div class="ajax_se_loaderbtnn" style="display:none;">
                                            <img src="<?php echo base_url(); ?>public/assets/img/ajax-loader.gif">
                                        </div>
                                        <a href="#" id="loadMore" style="display:none;" onclick="load_more_images($(this))">Load More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal upload-gallery fade brand_popup" id="addbrandspopup" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Brand profile </h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-lg-12 col-sm-12">
                    <div class="search-container col-sm-12">
                        <div class="col-md-12">
                            <div class="white-boundries mr-t0">
                                <form id="addbranDs" method="post" action="">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Brand Name <span>*</span></label>
                                                <input type="text" class="form-control" name="brand_name" id="exampleInputName" aria-describedby="NameHelp" required="" value="">                                            
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Website URL</label>
                                                <input type="text" class="form-control" name="website_url" id="exampleInputwebsite_url" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                              <div class="form-group">
                                                <label>Fonts</label>
                                                <input type="text" class="form-control" name="fonts" id="exampleInputwebsite_url" value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                             <div class="form-group">
                                                <label>Color Preferences</label>
                                                <select name="color_preference" class="form-control">
                                                    <option value="No Preference">No Preference</option>
                                                    <option value="CMYK Files">CMYK Files</option>
                                                    <option value="RGB Files">RGB Files</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                             <div class="form-group">
                                                <label>Description <span>*</span></label>
                                                    <textarea class="form-control" rows="5" name="description" id="comment" required=""></textarea>
                                                    <p class="fill-sub">Please provide us as much detail as possible. Include things such as color preferences, styles you like, text you want written on the graphic, and anything else that may help the designer.</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="form-group add_ref_link">
                                                <label>addition reference link</label>
                                                <?php
                                                if (!empty($editvar['google_link'])) {
                                                    $links_arr = explode(',', $editvar['google_link']);
                                                    //echo "<pre/>";print_r($links_arr);
                                                    foreach ($links_arr as $lk => $lv) {
                                                        ?>
                                                        <input type="text" name="google_link[]" class="form-control google_link f_cls_nm" value="<?php echo isset($lv) ? $lv : ''; ?>"/>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <input type="text" name="google_link[]" class="form-control google_link f_cls_nm"/>
                                                <?php } ?>
                                                <a class="add_more_button">
                                                    <i class="icon-gz_plus_icon"></i>
                                                </a>
                                                <div class="input_fields_container"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="upload-card">
                                                <label for="exampleInputlogo">Brand Logo</label>
                                                <div class="file-drop-area file-upload">
                                                    <span class="fake-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_Draft_img_icon.svg" class="img-responsive">
                                                    </span>
                                                    <span class="file-msg">Select files to upload or Drag and drop file</span>
                                                    <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="logo_upload[]" id="logo_file_input">
                                                </div>
                                            </div>
                                            <div class="uploadFileListContainer row logo_uploadFileListContain">                      
                                            </div>
                                            <?php if (sizeof($edit_materail) > 0) { ?>
                                                <div class="uploadFileListContainer row">   
                                                    <?php
                                                    for ($i = 0; $i < count($edit_materail); $i++) {
                                                        if ($edit_materail[$i]['file_type'] == 'logo_upload') {
                                                            ?>
                                                            <div  class="uploadFileRow" id="file<?php echo $edit_materail[$i]['id']; ?>">
                                                                <div class="col-md-12">
                                                                    <div class="extnsn-lst">
                                                                        <p class="text-mb">
                                                                            <?php echo $edit_materail[$i]['filename']; ?>
                                                                        </p>
                                                                        <p class="cross-btnlink">
                                                                            <a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $editID . '/' . $edit_materail[$i]['filename']; ?>" download>
                                                                                <span><i class="fa fa-download" aria-hidden="true"></i></span>
                                                                            </a>
                                                                        </p>
                                                                        <p class="cross-btnlink">
                                                                            <a href="javascript:void(0)" data-fieldname="logo" data-delfile="<?php echo $edit_materail[$i]['filename']; ?>" data-fileid="<?php echo $edit_materail[$i]['id']; ?>" data-flpath="<?php echo FS_UPLOAD_PUBLIC_UPLOADS . 'brand_profile/' . $editID . '/' . $edit_materail[$i]['filename']; ?>" class="edt_brnd_prfl">
                                                                                <span>x</span>
                                                                            </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="upload-card">
                                                <label for="exampleInputlogo">Marketing Materials</label>
                                                <div class="file-drop-area file-upload">
                                                    <span class="fake-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_Draft_img_icon.svg" class="img-responsive">
                                                    </span>
                                                    <span class="file-msg">Select files to upload or Drag and drop file</span>
                                                    <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="materials_upload[]" id="materials_file_input">
                                                </div>
                                            </div>
                                            <div class="uploadFileListContainer row materials_uploadFileListContain">                      
                                            </div>
                                            <?php if (sizeof($edit_materail) > 0) { ?>
                                                <div class="uploadFileListContainer row">   
                                                    <?php
                                                    for ($i = 0; $i < count($edit_materail); $i++) {
                                                        // $type = substr($edit_materail['customer_attachment'][$i]['file_name'], strrpos($editvar['customer_attachment'][$i]['file_name'], '.') + 1);
                                                        if ($edit_materail[$i]['file_type'] == 'materials_upload') {
                                                            ?>
                                                            <div  class="uploadFileRow" id="file<?php echo $edit_materail[$i]['id']; ?>">
                                                                <div class="col-md-12">
                                                                    <div class="extnsn-lst">
                                                                        <p class="text-mb">
                                                                            <?php echo $edit_materail[$i]['filename']; ?>
                                                                        </p>
                                                                        <p class="cross-btnlink">
                                                                            <a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $editID . '/' . $edit_materail[$i]['filename']; ?>" download>
                                                                                <span><i class="fa fa-download" aria-hidden="true"></i></span>
                                                                            </a>
                                                                        </p>
                                                                        <p class="cross-btnlink">
                                                                            <a href="javascript:void(0)" data-fieldname="marketing_materials" data-delfile="<?php echo $edit_materail[$i]['filename']; ?>" data-fileid="<?php echo $edit_materail[$i]['id']; ?>" data-flpath="<?php echo FS_UPLOAD_PUBLIC_UPLOADS . 'brand_profile/' . $editID . '/' . $edit_materail[$i]['filename']; ?>" class="edt_brnd_prfl">
                                                                                <span>x</span>
                                                                            </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            <?php } ?>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="upload-card">
                                                <label for="exampleInputlogo">Additional Images </label>
                                                <div class="file-drop-area file-upload">
                                                    <span class="fake-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_Draft_img_icon.svg" class="img-responsive">
                                                    </span>
                                                    <span class="file-msg">Select files to upload or Drag and drop file</span>
                                                    <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="additional_upload[]" id="additional_file_input">
                                                </div>
                                            </div>
                                            <div class="uploadFileListContainer row additional_uploadFileListContain">                      
                                            </div>
                                            <?php
                                            //echo "<pre>";print_r($edit_materail); 
                                            if (sizeof($edit_materail) > 0) {
                                                ?>
                                                <div class="uploadFileListContainer row">   
                                                    <?php
                                                    for ($i = 0; $i < count($edit_materail); $i++) {
                                                        // $type = substr($edit_materail['customer_attachment'][$i]['file_name'], strrpos($editvar['customer_attachment'][$i]['file_name'], '.') + 1);
                                                        if ($edit_materail[$i]['file_type'] == 'additional_upload') {
                                                            ?>
                                                            <div  class="uploadFileRow" id="file<?php echo $edit_materail[$i]['id']; ?>">
                                                                <div class="col-md-12">
                                                                    <div class="extnsn-lst">
                                                                        <p class="text-mb">
                                                                            <?php echo $edit_materail[$i]['filename']; ?>
                                                                        </p>
                                                                        <p class="cross-btnlink">
                                                                            <a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $editID . '/' . $edit_materail[$i]['filename']; ?>" download>
                                                                                <span><i class="fa fa-download" aria-hidden="true"></i></span>
                                                                            </a>
                                                                        </p>
                                                                        <p class="cross-btnlink">
                                                                            <a href="javascript:void(0)" data-fieldname="additional_images" data-delfile="<?php echo $edit_materail[$i]['filename']; ?>" data-fileid="<?php echo $edit_materail[$i]['id']; ?>" data-flpath="<?php echo FS_UPLOAD_PUBLIC_UPLOADS . 'brand_profile/' . $editID . '/' . $edit_materail[$i]['filename']; ?>" class="edt_brnd_prfl">
                                                                                <span>x</span>
                                                                            </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-lg-12 ad_new_reqz">
                                            <div class="submit-brnd-btn">
                                                <input type="submit" class="submit cnsl-sbt" value="save" id="brand_profile_submit_add" name="brand_profile"/>
                                                <a href="javascript:void(0)" data-dismiss="modal" class="cnsl-sbt theme-cancel-btn">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS . 'js/select2.min.js';?>"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php echo FS_PATH_PUBLIC_ASSETS . 'js/customer/add_request.js'; ?>"></script>
<script type="text/javascript" src="<?php // echo FS_PATH_PUBLIC_ASSETS . 'js/customer/jquery.nice-select.min.js'; ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>

<script>
    $(document).on('click','.add_brands',function(e){
     $('#addbrandspopup').modal('show');
});
$uploadPath = "<?php echo FS_PATH_PUBLIC_UPLOADS ?>";
var get_method = "<?php echo $get_method; ?>";
var reqID = "<?php echo isset($_GET['reqid']) ? $_GET['reqid']:'';?>";
var dupID = "<?php echo isset($_GET['did']) ? $_GET['did']:'';?>";
</script>
