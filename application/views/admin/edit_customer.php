<section id="content-wrapper">

    <div class="site-content-title">       

        <ol class="breadcrumb float-xs-right">
            <li class="breadcrumb-item">
                <span class="fs1" aria-hidden="true" data-icon="?"></span>
                <a href="#">Main Menu</a>
            </li>
            <li class="breadcrumb-item active">View Users</li>
        </ol>
    </div>
	
	
	
	<div class="content" style="background:aliceblue;border-top: none;">
		<h2 class="float-xs-left content-title-main">Edit Customer</h2>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">                
                <div class="nav-tab-pills-image" style="background: white;">
                    <ul class="nav nav-tabs" role="tablist" style="background: white;">                      
                        
                        <li class="nav-item active">
                            <a class="nav-link myactiveclass" data-toggle="tab" data-id="edit_profile" href="#edit_profile" role="tab">Edit Profile</a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link myactiveclass" data-toggle="tab" href="#design_requests_tab" role="tab">Design Requests</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link myactiveclass" data-toggle="tab" href="#approved_designs_tab" role="tab">Approved Designs</a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link myactiveclass" data-toggle="tab" href="#billing" role="tab" data-id="billing">Billing</a>
                        </li>
                    </ul>
                    <div class="divider15"></div>
                    <div class="tab-content">
                        

                        <div class="tab-pane active" id="edit_profile" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
    <div class="content col-md-8" style="box-shadow: none;">
        <div class="general-elements-content">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="nav-tab-horizontal">
                        <div class="divider15"></div>
                        <!--<h4 class="page-content-title">Default validators</h4>-->
                       <?php if($this->session->flashdata('message_error') != '') {?>				
				   <div class="alert alert-danger alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
						<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
					</div>
				   <?php }?>
				   <?php if($this->session->flashdata('message_success') != '') {?>				
				   <div class="alert alert-success alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
						<strong><?php echo $this->session->flashdata('message_success');?></strong>
					</div>
				   <?php }?>
        <div class="all-form-section">
            <div class="row">                
                <form enctype="multipart/form-data">

					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
							<label>Company Name</label>
						</div>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="form-group">
								<input type="text" name="company_name" class="form-control" maxlength="500" id="company-name" value="<?php echo $customer[0]['company_name']; ?>">
							</div>
						</div>
					</div>

					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
							<label>First Name</label>
						</div>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="form-group">
								<input type="text" name="first_name" class="form-control" maxlength="500" id="company-name" value="<?php echo $customer[0]['first_name']; ?>">
							</div>
						</div>
					</div>

					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
							<label>Last Name</label>
						</div>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="form-group">
								<input type="text" name="last_name" class="form-control" maxlength="500" id="company-name" value="<?php echo $customer[0]['last_name']; ?>">
							</div>
						</div>
					</div>
					
					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
							<label>Current Plan</label>
						</div>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="form-group">
								<select name="user_current_plan" class="form-control">
									<?php print_r($subscription); foreach($subscription as $key=>$value){ ?>
										<option value="<?php echo $key; ?>" <?php if($key==$planid){ echo "selected"; } ?>><?php echo $value; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					
					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
							<label>Designer</label>
						</div>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="form-group">
								<select name="designer_id" class="form-control">
									<option>Please Select Designer</option>
									<?php for($i=0;$i<sizeof($designer_list);$i++){ ?>
										<option value="<?php echo $designer_list[$i]['first_name']." ".$designer_list[$i]['last_name']; ?>"><?php echo $designer_list[$i]['first_name']." ".$designer_list[$i]['last_name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>

					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
							
						</div>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="form-group" style="margin:0px;">
								<input type="checkbox" class="" name="alldesign">
							</div>
						</div>
					</div>
					
					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
							<label>Email</label>
						</div>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="form-group">
								<input type="email" name="email" class="form-control" maxlength="500" id="company-name" value="<?php echo $customer[0]['email']; ?>">
							</div>
						</div>
					</div>

					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
							<label>Profile Picture</label>
						</div>
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-8">
										<?php
										//if ($customer[0]['profile_picture_url']):
											//echo $this->Html->image($customer->profile_picture_url, ['class' => 'img img-responsive img-medium']);
											
											//echo '<div class="divider15"></div>';
										//endif;
										?>
										<input type="file" class="dropify" name="profile_picture1" data-height="200" data-plugin="dropify">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="element-form">
						<div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
						<div class="col-xl-9 col-lg-9 col-sm-12 col-md-9 col-xs-12">
							<input type="submit" class="btn btn-success flat-buttons waves-effect waves-button" value="Edit Customer">
						</div>
					</div>
                </form>
            </div>
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                                </div>
                            </div>
                        </div>
						
						
						<div class="tab-pane content-datatable datatable-width" id="design_requests_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Title</th>
                                                <th>Description</th>                           
                                                <th>Design Preview</th>
                                                <th>Date Requested</th>
                                                <th>Action</th>                            
                                            </tr>
                                        </thead>
                                        <tbody>
											 <?php for ($i=0;$i<sizeof($designs_requested);$i++){ ?>
                                                <tr>
                                                    <td><?= $i+1 ?></td>
                                                    <td><?= $designs_requested[0]['title'] ?></td>
                                                    <td><?= $designs_requested[0]['description'] ?></td>  
                                                    <td>
                                                        <?php
                                                       // if ($designs_requested[0]['attachment']):
                                                            //$img_path = REQUEST_IMG_PATH . DS . $request->id . DS . $request->profile_picture;
//                                        echo $this->Html->image($img_path, ['class' => 'img img-responsive img-small']);
                                                       // endif;
                                                        ?>
                                                    </td>
                                                    <td><?= ($designs_requested[0]['created']) ? date("d-m-Y",strtotime($designs_requested[0]['created'])) : ''; ?></td>  
                                                    <td>
                                                        <a href="">
                                                            <span class="fa fa-eye fa-2x text-primary"></span>
                                                        </a>

                                                        <?php
                                                        //echo $this->Form->postLink('<span class="fa fa-trash-o fa-2x text-danger"></span>', '#', ['escape' => FALSE, 'confirm' => 'Are you sure want to delete?']);
                                                        ?>
                                                    </td>
                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Title</th>
                                                <th>Description</th>                           
                                                <th>Design Preview</th>
                                                <th>Date Requested</th>
                                                <th>Action</th>                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i=0;$i<sizeof($designs_approved);$i++){ ?>
                                                <tr>
                                                    <td><?= $i+1 ?></td>
                                                    <td><?= $designs_approved[0]['title'] ?></td>
                                                    <td><?= $designs_approved[0]['description'] ?></td>  
                                                    <td>
                                                        <?php
                                                       // if ($designs_approved[0]['attachment']):
                                                            //$img_path = REQUEST_IMG_PATH . DS . $request->id . DS . $request->profile_picture;
//                                        echo $this->Html->image($img_path, ['class' => 'img img-responsive img-small']);
                                                        //endif;
                                                        ?>
                                                    </td>
                                                    <td><?= ($designs_approved[0]['created']) ? date("d-m-Y",strtotime($designs_approved[0]['created'])) : ''; ?></td>  
                                                    <td>
                                                        <a href="">
                                                            <span class="fa fa-eye fa-2x text-primary"></span>
                                                        </a>

                                                        <?php
                                                        //echo $this->Form->postLink('<span class="fa fa-trash-o fa-2x text-danger"></span>', '#', ['escape' => FALSE, 'confirm' => 'Are you sure want to delete?']);
                                                        ?>
                                                    </td>
                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						
						
						
						
						
						
						
						
						<div class="tab-pane" id="billing" role="tabpanel"><!--------------- billing --------------------------->
		<div class="row">
			<div class="col-md-12">
			<div class="content col-md-8 col-md-offset-2" style="box-shadow: none;">
			
				<h4 class="margin-bottom-20" style="color:#ec1c41 !important;">Change Your Billing Detail</h4>
				
				<div class="col-md-3"><label>Change Billing Detail</label></div>				
				<div class="col-md-9">
				<div class="container">
				<div class="row" style="border: 1px solid #ccc;padding: 23px;color: #000;border-radius: 5px;">
					<div class="form-group">
						<!--<input type="text" class="form-control" data-validation="required" placeholder="Enter Category Name">-->
						 <label>XXXX XXXX XXXX <?= $carddetails['last4'] ?></label>
					</div>
					
					<div class="form-group">
						<!--<input type="text" class="form-control" data-validation="required" placeholder="Enter Category Name">-->
						<div style="float:left;">
						 <label><?= $carddetails['exp_month'] ?>/<?= $carddetails['exp_year'] ?></label>
						 </div>
					
						<!--<input type="text" class="form-control" data-validation="required" placeholder="Enter Category Name">-->
						<div style="float:right;">
							 <label style="font-size: 25px;font-weight: 600;"><?= $carddetails['brand'] ?></label>
						</div>	 
					</div>
				</div>
				</div>
				</div>

				<form>
					<div class="payment-method">
						<h4 class="sign-up-form-heading margin-bottom-20" style="color:#ec1c41 !important;">Payment Method</h4>
						<div class="payment-method-inputs-wrapper">
							<div class="payment-method-input">
								<div class="col-md-3"><label>Card Number</label></div>
								<div class="col-md-9">
									<div class="form-group">
										<input type="tel" name="last4" id="cc" class="textInput box_round4 transition masked" pattern="\d{4} \d{4} \d{4} \d{4}" required="required" maxlength="19" data-placeholder="XXXX XXXX XXXX XXXX">
									</div>
								</div>						
							</div>
							<div class="payment-method-input exp_date">
								<div class="col-md-3"><label>Exp Date</label></div>
								<div class="col-md-9">
									<div class="form-group">
										<input type="tel" name="exp_month" id="expiration1" class="textInput box_round4 transition masked" pattern="(1[0-2]|0[1-9])\/\d\d\d\d" data-valid-example="11/2010" required="required" maxlength="7" data-placeholder="MM/YYYY">
									</div>
								</div>							
							</div>
							<div class="payment-method-input">
								<div class="col-md-3"><label>Card CVC</label></div>
								<div class="col-md-9">
									<div class="form-group">
										<input type="tel" name="cvc" id="tel" class="textInput box_round4 transition masked" pattern="\d{3}\" required="required" maxlength="3" data-placeholder="XXX">
									</div>
								</div>														
							</div>
							<div class="col-md-3"></div>
							<div class="col-md-9">
								<div class="form-group">
									<input type="submit" value= "Change My Card Details" class="btn btn-primary site-btn" name="change_plan">
									<!--<label><?= $carddetails['change_plan'] ?></label>-->
								</div>
							</div>
						</div>
					</div>
				</form>
				
			</div>	
			</div>
		</div>
		<div class="row">
			<div class="content col-md-8 col-md-offset-2" style="box-shadow: none;">
			 
				<h4 class="margin-bottom-20" style="color:#ec1c41 !important;">Change Billing Detail</h4>
				<form>
					<div class="col-md-3"><label>Change Plan</label></div>
					<div class="col-md-9">
					<div class="form-group">
						<!--<input type="text" class="form-control" data-validation="required" placeholder="Enter Category Name">-->
						<select name="myplan" class="form-control">
							<?php for($i=0;$i<sizeof($subscription);$i++){ ?>
								<option></option>
							<?php } ?>
						</select>
					</div>
					 <div class="form-group">
						<input type="submit" value= "Change My Plan" class="btn btn-primary site-btn" name="change_plan">
					</div>
					</div>
				</form>
			</div>
		</div>
	</div><!--------------- billing --------------------------->
						

						
						
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
