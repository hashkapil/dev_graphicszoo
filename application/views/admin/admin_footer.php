<div id="footer_div"></div>          
<!-- jQuery (necessary for JavaScript plugins) -->
<?php 
$url = current_url();
$messageurl = explode('/',$url);
// echo "<pre/>";print_R($messageurl);

if(in_array("addportfolio",array_filter($messageurl)) || (in_array("portfolio_edit",array_filter($messageurl)))  || (in_array("view_files",array_filter($messageurl)))){
} else { ?>
 <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/jquery.min.js');?>"></script>  
 <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/bootstrap.min.js');?>"></script>
<?php } ?>
<link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/chosen.css');?>" rel="stylesheet">
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/ui-slider/jquery.ui-slider.js');?>"></script>

<script defer src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/svgxuse.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/editor/editor.js');?>"></script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/lightgallery-all.min.js');?>"></script>
<!-- <script src="<?php //echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/jquery.mousewheel.min.js');?>"></script> -->
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/all.js');?>"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script> -->

<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/chosen.jquery.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'js/admin/admin_portal.js'); ?>"></script>
<!-- <script src="<?php //echo FS_PATH_PUBLIC_ASSETS ?>js/jquery.fancybox.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<div class="modal similar-prop nonflex fade" id="AddCategory" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add Category</h2>
                <div class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <form action="<?php echo base_url(); ?>admin/Categories/add_category/" method="post" id="addCatForm" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group">
                                         <p class="label-txt label-active"> Parent category</p>
                                    <select name="parent_cat" class="input parent_cat" id="parent_cat">
                                            <option value="">Choose parent</option>
                                            <?php foreach ($cat_data as $kk => $vv) { ?>
                                                <option value="<?php echo $vv['id']; ?>"><?php echo $vv['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="line-box"></div>
                                        <p class="ab-notify">Don't choose if you are adding parent category.</p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Name</p>
                                        <input type="text" name="cat_name" required  class="input name1">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">position</p>
                                        <input type="number" name="position" required class="input position1">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
<!--                                        <p class="label-txt">Bucket</p>-->
                                        <select name="bucket_type" class="input bucket_type">
                                            <option value=''>Choose Bucket</option>
                                            <option value='1'>Artwork</option>
                                            <option value='2'>Creative</option>
                                        </select>
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Timeline</p>
                                        <input type="number" name="timeline" required class=" input timeline">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group goup-x1 text-left">
                                <div class="sound-signal">
                                    <div class="form-radion2">
                                        <label class="containerr">
                                           <input type="checkbox" name="active" class="active1">
                                           <span class="checkmark"></span> Active
                                       </label>
                                   </div>
                               </div>                               
                           </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <img src="" class="showimg">
                                <div class="file-drop-area file-upload">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input type="file" class="file-input project-file-input file-input-add" multiple="" name="cat_image" id="file_input">
                                </div>
                                <div id="file-uploadNameAdd">
                                <span></span>
                            </div>
                            </div>
                            
                          <button type="submit" class="load_more button" name="save">Save</button>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>

<div class="modal similar-prop nonflex fade" id="EditCategory" tabindex="-1" role="dialog" aria-labelledby="EditClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
              <h2 class="popup_h2 del-txt">Edit Category</h2>
              <div class="cross_popup close edit_close" data-dismiss="modal"> x</div>
          </header>
          <div class="cli-ent-model-box">
            <div class="cli-ent-model">
                <div class="fo-rm-body">
                    <form action="<?php echo base_url(); ?>admin/Categories/update_category/" method="post" id="EditCategoryForm" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-group">
                                   <p class="label-txt label-active"> Parent category</p>
                                    <select name="parent_cat" class="input" id="Editparent_cat" onchange="checkbucket(this);">
                                        <option value="">Choose parent</option>
                                        <?php foreach ($cat_data as $kk => $vv) { ?>
                                            <option value="<?php echo $vv['id']; ?>" id="option-id-<?php echo $vv['id']; ?>"><?php echo $vv['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="line-box"></div>
                                    <p class="ab-notify">Don't choose if you are adding parent category.</p>
                                </label>
                            </div>
                            <input type="hidden" name="cat_id" id="cat_id" value="">
                            <div class="col-md-6">
                                <label class="form-group">
                                    <p class="label-txt">Sub category</p>
                                    <input type="text" name="cat_name" required  class="input name">
                                    <div class="line-box"></div>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="form-group">
                                    <p class="label-txt">position</p>
                                    <input type="number" name="position" required class="input position">
                                    <div class="line-box"></div>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="form-group">
<!--                                    <p class="label-txt">Bucket</p>-->
                                    <select name="bucket_type" class="input bucket_type">
                                        <option value=''>Choose Bucket</option>
                                        <option value='1'>Artwork</option>
                                        <option value='2'>Creative</option>
                                    </select>
                                    <div class="line-box"></div>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="form-group">
                                    <p class="label-txt">Timeline</p>
                                    <input type="number" name="timeline" required class=" input timeline">
                                    <div class="line-box"></div>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group goup-x1 text-left">
                                <div class="sound-signal">
                                    <div class="form-radion2">
                                        <label class="containerr">
                                           <input type="checkbox" name="active" class="active"> 
                                           <span class="checkmark"></span> Active
                                       </label>
                                   </div>
                               </div>
                           </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <img src="" class="showimg">
                            <div class="file-drop-area file-upload">
                                <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                <input type="file" class="file-input project-file-input file-input-edit" multiple="" name="cat_image" id="file_inputEdit">
                            </div>
                            <div id="file-uploadNameEdit">
                                <span></span>
                            </div>
                        </div>
                        
                       <button type="submit" name="edit" class="load_more button">Save
                       </button>
                   </form>
               </div>
           </div>
       </div>
   </div>
</div>
</div>
<div class="modal fade slide-3 model-close-button in" id="design_button_editor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

            </div>
            <div class="content_button_editor">
                <input class="InputBtn_Name" name="InputBtn_Name" value=""> 
                <input class="InputBtn_Clr" name="InputBtn_Clr" value=""> 
                <input class="InputBtn_size" name="InputBtn_size" value=""> 
                <input class="InputBtn_url" name="InputBtn_url" value=""> 
            </div>
            <p class="space-b"></p>
        </div>
    </div>
</div>
<div class="modal fade slide-3 model-close-button in" id="show_notepad_area" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="popup_h2 del-txt notetitle"> Add Note For <span></span></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="add_notes_sec">
                    <div class="text-aprt">
                     <textarea type="text" class="pstcmm t_w" id="notes_txtmsg" placeholder="Type a note..."></textarea>
                     <span class="cmmsend">
                         <button class="add_notemsg">
                             <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat-send.png" class="img-responsive" id="add_notes_btn" data-sender_id="<?php echo $login_user_id; ?>">
                         </button>
                     </span>
                 </div>
                 <div class="usernotes_list">
                 </div>
             </div>

         </div>
     </div>

 </div>
</div>
<button style="display: none;" id="opn_design_button_editor" data-toggle="modal" data-target="#design_button_editor">click here</button>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery_lazyload/1.9.7/jquery.lazyload.min.js"></script>


<link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/sweetalert.css">
<script type="text/javascript">
    var baseUrl = '<?php echo base_url();?>';
    $(document).ready(function () {
        $('#lightgallery').lightGallery({
            thumbnail: false,
            zoom: false,
            mousewheel: false,
            fullScreen: false,
            width: "70%"
        });
    });

    setTimeout(function () {
        $('.alert').fadeOut();
    }, 2000);

    <?php if((!in_array('projectReports',$messageurl)) && (!in_array('view_projects_basedonslug',$messageurl)) && (!in_array('cancel_request_users',$messageurl)) && (!in_array('cat_listing',$messageurl)) && (!in_array('view_qa_profile',$messageurl)) && (!in_array('billing_subscription',$messageurl)) && (!in_array('notifications',$messageurl)) && (!in_array('addportfolio',$messageurl)) && ($url != base_url().'admin/Contentmanagement/index') && (!in_array('add_samples',$messageurl)) && (!in_array('category_based_samples',$messageurl)) && (!in_array('category_based_questions',$messageurl)) && (!in_array('add_category_questions',$messageurl)) && (!in_array('feedback_matrix',$messageurl)) && (!in_array('rating_details',$messageurl)) && (!in_array('overall_rating',$messageurl))){ ?>   
    $('#right_nav > li').click(function () {
            $(this).toggleClass('open');
        });
    <?php } ?>

    $(function ()
    {
        $('#save').click(function ()
        {
            $('#txtEditor').text($('#txtEditor').Editor("getText"));
        });
    });

    $(document).ready(function () {
        if($("#txtEditor").length > 0){
    //   $("#txtEditor").Editor();
    var settings ={
        'indent': false,
        'outdent': false,
        'strikeout' : false,
        'block_quote' : false,
        'hr_line':false,
        'splchars':false,
        'ol':false,
        'ul':false
    }
    $("#txtEditor").Editor(settings);
    $("#txtEditor").Editor('createMenuItem', {
    "text": "button", //Text replaces icon if its not available
    "icon":"fa fa-plus-square-o", //This is a Font-Awesome Icon 
    "tooltip": "Design Button",
    "custom": function(button, parameters){ 
                  //Your Custom Function.
                  $('#opn_design_button_editor').trigger('click');
//                                        console.log(parameters);
//                                        console.log(button);
               // alert(parameters);
           },
    "params": {'button':"Button Name",'color':"Button Color",'font':"Font Size"} //Any custom parameters you want to pass
                                     //to your custom function.
                                 });
}
});


    $(document).on('click', "#comment_list li", function (event) {
        var clicked_id = $(this).find('.comments_link').attr('id');
        var topPosition = $("#" + clicked_id + "").css('top');
        var newtopPosition = topPosition.substring(0, topPosition.length - 2); //string 800
        newtopPosition = parseFloat(newtopPosition) || 0;
        var position = $("#" + clicked_id + "").offset();
        var posX = position.left;
        var posR = ($(window).width() + $(window).scrollLeft()) - (position.left + $('#whatever').outerWidth(true));
        $(".mycancel").hide();
        $(".openchat").hide();
        if (newtopPosition < 100) {
            $(".customer_chat").hide();
            $('.customer_chat' + clicked_id + ' .posone2abs').addClass('topposition');
        } else {
            $(".customer_chat").hide();
            $('.customer_chat' + clicked_id + ' .posone2abs').removeClass('topposition');
        }
        if (event.pageX - posR > 450) {
            $('.customer_chat' + clicked_id + ' .posone2abs').addClass('leftposition');
        } else {
            $('.customer_chat' + clicked_id + ' .posone2abs').removeClass('leftposition');
        }
        if (newtopPosition > 500) {
            $(".customer_chat").hide();
            $(".customer_chatimage2").hide();
            $(".customer_chatimage1").show();
            $(".customer_chatimage2.customer_chatimage" + clicked_id).show();
            $(".customer_chatimage1.customer_chatimage" + clicked_id).hide();
            $(".customer_chat" + clicked_id).toggle();
            $('html, body').animate({
                scrollTop: $(".customer_chat" + clicked_id + " .arrowdowns").offset().top
            }, 100);
        } else {
            $(".customer_chat").hide();
            $(".customer_chatimage2").hide();
            $(".customer_chatimage1").show();
            $(".customer_chatimage2.customer_chatimage" + clicked_id).show();
            $(".customer_chatimage1.customer_chatimage" + clicked_id).hide();
            $(".customer_chat" + clicked_id).toggle();
        }
    });
    
    $(document).ready(function () {
        var content = $('#txtEditor').val();
        $('.Editor-editor').html(content);
    });

    /****************categories****************/
//    $(document).on('click','.delete_cat',function(){
//        var r = confirm("Are you sure you want to delete this ?");
//        var id = $(this).attr('data-id');
//        if(r == true){
//            $.ajax({
//                type: "POST",
//                url: "<?php echo base_url(); ?>admin/categories/delete_category",
//                data: {"category_id": id},
//                success: function (data) {
//                    if(data){
//                        window.location.reload();
//                    }
//                }
//            });
//        }
//    });

//    $(document).on('click','.edit_cat',function(e){
//        var id = $(this).attr('data-id');
//        $('#cat_id').val(id);
//        $.ajax({
//            type: "POST",
//            url: "<?php echo base_url(); ?>admin/categories/edit_detailcat",
//            data: {"category_id": id},
//            success: function (data) {
//                var returnedData = JSON.parse(data);
//                if(returnedData){
//                    $.each(returnedData, function( index, value ) {
//                        $('.name').val(value.name);
//                        $('.position').val(value.position);
//                        $('.timeline').val(value.timeline);
//                        $('select[id^="parent_cat"] option[value="'+value.parent_id+'"]').attr("selected","selected");
//                        $('select[name^="bucket_type"] option[value="'+value.bucket_type+'"]').attr("selected","selected");
//                        if(value.image_url){
//                           $(".showimg").attr("src", '<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/'+value.image_url);
//                       }
//                       if(value.is_active == '1'){
//                        $('.active').prop('checked', true);
//                    }
//                });
//                }
//            }
//        });
//    });


//    $(document).on('click','.update_cat',function(e){
//        var id = $(this).attr('data-id');
//        $.ajax({
//            type: "POST",
//            url: "<?php echo base_url(); ?>admin/categories/update_category",
//            data: {"category_id": id},
//            success: function (data) {
//
//            }
//        });
//    });


//    $(document).on('click','.show_child',function(){
//        var id = $(this).attr('data-child_id');
//        $('.plus_'+id).html('-');
//        $(this).addClass('minus');
////        $("child_"+id).slideToggle();
//        $('.child_'+id).css('display','table-row');
//    });
    $(document).on('click','.product-list-show .cli-ent-row.tr',function(){
        $(this).toggleClass('fullheight',1000);
        $(this).find('.mobile-visibles i').toggleClass('fa-plus-circle fa-minus-circle')
    });
//    $(document).on('click','.minus',function(){
//        var id = $(this).attr('data-child_id');
//        $('.plus_'+id).html('+');
//        $('.child_'+id).css('display','none');
//        $(this).removeClass('minus');
//    });

//    function checkbucket(val){
//        var selectedVal = val.value;
//    //if(selectedVal){
//        $(".bucket_type").attr("required", "true");
//    //}
//}

//jQuery(document).ready(function(){
//    if($('.input').val()){
//        console.log("xcvxcv");
//          $('.label-txt').addClass('labve');
//    }else{
//         $('.label-txt').addClass('labve');
//    }
//});


    $(".file-input-add").on("change", function(){
        //$("#file-uploadNameAdd").show();
        var filename = $(this).val().replace(/C:\\fakepath\\/i, ''); 
        $("#file-uploadNameAdd span").text(filename);
   
});

$(".file-input-edit").on("change", function(){
  $("#file-uploadNameEdit").show();
  var filename = $(this).val().replace(/C:\\fakepath\\/i, '');  
  $("#file-uploadNameEdit span").text(filename);
});
$(".addderequesr").click(function(){
$("#addCatForm .label-txt").addClass("label-active");
});

</script>
<!--  script for overall page   ui-state-active -->
<script type="text/javascript">

//     $(document).ready(function(){

//         setTimeout(function(){   
//             var dateRange1 = parseInt($("#dateRange1").data("day"), 10);
//             var dateRange2 = parseInt($("#dateRange2").data("day"), 10);
//                if($(".ui-state-default").hasClass("ui-state-highlight")){
//     $(".ui-state-default").removeClass("ui-state-highlight ui-state-active");
//     }
            
//             $('.ui-datepicker-calendar > tbody  > tr >td').each(function(index, tr) { 
//            console.log("index",index); 
//            //if($(this).find("a").text() == dateRange1 ){
//             if(index == dateRange2 ){
//                 return false; 
//             }else{
//                 $(this).find("a").addClass("ui-state-active");
//                 if($(this).find("a").text() == dateRange1-1 ){
//                     $(this).find("a").removeClass("ui-state-active");
//                 }
                
             
//           }
//          // }
//         })

//     }, 1000);
// });
// $(function() {
//     var startDate;
//     var endDate;

    
//     var selectCurrentWeek = function() {
//         window.setTimeout(function () {
//              $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
//         }, 1);
//     }
    
//     $('.week-picker').datepicker( {
//         showOtherMonths: true,
//         selectOtherMonths: true,

//         onSelect: function(dateText, inst) { 
//             var date = $(this).datepicker('getDate');
//             startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
//             endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
//             var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
//             $('#dateRange1').text($.datepicker.formatDate("M dd", startDate, inst.settings ));
//             $('#dateRange2').text($.datepicker.formatDate( "M dd", endDate, inst.settings ));
//             var start = $.datepicker.formatDate("yy-m-dd", startDate, inst.settings );
//             var end = $.datepicker.formatDate( "yy-m-dd", endDate, inst.settings );

//             $('#dateRange1').attr("data-start",start);
//             $('#dateRange2').attr("data-end",end);
//              AjaXForGettingEntries(start,end);
//             $(".week-picker").addClass("hide"); 
            
//             selectCurrentWeek();
//         },
//         beforeShowDay: function(date) {
//             var cssClass = '';
//             if(date >= startDate && date <= endDate)
//                 cssClass = 'ui-datepicker-current-day';
//             return [true, cssClass];
//         },

//         onChangeMonthYear: function(year, month, inst) {
//             selectCurrentWeek();
//         },
       
//     });
    
     
// });
// $("#datetimepicker1").click(function(){
//   $(".week-picker").toggleClass("hide");
// });

   

// $(document).mouseup(function(e) 
// {
//     var container = $(".week-picker");
//     // if the target of the click isn't the container nor a descendant of the container
//     if (!container.is(e.target) && container.has(e.target).length === 0) 
//     {
//         container.addClass('hide');
//     }
// });




$(".change_designer").click(function(){
    $("#assign_customer_id").val($(this).attr("data-id")); 
}); 


  $(".switch-custom label").on("click",function(){
    var usr_id= $(this).parent().find(".closecancel").attr('data-cid'); 
     if($(this).parent().find(".closecancel").attr('checked')){
      $("#reason_box_popup").modal("show"); 
      $("#reason_box").attr("data-id",usr_id)

     }
    });
   $("#confirm_close").click(function(){
    var value,usr_id; 
    value= $("#reason_box").text();
    usr_id = $("#reason_box").attr("data-id");
    ajax_forUpdating(usr_id,value)
   });
  function ajax_forUpdating(usr_id,reason_for_close){

    if(reason_for_close){
        reason_for_close = reason_for_close;
    }else{
        reason_for_close = ""; 
    }
    $.ajax({
        type: 'POST',
        url: baseUrl+"admin/contentmanagement/ajax_forUpdating",
        data: {"customer_id": usr_id,"reason_for_close" : reason_for_close},
        dataType: "JSON",
        success: function (res) {
             if(res.status==1){
                 swal("success", "Designer has been changed", "success"); 
                 location.reload();

             }else{
                 swal("oops!", "There is something wrong", "error"); 
             }
        }
    });
  }
</script>

            
    <script type="text/javascript">
      $(document).ready(function() {
        $(".btn-pref .btn").click(function () {
        $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).removeClass("btn-default").addClass("btn-primary");   
  });
      });

 setTimeout(function(){
    $(".alert").fadeOut(400);
}, 5000)

</script>

<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
<script type="text/javascript">CKEDITOR.replace( 'HighLights' );

$(".meta_save").click(function(){
$('#HighLights').text($("#HighLights").val()); 
});



  $(".close-img").click(function(){
        $this = $(this); 
        // console.log("asdjg"); 
       var imgid =  $(this).attr("data-img-id");
       var img_src =  $(this).parent().find("img").attr("src");
       var img_folder =  $(this).attr("data-folder");
       var db_field =  $(this).attr("data-db");
       var filename = img_src.substring(img_src.lastIndexOf('/')+1);
         $.ajax({
        type: 'POST',
        url: "<?php echo base_url();?>admin/contentmanagement/delete_seo_images",
        data: {"imgid": imgid,"img_src":img_src,"folder_path":img_folder,"db_field":db_field},
        dataType: "JSON",
        success: function (res) { 
            if(res){
             
                $this.parents(".samples-designer").fadeOut();
                $(".alert-success").show();
                $(".alert-success").find("strong").text(filename+"Deleted Successfully");
            }else{
                $(".alert-danger").show();
                $(".alert-danger").find("strong").text(filename+" not deleted");
            }
            // setTimeout(function(){  location.reload(); }, 2000);
        }

           
    });

    }); 
   
 
 $(document).ready(function() {
   $(".view_sim_design").parent("a").fancybox();
 });


 //  Project design script Start from  here 
 $(document).ready(function() {
   $("#load_more_projects").click(function(e){
    $this = $(this); 
    var data_load= $this.attr("data-load");
    var data_append= $this.attr("data-append");
    var datatosend  = {"data_load": data_load, "data_append":data_append};  
    var ajaxurl =  baseUrl+"admin/contentmanagement/project_designs/1"; 

    if($("#is_filter").val() == 1){
      var client_id = $("#client_id").val();
      var designerid = $("#choose-designer").val();
      var categoryid = $("#choose-category").val();
      var subcategoryid = $("#choose-sub-category").val();
      var approve_by_client = "";
      if($("#approve_by_client").prop('checked') == true){
        var approve_by_client = "1";
      } 
      var datefrom  = $("#date-from").val();
      var dateto = $("#date-to").val(); 
      $("#is_filter").val(1);
      var limit = $("#is_filter").attr("data-append"); 
      var start = $("#is_filter").attr("data-load"); 
      var datatosend  = {"client_id": client_id,"designerid" : designerid,"categoryid":categoryid,"subcategoryid":subcategoryid,"approve_by_client":approve_by_client,"datefrom":datefrom,"dateto":dateto,"limit":limit,"start":start};  
      var ajaxurl =  baseUrl+"admin/contentmanagement/project_designs/3";
      ajax_for_projectDesigns(datatosend, ajaxurl,$this,".ajax_loader","1");
    }else{
       ajax_for_projectDesigns(datatosend, ajaxurl,$this,".ajax_searchload","1");
    }
   });                   
 });  

 $("#filter_apply").click(function(){
   $this = $(this); 
  var client_id = $("#client_id").val();
  var designerid = $("#choose-designer").val();
  var categoryid = $("#choose-category").val();
  var subcategoryid = $("#choose-sub-category").val();
  var approve_by_client = "";
  if($("#approve_by_client").prop('checked') == true){
    var approve_by_client = "1";
  } 
  var datefrom  = $("#date-from").val();
  var dateto = $("#date-to").val(); 
  $("#is_filter").val(1);
  var limit = $("#is_filter").attr("data-limit"); 
  var start = $("#is_filter").attr("data-start"); 
  var datatosend  = {"client_id": client_id,"designerid" : designerid,"categoryid":categoryid,"subcategoryid":subcategoryid,"approve_by_client":approve_by_client,"datefrom":datefrom,"dateto":dateto,"limit":limit,"start":start};  
  var ajaxurl =  baseUrl+"admin/contentmanagement/project_designs/3";
  $("#close-filter").trigger('click');  
  ajax_for_projectDesigns(datatosend, ajaxurl,$this,".ajax_loader","3");
});                      

   $(document).on("click", ".show_action", function () {
      $this = $(this); 
      $this.next().slideToggle(); 
  });
     $(document).on("click", ".slide-close", function () {
      $this = $(this); 
      $this.parent(".shown_grid_action").slideToggle();
  }); 


$("#search_client_filter").on("keyup",function(){
       var search_key = $(this).val();
       var loader = '<div class="ajax_searchload text-center"><img src="'+baseUrl+'public/assets/img/ajax-loader.gif"></div>';
       $("#suggesstion-box-filter ul").html(loader);
       setTimeout(function(){  
        if(search_key!=""){

          ajax_search_filter(search_key);
        }else{
          $("#client_id").removeAttr("value"); 
          $("#suggesstion-box-filter").hide(); 
        }
      },3000); 

});

$(document).on("click",".search_result li",function(){
  $this = $(this); 
  $("#search_client_filter").val($this.text()); 
  $("#client_id").val($this.val()); 
  $("#suggesstion-box-filter").fadeOut(); 
});
 function ajax_for_projectDesigns(datatosend, ajaxurl,$this,loaderclass="",datashow=""){
$(loaderclass).show();
   var extensions  =  ['jpg', 'JPG', 'png' ,'PNG' ,'jpeg' ,'JPEG','gif','GIF','bmp','BMP']; 
   var html = ''; 
   var downloadurl = ''; 
  $.ajax({
    type: 'POST',
    url: ajaxurl,
    data: datatosend,
    dataType: "JSON",
    success: function (res) {
      $(loaderclass).hide();
      if(res!=false){
         if($("#is_filter").val() == 1){
          var data_load=   $("#is_filter").attr("data-load");
          var data_append= $("#is_filter").attr("data-append");
          data_load = parseInt(data_load)+ parseInt(data_append);
          $("#is_filter").attr('data-load',data_load);
         }else{
           var data_load= $this.attr("data-load");
           var data_append= $this.attr("data-append");
           data_load = parseInt(data_load)+ parseInt(data_append);
           $this.attr('data-load',data_load);
         }
      
       
        $(".close-filter").trigger("click"); 
        var lazy =  $asset_path+'img/lazy.png'; 
        $.each(res, function (k, v) {
          var ext = v.file_name.split('.').pop();
          if(jQuery.inArray(ext, extensions) != -1) {
            url = $request_path+v.rqid+'/_thumb/'+v.file_name;
            downloadurl = $request_path+v.rqid+'/'+v.file_name;
          } else {
            downloadurl = $request_path+v.rqid+'/'+v.file_name;
            url = baseUrl+'public/assets/img/gz_icons/gz_project_thumbnail.svg';
          } 
          $source_url = $request_path+v.rqid+'/'+v.src_file;
          $project_url = baseUrl+'admin/dashboard/view_request/'+v.rqid;
          $preview_url = baseUrl+'admin/dashboard/view_files/'+v.id+'?id='+v.rqid;



          html += '<div class="col-md-2"><div class="images_grid_img"> <a rel="gallery1"  data-fancybox="images" class="fancybox" href="'+url+'"><img   class="lazy" src="'+lazy+'" data-original="'+url+'"></a><div class="grid_action"><button class="show_action_change_asj" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button> <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item" href="'+baseUrl+'admin/contentmanagement/donwload_data?url='+downloadurl+'" download> <i class="fa fa-download" aria-hidden="true"></i> Download Preview </a> <a href="'+baseUrl+'admin/contentmanagement/donwload_data?url='+$source_url+'" class="dropdown-item" download> <i class="fa fa-download" aria-hidden="true"></i> Download Source </a> <a class="dropdown-item" href="'+$project_url+'" target="_blank"> <i class="fa fa-link"></i>GO to Project </a> <a class="dropdown-item" href="'+$preview_url+'" target="_blank"> <i class="fa fa-link"></i>Go to Design </a> </div></div></div></div>';
        }); 
        $("#load_more_projects").show();

      }else{
        if(datashow==1){
          $("#load_more_projects").fadeOut(); 
        }else{
          $("#load_more_projects").hide();
         html += '<div class="no-data-found text-center"><p>There is no Data found for your filters</p></div>'; 
       }
      }
      
      if(datashow==1){
          
          $("#images-grid").append(html);
        }else if(datashow == 3){
         $("#images-grid").html(html);
          
        }
      setTimeout(function(){  
        $("img.lazy").lazyload({
         effect : "fadeIn"
       });
        // $(".lazy").parent("a").fancybox(); 
      }, 1500);

    }
  });
 }

 function ajax_search_filter(search_key){
  
   var html =''; 
   $.ajax({
    type: 'POST',
    url: baseUrl+"admin/contentmanagement/project_designs/2",
    data: {"search_key": search_key},
    dataType: "JSON",
    success: function (res) {
      $("#suggesstion-box-filter").fadeIn(); 
      $.each(res, function (k, v) { 
        html+='<li value='+v.id+'>'+v.name+'</li>';
      }); 
      $("#suggesstion-box-filter ul").html(html) 
    }
  });
 }
$(document).ready(function(){

 $("img.lazy").lazyload({
       effect : "fadeIn"
     });
    // $(".lazy").parent("a").fancybox({
    //    showNavArrows: true,
    // });
});
</script>
<style type="text/css">
 
</style>
</body>
</html>
