<?php //echo "<pre/>";print_r($sub_user_permsion[0]);  ?>
<style>
div#brandcheck_chosen {
width: 100% !important;
}
div#brandcheck_chosen ul.chosen-choices {
width: 100%;
border: 1px solid #ccc;
color: #757575;
height: 70px;
box-shadow: none;
font-size: 15px;
padding: 20px;
background: #fff;
outline: none;
border-radius: 6px;
font: normal 16px/20px 'GothamPro-Medium', sans-serif;
}

a.backlist {
    background: #1b1d8a;
    color: #fff;
    padding: 16px 0;
    width: 170px;
    display: inline-block;
    font-size: 15px;
    text-align: center;
    border: 1px solid #1b1d8a;
    text-align: center;
    border-radius: 5px;
   
}
</style>
<section class="con-b">
    <div class="container">
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_error'); ?></p>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_success'); ?></ps>
            </div>
        <?php } ?>
        <div class="col-md-12">
            <div class="edit-submt">
                <form action="" method="post" role="form" class="sub_user_form" id="sub_user_sbmt">
                    <div class="row">
                        <?php if (!empty($brandprofile)) { ?>
                            <div class="col-md-12">
                                <div class="access-brand">
                                    <div class="col-md-6">
                                        <div class="notify-lines finish-line">
                                            <h3>Access All brands</h3>
                                            <label class="switch-custom-usersetting-check">
                                                <div class="switch-custom-usersetting-check">
                                                    <input type="checkbox" name="access_brand_pro" id="switch_access"  <?php echo (!empty($selectedbrandIDs)) ? '' : 'checked'; ?>/>
                                                    <label for="switch_access"></label> 
                                                </div>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-6" id="brandshowing">

                                        <select name="brandids[]" id="brandcheck"  class="chosen-select" style='display:none' data-live-search="true" multiple>
                                            <?php ?>
                                            <option value="" disabled>Select Brand</option>
                                            <?php
                                            foreach ($brandprofile as $brand) {
                                                ?> 
                                                <option value="<?php echo $brand['id'] ?>" <?php echo in_array($brand['id'], $selectedbrandIDs) ? 'selected' : '' ?>><?php echo $brand['brand_name'] ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="first_name" class="form-control" id="fname" placeholder="First Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="<?php echo isset($edit_sub_user[0]['first_name']) ? $edit_sub_user[0]['first_name'] : ""; ?>" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="last_name" class="form-control" id="lname" placeholder="Last Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="<?php echo isset($edit_sub_user[0]['last_name']) ? $edit_sub_user[0]['last_name'] : ""; ?>" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="email" class="form-control" name="email" id="user_email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" value="<?php echo isset($edit_sub_user[0]['email']) ? $edit_sub_user[0]['email'] : ""; ?>" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="tel" name="phone" placeholder="Enter Phone Number" id="phone" value="<?php echo isset($edit_sub_user[0]['phone']) ? $edit_sub_user[0]['phone'] : ""; ?>"required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" name="new_password" placeholder="Enter New Password" id="new_pass">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" name="confirm_password" placeholder="Enter Confirm Password" id="cnf_pass">
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-6">
                            <div class="notify-lines finish-line">
                                <h3>View Only</h3>
                                <label class="switch-custom-usersetting-check">
                                    <input type="checkbox" name="view_only" class="form-control" id="view_only" <?php echo ($sub_user_permsion[0]['view_only'] == 1) ? "checked" : ""; ?>>
                                    <label for="view_only"></label> 
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group">

                            <div class="col-md-6">
                                <div class="notify-lines">
                                    <p>Add Request</p>
                                    <label class="switch-custom-usersetting-check uncheckview">
                                        <input type="checkbox" name="add_requests" class="form-control" id="add_requests" <?php echo ($sub_user_permsion[0]['add_requests'] == 1) ? "checked" : ""; ?>>
                                        <label for="add_requests"></label> 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="notify-lines">
                                    <p>Comment on Request</p>
                                    <label class="switch-custom-usersetting-check uncheckview">
                                        <input type="checkbox" name="comnt_requests" class="form-control" id="comnt_requests" <?php echo ($sub_user_permsion[0]['comment_on_req'] == 1) ? "checked" : ""; ?>>
                                        <label for="comnt_requests"></label> 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="notify-lines">
                                    <p>Delete Request</p>
                                    <label class="switch-custom-usersetting-check uncheckview">
                                        <input type="checkbox" name="del_requests" class="form-control" id="del_requests" <?php echo ($sub_user_permsion[0]['delete_req'] == 1) ? "checked" : ""; ?>>
                                        <label for="del_requests"></label> 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="notify-lines">
                                    <p>Billing Module</p>
                                    <label class="switch-custom-usersetting-check uncheckview">
                                        <input type="checkbox" name="billing_module" class="form-control" id="billing_module" <?php echo ($sub_user_permsion[0]['billing_module'] == 1) ? "checked" : ""; ?>>
                                        <label for="billing_module"></label> 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="notify-lines">
                                    <p>Approve/Revision Request</p>
                                    <label class="switch-custom-usersetting-check uncheckview">
                                        <input type="checkbox" name="app_requests" class="form-control" id="app_requests" <?php echo ($sub_user_permsion[0]['approve/revision_requests'] == 1) ? "checked" : ""; ?>>
                                        <label for="app_requests"></label> 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="notify-lines">
                                    <p>Add/Edit Brand Profile</p>
                                    <label class="switch-custom-usersetting-check uncheckview">
                                        <input type="checkbox" name="add_brand_pro" class="form-control" id="add_brand_pro" <?php echo ($sub_user_permsion[0]['add_brand_pro'] == 1) ? "checked" : ""; ?>>
                                        <label for="add_brand_pro"></label> 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="notify-lines ">
                                    <p>Download File</p>
                                    <label class="switch-custom-usersetting-check uncheckview">
                                        <input type="checkbox" name="downld_requests" class="form-control" id="downld_requests" <?php echo ($sub_user_permsion[0]['download_file'] == 1) ? "checked" : ""; ?>>
                                        <label for="downld_requests"></label> 
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="btn-here">
                                    <input type="submit" name="save_subuser" class="btn-red" value="SAVE">
                                     <a href="<?php echo base_url();?>/admin/dashboard/sub_user/<?php echo $cust_id; ?>" class="backlist">Cancel</a>
                                </div>
                            </div>


                            </form>
                        </div>
                    </div>
            </div>
        </div></div>
</section>
<link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/chosen.css');?>" rel="stylesheet">
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/chosen.jquery.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/init.js');?>"></script>
<script>
$(document).ready(function () {
    function brandsShow(is_checked) {
        if (is_checked == false) {
            $('#brandshowing').css('display', 'block');
        } else {
            $('#brandshowing').css('display', 'none');
        }
    }
    
$(document).ready(function () {
    var is_checked1 = $('#switch_access').is(":checked");
    brandsShow(is_checked1);
    $(document).on('change', '#switch_access', function () {
        var is_checked = $('#switch_access').is(":checked");
        brandsShow(is_checked)
    });
});
});

$(document).on('change', '#view_only', function () {
    var is_view = $('#view_only').is(":checked");
    if (is_view == true) {
        $('#add_requests').attr('checked', false);
        $('#comnt_requests').attr('checked', false);
        $('#del_requests').attr('checked', false);
        $('#billing_module').attr('checked', false);
        $('#app_requests').attr('checked', false);
        $('#downld_requests').attr('checked', false);
        $('#add_brand_pro').attr('checked', false);
    }
});

$(document).on('change', '.uncheckview', function () {
    var is_view = $('#view_only').is(":checked");
    if(is_view == true){
        $('#view_only').attr('checked', false);
    }
});

</script>