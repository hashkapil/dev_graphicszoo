<section id="content-wrapper">

    <div class="site-content-title">
        <ol class="breadcrumb float-xs-right">
            <li class="breadcrumb-item">
                <span class="fs1" aria-hidden="true" data-icon="?"></span>
                <a href="#">Main Menu</a>
            </li>
            <li class="breadcrumb-item active">View Users</li>
        </ol>
    </div>
    <div class="content">
		<h2 class="float-xs-left content-title-main">All Requests</h2>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">                
                <div class="nav-tab-pills-image">
                    <ul class="nav nav-tabs" role="tablist">                      
                        <li class="nav-item active">
                            <a class="nav-link" data-toggle="tab" href="#designs_request_tab" role="tab">
                                <i class="icon icon_cog"></i>In Progress And Revision Requests(<?= $total_unread_message ?>)
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#inprogressrequest" role="tab">
                                <i class="icon icon_cog"></i>In Queue Requests(<?= $total_assign_message ?>)
                            </a>
                        </li>
						 <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#pendingforapproverequest" role="tab">
                                <i class="icon icon_cog"></i>Pending Approval Requests(<?= $total_checkforapprove_message ?>)
                            </a>
                        </li>
						 <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#approved_designs_tab" role="tab">
                                <i class="icon icon_cog"></i>Approved Requests(<?= $total_unread_message2 ?>)
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Design Status</th>
												
												<th>Customer Name</th>
												<th>Designer Name</th>
												<th>Title</th>
                                                <th>Requested</th>
												<th>In Progress</th>
												<th>Due Date</th>
												<th>Messages(<?= $total_unread_message ?>)</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											for ($i=0;$i<sizeof($requested_designs);$i++){ ?>
											<?php
											
												$duedate="";
												$bgcolor = "";
												$txtcolor = "";
												$status="";
												$button = "";
												if($requested_designs[$i]['status_admin'] == "active"){
													$duedate = "";
													$status = "In Progress";
													$bgcolor="";
													$txtcolor = "";
													 $button="color: #f7941e !important;    margin: auto;    border-radius: 20px;";
													 
												}elseif($requested_designs[$i]['status_admin'] == "pending"){
													$duedate = "--";
													$bgcolor = "";
													$txtcolor = "";
													$status = "Pending";
													$button="color:#469202 !important;    margin: auto;    border-radius: 20px;"; 
												}elseif($requested_designs[$i]['status_admin'] == "running"){
													$duedate = "--";
													$bgcolor = "";
													$txtcolor = "";
													$status = "In Queue";
													$button="color:#d87a08 !important;    margin: auto;    border-radius: 20px;";
												}elseif($requested_designs[$i]['status_admin'] == "disapprove"){
													$duedate = "--";
													$status = "Revision ";
													$bgcolor = "coral";
													$txtcolor = "#000";
													$button="color: rgb(255, 255, 255);    margin: auto;    border-radius: 20px;background: coral;";
												}elseif($requested_designs[$i]['status_admin'] == "pendingforapprove"){
													$duedate = "--";
													$status = "Waiting for Approval";
													$bgcolor="";
													$txtcolor = "";
													$button="color: #52a447 !important;    margin: auto;    border-radius: 20px;background: ;";
												}elseif($requested_designs[$i]['status_admin'] == "checkforapprove"){
													$duedate = "--";
													$status = "Check for Approval";
													$bgcolor="lightyellow";
													$txtcolor = "";
													 $button="color: #ec1c41 !important;    margin: auto;    border-radius: 20px;background: lightyellow;";
												}elseif($requested_designs[$i]['status_admin'] == "assign"){
													$duedate = "--";
													$status = "In Queue";
													$bgcolor="";
													$txtcolor = "";
													$button="color: #d87a08 !important;    margin: auto;    border-radius: 20px;background: ;";
												}else{
													$duedate = "";
													$status = "";
													$bgcolor="";
													$txtcolor = "";
													$button="";
												}
												?>
                                                <tr style="background:<?php //echo $bgcolor; ?>;" >
                                                    <td style="color:<?php echo $txtcolor; ?>;"><?= $i+1 ?></td>
													<td style="color:<?php echo $txtcolor; ?>;">
													<?php if($button){ ?>
														<button class="btn btn-deafult" style="<?= $button ?>"><?= $status ?></button>
													<?php } ?>
													</td>
													
													<td style="color:<?php echo $txtcolor; ?>;"><span style="color: blue;background:<?php if(isset($customerarray[$requested_designs[$i]['customer_id']])) echo $customerarray[$requested_designs[$i]['customer_id']]['current_plan_color']; ?>;border-radius: 50%;width: 7px;border: 0px;height:7px;display:block;margin:auto;text-align:center;">&nbsp;</span></br><?= ($requested_designs[$i]['customer']) ? $requested_designs[$i]['customer'][0]['first_name'] . ' ' . $requested_designs[$i]['customer'][0]['last_name'] : '' ?></td>
													
													<td style="color:<?php echo $txtcolor; ?>;"><?= ($requested_designs[$i]['designer']) ? $requested_designs[$i]['designer'][0]['first_name'] . ' ' . $requested_designs[$i]['designer'][0]['last_name'] : '' ?></td>
													
													 <td style="color:<?php echo $txtcolor; ?>;"><?= $requested_designs[$i]['title'] ?></td>
													 <td  style="color:<?php echo $txtcolor; ?>;"><?= ($requested_designs[$i]['created']) ? str_replace("-"," ",str_replace(" ","<br>",date("m/d/Y g:i-a",strtotime($requested_designs[$i]['created'])))) : ''; ?></td>
													 
													<td  style="color:<?php echo $txtcolor; ?>;"><?= ($requested_designs[$i]['dateinprogress']) ?  str_replace("-"," ",str_replace(" ","<br>",date("m/d/Y g:i-a",strtotime($requested_designs[$i]['dateinprogress'])))) : ''; ?></td>
													
													<td style="color:<?php echo $txtcolor; ?>;">
													<?php
														if($requested_designs[$i]['dateinprogress'] != ""){
															if(!empty($requested_designs[$i]['customer']['plan_turn_around_days']) && isset($requested_designs[$i]['customer'])){

																$duedate = strtotime($requested_designs[$i]['dateinprogress']) + 60*60*24*$requested_designs[$i]['customer']['plan_turn_around_days'];		
																$date = date("Y-m-d",strtotime($requested_designs[$i]['dateinprogress']));
																$time = date("h:i:s",strtotime($requested_designs[$i]['dateinprogress']));
																
															   if($requested_designs[$i]['status_admin'] == "disapprove"){ $requested_designs[$i]['customer']['plan_turn_around_days'] = $requested_designs[$i]['customer']['plan_turn_around_days']+1; }
																echo date("m/d/Y g:i a",strtotime($date." ".$requested_designs[$i]['customer']['plan_turn_around_days']." weekdays ".$time));
															}
														}
													?>
													</td> 
													<td style="color:<?php echo $txtcolor; ?>;"><?php if($admin_unread_message_count_array[$requested_designs[$i]['id']]!=0) { ?>
													<button class="btn btn-msg" style="border-radius:20px;background:;border:none;">
													<?= $admin_unread_message_count_array[$requested_designs[$i]['id']] ?></button> 
													<?php } ?></td>
                                                    
													
													<td style="color:<?php echo $txtcolor; ?>;">
                                                        <a href="<?php echo base_url(); ?>admin/view_request/<?php echo $requested_designs[$i]['id']; ?>">
															 <button class="btn-view btn-primary"><i class="fa fa-eye" aria-hidden="true"></i>view</button> 
                                                        </a>
                                                        <?php
														//echo $this->Form->postLink('<button class="btn-cancel btn-danger"><i class="fa fa-times" style="margin-right: 5px;font-size: 15px;"></i></button>', Router::url(['controller' => 'Requests', 'action' => 'cancleRequest', $requested_designs->id], TRUE), ['escape' => FALSE, 'confirm' => 'Are you sure want to delete?']);
                                                        ?>
													</td>

                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
												<th>Design Status</th>
												<th>Customer Name</th> 
												<th>Designer Name</th>
												<th>Title</th>
                                                <th>Date Requested</th>
												<th>Messages(<?= $total_assign_message ?>)</th>
												<th>Action</th>
                                                                           
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											for ($i=0;$i<sizeof($inprogressrequest);$i++){ ?>
												<?php
												$duedate="";
												$bgcolor = "";
												$txtcolor = "";
												$status="";
												$button = "";
												if($inprogressrequest[$i]['status_admin'] == "active"){
													$duedate = "";
													$status = "In Progress";
													$bgcolor="";
													$txtcolor = "";
													 $button="color: #f7941e !important;    margin: auto;    border-radius: 20px;";
													 
												}elseif($inprogressrequest[$i]['status_admin'] == "pending"){
													$duedate = "--";
													$bgcolor = "";
													$txtcolor = "";
													$status = "Pending";
													$button="color:#469202 !important;    margin: auto;    border-radius: 20px;"; 
												}elseif($inprogressrequest[$i]['status_admin'] == "running"){
													$duedate = "--";
													$bgcolor = "";
													$txtcolor = "";
													$status = "In Queue";
													$button="color:#d87a08 !important;    margin: auto;    border-radius: 20px;";
												}elseif($inprogressrequest[$i]['status_admin'] == "disapprove"){
													$duedate = "--";
													$status = "Revision ";
													$bgcolor = "coral";
													$txtcolor = "#000";
													$button="color: rgb(255, 255, 255);    margin: auto;    border-radius: 20px;background: coral;";
												}elseif($inprogressrequest[$i]['status_admin'] == "pendingforapprove"){
													$duedate = "--";
													$status = "Waiting for Approval";
													$bgcolor="";
													$txtcolor = "";
													$button="color: #52a447 !important;    margin: auto;    border-radius: 20px;background: ;";
												}elseif($inprogressrequest[$i]['status_admin'] == "checkforapprove"){
													$duedate = "--";
													$status = "Check for Approval";
													$bgcolor="lightyellow";
													$txtcolor = "";
													 $button="color: #ec1c41 !important;    margin: auto;    border-radius: 20px;background: lightyellow;";
												}elseif($inprogressrequest[$i]['status_admin'] == "assign"){
													$duedate = "--";
													$status = "In Queue";
													$bgcolor="";
													$txtcolor = "";
													$button="color: #d87a08 !important;    margin: auto;    border-radius: 20px;background: ;";
												}else{
													$duedate = "";
													$status = "";
													$bgcolor="";
													$txtcolor = "";
													$button="";
												}
											
												?>
                                                <tr style="background:<?php echo $bgcolor; ?>;" >
                                                    <td style="color:<?php echo $txtcolor; ?>;"><?= $i ?></td>
													<td style="color:<?php echo $txtcolor; ?>;">
													<?php if($button){ ?>
														<button class="btn btn-deafult" style="<?= $button ?>"><?= $status ?></button>
													<?php } ?>
													</td>
													
													<td style="color:<?php echo $txtcolor; ?>;"><span style="color: blue;background:<?php if(isset($customerarray[$inprogressrequest[$i]['customer_id']])) echo $customerarray[$inprogressrequest[$i]['customer_id']]['current_plan_color']; ?>;border-radius: 50%;width: 7px;border: 0px;height:7px;display:block;margin:auto;text-align:center;">&nbsp;</span></br><?= ($inprogressrequest[$i]['customer']) ? $inprogressrequest[$i]['customer'][0]['first_name'] . ' ' . $inprogressrequest[$i]['customer'][0]['last_name'] : '' ?></td>
													
													<td style="color:<?php echo $txtcolor; ?>;"><?= ($inprogressrequest[$i]['designer']) ? $inprogressrequest[$i]['designer'][0]['first_name'] . ' ' . $inprogressrequest[$i][0]['designer']['last_name'] : '' ?></td>
													
                                                     <td style="color:<?php echo $txtcolor; ?>;"><?= $inprogressrequest[$i]['title'] ?></td>
													 <td  style="color:<?php echo $txtcolor; ?>;"><?= ($inprogressrequest[$i]['created']) ? str_replace("-"," ",str_replace(" ","<br>",date("m/d/Y g:i-a",strtotime($inprogressrequest[$i]['created'])))) : ''; ?></td>
													 
													
													<td style="color:<?php echo $txtcolor; ?>;"><?php if(isset($admin_unread_message_count_array[$inprogressrequest[$i]['id']]) && $admin_unread_message_count_array[$inprogressrequest[$i]['id']]!=0) { ?>
													<button class="btn btn-msg" style="border-radius:20px;border:none;">
													<?= $admin_unread_message_count_array[$inprogressrequest[$i]['id']] ?></button> 
													<?php } ?></td> 
                                                    <td style="display: inline-flex;color:<?php echo $txtcolor; ?>;">
                                                        <a href="">
															<button class="btn-view btn-primary"><i class="fa fa-eye" aria-hidden="true"></i>view</button> 
                                                        </a>
                                                        <?php
														//echo $this->Form->postLink('<button class="btn-cancel btn-danger"><i class="fa fa-times" style="margin-right: 5px;font-size: 15px;"></i></button>', Router::url(['controller' => 'Requests', 'action' => 'cancleRequest', $request->id], TRUE), ['escape' => FALSE, 'confirm' => 'Are you sure want to delete?']);
                                                        ?>
                                                    </td>

                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="pendingforapproverequest" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">						
                                        <thead>
                                            <tr>
                                                <th>Id</th>
												<th>Design Status</th>
												<th>Customer Name</th> 
												<th>Designer Name</th>
                                                <th>Title</th>                                               
                                                <th>Date Requested</th>
												<th>Date In Progress</th>												
												<th>Due Date</th>  
												<th>Messages(<?= $total_checkforapprove_message ?>)</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php  
											for ($i=0;$i<sizeof($checkforapproverequests);$i++){ ?>
												<?php
												$duedate="";
												$bgcolor = "";
												$txtcolor = "";
												$status="";
												$button = "";
												if($checkforapproverequests[$i]['status_admin'] == "active"){
													$duedate = "";
													$status = "In Progress";
													$bgcolor="";
													$txtcolor = "";
													 $button="color: #f7941e !important;    margin: auto;    border-radius: 20px;";
													 
												}elseif($checkforapproverequests[$i]['status_admin'] == "pending"){
													$duedate = "--";
													$bgcolor = "";
													$txtcolor = "";
													$status = "Pending";
													$button="color:#469202 !important;    margin: auto;    border-radius: 20px;"; 
												}elseif($checkforapproverequests[$i]['status_admin'] == "running"){
													$duedate = "--";
													$bgcolor = "";
													$txtcolor = "";
													$status = "In Queue";
													$button="color:#d87a08 !important;    margin: auto;    border-radius: 20px;";
												}elseif($checkforapproverequests[$i]['status_admin'] == "disapprove"){
													$duedate = "--";
													$status = "Revision ";
													$bgcolor = "coral";
													$txtcolor = "#000";
													$button="color: rgb(255, 255, 255);    margin: auto;    border-radius: 20px;background: coral;";
												}elseif($checkforapproverequests[$i]['status_admin'] == "pendingforapprove"){
													$duedate = "--";
													$status = "Waiting for Approval";
													$bgcolor="";
													$txtcolor = "";
													$button="color: #52a447 !important;    margin: auto;    border-radius: 20px;background: ;";
												}elseif($checkforapproverequests[$i]['status_admin'] == "checkforapprove"){
													$duedate = "--";
													$status = "Check for Approval";
													$bgcolor="lightyellow";
													$txtcolor = "";
													 $button="color: #ec1c41 !important;    margin: auto;    border-radius: 20px;background: lightyellow;";
												}elseif($checkforapproverequests[$i]['status_admin'] == "assign"){
													$duedate = "--";
													$status = "In Queue";
													$bgcolor="";
													$txtcolor = "";
													$button="color: #d87a08 !important;    margin: auto;    border-radius: 20px;background: ;";
												}else{
													$duedate = "";
													$status = "";
													$bgcolor="";
													$txtcolor = "";
													$button="";
												}
											
												?>
                                                <tr style="background:<?php echo $bgcolor; ?>;" >
                                                    <td style="color:<?php echo $txtcolor; ?>;"><?= $i ?></td>
													<td style="color:<?php echo $txtcolor; ?>;">
													<?php if($button){ ?>
														<button class="btn btn-deafult" style="<?= $button ?>"><?= $status ?></button>
													<?php } ?>
													</td>
													<td style="color:<?php echo $txtcolor; ?>;">
														<span style="color: blue;background:<?php 
															if(isset($customerarray[$checkforapproverequests[$i]['customer_id']]['current_plan_color'])){
																echo $customerarray[$checkforapproverequests[$i]['customer_id']]['current_plan_color']; 
															}
															?>;border-radius: 50%;width: 7px;border: 0px;height:7px;display:block;margin:auto;text-align:center;">&nbsp;</span></br><?= ($checkforapproverequests[$i]['customer']) ? $checkforapproverequests[$i]['customer'][0]['first_name'] . ' ' . $checkforapproverequests[$i]['customer'][0]['last_name'] : '' ?></td>
													
													<td style="color:<?php echo $txtcolor; ?>;"><?= ($checkforapproverequests[$i]['designer']) ? $checkforapproverequests[$i]['designer'][0]['first_name'] . ' ' . $checkforapproverequests[$i]['designer'][0]['last_name'] : '' ?></td>
													
													 <td style="color:<?php echo $txtcolor; ?>;"><?= $checkforapproverequests[$i]['title'] ?></td>
													 <td  style="color:<?php echo $txtcolor; ?>;"><?= ($checkforapproverequests[$i]['created']) ? str_replace("-"," ",str_replace(" ","<br>",date("m/d/Y g:i-a",strtotime($checkforapproverequests[$i]['created'])))) : ''; ?></td>
													 
													<td  style="color:<?php echo $txtcolor; ?>;"><?= ($checkforapproverequests[$i]['dateinprogress']) ?  str_replace("-"," ",str_replace(" ","<br>",date("m/d/Y g:i-a",strtotime($checkforapproverequests[$i]['dateinprogress'])))) : ''; ?></td>
													
													<td style="color:<?php echo $txtcolor; ?>;">
													<?php
														if($checkforapproverequests[$i]['dateinprogress'] != ""){
															if(!empty($checkforapproverequests[$i]['customer']['plan_turn_around_days']) && isset($checkforapproverequests[$i]['customer'])){

																$duedate = strtotime($checkforapproverequests[$i]['dateinprogress']) + 60*60*24*$checkforapproverequests[$i]['customer']['plan_turn_around_days'];		
																$date = date("Y-m-d",strtotime($checkforapproverequests[$i]['dateinprogress']));
																$time = date("h:i:s",strtotime($checkforapproverequests[$i]['dateinprogress']));
																
															   if($checkforapproverequests[$i]['status_admin'] == "disapprove"){ $checkforapproverequests[$i]['customer']['plan_turn_around_days'] = $checkforapproverequests[$i]['customer']['plan_turn_around_days']+1; }
																echo date("m/d/Y g:i a",strtotime($date." ".$checkforapproverequests[$i]['customer']['plan_turn_around_days']." weekdays ".$time));
															}
														}
													?>
													</td> 
													<td style="color:<?php echo $txtcolor; ?>;"><?php if(isset($admin_unread_message_count_array[$checkforapproverequests[$i]['id']]) && $admin_unread_message_count_array[$checkforapproverequests[$i]['id']]!=0) { ?>
													<button class="btn btn-msg" style="border-radius:20px;border:none;">
													<?= $admin_unread_message_count_array[$checkforapproverequests[$i]['id']] ?></button> 
													<?php } ?></td> 
                                                    <td style="display: inline-flex;color:<?php echo $txtcolor; ?>;">
                                                        <a href="">
                                                            <!-- <span class="fa fa-eye fa-2x text-primary"></span> --->
															<button class="btn-view btn-primary"><i class="fa fa-eye" aria-hidden="true"></i>view</button> 
                                                        </a>
                                                        <?php                                                       
														//echo $this->Form->postLink('<button class="btn-cancel btn-danger"><i class="fa fa-times" style="margin-right: 5px;font-size: 15px;"></i></button>', Router::url(['controller' => 'Requests', 'action' => 'cancleRequest', $request->id], TRUE), ['escape' => FALSE, 'confirm' => 'Are you sure want to delete?']);
                                                        ?>
                                                    </td>

                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						

                        <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
												<th>Customer Name</th>
												<th>Designer Name</th>    
                                                <th>Title</th>                       
                                                <th>Date Requested</th>
												<th>Messages(<?= $total_unread_message2 ?>)</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											for ($i=0;$i<sizeof($approved_designs);$i++){ ?>
											
                                                <tr>
                                                    <td><?= $i+1 ?></td>
                                                   
                                                   <td style="color:<?php echo $txtcolor; ?>;">
														<span style="color: blue;background:<?php 
															if(isset($customerarray[$approved_designs[$i]['customer_id']]['current_plan_color'])){
																echo $customerarray[$approved_designs[$i]['customer_id']]['current_plan_color']; 
															}
															?>;border-radius: 50%;width: 7px;border: 0px;height:7px;display:block;margin:auto;text-align:center;">&nbsp;</span></br><?= ($approved_designs[$i]['customer']) ? $approved_designs[$i]['customer'][0]['first_name'] . ' ' . $approved_designs[$i]['customer'][0]['last_name'] : '' ?></td>
													
													<td style="color:<?php echo $txtcolor; ?>;"><?= ($approved_designs[$i]['designer']) ? $approved_designs[$i]['designer'][0]['first_name'] . ' ' . $approved_designs[$i]['designer'][0]['last_name'] : '' ?></td>
													
													 <td style="color:<?php echo $txtcolor; ?>;"><?= $approved_designs[$i]['title'] ?></td>
													 <td  style="color:<?php echo $txtcolor; ?>;"><?= ($approved_designs[$i]['created']) ? str_replace("-"," ",str_replace(" ","<br>",date("m/d/Y g:i-a",strtotime($approved_designs[$i]['created'])))) : ''; ?></td>
													 
													<td style="color:<?php echo $txtcolor; ?>;"><?php if(isset($admin_unread_message_count_array[$approved_designs[$i]['id']]) && $admin_unread_message_count_array[$approved_designs[$i]['id']]!=0) { ?>
													<button class="btn btn-msg" style="border-radius:20px;border:none;">
													<?= $admin_unread_message_count_array[$approved_designs[$i]['id']] ?></button> 
													<?php } ?></td> 
													
                                                    <td style="display: inline-flex;">
                                                        <a href="">
															<button class="btn-view btn-primary"><i class="fa fa-eye" aria-hidden="true"></i>view</button>
                                                        </a>
                                                    </td>

                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>