<?php //echo "<pre/>";print_R($aff_clients);     ?>
<div class="cli-ent-row tr brdr ">  
    <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
    <div class="client-hd-listing" style="width:25%;">
        <div class="cli-ent-xbox text-left">
            <div class="cli-ent-col td flex-uses" style="">
                <figure class="cli-ent-img circle one">
                    <?php //echo "<pre>"; print_r($aff_clients); ?>
                    <img src="<?php echo $aff_clients['profile_picture']; ?>" class="img-responsive one">
                </figure>
                <h3 class="pro-head-q">
                    <?php echo ucwords($aff_clients['first_name'] . " " . $aff_clients['last_name']); ?>
                    <?php if ($aff_clients['isfullaccess']) { ?>
                        <p class="pro-a"> <?php echo (strlen($aff_clients['email']) > 25) ? substr($aff_clients['email'], 0, 20) . '..' : $aff_clients['email']; ?></p>
                    <?php } ?>
                    <p class="neft">
                        <span class="blue <?php echo ($aff_clients['client_plan'] != '' || $aff_clients['client_plan'] != NULL) ? 'set_plancolor' : ''; ?>">
                            <?php echo $aff_clients['client_plan']; ?>
                        </span>
                    </p>
                </h3>
            </div>
        </div>
    </div>
    <div class="cli-ent-col td text-center" style="width: 25%;">
        <div class="cli-ent-xbox">
            <p class="pro-b"><?php echo $aff_clients['created']; ?></p>
        </div>
    </div>
    <div class="cli-ent-col td text-center" style="width: 25%;">
        <div class="cli-ent-xbox">
            <p class="pro-b"><?php echo $aff_clients['billing_end_date']; ?></p>
        </div>
    </div>
    <div class="cli-ent-col td action-per text-center" style="width: 25%;">
        <div class="cli-ent-xbox text-center">
            <div class="notify-lines">
                <div class="switch-custom switch-custom-usersetting-check">
                    <span class="checkstatus checkstatus_<?php echo $aff_clients['id']; ?>"></span>
                    <input type="checkbox" class="affactive" data-cid="<?php echo $aff_clients['id']; ?>" id="switch_<?php echo $aff_clients['id']; ?>" 
                    <?php
                    if ($aff_clients['is_active'] == 1) {
                        echo 'checked';
                    } else {
                        
                    }
                    ?>/>
                    <label for="switch_<?php echo $aff_clients['id']; ?>"></label> 
                </div>

                <?php if ($aff_clients['isfullaccess']) { ?>
                    <a href="<?php echo base_url(); ?>admin/accounts/view_client_profile/<?php echo $aff_clients['id']; ?>" title="View Client Details">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/icon-user.svg">
                    </a>
                <?php } ?>
                <?php if ($aff_clients['is_affiliated'] == 0) { ?>
                    <a href="<?php echo base_url(); ?>admin/dashboard?client_id=<?php echo $aff_clients['id']; ?>" title="View Projects"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/list_icon.svg" /></a>
                    <a data-toggle="modal" data-target="#show_notepad_area" class="add_notes" title="Add Note" data-id="<?php echo $aff_clients['id']; ?>" data-name="<?php echo ucwords($aff_clients['first_name'] . " " . $aff_clients['last_name']); ?>">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/add-note-icon.svg" />
                    </a>
                <?php } ?>
                <?php if ($aff_clients['isfullaccess'] && $aff_clients['is_affiliated'] == 0) { ?>
                    <a href="javascript:void(0)" data-customerid="<?php echo $aff_clients['id']; ?>" class="delete_customer">
                        <i class="icon-gz_delete_icon"></i>
                    </a>
                <?php } ?>
            </div>
        </div>


    </div>

    <!-- Modal -->
    <div class="modal fade" id="viewinfo<?php echo $aff_clients['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="cli-ent-model-box">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">
                            <h3 class="head-c">View Information</h3>
                        </header>
                        <div class="fo-rm-body">
                            <div class="projects">
                                <ul class="projects_info_list">
                                    <li>
                                        <div class="starus"><b>Active Projects</b></div>
                                        <div class="value"><?php echo $aff_clients['active']; ?></div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>In -Queue Projects</b></div>
                                        <div class="value"><?php echo $aff_clients['inque_request']; ?></div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>Revision Projects</b></div>
                                        <div class="value"><?php echo $aff_clients['revision_request']; ?></div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>Review Projects</b></div>
                                        <div class="value"><?php echo $aff_clients['review_request']; ?></div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>Complete Projects</b></div>
                                        <div class="value"><?php echo $aff_clients['complete_request']; ?></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ShowSubusers" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">

    </div>
    <!-- Modal -->
    <div class="modal fade" id="<?php echo $aff_clients['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="cli-ent-model-box">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">
                            <h3 class="head-c">Add Client</h3>
                        </header>
                        <div class="fo-rm-body">
                            <form onSubmit="return EditFormSQa(<?php echo $aff_clients['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/dashboard/edit_client/<?php echo $aff_clients['id']; ?>">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">First Name</label>
                                            <p class="space-a"></p>
                                            <input type="text" name="first_name" required class="efirstname form-control input-c" value="<?php echo ucwords($aff_clients['first_name']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">Last Name</label>
                                            <p class="space-a"></p>
                                            <input type="text" name="last_name" required class="elastname form-control input-c" value="<?php echo ucwords($aff_clients['last_name']); ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">Email</label>
                                            <p class="space-a"></p>
                                            <input type="email" name="email" required  class="eemailid form-control input-c" value="<?php echo $aff_clients['email']; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">Phone</label>
                                            <p class="space-a"></p>
                                            <input type="tel" name="phone" required class="ephone form-control input-c" value="<?php echo $aff_clients['phone']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group goup-x1">
                                    <label class="label-x3">State</label>
                                    <p class="space-a"></p>
                                    <input type="text" name="state"  required class="form-control input-c epassword" value="<?php echo ucwords($aff_clients['state']); ?>">
                                </div>

                                <div class="form-group goup-x1">
                                    <label class="label-x3">Password</label>
                                    <p class="space-a"></p>
                                    <input type="password" name="password"   class="form-control input-c epassword" >
                                </div>

                                <div class="form-group goup-x1">
                                    <label class="label-x3">Confirm Password</label>
                                    <p class="space-a"></p>
                                    <input type="password" name="confirm_password"  class="form-control input-c ecpassword" >
                                </div>
                                <div class="epassError" style="display: none;">
                                    <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                </div>
                                <hr class="hr-b">
                                <p class="space-b"></p>

                                <div class="form-group goup-x1">
                                    <label class="label-x3">Select payment Method</label>
                                    <p class="space-a"></p>
                                    <select class="form-control select-c">
                                        <option>Credit/Debit Card.</option>
                                        <option>Credit/Debit Card.</option>
                                        <option>Credit/Debit Card.</option>
                                    </select>
                                </div>

                                <div class="form-group goup-x1">
                                    <label class="label-x3">Card Number</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx" name="Card Number" required>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">Expiry Date</label>
                                            <p class="space-a"></p>
                                            <input type="text" name="Expiration Date" required class="form-control input-c" placeholder="22.05.2022">
                                        </div>
                                    </div>

                                    <div class="col-sm-3 col-md-offset-4">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">CVC</label>
                                            <p class="space-a"></p>
                                            <input type="text" name="CVC" required class="form-control input-c" placeholder="xxx" maxlength="4">
                                        </div>
                                    </div>
                                </div> 

                                <p class="space-b"></p>

                                <p class="btn-x"><button type="submit" class="btn-g">Add Customer</button></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End Row -->

