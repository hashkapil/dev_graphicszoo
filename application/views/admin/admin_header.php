<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google" content="notranslate">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Portal | Graphics Zoo</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo FAV_ICON_PATH;?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Bootstrap -->
      
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/designer/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/reset.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/editor.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/adjust.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/responsive.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/editor/editor.css');?>" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/jquery.fancybox.min.css');?>">
    <link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/ui-slider/jquery.ui-slider.css');?>" />
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/custom_style_for_all.css'); ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"/>

    <?php 
    $url = current_url();
    $messageurl = explode('/', $url);
    $activeurl = parse_url($url);
//        print_r($messageurl);
//        $messageurl = substr($url, strrpos($url, '/') + 1);
//        if($messageurl == 'message_list'){
    if (in_array("message_list", $messageurl)) { ?>
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/messages.css');?>" rel="stylesheet">
        <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
        <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js'); ?>"></script>
    <?php } ?>
     <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/leader-line.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/anim-event.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/in-view.min.js"></script>
        <script>
            var baseUrl = "<?php echo base_url(); ?>";
            var $asset_path  = '<?php echo FS_PATH_PUBLIC_ASSETS; ?>';
            var profile_path  = '<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>';
        </script> 
    <style type="text/css">
    span.mess-box img {
        max-width: 45px;
    }
    .alert strong {
        font: normal 16px/30px 'GothamPro-Medium', sans-serif;
    }
    p.ntifittext-z1 {
        white-space: normal;
        width: 200px;
    }
</style>
</head>
<?php
    $CI = & get_instance();
    $CI->load->library('myfunctions');
    $CI->load->helper('notifications');
    $helper_data = helpr();
    $edit_profile = $helper_data['profile_data'];
    $messagenotifications = $helper_data['messagenotifications'];
    $messagenotification_number = $helper_data['messagenotification_number'];
    $notifications = $helper_data['notifications'];
    $notification_number = $helper_data['notification_number'];
?>
    <body class="deshboard-bgnone <?php echo in_array('view_files',$messageurl) ? 'view_file_body' : '';?>" style="background: #fafafa;">
        <header class="nav-section">
            <div class="container-fluid">
                <nav class="navbar navbar-default flex-show centered"> <!-- centered -->
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".small-menu" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="custom-logo-link" href="<?php echo base_url(); ?>admin/dashboard"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.svg" class="img-responsive" style="width: 100%;;"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse small-menu" >
                        <ul class="nav navbar-nav">
                            <li class="<?php //echo ($activeurl['path'] == '/admin/dashboard')?'active':''?>"><a href="<?php echo base_url(); ?>admin/dashboard">
                                <span><i class="far fa-file-powerpoint"></i></span> Projects
                            </a></li>
                            <li class="<?php //echo in_array('view_clients',$messageurl)?'active':''?>"><a href="<?php echo base_url(); ?>admin/dashboard/view_clients">
                                <span><i class="fas fa-user-tie"></i></span> Clients</a></li>
                                <li class="<?php //echo in_array('view_designers',$messageurl)?'active':''?>"><a href="<?php echo base_url(); ?>admin/dashboard/view_designers">
                                    <span><i class="fas fa-palette"></i></span> Designers</a></li>
                                    <?php if(($edit_profile[0]['full_admin_access'] != 0 || $edit_profile[0]['full_admin_access'] == NULL) && $edit_profile[0]['role'] == 'admin'){ ?>        
                                        <li class="<?php //echo in_array('view_qa',$messageurl)?'active':''?>"><a href="<?php echo base_url(); ?>admin/dashboard/view_qa">
                                            <span><i class="fas fa-edit"></i></span> QA/VA</a></li>
                                            <li class="<?php //echo in_array('reports',$messageurl)?'active':''?>"><a href="<?php echo base_url(); ?>admin/report/dashboard">
                                                <span><i class="fas fa-laptop-code"></i></span> Reporting</a>
                                            </li>
                                            <li class="<?php //echo in_array('Contentmanagement',$messageurl)?'active':''?>"><a href="<?php echo base_url(); ?>admin/Contentmanagement">
                                                <span><i class="fas fa-network-wired"></i></span>  Content Management</a></li>
                                            <?php } ?>        
                                        </ul>
                                    </div><!-- /.navbar-collapse -->

                                    <div class="box-setting">
                                        <ul id="right_nav" class="list-unstyled list-setting">
                                            <li><a href="<?php echo base_url(); ?>admin/dashboard/adminprofile">
                                                <!--<img src="images/icon-setting.png" class="img-responsive">-->
                                                <i class="fas fa-cog"></i>
                                            </a></li>
                                            <!-- Message Section -->
                                            <li class="dropdown">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <span class="mess-box">
                                                         <i class="far fa-comment-alt"></i>
                                       <!--  <svg class="icon">
                                            <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-notification-bell"></use>
                                        </svg> -->
                                        <?php
                                        if (isset($messagenotifications)) {
                                            if (sizeof($messagenotifications) > 0) {
                                                ?>
                                                <span class="numcircle-box">
                                                    <?php echo $messagenotification_number; ?>
                                                </span>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </span>
                                </a>
                                <div class="dropdown-menu">
                                    <h3 class="head-notifications">Messages</h3>
                                    <ul class="list-unstyled list-notificate">
                                        <?php if (sizeof($messagenotifications) > 0) { ?>
                                            <?php for ($i = 0; $i < sizeof($messagenotifications); $i++) { ?>
                                                <li><a href="<?php echo base_url() . $messagenotifications[$i]['url']; ?>">
                                                    <div class="setnoti-fication">
                                                        <figure class="pro-circle-k1">
                                                            <img src="<?php echo $messagenotifications[$i]['profile_picture']; ?>" class="img-responsive msg">
                                                        </figure>

                                                        <div class="notifitext">
                                                            <p class="ntifittext-z1">
                                                                <h5 style="font-size: 13px;"><?php echo $messagenotifications[$i]['heading']; ?></h5>
                                                                <!--small style="font-size: 12px;"--><?php
                                                                if (strlen($messagenotifications[$i]['title']) >= 50) {
                                                                    echo substr($messagenotifications[$i]['title'], 0, 40) . "..";
                                                                } else {
                                                                    echo $messagenotifications[$i]['title'];
                                                                }
                                                                ?><!--/small-->
                                                            </p>
                                                            <!-- <p class="ntifittext-z1"><strong>kevin Jenkins</strong> posted in <strong>Other Detail</strong></p> -->
                                                            <p class="ntifittext-z2">
                                                                <?php
                                                                $approve_date = $messagenotifications[$i]['created'];
                                                                $msgdate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                                                echo $msgdate;
                                                                
                                                                    //echo date("M d, Y h:i A", strtotime($approve_date));
                                                                ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a></li>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <li style="padding:10px;">No new message</li>
                                        <?php } ?>

                                    </ul>
                                    <!-- <div class="seeAll"><a href="#">See All</a></div> -->
                                </div>
                            </li>
                            <!-- Message Section End -->
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="bell-box">
                                        <i class="far fa-bell"></i>
                                       <!--  <svg class="icon">
                                            <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-notification-bell"></use>
                                        </svg> -->
                                        <?php
                                        if (isset($notifications)) {
                                            if (sizeof($notifications) > 0) {
                                                ?>
                                                <span class="numcircle-box">
                                                    <?php echo $notification_number; ?>
                                                </span>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </span>
                                </a>
                                <div class="dropdown-menu">
                                    <h3 class="head-notifications">Notifications</h3>
                                    <ul class="list-unstyled list-notificate">
                                        <?php if (sizeof($notifications) > 0) { ?>
                                            <?php for ($i = 0; $i < sizeof($notifications); $i++) { ?>
                                            <li><a href="<?php echo base_url() . $notifications[$i]['url']; ?>">
                                                <div class="setnoti-fication">
                                                    <figure class="pro-circle-k1">
                                                     
                                                        <img src="<?php echo $notifications[$i]['profile_picture']; ?>" class="img-responsive">
                                                    </figure>

                                                    <div class="notifitext" title="<?php echo $notifications[$i]['title']; ?>">
                                                        <p class="ntifittext-z1"><strong><?php echo $notifications[$i]['first_name'] . " " . $notifications[$i]['last_name']; ?></strong>
                                                            <?php echo $notifications[$i]['title']; ?>
                                                        </p>
                                                        <p class="ntifittext-z2">
                                                            <?php
                                                            $approve_date = $notifications[$i]['created'];
                                                            $notidate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                                            echo $notidate;
//                                                                    echo date("M d, Y h:i A", strtotime($approve_date));
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </a></li>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <li style="padding:10px; font: normal 12px/25px 'GothamPro', sans-serif;">No New notification</li>
                                    <?php } ?>

                                </ul>
                                <!-- <div class="seeAll"><a href="#">See All</a></div> -->
                            </div>
                        </li>

                        <li>
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expended="false">
                                <figure class="pro-circle-img-xxs">
                                        <img src="<?php echo $edit_profile[0]['profile_picture']; ?>" class="img-responsive">
                                    </figure>
                                </a>
                                <div class="dropdown-menu pull-right" aria-labelledby="dropdownMenuButton">
                                    <h3 class="head-notifications"><?php echo $edit_profile[0]['first_name'] . " " . $edit_profile[0]['last_name']; ?></h3>
                                    <h2 class="head-notifications"><?php echo $edit_profile[0]['email']; ?></h2>
                                    <ul class="list-unstyled list-notificate">
                                        <li><a href="<?php echo base_url(); ?>admin/messages/message_list">Messages</a></li>
                                        <?php if(($edit_profile[0]['full_admin_access'] != 0 || $edit_profile[0]['full_admin_access'] == NULL) && $edit_profile[0]['role'] == 'admin'){ ?>
                                           <li><a href="<?php echo base_url(); ?>admin/categories/cat_listing">Category Buckets</a></li>
                                       <?php } ?>
                                       <li>
                                        <a href="<?php echo base_url(); ?>admin/dashboard/reports">Reports</a>
                                    </li> 
                                    <li>
                                        <a href="<?php echo base_url(); ?>admin/dashboard/logout">Logout</a>
                                    </li>
                                </ul>
                                <!-- <a href="<?php echo base_url(); ?>qa/dashboard/logout" class="dropdown-item btn-default btn-block" style="padding: 10px;font: normal 12px/25px 'GothamPro', sans-serif;">Logout</a> -->
                            </div>
                        </li>                        
                    </ul>
                </div>

            </div><!-- /.container-fluid -->
        </nav>
    </div>
</header>
