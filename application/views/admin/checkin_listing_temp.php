<style>
    .feed_emo{
            box-sizing: border-box;
    max-width: 100%;
    width: 16px;
    height: auto;
    margin-right: 2px;
    position: relative;
    top: 3px;
}
</style>
<?php 
$checkinDate = isset($data['checkin_date']) ? $data['checkin_date']:'N/A';
$lastlogin_date = (isset($data['lastlogin_date']) && $data['lastlogin_date'] != NULL) ? $data['lastlogin_date'] : 'N/A';
$avg = (isset($data['total_avg']) && $data['total_avg'] != '') ? $data['total_avg'] : '';
$svg = ($avg != '') ? $data['svg'] : 'N/A';
$img = '<img class="feed_emo" src="'.base_url().'public/assets/img/customer/'.$svg.'">'.$avg.'<a href="javascript:void(0)" class="getrating_details" data-uid="'.$data['cust_id'].'"><i class="fa fa-eye" aria-hidden="true"></i></a>';
$svgIMG = ($avg != '') ? $img : 'N/A';
?>
<div class="cli-ent-row tr brdr">
    <div class="cli-ent-col td cust-name-req" style="width: 20%">
        <span class="cust_name_cancel"><?php echo $data['fname'] . ' ' . $data['lname']; ?></span>
        <span class="email_cancel" data-toggle="tooltip" title="<?php echo $data['email']; ?>"><?php echo (strlen($data['email']) > 20 ) ? substr($data['email'], 0, 20) . '..' : $data['email']; ?>
    </div>
    <div class="cli-ent-col td" style="width: 20%"><p class="cancel_rsnn"><?php echo $data['plan_name'];?></p></div>
    <div class="cli-ent-col td" style="width: 10%"><?php echo $data['active_count'].'/'.$data['count'];?></div>
    <div class="cli-ent-col td" style="width: 10%"><?php echo $lastlogin_date; ?></span></div>
    <div class="cli-ent-col td" style="width: 10%"><?php echo $svgIMG;?></div>
    <div class="cli-ent-col td" style="width: 20%"><span class="last_login_<?php echo $data['cust_id']; ?>"><?php echo $checkinDate; ?></span><?php echo ($checkinDate != 'N/A') ? '<span class="checkin_history" data-chkinuid="'.$data['cust_id'].'"> <i class="fas fa-info-circle"></i></span>' : '';?></div>
    <div class="cli-ent-col td" style="width: 20%">
        <a class="adddesinger checkin_by" data-userID="<?php echo $data['cust_id']; ?>" href="javascript:void(0)"> 
            <span> Checkin</span>
        </a>&nbsp;
        <a class="adddesinger notes_chekin" href="javascript:void(0)" data-userid="<?php echo $data['cust_id'];?>"> 
            <span> Notes</span>
        </a>
    </div>
</div>