<section id="content-wrapper">
    <div class="site-content-title">
        <ol class="breadcrumb float-xs-right">
            <li class="breadcrumb-item">
                <span class="fs1" aria-hidden="true" data-icon="?"></span>
                <a href="#">Main Menu</a>
            </li>
            <li class="breadcrumb-item active">View Users</li>
        </ol>
    </div>
    <div class="content content-datatable datatable-width">
		<h2 class="float-xs-left content-title-main">Designers</h2>
        <div class="row">
            <div class="col-md-12">
                <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Profile</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Manager Assigner</th>
                            <th>Email</th>
                            <th>Phone</th>
							<th>Customer</th>
                            <!--<th>Current Status</th>-->
                            <th>Action</th>                            
                        </tr>
                    </thead>
                    <tbody>
						<?php for($i=0;$i<sizeof($designers);$i++){ ?>
                            <tr>
                                <td><?= $i+1; ?></td>
                                <td>
                                    <?php
                                        // $img_path = '';
                                        // $img_path = @$designer->profile_picture_url;
                                        // if ($img_path):
                                        // echo $this->Html->image($img_path, ['class' => 'img img-responsive img-small']);
                                    // endif;
                                    ?>
                                </td>
                                <td><?= $designers[$i]['first_name'] ?></td>
                                <td><?= $designers[$i]['last_name'] ?></td>
                                <td></td>
                                <td><?= $designers[$i]['email'] ?></td>
                                <td><?= $designers[$i]['phone'] ?></td>
								<td><?php echo $designer_customer[$designers[$i]['id']]; ?></td>
                                <?php
                                $designer_status = '';
                                $designer_text = '';
                                $designer_message = '';
                                if ($designers[$i]['status'] == 0):
                                    $designer_status = 'text-danger';
                                    $designer_text = 'Block';
                                    $designer_icons = 'fa fa-times fa-2x text-danger';
                                    $designer_message = 'UnBlock';
                                elseif ($designers[$i]['status'] == 1):
                                    $designer_status = 'text-success';
                                    $designer_text = 'UnBlock';
                                    $designer_icons = 'fa fa-check text-success';
                                    $designer_message = 'Block';
                                endif;
                                ?>
								<td class="action-icon">
                                    <a href="<?php echo base_url()."admin/view_designer/".$designers[$i]['id'];?>">View /</a>
                                    <a href=""> Edit </a>
									<a href=""> <span class="fa fa-times"></span></a>
                                    <?php //echo $this->Html->link(__('<i style="font-style: normal;" class="' . $designer_icons . '"></i>'), ['controller' => 'Designers', 'action' => 'statusChange', $designer->id, $designer->status], ['confirm' => __('Are you sure you want to ' . $designer_message . ' this employee ?', $designer->id), 'escape' => FALSE]) ?>&nbsp;&nbsp;                                            
                                </td>
                            </tr>        
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>