<div class="cli-ent-model-box">
    <div class="cli-ent-model">
        <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">
                <?php echo $branddata[0]['brand_name']; ?>
            </h2>
            <div class="cross_popup" data-dismiss="modal" aria-label="Close">
                x
            </div>
        </header>
        <?php if ($branddata[0]['id'] != '') { ?>
            <div class="displayed-pic">
                <div class="dp-here">
                    <?php
                    $type = substr($brand_materials_files['latest_logo'], strrpos($brand_materials_files['latest_logo'], '.') + 1);
                    $allow_type = array("jpg", "jpeg", "png", "gif");
                    if (isset($brand_materials_files['latest_logo']) && in_array(strtolower($type), $allow_type)) {
                        ?>
                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" height="150">
                    <?php } elseif (isset($brand_materials_files['latest_logo']) && !in_array(strtolower($type), $allow_type)) { ?>
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/all-file.jpg" height="150">
                        <!-- <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div> -->
                    <?php } else { ?>
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/logo-brand-defult.jpg" height="150" />
                    <?php } ?>
                </div>
                <div class="profile_info">
                    <div class="profile colm">
                        <div class="form-group">
                            <div class="brand_labal">Fonts Size</div>
                            <div class="brand_info">
                                <?php echo $branddata[0]['fonts']; ?>
                            </div>
                        </div>
                    </div>
                    <div class="profile colm">
                        <div class="form-group">
                            <div class="brand_labal">Color Preferences</div>
                            <div class="brand_info">
                                <?php echo $branddata[0]['color_preference']; ?>
                            </div>
                        </div>
                    </div>
                    <div class="profile colm">
                        <div class="form-group">
                            <div class="brand_labal">Website URL </div>
                            <div class="brand_info">
                                <?php echo $branddata[0]['website_url']; ?>
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($branddata[0]['google_link'])) { ?>
                        <div class="profile colm" style="width:100%">
                            <div class="form-group">
                                <div class="brand_labal">Addition Reference Link</div>
                                <div class="brand_info">
                                    <?php
                                    $brandlinks = explode(',', $branddata[0]['google_link']);
                                    foreach ($brandlinks as $bk => $bv) {
                                        echo $bv . "<br/>";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="profile colm">
                        <div class="form-group">
                            <div class="brand_labal">Brand Description </div>
                            <div class="brand_info discrptions two-can">
                                <?php echo $branddata[0]['description']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="all-brand-outer">
                <div class="profile colm materials-sp">
                    <div class="brand_labal">Brand Logo </div>
                    <div class="brand_info row">
                        <?php
                        for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                            if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
                                $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                    ?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="accimgbx33">
                                            <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                <p class="extension_name">
                                                    <?php echo $type; ?>
                                                </p>
                                            </a>
                                            <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="accimgbx33">
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                            <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="profile colm materials-sp">
                    <div class="brand_labal">Market Materials </div>
                    <div class="brand_info row">
                        <?php
                        for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                            if ($brand_materials_files[$i]['file_type'] == 'materials_upload') {
                                $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                    ?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="accimgbx33">
                                            <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                <p class="extension_name">
                                                    <?php echo $type; ?>
                                                </p>
                                            </a>
                                            <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="accimgbx33">
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                            <div class="bottom-icon">
                                                <a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="profile colm materials-sp">
                    <div class="brand_labal">Additional Images </div>
                    <div class="brand_info row">
                        <?php
                        for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                            if ($brand_materials_files[$i]['file_type'] == 'additional_upload') {
                                $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                    ?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="accimgbx33">
                                            <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                <p class="extension_name">
                                                    <?php echo $type; ?>
                                                </p>
                                            </a>
                                            <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="accimgbx33">
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                            <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>