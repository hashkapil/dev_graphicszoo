<section id="content-wrapper">

    <div class="site-content-title">
        <ol class="breadcrumb float-xs-right">
            <li class="breadcrumb-item">
                <span class="fs1" aria-hidden="true" data-icon="?"></span>
                <a href="#">Main Menu</a>
            </li>
            <li class="breadcrumb-item active">View Users</li>
        </ol>
    </div>
    <div class="content content-datatable datatable-width">
		<h2 class="float-xs-left content-title-main">Customers</h2>
        <div class="row">
            <div class="col-md-12">
                <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Company Name</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Plan</th>
							<th>Designer Name</th>
                            <th>Action</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php for($i=0;$i<sizeof($all_customer);$i++){ ?>
                            <tr>
                                <td><?= $i+1 ?></td>
                                <td><?= $all_customer[$i]['company_name'] ?></td>
                                <td><?= $all_customer[$i]['first_name'] ?></td>
                                <td><?= $all_customer[$i]['last_name'] ?></td>
                                <td><?= $all_customer[$i]['email'] ?></td>
                                <td><?= $all_customer[$i]['phone'] ?></td>
                                <td><?= $all_customer[$i]['display_plan_name'] ?></td>
								<td><?php if($all_customer[$i]['designer']!=""){ echo $all_customer[$i]['designer']['first_name']; } ?>
                                <?php
                                $user_status = '';
                                $user_text = '';
                                $user_message = '';
                                if ($all_customer[$i]['status'] == 1):
                                    $user_status = 'text-danger';
                                    $user_text = 'Block';
                                    $user_icons = 'fa fa-times fa-2x text-danger';
                                    $user_message = 'UnBlock';
                                elseif ($all_customer[$i]['status'] == 0):
                                    $user_status = 'text-success';
                                    $user_text = 'UnBlock';
                                    $user_icons = 'fa fa-check text-success';
                                    $user_message = 'Block';
                                endif;
                                ?>   
                                
								<td class="action-icon">                            
                                    <a href="<?php echo base_url(); ?>admin/edit_customer/<?php echo $all_customer[$i]['id']; ?>"> Edit </a>
									
									<?php //echo $this->Html->link(__('<i style="font-style: normal;" class="fa fa-times"></i>'), ['controller' => 'Customers', 'action' => 'delete', $customer->id], ['confirm' => __('Are you sure you want to delete this customer ?', $customer->id), 'escape' => FALSE]) ?>
									
                                    <?php //echo $this->Html->link(__('<i style="font-style: normal;" class="' . $user_icons . '"></i>'), ['controller' => 'Customers', 'action' => 'statusChange', $customer->id, $customer->status], ['confirm' => __('Are you sure you want to ' . $user_message . ' this customer ?', $customer->id), 'escape' => FALSE]) ?>              
                                </td>
                            </tr>        
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

