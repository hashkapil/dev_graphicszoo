<!-- Uploaded Draft -->
<?php // echo "<pre/>";print_r($profile); 
//echo $profile[0]['full_admin_access'].'accessadmin'.$profile[0]['profile_picture'];?>
<div class="pro-inrightbox">
    <a class="show_trans" id="show_trans"><span></span><span></span><span></span></a>
    <div class="transcopy" id="transcopy" style="display: none;">
      <?php //if($profile[0]['full_admin_access'] != 0){ ?> 
         <a class="mark_messages reddelete" data-status="mark_read" data-id="<?php echo $data[0]['id']; ?>">Mark as read</a>
     <?php //}else{ ?>
<!--         <a class="markmsg_disabled" data-status="mark_read" data-id="<?php echo $data[0]['id']; ?>">Mark as read</a>  -->
     <?php //} ?>
     <a class="mark_messages" data-status="mark_unread" data-id="<?php echo $data[0]['id']; ?>">Mark as unread</a>
     <?php if($data[0]['is_star'] == 0){?>
         <a class="mark_messages add_star" data-status="mark_star" data-id="<?php echo $data[0]['id']; ?>">Add Star</a>
     <?php }else {?>
         <a class="mark_messages remove_star" data-status="remove_star" data-id="<?php echo $data[0]['id']; ?>">Remove Star</a>
     <?php } ?>
     <a class="mark_messages remove_star" data-status="remove_star" style="display:none" data-id="<?php echo $data[0]['id']; ?>">Remove Star</a>

 </div>
</div>

<?php if($ajax_type == '1'){?>
    <div class="msg_project_name"><?php echo $data[0]['title']; ?></div>
    <?php 
    if(count($draft_array) > 0) { ?>
        <div class="draft_count"><p class="show_count">Design Draft (<?php echo count($draft_array); ?>)</p></div> 
    <?php } ?>
<div class="project-list-share">
    <div class="orb-xb-col">
        <div class="orbxb-img-xx3s one-can">
            <div class="row-designdraftnew">
                <ul class="list-unstyled clearfix list-designdraftnew two-can">
                    <li class="show_main_chat open" data-id="<?php echo $data[0]['id']; ?>" data-chat="main_chat" data-value="1">
                        <div class="chatbox-add">
                           <?php if($count_mainchat_unread_msg > 0){ ?>
                            <span class="numcircle-box-01"><?php echo $count_mainchat_unread_msg; ?></span>
                        <?php } ?>
                    </div>
                    <div class="star_chat"></div>
                    <i class="far fa-comments"></i>  Main Chat</li>

                    <?php if ($data[0]['designer_attachment']): ?>
                        <?php for ($i = 0; $i < count($data[0]['designer_attachment']); $i++) :
                            ?>
                            <?php
                            $ap = $data[0]['designer_attachment'][$i]['status'];

                            if (($ap != "pending") && ($ap != "Reject")) {
                                ?>
                                <li data-id="<?php echo $data[0]['designer_attachment'][$i]['id'];?>" data-chat="comment_chat" class="">
                                    <div class="unread_chat"></div>
                                    <div class="star_chat"></div>
                                    <div class="figimg-one get_comment_chat" data-id="<?php echo $data[0]['designer_attachment'][$i]['id'];?>">
                                        <?php
                                        $type = substr($data[0]['designer_attachment'][$i]['file_name'], strrpos($data[0]['designer_attachment'][$i]['file_name'], '.') + 1);

                                        ?>
                                        <div class="go_inside">
                                            <a href="<?php echo base_url() . "admin/dashboard/view_files/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $data[0]['id']; ?>" target="_blank" class=""><i class="fas fa-arrow-up"></i></a>
                                        </div>
                                        <a href="javascript:void(0)">
                                            <img src="<?php echo imageUrl($type, $data[0]['id'], $data[0]['designer_attachment'][$i]['file_name']); ?>" class="img-responsive" />

                                            <div class="no-heading">
                                                <div class="chatbox-add">
                                                    <!-- <img src="<?php //echo FS_PATH_PUBLIC_ASSETS ?>img/customer/icon-chat.png" class="img-responsive"> -->
                                                    <?php for ($j = 0; $j < count($file_chat_array); $j++): ?>
                                                        <?php if ($file_chat_array[$j]['id'] == $data[0]['designer_attachment'][$i]['id']): ?>
                                                            <span class="numcircle-box-01"><?php echo $file_chat_array[$j]['count']; ?></span>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                            <?php } endfor; ?>											
                        </ul>
                        <?php else: ?>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- End Uploaded Draft -->
    <div id="message_chat_area">
        <div class="read-msg-box two-can" id="message_container" style="padding:0px; height:320px; overflow-y: scroll;overflow-x: hidden;width: 99%">

           <?php for ($j = 0; $j < count($chat_request); $j++) { ?>
            <!-- Left Start -->
            <div class="msgk-chatrow clearfix">
                <?php 
                if ($chat_request[$j]['with_or_not_customer'] == 1) { ?>
                    <?php if ($chat_request[$j]['sender_id'] != $login_user_id) { ?>	
                        <div class="msgk-user-chat msgk-left <?php echo $chat_request[$j]['sender_type']; ?>">
                            <!-- If Current Message and Previous Message are not Same Sender Type-->
                            <?php if ($j == 0) { ?>
                                <figure class="msgk-uimg">
                                    <a class="msgk-uleft" href="javascript:void(0)" title="<?php echo isset($chat_request[$j]['first_name'])?$chat_request[$j]['first_name']:$chat_request[$j]['shared_user_name']; ?>">
                                        <?php if ($chat_request[$j]['profile_picture'] != "") { ?>
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $chat_request[$j]['profile_picture']; ?>" class="img-responsive">
                                        <?php } else { ?>
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive"/>
                                        <?php } ?>
                                    </a>
                                </figure>
                            <?php } ?>
                            <?php if ($j != 0) { ?>
                                <?php if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type'] || ($chat_request[$j]['sender_id'] != $chat_request[$j - 1]['sender_id'] || $chat_request[$j]['shared_user_name'] != $chat_request[$j - 1]['shared_user_name'])) { ?>
                                    <figure class="msgk-uimg">
                                        <a class="msgk-uleft" href="javascript:void(0)" title="<?php echo isset($chat_request[$j]['first_name'])?$chat_request[$j]['first_name']:$chat_request[$j]['shared_user_name']; ?>">
                                            <?php if ($chat_request[$j]['profile_picture'] != "") { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $chat_request[$j]['profile_picture']; ?>" class="img-responsive">
                                            <?php } else { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive"/>
                                            <?php } ?>
                                        </a>
                                    </figure>
                                    <p class="msgk-udate right">
                                        <?php echo (isset($chat_request[$j]['first_name']) && $chat_request[$j]['first_name'] != '') ? $chat_request[$j]['first_name'] . ' - ' : $chat_request[$j]['shared_user_name']. ' - '; ?><?php echo $chat_request[$j]['chat_created_date']; ?>
                                    </p>
                                    <div class="msgk-mn">
                                        <div class="msgk-umsgbox">
                                            <pre><span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span></pre>
                                        </div>
                                    </div>


                                    <!-- If Current Message and Next Message are not Same Sender Type-->
                                <?php } elseif (array_key_exists($j + 1, $chat_request) && $chat_request[$j]['sender_type'] != $chat_request[$j + 1]['sender_type']) { ?>
                                    <p class="msgk-udate t7">
                                        <?php echo (isset($chat_request[$j]['first_name']) && $chat_request[$j]['first_name'] != '') ? $chat_request[$j]['first_name'] . ' - ' : ""; ?><?php echo $chat_request[$j]['chat_created_date']; ?>
                                    </p>
                                    <div class="msgk-mn last_message">
                                        <div class="msgk-umsgbox">
                                            <pre><span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span></pre>
                                        </div>
                                    </div>

                                    <!-- If Current Message and Previous Message are Same Sender Type-->
                                <?php } else { ?>
                                    <p class="msgk-udate t6">
                                        <?php echo (isset($chat_request[$j]['first_name']) && $chat_request[$j]['first_name'] != '') ? $chat_request[$j]['first_name'] . ' - ' : ""; ?><?php echo $chat_request[$j]['chat_created_date']; ?>
                                    </p>
                                    <div class="msgk-mn center_message">
                                        <div class="msgk-umsgbox">
                                            <pre><span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span></pre>
                                        </div>
                                    </div>
                                <?php } ?>	
                            <?php } else { ?>
                                <p class="msgk-udate t5">
                                    <?php echo (isset($chat_request[$j]['first_name']) && $chat_request[$j]['first_name'] != '') ? $chat_request[$j]['first_name'] . ' - ' : ""; ?><?php echo $chat_request[$j]['chat_created_date']; ?>
                                </p>
                                <div class="msgk-mn center_message">
                                    <div class="msgk-umsgbox">
                                        <pre><span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span></pre>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
                <!-- Left End -->

                <!-- Right End -->

                <?php if ($chat_request[$j]['sender_id'] == $login_user_id) { ?>
                    <div class="msgk-user-chat msgk-right <?php echo $chat_request[$j]['sender_type']; ?>">
                        <?php if ($j == 0) { ?>
                                                    <!--<figure class="msgk-uimg right-img">
                                                        <a class="msgk-uleft" href="javascript:void(0)" title="<?php echo $profile[$j]['first_name']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $profile[0]['profile_picture']; ?>" class="img-responsive">
                                                        </a>
                                                    </figure>-->
                                                <?php }
                                                ?>
                                                <?php
                                                if ($j > 0) {
                            //echo $chat_request[$j - 1]['sender_id'];
                                                    if (($chat_request[$j]['sender_id'] != $chat_request[$j - 1]['sender_id'])) {
                                                        ?>
                                                        <!--<figure class="msgk-uimg right-img"><a class="msgk-uleft" href="javascript:void(0)" title="<?php echo $profile[0]['first_name']; ?>"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $profile[0]['profile_picture']; ?>" class="img-responsive"></a></figure>-->
                                                        <p class="msgk-udate t7">
                                                            <?php echo (isset($chat_request[$j]['first_name']) && $chat_request[$j]['first_name'] != '') ? $chat_request[$j]['first_name'] . ' - ' : ""; ?><?php echo $chat_request[$j]['chat_created_date']; ?>
                                                        </p>
                                                        <div class="msgk-mn">
                                                            <div class="msgk-umsgbox">
                                                                <pre><span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span></pre>
                                                            </div>                                          
                                                        </div>
                                                    <?php } elseif (array_key_exists($j + 1, $chat_request) && $chat_request[$j]['sender_type'] != $chat_request[$j + 1]['sender_type']) { ?>
                                                        <p class="msgk-udate t7">
                                                            <?php echo (isset($chat_request[$j]['first_name']) && $chat_request[$j]['first_name'] != '') ? $chat_request[$j]['first_name'] . ' - ' : ""; ?><?php echo $chat_request[$j]['chat_created_date']; ?>
                                                        </p>
                                                        <div class="msgk-mn last_message">
                                                            <div class="msgk-umsgbox">
                                                                <pre><span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span></pre>
                                                            </div>                                          
                                                        </div>
                                                    <?php } else { ?>
                                                        <p class="msgk-udate t7">
                                                            <?php echo (isset($chat_request[$j]['first_name']) && $chat_request[$j]['first_name'] != '') ? $chat_request[$j]['first_name'] . ' - ' : ""; ?><?php echo $chat_request[$j]['chat_created_date']; ?>
                                                        </p>
                                                        <div class="msgk-mn center_message">
                                                            <div class="msgk-umsgbox">
                                                                <pre><span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span></pre>
                                                            </div>                                          
                                                        </div>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <div class="msgk-mn">
                                                        <div class="msgk-umsgbox">
                                                            <pre><span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span></pre>
                                                        </div>                                          
                                                    </div>

                                                <?php } ?>
                                            </div>
                                        <?php } ?>   
                                    </div>
                                    <!-- Customer Left End -->
                                <?php } ?>

                            </div>
                            <div class="cmmtype-box main_chat_box">
                                <div class="cmmtype-row">

                                    <textarea <?php if ($data[0]['status'] == "assign") {
                                        echo "disabled";
                                    } ?> type="text" class="pstcmm t_w text_<?php echo $data[0]['id']; ?>" Placeholder="Type a message..."></textarea>
                                    <span class="cmmsend">
                                        <button class="cmmsendbtn send_request_chat_admin"
                                        data-designerid="<?php echo $data[0]['designer_id']; ?>"
                                        data-requestid="<?php echo $data[0]['id']; ?>"  
                                        data-senderrole="admin" 
                                        data-senderid="<?php echo $login_user_id; ?>" 
                                        data-receiverid="<?php echo $data[0]['customer_id']; ?>" 
                                        data-receiverrole="customer"
                                        data-sendername="<?php echo $data[0]['designer_first_name'] . "" . $data[0]['designer_last_name']; ?>"
                                        data-customerwithornot="1" 
                                        data-profilepic="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $profile[0]['profile_picture']; ?>">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat-send.png" class="img-responsive">
                                    </button>
                                </span>


                            </div>
                        </div>
                    </div>

