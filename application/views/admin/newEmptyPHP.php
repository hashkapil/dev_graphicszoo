 <?php for ($i = 0; $i < sizeof($requestClients); $i++): //echo "<pre/>";print_r($trailClients);  ?>
                <!-- Start Row -->
                <div class="cli-ent-row tr brdr ">  
                    <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                    <div class="cli-ent-col td" style="width:38%;">
                        <div class="cli-ent-xbox text-left">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 80px; padding-right: 15px;">
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo $requestClients[$i]['profile_picture']; ?>" class="img-responsive one">
                                    </figure>
                                </div>

                                <div class="cell-col" style="width: 185px;">
                                    <h3 class="pro-head-q">
                                        <a href="<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $requestClients[$i]['id']; ?>"><?php echo ucwords($requestClients[$i]['first_name'] . " " . $requestClients[$i]['last_name']); ?>
                                    </a><br>
                                    <?php if(($edit_profile[0]['full_admin_access'] != 0 || $edit_profile[0]['full_admin_access'] == NULL) && $edit_profile[0]['role'] == 'admin'){ ?>
                                        <a href="<?php echo base_url(); ?>admin/accounts/view_client_profile/<?php echo $requestClients[$i]['id']; ?>">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </a>

                                        <a href="javascript:void(0)" data-customerid="<?php echo $requestClients[$i]['id']; ?>" class="delete_customer">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    <?php } ?>
                                    <a href="<?php echo base_url(); ?>admin/dashboard/brand_profile/<?php echo $requestClients[$i]['id']; ?>" target="_blank" class="edit-brand">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </h3>
                                <p class="pro-a"> <?php echo $requestClients[$i]['email']; ?></p>
                                <p class="neft">
                                    <span class="blue text-uppercase">
                                        <?php
                                        $member_type = "";
                                        if ($requestClients[$i]['plan_turn_around_days'] == "1") {
                                            $member_type = "Premium Member";
                                        } elseif ($trailClients[$i]['plan_turn_around_days'] == "3") {
                                            $requestClients = "Standard Member";
                                        } else {
                                            $member_type = "No Member";
                                        }
                                        echo $member_type;
                                        ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $requestClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class=" ">Sign Up Date</p>
                        <p class="space-a"></p>

                        <p class="pro-b"><?php echo date("M /d /Y", strtotime($requestClients[$i]['created'])) ?></p>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $requestClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class=" ">Active</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $requestClients[$i]['active']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $requestClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class=" ">In Queue</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $requestClients[$i]['inque_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 15%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $requestClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class=" ">Pending Approval</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $requestClients[$i]['review_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 15%;">
                    <div class="cli-ent-xbox text-center">
                        <div class="switch-custom">
                            <span class="checkstatus_<?php echo $requestClients[$i]['id']; ?>">
                                <?php echo ($requestClients[$i]['is_active'] == 1) ? 'Active' : 'Inactive'; ?>
                            </span>
                            <input type="checkbox" data-cid="<?php echo $requestClients[$i]['id']; ?>" id="switch_<?php echo $requestClients[$i]['id']; ?>" <?php echo ($requestClients[$i]['is_active'] == 1) ? 'checked' : ''; ?>/>
                            <label for="switch_<?php echo $requestClients[$i]['id']; ?>"></label> 
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 15%">
                    <?php if (!empty($requestClients[$i]['designer'])) { ?>
                        <div class="cli-ent-xbox">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                    <a href="">
                                        <figure class="pro-circle-img">
                                            <img src="<?php echo $requestClients[$i]['designer'][0]['profile_picture']; ?>" class="img-responsive">
                                        </figure>
                                    </a>
                                </div>
                                <div class="cell-col">
                                    <p class="text-h " title="<?php echo $requestClients[$i]['designer'][0]['first_name'] . " " . $requestClients[$i]['designer'][0]['last_name']; ?>
                                    ">
                                    <?php echo $requestClients[$i]['designer'][0]['first_name']; ?>
                                    <?php
                                    if (strlen($requestClients[$i]['designer'][0]['last_name']) > 5) {
                                     echo ucwords(substr($requestClients[$i]['designer'][0]['last_name'], 0, 1));
                                 } else {
                                     echo $requestClients[$i]['designer'][0]['last_name'];
                                 }
                                 ?>

                             </p>
                             <p class="pro-b">Designer</p>
                             <p class="space-a"></p>
                             <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $requestClients[$i]['id']; ?>">
                                <span class="sma-red">+</span> Add Designer
                            </a>
                        </div>
                    </div> 
                </div>
            <?php } else { ?>
                <div class="cli-ent-xbox">
                    <a href="#" class="upl-oadfi-le noborder permanent_assign_design assign_de" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $requestClients[$i]['id']; ?>">
                        <span class="icon-crss-3">
                            <span class="icon-circlxx55 margin5">+</span>
                            <p class="attachfile">Assign designer<br> permanently</p>
                        </span>
                    </a>
                </div>
            <?php } ?>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="viewinfo<?php echo $requestClients[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="cli-ent-model-box">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="cli-ent-model">
                            <header class="fo-rm-header">
                                <h3 class="head-c">View Information</h3>
                            </header>
                            <div class="fo-rm-body">
                                <div class="projects">
                                    <ul class="projects_info_list">
                                        <li><div style="float: left;"><b>Active Projects</b></div><div style="float: right;"><?php echo $i; ?></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="viewinfo<?php echo $requestClients[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="cli-ent-model-box">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="cli-ent-model">
                            <header class="fo-rm-header">
                                <h3 class="head-c">View Information</h3>
                            </header>
                            <div class="fo-rm-body">
                                <div class="projects">
                                    <ul class="projects_info_list">
                                        <li>
                                            <div class="starus"><b>Active Projects</b></div>
                                            <div class="value"><?php echo $requestClients[$i]['active']; ?>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>In -Queue Projects</b></div>
                                        <div class="value"><?php echo $requestClients[$i]['inque_request']; ?>
                                    </div>
                                </li>
                                <li>
                                    <div class="starus"><b>Revision Projects</b></div>
                                    <div class="value"><?php echo $requestClients[$i]['revision_request']; ?>
                                </div>
                            </li>
                            <li>
                                <div class="starus"><b>Review Projects</b></div>
                                <div class="value"><?php echo $requestClients[$i]['review_request']; ?>
                            </div>
                        </li>
                        <li>
                            <div class="starus"><b>Complete Projects</b></div>
                            <div class="value"><?php echo $requestClients[$i]['complete_request']; ?>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="<?php echo $requestClients[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Add Client</h3>
                    </header>
                    <div class="fo-rm-body">
                        <form onSubmit="return EditFormSQa(<?php echo $requestClients[$i]['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/dashboard/edit_client/<?php echo $requestClients[$i]['id']; ?>">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">First Name</label>
                                        <p class="space-a"></p>
                                        <input type="text" name="first_name" required class="efirstname form-control input-c" value="<?php echo ucwords($requestClients[$i]['first_name']); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Last Name</label>
                                        <p class="space-a"></p>
                                        <input type="text" name="last_name" required class="elastname form-control input-c" value="<?php echo ucwords($requestClients[$i]['last_name']); ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Email</label>
                                        <p class="space-a"></p>
                                        <input type="email" name="email" required  class="eemailid form-control input-c" value="<?php echo $requestClients[$i]['email']; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Phone</label>
                                        <p class="space-a"></p>
                                        <input type="tel" name="phone" required class="ephone form-control input-c" value="<?php echo $requestClients[$i]['phone']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group goup-x1">
                                <label class="label-x3">State</label>
                                <p class="space-a"></p>
                                <input type="text" name="state"  required class="form-control input-c epassword" value="<?php echo ucwords($requestClients[$i]['state']); ?>">
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x3">Password</label>
                                <p class="space-a"></p>
                                <input type="password" name="password"   class="form-control input-c epassword" >
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x3">Confirm Password</label>
                                <p class="space-a"></p>
                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword" >
                            </div>
                            <div class="epassError" style="display: none;">
                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                            </div>
                            <hr class="hr-b">
                            <p class="space-b"></p>

                            <div class="form-group goup-x1">
                                <label class="label-x3">Select payment Method</label>
                                <p class="space-a"></p>
                                <select class="form-control select-c">
                                    <option>Credit/Debit Card.</option>
                                    <option>Credit/Debit Card.</option>
                                    <option>Credit/Debit Card.</option>
                                </select>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x3">Card Number</label>
                                <p class="space-a"></p>
                                <input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx" name="Card Number" required>
                            </div>

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Expiry Date</label>
                                        <p class="space-a"></p>
                                        <input type="text" name="Expiration Date" required class="form-control input-c" placeholder="22.05.2022">
                                    </div>
                                </div>

                                <div class="col-sm-3 col-md-offset-4">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">CVC</label>
                                        <p class="space-a"></p>
                                        <input type="text" name="CVC" required class="form-control input-c" placeholder="xxx" maxlength="4">
                                    </div>
                                </div>
                            </div> 
                            <p class="space-b"></p>
                            <p class="btn-x"><button type="submit" class="btn-g">Add Customer</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> <!-- End Row -->
<?php endfor; ?>