<?php 
//echo "url_from_client".$url_from_client."<br/>";
//echo "url_from_designer".$url_from_designer."<br/>";

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google" content="notranslate">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin Portal | Graphics Zoo</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="preload" href="//fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" as="style" rel = 'stylesheet'>
        <!-- Bootstrap -->
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/bootstrap.min.css');?>" rel="stylesheet">
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/reset.css');?>" rel="stylesheet">
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/editor.css');?>" rel="stylesheet">
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/adjust.css');?>" rel="stylesheet">
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'css/admin/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/responsive.css');?>" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/css/toastr.css">
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/editor/editor.css');?>" type="text/css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/ui-slider/jquery.ui-slider.css');?>" />
        <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'css/custom_style_for_all.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"/>

        
        <script href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
        <?php
        $url = current_url();
        $messageurl = explode('/', $url);
        $activeurl = parse_url($url);
        if (in_array("message_list", $messageurl)) {
            ?>
            <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/messages.css');?>" rel="stylesheet">
            <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
            <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
        <?php } ?>
    
    </head>
    <?php
    $CI = & get_instance();
    $CI->load->library('customfunctions');
    $CI->load->library('myfunctions');
    $CI->load->helper('notifications');
    $helper_data = helpr();
    $edit_profile = $helper_data['profile_data'];
//echo "<pre/>";print_R($edit_profile);
    $messagenotifications = $helper_data['messagenotifications'];
    $messagenotification_number = $helper_data['messagenotification_number'];
    $notifications = $helper_data['notifications'];
    $notification_number = $helper_data['notification_number'];
    ?>
    <body class="deshboard-bgnone" style="background: #f2f2f2;">
        <header class="nav-section">
            <div class="container-fluid">
                <nav class="navbar navbar-default flex-show centered"> 
                    <div class="navbar-header">
                        <ul class="gz-breadcrumb">
                            <?php
                            //echo "<pre/>";print_R($breadcrumbs);
                            foreach ($breadcrumbs as $name => $url) {
                                ?>
                                <li><a href="<?php echo $url; ?>"><?php echo $name; ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="box-setting">
                        <ul id="right_nav" class="list-unstyled list-setting">
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="mess-box">
                                        <i class="icon-gz_message_icon"></i>
                                        <?php
                                        if (isset($messagenotifications)) {
                                            if (sizeof($messagenotifications) > 0) {
                                                ?>
                                                <span class="numcircle-box">
                                                    <!-- <?php echo $messagenotification_number; ?> -->
                                                </span>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </span>
                                </a>
                                <div class="dropdown-menu">
                                    <h3 class="head-notifications">Messages</h3>
                                    <ul class="list-unstyled list-notificate">
                                        <?php if (sizeof($messagenotifications) > 0) { ?>
                                            <?php for ($i = 0; $i < sizeof($messagenotifications); $i++) { ?>
                                                <li><a href="<?php echo base_url() . $messagenotifications[$i]['url']; ?>">
                                                        <div class="setnoti-fication">
                                                            <figure class="pro-circle-k1">
                                                                <img src="<?php echo $messagenotifications[$i]['profile_picture']; ?>" class="img-responsive msg">
                                                            </figure>

                                                            <div class="notifitext">
                                                                <h5 style="font-size: 13px;"><?php echo $messagenotifications[$i]['heading']; ?></h5>
                                                                <!--small style="font-size: 12px;"--><?php
                                                                if (strlen($messagenotifications[$i]['title']) >= 50) {
                                                                    echo substr($messagenotifications[$i]['title'], 0, 40) . "..";
                                                                } else {
                                                                    echo $messagenotifications[$i]['title'];
                                                                }
                                                                ?>
                                                                <p class="ntifittext-z2">
                                                                    <?php
                                                                    $approve_date = $messagenotifications[$i]['created'];
                                                                    $msgdate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                                                    echo $msgdate;
                                                                    ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </a></li>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <li style="padding:10px;">No new message</li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>
                            <!-- Message Section End -->
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="bell-box">
                                        <i class="icon-gz_bell_icon"></i>
                                        <?php
                                        if (isset($notifications)) {
                                            if (sizeof($notifications) > 0) {
                                                ?>
                                                <span class="numcircle-box">
                                                </span>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </span>
                                </a>
                                <div class="dropdown-menu">
                                    <h3 class="head-notifications">Notifications</h3>
                                    <ul class="list-unstyled list-notificate">
                                        <?php if (sizeof($notifications) > 0) { ?>
                                            <?php for ($i = 0; $i < sizeof($notifications); $i++) { ?>
                                                <li><a href="<?php echo base_url() . $notifications[$i]['url']; ?>">
                                                        <div class="setnoti-fication">
                                                            <figure class="pro-circle-k1">
                                                                <img src="<?php echo $notifications[$i]['profile_picture']; ?>" class="img-responsive">
                                                            </figure>

                                                            <div class="notifitext" title="<?php echo $notifications[$i]['title']; ?>">
                                                                <p class="ntifittext-z1"><strong><?php echo $notifications[$i]['first_name'] . " " . $notifications[$i]['last_name']; ?></strong>
                                                                    <?php echo $notifications[$i]['title']; ?>
                                                                </p>
                                                                <p class="ntifittext-z2">
                                                                    <?php
                                                                    $approve_date = $notifications[$i]['created'];
                                                                    $notidate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                                                    echo $notidate;
                                                                    ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </a></li>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <li style="padding:10px; font: normal 12px/25px 'GothamPro', sans-serif;">No New notification</li>
                                        <?php } ?>

                                    </ul>
                                    <a href="<?php echo base_url(); ?>admin/Contentmanagement/notifications" class="all-nitify">View all notifications</a>
                                </div>
                            </li>

                            <li>
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expended="false">
                                    <figure class="pro-circle-img-xxs">
                                        <img src="<?php echo $edit_profile[0]['profile_picture']; ?>" class="img-responsive">
                                    </figure>
                                </a>
                                <div class="dropdown-menu pull-right profile-dropdown" aria-labelledby="dropdownMenuButton">
                                    <h3 class="head-notifications"><?php echo $edit_profile[0]['first_name'] . " " . $edit_profile[0]['last_name']; ?></h3>
                                    <h2 class="head-notifications"><?php echo $edit_profile[0]['email']; ?></h2>
                                    <div class="profile_wrapper">
                                        <ul class="list-unstyled list-notificate">
                                            <li><a href="<?php echo base_url(); ?>admin/dashboard/reports"><i class="far fa-calendar-alt"></i> Reports</a></li>
                                            <li><a href="<?php echo base_url(); ?>admin/dashboard/adminprofile"><i class="fas fa-cog"></i> Settings</a></li> 
                                            <li><a href="<?php echo base_url(); ?>admin/dashboard/logout"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>                        
                        </ul>
                    </div>
                </nav>
            </div><!-- /.container-fluid -->

        </div>
    </header>
    <div class="admin-wraper">
        <div class="admin-sidebar">
            <?php $this->load->view('admin/admin_sidebar', array('profileinfo' => $edit_profile,"admin_role_prmsn" => $helper_data["role_permissions"])); ?>            
        </div>
        <script>
            var BASE_URL = "<?php echo base_url(); ?>";
            var timezone = '<?php echo isset($login_user_data[0]['timezone']) ? $login_user_data[0]['timezone'] : $_SESSION['timezone']; ?>';
            var $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
            var $asset_path = '<?php echo FS_PATH_PUBLIC_ASSETS; ?>';
            var $request_path = '<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS; ?>';
            var subscription_plan = '<?php echo json_encode(SUBSCRIPTION_DATA); ?>';
            var total_active_requests = "<?php echo TOTAL_ACTIVE_REQUEST; ?>";
            var BUCKET_TYPE, ASSIGN_PROJECT = '';
            var dash_client_id = "<?php echo isset($_GET['client_id']) ? $_GET['client_id'] : "" ?>";
            var dash_designer_id = "<?php echo isset($_GET['designer_id']) ? $_GET['designer_id'] : "" ?>";
            var comefrm = '';
            var requestmainchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTMAINCHATFILES; ?>";

        </script>


