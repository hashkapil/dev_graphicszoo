<style>
h2.brnd-info-section {
    font: normal 21px/25px 'GothamPro-Medium', sans-serif;
    font-weight: 600;
    margin-bottom: 10px;
}
a.view-materials:hover{
	    color: #e73250;
}
.product-list-show h3 {
    color: #e52344;
    text-transform: uppercase;
}
.brand_profile_row .cell-row {
    font: normal 15px/25px 'GothamPro', sans-serif;
}
.brand_profile_row .cell-row a.view-materials {
    background: #e52344;
    color: #fff;
    width: 100px;
    display: block;
    text-align: center;
    line-height: 29px;
    border-radius: 30px;font-size: 13px;
    text-decoration: none;
}
.brand_info_not_found {
    background: #f5f5f5;
    padding: 80px;
    text-align: center;
    border: 1px solid #ccc;
    border-radius: 12px;
    font-family: GothamPro, sans-serif;
    color: #e73250;
    margin-top: 30px;
}
</style>
<section class="con-b">
<div class="container">
 <h2 class="brnd-info-section"> Brand Profiles </h2>	
<div class="product-list-show">
	<div class="row">  
   <?php if(sizeof($brand_profiles) > 0){ ?>
		<!-- Start Row -->
		<div class="cli-ent-row tr brdr">
			<div class="cli-ent-col td" style="width: 25%;">
				<div class="cli-ent-xbox text-left">
				<div class="cell-row">

				<div class="cell-col">
				<h3 class="pro-head space-b">Brand Title</h3>
				
				</div>
				</div>
				</div>
			</div>
		<div class="cli-ent-col td" style="width: 25%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Website Url</h3>
			
			</div>
			</div>
			</div>
		</div>

		<div class="cli-ent-col td" style="width: 25%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Description</h3>
			
			</div>
			</div>
			</div>
		</div>
		<div class="cli-ent-col td" style="width: 25%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Other Info</h3>
			
			</div>
			</div>
			</div>
		</div>
		</div> <!-- End Row -->
		<div class="brand_profile_row">
		<?php for($i=0; $i<sizeof($brand_profiles);$i++){ ?>
		<!-- Start Row -->
		<div class="cli-ent-row tr brdr">
			<div class="cli-ent-col td" style="width: 25%;">
				<div class="cli-ent-xbox text-left">
				<div class="cell-row">

				<?php echo $brand_profiles[$i]['brand_name']; ?>
				
				</div>
				</div>
			</div>
		<div class="cli-ent-col td" style="width: 25%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<?php echo $brand_profiles[$i]['website_url']; ?>
			
			</div>
			</div>
		</div>

		<div class="cli-ent-col td" style="width: 25%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			 
			<?php echo $brand_profiles[$i]['description']; ?>
			
			</div>
			</div>
		</div>
		<div class="cli-ent-col td" style="width: 25%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b" style="cursor: pointer;"><a href="<?php echo base_url(); ?>admin/dashboard/brand_materials/<?php echo $brand_profiles[$i]['id']; ?>" class="view-materials">View </a></h3>
			
			</div>
			</div>
			</div>
		</div>
		</div> <!-- End Row -->
		<?php } ?>

	</div>
        <?php } else { ?>
        <div class="brand_info_not_found">  
	<h3>Brand Profile Not Found</h3>
        </div>
	<?php } ?>
	</div>
</div>
</div>
</section>
	<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script>
jQuery(document).on('click','.box-setting ul#right_nav li',function(){
                   $(this).toggleClass('open'); 
                });
</script>