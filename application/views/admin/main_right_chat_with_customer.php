<?php //echo "<pre/>";print_R($chat_with_customer);
$docArrOffice = array('doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt', 'ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
$font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
$vedioArr = array('mp4', 'Mov');
$audioArr = array('mp3');
$ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');
$zipArr = array('zip', 'rar');
?>
<div class="msgk-chatrow clearfix">
    <div class="msgk-user-chat msgk-right <?php echo $chat_with_customer['sender_type']; ?>">
        <div class="time-edit">
            <p class="time_msg"> <?php echo $chat_with_customer['chat_created_time']; ?></p>
            <?php if ($chat_with_customer['is_deleted'] != 1) { ?>
                <div class="editDelete editdeletetoggle_<?php echo $chat_with_customer['id']; ?>">
                    <i class="fas fa-ellipsis-v openchange" data-chatid="<?php echo $chat_with_customer['id']; ?>"></i>
                    <div class="open-edit open-edit_<?php echo $chat_with_customer['id']; ?>" style="display:none">
                        <?php if($chat_with_customer['is_filetype'] != 1){ ?>
                        <a href="javascript:void(0)" class="editmsg" data-editid="<?php echo $chat_with_customer['id']; ?>">edit</a>
                        <?php } ?>
                        <a href="javascript:void(0)" class="deletemsg" data-delid="<?php echo $chat_with_customer['id']; ?>">delete</a>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="msgk-mn-edit edit_main_<?php echo $chat_with_customer['id']; ?>" style="display:none">
            <form method="post" id="editMainmsg">
                <textarea class="pstcmm sendtext" id="edit_main_msg_<?php echo $chat_with_customer['id']; ?>"><?php echo $chat_with_customer['message']; ?></textarea> 
                <a href="javascript:void(0)" class="edit_save"  data-id="<?php echo $chat_with_customer['id']; ?>">Save</a> 
                <a href="javascript:void(0)" class="cancel_main" data-msgid="<?php echo $chat_with_customer['id']; ?>">Cancel</a>
            </form>
        </div>
        <div class="msgk-mn msg_desc_<?php echo $chat_with_customer['id']; ?>">
            <div class="msgk-umsgbox">
                <?php
                $varmsg = '';
                if ($chat_with_customer['is_deleted'] == 1 && $chat_with_customer['is_edited'] != 1 || ($chat_with_customer['is_edited'] == 1 && $chat_with_customer['is_deleted'] == 1)) {
                    $varmsg = '<div class="edited_msg deleted_msg"><i class="fa fa-ban" aria-hidden="true"></i> You have deleted this message.</div>';
                } elseif ($chat_with_customer['is_edited'] == 1 && $chat_with_customer['is_deleted'] != 1) {
                    $varmsg = '<div class="edited_msg">' . $chat_with_customer['message'] . '</div>';
                } else {
                    $varmsg = $chat_with_customer['message'];
                }

                if ($chat_with_customer['is_filetype'] == '1' && $chat_with_customer['is_deleted'] != 1) {

                    $type = strtolower(substr($chat_with_customer['message'], strrpos($chat_with_customer['message'], '.') + 1));
                    $updateurl = '';
                    if (in_array($type, $docArrOffice)) {
                        $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                        $class = "doc-type-file";
                        $exthtml = '<span class="file-ext">' . $type . '</span>';
                    } elseif (in_array($type, $font_file)) {
                        $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                        $class = "doc-type-file";
                        $exthtml = '<span class="file-ext">' . $type . '</span>';
                    } elseif (in_array($type, $zipArr)) {
                        $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-zip.svg';
                        $class = "doc-type-zip";
                        $exthtml = '<span class="file-ext">' . $type . '</span>';
                    } elseif (in_array($type, $ImageArr)) {
                        $imgVar = imageRequestchatUrl($type, $chat_with_customer['request_id'], $chat_with_customer['message']);
                        $updateurl = '/_thumb';
                        $class = "doc-type-image";
                        $exthtml = '';
                    }
                    $basename = $chat_with_customer['message'];
                    $basename = strlen($basename) > 20 ? substr($basename, 0, 20) . "..." . $type : $basename;
//                             FS_PATH_PUBLIC_UPLOADS.'requestmainchat/'.$chat_with_customer['request_id'].'/_thumb/'.$chat_with_customer['message']
                    $data_src = FS_PATH_PUBLIC_UPLOADS.'requestmainchat/'.$chat_with_customer['request_id'].'/'.$chat_with_customer['message'];
                    ?>
                    <span class="msgk-umsxxt took_<?php echo $chat_with_customer['id']; ?>">
                        <div class="contain-info <?php echo (!in_array($type, $ImageArr)) ? 'only_file_class' : ''; ?>">
                            <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>">
                                <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                            </a>
                            <div class="download-file">
                                <div class="file-name"><h4><b><?php echo $basename; ?></b></h4></div>
                                <a href="<?php echo base_url() ?>/customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" target="_blank">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                                </a>
                            </div>
                        </div>
                    </span>
                <?php } else { ?>
                    <pre>
                                                    <span class="edit_icon_<?php echo $chat_with_customer['id']; ?> edit_icon_msg">
                            <?php if ($chat_with_customer['is_edited'] == 1 && $chat_with_customer['is_deleted'] != 1) { ?>
                                                                                                <i class="fas fa-pen"></i>
                            <?php } ?>
                                                    </span>
                                                    <span class="msgk-umsxxt took_<?php echo $chat_with_customer['id']; ?> message-text"><?php echo $varmsg; ?></span>
                    </pre>
                <?php } ?>
            </div>  
        </div>

    </div>
</div>