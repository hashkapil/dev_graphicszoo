<style type="text/css">
   .col-md-12 {
    width: 100% !important;
	    margin: 20px;
}
h2.brnd-info-section {
    font: normal 21px/25px 'GothamPro-Medium', sans-serif;
    margin-bottom: 10px;
}
.product-list-show .logo-brands, .marketing_materials-brands, .product-list-show .additional_images-brands {
    background: #f7f7f7;
    border: 1px solid #ccc;
    border-radius: 12px;    padding: 10px 0px 20px 20px !important;
}
.brand_info_not_found {
    background: #f5f5f5;
    padding: 80px;
    text-align: center;
    border: 1px solid #ccc;
    border-radius: 12px;
    font-family: GothamPro, sans-serif;
    color: #e73250;
    margin-top: 30px;
}
</style>

<section class="con-b">
	<div class="container">
	 <h2 class="brnd-info-section"> Brand Profile Info </h2>
	<div class="product-list-show">
	<div class="row">
	<?php //echo "<pre>";print_r($materials_files); ?>
	<?php if(sizeof($materials_files) > 0){ ?>
	<div class="col-md-12 logo-brands">
	<h3 class="pro-head space-b">Logo</h3>
	<div class="">
	<?php for($i=0; $i<sizeof($materials_files);$i++){ 
	if($materials_files[$i]['file_type'] == 'logo_upload') { 
            $type = substr($materials_files[$i]['filename'], strrpos($materials_files[$i]['filename'], '.') + 1); 
            if ($type == "pdf") {
                ?>
                <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "zip") {
                ?>
                <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "doc" || $type == "docx") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/docx.png" height="150"/>
                        </a>
                    </div>
               </div>
            <?php } elseif ($type == "mov" || $type == "mp4") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "odt" || $type == "ods") {
                ?>
                <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "psd") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "rar") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/rar.jpg" height="150"/>
                        </a>
                    </div>
               </div>
            <?php } elseif ($type == "ai") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/ai.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } else { ?>
	<div class="col-md-3"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/'.$materials_files[$i]['filename']; ?>" alt="logo-brand"></div>
	<?php } 
	}
        } ?>
	</div>
	</div>
	
	<div class="col-md-12 marketing_materials-brands">
	<h3 class="pro-head space-b">Market Materials</h3>
	<div class="">
	<?php for($i=0; $i<sizeof($materials_files);$i++){ 
	if($materials_files[$i]['file_type'] == 'materials_upload') { 
            $type = substr($materials_files[$i]['filename'], strrpos($materials_files[$i]['filename'], '.') + 1); 
            if ($type == "pdf") {
                ?>
               <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "zip") {
                ?>
                <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                        </a>
                    </div>
               </div>
            <?php } elseif ($type == "doc" || $type == "docx") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/docx.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "mov" || $type == "mp4") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/'. $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "odt" || $type == "ods") {
                ?>
               <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                        </a>
                    </div>
               </div>
            <?php } elseif ($type == "psd") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "rar") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/rar.jpg" height="150"/>
                        </a>
                    </div>
               </div>
            <?php } elseif ($type == "ai") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/ai.png" height="150"/>
                        </a>
                    </div>
               </div>
            <?php } else { ?> 
	<div class="col-md-3"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/'.$materials_files[$i]['filename']; ?>" alt="marketing_materials-brand"></div>
	<?php }
        }
	} ?>
	</div>
	</div>
	
	<div class="col-md-12 additional_images-brands">
	<h3 class="pro-head space-b">Additional Images</h3>
	<div class="">
	<?php for($i=0; $i<sizeof($materials_files);$i++){ 
	if($materials_files[$i]['file_type'] == 'additional_upload') { 
           $type = substr($materials_files[$i]['filename'], strrpos($materials_files[$i]['filename'], '.') + 1); 
            if ($type == "pdf") {
                ?>
                <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "zip") {
                ?>
                <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                        </a>
                    </div>
                 </div>
            <?php } elseif ($type == "doc" || $type == "docx") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/docx.png" height="150"/>
                        </a>
                    </div>
                 </div>
            <?php } elseif ($type == "mov" || $type == "mp4") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/'. $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>
                        </a>
                    </div>
                 </div>
            <?php } elseif ($type == "odt" || $type == "ods") {
                ?>
                <div class="col-md-3">
                    <div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "psd") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>
                        </a>
                    </div>
                </div>
            <?php } elseif ($type == "rar") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/rar.jpg" height="150"/>
                        </a>
                    </div>
                 </div>
            <?php } elseif ($type == "ai") { ?>
                <div class="col-md-3"><div class="accimgbx33">
                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/' . $materials_files[$i]['filename']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/ai.png" height="150"/>
                        </a>
                    </div>
                 </div>
            <?php } else { ?> 
	<div class="col-md-3"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$brand_id.'/'.$materials_files[$i]['filename']; ?>" alt="additional_images-brand"></div>
	<?php } 
        }
	} ?>
	</div>
	</div>
	<?php } else { ?>
            <div class="brand_info_not_found">  
	<h3>Brand Additional Info Not Found</h3>
            </div>
	<?php } ?>
	</div>
	</div>
	</div>
</section>