<section class="con-b">
    <div class="header-blog">
        <input type="hidden" value="" class="bucket_type">
        <div class="row flex-show">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading">Checkin Users</h2>
                      <div class="header_searchbtn">
    <div class="search-first">
        <div class="focusout-search-box">
            <div class="search-box view-client">
                <form method="post" class="search-group clearfix">
                    <input type="text" placeholder="Search here..." class="form-control searchdata searchdata_client search_text search_checkins search_chck_text">
                    <div class="ajax_searchload" style="display:none;">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                    </div>
                    <input type="hidden" name="status"  value="active,disapprove,assign,pending,checkforapprove">
                    <div class="close-search"><i class="far fa-eye-slash"></i></div>
                    <button type="submit" class="search-btn search search_data_ajax">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                    </button>
                </form>
            </div>
        </div>
    </div>
    </div>
                </div>
            </div>
        </div>
    </div>
<!--    <input type="text" placeholder="Search here..." class="form-control search_checkins search_chck_text">-->
    <div class="header-blog">
    <div class="flex-this">
  
    </div>
    </div>
    
    <div class="product-list-show" id="Refund_taxes" data-total-count="<?php echo $count_checkInUsers; ?>" data-loaded="0" data-group="1">
        <div class="cli-ent-row tr tableHead">
            <div class="cli-ent-col td" style="width: 20%">Customer Name</div>
            <div class="cli-ent-col td" style="width: 20%">Customer Plan</div>
            <div class="cli-ent-col td" style="width: 10%">Active/Total Projects</div>
            <div class="cli-ent-col td" style="width: 10%">Last Login</div>
            <div class="cli-ent-col td" style="width: 10%">Average Feedback</div>
            <div class="cli-ent-col td" style="width: 20%">Last Checkin</div>
            <div class="cli-ent-col td" style="width: 20%">Action</div>
        </div>
        <div class="row two-can">
        <?php
        foreach ($checkInUsers as $kk => $vv) { ?>
            <?php 
              $this->load->view('admin/checkin_listing_temp',array('data' => $vv));
        } ?>
        </div>
        <?php 
          if ($count_checkInUsers > LIMIT_ADMIN_LIST_COUNT) { ?>
            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" />
            </div>
            <div class="" style="text-align:center">
                <a id="load_more_checkin_client" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_checkInUsers; ?>" class="load_more button">Load more</a>
            </div>
        <?php } ?>
    </div>
</section>
<!---****************Checkin history **************--->
<div class="modal fade slide-3 model-close-button in" id="show_checkin_history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="popup_h2 del-txt historyuser"> Checkin history<span></span></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="add_notes_sec check-history-table">
                    <table class="table table-dark">
                        <thead>
                          <tr>
                            <th scope="col">Checkin by</th>
                            <th scope="col">Checkin date</th>
                          </tr>
                        </thead>
                        <tbody class="usercheckin_list"></tbody>
                      </table>
<!--                 <div class="usercheckin_list"></div>-->
             </div>
         </div>
     </div>
 </div>
</div>
<!----------------------------Checkin Notesssss------------------------------->
<div class="modal fade slide-3 model-close-button in" id="show_checkin_notes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="popup_h2 del-txt notetitle"> Add Note of <span class="notes_of"></span></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="add_notes_sec ">
                    <div class="text-aprt">
                     <textarea type="text" class="pstcmm t_w" id="chknotes_txtmsg" placeholder="Type a note..."></textarea>
                     <span class="cmmsend">
                         <button class="add_chcknotemsg" data-sender_id="<?php echo $login_user_id; ?>" data-user_id="0">
                             <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat-send.png" class="img-responsive" id="add_cknotes_btn" data-sender_id="<?php echo $login_user_id; ?>">
                         </button>
                     </span>
                 </div>
                 <div class="chkusernotes_list usernotes_list">
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
<!--**********************rating details*************************-->
<div class="modal fade slide-3 model-close-button in" id="show_rating_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="popup_h2 del-txt notetitle"> Rating Details<span></span></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body check-history-table">
                <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">Project Name</th>
                    <th scope="col">Design Name</th>
                    <th scope="col">Design Rating</th>
<!--                    <th scope="col">Rating Text</th>-->
                  </tr>
                </thead>
                <tbody class="rating_details_listt ">
                </tbody>
              </table>
         </div>
     </div>
 </div>
</div>
<script>
var $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
</script>