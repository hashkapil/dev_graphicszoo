<?php
$ext = array('jpg', 'JPG', 'png' ,'PNG' ,'jpeg' ,'JPEG','gif','GIF','bmp','BMP');

// $CI = & get_instance();
// $CI->load->library('customfunctions');


?>
<section class="con-b">

    <div class="gz ajax_loader" style="display:none;text-align:center">
       <img src="<?php echo base_url();?>/public/assets/img/gz-ajax-loader.gif"> 
  </div>
  <input type="hidden" id="is_filter" value="0" data-append="50" data-load="0" >
    <div class="container-fluid">
        <div class="row_msg">
            <div class="alert alert-danger alert-dismissable error" style="display:none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c" id="show_error"></p>
            </div>
            <div class="alert alert-success alert-dismissable success" style="display:none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"></p>
            </div>
        </div>
        <div class="header-blog">
            <input type="hidden" value="" class="bucket_type"/>
            <div class="row flex-show">
                <div class="col-md-12">
                    <div class="flex-this project_side_filter">
                        <h2 class="main_page_heading">Projects Designs</h2>


                        <div class="header_searchbtn"> 
                           
                           

                            <div class="filter-site">
                                <i class="fas fa-sliders-h"></i>
                            </div>

                            <div class="gz-filter">
                                <div class="filter-header">
                                    <h2>Filter</h2>
                                    <a href="javascript:void(0)" class="close-filter">x</a>
                                </div>
                                <!-- <form method="Post" action="<?php// echo base_url().'admin/contentmanagement/project_design_filters'?>"> -->
                                    <div class="status-filter">
                                            
                                            <div class="sound-signal2">
                                                <div class="form-radion2">
                                                    <label class="containerr">Approved Design By Client
                                                        <input id="approve_by_client" type="checkbox" name="approved_designs_client" value="active">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div> 
                                            </div>
                                        </div>
                                    <div class="filter-body">
                                        <div class="frmSearch">
                                            <input type="text" placeholder="search client" name="client_name" id="search_client_filter" size="50"/>
                                            <input type="hidden"  name="client_id" id="client_id" />
                                            <div id="suggesstion-box-filter">
                                                <ul class="search_result list-unstyled"></ul></div>
                                        </div>
                                        <br/>
                                        <select name="designer" id="choose-designer" class="designers_show">
                                            <option value="">Choose Designers</option>
                                            <?php foreach ($extra_data['designer'] as $key => $d_info) {?>   
                                            <option value="<?php echo $d_info['id']; ?>">
                                                <?php echo $d_info['name']; ?>
                                            </option>
                                            <?php } ?>
                                        </select>

                                        <!--select name="category" id="choose-category" class="designers_show">
                                            <option value="">Choose Category</option>
                                            <?php foreach ($extra_data['category'] as $key => $c_info) { ?>   
                                            <option value="<?php echo $c_info['id'];?>"><?php echo $c_info['name'];?></option>
                                             <?php }?>
                                        </select-->

                                        <select name="sub-category" id="choose-sub-category" class="designers_show">
                                             <option value="">Choose Sub-category</option>

                                             <?php foreach ($extra_data['category'] as $key => $c_info) { ?>
                                             <optgroup label="<?php echo $c_info['name'];?>">
                                                <?php 
                                                   foreach ($c_info['child'] as $key => $s_info) {?>  
                                                    <option value="<?php echo $s_info['id'];?>">
                                                        <?php echo $s_info['name'];?>
                                                    </option>
                                                <?php }  ?>
                                             </optgroup>   
                                            <!--option value="<?php echo $c_info['id'];?>"><?php echo $c_info['name'];?></option-->
                                             <?php }?>
                                             
                                        </select>
                                        
                                        
                                          <a href="#" class="addderequesr">
                                                <span class="">Select Range For Approve Designs</span>
                                            </a>
                                        
                                        <div class="status-filter date-filter">
                                            <!-- <h4>Task Time</h4> -->
                                            <div class="date-stand col-md-6">
                                                <div class="date-from">
                                                    <label>From</label>
                                                    <input id="date-from" type="date" class="week-picker hasDatepicker">
                                                </div>
                                            </div>
                                             <div class="date-stand col-md-6">
                                                <div class="date-from">
                                                    <label>To</label>
                                                    <input id="date-to"  type="date" class="week-picker hasDatepicker">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="filter_button">
                                            <input type="button" name="sumbit" id="filter_apply" value="Apply Filter" class="apply_filter">
                                            <!--<a href="" class="more_load">Apply Filter</a>-->
                                            <a href="<?php echo base_url().'admin/project-designs' ?>" class="reset">Reset</a>
                                        </div>
                                    </div>
                                <!-- </form> -->
                            </div>

                        </div>
                    </div>
                        
                                 <!-- Div for filters tags  start-->
                                 <!-- <div id="filter_tags"class="filter_list">
                                    <div class="filter_items">
                                        <span>lorem ispum <i class="fa fa-times" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div> -->
                                <!-- Div for filters tags  end -->

                </div>
            </div>
        </div>
        
        <div class="cli-ent table">
            <div class="tab-content">
<input type="hidden" id="is_filter_request" value="0">
                <div id="images-grid" class="row images-grid">
                    <?php 
                        $lazy = FS_PATH_PUBLIC_ASSETS.'img/lazy.png'; 
                    foreach($data as $key => $info) { 
                        $extension = pathinfo($info['file_name']); 
                        if(in_array($extension['extension'],$ext)){
                             
                            $url = FS_PATH_PUBLIC_UPLOADS_REQUESTS.$info['rqid'].'/_thumb/'.$info['file_name'];
                            $downloadurl = FS_PATH_PUBLIC_UPLOADS_REQUESTS.$info['rqid'].'/'.$info['file_name'];

                        }else{
                            $downloadurl = FS_PATH_PUBLIC_UPLOADS_REQUESTS.$info['rqid'].'/'.$info['file_name'];
                            $url =base_url().'public/assets/img/gz_icons/gz_project_thumbnail.svg'; 
                        }
                        $source_url = FS_PATH_PUBLIC_UPLOADS_REQUESTS.$info['rqid'].'/'.$info['src_file'];
                        $project_url = base_url().'admin/dashboard/view_request/'.$info['rqid'];

                        $preview_url = base_url().'admin/dashboard/view_files/'.$info['id'].'?id='.$info['rqid'];
                        ?>
                    <div class="col-md-2">
                        <div class="images_grid_img">
                          <a class="fancybox"   data-fancybox="images" href="<?php echo $url; ?>" rel="gallery1"> 
                             <img class="lazy" src="<?php echo $lazy; ?>" data-original="<?php echo $url; ?>">
                         </a>
                            <div class="grid_action">
                                
                             
                                <div class="dropdown">

                                <button class="show_action_change" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                   <a class="dropdown-item" href="<?php echo base_url();?>admin/contentmanagement/donwload_data?url=<?php echo $downloadurl;?>">
                                    <i class="fa fa-download" aria-hidden="true"></i> Download Preview
                                </a>
                                <a class="dropdown-item" href="<?php echo base_url();?>admin/contentmanagement/donwload_data?url=<?php echo $source_url;?>" class="reddelete" download>
                                    <i class="fa fa-download" aria-hidden="true"></i> Download Source
                                </a>
                                <a class="dropdown-item" href="<?php echo $project_url;?>" target="_blank">
                                    <i class="fa fa-link"></i>GO to Project
                                </a>
                                <a class="dropdown-item" href="<?php echo $preview_url;?>" target="_blank">
                                    <i class="fa fa-link"></i>Go to Design
                                </a> 
                                </div>
                            </div> 
                        </div>
                        </div>
                    </div>
                <?php } ?>

                    
                </div>
                <div class="load_more_projects text-center">
                    <div class="ajax_searchload" style="display:none;">
                        <img src="<?php echo base_url(); ?>/public/assets/img/ajax-loader.gif">
                 </div>
                    
                <button type="button" class="apply_filter" id="load_more_projects" data-append="50" data-load="<?php echo count($data);?>">Load more</button>
                </div>
                 
            </div>                   
        </div>        
    </div>
</section>

<div id="results"></div>  
<style type="text/css">
    .apply_filter{
        width: 150px;
        height: 40px;
        background: #fd2041 !important;
        color: #fff;
        font-size: 12px;
        text-transform: uppercase;
        font-weight: 400;
        letter-spacing: 1px;
        display: inline-block;
        line-height: 40px;
        border-radius: 4px;
        border: 0px;
        text-align: center;
    }
</style>
 
 
