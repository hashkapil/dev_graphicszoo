<section class="con-b">
    <div class="header-blog">
        <input type="hidden" value="" class="bucket_type">
        <div class="row flex-show">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading">Change Designer Request</h2>
                </div>
            </div>
        </div>
    </div>


    <div class="product-list-show" id="Refund_taxes">
        <div class="cli-ent-row tr tableHead">
            <div class="cli-ent-col td" style="width: 20%">Customer Name</div>
            <div class="cli-ent-col td" style="width: 20%">Reason Selected</div>
            <div class="cli-ent-col td" style="width: 40%">Additional Text</div>
            <!-- <div class="cli-ent-col td" style="width: 10%">Avail offer</div> -->
            <!-- <div class="cli-ent-col td" style="width: 10%">Feedback</div> -->
            <div class="cli-ent-col td" style="width: 20%">Request Date</div>
            <div class="cli-ent-col td" style="width: 20%">Request status </div>
            <div class="cli-ent-col td" style="width: 20%">Action</div>
        </div>
        <?php
                
        if (count($designer_data) > 0) {
            foreach ($designer_data as $kk => $vv) {


                /*                 * ***request satatus**** */
                if ($vv['status'] == 1) {
                    $cancel_status = 'Open';
                } else {
                    $cancel_status = 'Closed';
                }
                /*                 * ***designer switch or not** */
                if ($vv['is_designer_switch'] == 1) {
                    $switch_designer = 'Yes';
                } else {
                    $switch_designer = 'No';
                }
                /*                 * ****avail offer********* */
                if ($vv['coupon_code'] != '') {
                    $offeravail = 'Yes';
                } else {
                    $offeravail = 'No';
                }
                ?>
                <div class="row two-can">
                    <div class="cli-ent-row tr brdr">
                        <div class="cli-ent-col td cust-name-req" style="width: 20%">
                            <span class="cust_name_cancel"><?php echo $vv['first_name'] . ' ' . $vv['last_name']; ?></span>
                            <span class="email_cancel" data-toggle="tooltip" title="<?php echo $vv['email']; ?>"><?php echo (strlen($vv['email']) > 20 ) ? substr($vv['email'], 0, 20) . '..' : $vv['email']; ?></span><span class="email_cancel" data-toggle="tooltip" title="<?php echo $vv['display_plan_name']; ?>"><?php echo (strlen($vv['display_plan_name']) > 20 ) ? substr($vv['display_plan_name'], 0, 20) . '..' : $vv['display_plan_name']; ?></span>
                        </div>
                        <div class="cli-ent-col td" style="width: 20%"><p class="cancel_rsnn"><?php  echo $vv['reason_selected']; ?></p>
                        </div>
                        <div class="cli-ent-col td" style="width: 40%"><?php  echo $vv['addtional_text']; ?></div>
                        
                        
                        <!-- <div class="cli-ent-col td" style="width: 10%"><?php echo $offeravail; ?></div> -->
                        <div class="cli-ent-col td" style="width: 10%"><p class="cancel_feedB">
                            <?php echo substr($vv['cancel_feedback'], 0, 40);?></p><?php 
        echo ($vv['cancel_feedback'] != '') ? '<i class="far fa-comment-alt" aria-hidden="true" data-toggle="tooltip" title="' . $vv['cancel_feedback'] . '"></i>' : ''; ?></div>

                        <div class="cli-ent-col td" style="width: 20%"><?php echo $vv['created']; ?></div>

                        <div class="cli-ent-col td" style="width: 20%"><div class="switch-custom switch-custom-usersetting-check for_disply">
                                    <span class="checkstatus checkstatus_<?php echo $vv['user_id']; ?>"></span>
                                    <span class="action_lab_<?php echo $vv['user_id']; ?>"><?php echo $cancel_status; ?></span>    
                                    <input type="checkbox" class="closecancel" data-cid="<?php echo $vv['user_id']; ?>" id="switch_<?php echo $vv['cid']; ?>" <?php echo ($vv['status'] == '1') ? 'checked' : ''; ?>>
                                    <label for="switch_<?php echo $vv['user_id']; ?>"></label> 
                                </div> 
                        </div>
                        <div class="cli-ent-col td" style="width: 20%">
                            <div class="notify-lines">
                                <button data-id="<?php  echo $vv['user_id']; ?>" type="button" class="btn red-theme-btn btn-info btn-lg change_designer" data-toggle="modal" data-target="#AddPermaDesign">Change Designer</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php
        } else {
            echo "<p style='text-align:center;padding:20px;'>Data Not Found</p>";
        }
        ?>
    </div>
</section>

<div class="modal similar-prop fade" id="AddPermaDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add Designer</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">

                <div class="cli-ent-model">

                    <div class="noti-listpopup">
                        <div class="newsetionlist">
                            <div class="cli-ent-row tr notificate">
                                <div class="cli-ent-col td" style="width: 32%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Designer</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 43%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Skill</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 25%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b text-center">Active Requests</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="<?php echo base_url(); ?>admin/dashboard/assign_designer_for_customer/change_req" method="post">
                            <ul class="list-unstyled list-notificate two-can">
                                <?php foreach ($data['all_designers'] as $value): ?>
                                    <li>
                                        <a href="#">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="sound-signal">
                                                        <div class="form-radion">
                                                           <input class="selected_btn" type="radio" value="<?php echo $value['id']; ?>" name="assign_designer" id="<?php echo $value['id']; ?>" data-image-pic="<?php echo $value['profile_picture']?>" data-name="<?php echo $value['first_name'] ." ". $value['last_name'];?>">
                                                           <label for="<?php echo $value['id'];?>" data-image-pic="<?php echo $value['profile_picture']?>"></label>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="cli-ent-col td" style="width: 30%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="setnoti-fication">
                                                        <figure class="pro-circle-k1">
                                                            <img src="<?php echo $value['profile_picture'] ?>" class="img-responsive">
                                                        </figure>

                                                        <div class="notifitext">
                                                            <p class="ntifittext-z1"><strong><?php echo $value['first_name'] . " " . $value['last_name']; ?></strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="cli-ent-col td" style="width: 45%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                </div>
                                            </div>

                                            <div class="cli-ent-col td" style="width: 25%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="cli-ent-xbox text-center">

                                                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $value['active_request']; ?></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <input type="hidden" name="customer_id" id="assign_customer_id">
                        <p class="space-c"></p>
                        <p class="btn-x text-center"><button name="submit" type="submit" class="red_gz_btn button" id="assign_designer_per" >Assign Designer</button></p>
                        <!--<p class="btn-x text-center"><button name="submit" type="submit" class="load_more button" id="assign_designer_per" >Assign Designer</button></p>-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal similar-prop fade" id="reason_box_popup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Are you Sure you want to close this request ?</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">

                <div class="cli-ent-model">

                    <div class="noti-listpopup">
                       <textarea id="reason_box" class="reason_box" rows="4" cols="50" placeholder="Enter you reason for close request (optional)"></textarea> 
                       <button id="confirm_close" class="red-theme-btn"> Confirm Close Request</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style type="text/css">.red-theme-btn {
    text-align: center;
    font-size: 16px;
    border: 0;
    font-weight: 500;
    line-height: 50px;
    border: 1px solid #e42647;
    display: inline-block;
    background: #e42647;
    color: #fff;
    border-radius: 30px;
    margin: 30px auto 0;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    transition: all 0.56s;
    text-decoration: none;
    padding: 0 20px;
    cursor: pointer;
}
#reason_box::placeholder {  
    text-align: center; 

} 

.switch-custom.switch-custom-usersetting-check.for_disply {
    flex-direction: column;
    display: table;
    margin: auto;
}
button.btn.red-theme-btn.btn-info.btn-lg.change_designer {
    margin: 0;
}
</style> 


