<section class="con-b">
    <div class="container-fluid">
        <?php
        if ($this->session->flashdata('message_error') != '') { ?>				
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>				
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
            </div>
        <?php } ?>
        <div class="alert alert-danger alert-dismissable error" style="display:none">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c" id="show_error"></p>
        </div>
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-12">
                    <div class="flex-this">
                        <h2 class="main_page_heading">Affiliate Users</h2>
                        <div class="header_searchbtn">
                            <div class="search-first">
                                <div class="focusout-search-box">
                                    <div class="search-box view-client">
                                        <form method="post" class="search-group clearfix">
                                            <input type="text" placeholder="Search here..." class="form-control searchdata searchdata_affclient search_text">
                                            <div class="ajax_searchload" style="display:none;">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                            </div>
                                            <input type="hidden" name="status"  value="active,disapprove,assign,pending,checkforapprove">
                                            <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                            <button type="submit" class="search-btn search search_aff_data_ajax">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_aff; ?>" class="tab-pane active view_clnts content-datatable datatable-width" id="Admin_active" role="tabpanel">
                <div class="product-list-show">
                    <div class="cli-ent-row tr tableHead">
<!--                        <div class="cli-ent-col td">
                            <div class="sound-signal">
                                <div class="form-radion2">
                                    <label class="containerr">
                                        <input type="checkbox" class="checkallcus" data-attr="paid" name="customer">  
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>-->
                        <div class="cli-ent-col td" style="width: 25%;">Client NAME</div>
                        <div class="cli-ent-col td text-center" style="width: 25%;">Sign Up Date</div>
                        <div class="cli-ent-col td  text-center" style="width: 25%;">Billing end date</div>
                        <div class="cli-ent-col td  text-center"style="width: 25%;">Action</div>
                    </div>
                    <div class="add-rows">    
                        <div class="row two-can">
                            <?php
                            for ($i = 0; $i < sizeof($user_info); $i++) {
                                $data['aff_clients'] = $user_info[$i];
                                $this->load->view('admin/affiliate_users_template', $data);
                            }
                            ?>
                        </div>			
                    </div>
                    <?php if ($count_aff > LIMIT_ADMIN_LIST_COUNT) { ?>
                        <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                        <div class="" style="text-align:center">
                            <a id="load_more_aff_client" href="javascript:void(0)" data-is_single="0" data-row="0" data-count="<?php echo $count_project_a; ?>" class="load_more button">Load more</a>
                        </div>
                    <?php } ?>
                </div>
            </div>   
        </div>
    </div>
    </section>
    <!-- Modal -->
    <div class="modal similar-prop nonflex fade" id="AddClient" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <header class="fo-rm-header">
                  <h2 class="popup_h2 del-txt">Add Client</h2>
                  <div class="close" data-dismiss="modal" aria-label="Close"> x</div>
              </header>
              <div class="cli-ent-model-box">
                  <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <form method="post" action="<?php echo base_url(); ?>admin/accounts/create_customer">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">First Name</p>
                                        <input type="text" name="first_name" required class="input firstname">
                                        <div class="line-box">
                                          <div class="line"></div>
                                      </div>
                                  </label>
                              </div>
                              <div class="col-md-6">
                                <label class="form-group">
                                    <p class="label-txt">Last Name</p>
                                    <input type="text" name="last_name" required class="input lastname">
                                    <div class="line-box">
                                      <div class="line"></div>
                                  </div>
                              </label>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt">Email</p>
                                <input type="email" name="email" required  class="input emailid">
                                <div class="line-box">
                                  <div class="line"></div>
                              </div>
                          </label>
                      </div>
                      <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">Phone</p>
                            <input type="tel" name="phone" required class="input phone">
                            <div class="line-box">
                              <div class="line"></div>
                          </div>
                      </label>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <label class="form-group">
                        <p class="label-txt">Password</p>
                        <input type="password" name="password" required  class="input password">
                        <div class="line-box">
                          <div class="line"></div>
                      </div>
                      <i>Only alphanumeric, @ and # allowed</i>
                  </label>                                    
              </div>
              <div class="col-md-6">
                <label class="form-group">
                    <p class="label-txt">Confirm Password</p>
                    <input type="password" name="confirm_password" required class="input cpassword">
                    <div class="line-box">
                      <div class="line"></div>
                  </div>
                  <div class="passError" style="display: none;">
                    <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                </div>
            </label>  
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="form-group">
                <p class="label-txt label-active">State</p>
                <input type="text" name="state"  required class="input" value="<?php echo ucwords($activeClients[$i]['state']); ?>">
                <div class="line-box">
                  <div class="line"></div>
              </div>
          </label>
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
        <label class="form-group">
            <p class="label-txt label-active">Select payment Method</p>
            <select class="input select-c" name="plan_name">
                <?php for ($i = 0; $i < sizeof($planlist); $i++) { ?>
                    <option value="<?php echo $planlist[$i]['id']; ?>">
                        <?php echo $planlist[$i]['name']; ?>
                    </option>';
                <?php } ?>
            </select>
            <div class="line-box">
              <div class="line"></div>
          </div>
      </label>
  </div>
</div>
<div class="form-group goup-x1">
    <div class="sound-signal text-left">
        <div class="form-radion2">
            <label class="containerr">
               <input value="0" type="checkbox" name="bypasspayment" class="form-check-input" id="bypasspayment">
               <span class="checkmark"></span> Bypass payment
           </label>
       </div>
   </div>
</div>
<div id="div_bypass">
   <label class="form-group">
    <p class="label-txt">Card Number</p>
    <input id="card_number" type="text" class="input" name="card_number" required>
    <div class="line-box">
      <div class="line"></div>
  </div>
</label>
<div class="row">
    <div class="col-sm-6">
        <label class="form-group">
            <p class="label-txt">Expiry Date</p>
            <input id="expir_date" type="text" name="expir_date" required class="input" onfocusout="ValidDate(this.value);">
            <div class="line-box">
              <div class="line"></div>
          </div>
      </label>
  </div>
  <div class="col-sm-6">
    <label class="form-group">
        <p class="label-txt">CVC</p>
        <input id="cvc" type="text" name="CVC" required class="input" maxlength="4">
        <div class="line-box">
          <div class="line"></div>
      </div>
  </label>
</div>
</div> 
</div>
<p class="btn-x">
 <button type="submit" class="load_more button">Add Customer</button></p>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal similar-prop fade" id="AddPermaDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add Designer</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">

                <div class="cli-ent-model">

                    <div class="noti-listpopup">
                        <div class="newsetionlist">
                            <div class="cli-ent-row tr notificate">
                                <div class="cli-ent-col td" style="width: 32%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Designer</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 43%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Skill</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 25%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b text-center">Active Requests</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="<?php echo base_url(); ?>admin/dashboard/assign_designer_for_customer" method="post">
                            <ul class="list-unstyled list-notificate two-can">
                                <?php foreach ($data['all_designers'] as $value): ?>
                                    <li>
                                        <a href="#">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="sound-signal">
                                                        <div class="form-radion">
                                                           <input class="selected_btn" type="radio" value="<?php echo $value['id']; ?>" name="assign_designer" id="<?php echo $value['id']; ?>" data-image-pic="<?php echo $value['profile_picture']?>" data-name="<?php echo $value['first_name'] ." ". $value['last_name'];?>">
                                                           <label for="<?php echo $value['id'];?>" data-image-pic="<?php echo $value['profile_picture']?>"></label>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="cli-ent-col td" style="width: 30%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="setnoti-fication">
                                                        <figure class="pro-circle-k1">
                                                            <img src="<?php echo $value['profile_picture'] ?>" class="img-responsive">
                                                        </figure>

                                                        <div class="notifitext">
                                                            <p class="ntifittext-z1"><strong><?php echo $value['first_name'] . " " . $value['last_name']; ?></strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="cli-ent-col td" style="width: 45%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                </div>
                                            </div>

                                            <div class="cli-ent-col td" style="width: 25%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="cli-ent-xbox text-center">

                                                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $value['active_request']; ?></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <input type="hidden" name="customer_id" id="assign_customer_id">
                        <p class="space-c"></p>
                        <p class="btn-x text-center"><button name="submit" type="submit" class="load_more button" id="assign_designer_per" >Assign Designer</button></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade similar-prop" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="id" id="id" value=""/>
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">Delete Customer</h2>
                        <div class="cross_popup" data-dismiss="modal"> x</div>
                    </header>
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">
                        <h3 class="head-c text-center">Are you sure you want to delete this customer?</h3>
                    <div class="confirmation_btn text-center">
                       <a href="javascript:voild(0)" class="btn btn-yes btn-ydelete" data-dismiss="modal" aria-label="Close">Delete</a>
                        <button class="btn btn-nq btn-ndelete" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal similar-prop fade" id="Addva" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
              <h2 class="popup_h2 del-txt">Add VA</h2>
              <div id="close-pop" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
          </header>
          <div class="cli-ent-model-box">
            <div class="cli-ent-model">
                <div class="noti-listpopup">
                    <form action="" method="post">
                        <ul class="list-unstyled list-notificate two-can">
                            <?php foreach($data['all_va'] as $va){ ?>
                                <li>
                                    <a href="#">
                                        <div class="cli-ent-row tr notificate va-adder">
                                            <div class="cli-ent-col td" >
                                                <div class="sound-signal">
                                                    <div class="form-radion">
                                                       <input class="selected_btn" type="radio" value="<?php echo $va['id'];?>" name="assign_va" id="<?php echo $va['id'];?>" data-image-pic="<?php echo $va['profile_picture']?>" data-name="<?php echo $va['first_name'] ." ". $va['last_name'];?>">
                                                       <label for="<?php echo $va['id'];?>" data-image-pic="<?php echo $va['profile_picture']?>"></label>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="cli-ent-col td" style="width: 30%;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="setnoti-fication">
                                                    <figure class="pro-circle-k1">
                                                        <img src="<?php echo $va['profile_picture']?>" class="img-responsive">
                                                    </figure>

                                                    <div class="notifitext">
                                                        <p class="ntifittext-z1">
                                                            <strong>
                                                                <?php echo $va['first_name'] ." ". $va['last_name'];?>

                                                            </strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                            </li>  
                        <?php } ?>                          
                    </ul>
                    <input type="hidden" name="allcustomers_id" id="allcustomers_id">
                    <p class="space-c"></p>
                    <p class="btn-x text-center">
                        <button name="submit" type="submit" id="assign_vatocus" class="load_more button" data-dismiss="modal" aria-label="Close">Assign Va</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<button style="display: none;" id="confirmation" data-toggle="modal" data-target="#myModal">click here</button>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/bootstrap.min.js"></script>
<script type="text/javascript">
    var ASSIGN_PROJECT = '<?php echo isset($_GET['assign'])?$_GET['assign']:""; ?>';
</script> 
