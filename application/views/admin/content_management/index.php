<section class="con-b">
    <div class="container-fluid">
        <div class="flex-this">
            <h2 class="main_page_heading">Additional Modules</h2>
        </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="main-indxOuter">
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/Contentmanagement/blog">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/blog_icon.svg">
                    </div>
                    <span>Blog</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                     <a href="<?php echo base_url(); ?>admin/Contentmanagement/portfolio">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/portfolio_icon.svg">
                    </div>
                   <span>Portfolio</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/Contentmanagement/email">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/email_settings_icon.svg">
                    </div>
                    <span>Email Setting</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/categories/cat_listing">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/category_bucket_icon.svg">
                    </div>
                   <span> Category Buckets</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/Contentmanagement/billing_subscription">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/billing_subscrptn.svg">
                    </div>
                    <span>Billing and Subscription</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                     <a href="<?php echo base_url(); ?>admin/Contentmanagement/notifications">
                    <div class="icon-blez">
                         <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/notification.svg">
                    </div>
                   <span>Notifications</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/Contentmanagement/abandoned_users_list">
                    <div class="icon-blez">
                         <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/abandoned.svg" />
                    </div>
                    
                   <span> Abandoned Users</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/report/dashboard">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/abandoned.svg">
                    </div>
                    <span>Reporting</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                     <a href="<?php echo base_url(); ?>admin/Contentmanagement/category_based_questions">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/help.svg">
                    </div>
                   <span>Request questions</span></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/Contentmanagement/category_based_samples">
                        <div class="icon-blez">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/category-sample.svg">
                        </div>
                        <span>Request Samples</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/Contentmanagement/feedback_matrix">
                        <div class="icon-blez">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/feedback_matrix.svg">
                        </div>
                        <span>Feedback Matrix</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/Contentmanagement/getAffiateUsers">
                        <div class="icon-blez">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/category-sample.svg">
                        </div>
                        <span>Affiliate Users</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                     <a href="<?php echo base_url(); ?>admin/Checkin/index">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/help.svg">
                    </div>
                   <span>Checkin Detail</span></a>
                </div>
            </div>

            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                     <a href="<?php echo base_url(); ?>admin/Contentmanagement/change_designer_admin_view">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/help.svg">
                    </div>
                   <span>Designer Change Requests </span></a>
                </div>
            </div>

            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                     <a href="<?php echo base_url(); ?>admin/Contentmanagement/seo_landing_pages">
                    <div class="icon-blez">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/help.svg">
                    </div>
                   <span>SEO-Landing-Pages</span></a>
                </div>
            </div>

            <div class="col-sm-6 col-md-3">
                <div class="main-linked">
                    <a href="<?php echo base_url(); ?>admin/project-designs">
                        <div class="icon-blez">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/category-sample.svg">
                        </div>
                        <span>Project/drafts Filter</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
