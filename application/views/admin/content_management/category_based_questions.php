<?php //echo "<pre/>";print_R($countallquestions); exit;?>
<section class="con-b">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('message_error') != '') { ?>    
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('message_success') != '') { ?>    
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="tab-content">
            <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $designerscount; ?>" class="tab-pane active view_clnts content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                <div class="header-blog">
                    <div class="row flex-show">
                        <div class="col-md-12">
                            <div class="flex-this">
                                <h2 class="main_page_heading">Questions</h2>
                                <div class="header_searchbtn">
                                    <a href="<?php echo base_url() . 'admin/Contentmanagement/add_category_questions'; ?>" class="upl-oadfi-le adddesinger">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/add_request.svg">
                                        <span class="">Add Question</span>
                                    </a>
                                    <div class="search-first">
                                        <div class="focusout-search-box">
                                            <div class="search-box">
                                                <form method="post" class="search-group clearfix">
                                                    <input type="text" placeholder="Search here..." class="form-control searchdata searchdata_questions search_text">
                                                    <div class="ajax_searchload" style="display:none;">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                                    </div>
                                                    <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                                    <button type="submit" class="search-btn search search_designer_ajax">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-list-show">
                    <div class="cli-ent-row tr tableHead">
                        <div class="cli-ent-col td" style="width: 25%;">
                            CATEGORY NAME
                        </div>
                        <div class="cli-ent-col td" style="width: 25%;">
                            SUBCATEGORY NAME
                        </div>
                        <div class="cli-ent-col td  text-center" style="width: 30%;">
                            QUESTION LABEL
                        </div>
                        <div class="cli-ent-col td text-center"style="width:20%;">
                            ACTION
                        </div>
                    </div>
                        <input type="hidden" class="data_group" data-group="1">
                        <input type="hidden" class="data_total_count" data-total-count="<?php echo $countallquestions;?>">
                        <input type="hidden" class="data_loaded" data-loaded="<?php echo LIMIT_ADMIN_LIST_COUNT; ?>">
                    <div class="cli-ent" id="designerslist">
                        <div class="row two-can">
                            <?php
                            if(!empty($result)){
                            for ($i = 0; $i < sizeof($result); $i++):
                                $data['questions'] = $result[$i];
                                $this->load->view('admin/content_management/questions_template', $data);
                            endfor;
                            }else{
                            ?>
                            <div class='no_data_ajax'>No record found</div>
                            <?php } ?>
                        </div>
                        
                    </div>
                    <?php if ($countallquestions > LIMIT_ADMIN_LIST_COUNT) { ?>
                        <div class="" style="text-align:center">
                            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                            <a id="load_more_questions" href="javascript:void(0)" data-row="0" data-count="<?php echo $designerscount; ?>" class="load_more button load_more_questions">Load more</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
