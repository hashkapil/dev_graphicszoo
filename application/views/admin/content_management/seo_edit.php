 <style type="text/css">/* USER PROFILE PAGE */
 .card {
    margin-top: 20px;
    padding: 30px;
    background-color: rgba(214, 224, 226, 0.2);
    -webkit-border-top-left-radius:5px;
    -moz-border-top-left-radius:5px;
    border-top-left-radius:5px;
    -webkit-border-top-right-radius:5px;
    -moz-border-top-right-radius:5px;
    border-top-right-radius:5px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.card.hovercard {
    position: relative;
    padding-top: 30px;
    overflow: hidden;
    text-align: center;
    background-color: #fff;
    background-color: rgba(255, 255, 255, 1);
}
.card.hovercard .card-background {
    height: 130px;
}
.card-background img {
    -webkit-filter: blur(25px);
    -moz-filter: blur(25px);
    -o-filter: blur(25px);
    -ms-filter: blur(25px);
    filter: blur(25px);
    margin-left: -100px;
    margin-top: -200px;
    min-width: 130%;
}
.card.hovercard .useravatar {
    position: absolute;
    top: 15px;
    left: 0;
    right: 0;
}
.card.hovercard .useravatar img {
    width: 100px;
    height: 100px;
    max-width: 100px;
    max-height: 100px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    border: 5px solid rgba(255, 255, 255, 0.5);
}
.card.hovercard .card-info {
    position: absolute;
    bottom: 14px;
    left: 0;
    right: 0;
}
.card.hovercard .card-info .card-title {
    padding:0 5px;
    font-size: 20px;
    line-height: 1;
    color: #262626;
    background-color: rgba(255, 255, 255, 0.1);
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}
.card.hovercard .card-info {
    overflow: hidden;
    font-size: 12px;
    line-height: 20px;
    color: #737373;
    text-overflow: ellipsis;
}
.card.hovercard .bottom {
    padding: 0 20px;
    margin-bottom: 17px;
}
.btn-pref .btn {
    -webkit-border-radius:0 !important;
}
.fo-rm-body {
    display: block;
    position: relative;
    margin-top: 20px;
}
.sample_uploaded img{
    height: 220px;
    width: auto;
}



/**********************/
.samples-designer {
    text-align: center;
}

.samples-designer .cross-del {
    /*width: 130px;*/
    margin: auto;
    position: relative;
}

.upload_images_catg {
    display: inline-block;
    width: 100%;
    border: 1px solid #ccc;
    background: #fff;
}
.upload_images_catg span.fake-img {
    margin: 10px 0;
}
.admin-wraper .well {
    display: inline-block;
    width: 100%;
}
.samples-designer .cross-del .close-img {
    height: 20px;
    width: 20px;
    background: #e42647;
    color: #fff;
    font-size: 14px;
    line-height: 20px;
    border-radius: 50px;
    position: absolute;
    right: -10px;
    top: -9px;
    cursor: pointer;
}
.cross-del .view_sim_design {
    position: absolute;
    height: 25px;
    opacity: 0;
    width: 25px;
    color: #fff;
    background: #e42647;
    border-radius: 50px;
    line-height: 23px;
    font-size: 13px;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
    right: 0;
    margin: auto;
    transition: all 0.2s ease-out;
    cursor: pointer;
}
.samples-designer .cross-del::before {
    content: "";
    height: 100%;
    width: 100%;
    position: absolute;
    background: rgba(0, 0, 0, 0.44);
    opacity: 0;
    transition: all 0.2s ease-out;
    left: 0;
}
.samples-designer .cross-del:hover::before {
    opacity: 1;
}
.cross-del:hover .view_sim_design {
    opacity: 1;
}
</style>
<?php 

// echo "<pre>"; print_r($cat_data); die;
 ?>

<div class="col-lg-12 col-sm-12">
   
    <div class="card hovercard">
 <?php if(!empty($this->session->flashdata('success'))){ ?>
    <div class="alert alert-success" role="alert">
      <?php echo $this->session->flashdata('success');?>
  </div>
<?php } if(!empty($this->session->flashdata('error'))) { ?>
  <div class="alert alert-danger" role="alert">
     <?php echo $this->session->flashdata('error'); ?>
  </div>
<?php } ?>
        <div class="card-info"> 
            <span class="card-title"> Category <?php if($this->router->fetch_method()=='seo_add'){ echo "Add"; }else{ echo "Edit"; } ?>   </span>
        </div>
    </div>
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"> 
                <div class="hidden-xs">Basic-Info </div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"> 
                <div class="hidden-xs">Meta info</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"> 
                <div class="hidden-xs">Sample info</div>
            </button>
        </div>
    </div>

        <div class="well">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
              
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <?php //echo "<pre>"; print_r($data); die; ?>
                    <form  method="post" id="addCatForm" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group">
                                         <p class="label-txt label-active"> Parent category</p>
                                    <select name="parent_cat" class="input parent_cat" id="parent_cat">
                                            <option value="">Choose parent</option>
                                            <?php foreach ($cat_data as $kk => $vv) { 

                                                    
                                                ?>
                                                <option <?php if($vv['id'] ==  $data[0]["parent_id"]){  echo "selected";  
                                                    }?> value="<?php echo $vv['id']; ?>"><?php echo $vv['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="line-box"></div>
                                        <p class="ab-notify">Don't choose if you are adding parent category.</p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Name</p>
                                        <input type="text" name="cat_name" required  value= "<?php echo $data[0]['name']; ?>" class="input name1">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">position</p>
                                        <input type="number" name="position" required class="input position1" value= "<?php echo $data[0]['position']; ?>">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                 
                                <div class="col-md-6">
                                    <div class="form-group goup-x1 text-left">
                                <div class="sound-signal">
                                    <div class="form-radion2">
                                        <label class="containerr">
                                           <input type="checkbox" name="active" class="active1" <?php if($data[0]['is_active']==1){echo "checked";}else{ echo ""; }?>>
                                           <span class="checkmark"></span> Active
                                       </label>
                                   </div>
                               </div>                               
                           </div>
                                </div>
                        <?php if(!empty($data[0]['image_url'])){ 


                            $imageurl =  './public/assets/img/customer/customer-images/'.$data[0]['image_url']; 

                               // echo $imageurl; 
                            if(file_exists($imageurl)){
                               $url = base_url().'/public/assets/img/customer/customer-images/'.$data[0]['image_url']; 
                               $folder  = "/public/assets/img/customer/customer-images/";
                           }else{
                               $url = FS_PATH_PUBLIC_UPLOADS_SEO.$data[0]['id'].'/cat_image/'.$data[0]['image_url']; 
                               $folder  = 'public/uploads/Seo_material/'.$data[0]['id'].'/cat_image/';
                           }


                            ?> 
                                <div class="col-md-12">

                                <div class="samples-designer">
                                    <div class="cross-del" style="width: 150px;">
                                     <a  href="<?php echo $url; ?>"><i class="view_sim_design fa fa-eye" aria-hidden="true"></i></a>
                                        
                                        <i class="close-img fa fa-times" aria-hidden="true" data-img-id="<?php echo $data[0]['id'];?>" data-folder="<?php echo $folder;?>" data-db="image_url"></i>
                                        <img src="<?php echo $url; ?>">
                                    </div>
                                    
                                </div>
                                
                            </div>

                        <?php }?>

                            </div>
                             <input type="hidden" name="check" value="basic-info">
                            <div class="col-md-offset-2 col-md-8 form-group">
                                <div class="upload_images_catg">
                                <img src="" class="showimg">
                                <div class="file-drop-area file-upload">
                             <label>upload Category image *</label>
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input type="file" class="file-input project-file-input file-input-add" multiple="" name="cat_image" id="file_input">
                                </div>
                                <div id="file-uploadNameAdd" class="uploadFileListContainer">
                                <span></span>
                            </div>
                            </div> 
                            </div> 
                            <div class="col-md-12 text-center">
                          <button type="submit" class="load_more button" name="save">Save</button>
                              </div>
                       </form>
                   </div>
               </div>
           </div>

        </div>
        <div class="tab-pane fade in" id="tab2">
           <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <?php //echo "<pre>"; print_r($data); die; ?>
                    <form  method="post" id="addCatForm" enctype="multipart/form-data">
                            <div class="row">
                                 
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Meta title</p>
                                        <input type="text" name="meta_title" required  value= "<?php echo $data[0]['meta_title']; ?>" class="input name1">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Meta Description</p>
                                        <textarea name="meta_description" required class="input position1" ><?php echo $data[0]['meta_description']; ?></textarea> 
                                        <div class="line-box"></div>
                                    </label>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Meta Keywords</p>
                                         <textarea name="keyword" required class="input position1" ><?php echo $data[0]['keyword']; ?></textarea>
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Page Slug</p>
                                        <input type="text" name="slug" required class="input position1" value= "<?php echo $data[0]['slug']; ?>">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">canonical</p>
                                        <input type="text" name="canonical" required class="input position1" value= "<?php echo $data[0]['canonical']; ?>">
                                        <div class="line-box"></div>
                                    </label>
                                </div>

                                <div class="col-md-12">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Description</p>
                                         <textarea name="parent_description"   required class="input position1"><?php echo $parent_des; ?></textarea>
                                        <div class="line-box"></div>
                                        <span>*Description for Listing Main category (Short)</span>
                                    </label>
                                </div>

                                <input type="hidden" name="check" value="meta-info">


                                 <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Content Banner</p>
                                         <textarea name="content_description" placeholder="Description for Listing Main category (Short)" required class="input position1"><?php echo $data[0]['content_description']; ?></textarea>
                                        <div class="line-box"></div>
                                         
                                    </label>
                                </div>

                                 <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Content Tags</p>
                                        <input type="text" name="content_tags" required placeholder="Add with Comma Seprated " value= "<?php echo $data[0]['content_tags']; ?>" class="input name1">
                                        <div class="line-box"></div>
                                    </label>
                                </div> 
                                 <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">HighLights Title</p>
                                         <textarea name="highlights_title"  required class="input position1"><?php echo $data[0]['highlights_title']; ?></textarea>
                                        <div class="line-box"></div>
                                        
                                    </label>

                                </div>

                                  <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">HighLights Description</p>
                                         <textarea name="highlights_description"  required class="input position1"><?php echo $data[0]['highlights_description']; ?></textarea>
                                        <div class="line-box"></div>
                                        
                                    </label>

                                </div>

                                 <div class="col-md-12">
                                        <p class="label-txt label-active">Highlights Content</p>
                                    <label class="form-group">
                                         <textarea name="highlights_content" id="HighLights" required class="input position1"><?php echo $data[0]['highlights_content']; ?></textarea>
                                        <div class="line-box"></div>
                                        
                                    </label>

                                </div>
                              
                                <?php if(!empty($data[0]['highlight_image'])){

                                    $folder ='public/uploads/Seo_material/'.$data[0]['id'].'/highlight_image';
                                    ?> 
                                <div class="col-md-4">
                                            <div class="samples-designer">
                                                <div class="cross-del">
                                                    <a href="<?php echo FS_PATH_PUBLIC_UPLOADS.'Seo_material/'.$data[0]['id'].'/highlight_image/'.$data[0]['highlight_image']; ?>">
                                                    <i class="view_sim_design fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                    <i class="close-img fa fa-times" aria-hidden="true" data-img-id="<?php echo $data[0]['id'];?>" data-folder="<?php echo $folder;?>" data-db="highlight_image"></i>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS.'Seo_material/'.$data[0]['id'].'/highlight_image/'.$data[0]['highlight_image']; ?>">
                                                </div>

                                            </div>

                                        </div>
    
                                <?php } ?>
                                
                                <div class="form-group">
                                        <img src="" class="showimg">
                                        <div class="file-drop-area file-upload">
                                <label>Upload highlighed image </label>
                                            <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                            <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                            <input type="file" class="file-input project-file-input file-input-add"  name="highlight_image" id="file_input1">
                                        </div>
                                        <div id="file-uploadNameAdd1" class="uploadFileListContainer"> 
                                            <span></span>
                                        </div>
                                        
                                    </div> 
                                
                                 
                                 
                            </div>
                            
                          <button type="submit" class="load_more button meta_save" name="save">Save</button>
                       </form>
                   </div>
               </div>
           </div>
        </div>
        <div class="tab-pane fade in" id="tab3">
           <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <?php //echo "<pre>"; print_r($data); die; ?>
                    <form  method="post" id="addCatForm" enctype="multipart/form-data">
                            <div class="row"> 
                                 
                                <input type="hidden" name="check" value="sample-info">
                                <input type="hidden" name="category_id" value="<?php echo $data[0]['id']; ?>">
                                <input type="hidden" name="subcategory_id" value="<?php echo $data[0]['parent_id']; ?>">
                                <input type="hidden" name="is_active" value="<?php echo $data[0]['is_active']; ?>">
                                 

                               <!--  <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Minimum Samples</p>
                                        <input type="number" name="minimum_sample" required placeholder="Add with Comma Seprated " value= "" class="input name1">
                                        <div class="line-box"></div>
                                    </label>
                                </div> 

                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Maximum samples</p>
                                        <input type="number" name="maximum_sample" required placeholder="" value= "" class="input name1">
                                        <div class="line-box"></div>
                                    </label>
                                </div> 

                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Position</p>
                                        <input type="number" name="position" required placeholder="" value= "" class="input name1">
                                        <div class="line-box"></div>
                                    </label>
                                </d iv> -->

                                <?php 
                                if(!empty($sampledata)){
                                    foreach ($sampledata as $key => $sam_info) {  ?> 
                                        <div class="col-md-4">
                                            <div class="samples-designer">
                                                <div class="cross-del sample_uploaded">
                                                    <a href="<?php echo FS_PATH_PUBLIC_UPLOADS.'samples_seo/'.$sam_info['sample_id'].'/'.$sam_info['sample_material']; ?>">
                                                     <i class="view_sim_design fa fa-eye" aria-hidden="true"></i>
                                                 </a>
                                                    <i class="close-img fa fa-times" aria-hidden="true" data-img-id="<?php echo $sam_info['id'];?>" data-folder="public/uploads/samples_seo/<?php echo $data[0]['id']; ?>" data-db="sample_material"></i>
                                                     
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS.'samples_seo/'.$sam_info['sample_id'].'/'.$sam_info['sample_material']; ?>">
                                                </div>

                                            </div>

                                        </div>
    
                                <?php } } ?>
                                 
                                
                                <div class="form-group">
                                        <img src="" class="showimg">
                                        <div class="file-drop-area file-upload">
                                <label>Upload Samples for Category </label>
                                            <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                            <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                            <input type="file" class="file-input project-file-input file-input-add"  multiple name="cat_samples" id="file_input2">
                                        </div>
                                        <div id="file-uploadNameAdd2" class="uploadFileListContainer">
                                            <span></span>
                                        </div>
                                        
                                    </div> 
                            
                          <button type="submit" class="load_more button" name="save">Save</button>
                       </form>
                   </div>
               </div>
           </div>
        </div>
      </div>
    </div>
    
    </div>
