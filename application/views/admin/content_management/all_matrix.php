<?
$is_past_designer = array_search(0, array_column($designer_info, 'is_active'));
$is_past_cus = array_search(0, array_column($customer_info, 'is_active'));
?>

<section class="con-b">
	<div class="container-fluid">
		<div class="matrix-section feedback-matrix-rate">

            <div class="all-feedback-colmn">
                <div class="row">
                   
                    <div class="col-md-12">
                        <div class="rating-column">
                            <div class="feedback-item">
                            <div class="feedback-head">
                                <h2>Rating by QA</h2>
                            
                            </div>
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Qa Name</th>
                                        <th>Total Drafts / Feedback of Drafts</th>
                                        <th>Avg. Rating</th>
                                        <th>Last 50 Design Ratings</th>
                                        <th>Rejected Drafts By Qa % </th>
                                        <th>Design Approved By Client%</th>
                                         
                                    </tr>
                                </thead>
                                <tbody style="max-height:270px;height:270px;">
                                   <?php 
                                     foreach ($qa_info as $key => $q_info) { 
                                    
                                            // echo "<pre>"; print_r($q_info); echo "</pre>"; 
                                            // die(); 
                                      ?> 
                                 
                                    <tr>
                                        <td><?php echo $q_info['name']; ?></td>
                                        <td><?php echo $q_info['total_drafts'];?> / <?php echo $q_info['total_design'];?></td>
                                        <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $q_info["svg"];?>"> <?php echo $q_info['total_avg']; ?></td>
                                        <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $q_info["lastsvg"];?>"><?php echo $q_info['lastAvg']['avg']; ?></td>
                                        <td><?php echo number_format($q_info['qa_notApprov'],2,".",""). "%" ; ?></td>
                                        <td><?php echo number_format($q_info['percentageOfApprovedByClint'],2,".",""). "%" ; ?></td>
                                        

                                    </tr>
                                <?php 
                                //echo "<pre>"; print_r($q_info['lastAvg']); 
                                $Qatotaldrafts+= $q_info['total_design'];
                                $QatotalOfAvgRating+= $q_info['total_avg'];
                                $totalavgRatingqa += $q_info['total_design'] * $q_info['total_avg'];
                                $toal15Designqa +=  $q_info['lastAvg']['Avgtotal_design'];
                                $AvgWeightedqa +=  $q_info['lastAvg']['Avgtotal_design'] * $q_info['lastAvg']['avg'];

                                $totalAvgOF15qa = $AvgWeightedqa/$toal15Designqa;

                              } ?>
                                
                     
                                </tbody>
                               <tfoot>
                                    <tr>
                                        
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $Qatotaldrafts; ?> </strong></td>
                                        <td id="totalAvgRatingqa"><strong><?php echo number_format($totalavgRatingqa/$Qatotaldrafts,2,'.',''); ?></strong></td>
                                        <td><strong><?php echo number_format($totalAvgOF15qa,2,'.',''); ?> </strong></td>
                                        <td><strong>---</strong></td>
                                        <td><strong>---</strong></td>
                                         
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                        </div>

                        <!--  rating by Plan name start  -->
                        <div class="col-md-12">
                        <div class="rating-column">
                            <div class="feedback-item">
                            <div class="feedback-head">
                                <h2>Rating by Plan</h2>
                            
                            </div>
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Plan Name</th>
                                        <th>Total Drafts / Feedback of Drafts</th>
                                        <th>Avg. Rating</th> 
                                    </tr>
                                </thead>
                                <tbody style="max-height:200px;height:200px;">
                                   <?php 
                                     foreach ($ratingByplanName as $key => $plan_rating) { 
                                      $plnsvg =  $this->myfunctions->getfacesaccavg($plan_rating['avg_rating']);
                                    ?> 
                                 
                                    <tr>
                                        <td><?php echo $plan_rating['plan_name']; ?></td>
                                        <td><?php echo $plan_rating['total_drafts'];?> / <?php echo $plan_rating['total_designs'];?></td>
                                        <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $plnsvg;?>"> <?php echo $plan_rating['avg_rating']; ?></td>
                                    </tr>
                                <?php 
                                $Prtotaldrafts+= $plan_rating['total_designs'];
                                //$PrtotalOfAvgRating+= $plan_rating['avg_rating'];
                                $totalavgRatingPr += $plan_rating['total_designs'] * $plan_rating['avg_rating'];
                                 } ?>
                                
                     
                                </tbody>
                               <tfoot>
                                    <tr>
                                        
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $Prtotaldrafts; ?> </strong></td>
                                        <td id="totalAvgRatingc"><strong><?php echo number_format($totalavgRatingPr/$Prtotaldrafts,2,'.',''); ?></strong></td>
                                       
                                         
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                        <!--  rating by Plan name end  -->

                        
                </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-12">
                        <div class="rating-column">
                            <div class="feedback-item">
                            <div class="feedback-head">
                                <h2>Rating by Designer</h2>
                            </div>
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Designer Name</th>
                                        <th>Total Drafts / Feedback of Drafts</th>
                                        <th>Avg. Rating</th>
                                        <th>Last 15 Design Ratings</th>
                                        <th>Design Rejected By Qa %</th>
                                        <th>Design Approved By Client%</th>
                                        <th>Design Rejected By Client%</th>
                                        
                                    </tr>
                                </thead>
                               <tbody style="max-height:626px;height:626px;">
                                   <tr class="cstm-desigr-rating actv_dsgnr"><th colspan="4">Current Designers</th><td><table><tbody>
                                   <?php
                                   foreach ($designer_info as $ky => $d_info) {
                                       if ($d_info['is_active'] == 1) {
                                           ?>
                                       <tr>
                                           <td><?php echo $d_info['name']; ?></td>
                                           <td><?php echo $d_info['total_drafts']; ?> / <?php echo $d_info['total_design']; ?></td>
                                           <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $d_info["svg"]; ?>"> <?php echo $d_info['total_avg']; ?></td>
                                           <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $d_info['lastsvg']; ?>"> <?php echo $d_info['lastAvg']['avg']; ?></td>
                                           <td><?php echo number_format($d_info['qa_rejected'],2,".",""). "%" ; ?></td>
                                           <td><?php echo number_format($d_info['percentageOfApprovedByClint'],2,".",""). "%" ; ?></td>
                                           <td><?php echo number_format($d_info['RejectedByClient'],2,".",""). "%" ; ?></td>
                                           

                                       </tr>

                                       <?php
                                       $actvdis_totaldrafts += $d_info['total_design'];
                                       $actvdis_totalOfAvgRating += $d_info['total_avg'];
                                       $actvdis_totalavgRatingD += $d_info['total_design'] * $d_info['total_avg'];
                                       $actvdis_toal15Design += $d_info['lastAvg']['Avgtotal_design'];
                                       $actvdis_AvgWeighted += $d_info['lastAvg']['Avgtotal_design'] * $d_info['lastAvg']['avg'];

                                       $actvdis_totalAvgOF15 = $actvdis_AvgWeighted / $actvdis_toal15Design;
                                   }
                                }
                                ?>
                            </tbody>
                              <tfoot>
                                    <tr>
                                         
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $actvdis_totaldrafts; ?> </strong></td>
                                        <td id="totalAvgRating"><strong><?php echo number_format($actvdis_totalavgRatingD/$actvdis_totaldrafts,2,'.',''); ?></strong></td>
                                        <td><strong><?php echo number_format($actvdis_totalAvgOF15,2,'.',''); ?></strong></td>
                                         <td><strong>---</strong></td>
                                         <td><strong>---</strong></td>
                                         
                                    </tr>
                                </tfoot></table></td></tr>
                                   <?php if($is_past_designer){ ?>
                                   
                                   <tr class="cstm-desigr-rating dectv_dsgnr"><th class="accord-outer" data-toggle="collapse" colspan="4" data-target="#past_designers">Past Designers</th><td class="accordion-step collapse" id="past_designers"><table><tbody>
                                   <?php 
                                   foreach ($designer_info as $ky => $d_info) {
                                       if ($d_info['is_active'] == 0) {
                                           ?>
                                       <tr>
                                           <td><?php echo $d_info['name']; ?></td>
                                           <td><?php echo $d_info['total_drafts'];?> / <?php echo $d_info['total_design']; ?></td>
                                           <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $d_info["svg"]; ?>"> <?php echo $d_info['total_avg']; ?></td>
                                           <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $d_info['lastsvg']; ?>"> <?php echo $d_info['lastAvg']['avg']; ?></td>
                                            <td><?php echo number_format($d_info['qa_rejected'],2,".",""). "%" ; ?></td>
                                            <td><?php echo number_format($d_info['percentageOfApprovedByClint'],2,".",""). "%" ; ?></td>

                                       </tr>

                                       <?php
                                       $pastde_totaldrafts += $d_info['total_design'];
                                       $pastde_totalOfAvgRating += $d_info['total_avg'];
                                       $pastde_totalavgRatingD += $d_info['total_design'] * $d_info['total_avg'];

                                       $pastde_toal15Design += $d_info['lastAvg']['Avgtotal_design'];
                                       $pastde_AvgWeighted += $d_info['lastAvg']['Avgtotal_design'] * $d_info['lastAvg']['avg'];

                                       $pastde_totalAvgOF15 = $pastde_AvgWeighted / $pastde_toal15Design;
                                   }
                                } ?>
                            </tbody>
                                <tfoot>
                                    <tr>
                                         
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $pastde_totaldrafts; ?> </strong></td>
                                        <td id="totalAvgRating"><strong><?php echo number_format($pastde_totalavgRatingD/$pastde_totaldrafts,2,'.',''); ?></strong></td>
                                        <td><strong><?php echo number_format($pastde_totalAvgOF15,2,'.',''); ?></strong></td>
                                         <td><strong>---</strong></td>
                                         <td><strong>---</strong></td>
                                         
                                    </tr>
                                </tfoot>
                                </table></td></tr>
                            <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                         <?php 
                                              $final_drafts = $actvdis_totaldrafts+$pastde_totaldrafts;
                                              $final_totalavgRatingD = $actvdis_totalavgRatingD+$pastde_totalavgRatingD;
                                              $final_toal15Design += $actvdis_toal15Design+$pastde_toal15Design;
                                              $final_AvgWeighted += $pastde_AvgWeighted+$actvdis_AvgWeighted;
                                              $final_15avgrating = $final_AvgWeighted/$final_toal15Design;
                                          ?>
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $final_drafts; ?> </strong></td>
                                        <td id="totalAvgRating"><strong><?php echo number_format($final_totalavgRatingD/$final_drafts,2,'.',''); ?></strong></td>
                                        <td><strong><?php echo number_format($final_15avgrating,2,'.',''); ?></strong></td>
                                         <td><strong>---</strong></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                    </div>
                </div

	 
                </div>
                </div>
                </div>
            </section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
  $(".accordion").click(function(){

    $(".accordion").not(this).removeClass("active"); 
    
 
    $(this).addClass('active');
    var id = $(this).data('id'); 
    //AjaxForSubCustomer(id);

    if($("#i_"+id).hasClass("fa-plus")){
      $("#i_"+id).removeClass("fa-plus").addClass("fa-minus");
    }else{
      $("#i_"+id).removeClass("fa-minus").addClass("fa-plus");
    } $("#target_"+id).toggleClass('hide');
  });
 

 </script>