<section class="con-b">
    <div class="container-fluid">
        <?php if ($this->session->flashdata('message_error') != '') { ?>    
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>    
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
            </div>
        <?php } ?>
        <div class="header-blog">
            <div class="row flex-show">
               <div class="col-md-12">
                   <div class="flex-this">
                    <h2 class="main_page_heading">Blogs</h2>
                    <div class="header_searchbtn">
                        <a href="<?php echo base_url(); ?>admin/Contentmanagement/addblog" class="addderequesr" >
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/add_request.svg" class="img-responsive"><span> Add blog</span>
                        </a>
                        <div class="search-first">
                            <div class="focusout-search-box">
                                <div class="search-box">
                                    <form method="post" class="search-group clearfix">
                                        <input type="text" placeholder="Search here..." class="form-control searchdata searchdata_blog search_text" id="search_text">
                                        <input type="hidden" name="status" id="status" value="blog">
                                        <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                        <button type="submit" class="search-btn search search_data_blog">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="blog_list">
        <?php 
//        echo "<pre/>";print_R($data);exit;
        for ($i = 0; $i < sizeof($data); $i++):
           $data['blog'] = $data[$i];  
           $this->load->view('admin/content_management/blog_listing_template',$data); 
        endfor; ?>			

    </div>
</div>
</section>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js'); ?>"></script>
<script type="text/javascript">
//    $.urlParam = function (name) {
//        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
//        if (results == null) {
//            return null;
//        }
//        else {
//            return decodeURI(results[1]) || 0;
//        }
//    }
       // var status = $.urlParam('status');
        // $('#status_check #' + status + " a").click();
        // $('#status_check li a').click(function () {
        //     var status = $(this).data('status');
        //     $('#status').val(status);
        //     $('#search_text').val("");
        //     ajax_call();
        // });

//        $('.searchdata_blog').keyup(function (e) {
//            ajax_call();
//        });
//
//        $('.search_data_blog').click(function (e) {
//            e.preventDefault();
//            ajax_call();
//        });
//        function ajax_call() {
//            var search = $('#search_text').val();
//            var status = $('#status').val();
//            if (status == "blog") {
//                $.get("<?php echo base_url(); ?>admin/contentmanagement/search_ajax_blog?title=" + search, function (data) {
//                    var content_id = $('a[data-status="' + status + '"]').attr('href');
//                    $('#blog_list').html(data);
//                });
//            } else if (status == "portfolio") {
//                $.get("<?php echo base_url(); ?>admin/contentmanagement/search_ajax_portfolio?title=" + search, function (data) {
//                    var content_id = $('a[data-status="' + status + '"]').attr('href');
//                    $('#portfolio_list').html(data);
//                });
//            }
//        }
        
//$(document).on('click','.delete_blog',function(){
//var delID = $(this).attr('data-delID');
//var cnfrm = confirm('Are you sure you want to delete this blog ?');
//if(cnfrm == true){
//    window.location.href = "<?php echo base_url(); ?>admin/contentmanagement/delete_blog/"+delID;
//}
//});
</script>