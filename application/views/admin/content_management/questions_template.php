<?php
//echo "<pre/>";print_R($questions);
if($questions['subcategory_id'] != 0 && $questions['category_id'] != 0){
   $subcategory =  $questions['subcat_name'];
}elseif($questions['subcategory_id'] == 0 && $questions['category_id'] != 0){
    $subcategory =  'All Subcategories';
}else{
   $subcategory =  'All Subcategories'; 
}
?>
<div class="cli-ent-row tr brdr" id="remove_<?php echo $questions['id']; ?>">
    <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
    <div class="cli-ent-col td flex-uses" style="width: 25%;">
        <div class="cli-ent-xbox">
            <p class="pro-b"><?php echo ($questions['category_id'] != 0) ? $questions['cat_name'] : 'All Categories'; ?></p>
        </div>
    </div>

    <div class="cli-ent-col td" style="width: 25%;">
        <div class="cli-ent-xbox">
            <p class="pro-b"><?php echo $subcategory; ?></p>
        </div>
    </div>


    <div class="cli-ent-col td" style="width: 30%;">
        <div class="cli-ent-xbox text-center">
            <span>
                <?php echo $questions['question_label']; ?>
            </span>
        </div>
    </div> 
<!--    <div class="cli-ent-col td" style="width: 30%;">
        <div class="cli-ent-xbox text-center">
            <span>
                <?php //echo $questions['question_placeholder']; ?>
            </span>
        </div>
    </div>-->
<!--    <div class="cli-ent-col td  text-center" style="width: 10%;" >
        <div class="notify-lines">
           <div class="cli-ent-xbox text-center">
                <span><?php // ($questions['is_active'] == '1') ? "Active" : "Not Active"; ?></span>
                
            </div>
        </div>
    </div>-->
    <div class="cli-ent-col td text-center" style="width: 20%;" >
        <div class="cli-ent-xbox action-per">
            <a href="<?php echo base_url(); ?>admin/Contentmanagement/add_category_questions/<?php echo $questions['id']; ?>" title="Edit Designer">
                <i class="icon-gz_edit_icon"></i>
            </a>
            <a title="delete" id="delete<?php echo $questions['id']; ?>" href="#" data-toggle="modal" class="delete_quest" data-questid="<?php echo $questions['id']; ?>">
                <i class="icon-gz_delete_icon"></i>
            </a>
        </div>
    </div>
</div> 