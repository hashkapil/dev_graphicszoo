<?php
preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $matches);
$last_word = $matches[0];
?>
<section class="con-b">
    <div class="container-fluid">
        <div class="header-blog">
            <input type="hidden" value="" class="bucket_type"/>
            <div class="row flex-show">
                <div class="col-md-12">
                    <div class="flex-this">
                        <div>
                            <h2 class="main_page_heading perfect_space">Projects (<?php echo ($countallprojects >= 1) ? $countallprojects : 0; ?>)</h2>
                            <p class="descr_subhead"><?php echo $desc_basedon_slug; ?></p>
                        </div>
                        <div class="header_searchbtn">
                             <a class="adddesinger" href="<?php echo base_url().'admin/Contentmanagement/projectReports'?>">Go Back</a>
                            <div class="search-first">
                                <div class="focusout-search-box">
                                    <div class="search-box">
                                        <form method="post" class="search-group clearfix">
                                            <input type="text" placeholder="Search here..." class="form-control searchdata searchdata_reports search_text">
                                            <div class="ajax_searchload" style="display:none;">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                            </div>
                                            <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                            <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                            <button type="submit" class="search-btn search search_data_ajax">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-site">
                                <i class="fas fa-sliders-h"></i>
                            </div>
                            <div class="gz-filter">
                                <div class="filter-header">
                                    <h2>Filter</h2>
                                    <a href="javascript:void(0)" class="close-filter">x</a>
                                </div>
                                <form method="Post" action="<?php echo base_url().'admin/dashboard/filter_form_submit'?>">
                                    <div class="filter-body">
                                        <div class="frmSearch">
                                            <input type="text" name="client_name" id="search_client" size="50"/>
                                            <div id="suggesstion-box"></div>
                                        </div>
                                        <br/>
                                        <select name="" id="" class="designers_show">
                                            <option value="">Choose Designers</option>
                                            <option value="designers">select Designer</option>
                                        </select>
                                        <ul class="designers_list" style="display: none;">
                                            <?php foreach ($designerlist as $dk => $dv) { ?>
                                                <li><input type="checkbox" class="des_name" name="des_name[]" value="<?php echo $dv['id']; ?>"/><?php echo $dv['first_name']; ?></li>
                                            <?php } ?>
                                        </ul>
                                        <div class="status-filter">
                                            <h4>Project Status</h4>
                                            <div class="sound-signal2">
                                                <div class="form-radion2">
                                                    <label class="containerr">IN-PROGRESS
                                                        <input type="checkbox" name="pro_status[]" value="active" checked="checked">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">IN-Queue
                                                        <input type="checkbox" name="pro_status[]" value="assign" checked="checked">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">PENDING REVIEW
                                                        <input type="checkbox" name="pro_status[]" value="pendingrevision">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">PENDING APPROVAL
                                                        <input type="checkbox" name="pro_status[]" value="checkforapprove">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                 <div class="form-radion2">
                                                    <label class="containerr">COMPLETED
                                                        <input type="checkbox" name="pro_status[]" value="approved">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">ON-HOLD
                                                        <input type="checkbox" name="pro_status[]" value="hold">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">CANCEL
                                                        <input type="checkbox" name="pro_status[]" value="cancel">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                         <?php if (!$full_admin_access && $_GET['assign'] == 1) { ?>
                                            <a href="<?php echo base_url(); ?>admin/dashboard" class="addderequesr">
                                                <span class="">All Project</span>
                                            </a>
                                        <?php } else if (!$full_admin_access) { ?>
                                            <a href="<?php echo base_url(); ?>admin/dashboard/index?assign=1" class="addderequesr">
                                                <span class="">Project assign to me </span>
                                            </a>
                                        <?php } ?>
                                        <div class="status-filter date-filter">
                                            <h4>Task Time</h4>
                                            <div class="date-stand">
                                                <div class="date-from">
                                                    <label>From</label>
                                                    <input type="date" class="week-picker hasDatepicker">
                                                </div>
                                            </div>
                                             <div class="date-stand">
                                                <div class="date-from">
                                                    <label>To</label>
                                                    <input type="date" class="week-picker hasDatepicker">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="status-filter verify-option">
                                            <h4>VERIFY OPTION</h4>
                                            <div class="sound-signal2">
                                                <div class="form-radion2">
                                                    <label class="containerr">VERIFY
                                                        <input type="checkbox" name="filetype[]" value="SourceFiles" checked="checked">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">VERIFIED
                                                        <input type="checkbox" name="filetype[]" value="jpeg">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter_button">
                                            <input type="submit" name="submit" value="Apply Filter">
                                            <!--<a href="" class="more_load">Apply Filter</a>-->
                                            <a href="<?php echo base_url().'admin/dashboard/' ?>" class="reset">Reset</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cli-ent table">
            <div class="tab-content">
                <!--Admin Active Section Start Here -->
                    <div class="product-list-show">
                            <div class="cli-ent-row tr tableHead">
                                <div class="cli-ent-col td" style="width: 25%;">
                                    PROJECT NAME
                                </div>
                                <div class="cli-ent-col td" style="width: 20%;">
                                    PROJECT STATUS
                                </div>
                                <div class="cli-ent-col td" style="width: 20%;">
                                    <?php //if($last_word == 'delayed_project'){
                                       // echo "EXPECTED DATE";
                                   // }else{
                                        echo "NUMBER OF REVISION";
                                    //}?>
                                </div>
                                <div class="cli-ent-col td" style="width: 25%;">
                                    TASK TIME
                                </div>
                            </div>
                        <input type="hidden" class="data_group" data-group="1">
                        <input type="hidden" class="data_total_count" data-total-count="<?php echo $countallprojects;?>">
                        <input type="hidden" class="data_loaded" data-loaded="<?php echo LIMIT_ADMIN_LIST_COUNT; ?>">
                        <input type="hidden" class="slug" slug="<?php echo $slug; ?>">
                        <input type="hidden" class="num" num="<?php echo $num; ?>">
                        <div class="row two-can"> 
                            <?php
                           // echo $countallprojects;exit;
                            if($countallprojects != 0){
                            for ($i = 0; $i < count($reports_projects); $i++) {
                                $data['projects'] = $reports_projects[$i];
                                $this->load->view('admin/content_management/project_listing_templ', $data);
                            }}else{
                                echo "<div class='no_data_ajax'>No record found</div>";
                            }
                            ?>
                        </div>
                        <?php if ($countallprojects > LIMIT_ADMIN_LIST_COUNT) { ?>
                            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" />
                            </div>
                            <div class="" style="text-align:center">
                                <a id="load_more_reports" href="javascript:void(0)" data-row="0" data-count="<?php echo $countallprojects; ?>" class="load_more load_more_rep button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--Admin Active Section End Here -->

                
            </div>                   
        </div>        
    </div>
</section>

<script type="text/javascript">
    var BUCKET_TYPE = '<?php echo isset($bucket_type) ? $bucket_type : ''; ?>';
    var ASSIGN_PROJECT = '<?php echo isset($_GET['assign']) ? $_GET['assign'] : ""; ?>';
    var timezone = '<?php echo isset($login_user_data[0]['timezone']) ? $login_user_data[0]['timezone'] : $_SESSION['timezone']; ?>';
    var $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
    var comefrm = '<?php echo isset($comefrm) ? $comefrm : ''; ?>';
</script>
