<link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/rangePicker.css');?>">

<?php $qustArr  = array(
        'q_instructions' => "Did you follow all the instructions given by the customer in the product descriptions or the latest revision comments?",
        'q_brand_guidelines' => "Is the design created according to the brand guidelines and information provided in the brand profile?",
        'q_attachments' =>  "Did you review all the attachments sent by the customer and the designer included them, as needed?",
        'q_dimensions' =>   "Is the design created according to the dimensions given by the customer?",
        'q_color_preferences' =>    "Is the design matching the color preferences given by the customer? (if provided)",
        'q_required_text' =>    "Did the designer include all the required text?",
        'q_source_files' => "Did the designer attach the correct source files as requested by the customer?",
        'q_font_files' =>   "Did the designer include the font files in the source folder if required? ",
        'q_quality_review' =>   "Are you satisfied with the quality of your design you are reviewing? (Does it look good?)"
 ); 

 $toalCount = count($questionData); 
   //   echo "<pre/>";print_R($questionData); die; 
 $totalRejection = array();  
 foreach ($questionData as $ky => $val) {

   if($val['q_instructions']==2){
        $totalRejection['q_instructions'] += 1; 
    }
    else if($val['q_brand_guidelines']==2){
        $totalRejection['q_brand_guidelines'] += 1; 
    }
    else if($val['q_attachments']==2){
        $totalRejection['q_attachments'] += 1; 

    }
    else if($val['q_dimensions']==2){
        $totalRejection['q_dimensions'] += 1; 
    }
    else if($val['q_color_preferences']==2){
        $totalRejection['q_color_preferences'] += 1; 
    }
    else if($val['q_required_text']==2){
        $totalRejection['q_required_text'] += 1; 
    }
    else if($val['q_source_files']==2){
        $totalRejection['q_source_files'] += 1; 
    }else if($val['q_font_files']==2){
        $totalRejection['q_font_files'] += 1; 
    }else if($val['q_quality_review']==2){
        $totalRejection['q_quality_review'] += 1; 
    }else{
       $totalRejection['StatusRejectedButStillApproved'] += 1;  
    }
    

 }
    // echo "<pre/>";print_R($totalRejection['StatusRejectedButStillApproved']); die; 

?>

<style type="text/css">.tr_custom td:hover{
cursor: pointer; 
}
.bjp-congras {
    display: block;
    float: right;
}
.red-theme-btn {
    text-align: center;
    font-size: 16px;
    border: 0;
    font-weight: 500;
    line-height: 50px;
    border: 1px solid #e42647;
    display: inline-block;
    background: #e42647;
    color: #fff;
    border-radius: 8px;
    margin: 0px 17px;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    transition: all 0.56s;
    text-decoration: none;
    padding: 0 20px;
    cursor: pointer;
}
</style>
<section class="con-b">
	<div class="container-fluid">
		<div class="matrix-section feedback-matrix-rate">
			


           <div class="row">
             <div class="col-md-12">
                <div class="rating-column">
                    <div class="feedback-item">
                        <div class="feedback-head">
                            <h2>Qa Feedback On Question</h2>
                                 <div class="rating-detailss">
                                    <button id="resettoorigenal" class="red-theme-btn" style="display: none; ">lifelong</button>

                      <div class="custom-range-picker">
                        <div id="range1">
                         <input id="startrange" name="startrange" style="display: none;">
                       </div>
                        
                    </div>
                        
                    </div>

                            <div class="bjp-congras upper normal_life_count" >
                                <span>Total Rejected <?php echo $toalCount;  ?></span>
                            </div>

                            <div id="ajax_total" class="bjp-congras upper ajax_total" style="display: none;">
                                <span></span>
                            </div>
                        </div>
                        <table class="feedback-col-table normal_lifelong">
                            <thead>
                                <?php $i=1; foreach ($qustArr as $key => $value) {?>       
                                
                                <tr>
                                    <th> <?php echo $i."&nbsp;&nbsp;".$value; ?> 
                                    <div class="bjp-congras">
                                        <span><?php echo $totalRejection[$key];  ?></span>
                                    </div>
                                </th>

                                </tr>
                            <?php $i++; } ?>
                            <tr>
                                <th>All the questions marked as Yes but Quality rejected. 
                                <div class="bjp-congras">
                                        <span><?php echo $totalRejection['StatusRejectedButStillApproved'];  ?></span>
                                    </div>
                                    </th>
                            </tr>
                            </thead>
                            <tbody>


                                 
                         </tbody>
                        <!--  <tfoot>
                            <tr>

                                <td><strong>Total</strong></td>
                                <td><strong>4595 </strong></td>
                                <td id="totalAvgRating"><strong>3.70</strong></td>
                                <td><strong>3.65</strong></td>
                                <td><strong>---</strong></td>

                            </tr>
                        </tfoot> -->
                    </table> 

                     <table class="feedback-col-table ajax_table" style="display: none;">
                            <thead>
                            </thead>    
                    </table> 
   </div>
</div>
</div>
</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/2.3.1/moment.min.js"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/rangePicker.js');?>">
</script>
 
 <script type="text/javascript">
  // using callbacks
   
 $("#startrange").daterangepicker({
  dateFormat: "yy/mm/dd",
    datepickerOptions : {
     numberOfMonths : 1,
     
     },
     presetRanges: true,
     initialText : 'Select',
    applyOnMenuSelect: false,
 
 });

$("#startrange").on('change', function(event,data) {  

var daterange1  = $("#range1").find(".ui-button-text").text();
 if(daterange1!="Select"){
  $("#range1").find("button").removeAttr("style"); 
 question_rejection(daterange1);
}else{
  $("#range1").find("button").css("border","1px solid #f92141"); 
}
 });
 function question_rejection(range){
    var qustArr = {}; 
         
    qustArr['q_instructions'] = "Did you follow all the instructions given by the customer in the product descriptions or the latest revision comments?";
    qustArr['q_brand_guidelines'] = "Is the design created according to the brand guidelines and information provided in the brand profile?";
    qustArr['q_attachments'] = "Did you review all the attachments sent by the customer and the designer included them, as needed?";
    qustArr['q_dimensions'] = "Is the design created according to the dimensions given by the customer?";
    qustArr['q_color_preferences'] = "Is the design matching the color preferences given by the customer? (if provided)";
    qustArr['q_required_text'] = "Did the designer include all the required text?";
    qustArr['q_source_files'] = "Did the designer attach the correct source files as requested by the customer?";
    qustArr['q_font_files'] = "Did the designer include the font files in the source folder if required?";
    qustArr['q_quality_review'] = "Are you satisfied with the quality of your design you are reviewing? (Does it look good?)";
    $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>admin/Contentmanagement/qa_question_matrix',
                data: {"range":range},  
                dataType:"json",  
                success: function(data) {

                    var html = {};
                    var append = "";
                    var i=1; 
                    $.each(data, function (key, value) { 
                      if(value.q_instructions == 2){
                            html['q_instructions'] = i++;     
                        }else if(value.q_brand_guidelines==2){
                           html['q_brand_guidelines'] =   i++;
                        }else if(value.q_brand_guidelines==2){
                            html['q_brand_guidelines']  = i++;
                        }else if(value.q_attachments==2){
                            html['q_attachments'] =  i++; 
                        }else if(value.q_dimensions==2){
                            html['q_dimensions'] =  i++; 
                        }else if(value.q_required_text==2){
                            html['q_required_text'] =  i++;
                        }else if(value.q_source_files==2){
                            html['q_source_files'] = i++;
                        }else if(value.q_font_files==2){
                            html['q_font_files'] =  i++;
                        }else if(value.q_quality_review==2){
                            html['q_quality_review'] =  i++;
                        }else{
                            html['StatusRejectedButStillApproved'] =  i++;
                        }
                        
                    });
                    var k=1;
                    var kultotal; 
                    var newarr = [];
                $.each(qustArr, function (key, value) { 

                    console.log("ASd",k); 

                    if(html[key]==null || html[key]=="undefined"){
                        html[key] = 0; 
                    } 
                    
                     newarr.push(html[key]); 
                    append += '<tr><th>'+k+' '+value+'<div class="bjp-congras"><span>'+html[key]+'</span></div></th></tr>'; 
                   k++; 
                });
                newarr.push(html['StatusRejectedButStillApproved']); 
                var kultotal = sum(newarr);

                if(html['StatusRejectedButStillApproved'] ==null || html['StatusRejectedButStillApproved']=="undefined"){
                        html['StatusRejectedButStillApproved'] = 0; 
                    }
                append += '<tr><th>All the questions marked as Yes but Quality rejected.<div class="bjp-congras">'+html['StatusRejectedButStillApproved']+'<span></span></div></th></tr>'; 
                // console.log(append);
                $(".normal_lifelong").hide(); 
                $(".normal_life_count").hide(); 
                $(".ajax_table").show();
                $(".ajax_total").show(); 
                $(".ajax_table thead").show().html(append);
                $(".ajax_total span").html("Total Rejected "+kultotal); 
                $("#resettoorigenal").show(); 
                

                }
            });
 }

 $("#resettoorigenal").click(function(){
    $(".ajax_table").hide();
    $(".ajax_total").hide(); 
    $(".normal_lifelong").show();
    $(".normal_life_count").show();
 });
 
 function sum(input) { 
 
    if (toString.call(input) !== "[object Array]") 
        return false; 

    var total = 0; 
    for(var i=0;i<input.length;i++) {                  
        if(isNaN(input[i])) { 
            continue; 
        } 

        total += Number(input[i]); 
    } 
    return total; 
  } 
 </script>
  

