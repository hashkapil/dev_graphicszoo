 <section class="con-b rating_details">
    <div class="container-fluid">

        <div class="matrix-section">

            <div class="all-feedback-colmn">
                <div class="overall-heading">
                        <h2>overall customers rating</h2> 
                        <a class="backbtn ratingback" href="<?php echo base_url();?>admin/Contentmanagement/feedback_matrix"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a> 
                        <div class="rating_details_FeedBack">
                          
                         <label class="rating_check">Feedback with text
                          <input type="checkbox" id="feedbackWithText">
                          <span class="checkmark"></span>
                        </label>
                        
                      </div>
                            <div class="download-exs">
                                <form method="post" id="download-exs-form" action="<?php echo base_url();?>admin/Contentmanagement/ExportdataINExl">
                                  <input type='hidden' id='withtext' name='withtext'>
                                  <button type="submit" class="export-btn"> Export Excel</button>
                                </form>
                                    
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="feedback-item">
                            
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Customer Name </th>
                                        <th>Project Name</th>
                                        <!--    <th># of Designs</th>-->
                                        <th>Design Name</th>
                                        <th>Date Delivered</th>
                                       <!--  <th>Design Draft</th> -->
                                        <th>Design Rating</th>
                                        <th>Rating Text</th>
                                        <th>Designer Name </th>
                                        <th>Qa Name</th>
                                         
                                    </tr>
                                </thead> 
                                <tbody id="rating_details" data-drafts="<?php echo count($info);?>">
                                    
                                     <?php 
                                     // echo count($rating_details); 
                                     foreach ($rating_details as $ky => $rating_info) {  
                                        if($ky <=  49){
                                      ?>
                                          
                                    <tr>
                                        <td><a href="<?php echo base_url();?>admin/accounts/view_client_profile/<?php echo $rating_info['c_id'];?>" target="_blank"><?php echo $rating_info['customer_name'];?></a></td>
                                         <td><a href="<?php echo base_url();?>admin/dashboard/view_request/<?php echo $rating_info['req_id'];?>" target="_blank"><?php echo $rating_info['request_name'];?></a></td>
                                         <!--<td><?php// echo $rating_info['no_drafts'];?></td>-->
                                         <td><a href="<?php echo base_url();?>admin/dashboard/view_files/<?php echo $rating_info['draft_id'];?>?id=<?php echo $rating_info['req_id'];?>" target="_blank"><?php echo $rating_info['design_name'];?></a></td>
                                         <td><?php echo $rating_info['created'];?></td>
                                         <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $rating_info["svg"];?>"><?php echo $rating_info['satisfied_with_design'];?></td>
                                         <td class="additional-note-des"> 
                                           <?php if($rating_info['additional_notes'] != ""){ ?>
                                          <i data-placement="bottom" data-toggle="tooltip" title="<?php echo $rating_info['additional_notes'];?>" class="far fa-comment-alt"></i>
                                           <?php }else{
                                               echo "";
                                           } ?>
                                         </td>
                                         <td><a href="<?php echo base_url();?>admin/accounts/view_designer_profile/<?php echo $rating_info['d_id'];?>" target="_blank"><?php echo $rating_info['designer_name'];?></a></td>
                                         <td><a href="<?php echo base_url();?>admin/accounts/view_qa_profile/<?php echo $rating_info['q_id'];?>" target="_blank"><?php echo $rating_info['qa_name'];?></a></td>
                                         
                                    </tr>
                                  
                                     <?php } }?>
                                </tbody>
                                  <!-- <tfoot>
                                    <tr>
                                         <td colspan="10" id="totalCount" data-count=" "><strong>Total</strong></td>
                                        
                                        
                                    </tr>
                                </tfoot> -->
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagination rating-peg">
        <span data-startfrom = "0" data-load-row="50" id="loadrows"></span>
     
      <a id="pegid_1" class="active" data-startfrom = "0" data-load-row="50" >1</a>
    <?php
        $count = count($rating_details); 
        $loopCount = round($count/50)+1;
         $total =  50;
    for($i=2; $i<= $loopCount; $i++) { 
             $limt = $total + 50;  
             
        ?>

      <a id="pegid_<?php echo $i; ?>" data-startfrom="<?php echo $total;  ?>" data-load-row="<?php echo $limt;  ?>" ><?php echo $i; ?></a>

               
      <?php   
        $total =  $limt; } 

      ?>
      
  </div>
 <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo base_url();?>public/assets/img/gz-ajax-loader.gif" />
 </div>
 
    
</section>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/jquery.min.js');?>"></script>

<script type="text/javascript">
    
  $('#feedbackWithText').click(function() {
    if(this.checked){
          $("#withtext").val("1");
              feedbackWithText(isChecked="1")   
              $(".rating-peg a").hide(); 
        }else{

          $("#withtext").val("0");
          $(".rating-peg a").show(); 
            
           feedbackWithText(isChecked="0")     
      }
      });

  function feedbackWithText(isChecked,start="",limit=""){
    console.log("isChecked",isChecked); 
    var html;
    $(".ajax_loader").show();
    $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>admin/Contentmanagement/rating_details_ajax',
                data: {"withText":isChecked,"start":start,"limit":limit},  
                dataType:"json",  
                success: function(data) {
                    if(data!=''){
                       var resultLength = data.length; 
                       var numberofPagnation =  Math.round(resultLength/50);
                         
                       for(i = 1; i<= numberofPagnation; i++)
                       {
                         $("#pegid_"+i).show();
                       }
                  $(".ajax_loader").hide();
                  $.each(data, function (key, value) {
                     html+= '<tr><td><a href="<?php echo base_url();?>admin/accounts/view_client_profile/'+value.c_id+'" target="_blank">'+value.customer_name+'</a></td><td><a href="<?php echo base_url();?>admin/dashboard/view_request/'+value.req_id+'" target="_blank">'+value.request_name+'</a></td><td><a href="<?php echo base_url();?>admin/dashboard/view_files/'+value.draft_id+'?id='+value.req_id+'" target="_blank">'+value.design_name+'</a></td><td>'+value.created+'</td> <td><img src="<?php echo base_url();?>public/assets/img/customer/'+value.svg+'">'+value.satisfied_with_design+'</a></td><td class="additional-note-des"><i data-placement="bottom" data-toggle="tooltip" title="'+value.additional_notes+'" class="far fa-comment-alt"></i></td><td><a href="<?php echo base_url();?>admin/accounts/view_designer_profile/'+value.d_id+'" target="_blank">'+value.designer_name+'</a></td><td><a href="<?php echo base_url();?>admin/accounts/view_qa_profile/'+value.q_id+'" target="_blank">'+value.qa_name+'</a></td></tr>';
                     

                   });
              }else{
                $(".ajax_loader").hide();
                html+= '<tr><td colspan="10" class="NoRecord">No Record found </td></tr>';
              }
                  $("#rating_details").html(html);
                  $('[data-toggle="tooltip"]').tooltip();
                } 
            });
  }

  $(".pagination a").click(function(){
     $(".pagination a").removeClass("active");
    $(this).addClass("active");
    // var data_count = $(".pagination").find("a.active").data("count");
    var startfrom = $(this).data("startfrom");
    var loadrow = $(this).data("load-row");
    if($('#feedbackWithText').prop("checked")){
        feedbackWithText(isChecked="1",startfrom,loadrow) 
    }else{
        feedbackWithText(isChecked="0",startfrom,startfrom) 
       // console.log("false");
    }
    

  });
</script>
