<?php //echo "<prE>"; print_r($info); exit; ?> 
<link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/rangePicker.css');?>">

<section class="con-b">
    <div class="container-fluid">

        <div class="matrix-section overall-sorting-table">

            <div class="all-feedback-colmn">
                <div class="overall-heading">
                        <h2>overall customers rating</h2>  
                        <a class="backbtn" href="<?php echo base_url();?>admin/Contentmanagement/feedback_matrix"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                        <div class="overall_date_pick custom-range-picker">
                           <!-- <button id="resettoorigenal" class="backbtn" style="">lifelong</button> -->
                           <div class="rating_details_FeedBack">

                            <label class="rating_check">lifelong
                              <input type="checkbox" id="resettoorigenal">
                              <span class="checkmark"></span>
                            </label>

                          </div>
                             <div id="range1">
                         <input id="startrange" name="startrange" style="display: none;">
                       </div>
                           

                               
                              <!-- <span id="dateRange1"  data-day="<?php echo date('d', strtotime("last week monday"));  ?>" data-start="<?php echo date("Y-m-d", strtotime("last week monday"));  ?>"> <?php echo date('M d', strtotime("last week monday"));  ?></span>
                              <span class='space'> - </span>
                              <span id="dateRange2" data-day="<?php echo date('d', strtotime("last sunday"));  ?>" data-end="<?php echo date("Y-m-d", strtotime("last sunday"));  ?>"><?php echo date('M d', strtotime("last sunday"));  ?></span> -->
                              
                            </div>
                            <div class="week-picker hide"></div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="feedback-item">
                            
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Design Category</th>
                                        <th>Design Sub-category</th>
                                        <th>No Of Designs </th>
                                        <th>Avg. rating</th>
                                        <th>Rejected % By category</th>
                                         
                                    </tr>
                                </thead>
                                <tbody id="OverAll" data-drafts="<?php echo count($info);?>">
                                   <?php foreach ($info as $k => $user_info) { ?> 
                                    <tr>
                                        <td> <?php echo $user_info['parent_cat_name'];?></td>
                                        <td> <?php echo $user_info['sub_cat_name'];?></td>
                                        <td> <?php echo $user_info['total_drafts'];?></td>
                                        <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $user_info["svg"];?>">  <?php echo number_format($user_info['total_rating'],2,'.','');?></td> 
                                        <td> <?php echo number_format($user_info['total_rejected'],2,'.','');?>%</td> 
                                    </tr>
                                 
                                    <?php
                                        $totl += $user_info['total_drafts'];
                                        $avgRating += $user_info['total_rating'] * $user_info['total_drafts'];
                                        $resultLoad = $k;
                                    }
                                    ?>
                                </tbody>
                                  <tfoot>
                                    <tr>
                                         <td id="totalCount" data-count="<?php echo $totl; ?>"><strong>Total &nbsp;</strong></td>
                                         <td></td>
                                         <td id="totalCounts"><strong> <?php echo $totl; ?></strong></td>
                                         <td id="totalRating"><strong><?php echo number_format($avgRating/$totl,2,'.','');?> </strong></td>
                                         <td><strong>---</strong></td>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo base_url();?>public/assets/img/gz-ajax-loader.gif" />
 </div>
    <!--div class="" style="text-align:center">
      <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $resultLoad; ?>" class=" load_more button">Load more</a>
    </div-->
    
</section>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/2.3.1/moment.min.js"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/rangePicker.js');?>">
</script>
 
 <script type="text/javascript">
  // using callbacks
 $("#startrange").daterangepicker({
  dateFormat: "yy/mm/dd",
    datepickerOptions : {
     numberOfMonths : 1,
     
     },
     presetRanges: true,
     initialText : 'Select',
    applyOnMenuSelect: false,
 
 });

$("#startrange").on('change', function(event,data) {  

var daterange1  = $("#range1").find(".ui-button-text").text();
console.log(".asd",daterange1);
 if(daterange1!="Select"){
  $("#range1").find("button").removeAttr("style"); 
  AjaXForGettingEntries(daterange1); 
 // question_rejection(daterange1);
}else{
  $("#range1").find("button").css("border","1px solid #f92141"); 
}
 });

$("#resettoorigenal").click(function(){
   if(this.checked){
   AjaXForGettingEntries(range=""); 
 }
});

function AjaXForGettingEntries(range){
  var draftCount  = $('#OverAll').attr('data-drafts');
   if(draftCount > 15){
    $("#load_more").show(); 
  }
  $(".ajax_loader").show();
   $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>admin/Contentmanagement/overall_rating_ajax',
        dataType: "json",
        data: {"range": range},

        success: function (res) {    
                      var html;
                      var total = 0;
                      var total_rating = 0;
                      var totlRating = 0;
                      if(res !=''){
                       $.each(res, function (key, value) {
                        $(".ajax_loader").hide();
                         html+= '<tr><td>'+value.parent_cat_name+'</td><td>'+value.sub_cat_name+'</td><td>'+value.total_drafts+'</td><td><img src="<?php echo base_url();?>public/assets/img/customer/'+value.svg+'">'+value.total_rating+'</td><td>'+value.total_rejected.toFixed(2)+'%</td></tr>';
                         total += parseInt(value.total_drafts);
                         total_rating +=parseInt(value.total_rating)*parseInt(value.total_drafts); 
                         totlRating = total_rating/total; 

                       });
                        // console.log("1");
                       $("#totalCounts strong").text(total);
                      $("#totalRating strong").text(totlRating.toFixed(2));
                     }else{
                        // console.log("2");
                      $(".ajax_loader").hide();
                      $("#totalCount strong").text(''); $("#load_more").hide();
                      html+= '<tr><td class="NoRecord" colspan="10">No Record For '+range+' </td></tr>';
                       $("#totalCounts strong").text('');
                       $("#totalRating strong").text('');
                    }
                      $("#OverAll").html(html);
                      
                    
    }
}); 
}
</script>
<style type="text/css">
    .comiseo-daterangepicker.ui-widget.ui-widget-content.ui-corner-all.ui-front.comiseo-daterangepicker-left {
position: absolute !important;
}
button#resettoorigenal {
    background: #fd2041;
    color: #fff;
    font-size: 15px;
    padding: 11px 20px;
    border-radius: 4px;
}
</style>