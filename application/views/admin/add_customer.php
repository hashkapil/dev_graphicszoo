<section id="content-wrapper">

    <div class="site-content-title">
        <h2 class="float-xs-left content-title-main">Add Designer</h2>

        <ol class="breadcrumb float-xs-right">
            <li class="breadcrumb-item">
                <span class="fs1" aria-hidden="true" data-icon="?"></span>
                <a href="#">Main Menu</a>
            </li>
            <li class="breadcrumb-item active">View Users</li>
        </ol>
    </div>
    <div class="content content-datatable datatable-width">
        <div class="all-form-section">
            <div class="row">
				<form method="post" enctype="multipart/form-data">
                <div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                        <label>First Name</label>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group">
							<input type="text" name="first_name" class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                        <label>Last Name</label>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group">
							<input type="text" name="last_name" class="form-control" />
                        </div>
                    </div>
                </div>

               

                <div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                        <label>Email</label>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group">
							<input type="email" class="form-control" name="email" />
                        </div>
                    </div>
                </div>

              

                <div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                        <label>Password</label>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group">
							<input type="password" class="form-control" name="password" />
                        </div>
                    </div>
                </div>

                <div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                        <label>Confirm Password</label>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group">
							<input type="password" class="form-control" name="confirm_password" />
                        </div>
                    </div>
                </div>
				
				
				<div class="payment-method">
					<h4 class="sign-up-form-heading margin-bottom-20" style="color:#ec1c41 !important;">Payment Method</h4>
					<div class="payment-method-inputs-wrapper">
						<div class="payment-method-input">
							<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
								<label>Card Number</label>
							</div>
							<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<div class="form-group">
								<div class="input tel">
									<input type="tel" name="last4" id="cc" class="textInput box_round4 transition masked" placeholder="XXXX XXXX XXXX XXXX" pattern="d{4} \d{4} \d{4} \d{4}" required maxlength='16' />
								</div>
								</div>
							</div>						
						</div>
						<div class="payment-method-input exp_date">
							<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
								<label>Exp Date</label>
							</div>
							<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<div class="form-group">
								<div class="input tel">
									<input type="tel" name="exp_month" id="expiration1" class="textInput box_round4 transition masked" placeholder="MM/YYYY" pattern="(1[0-2]|0[1-9])\/\d\d\d\d" required data-valid-example="11/2010"/>
								</div>
								</div>
							</div>							
						</div>
						<div class="payment-method-input">
							<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
								<label>Card CVC</label>
							</div>
							<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<div class="form-group">
								<div class="input tel">
									<input type="tel" name="cvc" id="tel" class="textInput box_round4 transition masked" placeholder="XXX" pattern="\\d{3}\\" required />
								</div>
								</div>
							</div>														
						</div>
						
					</div>
				</div>
				
				
				
				
				<div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                        <label>Select Plan</label>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group">
						   <select name="subscription_plans_id" class="form-control">
								<?php foreach($subscription as $key=>$value){ ?>
									<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
								<?php } ?>
						   </select>
                        </div>
                    </div>
                </div>
				
				<div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group">
						   <input type="submit" value="Add Customer" class="btn btn-success flat-buttons waves-effect waves-button">
                        </div>
                    </div>
                </div>
				</form>
				
            </div>
			
		
        </div>
    </div>
</section>