<?php 
$identifiers = array(
    "{{MESSAGES}}" => "Hello test messages",
    "{{CUSTOMER_NAME}}" => "Test User",
    "{{PROJECT_LINK}}" => "https://www.graphicszoo.com/",
    "{{CUSTOMER_EMAIL}}" => "hello@gmail.com",
    "{{CUSTOMER_PASSWORD}" => "12345",
    "{{PARENT_USER}}" => "Parent User",
    "{{LOGIN_URL}}" => "https://www.graphicszoo.com/login",
    "{{CHANGE_PASSWORD_LINK}}" => "https://www.graphicszoo.com/",
    "{{SENDER_NAME}}" => "Sender User",
    "{{PROJECT_TITLE}}" => "Test Project",
    "{{MESSAGE}}" => "<p style='color: #20364c; margin: 30px 35px 10px 35px;font: normal 15px/30px GothamPro-Medium,sans-serif;
    '>Hello test message",
    "{{NAME}}" => "Test name",
    "{{SUBJECT}}" => "Test subject",
    "{{EMAIL}}" => "test@gmail.com",
    "{{ACTIVATE_URL}}" => "https://www.graphicszoo.com/",
);
?>