<?php 
if(isset($chat_with_customer['fname']) && $chat_with_customer['fname']!=''){
    $username = $chat_with_customer['fname'];
}else{
    $username = $chat_with_customer['shared_name'];
}
if(strpos($chat_with_customer['profile_picture'], '/public/uploads/profile_picture/') !== false){
    $chat_with_customer['dp'] = $chat_with_customer['profile_picture'];
} else{
    $chat_with_customer['dp'] = '';
}
if($chat_with_customer['profile_picture'] != NULL){
    if(file_get_contents(FS_PATH. FS_UPLOAD_PUBLIC_UPLOADS_PROFILE ."_thumb/". $chat_with_customer['profile_picture'])){
        $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."_thumb/".$chat_with_customer['profile_picture'];
    }else{
        $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE .$chat_with_customer['profile_picture'];
    }
}else{
    $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."user-admin.png";
}
$docArrOffice = array('doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt','ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
    $font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
    $vedioArr = array('mp4', 'Mov');
    $audioArr = array('mp3');
    $ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');
    $zipArr = array('zip','rar');
?>
<div class="msgk-chatrow clearfix">
    <div class="msgk-user-chat msgk-left <?php echo $chat_with_customer['sender_type']; ?>">
           <?php if($chat_with_customer['dp']) { ?>
           <figure class="msgk-uimg t9">
                <span class="cust_tl"><?php //echo $username; ?></span>
                <a class="msgk-uleft" href="javascript:void(0)" title="<?php echo $username; ?>">
                        <img src="<?php echo $chat_with_customer['dp']; ?>" class="img-responsive">
                </a>
            </figure>
        <?php } ?>
         <p class="msgk-udate right">
            <?php echo $username . ' ' . $chat_with_customer['chat_created_time']; ?>
        </p>
        <div class="msgk-mn">
            <div class="msgk-umsgbox">
                <?php if($chat_with_customer['is_filetype'] != 1){ ?>
                <pre><span class="msgk-umsxxt message-text"><?php echo $chat_with_customer['message']; ?></span></pre>
                <?php }else {  ?>
        <!-- changes for file type in chatting -->
                <?php 
                $type = substr($chat_with_customer['message'], strrpos($chat_with_customer['message'], '.') + 1);
                 $updateurl = ''; 
                    if (in_array(strtolower($type), $docArrOffice)) {
                        $imgVar = FS_PATH_PUBLIC_ASSETS.'img/default-img/chat-file.svg';
                        $class = "doc-type-file"; 
                        $exthtml = '<span class="file-ext">'.$type.'</span>';
                     }elseif (in_array(strtolower($type),$font_file)){
                         $imgVar = FS_PATH_PUBLIC_ASSETS.'img/default-img/chat-file.svg';
                         $class = "doc-type-file"; 
                         $exthtml = '<span class="file-ext">'.$type.'</span>';
                     }elseif (in_array(strtolower($type),$zipArr)){
                         $imgVar = FS_PATH_PUBLIC_ASSETS.'img/default-img/chat-zip.svg';
                         $class = "doc-type-zip"; 
                         $exthtml = '<span class="file-ext">'.$type.'</span>';
                     }elseif (in_array(strtolower($type),$ImageArr)){
                         $imgVar = imageRequestchatUrl(strtolower($type), $chat_with_customer['request_id'], $chat_with_customer['message']);
                         $updateurl = '/_thumb'; 
                         $class = "doc-type-image"; 
                         $exthtml = '';

                     }
                  $basename =$chat_with_customer['message']; 
                  $basename = strlen($basename) > 20 ? substr($basename,0,20)."...".$type : $basename;
                  $data_src = FS_PATH_PUBLIC_UPLOADS.'requestmainchat/'.$chat_with_customer['request_id'].'/'.$chat_with_customer['message'];
                 ?>    
                    <span class="msgk-umsxxt">
                    
                    <div class="contain-info <?php echo (!in_array($type,$ImageArr)) ? 'only_file_class' :''; ?>">
                        <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>">
                            <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                        </a>
                        <div class="download-file">
                            <div class="file-name"><h4><b><?php echo $basename; ?></b></h4>
                            </div>
                        <a href="<?php echo base_url() ?>customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" target="_blank"> 
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                        </a>
                    </div>
                    </div>
                    
                    </span>
                <?php } ?>
            </div>
        </div>
    </div>
    </div>

                                                               

                                                                