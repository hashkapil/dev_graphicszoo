<style>
.pad{
	padding-right:5px;
}

.colorblack{
	color:#000;
	padding-bottom:0px;
	font-size:15px;
}

.nav-tab-pills-image ul li .nav-link:hover{
	border-bottom:unset !important;
}
.nav-tab-pills-image ul .nav-item+.nav-item{
	margin-left:9px;
}
.nav-tab-pills-image ul li .nav-link:focus
{
	border-bottom:unset !important;
}
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #e52344; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.pinktext { color: #e52344; }
</style>

<div class="Container-fluid">
	<div class="nav-tab-pills-image">
		<ul class="nav nav-tabs" role="tablist" style="border:none;">                      
			<li class="nav-item active" style="color:#fff;background:#ccc;border-radius: 12px 0px 0px 12px">
				<a class="nav-link"  href="<?php echo base_url(); ?>designer/profile/edit_profile"  style="color:#2f4458;background: #ccc;font-weight: 600;border-radius: 12px 0px 0px 12px">
					Edit Profile
				</a>
			</li>
			<li class="nav-item" style="color:#fff;background:#ccc;">
				<a class="nav-link" style="color:#2f4458;background: #ccc;font-weight: 600;" href="<?php echo base_url(); ?>designer/profile/change_password">
					Change Password
				</a>
			</li>
			 <li class="nav-item" style="background:#2f4458;margin-left:6px;color:#fff;border-radius: 0px 12px 12px 0px"">
				<a class="nav-link" href="<?php echo base_url(); ?>designer/profile/public_profile" style="background:#2f4458;color:#fff;border-radius: 0px 12px 12px 0px">
					Public Profile
				</a>
			</li>
			
		</ul>
		
	</div>
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="background-color:#fff;">
		
		<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 col-md-offset-3 col-lg-offset-3" style="padding:15px;">
		<?php if($this->session->flashdata('message_error') != '') {?>				

			   <div class="alert alert-danger alert-dismissable">

					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

					<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				

				</div>

			   <?php }?>

			   <?php if($this->session->flashdata('message_success') != '') {?>				

			   <div class="alert alert-success alert-dismissable">

					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

					<strong><?php echo $this->session->flashdata('message_success');?></strong>

				</div>

			   <?php }?>
		<form method="post" action="">
			<h2 style="text-align:center;">Public Profile</h2>
			<div class="pad">
				<p class="colorblack">About Me</p>
				<textarea rows="4" cols="50" placeholder="About Me" name="about_me" class="form-control"><?php echo $data[0]['about_me']; ?></textarea>
				
			</div>
			<div class="pad">
				<p class="colorblack">Specializations</p>
				<textarea rows="4" cols="50" placeholder="Specializations" name="specializations" class="form-control"><?php echo $data[0]['specializations']; ?></textarea>
			</div>
			<div class="pad" style="padding-top:10px;">	
				<input  style="background-color:#ec1c41;color:#fff" type="submit" name="comformbtn" value="Submit" class="form-control"/>
			</div>
		</form>
		<div class="col-md-4">
			<h2 class="greytext weight600" style="font-size:25px;"><?php if($dt[0]['grade'] == ""){ echo "No Rating"; }else { echo "Your Rating"; } ?></h2>
			<h3 class="orangetext" style="font-weight:600;font-size:45px;"><?php if($dt[0]['grade'] == ""){ echo "0.0"; }else{ echo round($dt[0]['grade'], 1);} ?></h3>
			<i class="fa fa-star <?php if($dt[0]['grade'] >= "1"){ echo "orangetext"; } ?>"></i>
			<i class="fa fa-star <?php if($dt[0]['grade'] >= "2" ){echo "orangetext"; } ?>"></i>
			<i class="fa fa-star <?php if($dt[0]['grade'] >= "3" ){echo "orangetext"; } ?>"></i>
			<i class="fa fa-star <?php if($dt[0]['grade'] >= "4" ){echo "orangetext"; } ?>"></i>
			<i class="fa fa-star <?php if($dt[0]['grade'] >= "5" ){echo "orangetext"; } ?>"></i>
		 </div>
		</div>
		
	</div>
</div>	