<div class="se-pre-con" style="display: none;"></div>
<section class="con-b">
    <div class="container-fluid">
        <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php }
            ?>
        </div>
    <input type="hidden" value="<?php  echo LIMIT_DESIGNER_LIST_COUNT ?>" id="LoadMoreLimter">
      <div class="header-blog">
         <div class="row flex-show">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading">Projects</h2>
                    <div class="header_searchbtn">
                        <div class="search-first">
                            <div class="focusout-search-box">
                                <div class="search-box">
                                    <form method="post" class="search-group clearfix">
                                        <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                        <div class="ajax_searchload" style="display:none;">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                        </div>
                                        <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                        <button type="submit" class="search-btn search search_data_ajax">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </div>
<!--                        <div class="filter-site">
                            <i class="fas fa-sliders-h"></i>
                        </div>-->
                        </div>
                    </div>
                </div>
             </div>
          </div>
     <div class="header-blog">
               <ul  id="status_check" class="list-unstyled list-header-blog" role="tablist" style="border:none;">
                  <li class="active" id="1">
                     <a data-status="active,disapprove" data-count ="<?php echo isset($count_active_project) ? $count_active_project : '0'; ?>" data-toggle="tab" href="#designs_request_tab" role="tab" data-ajax-load="0">
                     Active (<?php echo isset($count_active_project) ? $count_active_project : '0'; ?>)
                     </a>
                  </li>
                  <li class="" id="3">
                     <a data-status="pendingrevision" data-count="<?php echo isset($count_pending_revision_project) ? $count_pending_revision_project : 0; ?>" data-toggle="tab" href="#pending_revision_tab" role="tab" data-ajax-load="0"> 
                     Pending Review (<?php echo isset($count_pending_revision_project) ? $count_pending_revision_project : 0; ?>)
                     </a>
                  </li>
                  <li class="" id="4">
                     <a data-status="checkforapprove"  data-count="<?php echo isset($count_pending_approval_projectt) ? $count_pending_approval_projectt : '0'; ?>" data-toggle="tab" href="#pending_designs_tab" role="tab" data-ajax-load="0">
                     Pending Approval (<?php echo isset($count_pending_approval_projectt) ? $count_pending_approval_projectt : '0'; ?>)
                     </a>
                  </li>
                  <li class="" id="5">
                     <a data-status="approved" data-toggle="tab" href="#approved_designs_tab" data-count="<?php echo isset($count_approved_project) ? $count_approved_project : '0'; ?>" role="tab" data-ajax-load="0">
                     Completed (<?php echo isset($count_approved_project) ? $count_approved_project : '0'; ?>)
                     </a>
                  </li>
                  <li class="" id="6">
                     <a data-status="hold" data-count= "<?php echo isset($count_hold_project) ? $count_hold_project : '0'; ?>" data-toggle="tab" href="#hold_designs_tab" role="tab" data-ajax-load="0">
                     On Hold (<?php echo isset($count_hold_project) ? $count_hold_project : '0'; ?>)
                     </a>
                  </li>
               </ul>
            </div>
      <div class="pro-deshboard-list">
        <!-- main div start from here  -->
        <?php 
          $ArrVar =  array('designs_request_tab','pending_revision_tab','pending_designs_tab','approved_designs_tab','hold_designs_tab');
          foreach ($ArrVar as $k => $ListIds) { ?> 
             <div class="tab-content">
                <div data-group="" data-loaded=""  data-total-count="<?php echo $count_active_project; ?>" class="tab-pane content-datatable datatable-width" id="<?php echo $ListIds; ?>" role="tabpanel">
                   <div class="product-list-show">
                       <div class="cli-ent-row tr tableHead">
                           
                            <div class="cli-ent-col td" style="width: 30%;">
                                PROJECT NAME
                            </div>
                            <div class="cli-ent-col td" style="width: 20%;">
                                PROJECT STATUS
                            </div>
                            <div class="cli-ent-col td" style="width: 20%;">
                                CLIENT
                            </div>
                           <div class="cli-ent-col td"style="width:20%;">
                                EXPECTED/DELIVERY
                            </div>
                            <div class="cli-ent-col td"style="width: 10%;">
                                VERIFIED
                            </div>
                        </div>
                      <div class="row two-can">
                         <!-- Code goes here  -->
                      </div> 
                     
                      <div class="gz ajax_loader" style="display:none; text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" />
                      </div>
                        
                       <div class="load_more_div" class="hide" style="text-align:center;">
                         <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more button">Load more</a>
                      </div>
                   </div>
                </div>
             </div>
             <?php  } ?>
             <!-- main div closed here  -->
      </div>
   </div>
</section>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/designer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/qa/bootstrap.min.js"></script>
<script type="text/javascript">
   // For Ajax Search
   var base_url = '<?php echo base_url(); ?>'; 
   $.urlParam = function (name) {
   var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
   if (results == null) {
   return null;
   }
   else {
   return decodeURI(results[1]) || 0;
   }
   }
   var status = $.urlParam('status');
   $('#status_check #' + status + " a").click();
   $('#right_nav > li').click(function () {
   $(this).toggleClass('open');
   });
   
   function load_more_designer(data_status=null,tabID=null,listText=null) {
    $(".ajax_loader").css("display", "flex");
   countTimer();
        if(tabID==null){
            var tabid = $('#status_check li.active a').attr('href');
          }else{
             var tabid =tabID;
          }
         var activeTabPane = $('.tab-pane.active');
          if(data_status==null){
               var status_scroll = $('.list-header-blog li a').attr('data-status');
          }else{
              status_scroll = data_status;
          }
          var searchval = $('.search_text').val();
//        var searchval = activeTabPane.find('.search_text').val();
        $.post('<?php echo site_url() ?>designer/request/load_more_designer', {'group_no': 0, 'status': status_scroll, 'search': searchval,'tabId':tabid},
            function (data) {
              $(".ajax_loader").css("display", "none");
                var newTotalCount = 0;
                if (data != '') {
                    $(".ajax_searchload").fadeOut(500);
                    $(tabid+" .product-list-show .row").html(data);
                   // newTotalCount = $(tabid + " .product-list-show .row").find("#loadingAjaxCount").attr('data-value');    
                   //$(tabid + " .product-list-show .row").find("#loadingAjaxCount").remove();
                    activeTabPane.attr("data-total-count", newTotalCount);
                    activeTabPane.attr("data-loaded", $rowperpage);
                    activeTabPane.attr("data-group", 1);
                }
                else
                {
                    activeTabPane.attr("data-total-count", 0);
                    activeTabPane.attr("data-loaded", 0);
                    activeTabPane.attr("data-group", 1);
                    if(listText!=null)
                    {
                      $(tabid+ " .product-list-show .row").html("<span class='Empty_record'> No"+listText+" Request found ..!!</span>");
                    }
                  else
                  {
                    $(tabid+" .product-list-show .row").html("<span class='Empty_record'> No Record found ..!!</span>");
                  }
                }
                if ($rowperpage >= newTotalCount) {
                    activeTabPane.find('.load_more').css("display", "none");
                } 
                else 
                {
                    activeTabPane.find('.load_more').css("display", "inline-block");
                }
            });
   }
   
   /*********Search *******************/
   $('.search_data_ajax').click(function (e) {
       e.preventDefault();
       load_more_designer();
       });
   $('.searchdata').keyup(function (e) {
       e.preventDefault();
       var status =  $('#status_check li.active a').data('status');
       delay(function () {
       $(".ajax_searchload").fadeIn(500);
       load_more_designer(status);
       }, 1000);
   });
   var delay = (function () {
   var timer = 0;
   return function (callback, ms) {
   clearTimeout(timer);
   timer = setTimeout(callback, ms);
   };
   })();
   
   /**************start on load & tab change get all records with load more button*****************/
        $(document).ready(function () {
        countTimer();
        var status = $('#status_check li.active a').attr('data-status');
        var id = $('#status_check li.active a').attr('href');
        $rowperpage = <?php echo LIMIT_DESIGNER_LIST_COUNT; ?>;
        $('.tab-pane').attr("data-loaded", $rowperpage);
        $(document).on("click", "#load_more", function () {
            countTimer();
            $(".ajax_loader").css("display", "flex");
            $(".load_more").css("display", "none");
            var row = parseInt($(this).attr('data-row'));
            row = row + $rowperpage;
            var activeTabPane   =   $('.tab-pane.active');
            var searchval       =   activeTabPane.find('.search_text').val();
            var allLoaded       =   $('#LoadMoreLimter').val();
            var allcount        =   $('#status_check li.active a').data('count');
            var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
            var status_scroll = $('#status_check li.active a').attr('data-status');
            var tabid = $('#status_check li.active a').attr('href');
    if (allLoaded < allcount) {
          $.post('<?php echo site_url() ?>designer/request/load_more_designer', {'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval},
          function (data) {
          if (data != "") {   
                  $(tabid+" .product-list-show .row").append(data);
                  row = row + $rowperpage;
                  $(".ajax_loader").css("display", "none");
                  $(".load_more").css("display", "inline-block");
                    activeTabPane.find('.load_more').attr('data-row', row);

                  activeTabPaneGroupNumber++;
                  activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                  allLoaded = allLoaded + $rowperpage;
                  activeTabPane.attr('data-loaded', allLoaded);
         
              if (allLoaded >= allcount) {
                    activeTabPane.find('.load_more').css("display", "none");
                    } else {
                    activeTabPane.find('.load_more').css("display", "inline-block");
                    }
              }  
          });
   }
   
   });
   });
   /**************End on load & tab change get all records with load more button*****************/
        $(document).on('change','.is_verified input[type="checkbox"]',function(e){
             var data_id = $(this).attr('data-pid');
             var ischecked = ($(this).is(':checked')) ? 2 : 0;
             if(ischecked == 2){
               $('#verified_'+data_id).addClass('verified_by_designer'); 
               $("#verified_"+data_id+" .verified").prop("disabled", true);
               if ($(this).closest('label').find('i').hasClass("fa-circle")) {
                    $(this).closest('label').find('i').removeClass('fa-circle');
                    $(this).closest('label').find('i').addClass('fas fa-check-circle');
                    $(this).closest('label').find('.vrfy_txt').html('Verified');
                }else if($(this).closest('label').find('i').hasClass("fa-check-circle")){
                   $(this).closest('label').find('i').removeClass('fa-check-circle');
                   $(this).closest('label').find('i').addClass('fas fa-check-circle'); 
                   $(this).closest('label').find('.vrfy_txt').html('Verify');
                }
             }else{
                $('#verified_'+data_id).removeClass('verified_by_designer'); 
             }
             $.ajax({
                  type: 'POST',
                  url: "<?php echo base_url(); ?>designer/request/request_verified_by_designer",
                  data: {data_id: data_id,ischecked: ischecked},
                  success: function (data) {
                  }
                  });
        });
   function countTimer() {
           $('.expectd_date').each(function () {
               var expected_date = $(this).attr('data-date');
               var user_datetimezone = '<?php echo $_SESSION['timezone'] ?>';
               var data_id = $(this).attr('data-id');
               // Set the date we're counting down to
               var countDownDate = new Date(expected_date).getTime();
               // Update the count down every 1 second
               var x = setInterval(function () {
               // Get todays date and time
               var nowdate = new Date().toLocaleString('en-US', { timeZone: user_datetimezone });
               if(user_datetimezone != '' || user_datetimezone != null)
               {
                  var now = new Date(nowdate).getTime();
               }
               else
               {
                  var now = new Date().getTime();
               }
           // Find the distance between now and the count down date
           var distance = countDownDate - now; 
           // Time calculations for days, hours, minutes and seconds
           var days = Math.floor(distance / (1000 * 60 * 60 * 24));
           var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
           var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
           var seconds = Math.floor((distance % (1000 * 60)) / 1000);
               if(minutes > 0)
               {
                 var timer = ("0" + days).slice(-2) + " : " +("0" + hours).slice(-2) + " : "
                 + ("0" + minutes).slice(-2) + " : " + ("0" + seconds).slice(-2) + " ";
                 $('#demo_'+data_id).text(timer);
               }else
                 {
                   $('#demo_'+data_id).text("EXPIRED");
                 }
              }, 1000);
           });
     }
   
   // get data response usinf ajax   
       $('.list-header-blog li a').click(function(e){
          e.preventDefault();
              $('.searchdata').val('');
              var active_tab          =   $(this);
              var status              =   active_tab.data('status');
              var tabid               =   active_tab.attr('href');
              var listText            =   active_tab.text().replace('(0)', '');
              var limter_load         =   $('#LoadMoreLimter').val();
              var total_active_count  =   active_tab.data('count'); 
              var ajax_count          =   parseInt(active_tab.data('ajax-load'));
              $('.tab-pane').removeClass("active");
              $(tabid).addClass("active");
              console.log(ajax_count);   
            
           if(total_active_count > limter_load )
            {
              $('.load_more_div').removeClass('hide');
            }
            else{
              $('.load_more_div').addClass('hide');
            }          
           //if(ajax_count != "1"){
              active_tab.data('ajax-load',"1");  
              load_more_designer(status,tabid,listText);
           //}
      }); 
       $('.pro-desh-box').click(function(){
        var project_id = $(this).data('proid');
        RedirecTURL(project_id);
       });

       function RedirecTURL(pro_id=null){
        $('.se-pre-con').fadeIn(); 
        var redirect_url = "<?php echo base_url(); ?>designer/request/project_info/"+pro_id+"/1";
        location.href=redirect_url;
       }

  
       $(window).on("load",function(e){
            e.preventDefault();
            var active_tab        =       $('#status_check li.active a');
            var status            =       active_tab.data('status');
            var tabid             =       active_tab.attr('href');
            var limter_load       =       $('#LoadMoreLimter').val();
            var total_active_count=       active_tab.data('count');
            active_tab.attr('data-ajax-load',"1");
                $(tabid).addClass('active');
              if(total_active_count >limter_load ){
                $('.load_more_div').removeClass('hide');
              }else{ 
                $('.load_more_div').addClass('hide');
              }  
               load_more_designer(status,tabid);
               
       });
       
       $(document).on('click','.pro-desh-row',function(){
        $(this).toggleClass('fullheight',1000);
        $(this).find('.mobile-visibles i').toggleClass('fa-plus-circle fa-minus-circle')
    });
    
</script>