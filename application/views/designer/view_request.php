<style>
#content-wrapper .content{padding: 0; overflow: visible !important}
.content-title-main{padding-left: 0;}
#content-wrapper{margin-top: 0 !important;}
</style>

<div class="clearfix content_title_dv">
<h2 class="float-xs-left content-title-main"><?php echo $data[0]['title']; ?></h2>
<div class="status-detail">
<a href="#" class="label label-default">Review Designs</a>
</div>
</div><!-- crearfix -->

</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1" style="padding:30px 0 0;">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575 !important; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.darkblackbackground { background-color:#1a3147; } 
.lightdarkblacktext { color:#b4b9be; }
.lightdarkblackbackground { background-color:#f4f4f4; }
.pinktext { color: #ec4159 !important; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
font-size: 31px !important;
padding: 8px 0px !important;
font-weight: 600 !important;
letter-spacing: -3px !important;
}
.projecttitle{
font-size: 25px;
padding-bottom: 0px;
text-align: left;
padding-left: 20px;
}
.trborder{
border: 1px solid #000;
background: unset;
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
color: #ec4159;
font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link{
background:#ededed;
font-weight: 600;
color: #2f4458;
}
.ls0{ letter-spacing: 0px; }
h4.darkblacktext{ font-size:18px; }
span.lightdarkblacktext {
font-size: 16px !important;

}
.chetcolor {
color: #464646;
}

.nav-tab-pills-image ul li .nav-link:hover
{
border-bottom:unset !important;
color:#2f4458 !important;
}
.nav-tab-pills-image ul li .nav-link active a:hover{
color:#fff !important;
}
.nav-tab-pills-image ul li .nav-link active a{
color:#fff !important;
}

.hovmein a:hover {
color: #f7941f !important;

}	
.wrapper .nav-tab-pills-image ul .active .nav-link:hover {
color: #fff !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:focus
{
color:#fff;
border:none;
}
.content .nav-tab-pills-image .nav-item.active a:hover{
color:#fff;

}
.content .nav-tab-pills-image .nav-item.active dd {
background:unset !important;
}
.nav-tab-pills-image ul li .nav-link:hover {
border-bottom: none !important;
}
.content .nav-tab-pills-image .nav-item.active a {

border:unset !important;
}
.content .nav-tab-pills-image .nav-item.active a {
/* border-bottom: 3px solid #ec1c41 !important; */
color: #fff;
background: #2f4458;
}
.slick-next::before {
font-family: FontAwesome;
content: "\f105";
font-size: 22px;
background: #FFFFFF;
opacity: 1;
border-radius: 30px;
color: #1a3147;
background-color: #e4e4e4;
width: 30px;
height: 30px;
line-height: 28px;
text-align: center;
display: inline-block;
}

.slick-prev::before {
font-family: FontAwesome;
content: "\f104";
font-size: 22px;
opacity: 1;
border-radius: 30px;
color: #1a3147;
background-color: #e4e4e4;
width: 30px;
height: 30px;
line-height: 28px;
text-align: center;
display: inline-block;
}
.font16 h4{
font-size:16px !important;
}
span.read_more_additional.more_additional {
font-size: 16px;
}
</style> 
<div class="content">

<div class="row">
<div class="">

<?php /* ?>
<!-- <div class="nav-tab-pills-image"> -->
<ul class="nav nav-tabs" role="tablist" style="border-bottom:unset !important;">                      
<li class="nav-item active" style="background-color:#dfdfdf;border-radius: 8px 0px 0px 8px;">
<a class="nav-link" data-toggle="tab" href="#designs_request_tab" role="tab" style="width:145px;height:40px;border-radius: 8px 0px 0px 8px;">
Project Info
</a>
</li>
<li class="nav-item" style="margin-left:0px;background-color:#dfdfdf;border-radius: 0px 8px 8px 0px;">
<a class="nav-link" data-toggle="tab" href="#inprogressrequest" role="tab" style="width:145px;height:40px;border-radius: 0px 8px 8px 0px;">
Files
</a>
</li>
</ul><!-- nav -->
<?php */ ?>


<div class="col-lg-8">
<div class="info-left-div">
<div class="row">
<div class="col-md-12">
<div class="clearfix draft-title design_draft-title">
<h5 class="darkblacktext weight600 ls0 float-left">Design Drafts</h5>
<div class="float-right designer-name">
<button id="" data-toggle="modal" data-target="#myModalSource" class="btn pinktext weight600" style="background:#fff;border-radius:5px;border: 2px solid;float: right;">+ Upload Drafts</button>
</div>
</div><!-- dreaft title -->
</div><!-- col12 -->

<div class="col-sm-12" style=" height:510px; overflow-y:scroll;">
<?php for ($i = 0; $i < sizeof($data[0]['designer_attachment']); $i++) { ?>
<div class="col-md-6" style="padding-bottom: 15px;<?php
if (($i) % 3 == 0) {
echo "clear:both;";
}
?>">
<div class="files-box">
zz<?php
$ap = $data[0]['designer_attachment'][$i]['status'];
if ($ap == "Approve") {
echo '<span class="label">Approved</span>';
}
if ($ap == "Reject") {
echo '<span class="label" style ="background-color:#e42244">Rejected</span>';
}
?>
<!--                                                               <span class="label">Approved</span>-->
<!--<span class="notesnum notnum">2</span>-->

<div class="filesbox-img lightdarkblackbackground">
<a href="<?php echo base_url() . "designer/request/view_files/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
<img src="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['designer_attachment'][$i]['file_name']; ?>" style="max-height:170px;"/>
</a>
<p style="padding: 0px !important;position: absolute;left: 90px;">
<?php
if ($data[0]['status'] == "approved") {
$rate = $data[0]['designer_attachment'][$i]['customer_grade'];
for ($j = 1; $j <= 5; $j++) {
if ($j <= $rate) {
echo '<i class="fa fa-star pinktext myapprovestar" ></i>';
} else {
echo '<i class="fa fa-star myapprovestar greytext"></i>';
}
}
} else {
echo '<form method="post" style="display: inline-block;position: absolute;bottom: 0px;left: 90px;">';
$rate = $data[0]['designer_attachment'][$i]['customer_grade'];
for ($j = 1; $j <= 5; $j++) {
if ($j <= $rate) {
echo '<i class="fa fa-star mystar pinktext fa-star' . $j . $data[0]['designer_attachment'][$i]['id'] . '" data-index="' . $j . '" data-id="' . $data[0]['designer_attachment'][$i]['id'] . '" ></i>';
} else {
echo '<i class="fa fa-star mystar greytext fa-star' . $j . $data[0]['designer_attachment'][$i]['id'] . '"  data-index="' . $j . '"  data-id="' . $data[0]['designer_attachment'][$i]['id'] . '"></i>';
}
}
echo '<input type="hidden" name="customer_rating" id="customer_rating' . $data[0]['designer_attachment'][$i]['id'] . '">';
echo '<input type="hidden" name="file_id" value="' . $data[0]['designer_attachment'][$i]['id'] . '">';
echo '<input type="hidden" name="request_id" value="' . $data[0]['id'] . '">';
echo '<input type="submit" id="form_' . $data[0]['designer_attachment'][$i]['id'] . '" style="display:none;"/>';
echo '</form>';
}
?>
</p>
<?php
for ($j = 0; $j < sizeof($file_chat_array); $j++) {
if ($file_chat_array[$j]['id'] == $data[0]['designer_attachment'][$i]['id']) {
?>
<h4 class="notesnummain">
<span class="notesnum notnum"><?php echo $file_chat_array[$j]['count']; ?></span>
</h4>
<?php
}
}
if ($data[0]['designer_attachment'][$i]['grade']) {
?>
<div class="notsum" style="background: #ec3d56;padding: 5px;color: #fff;position:absolute;right:0px;left: unset;">
<?php echo $data[0]['designer_attachment'][$i]['grade']; ?>
</div>
<?php } ?>
</div>
<script>

</script>
</div>
</div>
<?php } ?>
</div><!-- col -->
</div><!-- row -->
</div><!-- info-left-div -->
</div><!-- col -->

<div class="col-lg-4">
<div class="info-left-div design_msg_dv">
<div class="chat-main">
<h3 class="darkblacktext weight600 ls0">Messages</h3>
<div class="send-msg-to">
<?php
//echo "<pre>"; print_r($data[0]); echo "</pre>";
if (array_key_exists("customer_profile_picture",$data[0])) {
echo '<span class="lightdarkblacktext"><img src="' . base_url() . 'uploads/profile_picture/' . $data[0][ 'customer_profile_picture' ] . '" class="img-fluid"> </span>';
}
else{
echo '<img src="'.base_url().'public/img/img/client2.png" class="img-fluid">';
}
?>

<h5>TO:</h5>
<h5 class="nameofreceiver"><?php echo $data[0][ 'customer_first_name' ] . " ". $data[0]['customer_last_name'] ?></h5>


</div>

<?php /* ?>
<div class="col-md-12 hovmein" style="padding-bottom:10px;">
<?php for ($i = 0; $i < sizeof($file_chat_array); $i++) { ?>
<?php if ($file_chat_array[$i]['id'] != "") { ?>
<a href="<?php echo base_url() . "designer/request/view_files/" . $file_chat_array[$i]['id'] . "?id=" . $request_id; ?>">
<h4 class="notesnummain">
<span class="notesnum notnum"><?php echo $file_chat_array[$i]['count']; ?></span> Notes for <?php echo $file_chat_array[$i]['file_name']; ?>
</h4>
</a>
<?php } ?>
<?php } ?>
</div>
<?php */ ?>

<div class="chat_frame myscroller_div myscroller messagediv_<?php echo $data[0]['id']; ?>" style="clear:both;padding:0 0 15px; overflow-y: auto; height:345px;">
<?php for ($j = 0; $j < sizeof($chat_request); $j++) { ?>
<?php if ($chat_request[$j]['sender_type'] == "designer") { ?>

<div class="chatbox2 rightchat">
<div class="message-text-wrapper rightchatmain">
<p class="title">
<?php
if ($j != 0) {
if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type']) {
echo $data[0]['designer_name'];
}
} else {
echo $data[0]['designer_name'];
}
?>
</p><!-- title -->
<p class="description"><?php echo $chat_request[$j]['message']; ?></p>
</div><!-- rightchatmain -->

<p class="posttime">
<?php
$date = strtotime($chat_request[$j]['created']); //Converted to a PHP date (a second count)
//Calculate difference
$diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
$minutes = floor($diff / 60);
$hours = floor($diff / 3600);
if ($hours == 0) {
echo $minutes . " Minutes Ago";
} else {
$days = floor($diff / (60 * 60 * 24));
if ($days == 0) {
echo $hours . " Hours Ago";
} else {
echo $days . " Days Ago";
}
}
?>
</p>
</div><!-- rightchat -->

<?php } else { ?>

<div class="chatbox leftchat">


<div class="message-text-wrapper">

<p class="title">
<?php
if ($j != 0) {
if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type']) {
if ($chat_request[$j]['sender_type'] == "designer") {
echo $data[0]['designer_name'];
} elseif ($chat_request[$j]['sender_type'] == "admin") {
echo "Admin";
} elseif ($chat_request[$j]['sender_type'] == "qa") {
echo "QA";
} elseif ($chat_request[$j]['sender_type'] == "va") {
echo "VA";
}
}
} else {
if ($chat_request[$j]['sender_type'] == "designer") {
echo $data[0]['designer_name'];
} elseif ($chat_request[$j]['sender_type'] == "admin") {
echo "Admin";
} elseif ($chat_request[$j]['sender_type'] == "qa") {
echo "QA";
} elseif ($chat_request[$j]['sender_type'] == "va") {
echo "VA";
}
}
?>
</p>
<p class="description">
<?php echo $chat_request[$j]['message']; ?>
</p>                                                            
</div><!-- message-text-wrapper -->
<p class="posttime">
<?php
$date = strtotime($chat_request[$j]['created']); //Converted to a PHP date (a second count)
//Calculate difference
$diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
$minutes = floor($diff / 60);
$hours = floor($diff / 3600);
if ($hours == 0) {
echo $minutes . " Minutes Ago";
} else {
$days = floor($diff / (60 * 60 * 24));
if ($days == 0) {
echo $hours . " Hours Ago";
} else {
echo $days . " Days Ago";
}
}
?>
</p>

</div><!-- leftchat -->

<?php } ?>
<?php } ?>

</div>

<div class="chatbtn-boxmain">
<input type='text' class='form-control t_w text_<?php echo $data[0]['id']; ?>' placeholder="Reply To Client" />
<span class="chatsendbtn send_request_chat send s_t"
data-requestid="<?php echo $data[0]['id']; ?>"
data-senderrole="designer" 
data-senderid="<?php echo $_SESSION['user_id']; ?>" 
data-receiverid="<?php echo $data[0]['customer_id']; ?>" 
data-receiverrole="customer"
data-sendername="<?php echo $data[0]['designer_name']; ?>">Send
</span>
</div><!-- chatbtn-boxmain -->

</div>
</div>
</div><!-- col4 -->
</div><!-- col 12 -->

<div class="row project_information_bx">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 project_information_section">
<div class="info-left-div design_bot_dv" style="padding-bottom: 30px;">

<?php if ($this->session->flashdata('message_error') != '') { ?>                
<div class="alert alert-danger alert-dismissable" style="margin-top:10px;">
<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
<strong><?php echo $this->session->flashdata('message_error'); ?></strong>              
</div> 
<?php } ?>
<?php if ($this->session->flashdata('message_success') != '') { ?>              
<div class="alert alert-success alert-dismissable" style="margin-top:10px;">
<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
<strong><?php echo $this->session->flashdata('message_success'); ?></strong>
</div>
<?php } ?>

<div class="col-md-12">
<div class="draft-title">
<h5 class="darkblacktext weight600 ls0 float-left">Project Information</h5>
<div class="designer-name">
<span class="fa fa-pencil" aria-hidden="true"></span>
<input type="submit" onclick="my_eidit()" class="btnmy1" value="">
</div>
</div>

<?php /* ?>
<p>
<h4 class="darkblacktext weight600 ls0" style="display:inline-block;">Priority:</h4>
<span class="greenbackground" style="padding: 10px;color: white;margin-left: 15px;font-size: 18px;">1st</span>
</p>
<?php */ ?>
</div>

<style>
.font16 h4{
font-size:16px !important;
}
span.read_more_additional.more_additional {
font-size: 16px;
}
</style>
<div class="col-md-5 font16">

<div style="margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0">Category:</h4>
<span class="lightdarkblacktext"><?php echo $data[0]['category']; ?></span>
</div>
<div style="margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0">Project Title:</h4>
<span class="lightdarkblacktext"><?php echo $data[0]['title']; ?></span>
</div>
<div style="margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0">Design Dimension:</h4>
<span class="lightdarkblacktext"><?php echo $data[0]['design_dimension']; ?></span>
</div>
<div style="margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0">What industry is this design for?</h4>
<span class="lightdarkblacktext"><?php echo $data[0]['business_industry']; ?></span>
</div>

<div style="margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0">Description</h4>
<span class="lightdarkblacktext" style="font-size:18px;">
<?php
if (strlen($data[0]['description']) > 200) {
echo '<span class="read_more">' . substr($data[0]['description'], 0, 220) . '...<span class="read_more read_more_btn" id="readmore" style="color:#000;">See More</span></span>';
echo "<span class='more_text read_more' style='display:none;'>" . $data[0]['description'] . "...</span><span class='read_more_btn read_more' style='display:none;color:#000;'>See Less</span>";
} else {
echo "<span class='more_text'>" . $data[0]['description'] . "</span>";
}
?></span>
</div>

</div>

<div class="col-md-7 font16">
<?php /* ?>
<div style="width:45%;display:inline-block; margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0" style="display:inline-block;font-size:16px;">Designer:</h4>&nbsp;&nbsp;&nbsp;
<span class="lightdarkblacktext" style="font-size: 16px !Important;"><?php
if ($data[0]['designer_first_name'] == "") {
echo "NA";
} else {
echo $data[0]['designer_first_name'];
}
?></span>
</div>
<div style="width:52%;display:inline-block; margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0" style="display:inline-block;font-size:16px;margin-left: 7px;">Status:</h4>&nbsp;&nbsp;&nbsp;
<?php
if ($data[0]['status'] == "checkforapprove") {

$status = "Review your design";
$color = "greentext";
} elseif ($data[0]['status'] == "active") {

$status = "Design In Progress";
$color = "blacktext";
} elseif ($data[0]['status'] == "disapprove") {

$status = "Revision In Progress";
$color = "orangetext";
} elseif ($data[0]['status'] == "pending" || $data[0]['status'] == "assign") {

$status = "In Queue";
$color = "greentext";
} else {
$status = "Completed";
$color = "greentext";
}
?>
<span class="<?php echo $color; ?>" style="margin-left: 0px;font-size: 16px;"><?php echo $status; ?>
</span>
</div>
<?php */ ?>

<div style="margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0">Additional info to designer?</h4>
<span class="lightdarkblacktext" style="font-size:18px;">
<div class="rmore-rlessbox">
<?php
if (strlen($data[0]['other_info']) > 200) {
echo '<span class="read_more_additional more_additional">' . substr($data[0]['other_info'], 0, 290) . '...</span><span class="more_additional read_more_additional more_less_Click" id="readmore2" style="color:#000;">See More</span>';
echo "<span class='more_additional' style='display:none;'>" . $data[0]['other_info'] . "...</span><span class='read_more_additional_more more_additional more_less_Click' id='readmore2' style='color:#000;display:none'>See Less</span>";
} else {
echo "<span class='more_additional'>" . $data[0]['other_info'] . "</span>";
}
?>
</div>
</span>
</div>

<div class="attachments_dv">
<h4 class="darkblacktext weight600 ls0">Attachments</h4>
<div class="row">
<div class="col-md-9" style="display:inline-block;<?php //if (($i) % 4 == 0) { echo "clear:both;";}   ?>">
<div style="height:140px; overflow-y:scroll; overflow-x: hidden;">
<div class="row row5 row-no-padding">

<?php for ($i = 0; $i < sizeof($data[0]['customer_attachment']); $i++) { ?>
<div class="col-md-4">
<div class="attachmentsimgbox">
<a href="#" data-url="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-toggle="modal" data-target="#open_attachment_open" class="open_attachment_open">
<img src="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" style="max-height:100px;"/>
</a>
</div>
</div>
<?php } ?>

</div>
</div>
</div><!-- col9 -->

<div class="col-md-3 attach_file_dv" style="display:inline-block; text-align: center;cursor:pointer; height:110px; padding-left: 0px;" data-toggle="modal" data-target="#myModal3">
<div style="border:1px solid #eee; height:110px;padding:0px;">
<i class="fa fa-paperclip" style="padding-top: 30px;text-align:center;font-size:17px;"></i>
<p>Attach File</p>
</div>
</div><!-- col3 -->

</div>
</div>
</div><!-- col 7 -->

<?php /* ?>
<div class="col-md-5 font16" style="padding-top:16px;clear:both;">
<div style="margin-top: 10px;">
<h4 class="darkblacktext weight600 ls0">Description</h4>
<span class="lightdarkblacktext" style="font-size:18px;">
<?php
if (strlen($data[0]['description']) > 200) {
echo '<span class="read_more">' . substr($data[0]['description'], 0, 220) . '...<span class="read_more read_more_btn" id="readmore" style="color:#000;">See More</span></span>';
echo "<span class='more_text read_more' style='display:none;'>" . $data[0]['description'] . "...</span><span class='read_more_btn read_more' style='display:none;color:#000;'>See Less</span>";
} else {
echo "<span class='more_text'>" . $data[0]['description'] . "</span>";
}
?></span>
</div>
</div>
<div class="col-md-7 font16" style="padding-top:26px;">
<h4 class="darkblacktext weight600 ls0">Attachments</h4>
<div>
<div class="row">
<div class="col-md-9" style="display:inline-block;<?php //if (($i) % 4 == 0) { echo "clear:both;";}   ?>">
<div style="height:140px; overflow-y:scroll; overflow-x: hidden;">
<div class="row row5 row-no-padding">

<?php for ($i = 0; $i < sizeof($data[0]['customer_attachment']); $i++) { ?>
<div class="col-md-4">
<div class="attachmentsimgbox">
<a href="#" data-url="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-toggle="modal" data-target="#open_attachment_open" class="open_attachment_open">
<img src="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" style="max-height:100px;"/>
</a>
</div>
</div>
<?php } ?>

</div>
</div>
</div>

</div>
</div>
</div>


<div class="col-sm-12" style="border-top: 1px solid;margin-top: 35px;">
<div class="row">
<p style="margin-top:20px;">
<h4 class="darkblacktext weight600 ls0" style="padding-bottom: 15px;">Style Suggestions</h4>
<?php
if ($data[0]['image'] != "") {
$images = explode(",", $data[0]['image']);
for ($i = 0; $i < sizeof($images); $i++) {
?>

<div class="col-sm-3" style="padding-left: 5px; padding-right: 5px; margin-bottom: 10px;"> <!-- <?php
if (($i) % 4 == 0) {
echo 'margin-top:5px;clear:both;';
}
?> -->
<div class="style-suggestionslbox">
<div>
<image src="<?php echo $images[$i]; ?>" />
</div>
</div>
</div>
<?php
}
}
?>
</p>
<p>
<h4 class="darkblacktext weight600 ls0" style="clear: both;padding-top: 30px;">Additional description for the style?</h4>
<span class="lightdarkblacktext" style="font-size:18px;"><?php echo $data[0]['additional_description']; ?></span>
</p>
<p>
<h4 class="darkblacktext weight600 ls0">Color Preferences</h4>
<?php
if ($data[0]['color'] != "") {
$color = explode(",", $data[0]['color']);
for ($i = 0; $i < sizeof($color); $i++) {
?>
<div class="col-sm-2" style="padding-left:0px;" >
<div class="col-sm-12" style="padding-left:0px;" >
<div style="background:<?php echo $color[$i]; ?>;width:100%;height:50px;"></div>
</div>
<div class="col-sm-12" style="margin-top: 10px;" >
<p class="greytext" style="font-size: 16px;display:inline-block;padding-left: 10px;"><?php echo $color[$i]; ?></p>
</div>
</div>
<?php
}
}
?>
</p>
</div>
</div>
<?php */ ?>


</div>

<!--<div class="row" style="margin:50px 0px;">
<input type="button" class="darkblackbackground whitetext weight600" style="padding:15px 30px;font-size:18px;min-width: 160px;border: unset;border-radius: 6px;" value="EDIT REQUEST" />
<input type="button" class="whitetext weight600" style="padding:15px 30px;font-size:18px;min-width: 160px;background:#e52344;border: unset;border-radius: 6px;margin-left: 20px;" value="SAVE" />
</div>-->


</div><!-- col 12 -->
</div><!-- row -->


</div>
</div>

<!-- Modal Start-->
<div id="myModalSource" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Modal Header</h4>
</div>
<div class="modal-body">
<form method="post" action="" enctype="multipart/form-data">
<label>Source File: </label>
<input type="file" class="form-control dropify waves-effect waves-button" name="src_file" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" data-plugin="dropify" data-height="200" id="file" />

<label>Preview File: </label>
<input type="file" class="form-control dropify waves-effect waves-button" name="preview_file" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" data-plugin="dropify" data-height="200" id="file" />

<input type="submit" value="Submit" class="btn btn-primary" style="border-radius: 7px;margin: auto;display: block;margin-top: 10px;"/>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>
<!-- Modal End-->
</section>
</div>

<div id="myModal3" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Upload File</h4>
</div>
<div class="modal-body">
<form method="post" action="" enctype="multipart/form-data">

<input type="file" class="form-control dropify waves-effect waves-button" name="preview_file" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" data-plugin="dropify" data-height="200" id="file" required/>

<input type="submit" value="Submit" class="btn btn-primary" style="border-radius: 7px;margin: auto;display: block;margin-top: 10px;"/>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>


<div id="open_attachment_open" class="modal fade" role="dialog">
<div class="modal-dialog" style="width:500px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Attachments</h4>
</div>
<div class="modal-body">
<img src="" class="modal_img"  style="max-width: 80%;margin:auto;display:block;"/>
</div>
<div class="modal-footer">
<div class="col-md-12">
<div class="col-md-8">
<button type="button" class="btn btn-default" style="border: unset;border-radius: 6px;font-size: 16px;min-width: 160px;padding: 12px 20px;font-weight: 600;background-color: #1a3147;color: #fff;"><a href="#" style="color:#fff;"class="modal_download" download>Download</a></button>
</div>
<div class="col-md-4" style="padding-right:0px;">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

</div>
</div>
<script>

$(document).ready(function () {
$(".more_less_Click").click(function () {
$(".more_additional").toggle();
});
$(".read_more_btn").click(function () {
$(".read_more").toggle();
});

$('.t_w').keypress(function (e) {
if (e.which == 13) {
$('.s_t').click();
return false;
}
});

$(".open_attachment_open").click(function () {
var url = $(this).attr("data-url");
$(".modal_img").attr("src", url);
$(".modal_download").attr("href", url);
});

});
</script>

<script>
(function ($) {
$(window).on("load", function () {

$(".myscroller").mCustomScrollbar({
theme: "dark-3"
});
$(".myscroller").mCustomScrollbar("scrollTo", "bottom");
$(".myscroller").css("overflow-y", "hidden");

});
})(jQuery);
</script>
<style>
.chatbox .description{ width:180px; }
</style>
<?php
//echo "<pre>"; print_r($data[0]); echo "</pre>";
//echo "<pre>"; print_r($chat_request); echo "</pre>";
$prjtid = $chat_request[0]['request_id'];
$customerid = $chat_request[0]['reciever_id']; 
$designerid = $chat_request[0]['sender_id'];
//$customerseen = "0";

// echo($prjtid);
// echo($customerid);
// echo($designerid);
?>
<script type="text/javascript">
jQuery(document).ready(function ($){
setInterval(function(){
// e.preventDefault();
var project_id = "<?php echo $prjtid; ?>";
var cst_id = "<?php echo $customerid; ?>";
var desg_id = "<?php echo $designerid; ?>";
var customerseen = "0";

$.ajax({
type: "POST",
url:"<?php echo base_url(); ?>designer/Request/load_messages_from_customer",
data:{
'project_id': project_id,
'cst_id': cst_id,
'desg_id': desg_id,  
'customerseen': "0"
},
success: function (response) {
if (response) {
console.log(response)
jQuery('.chat_frame .mCSB_container').append(response);
jQuery( ".myscroller" ).mCustomScrollbar( "scrollTo", "bottom" );
}
else{

}
//console.log(data);
},
error: function(error){
console.log("error");
}
});
}, 2000);
});
</script>
