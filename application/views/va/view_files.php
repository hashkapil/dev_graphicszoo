<h2 class="float-xs-left content-title-main" style="display:inline-block;">Projects</h2>
<a href="<?php echo base_url(); ?>va/dashboard/view_clients" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Clients</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_designer" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Designers</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_qa" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">QA</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.darkblackbackground { background-color:#1a3147; } 
.lightdarkblacktext { color:#b4b9be; }
.lightdarkblackbackground { background-color:#f4f4f4; }
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 31px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link{
	background:#ededed;
	font-weight: 600;
	color: #2f4458;
}
.ls0{ letter-spacing: 0px; }


.slick-slide.slick-current.slick-center .portfolio-items-wrapper:before {
	content: "";
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: none;
}

.slick-next {
    left: 30%;
    z-index: 1000;
	height: auto;
	width: auto;
	top:-20px;
}

.slick-prev {
    left: 20%;
    z-index: 1000;
	height: auto;
	width: auto;
	top:-20px;
}

.slick-next::before {
    font-family: FontAwesome;
	content: "\f105";
	font-size: 40px;
	background: #FFFFFF;
	opacity: 1;
	padding: 2px 15px;
	border-radius: 30px;
	color: #EC3D56;
}

.slick-prev::before {
    font-family: FontAwesome;
	content: "\f104";
	font-size: 40px;
	background: #FFFFFF;
	opacity: 1;
	padding: 2px 15px;
	border-radius: 30px;
	color: #EC3D56;
}
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">                
                <div class="nav-tab-pills-image">
                    <ul class="nav nav-tabs" role="tablist" style="border-bottom:unset !important;">                      
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url(); ?>va/dashboard/view_project/"<?php echo $_GET['id']; ?>"" role="tab">
                                Back to Files
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        
						<div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                            <div class="row" style="">
							 
							<div class="portfolio-items center">
							<?php for($i=0;$i<sizeof($designer_file);$i++){ 
											$created_date_time = strtotime($designer_file[$i]['created']);
														
											$now_date_time = strtotime(date("Y-m-d H:i:s"));
											$span = "";
											if(($now_date_time - $created_date_time) < 5000){
												$span = '<span style="background:#ec445c;padding:5px;color:#fff;font-weight:600;position: absolute;top: 0px;">NEW</span>';
											}
											?>
								<div class="col-md-12" style="">
                                <div class="col-md-9"  style="border-right:1px solid #1a3147;margin-top:15px;">
									<div class="col-md-12" style="">
									
										<div class="col-md-6" style="padding:0px;">
											<h4 class="darkblacktext weight600 ls0"><?php echo $designer_file[$i]['src_file']; ?></h4>
											<p class="lightdarkblacktext" style="font-size:16px;margin-top:10px;"><?php echo number_format(filesize("./uploads/requests/".$_GET['id']."/".$designer_file[$i]['src_file']) / 1048576, 2)." Mb"; ?> | 
											<?php 
											
											$date=strtotime($designer_file[$i]['created']);//Converted to a PHP date (a second count)
											//Calculate difference
											$diff=strtotime(date("Y:m:d H:i:s"))-$date;//time returns current time in seconds
											$minutes = floor($diff/60);
											$hours = floor($diff/3600);
											if($hours == 0){
												echo $minutes. " Minutes Ago";
											}else{
												$days = floor($diff / (60 * 60 * 24));
												if($days == 0){
													echo $hours. " Hours Ago";
												}else{
													echo $days. " Days Ago";
												}
											}
											?>
											</p>
										</div>
										<div class="col-md-6" style="padding:0px;text-align: right;">
											<a href="<?php echo base_url()."uploads/requests/".$_GET['id']."/".$designer_file[$i]['src_file']; ?>" download><p class="darkblacktext weight600" style="font-size:16px;margin-top:10px;"><i class="fa fa-download" style="padding-right: 10px;"></i>  Download</p></a>
											 <p class="weight600 pinktext" style="font-size:18px;">Rating 
												<i class="fa fa-star pl5"></i>
												<i class="fa fa-star pl5"></i>
												<i class="fa fa-star pl5"></i>
												<i class="fa fa-star pl5"></i>
												<i class="fa fa-star pl5 greytext"></i>
											</p>
										</div>
									</div>
                                    <div class=""  style="width:100%;height:400px;clear:both;">
										<image src="<?php echo base_url()."uploads/requests/".$_GET['id']."/".$designer_file[$i]['file_name']; ?>" style="max-height:100%;width:100%;" />
									</div>
									
                                </div>
								<div class="col-md-3" style="padding: 0px;">
									
									<div class="col-md-12" style="padding-bottom:30px;margin-top:20px;">
										
											<h4 class="darkblacktext weight600 ls0">Messages</h4>
											<p class="orangetext weight600 font18"><i class="fa fa-envelope" style="margin-right:10px;"></i>Notes for New.png</p>
										
										<div class="col-md-12 messagediv_<?php echo $designer_file[$i]['id']; ?>" style="padding:0px;overflow-y: scroll;max-height: 400px;">
										<?php for($j=0;$j<sizeof($designer_file[$i]['chat']);$j++){ ?>
											<?php if($designer_file[$i]['chat'][$j]['sender_role'] == "customer"){ ?>
											<div class="col-md-12 greytext" style="border:1px solid;border-radius:8px;">
												<p class="darkblacktext weight600" style="font-size: 18px;padding-bottom: 0px;padding-top: 10px;"><?php echo $designer_file[$i]['chat'][$j]['customer_name']; ?></p>
												
												<p class="greytext" style="line-height: 16px;"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p>
												
												<p class="darkblacktext weight600">
												<?php 											
												$date=strtotime($designer_file[$i]['chat'][$j]['created']);//Converted to a PHP date (a second count)
												//Calculate difference
												$diff=strtotime(date("Y:m:d H:i:s"))-$date;//time returns current time in seconds
												$minutes = floor($diff/60);
												$hours = floor($diff/3600);
												if($hours == 0){
													echo $minutes. " Minutes Ago";
												}else{
													$days = floor($diff / (60 * 60 * 24));
													if($days == 0){
														echo $hours. " Hours Ago";
													}else{
														echo $days. " Days Ago";
													}
												}
												?>
												</p>
											</div>
											<?php }else{ ?>
											<div class="col-md-12 greytext" style="">
												<p class="pinktext weight600" style="text-align:right;font-size: 18px;padding-bottom: 0px;padding-top: 10px;"><?php echo $designer_file[$i]['chat'][$j]['sender_role']; ?></p>
												<p class="greytext" style="line-height: 16px;text-align:right;"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p>
												<p class="pinktext weight600" style="text-align:right;">
												<?php 											
												$date=strtotime($designer_file[$i]['chat'][$j]['created']);//Converted to a PHP date (a second count)
												//Calculate difference
												$diff=strtotime(date("Y:m:d H:i:s"))-$date;//time returns current time in seconds
												$minutes = floor($diff/60);
												$hours = floor($diff/3600);
												if($hours == 0){
													echo $minutes. " Minutes Ago";
												}else{
													$days = floor($diff / (60 * 60 * 24));
													if($days == 0){
														echo $hours. " Hours Ago";
													}else{
														echo $days. " Days Ago";
													}
												}
												?>
												</p>
											</div>
											<?php } ?>
										<?php } ?>
										</div>
										<input type='text' class='form-control text_<?php echo $designer_file[$i]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;" placeholder="Reply To Client" />
										<span style="float: right;margin-right: 6px;margin-top: -20px;position: relative;z-index: 2;color: red;" class="pinktext fa fa-send send_request_img_chat" 
										data-fileid="<?php echo $designer_file[$i]['id']; ?>" 
										data-senderrole="VA" 
										data-senderid="<?php echo $_SESSION['user_id']; ?>" 
										data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
										data-receiverrole="customer"></span>
										
									</div>
									
								</div>
								
								</div>
							<?php  } ?>
                            </div>
							</div>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- approve Modal -->
<div id="approvemodal" class="modal fade" role="dialog" style="margin-top:10%;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body " style="background-color:#6ad154;text-align: center;font-size: 170px;">
			<i class="fa fa-check whitetext"></i>
			<h3 style="letter-spacing: -1px;font-size: 21px;" class="whitetext weight600">DESIGN APPROVED!</h3>
      </div>
      <div class="modal-footer" style="text-align:center;">
			<h4 class="pinktext weight600" style="font-size:16px;letter-spacing: -1px;">RATE YOUR DESIGN</h4>
			<p style="font-size:35px;">
				<i class="fa fa-star pl5 greytext"></i>
				<i class="fa fa-star pl5 greytext"></i>
				<i class="fa fa-star pl5 greytext"></i>
				<i class="fa fa-star pl5 greytext"></i>
				<i class="fa fa-star pl5 greytext"></i>
			</p>
			<p style="padding-left:10%;padding-right:10%;margin-top:35px;">
				Click "Download' button to access source files and hi-resolution version of your design.
			</p>
			<input type="button" class="btn whitetext weight600" style="background:#6ad154;padding:15px;border-radius: 5px;" value="DOWNLOAD SOURCE FILES" />
			<p style="color:#6ad154;cursor:pointer;"  data-dismiss="modal">Thanks, I'll download this later.</p>
			<p style="margin-top:25px;">Please be notified that all source files will be removed after 14 days upon approval</p>
        
      </div>
    </div>

  </div>
</div>
</section>
</div>