</div>
</div>
</div>
</section>
</div>
<!-- START CORE JAVASCRIPT -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/dropify/dist/js/dropify.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/tether/dist/js/tether.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/switchery/dist/switchery.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/screenfull.js/dist/screenfull.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/classie/classie.js"></script>
<!-- END CORE JAVASCRIPT -->
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/jquery-form-validator/form-validator/jquery.form-validator.min.js"></script>


<!-- START GLOBAL JAVASCRIPT -->
<script src="<?php echo base_url(); ?>theme/assets/global/js/site.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/js/site-settings.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/js/global/form_validation.min.js"></script>

<?php // $this->Html->script('/assets/pages/global/js/global.min.js'); ?>
<!-- END GLOBAL JAVASCRIPT -->

<script src="<?php echo base_url(); ?>theme/assets/input-masking/js/masking-input.js"></script>

<script src="<?php echo base_url(); ?>theme/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/datatables-responsive/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/datatables-responsive/js/responsive.bootstrap4.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/datatables-scroller/js/dataTables.scroller.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/editable-table/mindmup-editabletable.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/editable-table/numeric-input-example.js"></script>

<script src="<?php echo base_url(); ?>theme/assets/global/js/global/datatable.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/slick.min.js"></script>

<!--<script src="socket.io.js"></script>-->

<!-- START PAGE JAVASCRIPT -->
<?php // $this->Html->script('/assets/global/js/global/dashboard_v4.js');  ?>
<!-- END PAGE JAVASCRIPT -->

<!-- START THEME LAYOUT JAVASCRIPT -->
<script src="<?php echo base_url(); ?>theme/assets/layouts/layout-left-menu/js/layout.min.js"></script>
<!-- END THEME LAYOUT JAVASCRIPT -->

<script src="<?php echo base_url(); ?>public/js/custom_js.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-103722162-1', 'auto');
    ga('send', 'pageview');

</script>

<!-- Start of Async Drift Code -->
<script>
    !function () {
        var t;
        if (t = window.driftt = window.drift = window.driftt || [], !t.init)
            return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
                    t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
                    t.factory = function (e) {
                        return function () {
                            var n;
                            return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                        };
                    }, t.methods.forEach(function (e) {
                t[e] = t.factory(e);
            }), t.load = function (t) {
                var e, n, o, i;
                e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
                        o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
                        n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
            });
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('d6yi38hkhwsd');
</script>
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction(id) {
	$('.dropdown-content').removeClass('show');
    //document.getElementById(id).classList.toggle("show");
	$("#"+id).addClass("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

$(".directmessage").click(function(){
	var clientid = $(this).attr('data-clientid');
	var clientname = $(this).attr('data-clientname');
	var imageurl = $(this).attr('data-imageurl');
	
	$(".image_name").html('<image src="'+imageurl+'" style="width:50px;margin-right:10px;margin-left: -15px;" />'+clientname);
	$('#directmessage').modal("show");
});

$(".gradeclick").click(function(){
	var current = $(this).html();
	for(var i=1;i<=10;i++){
		if(current < i){
			$('.currentgrade_'+i).css("background","#969191");
		}else{
			$('.currentgrade_'+i).css("background","#e52344");
			$('.currentgrade_main').html(i+"/10");
		}
	}
	$('.currentgrade_main').css("background","#e52344");
});
$('.portfolio-items').slick({
		  slidesToShow: 1,
		  responsive: [
			{
			  breakpoint: 768,
			  settings: {
				arrows: false,
				slidesToShow: 1
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				arrows: false,
				slidesToShow: 1
			  }
			}
		  ]
		});
		
$('.send_request_img_chat').click(function(){
	var request_file_id = $(this).attr("data-fileid");
	var sender_role = $(this).attr("data-senderrole");
	var sender_id = $(this).attr("data-senderid");
	var receiver_role = $(this).attr("data-receiverrole");
	var receiver_id = $(this).attr("data-receiverid");
	var text_id = 'text_'+request_file_id;
	var message = $('.slick-active .'+text_id).val();
	/*var data = { "request_file_id": request_file_id, 
				"sender_role": sender_role,
				"sender_id": sender_id,
				"receiver_role": receiver_role,
				"receiver_id": receiver_id,
				"message":message};*/
	if(message != ""){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>qa/dashboard/send_message",
		data: { "request_file_id": request_file_id, 
				"sender_role": sender_role,
				"sender_id": sender_id,
				"receiver_role": receiver_role,
				"receiver_id": receiver_id,
				"message":message},
		success:  function(data){
			//alert("---"+data);
			//alert("Settings has been updated successfully.");
			$('.text_'+request_file_id).val("");
			$('.messagediv_'+request_file_id).append('<div class="col-md-12 greytext" style="">\
									<p class="darkblacktext weight600" style="text-align:right;font-size: 18px;padding-bottom: 0px;padding-top: 10px;">VA</p>\
									<p class="greytext" style="line-height: 16px;text-align:right;">'+message+'</p>\
									<p class="darkblacktext weight600" style="text-align:right;">Few Seconds ago</p>\
								</div>');
								
			$("messagediv_"+request_file_id).animate({ scrollTop: 9999 }, 'slow');
		}
	});
	}
});

$('.send_request_chat').click(function(){
	var request_id = $(this).attr("data-requestid");
	var sender_type = $(this).attr("data-senderrole");
	var sender_id = $(this).attr("data-senderid");
	var reciever_id = $(this).attr("data-receiverid");
	var reciever_type = $(this).attr("data-receiverrole");
	var text_id = 'text_'+request_id;
	var message = $('.'+text_id).val();
	var verified_by_admin="1";
	var customer_name = $(this).attr("data-sendername");
	/*var data = { "request_file_id": request_file_id, 
				"sender_role": sender_role,
				"sender_id": sender_id,
				"receiver_role": receiver_role,
				"receiver_id": receiver_id,
				"message":message};*/
	if(message != ""){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>qa/dashboard/send_message_request",
		data: { "request_id": request_id, 
				"sender_type": sender_type,
				"sender_id": sender_id,
				"reciever_id": reciever_id,
				"reciever_type": reciever_type,
				"message":message},
		success:  function(data){
			//alert("---"+data);
			//alert("Settings has been updated successfully.");
			$('.text_'+request_id).val("");
			$('.messagediv_'+request_id).append('<div class="col-md-12 greytext" style="">\
									<p class="darkblacktext weight600" style="text-align:right;font-size: 18px;padding-bottom: 0px;padding-top: 10px;">'+customer_name+'</p>\
									<p class="greytext" style="line-height: 16px;text-align:right;">'+message+'</p>\
									<p class="darkblacktext weight600" style="text-align:right;">Few Seconds ago</p>\
								</div>');
								
			$("messagediv_"+request_id).animate({ scrollTop: 9999 }, 'slow');
		}
	});
	}
});

$('.send_room_chat').click(function(){
	if($('.room_chat_text').val() != ""){
		var room_no = $(this).attr("data-room");
		var msg = $('.room_chat_text').val();
		var sender_id = "<?php echo $_SESSION['user_id']; ?>";
		$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>va/dashboard/send_room_message",
		data: { "room_id": room_no, 
				"message": msg,
				"sender_id": sender_id},
		success:  function(data){
			//alert("---"+data);
			//alert("Settings has been updated successfully.");
			$('.text_'+room_no).val("");
			$('.messagediv_'+room_no).append('<div class="col-md-2"></div>\
												<div class="col-md-10">\
													<div class="col-md-12 greytext" style="margin-top: 20px;background:aliceblue;border-radius:8px;margin-bottom: 10px;width: 60%;float: right;">\
														<p class="darkblacktext" style="line-height: 16px;text-align:right;padding-top: 10px;">'+msg+'</p>\
														<p class="darkblacktext weight600" style="text-align:right;">Few Minutes Ago..</p>\
													</div>\
												</div>');
								
			$("messagediv_"+room_no).animate({ scrollTop: 9999 }, 'slow');
		}
	});
	}
});
</script>
</body>
</html>