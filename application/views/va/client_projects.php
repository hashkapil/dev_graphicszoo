<a href="<?php echo base_url(); ?>va/dashboard" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Projects</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_clients" class="float-xs-left content-title-main" style="padding-left: 10%;">Clients</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_designer" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Designers</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_qa" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">QA</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.pinktext { color: #ff0024;; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 31px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight:600;
	padding: 7px 25px;
}
.background_1st { background-color:#98d575; }
.background_2nd { background-color:#f7941f; }
.background_3rd { background-color:#409ae8; }
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">         
<?php 
					function ordinal($number) {
						$ends = array('th','st','nd','rd','th','th','th','th','th','th');
						if ((($number % 100) >= 11) && (($number%100) <= 13))
							return $number. 'th';
						else
							return $number. $ends[$number % 10];
					}
					?>
				<div class="col-sm-12" style="margin:40px 0px;">
					<div class="col-sm-1" style="padding:0px;">
						<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;" />
					</div>
					<div class="col-sm-9">
						<h5 class="weight600 darkblacktext" style="clear: both;"><?php echo $userdata[0]['first_name']." ".$userdata[0]['last_name']?></h5>
						<p class="greytext font16 pb0">Member since <?php echo date("M d, Y",strtotime($userdata[0]['created'])); ?></p>
					</div>
				</div>
                <div class="nav-tab-pills-image">
					<div class="col-sm-9">
						<ul class="nav nav-tabs" role="tablist" style="border-bottom: unset;padding:0px;">                      
							<li class="nav-item active" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#incoming_designs_tab" role="tab">
									Incoming Requests(<?php echo sizeof($incoming_request); ?>)
								</a>
							</li>
							<li class="nav-item" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#ongoing_designs_tab" role="tab">
									Ongoing Requests(<?php echo sizeof($ongoing_request); ?>)
								</a>
							</li>
							 <li class="nav-item"style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#pending_designs_tab" role="tab">
									Pending Requests(<?php echo sizeof($pending_request); ?>)
								</a>
							</li>
							<li class="nav-item"style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#approved_designs_tab" role="tab">
									Approved Requests(<?php echo sizeof($approved_request); ?>)
								</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-3">
						<input type="button" class="btn weight600" style="border-radius:5px;background:#d4d4d4;" value="Filter" />
						<p class="weight600 darkblacktext font18" style="display:inline-block;float: right;">
							<i class="darkblacktext fa fa-search"></i> Search
						</p>
					</div>
					
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="incoming_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                     <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-Progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Star</th>
												<th>Icon</th>
												<th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php for($i=0;$i<sizeof($incoming_request);$i++){ ?>
										   <tr class="trborder">
												<td class="<?php if($i+1 > 3){ echo "greybackground"; } else{ echo "background_".ordinal($i+1); } ?> whitetext numbercss"><?php echo ordinal($i+1); ?></td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5"><a style="text-decoration:unset;" href="<?php echo base_url()."qa/dashboard/view_project/".$incoming_request[$i]['id']; ?>"><?php echo $incoming_request[$i]['title']; ?></a></p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20"><?php echo $incoming_request[$i]['description']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Designer</p>
													<p class="darkblacktext weight600"><?php if($incoming_request[$i]['designer_first_name']){ echo $incoming_request[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Requested</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($incoming_request[$i]['created'])); ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">In-Progress</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($incoming_request[$i]['dateinprogress'])); ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Due Date</p>
													<?php 
													$duedate = "";
													
													if($incoming_request[$i]['plan_turn_around_days']){
													if($incoming_request[$i]['status_admin'] == "disapprove"){
														$incoming_request[$i]['dateinprogress'] = date("Y-m-d H:i:s",strtotime($incoming_request[$i]['dateinprogress']." +1 day"));
													}
														$date = date("Y-m-d",strtotime($incoming_request[$i]['dateinprogress']));
														$time = date("h:i:s",strtotime($incoming_request[$i]['dateinprogress']));
														$duedate = date("m/d/Y g:i a",strtotime($date." ".$incoming_request[$i]['plan_turn_around_days']." weekdays ".$time));
													} ?>
													<p class="darkblacktext weight600"><?php echo $duedate; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<p class="darkblacktext weight600"><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i></p>
												</td>
												<td>
													<p class="darkblacktext weight600">
														<i class="fa fa-arrow-up"></i>
														<i class="fa fa-arrow-down"></i>
													</p>
												</td>
												<td>
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr> 
										<?php } ?>
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="ongoing_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                               <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-Progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Star</th>
												<th>Icon</th>
												<th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php for($i=0;$i<sizeof($ongoing_request);$i++){ ?>
										   <tr class="trborder">
												<td class="<?php if($i+1 > 3){ echo "greybackground"; } else{ echo "background_".ordinal($i+1); } ?> whitetext numbercss"><?php echo ordinal($i+1); ?></td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5"><a style="text-decoration:unset;" href="<?php echo base_url()."qa/dashboard/view_project/".$ongoing_request[$i]['id']; ?>"><?php echo $ongoing_request[$i]['title']; ?></a></p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20"><?php echo $ongoing_request[$i]['description']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Designer</p>
													<p class="darkblacktext weight600"><?php if($ongoing_request[$i]['designer_first_name']){ echo $ongoing_request[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Requested</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($ongoing_request[$i]['created'])); ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">In-Progress</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($ongoing_request[$i]['dateinprogress'])); ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Due Date</p>
													<?php 
													$duedate = "";
													
													if($ongoing_request[$i]['plan_turn_around_days']){
													if($ongoing_request[$i]['status_admin'] == "disapprove"){
														$ongoing_request[$i]['dateinprogress'] = date("Y-m-d H:i:s",strtotime($ongoing_request[$i]['dateinprogress']." +1 day"));
													}
														$date = date("Y-m-d",strtotime($ongoing_request[$i]['dateinprogress']));
														$time = date("h:i:s",strtotime($ongoing_request[$i]['dateinprogress']));
														$duedate = date("m/d/Y g:i a",strtotime($date." ".$ongoing_request[$i]['plan_turn_around_days']." weekdays ".$time));
													} ?>
													<p class="darkblacktext weight600"><?php echo $duedate; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<p class="darkblacktext weight600"><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i></p>
												</td>
												<td>
													<p class="darkblacktext weight600">
														<i class="fa fa-arrow-up"></i>
														<i class="fa fa-arrow-down"></i>
													</p>
												</td>
												<td>
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr> 
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="pending_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">						
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-Progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Star</th>
												<th>Icon</th>
												<th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php for($i=0;$i<sizeof($pending_request);$i++){ ?>
										   <tr class="trborder">
												<td class="<?php if($i+1 > 3){ echo "greybackground"; } else{ echo "background_".ordinal($i+1); } ?> whitetext numbercss"><?php echo ordinal($i+1); ?></td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5"><a style="text-decoration:unset;" href="<?php echo base_url()."qa/dashboard/view_project/".$pending_request[$i]['id']; ?>"><?php echo $pending_request[$i]['title']; ?></a></p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20"><?php echo $pending_request[$i]['description']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Designer</p>
													<p class="darkblacktext weight600"><?php if($pending_request[$i]['designer_first_name']){ echo $pending_request[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Requested</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($pending_request[$i]['created'])); ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">In-Progress</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($pending_request[$i]['dateinprogress'])); ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Due Date</p>
													<?php 
													$duedate = "";
													
													if($pending_request[$i]['plan_turn_around_days']){
													if($pending_request[$i]['status_admin'] == "disapprove"){
														$pending_request[$i]['dateinprogress'] = date("Y-m-d H:i:s",strtotime($pending_request[$i]['dateinprogress']." +1 day"));
													}
														$date = date("Y-m-d",strtotime($pending_request[$i]['dateinprogress']));
														$time = date("h:i:s",strtotime($pending_request[$i]['dateinprogress']));
														$duedate = date("m/d/Y g:i a",strtotime($date." ".$pending_request[$i]['plan_turn_around_days']." weekdays ".$time));
													} ?>
													<p class="darkblacktext weight600"><?php echo $duedate; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<p class="darkblacktext weight600"><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i></p>
												</td>
												<td>
													<p class="darkblacktext weight600">
														<i class="fa fa-arrow-up"></i>
														<i class="fa fa-arrow-down"></i>
													</p>
												</td>
												<td>
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr> 
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						

                        <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-Progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Star</th>
												<th>Icon</th>
												<th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php for($i=0;$i<sizeof($approved_request);$i++){ ?>
										   <tr class="trborder">
												<td class="<?php if($i+1 > 3){ echo "greybackground"; } else{ echo "background_".ordinal($i+1); } ?> whitetext numbercss"><?php echo ordinal($i+1); ?></td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5"><a style="text-decoration:unset;" href="<?php echo base_url()."qa/dashboard/view_project/".$approved_request[$i]['id']; ?>"><?php echo $approved_request[$i]['title']; ?></a></p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20"><?php echo $approved_request[$i]['description']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Designer</p>
													<p class="darkblacktext weight600"><?php if($approved_request[$i]['designer_first_name']){ echo $approved_request[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Requested</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($approved_request[$i]['created'])); ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">In-Progress</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($approved_request[$i]['dateinprogress'])); ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Due Date</p>
													<?php 
													$duedate = "";
													
													if($approved_request[$i]['plan_turn_around_days']){
													if($approved_request[$i]['status_admin'] == "disapprove"){
														$approved_request[$i]['dateinprogress'] = date("Y-m-d H:i:s",strtotime($approved_request[$i]['dateinprogress']." +1 day"));
													}
														$date = date("Y-m-d",strtotime($approved_request[$i]['dateinprogress']));
														$time = date("h:i:s",strtotime($approved_request[$i]['dateinprogress']));
														$duedate = date("m/d/Y g:i a",strtotime($date." ".$approved_request[$i]['plan_turn_around_days']." weekdays ".$time));
													} ?>
													<p class="darkblacktext weight600"><?php echo $duedate; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<p class="darkblacktext weight600"><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i></p>
												</td>
												<td>
													<p class="darkblacktext weight600">
														<i class="fa fa-arrow-up"></i>
														<i class="fa fa-arrow-down"></i>
													</p>
												</td>
												<td>
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr> 
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>