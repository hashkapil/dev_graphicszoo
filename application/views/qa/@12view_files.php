        <style type="text/css">
        .headnris{
            border: none !important;
            margin-bottom:0px;
        }
        .slick_thumbs{
            position:absolute;
        }
        .slick-dots{
            display: none !important;
        }
        /*.list-comment-es > li:last-child {
            margin-bottom: 75px;
        }*/
        .list-comment-es > li:last-child .distxt-cmm {
            margin-bottom: 75px;
        }
        .abcd img {
            /*width: 50% !important;*/
            margin: 0 auto;
        }       header.nav-section {
            display: none;
        }
        .pstcmm{
            padding: 0px 5px;
        }
        .comment-box-es {
            padding: 15px 25px;
        }
        .comment-box-es a:hover {
            text-decoration: none;
        }
        .posone2 {
            top: -30px;
            z-index: 999999999999;
        }
        .posone1 {
            position: absolute;
            left: 0px;
            top: -20px;
        }
        .posone3abs{
            left: -45px;
        }
        .posoneto{
            top: -15px;
            left: -22px;
        }
        .slick-dotted.slick-slider {
            margin-bottom: 0px;
        }
       /* .list-comment-es {
            max-height: calc(100vh - 110px) !important;
        }*/
        
       /* #comment_list li:last-child .distxt-cmm{
            padding-bottom: 120px;
        }*/
        .rejected{
            font: normal 14px/20px 'GothamPro-Medium', sans-serif;
            color: #e52344;
            text-transform: uppercase;
            padding: 10px 0px;
        }
        .notappchanges {
            font: normal 14px/20px 'GothamPro-Medium', sans-serif;
            color: #37c473;
            text-transform: uppercase;
            padding: 10px 0px;
        }
        .slid {
            box-shadow: 0 0 50px rgba(53,77,103,.3);
        }
        .btn-x .redbtn:focus{
            color: #fff;
        }
        .notapproved{
            display: inline-flex;
        }
        .notapproved input {
            margin-right: 5px;
        }
        .both{
            display: inline-flex;
            width: 100%;
        }
        .both button  {
            margin-top: 6px;
            
        }
        .both button i {
            color: #37c473;
        }
        .distxt-cmm {
            word-wrap: break-word;
        }
        .right_msgBox {
            left: -254px;
        }
        .right_msgBox::after{
            left: 254px !important;
            top: -20px !important;
            transform: rotate(180deg) !important;
        }
        .right_msgBox1 {
            left: -251px;
        }
        .right_msgBox1::after {
            left: 251px !important;
        }
       .slid{
            position: relative;
            overflow: hidden;
        }
        .carousel-fade .carousel-inner .item {
          opacity: 0;
          -webkit-transition-property: opacity;
          -moz-transition-property: opacity;
          -o-transition-property: opacity;
          transition-property: opacity;
        }
        .carousel-fade .carousel-inner .active {
          opacity: 1;
        }
        .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
          left: 0;
          opacity: 0;
          z-index: 1;
        }
        .carousel-fade .carousel-inner .next.left,
        .carousel-fade .carousel-inner .prev.right {
          opacity: 1;
        }
        .carousel-fade .carousel-control {
          z-index: 2;
        }
        .fade-carousel {
            position: relative;
            height: 100vh;
        }
        .fade-carousel .carousel-inner .item {
            height: 100vh;
        }
        .fade-carousel .carousel-indicators > li {
            margin: 0 2px;
            background-color: #f39c12;
            border-color: #f39c12;
            opacity: .7;
        }
        .fade-carousel .carousel-indicators > li.active {
          width: 10px;
          height: 10px;
          opacity: 1;
        }
        div#myCarousel {
            margin-top: 3rem;
        }
        /*.pro-ject-top-cat{
            z-index: 99
        }*/
        .fade-carousel .carousel-inner .item {
            -webkit-transition: 0.6s ease-in-out left;
            -moz-transition: 0.6s ease-in-out left;
            -o-transition: 0.6s ease-in-out left;
            transition: 0.6s ease-in-out left;
        }
        .topposition {
            position: relative;
            margin-top: 30px;
        }
        .topposition::after {
            transform: rotate(180deg);
            top: -20px;
        }
        .dateApp{
            font: normal 13px/25px 'GothamPro', sans-serif;
            color: #596375;
            padding: 5px 0px;
        }
        .mydivposition{
            padding: 0px;
        }
        .slides.active {
            padding: 0px 10px;
        }
        .overlay_comment1{
            display: none;
            position: absolute;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0);
            z-index: 999;
        }
        .overlay_comment{
            position: absolute;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0);
            z-index: 999;
        }
        a.btn_reset:hover{
            text-decoration: none;
        }
        a.btn_reset {
            display: none;
            background: #43c473;
            color: #fff;
            padding: 6px 10px;
            border-radius: 5px;
        }
    </style>
<div class="pro-ject-wrapper">
    <div class="pro-ject-top-cat">
        <div class="de-signdraft">
            <div class="row flex-show flex-show-em1">
                <div class="col-xs-3 fulmoble">
                    <p class="savecols-col left">
                        <a  href="javascript: history.go(-1)" class="back-link-xx0 text-uppercase">Back</a>
                    </p>
                </div>
                        
                <div class="col-xs-4 fulmoble">

                    <div class="descol text-center">
                        <div class="despeginate-box">
                            <a class="despage prev" href="#myCarousel" data-slide="prev"></a>
                            <span class="pagin-one">1 to <?php echo sizeof($designer_file); ?></span>
                            <a class="despage next" href="#myCarousel" data-slide="next"></a>
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-5 fulmoble">
                    <div class="descol">
                        <div class="search-sec-uu8">
                            <a class="btn_reset" href="javascript:void(0)">Enable commenting</a>
                            <a id="zoomOut" href="javascript:void(0)"><img src="<?php echo base_url();?>theme/designer_css/images/icon-zoom-out.png" class="img-responsive"></a>
                            <a id="zoomIn" href="javascript:void(0)"><img src="<?php echo base_url();?>theme/designer_css/images/icon-zoom-in.png" class="img-responsive"></a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>      
    </div>
   <div class="row">
       <div class="col-md-12">
            <div id="myCarousel" class="carousel fade-carousel carousel-fade slide" data-ride="carousel" data-interval="false">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" id="cro" style="position: absolute;z-index: 99;overflow: unset;">
                    <?php  for ($i = 0; $i < sizeof($designer_file); $i++){ ?>
                    <div class="item slides" id="<?php echo $designer_file[$i]['id'];?>">
                        <div class="col-md-12 mydivposition mydivposition_<?php echo $designer_file[$i]['id']; ?>" style="margin-top:20px" onclick="printMousePos(event,<?php echo $designer_file[$i]['id']; ?>);" id="mydivposition_<?php echo $designer_file[$i]['id']; ?>" onmouseout="hideclicktocomment();" onmouseover="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" onmousemove="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" >
                            <div class="col-md-12 slid" style="padding:10px;" data-id="<?php echo $designer_file[$i]['id']; ?>">
                                <?php
                                 $type=substr($designer_file[$i]['file_name'],strrpos($designer_file[$i]['file_name'],'.')+1);
                                if($type == "zip"){ ?>
                                    <img class="img-responsive" src="<?php echo base_url()?>public/default-img/zip.png" style="max-height:100%;width:100%;margin: auto;display: block;"/>
                                <?php }elseif($type == "rar"){ ?>
                                    <img class="img-responsive" src="<?php echo base_url()?>public/default-img/rar.jpg" style="max-height:100%;width:100%;margin: auto;display: block;"/>
                                <?php }elseif($type == "psd"){ ?>
                                    <img class="img-responsive" src="<?php echo base_url()?>public/default-img/psd.jpg" style="max-height:100%;width:100%;margin: auto;display: block;"/>
                                <?php }elseif ($type == "ai") { ?>
                                    <img class="img-responsive" src="<?php echo base_url()?>public/default-img/ai.jpg" style="max-height:100%;width:100%;margin: auto;display: block;"/>
                                <?php }elseif ($type == "pdf") { ?>
                                    <img class="img-responsive" src="<?php echo base_url()?>public/default-img/pdf.png" style="max-height:100%;width:100%;margin: auto;display: block;"/>
                                <?php }else{ ?>
                                    <img class="img-responsive" src="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="max-height:100%;width:100%;margin: auto;display: block;"/>
                                <?php }
                                ?>
                            </div>
                            <!-- Chat toolkit start-->
                            <div class="appenddot_<?php echo $designer_file[$i]['id']; ?>">
                                <?php
                                for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                                    if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                        $background = "pinkbackground";
                                    } else {
                                        $background = "bluebackground";
                                    }
                                    ?>
                                    <?php
                                    if ($designer_file[$i]['chat'][$j]['xco'] != "") {
                                        if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                            $image1 = "dot_image1.png";
                                            $image2 = "dot_image2.png";
                                        } else {
                                            $image1 = "designer_image1.png";
                                            $image2 = "designer_image2.png";
                                        }
                                        ?>
                                        <?php if(is_numeric($designer_file[$i]['chat'][$j]['xco'])){ ?>
                                        <div id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" class="<?php //echo $background;    ?>" style="position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco'] -10 ; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco'] -10 ; ?>px;width:25px;z-index:0;height:25px;" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event);'>
                                            <img src="<?php echo base_url() . "public/" . $image1; ?>" class="customer_chatimage1 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="filter: drop-shadow(0px 1px 1px #ccc);"/>
                                            <img style="display:none;" src="<?php echo base_url() . "public/" . $image2; ?>" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="filter: drop-shadow(0px 1px 1px #ccc);"/>
                                        </div>
                                            <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none;position:absolute;left:<?php  echo $designer_file[$i]['chat'][$j]['xco'] - 3; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco'] + 25; ?>px;padding: 5px;border-radius: 5px;">
                                                <div class="posone2" >
                                                    <div class="posonclick-add">
                                                        <div class="posone2abs arrowdowns">
                                                            <div class="comment-box-es">
                                                                <div class="cmmbx-col left">
                                                                    <div class="cmmbxcir">
                                                                        <figure class="pro-circle-img-xxs">
                                                                            <?php if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer"){ ?>
                                                            <img src="<?php echo base_url()?>uploads/profile_picture/<?php echo $data[0]['customer_image']; ?>" class="img-responsive">
                                                        <?php }else{ ?>
                                                            <img src="<?php echo base_url()?>uploads/profile_picture/<?php echo $edit_profile[0]['profile_picture']; ?>">
                                                        <?php }?>
                                                                        </figure>
                                                                    </div>
                                                                    <span class="cmmbxtxt"><?php echo $designer_file[$i]['chat'][$j]['customer_name']; ?></span>
                                                                </div>
                                                                <p class="space-a"></p>
                                                                <p class="distxt-cmm"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php } } ?>
                                <?php } ?>
                            </div>
                            <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;">
                                <div class="posone1" style="z-index: 999999999999;">
                                    <div class="posonclick-add">
                                        <div class="posone3abs arrowdowns">
                                            <div class="cmmbxpost">
                                             <textarea type="text" class="tff-con text_write text_<?php echo $designer_file[$i]['id']; ?>" placeholder="Leave a Comment" ></textarea>
                                            </div>
                                            <div class="postrow12">
                                                <span class="pstcancel"><a class="mycancellable" href="javascript:void(0);">Cancel</a></span>
                                                <span class="pstbtns">
                                                    <button class="pstbtns-a send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" style="border: none;border-radius: 5px;padding: 3px 10px 4px;" data-fileid="<?php echo $designer_file[$i]['id']; ?>"
                                                    data-senderrole="qa" 
                                                    data-senderid="<?php echo $_SESSION['user_id']; ?>" data-designerpic="<?php echo $user[0]['profile_picture'] ; ?>"
                                                    data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                    data-receiverrole="customer"
                                                    data-customername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>"
                                                    data-xco="" data-yco="">Post</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="showclicktocomment" style="z-index: 9;color: #fff;background: #c7c1c1;padding: 5px;font-weight: 600;position:absolute;display:none;">
                                Click To Comment
                            </div>
                            <!-- Chat Toolkit end -->
                        </div>
                        <!-- <div class="overlay_comment1"></div> -->
                    <?php //echo "<pre>"; print_r($designer_file); exit;?>
                    <?php if($designer_file[$i]['status'] != "Reject" ) { ?>
                        <div class="overlay_comment"></div>
                    <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
   </div>        
</div>
    <div class="pro-ject-leftbar">
            <div class="headnris">
                <h3 class="head-b">Design Drafts</h3>
            </div>
            <div class="orb-xbin one-can slick_thumbs">
                <ul class="list-unstyled orbxbin-list" id="thub_nails">
                    <?php 
                    for ($i = 0; $i < sizeof($designer_file); $i++) {  ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" class="active1 <?php echo $designer_file[$i]['id'];?>" id="<?php echo $designer_file[$i]['id'];?>">
                            <figure class="imgobxbins" style="background-color:#fff;">
                                <?php
                                 $type=substr($designer_file[$i]['file_name'],strrpos($designer_file[$i]['file_name'],'.')+1);
                                ?>
                                <img src="<?php echo imageUrl($type,$_GET['id'],$designer_file[$i]['file_name']); ?>" class="img-responsive">
                            </figure>
                        </li>
                    <?php  } ?>
                </ul>
            </div>
    </div>  
    <div class="pro-ject-rightbar" id="comment_list_all">
        <?php  for ($i = 0; $i < sizeof($designer_file); $i++){ ?>
            <div class="comment_sec comment-sec-<?php echo $designer_file[$i]['id'];?>" id="<?php echo $designer_file[$i]['id'];?>" 
                <?php if($main_id == $designer_file[$i]['id']){ echo "style='display: block;'"; }else{ echo "style='display:none;'"; }?>>
                <div class="rpjecsrr both">
                    <h3 class="sub-head-e" style="width: 80%;word-wrap: break-word;"><?php echo $designer_file[$i]['file_name']; ?></h3>
                    <form action="<?php echo base_url();?>/qa/dashboard/downloadzip" method="post" style="width: 20%;">
                        <input type="hidden" name="srcfile" value="<?php echo "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                        <input type="hidden" name="prefile" value="<?php echo "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                         <input type="hidden" name="project_name" value="<?php echo $data[0]['title'];?>">
                        <button type="submit" name="submit" style="border:none"><i class="fa fa-download" aria-hidden="true"></i></button>
                    </form>
                </div>
                 <div class="design-draft-header noheight both">
                    <div class="buttons_quality">
                        <div class="approvedate"> 
                        <?php if($designer_file[$i]['status'] == "customerreview" || $designer_file[$i]['status'] == "Approve"){ ?>
                            <p class="dateApp"><?php $approve_date = $designer_file[$i]['modified']; 
                                echo date("M d, Y h:i A", strtotime($approve_date));
                            ?></p>
                        <?php } ?>
                        </div>
                        <?php if($designer_file[$i]['status'] == "pending"){ ?>
                            <p class="btn-x desi-gned notapproved">
                                <input type="button" class="btn-c greenbtn approve response_request" value="Quality Passed" data-fileid="<?php echo $designer_file[$i]['id'];?>" data-file="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" data-status="customerreview"  data-requestid="<?php echo $_GET['id'];?>" data-approve-date="<?php echo date("M d, Y h:i A");?>">
                                <input type="button" class="btn-c redbtn approve response_request" value="Quality Failed" data-fileid="<?php echo $designer_file[$i]['id'];?>" data-file="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" data-status="Reject"  data-requestid="<?php echo $_GET['id'];?>"> 
                            </p>
                            <p class="qua-litypas-sed approved" style="display: none;"><img src="<?php echo base_url(); ?>theme/assets/qa/images/icon-check.png" class="img-responsive"> Quality Passed
                                <h3 class="rejected notapprove" style="display: none;">NOT APPROVED</h3>
                            </p>
                        <?php } elseif ($designer_file[$i]['status'] == "Reject") { ?>
                            <h3 class="rejected">NOT APPROVED</h3>
                        <?php } elseif ($designer_file[$i]['status'] == "customerreview") {  ?>
                            <p class="qua-litypas-sed approved"><img src="<?php echo base_url(); ?>theme/assets/qa/images/icon-check.png" class="img-responsive"> Quality Passed</p>
                        <?php }elseif ($designer_file[$i]['status'] == "Approve"){ ?>
                            <p class="qua-litypas-sed approved">Approved</p>
                        <?php } ?>
                    </div>
                    
                </div> 
                <div class=""> 
                    <div class="headnris">
                        <h3 class="head-b">Comments</h3>
                    </div>
                    <p class="space-a"></p>
                        <ul class="list-unstyled list-comment-es comment_list_view one-can messagediv_<?php echo $designer_file[$i]['id']; ?>" id="comment_list">
                             <?php for ($j=0; $j < sizeof($designer_file[$i]['chat']); $j++) { ?>
                                 <?php if($designer_file[$i]['chat'][$j]['xco'] != "NaN" && $designer_file[$i]['chat'][$j]['yco'] != "NaN"){ ?>
                                <li>
                                    <div class="comment-box-es">
                                        <a class="comments_link" href="javascript:void(0)" id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">
                                            <div class="cmmbx-row">

                                                <div class="cmmbx-col left">
                                                    <div class="cmmbxcir">
                                                        <figure class="pro-circle-img-xxs">
                                                            <?php if($designer_file[$i]['chat'][$j]['sender_role'] == "qa"){ ?>
                                                                <img src="<?php echo base_url()?>uploads/profile_picture/<?php echo $edit_profile[0]['profile_picture']; ?>">
                                                        <?php } elseif ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {?>
                                                            <img src="<?php echo base_url()?>uploads/profile_picture/<?php echo $data[0]['customer_image']; ?>">                                            
                                                        <?php } ?>
                                                        </figure>
                                                        <?php if($designer_file[$i]['chat'][$j]['xco'] != "" && $designer_file[$i]['chat'][$j]['yco'] != ""){ 
                                                            if($designer_file[$i]['chat'][$j]['sender_role'] == "customer"){ 
                                                                $background_color = "#f93c55";
                                                            }elseif ($designer_file[$i]['chat'][$j]['sender_role'] == "qa") {
                                                                $background_color = "#1a3147";
                                                            }
                                                            ?>
                                                        <span class="cmmbxonline"></span>
                                                        <?php }?>
                                                    </div>
                                                    
                                                    <span class="cmmbxtxt">
                                                        <?php echo $designer_file[$i]['chat'][$j]['customer_name'];?>
                                                    </span>
                                                </div>
                                                <div class="cmmbx-col">
                                                    <span class="kevtxt" title='<?php echo date("d-m-Y", strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date("g:i A", strtotime($designer_file[$i]['chat'][$j]['created'])); ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <p class="space-b"></p>
                                            <p class="distxt-cmm"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p>
                                        </a>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php }?>
                        </ul>
                    <div class="cmmtype-box">

                        <form action="" method="post">
                            <div class="cmmtype-row">
                                <!-- <span class="cmmfiles"><img src="<?php echo base_url();?>theme/designer_css/images/icon-attech.png" class="img-responsive"></span> -->
                                <input type="text" class="pstcmm sendtext text1_<?php echo $designer_file[$i]['id']; ?>" Placeholder="Post Comment" id="sendcom_<?php echo $designer_file[$i]['id']; ?>" autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>">
                                <span class="cmmsend">
                                    <button class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>" id="send_comment" data-requestid="<?php echo $designer_file[$i]['id']; ?>" 
                                        data-senderrole="qa" 
                                        data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                        data-receiverid="<?php echo $request[0]['customer_id']; ?>"data-designerpic="<?php echo $user[0]['profile_picture'] ; ?>"
                                        data-receiverrole="customer"
                                        data-sendername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>">
                                        <img src="<?php echo base_url();?>theme/designer_css/images/icon-chat-send.png" class="img-responsive">
                                    </button>
                                </span>
                            </div>
                        </form> 
                    </div>
                </div>

            </div>
        <?php  } ?>
    </div>
</div>

<script src="<?php echo base_url();?>theme/designer_css/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>theme/designer_css/js/bootstrap.min.js"></script>
<script type="text/javascript">
    
        function goBack() {
            window.history.back();
        }
        $('#myCarousel').carousel({
          interval: false,
            cycle: true
        });
        $('#cro .item:first-child').addClass('active');
        $('#thub_nails li').click(function(){
            var id = $(this).attr('id');
            $(".comment_sec").hide();
            $('.openchat').hide();  
            $(".pro-ject-rightbar").find("#"+id).show();
        });
        $('.btn_reset').click(function(){
            $('#cro .active .mydivposition .slid img').css('transform','scale(1)');
            $(this).css('display','none');  
            $('#cro .active .overlay_comment1').css('display','none');  
        });

        var dzm = 1;
        var zm = 0.3;
        $('#zoomIn').click(function(){
            dzm += zm;
            if(dzm != 1){
                $('#cro .active .overlay_comment1').css('display','block');
                $('.btn_reset').css('display','inline-block');
            }else if(dzm == 1){
                $('#cro .active .overlay_comment1').css('display','none');
                $('.btn_reset').css('display','none');  
            }
            if(dzm > 3){
                dzm = 3;
                $('#cro .active .mydivposition .slid img').css('transform','scale('+dzm+')');
            }else{
                $('#cro .active .mydivposition .slid img').css('transform','scale('+dzm+')');
            }
        });
        $('#zoomOut').click(function(){
            dzm -= zm;
            if(dzm != 1){
                $('#cro .active .overlay_comment1').css('display','block');
                $('.btn_reset').css('display','inline-block');
            }else if(dzm == 1){
                $('#cro .active .overlay_comment1').css('display','none');
                $('.btn_reset').css('display','none');      
            }
            if(dzm < 0){
                dzm = 0.1
                $('#cro .active .mydivposition .slid img').css('transform','scale('+dzm+')');
            }else{
                $('#cro .active .mydivposition .slid img').css('transform','scale('+dzm+')');
            }
        });

    var total_design1 = <?php echo sizeof($designer_file); ?>;
    var ind_id = $('.<?php echo $main_id; ?>').index();
    $(".pagin-one").html(ind_id+1 + " of " + total_design1);   
    setTimeout(function(){
        $('#thub_nails .<?php echo $main_id;?>').click();
    },200);

</script>
<script type="text/javascript">
$(document).ready(function(){  
        $('.sendtext').keypress(function (e) {
            var postbtn = $(this).attr('data-reqid');
            if (e.which == 13) {
                $('.send_'+postbtn).click();
                return false;
            }
        });
        $('.send_comment_without_img').click(function (e) {
            e.preventDefault();
            var request_id = $(this).data("requestid");
            var sender_type = $(this).data("senderrole");
            var sender_id = $(this).data("senderid");
            var reciever_id = $(this).data("receiverid");
            var reciever_type = $(this).data("receiverrole");
            var text_id = 'text1_' + request_id;
            var message = $('.'+text_id).val();
            var verified_by_admin = "1";
            var customer_name = $(this).data("sendername");
            var designer_img = $(this).data("designerpic");
            if (message != "") {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>qa/dashboard/send_message",
                    data: {"request_file_id": request_id,
                    "sender_role": sender_type,
                    "sender_id": sender_id,
                    "receiver_role": reciever_type,
                    "receiver_id": reciever_id,
                    "message": message,
                    "customer_name": customer_name,
                    "qa_seen": 1,
                    },
                    success: function (data) {
                        // alert("---"+data);
                        //alert("Settings has been updated successfully.");
                        $('.sendtext').val("");
                        $(".messagediv_"+request_id+"").append('<li><div class="comment-box-es"><a href="javascript:void(0);"><div class="cmmbx-row"><div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="<?php echo base_url();?>uploads/profile_picture/'+designer_img+'" class="mCS_img_loaded"></figure></div><span class="cmmbxtxt">'+customer_name+' </span></div><div class="cmmbx-col"><span class="kevtxt">Just Now</span></div></div><p class="space-b"></p><p class="distxt-cmm">'+message+' </p></a></div></li>'); 
                        $(".messagediv_"+request_id+"").stop().animate({
                          scrollTop: $(".messagediv_"+request_id+"")[0].scrollHeight
                        }, 1000);
                    },
                });         
            }
        });
    });

    $('.send_request_img_chat').click(function (event) {
        event.stopPropagation();
        var request_file_id = $(this).attr("data-fileid");
        var sender_role = $(this).attr("data-senderrole");
        var sender_id = $(this).attr("data-senderid");
        var receiver_role = $(this).attr("data-receiverrole");
        var receiver_id = $(this).attr("data-receiverid");
        var text_id = 'text_' + request_file_id;
        var message = $('.' + text_id).val();
        var customer_name = $(this).attr("data-customername");
        var xco = $(this).attr("data-xco");
        var yco = $(this).attr("data-yco");
        var designer_img = $(this).data("designerpic");
        if (message != "") {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>qa/dashboard/send_message",
                data: {"request_file_id": request_file_id,
                    "sender_role": sender_role,
                    "sender_id": sender_id,
                    "receiver_role": receiver_role,
                    "receiver_id": receiver_id,
                    "message": message,
                    "customer_name": customer_name,
                    "xco": xco,
                    "yco": yco,
                    "qa_seen": 1,
                },
                success: function (data) {
                    // alert("---"+data);
                    //alert("Settings has been updated successfully.");
                    $('.text_' + request_file_id).val("");
                    $(".messagediv_"+request_file_id+"").append('<li><div class="comment-box-es"><a class="comments_link" href="javascript:void(0);" id="'+data+'"><div class="cmmbx-row"><div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="<?php echo base_url();?>uploads/profile_picture/'+designer_img+'" class="mCS_img_loaded"></figure><span class="cmmbxonline" style="background-color: #1a3147;"></span></div><span class="cmmbxtxt">'+customer_name+' </span></div><div class="cmmbx-col"><span class="kevtxt" title="02-07-2018">Just Now</span></div></div><p class="space-b"></p><p class="distxt-cmm">'+message+'</p></a></div></li>');
                    $('.appenddot_'+request_file_id).append('<div id="'+data+'" class="" style="position:absolute;left:'+(xco-10)+'px;top:'+(yco-10)+'px;width:25px;z-index:99999999;height:25px;" onclick="show_customer_chat('+data+', event);"><img src="<?php echo base_url();?>public/designer_image1.png" class="customer_chatimage1 customer_chatimage101" style="filter: drop-shadow(rgb(0, 0, 0) 0px 3px 8px); display: none;"><img style="" src="<?php echo base_url();?>public/designer_image2.png" class="customer_chatimage2 customer_chatimage101"></div>');
                    $('.appenddot_'+request_file_id).append('<div class="customer_chat customer_chat'+data+'" style="position: absolute; left: '+(xco-10)+'px; top: '+(yco-10)+'px; padding: 5px; border-radius: 5px; display: none;"><div class="posone2" style="z-index: 99999999999999999999999;top:0px; "><div class="posonclick-add"><div class="posone2abs arrowdowns"><div class="comment-box-es"><div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="<?php echo base_url();?>uploads/profile_picture/'+designer_img+'"></figure></div><span class="cmmbxtxt">'+customer_name+'</span></div><p class="space-a"></p><p class="distxt-cmm">'+message+'</p></div></div></div></div></div>');
                        $(".mycancel").remove();
                        $(".messagediv_"+request_file_id+"").stop().animate({
                          scrollTop: $(".messagediv_"+request_file_id+"")[0].scrollHeight
                        }, 1000);
                        $('.openchat').hide();
                }
            });
        }
    });

    $(document).ready(function(){
        $('#thub_nails li figure').removeClass('active');
        $('#thub_nails .<?php echo $main_id;?> figure').addClass('active');

        $('.response_request').click(function () {
            var btn = this;
            var parent = $(this).closest('.comment_sec').attr('id');
            var fileid = $(this).attr('data-fileid');
            var value = $(this).attr("data-status");
            var file = $(this).attr('data-file');
            var request_id = $(this).attr('data-requestid');
            var approve_date = $(this).attr('data-approve-date');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>qa/dashboard/request_response",
                data: {"fileid": fileid,
                    "value": value,
                    "request_id": request_id},
                success: function (data) {
                    //alert("---"+data);
                    //alert("Settings has been updated successfully.");
                    if (data == 1) {
                        if (value == "customerreview") {
                            $(btn).parent().css('display','none');
                            $(btn).parent().next('.approved').css('display','block');
                            $(btn).parent().prev('.approvedate').append("<p class='dateApp'>"+approve_date+"</p>");

                        } else if(value == "Reject"){
                            $(btn).parent().css('display','none');
                            $(btn).parent().siblings().eq(2).css('display','block');
                            $("#"+fileid+" .overlay_comment").remove();
                        }
                    }
                }
            });
        });

    
                $('.slick_thumbs ul li').click(function(){
                $(".slick_thumbs ul li").find('figure').removeClass('active');
                $(this).find('figure').addClass('active');
                var thumb_index = $(this).index();
                $('ul.slick-dots').find('li:eq(' + thumb_index + ')').trigger('click');
                var dot_length = $('ul.slick-dots').children().length;

                var count_slid = thumb_index;
                var current_design = count_slid;
                var total_design = <?php echo sizeof($designer_file); ?>;
                current_design += 1;
                if (current_design > total_design) {
                    current_design = 1;
                }
                $(".pagin-one").html(current_design + " of " + total_design);
            });
            // Slick Slider script Start here 
            $('#changepass').click(function(e){
                e.preventDefault();
            });
           
        
    });
        function showclicktocomment(event, id) {
            var position = $("#mydivposition_" + id).offset();
            var posX = position.left;
            var posY = position.top;
            var left = event.pageX - posX;
            var top = event.pageY - posY + +10;
            var window_height = $(window).height();
            if(top > window_height - 100){
                top = top - 40;
            }
            $('.showclicktocomment').remove();
            if ($(".openchat").css("display") != "block") {
                $('.openchat').after('<div class="showclicktocomment" style="z-index: 2147483647; color: rgb(255, 255, 255); background: rgb(0, 194, 121); padding: 5px; font-weight: 600; position: absolute; left: ' + left + 'px; top: ' + top + 'px; display: none; font-family:GothamPro;">\Click To Comment\</div>');
                $("#mydivposition_" + id + ' .showclicktocomment').show();

                //event.stopPropaganation();
                // event.preventDefault();
                return false;
            }

        }
        function hideclicktocomment()
        {
            $('.showclicktocomment').remove();
        }
        
        function printMousePos(event, id) {
            // $(".customer_chat" + id + " .posone2abs").removeClass('right_msgBox');
            $(".openchat").css("z-index", "0");
            $('.slick-active .openchat').css("z-index", "9999999999999999999");
            $(".customer_chatimage1").show();
            $(".customer_chatimage2").hide();
            $('.customer_chat').hide();
            $('.showclicktocomment').remove();
            var position = $("#mydivposition_" + id).offset();
            var posX = position.left;
            var posY = position.top;
           if(event.pageY - posY < 200){
                $('.mydivposition_'+id+' .posone3abs').addClass('topposition');
            }else{
                $('.mydivposition_'+id+' .posone3abs').removeClass('topposition');
            }
            $('.openchat').css("left", event.pageX - posX + +7);
            $('.openchat').css("top", event.pageY - posY + +7);
            $(".openchat").show();
            $('.send_text' + id).attr("data-yco", event.pageY - posY);
            $('.send_text' + id).attr("data-xco", event.pageX - posX);


            var posx = Math.round(event.pageX - posX - 10);
            var posy = Math.round(event.pageY - posY - 10);
            var posx_ = Math.round(event.pageX - posX);
            var posy_ = Math.round(event.pageY - posY);

            var myposition = Math.round(posX + posY);
            $('.send_text' + id).attr("data-cancel-false", myposition)
            $('.mycancellable').attr("data-cancel", myposition);
            $(".mydiv" + myposition).remove();
            $('.appenddot_' + id).append('<div class="mydiv' + myposition + ' mycancel" style="position:absolute;left:' + posx + 'px;top:' + posy + 'px;width:25px;height:25px;" onclick="show_customer_chat(' + myposition + ',event);">\<img src="<?php echo base_url(); ?>public/designer_image1.png" style="    position: relative;z-index: 999;">\</div>\<div class="customer_chat customer_chat' + myposition + '" style="display:none;background:#fff;position:absolute;left:' + posx_ + 'px;top:' + posy_ + 'px;padding: 5px;border-radius: 5px;">\<label></label>\</div>');
        }
        function show_customer_chat(id, event) {
            var topPosition = $("#"+id+"").css('top');
            var newtopPosition = topPosition.substring(0,topPosition.length-2); //string 800
            newtopPosition= parseFloat(newtopPosition) || 0; 
            if(newtopPosition < 200){
               $('.customer_chat'+id+' .posone2abs').toggleClass('topposition');
            }else{
                $('.customer_chat'+id+' .posone2abs').removeClass('topposition');
            }
            $(".customer_chat").hide();
            $(".customer_chat" + id).toggle();
            setTimeout(function () {
                $('.myallremove').remove();
                $(".openchat").hide();
                $(".customer_chat").each(function () {
                    if ($(this).html() == "<label></label>") {
                        $(this).remove();
                    }
                    if ($(this).text() == "") {
                        $(this).remove();
                    }
                })
                $(".customer_chatimage" + id).toggle();
                $(".customer_chat" + id).toggle();

                $('.showclicktocomment').remove();
                $('.mycancel').remove();
                event.preventDefault();
            }, 1);
            //event.stopPropogation();
            return false;
        }
</script>
<script type="text/javascript">
    window.onload = function(){
        $(".messagediv_<?php echo $main_id;?>").stop().animate({
          scrollTop: $(".messagediv_<?php echo $main_id;?>")[0].scrollHeight
        }, 1);
        // if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
        //     {
        //         $('.list-comment-es > li:last-child').css('margin-bottom','60px');
        //         //alert("Please dont use IE.");
        //     }else{
        //         $('#comment_list li:last-child').css('padding-bottom', '100px');
        //         $('.list-comment-es > li:last-child').css('margin-bottom','10px');
        //     }
    }
    
</script>
<script type="text/javascript">
    $('.mycancellable').click(function (event) {
        event.stopPropagation();
    $('.openchat').hide();
    var mycancel = $(this).attr('data-cancel');
    $('.mydiv' + mycancel).hide();
  });

  function mycancellable(id)
  {
      $('.openchat').hide();
      var mycancel = $(".mycancellable" + id).attr('data-cancel');
      $('.mydiv' + mycancel).hide();
  }
  $('.text_write').click(function(event){
    event.stopPropagation();
  });
</script>
