<style type="text/css">
    ul.list-header-blog li.active a{
        color: #e52344;
        border-bottom: 2px solid #e52344;
    }
    .list-header-blog.small li {
         margin-right: 0px; 
    }
    .draft_no {
        position: absolute;
        top: 60%;
        left: 12%;
        color: #5963759c;
    }
</style>

<section class="con-b">
    <div class="container">
        <div class="header-blog xss0">
            <div class="row flex-show">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="cell-row">
                                <div class="cell-col">
                                    <?php for ($i = 0; $i < sizeof($designers); $i++) { ?>
                                    <div class="client-leftshows">
                                        <?php if ($designers[$i]['profile_picture']){ ?>
                                        <figure class="cli-ent-img circle one">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$designers[$i]['profile_picture'];?>" class="img-responsive one">
                                        </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 23px; font-family: GothamPro-Medium;">
                                        <?php echo ucwords(substr($designers[$i]['first_name'],0,1)) .  ucwords(substr($designers[$i]['last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                        <h3 class="pro-head-q space-b"><a href="javascript:void(0);"><?php echo $designers[$i]['first_name']; ?></a></h3>
                                    </div>
                                <?php } ?>
                                </div>
                                
                                <div class="cell-col">
                                <a href="#" class="inshowskills" data-toggle="modal" data-target="#ShowSkill"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/skill_03.png" class="img-responsive"></a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-9">
                            <ul id="status_check" class="list-unstyled list-header-blog small" role="tablist">
                                <li class="active" id="1">
                                    <a  data-status="active,disapprove"  data-toggle="tab" href="#Qa_active" role="tab">Active(<?php echo sizeof($datadesigner['activeproject']); ?>)</a>
                                </li>         
                                <li  id="2">
                                    <a data-toggle="tab" data-status="assign"  href="#Qa_in_queue" role="tab">In-Queue(<?php echo sizeof($datadesigner['inqueueproject']); ?>)</a>
                                </li>       
                                <li  id="3">
                                    <a data-toggle="tab" data-status="pendingrevision" href="#Qa_pending_review" role="tab">Pending Review(<?php echo sizeof($datadesigner['pendingreviewproject']); ?>) </a>
                                </li>      
                                <li  id="4">
                                    <a data-toggle="tab" data-status="checkforapprove" href="#Qa_pending_approval" role="tab">Pending Approval(<?php echo sizeof($datadesigner['pendingproject']); ?>) </a>
                                </li>
                                <li  id="5">
                                    <a data-toggle="tab" data-status="approved"  href="#Qa_completed" role="tab">Completed(<?php echo sizeof($datadesigner['completeproject']); ?>)</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                
                </div>
               <div class="col-md-2">
                    <div class="search-box">
                        <form method="post" class="search-group clearfix">
                            <input type="text" placeholder="Search here..." class="form-control searchdata" id="search_text">
                            <input type="hidden" name="userid" value="<?php echo $designers[0]['id'];?>" id="userid">
                             <input type="hidden" name="status" id="status" value="active,disapprove">
                            <button type="submit" class="search-btn search search_data_ajax">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                <!-- <svg class="icon">
                                    <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                </svg>-->
                            </button>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
        
        <p class="space-e"></p>
        
        <div class="cli-ent table">
            <div class="tab-content">
                <!--QA Active Section Start Here -->
                    <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                        <?php for ($i=0; $i <sizeof($datadesigner['activeproject']) ; $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['activeproject'][$i]['id']; ?>/1'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        <?php if($datadesigner['activeproject'][$i]['status'] == "active") { 
                                                echo "Expected on";
                                            }elseif ($datadesigner['activeproject'][$i]['status_designer'] == "disapprove") {
                                                echo "Expected on";
                                            } ?>
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php if($datadesigner['activeproject'][$i]['status_designer'] == "active") { 
                                            echo date('M d, Y h:i A', strtotime($datadesigner['activeproject'][$i]['expected']));
                                        }elseif ($datadesigner['activeproject'][$i]['status_designer'] == "disapprove") {
                                            echo date('M d, Y h:i A', strtotime($datadesigner['activeproject'][$i]['expected']));
                                        } ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        
                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['activeproject'][$i]['id']; ?>/1"><?php echo $datadesigner['activeproject'][$i]['title'];?></a></h3>
                                            <p class="pro-b"><?php echo substr($datadesigner['activeproject'][$i]['description'], 0, 30); ?></p>
                                        </div>
                                        
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                    <?php if ($datadesigner['activeproject'][$i]['status_designer'] == "active") {?>
                                                        <span class="green text-uppercase text-wrap">In-Progress</span>
                                                    <?php }elseif ($datadesigner['activeproject'][$i]['status_designer'] == "disapprove" && $datadesigner['activeproject'][$i]['who_reject'] == 1) { ?>
                                                        <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                                    <?php }elseif ($datadesigner['activeproject'][$i]['status_designer'] == "disapprove" && $datadesigner['activeproject'][$i]['who_reject'] == 0) {  ?>
                                                         <span class="red  text-uppercase text-wrap">Quality Revision</span>
                                                    <?php } ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="cli-ent-col td" style="width: 20%;">
                                <div class="cli-ent-xbox text-left p-left1">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['activeproject'][$i]['id']; ?>/1">
                                                <figure class="pro-circle-img">
                                                    <?php if($datadesigner['activeproject'][$i]['profile_picture'] != ""){?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$datadesigner['activeproject'][$i]['profile_picture']?>" class="img-responsive">
                                                    <?php }else{ ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h" title="<?php echo $datadesigner['activeproject'][$i]['designer_first_name'] . " " . $datadesigner['activeproject'][$i]['designer_last_name']; ?>">
                                                <?php echo $datadesigner['activeproject'][$i]['designer_first_name'];?>
                                                <?php 
                                                    if(strlen($datadesigner['activeproject'][$i]['designer_last_name']) > 5){ 
                                                        echo ucwords(substr($datadesigner['activeproject'][$i]['designer_last_name'],0,1)); 
                                                    }else{ 
                                                        echo $datadesigner['activeproject'][$i]['designer_last_name']; 
                                                    } ?>
                                                
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['activeproject'][$i]['id']; ?>/1">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                        <?php if($datadesigner['activeproject'][$i]['total_chat'] + $datadesigner['activeproject'][$i]['comment_count'] != 0){ ?>
                                                <span class="numcircle-box">
                                                   <?php echo $datadesigner['activeproject'][$i]['total_chat'] + $datadesigner['activeproject'][$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                    <?php //echo $datadesigner['activeproject'][$i]['total_chat_all']; ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['activeproject'][$i]['id']; ?>/1">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                        <?php if(count($datadesigner['activeproject'][$i]['total_files']) != 0 ){?>
                                            <span class="numcircle-box"><?php count($datadesigner['activeproject'][$i]['total_files']); ?></span>
                                        <?php } ?>
                                        </span>
                                        <?php echo count($datadesigner['activeproject'][$i]['total_files_count']); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                        <?php } ?>
                        
                        <!-- <p class="space-e"></p>
                        <div class="loader">
                            <a class="loader-link text-uppercase" href=""><svg class="icon" width="35">
                                <use xlink:href="images/symbol-defs.svg#icon-loading"></use>
                            </svg> Loading</a>
                        </div> -->
                    </div>
                <!--QA Active Section End Here -->
                <!--QA IN Queue Section Start Here -->
                    <div class="tab-pane content-datatable datatable-width" id="Qa_in_queue" role="tabpanel">
                           <?php for ($i=0; $i <sizeof($datadesigner['inqueueproject']) ; $i++) { ?>
                           <!-- Start Row -->
                            <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                                <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>/2'">
                                    <div class="cli-ent-xbox">
                                        <p class="pro-a">In Queue</p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 48%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>/2'">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">
                                            
                                            <div class="cell-col" >
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>/2"><?php echo $datadesigner['inqueueproject'][$i]['title'];?></a></h3>
                                                <p class="pro-b"><?php echo substr($datadesigner['inqueueproject'][$i]['description'], 0, 30); ?></p>
                                            </div>
                                            
                                            <div class="cell-col col-w1">
                                                <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="cli-ent-col td" style="width: 20%;">
                                    <div class="cli-ent-xbox text-left p-left1">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>/2"><figure class="pro-circle-img">
                                                    <?php if($datadesigner['inqueueproject'][$i]['profile_picture'] != ""){?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$datadesigner['inqueueproject'][$i]['profile_picture']?>" class="img-responsive">
                                                    <?php }else{ ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h" title="<?php echo $datadesigner['inqueueproject'][$i]['designer_first_name'] . " " . $datadesigner['inqueueproject'][$i]['designer_last_name']; ?>">
                                                    <?php echo $datadesigner['inqueueproject'][$i]['designer_first_name'];?>
                                                    <?php 
                                                        if(strlen($datadesigner['inqueueproject'][$i]['designer_last_name']) > 5){ 
                                                            echo ucwords(substr($datadesigner['inqueueproject'][$i]['designer_last_name'],0,1)); 
                                                        }else{ 
                                                            echo $datadesigner['inqueueproject'][$i]['designer_last_name']; 
                                                        } ?>
                                                    </p>
                                                <p class="pro-b">Designer</p>
                                                <p class="space-a"></p>
                                                <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>" data-designerid= "<?php echo $datadesigner['inqueueproject'][$i]['designer_id']; ?>">
                                                        <span class="sma-red">+</span> Add Designer
                                                </a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>/2'">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"> <?php if($datadesigner['inqueueproject'][$i]['total_chat'] + $datadesigner['inqueueproject'][$i]['comment_count'] != 0){ ?>
                                                <span class="numcircle-box">
                                                   <?php echo $datadesigner['inqueueproject'][$i]['total_chat'] + $datadesigner['inqueueproject'][$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $datadesigner['inqueueproject'][$i]['total_chat_all'];?></a></p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>/2'">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['inqueueproject'][$i]['id']; ?>/2">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if(count($datadesigner['inqueueproject'][$i]['total_files']) != 0 ){?>
                                            <span class="numcircle-box"><?php echo count($datadesigner['inqueueproject'][$i]['total_files']); ?></span>
                                        <?php } ?>
                                        </span>
                                        <?php echo count($datadesigner['inqueueproject'][$i]['total_files_count']); ?></a></p>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                        <?php } ?>
                          <!--   <p class="space-e"></p>
                            <div class="loader">
                                <a class="loader-link text-uppercase" href=""><svg class="icon" width="35">
                                    <use xlink:href="images/symbol-defs.svg#icon-loading"></use>
                                </svg> Loading</a>
                            </div> -->
                    </div>
                <!--QA IN Queue Section End Here -->
                <!--QA Pending Review Section Start Here -->
                    <div class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel">
                        <?php for ($i=0; $i <sizeof($datadesigner['pendingreviewproject']) ; $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingreviewproject'][$i]['id']; ?>/3'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Expected on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php
                                            echo date('M d, Y h:i A', strtotime($datadesigner['pendingreviewproject'][$i]['revisiondate']));
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        
                                        <div class="cell-col">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingreviewproject'][$i]['id']; ?>/3"><?php echo $datadesigner['pendingreviewproject'][$i]['title']; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($datadesigner['pendingreviewproject'][$i]['description'], 0, 30); ?></p>
                                        </div>
                                        
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingreviewproject'][$i]['id']; ?>/3"><figure class="pro-circle-img">
                                               <?php if($datadesigner['pendingreviewproject'][$i]['customer_profile_picture'] != ""){?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$datadesigner['pendingreviewproject'][$i]['customer_profile_picture']?>" class="img-responsive">
                                                    <?php }else{ ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                            </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $datadesigner['pendingreviewproject'][$i]['customer_first_name'] . " " . $datadesigner['pendingreviewproject'][$i]['customer_last_name']; ?>">
                                                <?php echo $datadesigner['pendingreviewproject'][$i]['customer_first_name'];?>
                                                    <?php 
                                                        if(strlen($datadesigner['pendingreviewproject'][$i]['customer_last_name']) > 5){ 
                                                            echo ucwords(substr($datadesigner['pendingreviewproject'][$i]['customer_last_name'],0,1)); 
                                                        }else{ 
                                                            echo $datadesigner['pendingreviewproject'][$i]['customer_last_name']; 
                                                        } ?>
                                                </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingreviewproject'][$i]['id']; ?>/3"><figure class="pro-circle-img">
                                                <?php if($datadesigner['pendingreviewproject'][$i]['profile_picture'] != ""){?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$datadesigner['pendingreviewproject'][$i]['profile_picture']?>" class="img-responsive">
                                                    <?php }else{ ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                            </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $datadesigner['pendingreviewproject'][$i]['designer_first_name'] . " " . $datadesigner['pendingreviewproject'][$i]['designer_last_name']; ?>">
                                                 <?php echo $datadesigner['pendingreviewproject'][$i]['designer_first_name'];?>
                                                    <?php 
                                                        if(strlen($datadesigner['pendingreviewproject'][$i]['designer_last_name']) > 5){ 
                                                            echo ucwords(substr($datadesigner['pendingreviewproject'][$i]['designer_last_name'],0,1)); 
                                                        }else{ 
                                                            echo $datadesigner['pendingreviewproject'][$i]['designer_last_name']; 
                                                        } ?>
                                                </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingreviewproject'][$i]['id']; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if($datadesigner['pendingreviewproject'][$i]['total_chat'] + $datadesigner['pendingreviewproject'][$i]['comment_count'] != 0){ ?>
                                                <span class="numcircle-box">
                                                   <?php echo $datadesigner['pendingreviewproject'][$i]['total_chat'] + $datadesigner['pendingreviewproject'][$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                    <?php //echo $datadesigner['pendingreviewproject'][$i]['total_chat_all'];?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingreviewproject'][$i]['id']; ?>/3">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if(count($datadesigner['pendingreviewproject'][$i]['total_files']) != 0 ){?>
                                            <span class="numcircle-box"><?php echo count($datadesigner['pendingreviewproject'][$i]['total_files']);?></span>
                                        <?php } ?>
                                        </span>
                                        <?php echo count($datadesigner['pendingreviewproject'][$i]['total_files_count']); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                        <?php } ?>
                    </div>
                <!--QA Pending Review Section End Here -->
                <!--QA Pending Approval Section Start Here -->
                    <div class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel">
                        <?php for ($i=0; $i <sizeof($datadesigner['pendingproject']) ; $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingproject'][$i]['id']; ?>/4'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Delivered on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php
                                            echo date('M d, Y h:i A', strtotime($datadesigner['pendingproject'][$i]['reviewdate']));
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        
                                        <div class="cell-col">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingproject'][$i]['id']; ?>/4"><?php echo $datadesigner['pendingproject'][$i]['title']; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($datadesigner['pendingproject'][$i]['description'], 0, 30); ?></p>
                                        </div>
                                        
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-approval</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingproject'][$i]['id']; ?>/4"><figure class="pro-circle-img">
                                               <?php if($datadesigner['pendingproject'][$i]['customer_profile_picture'] != ""){?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$datadesigner['pendingproject'][$i]['customer_profile_picture']?>" class="img-responsive">
                                                    <?php }else{ ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                            </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $datadesigner['pendingproject'][$i]['customer_first_name'] . " " . $datadesigner['pendingproject'][$i]['customer_last_name']; ?>">
                                                <?php echo $datadesigner['pendingproject'][$i]['customer_first_name'];?>
                                                    <?php 
                                                        if(strlen($datadesigner['pendingproject'][$i]['customer_last_name']) > 5){ 
                                                            echo ucwords(substr($datadesigner['pendingproject'][$i]['customer_last_name'],0,1)); 
                                                        }else{ 
                                                            echo $datadesigner['pendingproject'][$i]['customer_last_name']; 
                                                        } ?>
                                                </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingproject'][$i]['id']; ?>/4"><figure class="pro-circle-img">
                                                <?php if($datadesigner['pendingproject'][$i]['profile_picture'] != ""){?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$datadesigner['pendingproject'][$i]['profile_picture']?>" class="img-responsive">
                                                    <?php }else{ ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                            </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $datadesigner['pendingproject'][$i]['designer_first_name'] . " " . $datadesigner['pendingproject'][$i]['designer_last_name']; ?>">
                                                 <?php echo $datadesigner['pendingproject'][$i]['designer_first_name'];?>
                                                    <?php 
                                                        if(strlen($datadesigner['pendingproject'][$i]['designer_last_name']) > 5){ 
                                                            echo ucwords(substr($datadesigner['pendingproject'][$i]['designer_last_name'],0,1)); 
                                                        }else{ 
                                                            echo $datadesigner['pendingproject'][$i]['designer_last_name']; 
                                                        } ?>
                                                </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingproject'][$i]['id']; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if($datadesigner['pendingproject'][$i]['total_chat'] + $datadesigner['pendingproject'][$i]['comment_count'] != 0){ ?>
                                                <span class="numcircle-box">
                                                   <?php echo $datadesigner['pendingproject'][$i]['total_chat'] + $datadesigner['pendingproject'][$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                    <?php //echo $datadesigner['pendingproject'][$i]['total_chat_all'];?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['pendingproject'][$i]['id']; ?>/4">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if(count($datadesigner['pendingproject'][$i]['total_files']) != 0 ){?>
                                            <span class="numcircle-box"><?php echo count($datadesigner['pendingproject'][$i]['total_files']);?></span>
                                        <?php } ?>
                                        </span>
                                        <?php echo count($datadesigner['pendingproject'][$i]['total_files_count']); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                        <?php } ?>
                    </div>
                <!--QA Pending Approval Section End Here -->
                <!--QA Completed Section Start Here -->
                        <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
                            <?php for ($i = 0; $i < sizeof($datadesigner['completeproject']); $i++) { ?>
                            <!-- Start Row -->
                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['completeproject'][$i]['id']; ?>/5'" style="cursor: pointer;">
                                <div class="cli-ent-col td" style="width: 13%;">
                                    <div class="cli-ent-xbox">
                                        <h3 class="app-roved green">Approved on</h3>
                                        <p class="pro-b">
                                            <?php
                                                echo date('M d, Y h:i A', strtotime($datadesigner['completeproject'][$i]['approvedate']));
                                            ?> 
                                        </p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 37%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">
                                            
                                            <div class="cell-col">
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['completeproject'][$i]['id']; ?>/5"><?php echo $datadesigner['completeproject'][$i]['title'];?></a></h3>
                                                <p class="pro-b"><?php echo substr($datadesigner['completeproject'][$i]['description'], 0, 30); ?></p>
                                            </div>
                                            
                                            <div class="cell-col col-w1">
                                                <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="cli-ent-col td" style="width: 16%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['completeproject'][$i]['id']; ?>/5"><figure class="pro-circle-img">
                                                    <?php if($datadesigner['completeproject'][$i]['customer_profile_picture'] != ""){?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$datadesigner['completeproject'][$i]['customer_profile_picture']?>" class="img-responsive">
                                                    <?php }else{ ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h text-wrap" title="<?php echo $datadesigner['completeproject'][$i]['customer_first_name'] . " " . $datadesigner['completeproject'][$i]['customer_last_name']; ?>">
                                                    <?php echo $datadesigner['completeproject'][$i]['customer_first_name'];?>
                                                    <?php 
                                                        if(strlen($datadesigner['completeproject'][$i]['customer_last_name']) > 5){ 
                                                            echo ucwords(substr($datadesigner['completeproject'][$i]['customer_last_name'],0,1)); 
                                                        }else{ 
                                                            echo $datadesigner['completeproject'][$i]['customer_last_name']; 
                                                        } ?>

                                                    </p>
                                                <p class="pro-b">Client</p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <div class="cli-ent-col td" style="width: 16%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['completeproject'][$i]['id']; ?>/5"><figure class="pro-circle-img">
                                                    <?php if($datadesigner['completeproject'][$i]['profile_picture'] != ""){?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$datadesigner['completeproject'][$i]['profile_picture']?>" class="img-responsive">
                                                    <?php }else{ ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h text-wrap" title="<?php echo $datadesigner['completeproject'][$i]['designer_first_name'] . " " . $datadesigner['completeproject'][$i]['designer_last_name']; ?>">
                                                     <?php echo $datadesigner['completeproject'][$i]['designer_first_name'];?>
                                                    <?php 
                                                        if(strlen($datadesigner['completeproject'][$i]['designer_last_name']) > 5){ 
                                                            echo ucwords(substr($datadesigner['completeproject'][$i]['designer_last_name'],0,1)); 
                                                        }else{ 
                                                            echo $datadesigner['completeproject'][$i]['designer_last_name']; 
                                                        } ?>
                                                    </p>
                                                <p class="pro-b">Designer</p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 9%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['completeproject'][$i]['id']; ?>/5">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if($datadesigner['completeproject'][$i]['total_chat'] + $datadesigner['completeproject'][$i]['comment_count'] != 0){ ?>
                                                <span class="numcircle-box">
                                                   <?php echo $datadesigner['completeproject'][$i]['total_chat'] + $datadesigner['completeproject'][$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $datadesigner['completeproject'][$i]['total_chat_all']; ?></a></p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 9%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $datadesigner['completeproject'][$i]['id']; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if(count($datadesigner['completeproject'][$i]['total_files']) != 0 ){?>
                                            <span class="numcircle-box"><?php echo count($datadesigner['completeproject'][$i]['total_files']);?></span>
                                        <?php } ?>
                                        </span>
                                        <?php echo count($datadesigner['completeproject'][$i]['total_files_count']); ?></a></p>
                                    </div>
                                </div>
                            </div><!-- End Row -->
                        <?php } ?>
                            <!-- 
                            <p class="space-e"></p>
                            <div class="loader">
                                <a class="loader-link text-uppercase" href=""><svg class="icon" width="35">
                                    <use xlink:href="images/symbol-defs.svg#icon-loading"></use>
                                </svg> Loading</a>
                            </div> -->
                        </div>
                <!--QA Completed Section End Here -->
            </div> 
        </div>           
    </div>
</section>
    
    <!-- Modal -->
    <div class="modal fade" id="ShowSkill" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="cli-ent-model-box">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">
                            <h3 class="head-c">Skills</h3>
                        </header>
                        <div class="fo-rm-body">
                            <div class=" profielveiwee-box">
                                <?php if(!empty($skills)){ ?>
                                    <!-- Progress Start -->
                                        <?php if($skills){ 
                                            if($skills[0]['brandin_logo'] != 0){
                                            ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Brandig & Logos</h3>
                                                    <div class="progress-value">
                                                        <span>
                                                            <?php echo $skills[0]['brandin_logo'];?>
                                                        </span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:<?php echo $skills[0]['brandin_logo'];?>%; background:#e73250;"></div>
                                                </div>
                                            </div> <!--Progress End -->
                                        <?php }
                                            }else{ ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Brandig & Logos</h3>
                                                    <div class="progress-value">
                                                        <span>0</span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:0%; background:#e73250;"></div>
                                                </div>
                                            </div> 
                                        <?php } ?>
                                    <!--Progress End -->

                                    <!-- Progress Start -->
                                        <?php if($skills){ 
                                            if($skills[0]['ads_banner'] != 0){
                                            ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Ads & Banners</h3>
                                                    <div class="progress-value">
                                                        <span>
                                                            <?php echo $skills[0]['ads_banner'];?>
                                                        </span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:<?php echo $skills[0]['ads_banner'];?>%; background:#e73250;"></div>
                                                </div>
                                            </div> <!--Progress End -->
                                        <?php }
                                            }else{ ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Ads & Banners</h3>
                                                    <div class="progress-value">
                                                        <span>0</span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:0%; background:#e73250;"></div>
                                                </div>
                                            </div> 
                                        <?php } ?>
                                    <!--Progress End -->
                                        
                                    <!-- Progress Start -->
                                        <?php if($skills){ 
                                            if($skills[0]['email_marketing'] != 0){
                                            ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Email & Marketing</h3>
                                                    <div class="progress-value">
                                                        <span>
                                                            <?php echo $skills[0]['email_marketing'];?>
                                                        </span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:<?php echo $skills[0]['email_marketing'];?>%; background:#e73250;"></div>
                                                </div>
                                            </div> <!--Progress End -->
                                        <?php }
                                            }else{ ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Email & Marketing</h3>
                                                    <div class="progress-value">
                                                        <span>0</span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:0%; background:#e73250;"></div>
                                                </div>
                                            </div> 
                                        <?php } ?>
                                    <!--Progress End -->
                                        
                                    <!-- Progress Start -->
                                        <?php if($skills){ 
                                            if($skills[0]['webdesign'] != 0){
                                            ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Web Design</h3>
                                                    <div class="progress-value">
                                                        <span>
                                                            <?php echo $skills[0]['webdesign'];?>
                                                        </span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:<?php echo $skills[0]['webdesign'];?>%; background:#e73250;"></div>
                                                </div>
                                            </div> <!--Progress End -->
                                        <?php }
                                            }else{ ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Web Design</h3>
                                                    <div class="progress-value">
                                                        <span>0</span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:0%; background:#e73250;"></div>
                                                </div>
                                            </div> 
                                        <?php } ?>
                                    <!--Progress End -->

                                    <!-- Progress Start -->
                                        <?php if($skills){ 
                                            if($skills[0]['appdesign'] != 0){
                                            ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">App Design</h3>
                                                    <div class="progress-value">
                                                        <span>
                                                            <?php echo $skills[0]['appdesign'];?>
                                                        </span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:<?php echo $skills[0]['appdesign'];?>%; background:#e73250;"></div>
                                                </div>
                                            </div> <!--Progress End -->
                                        <?php }
                                            }else{ ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">App Design</h3>
                                                    <div class="progress-value">
                                                        <span>0</span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:0%; background:#e73250;"></div>
                                                </div>
                                            </div> 
                                        <?php } ?>
                                    <!--Progress End -->
                                        
                                    <!-- Progress Start -->
                                        <?php if($skills){ 
                                            if($skills[0]['graphics'] != 0){
                                            ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Graphics Design</h3>
                                                    <div class="progress-value">
                                                        <span>
                                                            <?php echo $skills[0]['graphics'];?>
                                                        </span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:<?php echo $skills[0]['graphics'];?>%; background:#e73250;"></div>
                                                </div>
                                            </div> <!--Progress End -->
                                        <?php }
                                            }else{ ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Graphics Design</h3>
                                                    <div class="progress-value">
                                                        <span>0</span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:0%; background:#e73250;"></div>
                                                </div>
                                            </div> 
                                        <?php } ?>
                                    <!--Progress End -->
                                        
                                    <!-- Progress Start -->
                                        <?php if($skills){ 
                                            if($skills[0]['illustration'] != 0){
                                            ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Illustrations</h3>
                                                    <div class="progress-value">
                                                        <span>
                                                            <?php echo $skills[0]['illustration'];?>
                                                        </span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:<?php echo $skills[0]['illustration'];?>%; background:#e73250;"></div>
                                                </div>
                                            </div> <!--Progress End -->
                                        <?php }
                                            }else{ ?>
                                            <div class="pro-gressbar-row">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">Illustrations</h3>
                                                    <div class="progress-value">
                                                        <span>0</span>%
                                                    </div>
                                                </div>
                                                
                                                 <div class="progress">
                                                    <div class="progress-bar" style="width:0%; background:#e73250;"></div>
                                                </div>
                                            </div> 
                                        <?php } ?>
                                    <!--Progress End -->
                                <?php }else{ ?>
                                    <div class="figimg-one no_img">
                                        <h3 class="head-b draft_no">
                                            Designer has not added any skills yet.
                                        </h3>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Add Designer -->
    <div class="modal fade" id="AddDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-nose" role="document">
            <div class="modal-content">
                
                <div class="cli-ent-model-box">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">
                            <h3 class="head-c text-center">Add Designer</h3>
                        </header>
                        <div class="noti-listpopup">
                            <div class="newsetionlist">
                                <div class="cli-ent-row tr notificate">
                                    <div class="cli-ent-col td" style="width: 30%;">
                                        <div class="cli-ent-xbox text-left">
                                            <h3 class="pro-head space-b">Designer</h3>
                                        </div>
                                    </div>
                                    
                                    <div class="cli-ent-col td" style="width: 45%;">
                                        <div class="cli-ent-xbox text-left">
                                            <h3 class="pro-head space-b">Skill</h3>
                                        </div>
                                    </div>
                                    
                                    <div class="cli-ent-col td" style="width: 25%;">
                                        <div class="cli-ent-xbox text-left">
                                            <h3 class="pro-head space-b text-center">Active Requests</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form action="" method="post">
                                <ul class="list-unstyled list-notificate">
                                    <?php foreach ($alldesigners as $value): ?>
                                    <li>
                                        <a href="#">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="radio-boxxes">
                                                        <label class="containerones">
                                                          <input type="radio" value="<?php echo $value['id'];?>" name="assign_designer" id="<?php echo $value['id'];?>">
                                                          <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                
                                                <div class="cli-ent-col td" style="width: 30%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="setnoti-fication">
                                                            <figure class="pro-circle-k1">
                                                            <?php if($value['profile_picture'] != ""){?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$value['profile_picture']?>" class="img-responsive">
                                                            <?php }else{ ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                            <?php } ?>
                                                            </figure>
                                                            
                                                            <div class="notifitext">
                                                                <p class="ntifittext-z1"><strong><?php echo $value['first_name'] ." ". $value['last_name'];?></strong></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="cli-ent-col td" style="width: 45%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                    </div>
                                                </div>
                                                
                                                <div class="cli-ent-col td" style="width: 25%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cli-ent-xbox text-center">
                                                            
                                                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $value['active_request'];?></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <?php endforeach; ?>
                                    
                                </ul>
                                <input type="hidden" name="request_id" id="request_id">
                                <p class="space-c"></p>
                                <p class="btn-x text-center"><button name="submit" type="submit" class="btn-g minxx1">Select Designer</button></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/bootstrap.min.js"></script>
    <script type="text/javascript">     
        // For Ajax Search
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    }
    var status = $.urlParam('status');
    $('#status_check #'+status+" a").click();
        $('#right_nav > li').click(function(){
            $(this).toggleClass('open');
        });
        $('.adddesinger').click(function(){
            var request_id = $(this).attr('data-requestid');
            var designer_id = $(this).attr('data-designerid');
            $('#AddDesign #request_id').val(request_id);
            $('#AddDesign input#'+designer_id).click();
        });
         $('#status_check li a').click(function(){
                var status = $(this).data('status');
                $('#status').val(status);
                $('#search_text').val("");
                ajax_call();
            });

            $('.searchdata').keyup(function(e){
               ajax_call();
            });
            $('.search_data_ajax').click(function(e){
                e.preventDefault();
                 ajax_call();
            });
            function ajax_call(){
                var search = $('#search_text').val();
                var status = $('#status').val();
                var userid = $('#userid').val();
                $.get( "<?php echo base_url();?>qa/dashboard/search_ajax_designer_project?title="+search+"&status="+status+"&id="+userid, function( data ){
                    var content_id = $('a[data-status="'+status+'"]').attr('href');
                   $( content_id ).html( data );  
                });
            }
    </script>