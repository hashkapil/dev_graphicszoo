<style type="text/css">
	body{
		background: #fafafa;
	}
</style>
<p class="space-b"></p>
	<section class="con-b">
		<div class="container">
			<h2 class="head-d text-left">My Profile</h2>   
			<p class="space-d"></p>
		<?php if($this->session->flashdata('message_error') != '') {?>				
			   <div class="alert alert-danger alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
				</div>
		   	<?php }?>
		  	<?php if($this->session->flashdata('message_success') != '') {?>				
			   <div class="alert alert-success alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<strong><?php echo $this->session->flashdata('message_success');?></strong>
				</div>
		   <?php }?>
			<div class="row">
				<div class="profile_pic_sec">
					<div class="col-md-4">
						<div class="project-row-qq1 settingedit-box">
							<div class="settrow">
								
								<div class="settcol right">
									<a class="review-circleww editprofile-info-pic" href="javascript:void(0);"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-edit.png" class="img-responsive"></a> 
								</div>
							</div>
							
							<div class="setimg-row33">
								<?php if($edit_profile[0]['profile_picture'] != ""){?>
								<figure class="setimg-box33">
									<img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$edit_profile[0]['profile_picture']; ?>" class="img-responsive telset33">
								</figure>
							<?php }else{ ?>
								<figure class="setimg-box33" style="background: #0190ff; text-align: center; font-size: 30px; color: #fff;padding-top: 57px;font-family: GothamPro-Medium;">
	                    			<?php echo ucwords(substr($edit_profile[0]['first_name'],0,1)) .  ucwords(substr($edit_profile[0]['last_name'],0,1)); ?>
	                			</figure>
	                		<?php } ?>
							</div>
							
							<p class="space-a"></p>
							
							<div class="form-group">
								<h3 class="sub-head-h text-center"><?php echo $edit_profile[0]['first_name']." ".$edit_profile[0]['last_name']?></h3>
							</div>
							
							<div class="form-group">
								<h3 class="head-c text-center"><?php echo $edit_profile[0]['email']; ?></h3>
							</div>
							
							<p class="seter"><span>Administrative Email</span></p>
							<p class="space-e"></p>
							<p class="chngpasstext"><a href="javascript:void(0)" class="upl-oadfi-le noborder" data-toggle="modal" data-target="#ChangePasswordModel"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/change-password.png" class="img-responsive"> <span>Change Password</span></a></p>
						</div>	
					</div>
				</div>
				<div class="edit_profile_pic_sec" style="display: none;">
					<div class="col-md-4">
						<div class="project-row-qq1 settingedit-box">
							<form method="post" enctype="multipart/form-data" action="" id="withpicture">
								<div class="settrow">
									
									<div class="settcol right">
										<p class="btn-x"><input type="submit" class="btn-e" value="Save" name="submit"></p> 
									</div>
								</div>
								
								<div class="setimg-row33">
									<div class="imagemain2" style="padding-bottom: 20px; display: none;">
									        <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="qaprofilepic" style="display:none;border: 2px" id="inFile"/>
									    </div>
									<figure class="setimg-box33 imagemain">
										<img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$edit_profile[0]['profile_picture']; ?>" class="img-responsive telset33">
										<a class="setimg-blog33" href="javascript:void(0);" onclick="$('.dropify').click();">
											<span class="setimgblogcaps">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-camera.png" class="img-responsive"><br>
												<span class="setavatar33">Change <br>Avatar</span>
											</span>
										</a>
									</figure>
									<figure class="setimg-box33 imagemain3" style="display: none;">
										<img src="" class="img-responsive telset33" id="image3">
										<a class="setimg-blog33" href="javascript:void(0);" onclick="$('.dropify').click();">
											<span class="setimgblogcaps">
												<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-camera.png" class="img-responsive"><br>
												<span class="setavatar33">Change <br>Avatar</span>
											</span>
										</a>
									</figure>
								</div>
								<p class="space-a"></p>
								<div class="form-group setinput">
									<input type="text" class="form-control input-b" placeholder="Shawn Alley" value="<?php echo $edit_profile[0]['first_name']." ".$edit_profile[0]['last_name']?>" name="fndlname">
								</div>
								
								<div class="form-group setinput">
									<input type="text" class="form-control input-b" placeholder="<?php echo $edit_profile[0]['email']; ?>" name="email" value="<?php echo $edit_profile[0]['email']; ?>">
									<p class="seter"><span>Administrative Email</span></p>
								</div>
								<p class="space-b"></p>
								<p class="chngpasstext"><a href="javascript:void(0)" class="upl-oadfi-le noborder" data-toggle="modal" data-target="#ChangePasswordModel"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/change-password.png" class="img-responsive"> <span>Change Password</span></a></p>
							</form>
						</div>	
					</div>
				</div>
				<div class="other_profile_info">
					<div class="col-md-4">
						<div class="project-row-qq1 settingedit-box">
							<div class="settrow">
								<div class="settcol left">
									<h3 class="sub-head-b">About Info</h3>
								</div>
								<div class="settcol right">
									<a class="review-circleww other_profile_info_btn" href="<?php echo base_url();?>qa/dashboard/editprofile"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-edit.png" class="img-responsive"></a>
								</div>
							</div>
							<p class="space-c"></p>
							<div class="form-group">
								<label class="label-x2">Paypal Id</label>
								<p class="space-a"></p>
								<div class="row">
									<div class="col-md-12">
										<p class="text-f"><?php echo $edit_profile[0]['paypal_email_address']; ?></p>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="label-x2">Title</label>
										<p class="space-a"></p>
										<div class="row">
											<div class="col-md-12">
												<p class="text-f"><?php echo $edit_profile[0]['title']; ?></p>
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label class="label-x2">Phone</label>
										<p class="space-a"></p>
										<div class="row">
											<div class="col-md-12">
												<p class="text-f"><?php echo $edit_profile[0]['phone']; ?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="label-x2">Address</label>
										<p class="space-a"></p>
										<div class="row">
											<div class="col-md-12">
												<p class="text-f"><?php echo $edit_profile[0]['address_line_1']; ?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-7">
									<div class="form-group">
										<label class="label-x2">City</label>
										<p class="space-a"></p>
										<div class="row">
											<div class="col-md-12">
												<p class="text-f"><?php echo $edit_profile[0]['city']; ?></p>
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-md-5">
									<div class="form-group">
										<label class="label-x2">Zip</label>
										<p class="space-a"></p>
										<div class="row">
											<div class="col-md-12">
												<p class="text-f"><?php echo $edit_profile[0]['zip']; ?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="label-x2">State</label>
										<p class="space-a"></p>
										<div class="row">
											<div class="col-md-12">
												<p class="text-f"><?php echo $edit_profile[0]['state']; ?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="label-x2">Timezone</label>
                                                                    <p class="space-a"></p>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <p class="text-f"><?php
                                                                                if (isset($edit_profile[0]['timezone'])) {
                                                                                    echo $edit_profile[0]['timezone'];
                                                                                }
                                                                                ?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
						</div>	
					</div>
				</div>
				<div class="edit_other_profile_info" style="display: none;">
					<div class="col-md-4">
						<div class="project-row-qq1 settingedit-box">
							<form method="post" action="<?php echo base_url(); ?>qa/dashboard/editqadetaiL" id="withoutpic">
								<div class="settrow">
									<div class="settcol left">
										<h3 class="sub-head-b">About Info</h3>
									</div>
									<div class="settcol right">
										<p class="btn-x"><input type="submit" class="btn-e" value="Save" name="saveother"></p> 
									</div>
								</div>
								<p class="space-d"></p>
								
									
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<input type="text" class="form-control input-c" placeholder="Paypal Id" value="<?php echo $edit_profile[0]['paypal_email_address']; ?>" name="paypalid">
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<input type="text" class="form-control input-c" placeholder="Title" value="<?php echo $edit_profile[0]['title']; ?>" name="title" >
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<input type="text" class="form-control input-c" placeholder="Phone" value="<?php echo $edit_profile[0]['phone']; ?>" name="phone">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<textarea class="form-control textarea-b" placeholder="Address" value="<?php echo $edit_profile[0]['address_line_1']; ?>" name="address"></textarea>
										</div>
									</div>
									
									<div class="col-sm-12">
										<div class="form-group">
											<input type="text" class="form-control input-c" placeholder="City" value="<?php echo $edit_profile[0]['city']; ?>" name="city">
										</div>
									</div>
									
									<div class="col-sm-12">
										<div class="form-group">
											<input type="text" class="form-control input-c" placeholder="State" value="<?php echo $edit_profile[0]['state']; ?>" name="state">
										</div>
									</div>
									
									<div class="col-sm-12">
										<div class="form-group">
											<input type="text" class="form-control input-c" placeholder="Zip" value="<?php echo $edit_profile[0]['zip']; ?>" name="zip">
										</div>
									</div>
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <select class="form-control input-c" name="timezone" placeholder="Timezone">
                                                                                <?php for ($i = 0; $i < sizeof($timezone); $i++) { ?>
                                                                                    <option value="<?php echo $timezone[$i]['zone_name']; ?>" <?php if ($edit_profile[0]['timezone'] == $timezone[$i]['zone_name']) {
                                                                                    echo 'selected';
                                                                                }
                                                                                    ?> ><?php echo $timezone[$i]['zone_name']; ?></option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
								</div>
							
							</form>
						</div>	
					</div>
				</div>
				
				<!--<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Billing and Subscription</h3>
							</div>
						</div>
						<p class="space-b"></p>
						<div class="settrow yss1">
							<div class="settcol left">
								<h3 class="sub-head-f">Current Plan</h3>
							</div>
							<div class="settcol right">
								<p class="setrightstst">Cycle: May 01,2018-June 01, 2018</p> 
							</div>
						</div>
						<p class="space-a"></p>
						
						<div class="cyclebox25">
							<div class="settrow">
								<div class="settcol left">
									<h3 class="sub-head-f">Monthly Golden Plan</h3>
									<p class="setpricext25">$259</p>
								</div>
								<div class="settcol right">
									<p class="btn-x"><button class="btn-f chossbpanss">Change Plan</button></p>
								</div>
							</div>
						</div>
						<p class="space-d"></p>
						
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Billing Method</h3>
							</div>
							<div class="settcol right">
								<p class="reviewsettle">
									<a class="review-circleww-plus" href="brief.html"><img src="images/icon-plus.png" width="15" class="img-responsive"></a>
									<a class="review-circleww" href="setting-edit.html"><img src="images/icon-edit.png" class="img-responsive"></a>
								</p>
							</div>
						</div>
						
						<p class="space-b"></p>
						
						<div class="form-group">
							<label class="label-x2">Card Number</label>
							<p class="space-a"></p>
							<div class="row">
								<div class="col-md-10">
									<p class="text-f">xxxx-xxxx-xxxx</p>
								</div>
							</div>
						</div>
						
						<div class="row inflexsss">
							<div class="col-sm-6 col-xs-6">
								<div class="form-group">
									<label class="label-x2">Expiry Date</label>
									<p class="space-a"></p>
									<p class="text-f">May 22</p>
								</div>
							</div>
							
							<div class="col-sm-3 col-sm-offset-1 col-xs-3">
								<div class="form-group">
									<label class="label-x2">CVC</label>
									<p class="space-a"></p>
									<p class="text-f">xxx</p>
								</div>
							</div>
							
							<div class="col-sm-2 col-xs-3">
								<p class="text-right rightdeletete"><a class="review-circleww" href=""><img src="images/icon-delete.png" class="img-responsive"></a></p>
							</div>
						</div>
						
						<div class="row inflexsss">
							<div class="col-sm-6 col-xs-6">
								<div class="form-group">
									<label class="label-x2">Name</label>
									<p class="space-a"></p>
									<p class="text-f">Name</p>
								</div>
							</div>
							
							<div class="col-sm-4 col-xs-4">
								<div class="form-group">
									<label class="label-x2">Zip Code</label>
									<p class="space-a"></p>
									<p class="text-f">654321</p>
								</div>
							</div>
							
							<div class="col-sm-2 col-xs-3">
								<p class="text-right rightdeletete"><a class="review-circleww" href="setting-edit.html"><img src="images/icon-edit.png" class="img-responsive"></a></p>
							</div>
							
						</div>
					</div>	
				</div> -->
				
			</div>
			
		</div>
	</section>
	<!-- Modal -->
	<div class="modal fade" id="ChangePasswordModel" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				
				<div class="cli-ent-model-box">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="cli-ent-model">
						<header class="fo-rm-header">
							<h3 class="head-c">Change Password</h3>
						</header>
						<div class="fo-rm-body">
							<form action="<?php echo base_url(); ?>qa/dashboard/change_password_old" method="post">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group goup-x1">
										<label class="label-x3">Current Password</label>
										<p class="space-a"></p>
										<input type="password" required class="form-control input-c" name="old_password">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group goup-x1">
										<label class="label-x3">New Password</label>
										<p class="space-a"></p>
										<input type="password" required class="form-control input-c" name="new_password">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group goup-x1">
										<label class="label-x3">Confirm Password</label>
										<p class="space-a"></p>
										<input type="password" required class="form-control input-c" name="confirm_password">
									</div>
								</div>
							</div>
							
							<p class="btn-x"><button type="submit" class="btn-g">Save</button></p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.editprofile-info-pic').click(function(e){
				e.preventDefault();
				$('.profile_pic_sec').hide();
				$('.edit_profile_pic_sec').css('display','block');
			});

			$('.other_profile_info_btn').click(function(e){
				e.preventDefault();
				$('.other_profile_info').hide();
				$('.edit_other_profile_info').css('display','block');
			});
		});

		function validateAndUpload(input){
			var URL = window.URL || window.webkitURL;
			var file = input.files[0];
			if(file){
				console.log(URL.createObjectURL(file));
				$(".imagemain").hide();
				$(".imagemain3").show();
				$("#image3").attr('src', URL.createObjectURL(file));
				$(".dropify-wrapper").show();
			}
		}
	</script>