
<div class="seo_front_wrapper">
<section class="seo_front_banner">
    <div class="container">
        <div class="seo-caption">
            <h1><strong>Graphic Design Services</strong>
                <span> Professional Design Services</span>
            </h1>
            <p>
            So how did the classical Latin become so incoherent? According to McClintock, a 15th century typesetter likely scrambled part of Cicero's De Finibus in order to provide placeholder text to mockup various fonts for a type specimen book.
            </p>
            <p>
It's difficult to find examples of lorem ipsum in use before Letraset made it popular as a dummy text in the 1960s, although McClintock says he remembers coming across the lorem ipsum passage in a book of old metal type samples. So far he hasn't relocated where he once saw the passage, but the popularity of Cicero in the 15th century supports the theory that the filler text has been used for centuries.
            </p>
            <a class="seo_btns btn" href="#">Get Started Now</a>
        </div>
    </div>
</section>
<section class="seo_trusted">
    <div class="container">
    <h3 class="seo_sub_heading">Trusted by companies</h3>
    <ul class="all-logo">
                <li><img alt="goat grass" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/goat-grass.png">
                </li>
                <li><img alt="Bread_Logo_Retina" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Bread_Logo_Retina.png">
                </li>
                <li><img alt="enicode" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/enicode.png">
                <li><img alt="western-rockies" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/western-rockies.png" >
                </li>
                <li><img alt="green-gurd" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd.png" >
                </li>
                <li><img alt="perfect-imprents" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/perfect-imprents.png" >
                </li>
                <li><img alt="ourbookkiper" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ourbookkiper.png" >
                </li>
              </ul>
    </div>
</section>
<?php 
// echo "<pre>"; print_r($cat_data); die; 
    
foreach ($cat_data as $k => $catinfo) { ?>

<section class="catg_grid">
    <div class="container">
    <h3 class="seo_sub_heading"><?php echo $catinfo['name'];?>
<p><?php echo $catinfo['meta_description'];?>
</p>
    </h3>
 <div class="container">
     <div class="row">
        <?php foreach($catinfo['child'] as $i => $childCat) { 

                    $cls = "";  
                    if($childCat['name']== "gif"){
                        $cls = "alr-gifclas";
                    }

                     if(empty($childCat['slug'])) { 
                        $slugUrl = 'javascript:void(0);'; 
                        $target = "";
                     }else{ 
                        $slugUrl = base_url().'category/'.$childCat['slug']; 
                        $target = '';
                     }  
                            // echo "<pre>"; print_r($catinfo); echo "</pre>"; 
                    ?>
        <div class="col-md-2 catg_gird_list">
            <a href="<?php echo $slugUrl; ?>">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS.'img/customer/customer-images/'.$childCat['image_url']?>">                
            <span><?php echo $childCat['name'];?></span>
            </a>
        </div>
    <?php } ?>
        
 </div>
    </div>
</section>
<?php } ?>


<!-- <section class="catg_grid catg_grid_bg">
    <div class="container">
    <h3 class="seo_sub_heading">Website & App
<p>Get your very own logo design through our custom logo design tool. Design your own logo or ask our expert designers to create graphic design logo for your business.
</p>
    </h3>
 <div class="container">
     <div class="row">
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
        <div class="col-md-2 catg_gird_list">
            <a href="#">
            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/icon_Screenprint.svg">                
            <span>Outsource Graphic</span>
            </a>
        </div>
     </div>
 </div>
    </div>
</section> -->

<section class="how_it_work">
    <div class="container">
    <h3 class="seo_sub_heading">How it works
        <p>Checkout our simple and fast work flow</p>
    </h3>
 <div class="container">
     <div class="row">
        <div class="col-md-4">
        <div class="how_work_step">
        <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/submit-project.png">
            <h3>Submit Project </h3>
            <p>Create a new project by simply adding your design details such as name, description, file formats you would like to receive, and any examples you may have.</p>
        </div>
        </div>
        <div class="col-md-4">
        <div class="how_work_step">
        <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/approve-download.png">
            <h3>Feedback and Revisions </h3>
            <p>Once you receive your initial drafts let us know if there are any revisions you need by clicking anywhere on the design to put a feedback comment. Do this as many times as you need.</p>
        </div>
        </div>
        <div class="col-md-4">
        <div class="how_work_step">
        <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/submit-rq.png">
            <h3>Approve and Download </h3>
            <p>When you get the design as you had imagined, just click approve and download the source files requested.</p>
        </div>
        </div>
     </div>
 </div>
 <div class="text-center">
 <a class="seo_btns btn" href="#">Get Started Now</a>
 </div>
    </div>
</section>


<section class="seo_review_sec">
    <div class="container">
    <h3 class="seo_sub_heading">Client Reviews
        <p>Get your very own logo design through our custom logo design tool. Design your own logo or ask our expert designers to create graphic design logo for your business.
</p>
    </h3>
    <div class="seo__review">
        <div class="seo_review_item">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proidentDuis aute irure dolor in reprehenderit in voluptate.</p>
                <div class="seo_client">
                    <figure>
                        <img alt="client_img_a" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/seo_front/client_img_a.jpg">
                    </figure>
                    <em>Client Name</em>
                </div>
        </div>
        <div class="seo_review_item">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proidentDuis aute irure dolor in reprehenderit in voluptate.</p>
                <div class="seo_client">
                    <figure>
                        <img alt="client_img_a" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/seo_front/client_img_a.jpg">
                    </figure>
                    <em>Client Name</em>
                </div>
        </div>
        <div class="seo_review_item">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proidentDuis aute irure dolor in reprehenderit in voluptate.</p>
                <div class="seo_client">
                    <figure>
                        <img alt="client_img_a" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/seo_front/client_img_a.jpg">
                    </figure>
                    <em>Client Name</em>
                </div>
        </div>
    </div>
    </div>
</section>
<section class="join_seo_customer">
    <div class="container">
        <div class="inner_seo_caption">
            <h2>Join more than 500+ customers</h2>
            <span>The one thing which, as an organization, has been picking on our operations is managing our clients’ project database. </span>
            <a class="seo_btns btn" href="#">Get Started Now</a>            
        </div>
    </div>
</section>
</div>
<script>
jQuery( document ).ready(function() {
    jQuery('.seo__review').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
    });
});
</script>