<!--==========================
Hero Section
============================-->
<section id="hero_section" class="wow fadeIn">
	<div class="hero-container">
		<div class="container">
			<div class="row">
				<div class="col-md-5 mt-5 pt-5 pl-4">
					<h2>Earn monthly residual income effortlessly</h2>
					<a href="#get-started" class="btn-get-started scrollto"><b>Apply to become an affliate</b></a>
				</div>
				<div class="col-md-7 text-right">
					<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/affiliate-hero.png" alt="affiliate-hero" style="max-width:100%;" >
				</div>
			</div>
		</div>
	</div>
</section><!-- #hero -->
  
<section id="top-blocks" class="nopadding padd-section">
  <div class="container-fluid nopadding">
    <div class="row">
      <div class="col-md-4 nopadding lb-bg">
		<div class="padding-arround">
			<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/commission.png" alt="commission">
			<h4>High commission</h4>
			<p>Earn 10 - 20% monthly commission for every referral for the lifetime of that client</p>
		</div>
      </div>
	  <div class="col-md-4 nopadding rd-bg">
		<div class="padding-arround">
			<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/easy-enroll.png" alt="easy-enroll">
			<h4>Easy to enroll</h4>
			<p>Earn 10 - 20% monthly commission for every referral for the lifetime of that client</p>
		</div>
      </div>
	  <div class="col-md-4 nopadding db-bg">
		<div class="padding-arround">
			<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/easy-manager.png" alt="easy-manage">
			<h4>Easy to manage</h4>
			<p>Earn 10 - 20% monthly commission for every referral for the lifetime of that client</p>
		</div>
      </div>
    </div>
  </div>
</section><!-- #who we are-->
<div class="container text-center">
	<div class="row mb-5">
		<div class="col-md-12"><button class="button big-btn" style="font-size: 1.3em;"><b>APPLICATION FORM</b></button></div>
	</div>
</div>

<!--==========================
    Our Values
  ============================-->
<section id="our-values" class="padd-section text-center">
    <div class="container">
      <div class="section-title text-center">
        <h2>Benefits of being an affiliate</h2>
      </div>
    </div>
	<div class="container">
		<div class="row col-md-10" style="margin:0 auto;">
			<div class="col-md-4 benefit-wrap border-left-0 border-top-0 border-bottom-0 ">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/no-fees.png" class="img-responsive benefit-icon" alt="no-fees">
				<h4>No Fees</h4>
				<p>Our affiliate program is 100% free to enroll and earn.</p>
			</div>
			<div class="col-md-4 benefit-wrap border-left-0 border-top-0 border-bottom-0 ">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/unlimited-earnin.png" class="img-responsive benefit-icon" alt="unlimited-earnin">
				<h4>Unlimited earning potential</h4>
				<p>Earn 10 - 20% every month for every referral with no earning cap. The more you refer, the more you earn.</p>
			</div>
			<div class="col-md-4 benefit-wrap border-left-0 border-top-0 border-bottom-0 border-right-0">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/tools.png" class="img-responsive benefit-icon" alt="tools">
				<h4>Tools for success</h4>
				<p>We’ve included various marketing tools to ensure your success.</p>
			</div>
			<div class="col-md-4 benefit-wrap border-left-0 border-bottom-0 ">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/analytic.png" class="img-responsive benefit-icon" alt="analytic">
				<h4>Build-in analytics</h4>
				<p>Easy-to-use platform so you can easily manage and track your referrals, earnings, and analytics.</p>
			</div>
			<div class="col-md-4 benefit-wrap border-left-0 border-bottom-0 ">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/payout.png" class="img-responsive benefit-icon" alt="payout">
				<h4>Earn payout</h4>
				<p>We’ve made it easy for you to receive your earnings. Just one click to transfer funds to an account of your choice.</p>
			</div>
			<div class="col-md-4 benefit-wrap border-left-0 border-bottom-0 border-right-0">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/monthly-income.png" class="img-responsive benefit-icon" alt="monthly-income">
				<h4>Passive monthly income</h4>
				<p>Earn when someone signs up. And continue earning for the duration of their stay.</p>
			</div>
		</div>
	</div>
</section>
<section class="nopadding">
  <div class="container-fluid nopadding">
    <div class="row mh-mh">
      <div class="col-md-6 nopadding">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/man.png" width="100%" alt="man">
      </div>

      <div class="col-md-6 align-self-center affiliate_try_graphics">
        <h2>Earning Potential with<br> GraphicsZoo</h2>
        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
        <p style="color:#ffb70e; font-size: 12px; margin-bottom: 0px;">Graphicszoo affiliate partner,</p>
        <p style="font-weight: 700; font-size: 19px;">$1200 - $1500/month</p>
        <p>Apply today to become an affiliate</p>
        <button class="button button1">APPLICATION FORM</button>
      </div>
    </div>
  </div>
</section><!-- #Try Graphics -->
