﻿<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sign Up | <?php echo $page_title; ?></title>
    <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo $custom_favicon; ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,600i,700,700i,800,800i,900,900i" rel="stylesheet canonical">
    <link rel="stylesheet canonical" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/style.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/custom.css">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet canonical">
	<?php $CI =& get_instance();
	$CI->load->library('myfunctions');
	$this->load->view('front_end/variable_css');  ?>    
<style>
body{
    font-family: 'Poppins', sans-serif !important;
}
    .epassError.disc-code p.alert.alert-danger {
        background: transparent;
        border: none;
        padding: 0;
        margin: 10px 0;
        color: #e8304d;
    }
    .sing-up-heading {
        padding-bottom: 0;
    }
    div#logo img {
        max-width: 200px;
        max-height: 100px;
    }
    span.CouponSucc_code {
        color: #214a80;
        margin-top: 9px;
        display: block;
    }
    span.CouponErr_code {
        color: #e8304d;
        margin-top: 9px;
        display: block;
    }
    .grey-price{color:#828d99;}
    .subHeading {
        font-size: 18px !important;
        text-transform: capitalize;
        font-weight:600 !important;
    }
    select#pricebasedquantity {
        width: 41px;
        -webkit-appearance: none;
        margin-top: 20px;
    }
    .first-Ul {
        color: #253858;
        font-size: 12px;
        font-weight: 500;
        line-height: 22px;
    }
    div#logo {
        text-align: center;
        margin-bottom: 24px;
    }
    
    .bill-infoo {

        width: auto; 
        box-shadow: none; 
        margin-left: 0px; 
    }
    .sign-here .both-outer {
        background: #fff;
        padding: 20px;
        border-radius: 12px;
        max-width: 400px;
        box-shadow: 0 0 30px #ccc;
        margin: auto;
    }
    h1 {
        font-size: 40px;
        font-weight: 600;
        margin-bottom: 15px;
        margin-bottom: 50px;
    }
    .subdomain {
        max-width: 100%;
        margin: auto;
        width: 100%;
        background: #fff;
        }
.similar-prop .modal-dialog {
    max-width: 760px;
    width: 100% !important;
    margin:20px auto !important;
    height: 100%;
}
    .similar-prop .modal-dialog .modal-content {
    width: 100%;
    border-radius: 0;
}
h2.popup_h2 {
    font-size: 16px;
    color: #0d0d0d;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
}
header.fo-rm-header .cross_popup {
    position: absolute;
    right: 20px;
    top: 13px;
    font-size: 20px;
    opacity: 1;
    cursor: pointer;
    font-weight: 400;
}
header.fo-rm-header {
    padding: 20px;
    box-shadow: 0 2px 5px rgba(152, 167, 176, 0.5);
}
.conditions_text, .privacy_text{
    max-height: 400px;
    overflow-y: auto;
    white-space: normal;
        word-break: break-all;
}
.subdomain_signup {
    display: flex;
    height: 100%;
}

p.no_subs {
    text-align: center;
    font-weight: bold;
    padding: 10px 0;
}
.parsonial-plan .par-out .monthly-activate {
    margin: 12px 0;
}
.par-out .select-bx-p {
    margin-bottom: 20px;
}
.parsonial-plan .par-out select {
    background-position: 96%;
    padding: 0px 20px;
    background-color: #fff;
    cursor: pointer;
    border-radius: 50px;
    font-weight: 600;
}
.total-pPrice p:empty, .monthly-activate:empty {
    margin: 0 !important;
    display: none;
}
.parsonial-plan .par-out select:focus {
    outline: none;
}
.par-out .monthly-activate ul {
    margin: 0;
}
.monthly-activate:empty + .total-pPrice {
    margin-top: 35px;
}
.monthly-activate ul li {
    padding-left: 0;
    text-align: left;
    color: #333333;
}
.subdomain_signup .parsonial-plan {
    padding: 60px 0;
    height: 100%;
    width: 100%;
    border-right: 0;
    background: #f3f3f3;
    display:flex;
}
.inner-plan p {
    font-size: 14px;
}
.inner-plan {
    margin: auto;
    max-width: 480px;
    width: 100%;
}
.par-out .monthly-activate {
    font-size: 14px;
    color: #727c83;
    text-align: left;
    list-style-type: none;
    line-height: 32px;
}
.monthly-activate ul li {
    padding: 0 0 0 14px;
    line-height: 32px;
}
 .pb-info {
    margin-bottom: 20px;
    padding:60px;
    margin: auto;
}
.monthly-activate ul li::before {
    content: "";
    height: 6px;
    width: 11px;
    border-width: 0px 0px 2px 2px !important;
    position: absolute;
    border: solid #0c3147;
    transform: rotate(-45deg);
    left: 0px;top: 12px;
    }
.card-sec a.create_account_link {
    margin-top: 0;
}
.c-signup button {
    width: auto;
    margin-top: 0;
    border-radius: 50px;
    text-transform: capitalize;
    line-height: 40px;
    padding: 0 30px;
    letter-spacing: 1px;
    font-size: 15px;
}
.subdomain_signup > div {
    padding: 0;
}

.maintain-wrap {
    max-width: 500px;
    margin: 45px auto 0;
    border: 1px solid #ccc;
    border-radius: 8px;
    padding: 40px;
}

.maintain-wrap img {
    /* width: 80px; */
    opacity: 0.8;
}

.maintain-wrap p {
    margin: 0;
    font-size: 18px;
    font-weight: 600;
}
.maint-img {
    width: 100px;
    height: 100px;
    margin: -91px auto 25px;
    border: 1px solid #ccc;
    padding: 20px;
    border-radius: 50px;
    background: #fff;
}
.pb-info input.form-control {
    font-size: 15px;
    padding: 12px;
    border-radius: 4px;
    margin: 0;
}
p.plan-price {
    font-weight: 600;
    font-size: 18px;
}
.g-total h3 {
    font-weight: 600;
}
.pb-info h2 {
    font-size: 26px;
    margin: 0 0 50px;
    position: relative;
    padding-bottom: 15px;
}
.pb-info h2::before, .testimonial-signup h2::before {
    content: "";
    width: 54px;
    height: 3px;
    position: absolute;
    background: #e42547;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    }
    .testimonial-signup {
    padding: 70px 0;
}
    .testimonial-signup h2{
        position:relative;
        padding-bottom: 15px;
    }
.pb-info .terms-txt {
    font-size: 14px;
    color: #212529;
}

.c-signup span {
    display: block;
    font-size: 14px;
    letter-spacing: 0;
    margin-top: 15px;
}
.card-sec img.img-fluid.stripe-lock {
    margin-top: 5px;
    margin-left: 0;
}
.inner-plan #logo a {
    margin-top: 0;
}
.offer-total {
    margin-top: 10px;
}
.offer-total h3 {
 color:#e42547;
}
.inner-review {
    display: flex;
}
.client-rev-image span {
    height: 105px;
    width: 105px;
    display: flex;
    border-radius: 50px;overflow: hidden;
    box-shadow: 0px 0px 51px -15px #ccc;
}
.client-rev-image span img {
    margin: auto;
    max-height: 100%;
    max-width: 100%;
}
.client-rev-image name {
    font-weight: bold;
    color: #000000;
    display: inline-block;
    margin: 10px 0 2px;
}
.review-line {
    margin: auto;
    padding: 0 40px;
}

.review-line p {
    font-weight: normal;
    font-size: 16px;
    letter-spacing: 0px;
    margin-bottom: 0;
    position: relative;
}
.review-line p::before, .review-line p::after {
    content: "";
    width: 22px;
    height: 14px;
    background: url(public/assets/front_end/Updated_Design/img/quote-ico.png);
    position: absolute;
    top: -17px;
    left: -3px;
}
.review-line p::after {
    bottom: 0;
    top: auto;
    right: 0;
    left: auto;
    transform: rotate(180deg);
}
.parsonial-plan .par-out {
    padding: 0 20px;
}

@media only screen and (min-width:768px) and (max-width:1024px) {
    .subdomain_signup .parsonial-plan {
    padding: 10px 0;
    }
    .pb-info h2 {
    font-size: 20px;
    margin: 0 0 20px;
    }
    .inner-plan {
    padding: 0 15px;
    }
    .pb-info {
    padding: 0 15px;
    }
    .card-sec .text-right {
    text-align: left !important;
    }
    div#logo img {
    width: 100px;
    }
    .testimonial-signup {
    padding: 70px 0;
    margin: 0;
    flex: 0 0 100% !important;
    max-width: 100%;
    padding: 70px 40px;
    }
    .testimonial-signup h2 {
    font-size: 24px;
    }
}
@media only screen and (max-width:767px){
    div#logo img {
    width: 80px;
    }
    .client-rev-image name {
    display: block;
    }
    .review-line p {
    font-size: 14px;
    }
    .review-line {
    margin: 15px auto 0;
    padding: 0 40px;
    }
    .client-rev-image span {
    height: 75px;
    width: 75px;
    margin: 20px auto 0;
    }
    .client-rev-image {
    margin: auto;
    }
    .inner-review {
    flex-wrap: wrap;
    text-align: center;
    }
    .inner-review {
    flex-wrap: wrap;
    text-align: center;
    }
    .testimonial-signup h2 {
    font-size: 22px;
    }
    .inner-plan {
    padding: 0 15px;
    }
    .testimonial-signup {
    padding: 30px 0;
    margin: 0;
}
 .subdomain_signup .pb-info {
    padding: 0 20px;
 }
 .card-sec .text-right {
    text-align: center !important;
}
 .subdomain_signup .pb-info .form-group {
    margin-bottom: 15px;
}
    .subdomain_signup > div {
    clear: left;
    }
    .pb-info h2 {
    font-size: 20px;
    margin: 0 0 30px;
 }
    .subdomain_signup .parsonial-plan {
    padding: 30px 0;
    }
}
</style>
<?php
    if (!empty($client_script)) {
        /***Start client script before close head tag***/
        foreach ($client_script as $k => $s_vl) {
            if (($s_vl['show_only_specific_page'] == "signup" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_head") {
                if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                    echo base64_decode($s_vl["tracking_script"]);
                } else {
                    ?>
                    <script>
                    <?php echo base64_decode($s_vl["tracking_script"]); ?>
                    </script>
                    <?php
                }
            }
        }
        /***End client script before close body tag***/
    }
?>
</head>
<body>

    <section class="sing-up-sec">
        
                       <div class="subdomain_signup">
                           	<div class="col-md-6">
                            <div class="parsonial-plan">
                            	<div class="inner-plan">
                                <div id="logo" class="sign-logo pull-center">
                                    <a href="<?php echo base_url(); ?>">
                                    <!-- <img src="<?php echo $custom_logo; ?>" alt="<?php echo $page_title; ?>" title="<?php echo $page_title; ?>" /> -->
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/proof-logo.png" class="img-fluid stripe-lock" alt="proof">
                                    </a>
                                </div>
                            
                                <p class="text-center">14 days free trial and actual amount will debited after end of the trial period. You can cancel anytime in between</p>
                            
                                <div class="par-out">
                                     <?php if(!empty($subscription_list)){ ?>
                                    <div class="select-bx-p">
                                        <div class="sub_custom-select" >
                                            <select  id="priceselector" >
                                                <?php
                                                    foreach ($subscription_list as $subscription_k => $subscription_v) {
                                                        echo '<option value="' . $subscription_v['plan_id'] . '" data-quantity="' . $subscription_v['global_inprogress_request'] . '" data-amount="' . $subscription_v['plan_price'] . '">' . $subscription_v['plan_name'] . '</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="monthly-activate"></div>

                                    <div class="total-pPrice">
                                        <p class="plan-price">
                                            Plan Price:
                                            <span id="change_prc" class="box item1">$
                                                <span></span>
                                            </span>
                                        </p>
                                        <p class="plan-price tax_prc_for_texas"></p>

                                        <hr>
                                        <div class="g-total">
                                            <h3>Grand Total <span id="grand_total">$<span></span></span>
<!--                                                <i class="fas fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-original-title="This role manage everything according to permissions given to him by you."></i>-->
                                            </h3>
                                        </div>
                                        <div class="g-total offer-total">
                                            <h3>Billed Today <span id="grand_total">$0</h3>
                                        </div>
                                        <span class="coupon-des-newsignup"></span>
                                    </div>
                                    <?php }else{
                                        echo "<p class='no_subs'>No Subscription</p>";
                                    } ?>
                                </div>
                               </div> 
                            </div>
                             </div>
                             <div class="col-md-6">
                            <div class="pb-info">
                            <h2 class="text-center"> <strong>Sign up for Free Today!</strong></h2>
                                <div class="alert alert-danger alert-dismissable signp_err" style="display:none">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                    <strong></strong>
                                </div>
                                <?php if ($this->session->flashdata('message_success') != '') { ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                                    </div>
                                <?php } ?>
                                <form action="" method="post" role="form" class="saas_signup" id="saas_signup">
                                    <input type="hidden" value=""  name="plan_name" id="plan_name">
                                    <input type="hidden" id="plan_price" name="plan_price" value=""/>
                                    <input type="hidden" id="couponinserted_ornot" class="couponinserted_ornot" name="couponinserted_ornot" value=""/>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text"  name="first_name"  id="fname" class="form-control" data-rule="minlen:4" data-msg="Please enter First Name" required="" placeholder="First Name *">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="last_name" class="form-control" id="lname" placeholder="Last Name *"  data-msg="Please enter Last Name" required="">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" pattern="\d*" name="phone_number" class="form-control" id="phone_number" placeholder="Phone Number *"  maxlength="10" data-msg="Please enter Phone Number" required="">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group"> 
                                                    <input type="email"   name="email" class="form-control" id="email" placeholder="Email *" data-rule="email" data-msg="Please enter a valid email" required="">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="password" name="password" class="form-control" placeholder="Password *" data-rule="minlen:4" data-msg="Please enter the password" required="" >
                                                </div>
                                            </div>
                                        </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" pattern="\d*" class="form-control" name="card_number" placeholder="Card Number *" required="" maxlength="16">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" id="expir_date" name="expir_date" class="form-control" placeholder="MM  /  YY *" onkeyup="dateFormat(this.value);" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 cvv">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" pattern="\d*" name="cvc" class="form-control" placeholder="CVV *" maxlength="4" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 coupon_field">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control disc-code" name="Discount" placeholder="Discount Code"  data-msg="Please enter a valid email">
                                            </div>
                                            <div class="ErrorMsg epassErrorSuccess disc-code">
                                            </div>
                                        </div>

                                    </div>
                                    </div>
                                    <div class="" id="chk-box-pr">
                                        <div class="form-group">
                                            <div class="form-radion2">
                                                    <p class="terms-txt">
                                                        By clicking the below button, you agree to our Terms and that you have read our Data Use Policy, including our Cookie Use.
                                                    </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="f_loader_sgnup"></div>
                                    <div class="text-center ">
                                        
                                    </div>
                                    <div class="card-sec row">
                                         <div class="col-lg-8  col-12 text-left c-signup">
                                         <button type="submit" name="submit" id="submit" class="red-theme-btn">Register</button> 
                                         <span>Already have an account? <a href="<?php echo base_url().'login/' ?>" class="create_account_link">Login</a></span>
                                            </div>
                                        <div class="col-lg-4  col-12 text-right">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/stripe-lock.png" class="img-fluid stripe-lock" alt="stripe-lock">
                                       
                                        </div>
                                       
                                      </div>
                                  </form>
                              </div>
                          	</div>
                          </div>
              </div>
              <div class="offset-2 col-md-8 testimonial-signup">
                    <h2 class="text-center"><strong>PROOF from our Clients</strong></h2>

                    <div class="inner-review">
                        <div class="client-rev-image">
                            <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/client-image.jpg" class="img-fluid stripe-lock" alt="client-image"></span>
                            <name>Susan Bill</name>
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/five-star.png" class="img-fluid stripe-lock" alt="five-star">
                            
                        </div>
                        <div class="review-line">
                        <p>Managing graphic designing projects can be extremely tricky and we found PROOF to be a very user friendly tool. It is easy to navigate and makes it simpler to manage the database of all our projects. Highly recommended. </p>
                        </div>
                    </div>
              </div>
  </section>
<?php
if (!empty($client_script)) {
    /***Start client script before close head tag** */
    foreach ($client_script as $k => $s_vl) {
        if (($s_vl['show_only_specific_page'] == "signup" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_body") {
            if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                echo base64_decode($s_vl["tracking_script"]);
            } else {
                ?>
                <script>
            <?php echo base64_decode($s_vl["tracking_script"]); ?>
                </script>
                <?php
            }
        }
    }
    /***End client script before close body tag***/
}
?>
</body>

<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- REFERSION TRACKING: BEGIN -->
<script rel="nofollow" src="//graphicszoo.refersion.com/tracker/v3/pub_01537b7d8dcde95d5a0d.js"></script>
<script>_refersion();</script>
<!-- REFERSION TRACKING: END -->
<!-- UTM Tracking: BEGIN -->
<script rel="nofollow" src="https://d12ue6f2329cfl.cloudfront.net/resources/utm_form-1.0.4.min.js" async></script>
<script type="text/javascript" charset="utf-8">
    !function () {
                var t;
                if (t = window.driftt = window.drift = window.driftt || [], !t.init)
                    return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
                            t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
                            t.factory = function (e) {
                                return function () {
                                    var n;
                                    return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                                };
                            }, t.methods.forEach(function (e) {
                        t[e] = t.factory(e);
                    }), t.load = function (t) {
                        var e, n, o, i;
                        e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
                                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
                                n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
                    });
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('d6yi38hkhwsd');
    var _uf = _uf || {};
    _uf.utm_source_field = "YOUR_SOURCE_FIELD"; // Default 'USOURCE'
    _uf.utm_medium_field = "YOUR_MEDIUM_FIELD"; // Default 'UMEDIUM'
    _uf.utm_campaign_field = "YOUR_CAMPAIGN_FIELD"; // Default 'UCAMPAIGN'
    _uf.utm_content_field = "YOUR_CONTENT_FIELD"; // Default 'UCONTENT'
    _uf.utm_term_field = "YOUR_TERM_FIELD"; // Default 'UTERM'
    _uf.sessionLength = 2; // In hours. Default is 1 hour
    _uf.add_to_form = "first"; // 'none', 'all', 'first'. Default is 'all'

    //UTM Tracking: END
var BASE_URL = "<?php echo base_url(); ?>";
var assetspath = "<?php echo FS_PATH_PUBLIC_ASSETS ?>";
var sales_tax = "";
$plan_price = '';
$from = 'sub';
$discount_amount = 0;
$tax_amount = 0;
$tax_value = 0;
$discount_notes = '';

if($('.monthly-activate').length > 0){ 
    var setSelected = jQuery("#priceselector").val();
    var setAmount = jQuery("#priceselector").find(':selected').data('amount');
    Setselectedvalue(setSelected,setAmount); 
}

function Setselectedvalue(selectedval,setAmount) {
        $plan_price = setAmount;
        var user_id = "<?php echo $user_id; ?>";
        jQuery('#plan_name').val(selectedval);
        jQuery('#plan_price').val(setAmount);
        jQuery('#change_prc span').html(setAmount);
        jQuery('#grand_total span').html(setAmount);
        jQuery('.discount-amont-sec').css('display','none');
        jQuery('.disc-code').val('');  
        jQuery('.ErrorMsg').css('display','none');
        jQuery('.coupon-des-newsignup').css('display','none');
        $.ajax({
            type: 'POST',
            url: BASE_URL+'welcome/getsaas_subscriptions',
            data: {planid: selectedval,user_id:user_id},
            dataType: 'json',
            success: function (data) {
                
                if(data[0]){
                    var features = data[0].features;
                    var feature_array = features.split("_");
                    features_html = ""
                    if(feature_array != ""){
                       
                    features_html = "<ul>";
                    $.each(feature_array,function(k,val){
                        features_html += "<li>"+val+"</li>";
                    });
                    features_html += "</ul>";
                    }
                    if(data[0].apply_coupon == 0){
                       $(".coupon_field").css("display","none");
                    }else{
                        $(".coupon_field").css("display","block");
                    }
                    jQuery('.monthly-activate').html(feature_array);
                    jQuery('#plan_type_name').val(data[0].plan_type_name);
                    jQuery('#couponinserted_ornot').val(data[0].apply_coupon);
                    jQuery('#in_progress_request').val(data[0].global_inprogress_request);
                    
                }
            }
        });
    var discount_empty =  jQuery('.disc-code').val();
        if(discount_empty == ''){
           $('.ErrorMsg').css('display','none');
            $discount_amount = 0;
            $discount_notes = '';
           return; 
        }
}
jQuery('#priceselector').on('change', function (e) {
    var getSelectedPrice = $(this).val();
    var getAmount = $(this).find('option:selected').attr("data-amount");
  Setselectedvalue(getSelectedPrice,getAmount);
});
function dateFormat(val) {
    if (val.length == 2) {
        $('#expir_date').val($('#expir_date').val() + '/');
    }
}
$(document).on('blur','.disc-code',function(e){
            e.preventDefault();
            var dicountCode = $('.disc-code').val();
            var apply_couponor_not = $("#couponinserted_ornot").val();
            var setSelected = $("#priceselector").find(':selected').data('amount');;
        $("span.CouponSucc_code, span.CouponErr_code").remove();
        if(dicountCode == ''){
            $('.ErrorMsg').css('display','none');
            $discount_amount = 0;
            $discount_notes = '';
            showPriceSectionHTML();
           return;
       }
       else{
        $('.ErrorMsg').css('display','block');
    }
    $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponSucc_code"><img src="'+assetspath+'img/ajax-loader.gif" alt="loading"/></span>');
    delay(function(){
        $.ajax({
            type: 'POST',
            url: BASE_URL+'welcome/CheckCouponValidation',
            data: {dicountCode: dicountCode,apply_couponor_not:apply_couponor_not},
            dataType: 'json',
            success: function (data) {
                  $('.ErrorMsg').css('display','block');
                  $('.couponinserted_ornot').val(data.inertcoupon);
                  if (data.status == 1) {
                      if(data.return_data.valid == '1'){
                        if(data.return_data.amount_off !== null){
                            var str = data.return_data.amount_off;
                            var resStr = str/100;
                            $discount_amount = resStr;                           
                        } else{
                            var TotalPercent = data.return_data.percent_off;
                            var calcPrice  =  setSelected * (TotalPercent / 100 );
                            $discount_amount = calcPrice;
                       }
                       $('.CouponErr_code').css({"display": "none"});  
                       $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponSucc_code"><i class="fas fa-check-circle"></i> Coupon Applied</span>');
                    var discount_notes = '';                  
                    if(data.return_data.duration == 'forever'){
                        discount_notes = '<p>Forever</p>';
                    }else if(data.return_data.duration == 'once'){
                        discount_notes = '<p>One Time Only</p>';
                   } else{
                        discount_notes = '<p>First ' + data.return_data.duration_in_months + ' Months Only</p>';
                    }
                   $discount_notes = discount_notes;
               }else{
                    $('.CouponSucc_code').css({"display": "none"});
                    $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponErr_code"><i class="fas fa-times-circle"></i>This coupon is not valid</span>');
                    $discount_amount = 0;
                    $discount_notes = '';
               }
               showPriceSectionHTML();
           }
           else{
                showPriceSectionHTML();
                $('.CouponSucc_code').css({"display": "none"});
                $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponErr_code"><i class="fas fa-times-circle"></i> ' + data.message + '</span>');
                $discount_amount = 0;
                $discount_notes = '';
           }
       }
   });  
    }, 1000 );
    

});
 var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
})();
function showPriceSectionHTML(){
    $plan_price = parseFloat($("#priceselector").find(':selected').data('amount'));
    $discount_amount = parseFloat($discount_amount);
    var total_price = ($plan_price - $discount_amount);
    if(total_price % 1 !== 0){
        total_price = total_price.toFixed(2);
    }
    var htmll = '<p class="plan-price">Plan Price:<span id="change_prc" class="box item1">$<span>'+$plan_price+'</span></span></p>';
    if(parseFloat($discount_amount) > 0){
        htmll += '<p class="plan-price discount-amont-sec">Discount: <span class="box item12"><span>- $'+$discount_amount.toFixed(2)+'</span></span></p>';	
    }
    htmll += '<hr>';
    htmll += '<div class="g-total"><h3>Grand Total <span id="grand_total"><span>';
    if(parseFloat($discount_amount) > 0){
        var strikeamount = parseFloat($plan_price);
            htmll += '<strike>$';
            htmll += Math.round(strikeamount * 100) / 100;
            htmll += '</strike> $'+total_price;
    } else{
            htmll += '$'+total_price;
    }
    htmll += '</span></span></h3></div>';
    if(parseFloat($discount_amount).toFixed(2) > 0){
            htmll += '<span class="coupon-des-newsignup">'+$discount_notes+'</span>';
    }
    $('.total-pPrice').html(htmll);
    $('#selected_price').val(total_price);
}
$("#saas_signup").submit(function(e){
    e.preventDefault();
    var formdata = new FormData($(this)[0]);
    console.log("formdata",formdata);
        $(".signp_err").css("display","none");
        $(".f_loader_sgnup").html('<span class="sgnp_lodr"><img src="'+assetspath+'img/ajax-loader.gif" alt="loading"/></span>');
        $.ajax({
            type: 'POST',
            url: BASE_URL+"Welcome/saas_signup",
            data: formdata,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (response) {
                 if(response.status == 1){
                   window.location.href = response.url;
                   //  window.location.href = BASE_URL + "account/subdomain";
                }else{
                    $(".signp_err").css("display","block");
                    $(".signp_err strong").html(response.message);
                }
                $(".f_loader_sgnup").html('');
                }
        });
});
</script>
</html>
