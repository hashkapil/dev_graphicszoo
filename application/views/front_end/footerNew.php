<section class="publection-sec">
    <div class="container">
        <div class="alert alert-success alert-dismissable show_hide" style="display:none";>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
            <strong class="msg"></strong>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Newsletter</h2>
                <p>Join the thousands of subscribers, learning about how we’re helping businesses grow.</p>
                <form id="submit_newsletter">
                    <div class="publication">
                        <input type="email" name="newsletter_email" id="newsletter_email" placeholder="ENTER EMAIL ADDRESS *" required>
                        <input type="submit" class="btn-publish" name="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-2 col-lg-3">
                <div class="list-menu">
                    <ul class="list-unstyled">
                        <li>Resources</li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>features">Features</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>pricing">Pricing</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>login">Login</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>signup">Sign Up</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>faq">FAQs</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-6 col-md-3 col-lg-3">
                <div class="list-menu">
                    <ul class="list-unstyled">
                        <li>Company</li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>aboutus">About Us</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>blog">Blog</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>testimonials">Testimonials</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>support">Contact Us</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>aboutus#team">Team</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="list-menu">
                    <ul class="list-unstyled">
                        <li>Legal</li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>termsandconditions">Terms and Conditions</a></li>
                        <li><a rel="canonical" href="<?php echo base_url(); ?>privacypolicy">Privacy Policy</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <div class="list-menu follow-footer-menu">
                    <ul class="list-unstyled">
                        <li>Follow us</li>
                        <li><a target="_blank" href="https://www.facebook.com/graphicszoo1/"  rel="nofollow"><i class="fab fa-facebook-square"></i>Be a fan on Facebook</a></li>
                        <li><a target="_blank" href="https://www.instagram.com/graphics_zoo/"  rel="nofollow"><i class="fab fa-instagram"></i>Follow us on Instagram</a></li>
                                    <!-- <li><a target="_blank" href="https://www.facebook.com/graphicszoo1/"  rel="noFollow"><i class="fab fa-google-plus-g"></i>Join us on Google+</a></li>
                                        <li><a target="_blank" href="https://www.facebook.com/graphicszoo1/"  rel="noFollow"><i class="fab fa-twitter-square"></i>Follow us on Twitter</a></li>  -->                       
                    </ul>
                </div>
            </div>
            <div class="col-md-12 text-center copyright">
                <p>Copyright 2019. All rights reserved.</p>
                <p>Made with <span class="beatHeart"><i class="fas fa-heart"></i></span> by GraphicsZoo </p>
            </div>
        </div>
    </div>
</footer>
<script async="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/vanillajs/vanilla-lazyloading.js"></script>
<script async="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/vanillajs/vanilla-lazyloading.min.js"></script>
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<?php $request_uri = $_SERVER['REQUEST_URI']; ?>

<?php
$currenturl = current_url();
$pagename = explode('/', $currenturl);
if ($currenturl == base_url()) {
    ?> 
    <script rel="nofollow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
<?php } ?> 

<?php if ((!in_array('portfolio', $pagename)) && (!in_array('pricing', $pagename))) { ?>
    <script async="" rel="nofollow" src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'js/front_end.js'); ?>"></script>
<?php } ?>
<script defer async="" rel="nofollow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/bootstrap.min.js"></script>
<script defer rel="nofollow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/jquery.fancybox.js"></script>

<script rel="nofollow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.js" ></script>
<script defer rel="nofollow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/waypoints.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<!--<script src="<?php //echo FS_PATH_PUBLIC_ASSETS;  ?>js/jquery.exitintent.js"></script>-->
<!-- <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>front_end/js/main.js"></script> -->
<?php if ($request_uri != '/login') { ?>
    <!-- Drip -->
    <script type="text/javascript">

                var _dcq = _dcq || [];
                var _dcs = _dcs || {};
                _dcs.account = '1975472';
                (function() {

                var dc = document.createElement('script');
                        dc.type = 'text/javascript';
                        dc.async = true;
                        dc.defer = true;
                        dc.src = '//tag.getdrip.com/1975472.js';
                        var s = document.getElementsByTagName('script')[0];
                        // setTimeout(function(){ 
                        s.parentNode.insertBefore(dc, s);
                        // }, 1000);



                })();</script>
    <!-- end Drip -->
<?php } ?>
<script>
            $(document).ready(function(){
    jQuery('.workflow').waypoint(function() {
    var tabChange = function(){
    var tabs = $('.nav-tabs > li')
            var active = tabs.find('a.active');
            var next = active.parent().next('li').length? active.parent().next('li').find('a') : tabs.filter(':first-child').find('a');
            next.tab('show');
    }
// Tab Cycle function
    var tabCycle = setInterval(tabChange, 6000)
            // Tab click event handler
            $(function(){
            $('.nav-tabs a').click(function(e) {
            e.preventDefault();
                    // Parar o loop
                    // mosta o tab clicado, default bootstrap
                    $(this).tab('show')
                    clearInterval(tabCycle);
                    tabCycle = setInterval(tabChange, 6000);
            });
            });
    });
    });</script>

<script type="text/javascript">
            jQuery(document).ready(function ($) {
    $(".fancybox")
            .attr('rel', 'gallery')
            .fancybox({
            openEffect  : 'none',
                    closeEffect : 'none',
                    nextEffect  : 'none',
                    prevEffect  : 'none',
                    padding     : 0,
                    margin      : [20, 60, 20, 60] // Increase left/right margin
            });
    });</script>
<script>
            function updateGallery(selector) {
            //console.log(selector);
            let $sel = selector;
                    current_image = $sel.data('image-id');
                    $('#image-gallery-title')
                    .text($sel.data('title'));
                    //console.log( $sel.data('image'));
                    $('#image-gallery-image')
                    .attr('src', $sel.data('image'));
                    //disableButtons(counter, $sel.data('image-id'));
            }

    jQuery('#image-gallery .modal-body > img').owlCarousel({
    margin: 0,
            items:1,
            autoplay:true,
    });
            function loadGallery(setIDs, setClickAttr) {
            let current_image,
                    selector,
                    counter = 0;
                    $('#show-next-image, #show-previous-image')
                    .click(function () {
                    if ($(this)
                            .attr('id') === 'show-previous-image') {
                    current_image--;
                    } else {
                    current_image++;
                    }

                    selector = $('[data-image-id="' + current_image + '"]');
                            updateGallery(selector);
                    });
                    if (setIDs == true) {
            $('[data-image-id]')
                    .each(function () {
                    counter++;
                            $(this)
                            .attr('data-image-id', counter);
                    });
            }
            $(setClickAttr).on('click', function () {
            updateGallery($(this));
            });
            }

    let modalId = $('#image-gallery');
            $(document).ready(function () {
    loadGallery(true, 'a.thumbnail');
    });
            window.addEventListener('load', function(){
            var allimages = document.getElementsByTagName('img');
                    for (var i = 0; i < allimages.length; i++) {
            if (allimages[i].getAttribute('data-src')) {
            allimages[i].setAttribute('src', allimages[i].getAttribute('data-src'));
            }
            }
            }, false)
</script>
<!-- REFERSION TRACKING: BEGIN -->
<script async defer src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/pub_01537b7d8dcde95d5a0d.js"></script>
<!--script>_refersion();</script>
<!- REFERSION TRACKING: END -->
<!-- UTM Tracking: BEGIN -->
<script async defer src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/utm_form-1.0.4.min.js"></script>
<script type="text/javascript" charset="utf-8">
            var _uf = _uf || {};
            _uf.utm_source_field = "YOUR_SOURCE_FIELD"; // Default 'USOURCE'
            _uf.utm_medium_field = "YOUR_MEDIUM_FIELD"; // Default 'UMEDIUM'
            _uf.utm_campaign_field = "YOUR_CAMPAIGN_FIELD"; // Default 'UCAMPAIGN'
            _uf.utm_content_field = "YOUR_CONTENT_FIELD"; // Default 'UCONTENT'
            _uf.utm_term_field = "YOUR_TERM_FIELD"; // Default 'UTERM'
            _uf.sessionLength = 2; // In hours. Default is 1 hour
            _uf.add_to_form = "none"; // 'none', 'all', 'first'. Default is 'all'
</script>
<!-- UTM Tracking: END -->
<script>
            /***************newsletter submit ****************/
            $(document).ready(function(){
    $('#submit_newsletter').on('submit', function (e) {
    e.preventDefault();
            var newsletter_email = $('#newsletter_email').val();
            // console.log(newsletter_email);
            if (newsletter_email != ''){
    $.ajax({
    type: "POST",
            url: "<?php echo base_url(); ?>welcome/newsletter",
            data: {"newsletter_email": newsletter_email},
            success: function (data) {
            if (data){
            //console.log(data);
            $('.msg').html(data);
                    $('.show_hide').css('display', 'block');
                    $('#newsletter_email').val('');
                    // window.location.reload();
            }
            setTimeout(function() {
            $(".show_hide").css('display', 'none');
            }, 3000);
            }
    });
    }
    });
    });
<?php if ($agency_livechat_script != '') {
    ?>
        $("a.help_user").addClass("space_align");
<?php } else { ?>
        $("a.help_user").removeClass("space_align");
<?php } ?>
</script>

<!--***************Live Chat Script*************************-->
<?php if ($login_user_data[0]['user_flag'] != 'client') { ?>
    <script>
        "use strict";
                !function() {
                var t = window.driftt = window.drift = window.driftt || [];
                        if (!t.init) {
                if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
                        t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
                        t.factory = function(e) {
                        return function() {
                        var n = Array.prototype.slice.call(arguments);
                                return n.unshift(e), t.push(n), t;
                        };
                        }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
                }), t.load = function(t) {
                var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
                        o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
                        var i = document.getElementsByTagName("script")[0];
                        setTimeout(function(){  i.parentNode.insertBefore(o, i); }, 5000);
                };
                }
                }();
                drift.SNIPPET_VERSION = '0.3.1';
                drift.load('d6yi38hkhwsd');</script>
    <?php
} else {
    if (!empty($client_script)) {
        /***Start client script before close head tag***/
        foreach ($client_script as $k => $s_vl) {
            if (($s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_body") {
                if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                    echo base64_decode($s_vl["tracking_script"]);
                } else {
                    ?>
                    <script>
                    <?php echo base64_decode($s_vl["tracking_script"]); ?>
                    </script>
                    <?php
                }
            }
        }
        /***End client script before close body tag***/
    }
} ?> 

<script>
    /********on click tab make it on first position***********/
    var $owl = $('.owl-carousel');
            $owl.children().each(function (index) {
    $(this).attr('data-position', index); // NB: .attr() instead of .data()
    });
            $(document).on('click', '.owl-item>div', function (e) {
    e.preventDefault();
            $owl.trigger("to.owl.carousel", [$(this).data('position'), 200, true]);
    });
            $(document).on('click', '.red-theme-btn.testimonial', function(){
    window.location.href = BASE_URL + 'testimonials';
    });</script>

<!-- Facebook Pixel Code -->
<script>

            !function (f, b, e, v, n, t, s)
            {
            if (f.fbq)
                    return;
                    n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                    };
                    if (!f._fbq)
                    f._fbq = n;
                    n.push = n;
                    n.loaded = !0;
                    n.version = '2.0';
                    n.queue = [];
                    t = b.createElement(e);
                    t.async = !0;
                    t.defer = !0;
                    t.src = v;
                    s = b.getElementsByTagName(e)[0];
                    setTimeout(function(){ s.parentNode.insertBefore(t, s) }, 5000);
            }(window, document, 'script',
            '<?php echo base_url(); ?>public/assets/js/fbevents.js');
            fbq('init', '157875968111046');
            fbq('track', 'PageView');</script>
<noscript>
<a href="javascript:void(0)" rel="nofollow"><img alt="facebook" height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=157875968111046&ev=PageView&noscript=1"/></a>
</noscript>
<!-- End Facebook Pixel Code -->


<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "LocalBusiness",

    "address": {
    "@type": "PostalAddress",
    "streetAddress": "245 Commerce Green Blvd.",
    "addressLocality": "Sugar Land",
    "addressRegion": "TX",
    "postalCode": "77479"

    },

    "name": "Graphics Zoo",
    "telephone": "888-976-2747",
    "email": "<?php echo SUPPORT_EMAIL; ?>",
    "url": "https://www.graphicszoo.com",
    "image": "<?php echo base_url(); ?>public/new_fe_images/logo.png",
    "priceRange": "$$",
    "sameAs" :
    [
    "https://www.facebook.com/graphicszoo1/",
    "https://www.instagram.com/graphics_zoo/"
    ]
    }


</script>
<!--***************Live Chat Script*************************-->
<style>
    .calendly-badge-widget {
        left:30px;
    }
</style>

<!-- Calendly badge widget begin -->
<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet nofollow canonical">
<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript" rel="nofollow"></script>
<script type="text/javascript">Calendly.initBadgeWidget({ url: 'https://calendly.com/graphicszoo/graphics-zoo-whitelabel-demo', text: 'Schedule Demo', color: '#00a2ff', textColor: '#ffffff', branding: false });</script>
<!-- Calendly badge widget end -->

</body>
</html>

