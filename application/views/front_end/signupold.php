<style>
.authentication-main.signupmain {
    padding: 0px 0px 60px;
    background: url(<?php base_url() ;?>public/new_fe_images/signupbg.png) no-repeat center bottom;
    background-size: 100%;
}
</style>
 <link href="<?php echo base_url(); ?>public/new_fe_css/style.css" rel="stylesheet">
 <script src="<?php echo base_url(); ?>public/new_fe_js/jquery-2.2.0.min.js"></script>

<script src="<?php echo base_url(); ?>public/js/js-card.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/js/js-card.css">
<section class="authentication-main signupmain">
    <div class="container">

        <div class="innermain-banner">
            <h1 class="dblue">Sign up now and get the most<br />out of Graphics Zoo.</h1>
        </div>

        <div class="boxmain">

            <div class="row d-flex">

                <div class="col-sm-12 col-md-4 align-self-center">
                    <div class="logo"><img src="<?php echo base_url(); ?>public/new_fe_images/logo.png" class="img-responsive img" alt="" /></div>
                </div>

                <div class="col-sm-12 col-md-8">
					 <?php if($this->session->flashdata('message_error') != '') {?>

    			   <div class="alert alert-danger alert-dismissable">

    					<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>

    					<strong><?php echo $this->session->flashdata('message_error'); ?></strong>

    				</div>

    			   <?php }?>

    			   <?php if($this->session->flashdata('message_success') != '') {?>

    			   <div class="alert alert-success alert-dismissable">

    					<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>

    					<strong><?php echo $this->session->flashdata('message_success');?></strong>

    				</div>

    			   <?php }?>
                    <form class="row" method="post" action="">
                        <div class="col-md-12"><h3>Subscription Plan:</h3></div>
                        <div class="form-group col-md-7">
                            <select class="form-control red" name="plan_name">
                                <option>Please select a subscription plan</option>
								<?php for($i=0;$i<sizeof($planlist);$i++){
									echo '<option value="'.$planlist[$i]['id'].'">'.$planlist[$i]['name'].'</option>';
								} ?>
                            </select>
                        </div>


                        <div class="col-md-12"><h3>Account Info:</h3></div>
                        <div class="form-group col-md-6">
                            <label class="alllabel">First Name</label>
                            <input type="text" class="form-control" placeholder="" name="first_name" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="alllabel">Last Name</label>
                            <input type="text" class="form-control" placeholder="" name="last_name" required>
                        </div>


                        <div class="form-group col-md-6">
                            <label class="alllabel">Email Address</label>
                            <input type="text" class="form-control" placeholder=""  name="email" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="alllabel">Password</label>
                            <input type="password" class="form-control" placeholder="" name="password" required>
                        </div>

						<div class="card-js col-md-12">
                        <div class="col-md-12"><h3>Payment Method:</h3></div>
                        <div class="form-group col-md-5">
                            <label class="alllabel">Card Number</label>
                            <input type="text" class="card-number form-control" placeholder="" name="card_number" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="alllabel">Exp Date</label>
                            <input class="expiry-month" name="expiry-month">
							<input class="expiry-year" name="expiry-year">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="alllabel">CVC</label>
                            <input class="cvc" name="cvc" required>
                        </div>
						</div>
                        <div class="form-group col-md-12">
                            <div class="checkbox chckbox1">
                                <label>
                                    <input type="checkbox" name="" required> Please accept the Terms & Conditions in order to proceed further with your account.
                                </label>
                            </div>
                        </div>

                        <div class="clear sep1"></div>

                        <div class="col-md-12">

                            <div class="row">

                                <div class="col-xs-push col-sm-8 col-sm-push-4 col-md-8 col-md-push-4  col-lg-8">
                                    <div class="cards">
                                        <img src="<?php echo base_url(); ?>public/new_fe_images/cards.png" class="img-responsive img" alt="card">
                                    </div>
                                </div>


                                <div class="col-xs-pull col-sm-4 col-sm-pull-8 col-md-4 col-md-pull-8 col-lg-4">
                                    <button class="btn btn1 sqr" type="submit">Sign Up Now</button>
                                </div>

                            </div>

                        </div>


                    </form>

                </div>

            </div>

        </div>


    </div>
</section>