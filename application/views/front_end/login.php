<style>
  @media only screen and (max-width: 767px) {
    .subscription_pad {
      padding-top: 30px;
    }
  }

  .back-to-main a{
    margin-left: 10px;
    color: #e8304d;
  }
  section.publection-sec {
    display: none;
  }

  #SignupModal h2 {
    font-size: 20px;
    margin-bottom: 20px;
    color: #153669;
    font-weight: 400;
  }
  .border-bottom{
    padding-bottom: 17px;
    padding-right: 10px;
    border-color: #DCDCDC;
  }
  select.subs_plan {
    padding: 6px;
  }
  .select_wrap {
    border-bottom: 1px solid #e8304d;
  }
  .plans_table {
    border: 1px solid #e8304d;
    padding: 20px 15px;
    border-radius: 10px;
  }
  .plans_table h3 {
    text-transform: uppercase;
    color: #e8304d;
  }
  .plan_price span {
    font-size: 30px;
    font-weight: 800;
  }
  .form-signin-group.checkbox a {
    float: right;
  }
  button.pricing_btn {
    border: 1px solid #e8304d;
    background: transparent;
    width: 100%;
    border-radius: 30px;
    color: #e8304d;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 800;
    padding: 5px;
    margin-bottom: 15px;
  }
  .plan_list{list-style:none; padding:15px}
  h6{padding-left:15px;}
  section.footer_section {
    padding: 50px 50px 80px;
  }
  button.btn.btn-y {
    background: #37c473;
    color: #fff;
    width: 30%;
  }
  button.btn.btn-n {
    background: #e73250;
    color: #fff;
    width: 30%;
  }
  #forgot_password .modal-dialog {
    max-width: 600px;
    margin: 50px auto;
  }
  #forgot_password .modal-dialog p.sub-title{margin-top: 50px;}
  #forgot_password .cli-ent-model {
   padding: 10% 20%;
 }
 .note {
  max-width: 695px;
  margin: auto;
  margin-bottom: 30px;
  color: #e42647;
  box-shadow: 0 4px 20px 0px rgb(132, 14, 32, 0.15);
  padding: 16px;
  border-radius: 50px;
  font-size: 16px;
  font-weight: 500;
  text-shadow: 0px 2px 14px rgb(125, 14, 32, 0.15);
  animation: blink 4s linear infinite;
  display: flex;
  align-items: center;
}
span.bell {
  background: #e72c47;
  color: #fff;
  font-weight: bold;
  height: 40px;
  width: 40px;
  border-radius: 50px;
  font-size: 20px;
  line-height: 40px;
  animation: pendulum 1.3s linear infinite;
}
.note p {
  color: #e42647;
  font-size: 14px;
  margin-left: 15px;
}

@-moz-keyframes
pendulum{
  from{-moz-transform:rotate(-15deg)}50%{-moz-transform:rotate(15deg)}to{-moz-transform:rotate(-15deg)}}@-webkit-keyframes
  pendulum{from{-webkit-transform:rotate(-15deg)}50%{-webkit-transform:rotate(15deg)}to{-webkit-transform:rotate(-15deg)}}@keyframes
  pendulum{from{transform:rotate(-15deg)}50%{transform:rotate(15deg)}to{transform:rotate(-15deg)}}

  @keyframes blink{
    0%{
      color: rgba(228, 38, 71, 1);
      box-shadow: 0 4px 20px 0px rgb(132, 14, 32, 0.08);
    }
    15%{
      color: rgba(228, 38, 71, 0.50);

    }
    50%{
      color: rgba(228, 38, 71, 1);
      box-shadow: 0 4px 20px 0px rgb(132, 14, 32, 0.18);
    }
    100%{
      color: rgba(228, 38, 71, 1);
      box-shadow: 0 4px 20px 0px rgb(132, 14, 32, 0.08);
    }
  } 
</style>
<?php //echo "<pre/>";print_R($res);exit; ?>
<section class="page-heading-sec">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
<!--     <div class="note">
       <span class="bell"><i class="fas fa-bell"></i></span>
       <p>Our office will be closed on December 24th and 25th in celebration of Christmas.</p></div> -->
       <h1>Welcome to Graphics Zoo</h1>
       <p>Login with your email and password</p>
     </div>
   </div>
 </div>
</section>
<section id="signin">
 <div class="container">
  <div class="row">
   <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
    <form method="post" class="signin-form" action="<?php echo base_url()."login"?>">
     <?php if($this->session->flashdata('message_error') != '') {?>       
      <div class="alert alert-danger alert-dismissable">
       <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
       <strong><?php echo $this->session->flashdata('message_error'); ?></strong>              
     </div>
   <?php }?>
   <?php if($this->session->flashdata('message_success') != '') {?>             
    <div class="alert alert-success alert-dismissable">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
     <strong><?php echo $this->session->flashdata('message_success');?></strong>
   </div>
 <?php }?>
 <div class="form-signin-group col-md-12 inner-addon left-addon">
  <input type="email" name="email" class="form-control" id="uname" value="<?php echo isset($_COOKIE['email'])?$_COOKIE['email']:''; ?>" placeholder="Email Address" data-rule="minlen:4" data-msg="Please enter at correct username">
  <div class="validation"></div>
</div>
<input type="hidden" name="url" value="<?php echo isset($_SERVER['QUERY_STRING'])? $_SERVER['QUERY_STRING']:'' ?>">
<div class="form-signin-group col-md-12 inner-addon left-addon">
  <input type="password" name="password" class="form-control" id="password" value="<?php echo isset($_COOKIE['password'])?$_COOKIE['password']:''; ?>"placeholder="Password" data-rule="minlen:4" data-msg="Please enter at correct password">
  <div class="validation"></div>
</div>
<div class="form-signin-group col-md-12 checkbox">
  <div class="form-radion2">
    <label class="container"> Remember me
      <input type="checkbox" name="remember_me" >
      <span class="checkmark"></span>
    </label>
  </div>
  
  <a data-toggle="modal" data-target="#forgot_password" href="javascript:void(0)">Forgot password?</a>
  <div class="validation"></div>
</div>
<div class="form-signin-group col-md-12 inner-addon">
 <button type="submit" class="red-theme-btn">Login Now</button>
 
</div>
<div class="form-signin-group col-md-12 inner-addon">
 <a href="<?php echo base_url(); ?>signup" class="create_account_link" rel="canonical">Create Account</a>
</div>
</form>
</div>
</div>
</div>
</section>
  <!--==========================
    Footer
    ============================-->
<!--     <section class="footer_section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            Secured By <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/macafee-secure.png" width="110" >
          </div>
        </div>
      </div>
    </section> -->
    <!-- <?php echo base_url();?>welcome/forgot_password -->
    <!-- Modal -->
    <div class="modal fade" id="forgot_password" tabindex="-1" role="dialog" aria-labelledby="forgot_password" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="cli-ent-model-box">
            <div class="cli-ent-model">
              <header class="fo-rm-header">
                <h2 class="text-center">Forgot your Password?</h2>
                <p class="sub-title">Enter your email address and we'll send you a link to reset your password.</p>
              </header>
              <div class="confirmation_btn text-center">
                <form action="<?php echo base_url();?>welcome/forgot_password" method="post" >
                  <div class="">
                    <input class="form-control input-c" type="email" placeholder="Email Address" name="email" id="email" value=""/>
                  </div>
                  <div class="">
                    <button class="red-theme-btn" type="submit">Send Link</button>
                    <p>Not a member? <strong><a href="<?php echo base_url(); ?>signup" class="create_account_link" rel="canonical">Sign up!</a></strong></p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
