
<!--==========================
Hero Section
============================-->
<section id="hero_section" class="">
	<div class="hero-container">
		<div class="container">
			<div class="row">
				<div class="col-md-5 align-self-center">
					<h2>How does we work?</h2>
          <p style="letter-spacing: 0px;margin-top: -15px;display: block;margin-bottom: 30px;font-weight: 600;">We’re a unique service made for unique people.</p>
					<a href="#get-started" class="btn-get-started scrollto"><b>Get Started Today</b></a>
				</div>
				<div class="col-md-7 text-right">
					<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/how-it-works-right.png" style="max-width:100%;" alt="how-it-works-right">
				</div>
			</div>
		</div>
	</div>
</section><!-- #hero -->
<section id="design-for-everyone" class="">
    <div class="container">
      <div class="row">
        <div class="col-md-6 align-self-center">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/howitwork-2.png" style="max-width:100%;" alt="howitwork-2">
        </div>
        <div class="col-md-6 text-left">
          <div>
            <div style="float: left;width: 21%;"><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/everyone-arrow.png" alt="everyone-arrow"></div>
            <div style="float: right;width: 79%;">
              <p style="color:#e8304d;font-weight: 700;font-size: 18px;margin-top: 10px;">For startups & businesses</p>
              <p>Create unlimited design project and let Penji help with all your graphic design needs</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              <p>For Agencies</p>
              <p>For Merchant</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="step1" class="">
    <div class="container">
      <div class="row text-center">
        <div class="col-md-12 mt-5 mb-5">
<span style="color: #828d99;font-size: 30px;font-weight: 700;">How does it work?</span></div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/step1.png" alt="step1" style="max-width:100%;position: relative;z-index: 1;" >
        </div>
        <div class="col-md-8 align-self-center">
        <h3>Create Design Project</h3>
        <p>Tell your designer exactly what you want to design. A designer will automatically be assigned and get started on your project.</p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 text-center one-two" >
      <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/step1-2.png" alt="step1-2">
    </div>
  </div>

      <div class="row">
        <div class="col-md-8 align-self-center order2">
        <h3>Feedback & revisions</h3>
        <p>Tell your designer exactly what you want to design. A designer will automatically be assigned and get started on your project.</p>
        </div>
        <div class="col-md-4">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/step2.png" style="max-width:100%;position: relative;z-index: 1;" alt="step2">
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 text-center two-three" >
      <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/step2-3.png" alt="step2-3">
    </div>
  </div>

      <div class="row">
        <div class="col-md-4">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/step3.png" style="max-width:100%;position: relative;z-index: 1;" alt="step3">
        </div>
        <div class="col-md-8 align-self-center">
        <h3>Approve & get files</h3>
        <p>Tell your designer exactly what you want to design. A designer will automatically be assigned and get started on your project.</p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 text-center three-four" >
      <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/step3-4.png" alt="step3-4">
    </div>
  </div>

      <div class="row">
        <div class="col-md-8 align-self-center order2">
        <h3>Source files</h3>
        <p>Tell your designer exactly what you want to design. A designer will automatically be assigned and get started on your project.</p>
        </div>
        <div class="col-md-4">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/step4.png" style="max-width:100%;" alt="step4">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="nopadding">
  <div class="container-fluid nopadding">
    <div class="row mh-mh">
      <div class="col-md-6 nopadding">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/man.png" width="100%" alt="man">
      </div>

      <div class="col-md-6 align-self-center try_graphics">
        <h2>Try Graphics Zoo Risk-Free</h2>
        <p>We know trying a new service can be scary, that's why we want to make this 100% risk-free for you and your company.</p>
        <button class="button button1">GET STARTED</button>
      </div>
    </div>
  </div>
</section>