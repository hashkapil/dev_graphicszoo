<style>
    section.publection-sec{display: none}
    .col-md-6.text-center.post-box{
        margin-bottom: 30px;
    }
    #hero_section {
        margin-top: 0px;
    }
    .image img {
        max-width: 100%;
        height: 100%;
        width: auto;
    }
    section.page-heading-sec.pricing-header {
        background: transparent;
    }
</style>
<section class="page-heading-sec pricing-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Stay up to date with our latest blog posts.</h1>
                <p>We have everything you need for your business to succeed with design. Check out the articles below to learn the tips and tricks of the industry.</p>
            </div>
        </div>
    </div>
</section>
<section id="hero_section">
    <div class="hero-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 doubel-blog-cont ">
                    <?php
                    for ($i = 0; $i < sizeof($data); $i++) {
                        if ($data[$i]['category'] == "recomended") {
                            continue;
                        }
                        
                        ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 post-box">
                            <div class="content-blog">
                                <a rel="canonical" href="<?php echo base_url() . "article/" . $data[$i]['slug']; ?>" rel="nofollow">
                                    <div class="image">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_BLOGS . $data[$i]['image']; ?>" width="100%" alt="<?php echo pathinfo($data[$i]['image'], PATHINFO_FILENAME);?>">
                                    </div>
                                    <p class="post-box-cat"><span style="color: #606060;">IN</span> <span style="color:#e42547;">LIFESTYLE</span></p>
                                    <p class="post-box-title" title='<?php echo $data[$i]['title']; ?>'>
                                        <?php
                                        echo substr(strip_tags($data[$i]['title']), 0, 68);
                                        $length = strlen($data[$i]['title']);
                                        if ($length >= 68) {
                                            echo "...";
                                        }
                                        ?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="riskFree-sec pricing-risk">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Join more than 500+ customers</h2>
                <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
                <a rel="canonical nofollow" href="<?php echo base_url(); ?>pricing" class="red-theme-btn">Get Started Now
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid" alt="red-long-arrow"></a>

            </div>
        </div>
    </div>
</section>