<!DOCTYPE html>
<?php //echo "<pre>";print_r($user_data); ?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Thank You | Graphics Zoo</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">
        <!-- Favicons -->
        <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,600i,700,700i,800,800i,900,900i" rel="stylesheet canonical">
        <!-- Bootstrap css -->
        <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
        <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/css/bootstrap.css" rel="stylesheet canonical">
        <!-- Libraries CSS Files -->
        <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet canonical">
        <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/animate/animate.min.css" rel="stylesheet canonical">
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-57XFPLD');</script>
        <!-- End Google Tag Manager -->
        <!-- Font Awesomw library -->
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">-->

        <!-- Main Stylesheet File -->
        <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/style.css" rel="stylesheet canonical">
        <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/custom.css" rel="stylesheet canonical">
        
    </head>
    <style>
        #header{
            display: none;
        }
        .header_inner{
            display: block !important;
        }
        @media only screen and (max-width: 767px) {
            .subscription_pad {
                padding-top: 30px;
            }
        }

        #signup-page .header .container {
            max-width: 900px;
        }

        #signup-page .header .already_signin {
            text-align: right;
        }

        #signup-page #header{background:#F3F3F3; position:relative;}
        #signup-page {
            background-color: #F3F3F3;
            background-image: url(<?php echo base_url(); ?>public/front_end/Updated_Design/img/signup-bg.png);
            background-repeat: no-repeat;
            background-position: center bottom;
        }
        #signup-page h2{font-family: 'Montserrat', sans-serif; font-weight:400}
        .back-to-main a{
            margin-left: 10px;
            color: #e8304d;
        }
        .modal-dialog{max-width:900px}
        .modal-content{
            box-shadow: 0 0 35px rgba(0, 0, 0, 0.1);
            border: 1px solid #E5E5E5;
            padding: 25px 0px;
        }
        .form-group{position:relative;}
        .form-group i{
            position: absolute;
            top: 13px;
            left: 10px;
            color: #A0A0A0;
        }
        .col_2 .form-group i{left:24px;}
        .form-group input {
            padding-left: 35px;
        }
        #SignupModal{
            padding-top:40px;
        }
        #SignupModal h2 {
            font-size: 20px;
            margin-bottom: 20px;
            //margin-top: 20px;
            color: #153669;
            font-weight: 400;
            text-align:center;
        }
        .border-bottom{
            padding-bottom: 17px;
            padding-right: 10px;
            border-color: #DCDCDC;
        }
        select.subs_plan {
            padding: 6px;
            width:100%;
        }
        .select_wrap {
            border-bottom: 1px solid #e8304d;
        }
        .plans_table {
            border: 1px solid #e8304d;
            padding: 20px 15px;
            border-radius: 10px;
        }
        .plans_table h3 {
            text-transform: uppercase;
            color: #e8304d;
        }
        .plans_table_logo {
            border: 1px solid #e8304d;
            padding: 20px 15px;
            border-radius: 10px;
            height: 100%;
            display: table;
            /*position: absolute;*/
            width: 100%;
        }
        .plans_table_logo h3 {
            text-transform: uppercase;
            color: #e8304d;
        }
        .signup_logo{
            display: table-cell;
            vertical-align: middle;
        }
        .inner-logo{
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
        .plan_price span {
            font-size: 30px;
            font-weight: 800;
        }
        .plans_table_sec {
            border: 1px solid #e8304d;
            padding: 20px 15px;
            border-radius: 10px;
        }
        .plans_table_sec h3 {
            text-transform: uppercase;
            color: #e8304d;
        }
        .plan_price_sec span {
            font-size: 30px;
            font-weight: 800;
        }
        button.pricing_btn {
            border: 1px solid #e8304d;
            background: transparent;
            width: 100%;
            border-radius: 30px;
            color: #e8304d;
            text-transform: uppercase;
            font-size: 14px;
            font-weight: 800;
            padding: 5px;
            margin-bottom: 15px;
        }
        a.sign-up-p{
            cursor: pointer;
            text-decoration: none !important;
            border-radius: 50px !important;
            font-size: 17px;
            background: #e82f4d;
            font-weight: 600;
            margin: 16px 0 0;
            padding: 12px 18px;
            color: #fff !important;
            border: #e82f4d;
            display:inline-block;
        }
        .plan_list{list-style:none; padding:15px}
        h6{padding-left:15px;}
        section.footer_section {
            padding: 50px 50px 80px;
        }
    </style>
    <body id="signup-page">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <!-- Modal -->
        <div id="SignupModal" class="modal_ fade_" role="dialog_">
            <div class="col-md-5 col-sm-5 sign-up-pagep">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container">

                            <div class="row">

                                <div class="col-md-12 subscription_pad" style="text-align:center;">

                                    <img width="80px" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/check.png" alt="Graphics Zoo" title="Graphics Zoo" />
                                    <h2>&nbsp;<?php //echo "Hey ".(isset($user_data['first_name']) && $user_data['first_name'] != '') ? ucwords($user_data['first_name']) : "";  ?></h2>
                                    <h3 style="margin-bottom:20px;">You have Successfully Signed up!</h3>
                                    <p>Go to the dashboard to submit a new design request and see what your new design team can do for you.</p>
                                    <div><a class="sign-up-p" href="<?php echo base_url(); ?>customer/profile/thank_you"> Go to Dashboard</a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!--==========================
    Hero Section
    ============================-->

    <!--==========================
    Footer
    ============================-->
    <section class="footer_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">

                </div>
            </div>
        </div>
    </section>
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/superfish/hoverIntent.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/superfish/superfish.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/easing/easing.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/wow/wow.min.js"></script>
    <!-- Contact Form JavaScript File -->
<!--    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/contactform/contactform.js"></script>-->

    <!-- Template Main Javascript File -->
    <!--<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/main.js"></script>-->
    <script type="text/javascript">
            // function ValidDate(d) {
            //      d = new String(d)
            //      var re1 = new RegExp("[0|1][0-9]\/[1-2][0-9]{3}");
            //      var re2 = new RegExp("^[1[3-9]]");
            //      var re3 = new RegExp("^00");
            //      var expir_date = re1.test(d)&&(!re2.test(d))&&(!re3.test(d)); 
            //      if(expir_date === true){
            //         $('#expir_date').val(d);
            //      }else{
            //         $('#expir_date').val("");
            //      }               
            // }
            function dateFormat(val) {
                if (val.length == 2) {
                    $('#expir_date').val($('#expir_date').val() + '/');
                }
                /*if(val.length == 5){
                 document.getElementById('cvv').focus();
                 }*/
            }
            $('.subs_plan').on('change', function () {
                var sub_val = $(this).val();
                if (sub_val == 'A2870G') {
                    $('.plans_table_sec').show();
                    $('.plans_table').hide();
                    $('.plans_table_logo').hide();

                } else if (sub_val == 'M299G') {
                    $('.plans_table_sec').hide();
                    $('.plans_table_logo').hide();
                    $('.plans_table').show();
                }
            });
    </script>
	<!-- REFERSION TRACKING: BEGIN -->
	<script src="//graphicszoo.refersion.com/tracker/v3/pub_01537b7d8dcde95d5a0d.js"></script>
	<script>
	_refersion(function(){

		_rfsn._addTrans({
			'order_id': '<?php echo $user_data['customer_id']; ?>',
			'shipping': '0',
			'tax': '<?php echo $user_data['taxpaid']; ?>',
			'discount': '0',
			'discount_code': 'NULL',
			'currency_code': 'USD'
		});

		_rfsn._addCustomer({
			'first_name': '<?php echo $user_data['first_name']; ?>',
			'last_name': '<?php echo $user_data['last_name']; ?>',
			'email': '<?php echo $user_data['email']; ?>',
			'ip_address': '<?php  echo $_SERVER['REMOTE_ADDR'];?>'
		});

		_rfsn._addItem({
			'sku': '<?php echo $user_data['current_plan']; ?>',
			'quantity': '1',
			'price': '<?php echo $user_data['plan_amount']; ?>'
		});

		_rfsn._sendConversion();

	});
	</script>
	<!-- REFERSION TRACKING: END -->
</body>
</html>