<script rel="nofollow" src="https://proof.graphicszoo.com/public/assets/front_end/Updated_Design/owl/jquery.min.js"></script>
<section id="sass_banner" class="affiliate-section affiliate-banner saas_banner_section">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="left-earn-sec">
					<h1 class="saas-heading">PROOF - #1 <br>graphic designing tool for all your design needs</h1>
					<p>PROOF by GraphicsZoo is the best tool to manage all your graphic design work from one place. Design, edit, review, and share your designs with ease through PROOF.
					</p>
					<a class="join_today landing_button" href="/register">Here’s the PROOF</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="saas-banner-image">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/banner-image.png" class="">
				</div>
			</div>
		</div>
	</div>
</section>

<section id="s_features" class="affiliate-section saas_feature">
	<div class="container">
	<h2 class="affiliate-sub-heading saas-heading text-center"> Main Features </h2>
		<div class="row">
			<div class="col-md-6">
				
			<div class="left-feature">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
				<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
					<h3>Design </h3>
					<p>Create and manage all your designs from one place to improve your project turn-around time. With this feature, create and save your work for client reference and user access.</p>
				</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
				<h3>Share </h3>
					<p>Through PROOF, you get the advantage of sharing the design in progress with your management, team, and/or client to take a look at and decide if the project is in sync with the expectations. You get the authority to set permission on the shared link. The receiver may view, comment, and/or download the project design. </p>
				</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
				<h3>Review </h3>
					<p>One of the best features of PROOF by GraphicsZoo is the real-time reviewing of your project. You can add comment to your designs and let your designer know if you need any particular changes done to the current design. No need to write those lengthy emails and then keep waiting for the response. Simply add your reviews on the project itself and the designated designer will update the same. </p>
				</a>
				</li>
				</ul>
				<a class="join_today landing_button" href="/register">Get Started</a>
				</div>


			</div>
			<div class="col-md-6">
			<div class="ryt-feature">
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">  
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/saas_design.png" class="">
					</div>
					<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/saas_share.jpg" class="">	
					</div>
					<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/saas_review.jpg" class="">
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</section>

<section id="s_add_features" class="affiliate-section landing-feature">
	<div class="container">
		<h2 class="affiliate-sub-heading saas-heading text-center"> Additional Features </h2>
		
		<div class="row">
			<div class="col-md-4">
				<div class="affil-item">
				<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/client-mang.svg" class="">
					<h3>User and Client Management</h3>
					<p>Add team members to your project and let them have a firsthand look of the design as well. Keep a track on the progress and all the updates done on your designs. Share the status of your project further with your clients to keep them in loop and assure them of timely completion and delivery of the project. </p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
				<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/icons_file_manage.svg" class="">
					<h3>File management</h3>
					<p>Stack all your designs at one place to have easy access whenever required. Define your designs according to the clientele and put a tab on each project for simpler navigation. You can download the source files of all your design directly on your device. 
 
</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
				<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/icons_brand_profile.svg" class="">
					<h3>Brand profile</h3>
					<p>Create a separate brand profile for each of your clients to save time on new design requests. Simply select the brand you want the new design for and your allotted design partner will start working on it using the information saved in the profile beforehand. 
</p>
				</div>
			</div>

		</div>
		<div class="row">		
				<div class="col-md-4">
				<div class="affil-item">
				<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/revision.svg" class="">
					<h3>Interactive revisions</h3>
					<p>Make as many edits and changes to your designs by simply commenting on the part of the design you want to change. Let the designer know about your requirements and get the edits done immediately. </p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
				<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/icons_white_label.svg" class="">
					<h3>White Label</h3>
					<p>Why take the pain of creating and maintaining a website for your design services, when you can use built-up platform. With PROOF, you can use GraphicsZoo’s interface as your own and communicate with your clients while managing all your projects</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
				<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/payment_i.svg" class="">
					<h3>Payment integration with stripe</h3>
					<p>Get your personal payment gateway through Stripe to manage all your business deals and transactions with privacy and security. </p>
				</div>
			</div>
		</div>
		<div class="serv-button text-center">
		<a class="join_today landing_button" href="/register">Get Started</a>
		</div>
	</div>
</section>

<section id="s_pricing" class="affiliate-section saas-pricing">
	<div class="container">
		<h2 class="affiliate-sub-heading saas-heading text-center"> Subscription Package </h2>
		<p class="text-center">Whether you are holding a one man show, or a team to execute your graphic design projects, <br> we have created subscription packages catering to all. 
		</p>
		<div class="row">
			<div class="col-md-4">
				<div class="subscription_item">
					<h3>Individual Subscription</h3>
					<div class="saas-price">
						<span>$29</span><p>Per Month</p>
					</div>
					<ul>
						<li>1 admin user access</li>
						<li>5 client user access</li>
						<li>2GB storage</li>
						<li>Interactive revision</li>
						<li>User & Client Management (share, review, collaborate)</li>
						<li>Payment integration</li>
					</ul>
					<a class="join_today landing_button" href="/register">sign up for free trial</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="subscription_item">
					<div class="popular-plan">MOST POPULAR</div>
					<h3>Premium Subscription</h3>
					<div class="saas-price">
						<span>$129</span><p>Per Month</p>
					</div>
					<ul>
						<li>5 admin user access</li>
						<li>Unlimited client user access</li>
						<li>5GB storage/admin user</li>
						<li>User and client management (share, review, collaborate)</li>
						<li>Payment integration</li>
						<li>White label</li>
						<li>File management</li>
						<li>API integration</li>
					</ul>
					<a class="join_today landing_button" href="/register">sign up for free trial</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="subscription_item">
					<h3>Enterprise Subscription</h3>
					<div class="saas-price">
						<span>$249</span><p> Per Month</p>
					</div>
					<ul>
						<li>Unlimited admin user access</li>
						<li>Unlimited client user access
			</li>
						<li>25Gb storage/admin user
</li>

<li>User and client management (share, review, collaborate)</li>
<li>Payment integration</li>
<li>White label</li>
<li>File management</li>
<li>API integration</li>

					</ul>
					<a class="join_today landing_button" href="/register">sign up for free trial</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="s_reviews" class="affiliate-section saas-rev-sec">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="left-review-banner">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/reviewr-image.jpg" class="">
				</div>
			</div>
				<div class="col-md-6">
				<h3>PROOF from our Clients</h3>
                                    <div id="saas-review" class="saas_reviews owl-carousel owl-theme ">
                                            <div class="saas_review_item">
                                                    <p>The one thing which, as an organization, has been picking on our operations is managing our clients’ project database. With PROOF, we were finally able to consolidate all our data at one place and manage our work systematically. We are extremely glad to find such a helpful tool and are happy with the quick response on every service request raised to the GraphicsZoo’s team. They are thorough professionals. We would definitely recommend using PROOF.</p>
                                                    <name>-PROOF proved to be the best</name>
                                            </div>
                                            <div class="saas_review_item">
                                                    <p>Managing graphic designing projects can be extremely tricky and we found PROOF to be a very user friendly tool. It is easy to navigate and makes it simpler to manage the database of all our projects. Highly recommended. </p>
                                                    <name>-Life became simpler with PROOF</name>
                                            </div>
                                            <div class="saas_review_item">
                                                    <p>We are a very consumer-focused organization, and quality as well as punctuality is the core ethics of our business operations. From a company’s perspective, the entire organization does better when we know where to look for to access a particular data, and PROOF by GraphicsZoo has been a big part of that process.</p>
                                                    <name>-Customer satisfaction is everything for us</name>
                                            </div>
                                    </div>
				</div>		
			</div>
		</div>
	</div>
</section>

<section id="saas-intersted" class="affiliate-section saas-intersted">
	<div class="container">
		<div class="intersted-inner text-center">
		<h2 class="affiliate-sub-heading saas-heading"> Want more PROOF? </h2>
		<p> To learn more about our PROOF and how it can help scale up your own graphic designing services in a very short time.</p>
		<a class="join_today landing_button" href="/register">Connect with us today</a>
		</div>	
	</div>
</section>

<script>
$(document).ready(function() {
  $('.nav-menu li a').click(function(e) {
  	var targetHref = $(this).attr('href');
	$('html, body').animate({
		scrollTop: $(targetHref).offset().top
	}, 1000);
    e.preventDefault();
  });
});
</script>