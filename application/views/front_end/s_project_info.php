<?php //echo "<pre>";print_r($data);exit;     ?>
<link rel="shortcut icon canonical" type="image/x-icon" href="http://localhost/graphicszoo/favicon.ico">
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/bootstrap.min.css" rel="stylesheet canonical">
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/reset.css" rel="stylesheet canonical">
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/adjust.css" rel="stylesheet canonical">
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/style.css" rel="stylesheet canonical">
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/responsive.css" rel="stylesheet canonical">
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/owl/owl.carousel.css" rel="stylesheet canonical">
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/custom_style_for_all.css" rel="stylesheet canonical">
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/multi.css" rel="stylesheet canonical">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/bootstrap.min.js"></script>
<style type="text/css" media="screen">
    header.nav-section{display: none}
    div#Usernamemodal input.btn-red {
        border: 0;
        color: #fff;
        font-size: 14px;
        font-weight: 500;
        text-transform: uppercase;
        padding: 8px 16px;
        border-radius: 4px;
    }
    .msgk-umsxxt a{
        color: #0f3147 !important;
    }
</style>
<?php
if ($data[0]['status'] == "checkforapprove") {
    $status = "Review design";
    $color = "agency_review";
} elseif ($data[0]['status'] == "active") {
    $status = "In Progress";
    $color = "agency_inprogress";
} elseif ($data[0]['status'] == "disapprove") {
    $status = "Revision";
    $color = "agency_revision";
} elseif ($data[0]['status'] == "pending" || $data[0]['status'] == "assign") {
    $status = "In Queue";
    $color = "inqueue_status_button";
} elseif ($data[0]['status'] == "hold") {
    $status = "On Hold";
    $color = "hold_status_button";
} elseif ($data[0]['status'] == "cancel") {
    $status = "cancelled";
    $color = "cancel_status_button";
} elseif ($data[0]['status'] == "approved") {
    $status = "completed";
    $color = "completed_status_button";
} elseif ($data[0]['status'] == "draft") {
    $status = "draft";
    $color = "draft_status_button";
} else {
    $status = "";
    $color = "greentext";
}
$docArrOffice = array('doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt', 'ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
$font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
$vedioArr = array('mp4', 'Mov');
$audioArr = array('mp3');
$ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');
$zipArr = array('zip', 'rar');
if($table == 'link_sharing'){
    $filefunc = imageRequestchatUrl;
}else{
  $filefunc = imageRequestchatUrl_SAAS;   
}


?>
<section class="project-info-page">
    <div class="content_section">
        <?php //echo "<pre/>";print_r($userdata);  ?>
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div id="message" class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div id="message" class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_success'); ?>
                </p>
            </div>
        <?php }
        ?>

        <?php preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $matches); ?>
        <?php
        if (isset($_SERVER['HTTP_REFERER'])) {
            if (strpos($_SERVER['HTTP_REFERER'], 'customer/request/project-info') == false) {
                $last_url = explode('?', $_SERVER['HTTP_REFERER'])[0];
                $_SESSION['lastUrl'] = $last_url;
            } else {
                if (isset($_SESSION['lastUrl'])) {
                    $last_url = $_SESSION['lastUrl'];
                } else {
                    $last_url = base_url() . '/qa/dashboard';
                }
            }
        } else {
            $last_url = isset($_SESSION['lastUrl']) ? $_SESSION['lastUrl'] : '';
        }
        ?>
        <div class="content_wrapper">
            <div class="rheight-xt d-progress">
                <h3 class="head-b">
<!--                    <p class="savecols-col left">
                        <a href="<?php //echo $last_url;     ?>?status=<?php //echo $last_word = $matches[0];     ?>" class="backbtns">
                            <i class="icon-gz_back_icon"></i>
                        </a>
                    </p>-->
                    <?php echo $data[0]['title']; ?>
                </h3>
                <div class="focusuyt">
                    <ul class="h-c-r">
                        <li class="dropdown btn-d-status curnt_req_status <?php echo $color; ?>">
                            <a href="javascript:void(0)"><?php echo $status; ?> <span> <i class="fas fa-caret-down"></i></span></a>

                            <?php if (($data[0]['status'] == "checkforapprove" || $data[0]['status'] == "active" || $data[0]['status'] == "disapprove") && ($permissiondata[0]['mark_as_completed'] == '1' || $permissiondata[0]['canapprove_file'] != 0)) { ?>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a class="btn-d green checkfeedbacksubmit"  data-current_status="<?php echo $data[0]['status']; ?>" data-reqid="<?php echo $data[0]['id'] ?>" href="<?php echo base_url() ?>account/request/markAsCompleted/<?php echo $permissiondata[0]['request_id']; ?>/<?php echo $permissiondata[0]['share_key']; ?>/<?php echo $userdata[0]['id']; ?>">Mark as completed <span></span></a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row flex-show">
                <div class="col-sm-8">
                    <ul id="status_check" class="list-unstyled list-header-blog" role="tablist" style="border:none;">
                        <li class="active" id="1"><a data-toggle="tab" data-isloaded="0" data-view="" data-status="active,disapprove,assign,pending,checkforapprove" href="#designs_request_tab" role="tab">Project Drafts </a></li>
                        <li class="" id="2"><a class="nav-link tabmenu" data-isloaded="0" data-view="" data-status="draft" data-toggle="tab" href="#inprogressrequest" role="tab">Project Information </a></li>
                    </ul>
                </div>
            </div>
            <div class="project-row-qq1">
                <div>
                    <div class="orb-xb-col right">
                        <div class="tab-content">
                            <div data-group="1" data-loaded="" class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                                <div class="orbxb-img-xx3s two-can">
                                    <div class="row-designdraftnew">
                                        <?php if ($data[0]['designer_attachment']): ?>
                                            <ul class="list-unstyled clearfix list-designdraftnew">
                                                <?php
                                                //echo count($data[0]['designer_attachment']);exit;
                                                $divide = ($count_designerfile) % 3;
                                                //echo $divide;
                                                for ($i = 0; $i < count($data[0]['designer_attachment']); $i++) :
                                                    if ($divide == 2) {
                                                        if ($i <= 1) {
                                                            $class = "col-md-6 big_box";
                                                        } else {
                                                            $class = "col-md-4";
                                                        }
                                                    } else {
                                                        $class = "col-md-4";
                                                    }
                                                    $ap = $data[0]['designer_attachment'][$i]['status'];
                                                    if (($ap != "pending") && ($ap != "Reject")) {
                                                        ?>
                                                        <li class="<?php echo $class; ?>">
                                                            <?php
                                                            if ($permissiondata[0]['cron_msg'] == 1) {
                                                                $urldraftpage = base_url() . "s_project_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $userdata[0]['id'];
                                                            } else {
                                                                $urldraftpage = base_url() . "S_link_sharing/s_project_image_view/" . $permissiondata[0]['share_key'] . "/" . $data[0]['designer_attachment'][$i]['id'];
                                                            }
                                                            ?>
                                                            <div class="figimg-one">
                                                                <div class="draft-go">
                                                                    <?php if ($data[0]['designer_attachment'][$i]['status'] == "Approve" && $data[0]['status'] == "approved") { ?>
                                                                        <span class="notestxx">
                                                                            <p class="text-right">
                                                                                <span class="notapp-roved">Approved</span>
                                                                            </p>
                                                                        </span> 
                                                                    <?php } ?>
                                                                    <?php if ($data[0]['designer_attachment'][$i]['customer_seen'] == 0) { ?>
                                                                        <div class="status_label">
                                                                            <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                            <g>
                                                                            <path fill-rule="evenodd" fill="#A02302" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                            <path fill-rule="evenodd" fill="#A02302" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                            <path fill-rule="evenodd" fill="#F15A29" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                            </g>
                                                                            </svg>
                                                                            <span class="adnewpp">New Upload</span>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <?php
                                                                    $type = substr($data[0]['designer_attachment'][$i]['file_name'], strrpos($data[0]['designer_attachment'][$i]['file_name'], '.') + 1);
                                                                    ?>
                                                                    <div class="draft_img_wrapper">
                                                                        <img src="<?php echo imageUrl_SAAS($type, $data[0]['id'], $data[0]['designer_attachment'][$i]['file_name']); ?>" class="img-responsive" />
                                                                    </div>
                                                                    <div class="view-share">
                                                                        <a class="view-draft" href="<?php echo $urldraftpage; ?>">
                                                                            <i class="fas fa-eye"></i> View
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="tm-heading">
                                                                    <div class="iconwithtitle">
                                                                        <a href="<?php echo $urldraftpage; ?>">    
                                                                            <?php
                                                                            $filename = $data[0]['designer_attachment'][$i]['file_name'];
                                                                            $imagePreFix = substr($filename, 0, strpos($filename, "."));
                                                                            ?>
                                                                            <h3><?php echo $imagePreFix; ?></h3>
                                                                        </a>

                                                                    </div>
                                                                    <div class="chatbox-add">
                                                                        <p><?php echo $data[0]['designer_attachment'][$i]['created']; ?></p>
                                                                        <a href="<?php echo $urldraftpage; ?>"> 
                                                                            <i class="icon-gz_message_icon"></i>
                                                                            <?php for ($j = 0; $j < count($file_chat_array); $j++): ?>
                                                                                <?php if ($file_chat_array[$j]['id'] == $data[0]['designer_attachment'][$i]['id']): ?>
                                                                                    <span class="numcircle-box-01"><?php echo $file_chat_array[$j]['count']; ?></span>
                                                                                <?php endif; ?>
                                                                            <?php endfor; ?>
                                                                        </a>

                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </li>
                                                    <?php } endfor; ?>											
                                            </ul>
                                        <?php else: ?>
                                            <div class="figimg-one no_img" style="border:none;">
                                                <?php if ($data[0]['status'] == 'draft') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Design Project is in Draft
                                                    </h3>
                                                    <a class="btn btn-x draft_btn1" href="<?php echo base_url(); ?>customer/request/new_request_brief/<?php echo $data[0]['id']; ?>?rep=1">Edit</a>
                                                    <a class="btn btn-x draft_btn2" href="<?php echo base_url(); ?>customer/request/new_request_review/<?php echo $data[0]['id']; ?>?rep=1">Publish</a>
                                                <?php } elseif ($data[0]['status'] == 'assign') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Design Project is in queue
                                                    </h3>
                                                <?php } elseif ($data[0]['status'] == 'active') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Designer is working on the designs
                                                    </h3>
                                                <?php } ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div data-group="1" data-loaded="" data-search-text="" class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">

                                <div class="project-row-qq1">
                                    <div class="descol right-instbox"></div>
                                    <div class="designdraft-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="designdraft-views">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Project Title</label>
                                                        <p class="space-a"></p>
                                                        <div class="review-textcolww"><?php echo $data[0]['title']; ?></div>
                                                    </div>
                                                    <?php if (!empty($data[0]['design_dimension'])) { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Design Dimension</label>
                                                            <p class="space-a"></p>
                                                            <div class="review-textcolww"><?php echo $data[0]['design_dimension']; ?>                                       
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($data[0]['design_colors'])) { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Color Pallete</label>
                                                            <!-- Color Plates -->
                                                            <div class="form-group">
                                                                <?php
                                                                if ($data[0]['design_colors']) {
                                                                    $color = explode(",", $data[0]['design_colors']);
                                                                }
                                                                ?>
                                                                <p class="space-a"></p>
                                                                <?php if ($data[0]['design_colors'] && in_array("Let Designer Choose", $color)) { ?>
                                                                    <div class="radio-check-kk1">
                                                                        <label> 
                                                                            <div class="">
                                                                                <span class="review-colww left">
                                                                                    <span class="review-textcolww">
                                                                                        Let Designer Choose for me.</span>
                                                                                </span>

                                                                            </div>
                                                                        </label>
                                                                    </div>
                                                                    <?php
                                                                } else {
                                                                    $flag = 0;
                                                                    ?>

                                                                    <p class="space-a"></p>

                                                                    <div class="lessrows">
                                                                        <div class="right-lesscol noflexes">
                                                                            <div class="plan-boxex-xx6 clearfix">
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("blue", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id1" class="radio-box-xx2"> 
                                                                                        <div class="check-main-xx3">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-1.png" class="img-responsive">
                                                                                            </figure>                                       
                                                                                            <h3 class="sub-head-c text-center">Blue
                                                                                                <!-- <label for="colorbest">    Blues </label>                               -->
                                                                                            </h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("aqua", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id2" class="radio-box-xx2"> 
                                                                                        <span class="checkmark"></span>
                                                                                        <div class="check-main-xx3">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-2.png" class="img-responsive">
                                                                                            </figure>                                       
                                                                                            <h3 class="sub-head-c text-center">Auqa</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("green", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id3" class="radio-box-xx2"> 
                                                                                        <div class="check-main-xx3">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-3.png" class="img-responsive">
                                                                                            </figure>                                       
                                                                                            <h3 class="sub-head-c text-center">Greens</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("purple", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id4" class="radio-box-xx2"> 
                                                                                        <div class="check-main-xx3">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-4.png" class="img-responsive">
                                                                                            </figure>                                       
                                                                                            <h3 class="sub-head-c text-center">Purple</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("pink", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id5" class="radio-box-xx2"> 
                                                                                        <div class="check-main-xx3" onclick="funcheck()">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-5.png" class="img-responsive">
                                                                                            </figure>                                       
                                                                                            <h3 class="sub-head-c text-center">Pink</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("black", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id6" class="radio-box-xx2"> 
                                                                                        <div class="check-main-xx3" onclick="funcheck()">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-6.png" class="img-responsive">
                                                                                            </figure>                                       
                                                                                            <h3 class="sub-head-c text-center">Pink</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("orange", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id7" class="radio-box-xx2"> 
                                                                                        <div class="check-main-xx3" onclick="funcheck()">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-7.png" class="img-responsive">
                                                                                            </figure>                                       
                                                                                            <h3 class="sub-head-c text-center">Pink</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>

                                                                            </div>  
                                                                        </div>
                                                                    </div>

                                                                    <p class="space-a"></p>
                                                                    <?php if ($flag != 1): ?>
                                                                        <div class="radio-check-kk1">
                                                                            <label> 
                                                                                <div class="">
                                                                                    <span class="review-colww left">
                                                                                        <span class="review-textcolww">
                                                                                            <?php
                                                                                            echo $data[0]['design_colors'];
                                                                                            ?></span>
                                                                                    </span>
                                                                                </div>
                                                                            </label>
                                                                        </div>
                                                                    <?php endif ?>

                                                                <?php }
                                                                ?>
                                                                <p class="space-a"></p>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($data[0]['designer'])) { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Design Software Requirement</label>
                                                            <p class="space-a"></p>
                                                            <div class="review-textcolww"><?php echo $data[0]['designer']; ?>                                       
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($data[0]['color_pref'])) { ?>
                                                        <div class="form-group goup-x1">
                                                           <label class="label-x2"><span class="prowest"></span> Color pallete</label>
                                                           <div class="review-textcolww">
                                                              <?php 
                                                              echo ucfirst($data[0]['color_pref'])."<br/>"; 
                                                              if($data[0]['color_values']){?>
                                                               <span class="color_theme">
                                                                 <?php echo $data[0]['color_values']; } ?>
                                                               </span>
                                                           </div>
                                                        </div>
                                                        <?php } ?>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Deliverables</label>
                                                        <p class="space-a"></p>
                                                        <div class="review-textcolww"><?php echo $data[0]['deliverables']; ?></div>
                                                    </div>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Category </label>
                                                        <p class="space-a"></p>
                                                        <div class="review-textcolww">
                                                            <span>
                                                                <?php
                                                                echo isset($data['cat_name']) ? $data['cat_name'] : $data[0]['category'];
                                                                if ($data[0]['logo_brand'] != '') {
                                                                    ?></span><span><?php
                                                                    echo $data[0]['logo_brand'];
                                                                } elseif ($data['subcat_name'] != '') {
                                                                    ?></span><span><?php
                                                                        echo $data['subcat_name'];
                                                                    }
                                                                    ?></span></div>
                                                    </div>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Brand Profile</label>
                                                        <p class="space-a"></p>
                                                        <div class="review-textcolww">
                                                            <?php if ($branddata[0]['brand_name'] != '') { ?>
                                                                <a class="showbrand_profile" data-toggle="modal" data-target="#brand_profile"><i class="fa fa-user"></i> <?php echo $branddata[0]['brand_name']; ?></a>
                                                            <?php } else { ?>
                                                                No brand profile selected
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <!--******questions & answer*******-->
                                                    <?php
                                                    if (!empty($request_meta)) {
                                                        foreach ($request_meta as $qkey => $qval) {
                                                            if ($qval['question_label'] == 'DESIGN DIMENSIONS' || $qval['question_label'] == 'DESCRIPTION') {
                                                                if ($qval['question_label'] == 'DESIGN DIMENSIONS') {
                                                                    if ($qval['answer'] != '') {
                                                                        $qval['answer'] = $qval['answer'];
                                                                    } else {
                                                                        $qval['answer'] = $data[0]['design_dimension'];
                                                                    }
                                                                }
                                                                if ($qval['question_label'] == 'DESCRIPTION') {
                                                                    if ($qval['answer'] != '') {
                                                                        $qval['answer'] = $qval['answer'];
                                                                    } else {
                                                                        $qval['answer'] = $data[0]['description'];
                                                                    }
                                                                }
                                                            } else {
                                                                $qval['answer'] = $qval['answer'];
                                                            }

                                                            if(isset($qval['answer']) && $qval['answer'] != ''){
                                                                if(strlen(htmlspecialchars($qval['answer'])) > 100){
                                                                    $answer = substr(htmlspecialchars($qval['answer']),0,97).'... <a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.htmlspecialchars($qval['answer']).'</div><a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a>';
                                                                }else{
                                                                    $answer = htmlspecialchars($qval['answer']);
                                                                }
                                                            }else{
                                                                $answer = "N/A";
                                                            }
                                                            /******************Description excluded***************************/
                                                            if($qval['question_label'] != 'DESCRIPTION'){
                                                            ?>
                                                            <div class="form-group goup-x1">
                                                                <label class="label-x2"> <?php echo $qval['question_label']; ?></label>
                                                                <div class="review-textcolww">
                                                                     <?php echo $answer; ?>
                                                                </div>
                                                            </div>      
                                                            <?php } else { ?>
                                                            <div class="form-group goup-x1" style="width:100%;">
                                                                <label class="label-x2"> <?php echo $qval['question_label']; ?></label>
                                                                <div class="review-textcolww">
                                                                    <pre> <?php echo $answer; ?></pre>
                                                                </div>
                                                            </div> 
                                                            <?php } }
                                                    }else{ ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"> Design Dimension</label>
                                                            <div class="review-textcolww">
                                                                <?php echo isset($data[0]['design_dimension']) ? $data[0]['design_dimension']: 'N/A'; ?>
                                                            </div>
                                                        </div>
                                                    <div class="form-group goup-x1" style="width:100%;">
                                                            <label class="label-x2"> Description</label>
                                                            <div class="review-textcolww">
                                                                <pre><?php echo isset($data[0]['description']) ? (strlen(htmlspecialchars($data[0]['description'])) > 100)?substr(htmlspecialchars($data[0]['description']),0,97).'... <a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.htmlspecialchars($data[0]['description']).'</div> <a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a> ':htmlspecialchars($data[0]['description']) : 'N/A'; ?></pre>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                    ?>
                                                    <!--*********samples************-->
                                                    <?php if (!empty($sampledata)) { ?>
                                                        <div class="clearfix"></div>
                                                        <h3 class="head-b base-heading request_heading">Request Samples</h3>
                                                        <ul class="list-unstyled list-accessimg">
                                                            <?php
                                                            //echo "<pre/>";print_R($sampledata);  
                                                            foreach ($sampledata as $skey => $sval) {
                                                                ?>
                                                                <li>
                                                                    <div class="accimgbx33">
                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'samples/' . $sval['sample_id'] . '/' . $sval['sample_material']; ?>" />
                                                                        <!--                                                            <div class="viewDelete">
                                                                                                                                        <a href="<?php //echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name'];     ?>" data-fancybox="images">
                                                                                                                                            View
                                                                                                                                        </a>
                                                                                                                                        <a href="<?php //echo base_url() . "customer/Request/download_projectfile/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']     ?>"> Download
                                                                                                                                        </a>
                                                                                                                                        <p class="cross-btnlink">
                                                                                                                                            <a href="javascript:void(0)" class="delete_file_req" data-id="<?php //echo $data[0]['id'];     ?>" data-name="<?php //echo $data[0]['customer_attachment'][$i]['file_name'];     ?>">
                                                                                                                                                <span>Delete</span>
                                                                                                                                            </a>
                                                                                                                                        </p>
                                                                                                                                    </div>-->
                                                                    </div>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                    <div class="light-box" id="imgrmv">
                                                        <h3 class="base-heading">Attachments</h3>
                                                        <ul class="list-unstyled list-accessimg">
                                                            <?php
                                                            if (!empty($data[0]['customer_attachment'])) {
                                                                for ($i = 0; $i < count($data[0]['customer_attachment']); $i++) {
                                                                    ?>

                                                                    <?php
                                                                    $type = substr($data[0]['customer_attachment'][$i]['file_name'], strrpos($data[0]['customer_attachment'][$i]['file_name'], '.') + 1);
                                                                    if ($type == "pdf") {
                                                                        ?>
                                                                        <li>
                                                                            <div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                                                                            </div>
                                                                        </li>
                                                                    <?php } elseif ($type == "odt" || $type == "ods") {
                                                                        ?>
                                                                        <li>
                                                                            <div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                                                                            </div>
                                                                        </li>
                                                                        <?php
                                                                    } elseif ($type == "zip") {
                                                                        ?>
                                                                        <li>
                                                                            <div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                                                                            </div>
                                                                        </li>
                                                                    <?php } elseif ($type == "mov" || $type == "mp4") {
                                                                        ?>
                                                                        <li>
                                                                            <div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>

                                                                            </div>
                                                                        </li>
                                                                    <?php } elseif ($type == "doc" || $type == "docx") { ?>
                                                                        <li><div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/docx.png" height="150"/>

                                                                            </div>
                                                                        </li>
                                                                    <?php } elseif ($type == "psd") { ?>
                                                                        <li><div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>

                                                                            </div>
                                                                        </li>
                                                                    <?php } elseif ($type == "rar") {
                                                                        ?>
                                                                        <li><div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/rar.jpg" height="150"/>

                                                                            </div>
                                                                        </li>
                                                                    <?php } elseif ($type == "ai") { ?>
                                                                        <li><div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/ai.png" height="150"/>

                                                                            </div>
                                                                        </li>
                                                                    <?php } elseif ($type == "tiff") { ?>
                                                                        <li><div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/tiff.jpeg" height="150"/>

                                                                            </div>
                                                                        </li>
                                                                    <?php } elseif ($type == "eps") { ?>
                                                                        <li><div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/eps.png" height="150"/>

                                                                            </div>
                                                                        </li>
                                                                    <?php } else { ?>
                                                                        <li><div class="accimgbx33">
                                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" />
                                                                            </div>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                            } else {
                                                                echo "<div class='noatch'>There is no attachment.</div>";
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="side_bar">
        <div class="gz-sidebar" data-position='right' data-scrollTo='tooltip'>

            <div class="project-row-qq2" style="overflow: unset;">
                <div class="cmmtype-box">
                    <div class="cmmtype-row">
                        <textarea class="pstcmm customer_pro text_<?php echo $data[0]['id']; ?>" Placeholder="Type a message..." <?php
                        if ($data[0]['status'] == "assign" || ((isset($permissiondata[0]['commenting']) && $permissiondata[0]['commenting'] == 0)) || ((isset($permissiondata[0]['cancomment_on_req'])) && $permissiondata[0]['cancomment_on_req'] == 0)) {
                            echo "disabled";
                        }
                        ?>></textarea>
                        <div class="send-attech">
<!--                        <label for="image">
                            <input type="file" name="shre_file[]" id="shre_file" data-withornot="1" data-reqID="<?php echo $data[0]['id']; ?>" style="display:none;" multiple onchange="uploadChatfile(event, '<?php echo $data[0]['id']; ?>', 'customer', '<?php echo $_SESSION['user_id']; ?>', '<?php echo $data[0]['designer_id']; ?>', 'designer', '<?php echo $data[0]['first_name']; ?>', '<?php echo $data[0]['profile_picture']; ?>', 'customer_seen')"/>
                            <span class="attchmnt" title="Add Files"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
                            <input type="hidden" value="" name="saved_file[]" class="delete_file"/>
                        </label>-->
                                  <?php if (($userdata[0]['profile_picture'] != "") || (file_exists(FS_PATH_PUBLIC_UPLOADS_PROFILE . $userdata[0]['profile_picture']))) { ?>
                            <span <?php
                            if ($data[0]['status'] == "assign" || ((isset($permissiondata[0]['commenting']) && $permissiondata[0]['commenting'] == 0)) || ((isset($permissiondata[0]['cancomment_on_req'])) && $permissiondata[0]['cancomment_on_req'] == 0)) {
                                echo "disabled";
                            }
                            ?> class="cmmsend chatsendbtn send_request_chat send" data-profile_pic="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . isset($userdata[0]['profile_picture']) ? $userdata[0]['profile_picture'] : 'user-admin.png'; ?>" data-requestid="<?php echo $data[0]['id']; ?>" data-senderrole="customer" data-senderid="<?php echo isset($userdata[0]['id']) ? $userdata[0]['id'] : '0'; ?>" data-receiverid="<?php echo $data[0]['designer_id']; ?>" data-receiverrole="designer" data-sendername="<?php echo isset($userdata[0]['first_name']) ? $userdata[0]['first_name'] : $userdata[0]['shared_user_name']; ?>"  onclick="message(this);
                                        return false;" <?php
                                if ($data[0]['status'] == "assign" || ((isset($permissiondata[0]['commenting']) && $permissiondata[0]['commenting'] == 0)) || ((isset($permissiondata[0]['cancomment_on_req'])) && $permissiondata[0]['cancomment_on_req'] == 0)) {
                                    echo "disabled";
                                }
                                ?>><button class="cmmsendbtn">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-chat-send.png" class="img-responsive"/></button>
                            </span>   

                        <?php } else { ?>

                            <span <?php
                            if ($data[0]['status'] == "assign" || ((isset($permissiondata[0]['commenting']) && $permissiondata[0]['commenting'] == 0)) || ((isset($permissiondata[0]['cancomment_on_req'])) && $permissiondata[0]['cancomment_on_req'] == 0)) {
                                echo "disabled";
                            }
                            ?> class="cmmsend chatsendbtn send_request_chat send" data-profile_pic="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" data-requestid="<?php echo $data[0]['id']; ?>" data-senderrole="customer" data-senderid="<?php echo isset($userdata[0]['id']) ? $userdata[0]['id'] : '0'; ?>" data-receiverid="<?php echo $data[0]['designer_id']; ?>" data-receiverrole="designer" data-sendername="<?php echo isset($userdata[0]['first_name']) ? $userdata[0]['first_name'] : $userdata[0]['shared_user_name']; ?>"  onclick="message(this);
                                        return false;" <?php
                                if ($data[0]['status'] == "assign" || ((isset($permissiondata[0]['commenting']) && $permissiondata[0]['commenting'] == 0)) || ((isset($permissiondata[0]['cancomment_on_req'])) && $permissiondata[0]['cancomment_on_req'] == 0)) {
                                    echo "disabled";
                                }
                                ?>><button class="cmmsendbtn">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-chat-send.png" class="img-responsive"/></button>
                            </span>

                        <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="chat-box-row" id="allChats">
                    <div class="read-msg-box two-can" id="message_container" style="padding:0px; height:320px; overflow-y: scroll;overflow-x: hidden;width: 99%">
                        <?php
//                        echo "<pre/>";print_r($chat_request);
                        for ($j = 0; $j < count($chat_request); $j++) {
                            $chatdata = $chat_request[$j];
//                            echo "<pre/>";print_r($chatdata);
                            $type = substr($chatdata['message'], strrpos($chatdata['message'], '.') + 1);
                            $updateurl = '';
                            if (in_array($type, $docArrOffice)) {
                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                                $class = "doc-type-file";
                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                            } elseif (in_array($type, $font_file)) {
                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                                $class = "doc-type-file";
                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                            } elseif (in_array($type, $zipArr)) {
                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-zip.svg';
                                $class = "doc-type-zip";
                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                            } elseif (in_array($type, $ImageArr)) {
                                $imgVar = $filefunc($type, $chatdata['request_id'], $chatdata['message']);
                                $updateurl = '/_thumb';
                                $class = "doc-type-image";
                                $exthtml = '';
                            }
                            $basename = $chatdata['message'];
                            $basename = strlen($basename) > 20 ? substr($basename, 0, 20) . "..." . $type : $basename;
                            $data_src = FS_PATH_PUBLIC_UPLOADS.'requestmainchat/'.$chatdata['request_id'].'/'.$chatdata['message'];
                            //echo "<pre/>";print_r($chatdata);
                            ?>
                            <div class="msgk-chatrow">
                                <?php if ($chat_request[$j]['with_or_not_customer'] == 1) { ?>
                                    <?php if ($chat_request[$j]['sender_id'] != $login_user_id && $chat_request[$j]['sender_id'] != 0) { ?>
                                        <!-- Left Start -->
                                        <div class="msgk-user-chat msgk-left <?php echo $chat_request[$j]['sender_type']; ?>">
                                            <!-- If Current Message and Previous Message are not Same Sender Type-->
                                            <?php if ($j == 0) { ?>
                                                <figure class="msgk-uimg">
                                                    <a class="msgk-uleft" href="javascript:void(0)" title="<?php echo $chat_request[$j]['first_name']; ?>">
                                                        <?php
                                                        if ($chat_request[$j]['profile_picture'] != "" || $chat_request[$j]['sender_data'][0]['profile_picture'] != "") {
                                                            $image = isset($chat_request[$j]['profile_picture']) ? $chat_request[$j]['profile_picture'] : $chat_request[$j]['sender_data'][0]['profile_picture'];
                                                            //echo $image;
                                                            ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $image; ?>" class="img-responsive">
                                                        <?php } else { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive"/>
                                                        <?php } ?>
                                                    </a>
                                                </figure>
                                            <?php } ?>
                                            <?php
                                            if ($j != 0) {
                                                //echo "<pre/>";print_r($chat_request[$j]);
                                                ?>
                                                <?php if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type'] || ($chat_request[$j]['sender_id'] != $chat_request[$j - 1]['sender_id'] || $chat_request[$j]['shared_user_name'] != $chat_request[$j - 1]['shared_user_name'] )) { ?>
                                                    <figure class="msgk-uimg">
                                                        <a class="msgk-uleft" href="javascript:void(0)" title="<?php echo $chat_request[$j]['first_name']; ?>">
                                                            <?php if ($chat_request[$j]['profile_picture'] != "") { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $chat_request[$j]['profile_picture']; ?>" class="img-responsive">
                                                            <?php } else { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive"/>
                                                            <?php } ?>
                                                        </a>
                                                    </figure>
                                                    <p class="msgk-udate right">
                                                        <?php echo $chat_request[$j]['chat_created_date']; ?>
                                                    </p>
                                                    <div class="msgk-mn">
                                                        <div class="msgk-umsgbox">
                                                            <?php if($chat_request[$j]['is_filetype'] != 1) { ?>
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                            <?php }else { ?>
                                                            <span class="msgk-umsxxt">
                                                            <div class="contain-info <?php echo (!in_array($type,$ImageArr)) ? 'only_file_class' :''; ?>">
                                                                <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>" rel="nofollow">
                                                                    <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                                                                </a>
                                                                <div class="download-file">
                                                                    <div class="file-name"><h4><b><?php echo $basename; ?></b></h4>
                                                                    </div>
                                                                <a href="<?php echo base_url() ?>customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" rel="nofollow" target="_blank"> 
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                                                                </a>
                                                            </div>
                                                            </div>
                                                            </span>
                                                            <?php } ?>
                                                         </div>
                                                    </div>


                                                    <!-- If Current Message and Next Message are not Same Sender Type-->
                                                <?php } elseif (array_key_exists($j + 1, $chat_request) && $chat_request[$j]['sender_type'] != $chat_request[$j + 1]['sender_type']) { ?>
                                                    <p class="msgk-udate t7">
                                                        <?php echo $chat_request[$j]['chat_created_date']; ?>
                                                    </p>
                                                    <div class="msgk-mn last_message">
                                                        <div class="msgk-umsgbox">
                                                            <?php if($chat_request[$j]['is_filetype'] != 1) { ?>
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                            <?php }else { ?>
                                                            <span class="msgk-umsxxt">
                                                            <div class="contain-info <?php echo (!in_array($type,$ImageArr)) ? 'only_file_class' :''; ?>">
                                                                <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>" rel="nofollow">
                                                                    <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                                                                </a>
                                                                <div class="download-file">
                                                                    <div class="file-name"><h4><b><?php echo $basename; ?></b></h4>
                                                                    </div>
                                                                <a href="<?php echo base_url() ?>customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" rel="nofollow" target="_blank"> 
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                                                                </a>
                                                            </div>
                                                            </div>
                                                            </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>

                                                    <!-- If Current Message and Previous Message are Same Sender Type-->
                                                <?php } else { ?>
                                                    <p class="msgk-udate t6">
                                                        <?php echo $chat_request[$j]['chat_created_date']; ?>
                                                    </p>
                                                    <div class="msgk-mn center_message">
                                                        <div class="msgk-umsgbox">
                                                            <?php if($chat_request[$j]['is_filetype'] != 1) { ?>
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                            <?php }else { ?>
                                                            <span class="msgk-umsxxt">
                                                            <div class="contain-info <?php echo (!in_array($type,$ImageArr)) ? 'only_file_class' :''; ?>">
                                                                <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>" rel="nofollow">
                                                                    <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                                                                </a>
                                                                <div class="download-file">
                                                                    <div class="file-name"><h4><b><?php echo $basename; ?></b></h4>
                                                                    </div>
                                                                <a href="<?php echo base_url() ?>account/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" rel="nofollow" target="_blank"> 
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                                                                </a>
                                                            </div>
                                                            </div>
                                                            </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>


                                                <?php } ?>  
                                            <?php } else { ?>
                                                <p class="msgk-udate t5">
                                                    <?php echo $chat_request[$j]['chat_created_date']; ?>
                                                </p>
                                                <div class="msgk-mn center_message">
                                                    <div class="msgk-umsgbox">
                                                        <?php if($chat_request[$j]['is_filetype'] != 1) { ?>
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                            <?php }else { ?>
                                                            <span class="msgk-umsxxt">
                                                            <div class="contain-info <?php echo (!in_array($type,$ImageArr)) ? 'only_file_class' :''; ?>">
                                                                <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>" rel="nofollow">
                                                                    <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                                                                </a>
                                                                <div class="download-file">
                                                                    <div class="file-name"><h4><b><?php echo $basename; ?></b></h4>
                                                                    </div>
                                                                <a href="<?php echo base_url() ?>account/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" rel="nofollow" target="_blank"> 
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                                                                </a>
                                                            </div>
                                                            </div>
                                                            </span>
                                                            <?php } ?>
                                                    </div>
                                                </div>

                                            <?php } ?>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <!-- Left End -->

                                <!-- Right start -->
                                <?php if ($chat_request[$j]['sender_id'] == $login_user_id || $chat_request[$j]['sender_id'] == 0) { ?>
                                    <div class="msgk-user-chat msgk-right <?php echo $chat_request[$j]['sender_type']; ?>">
                                        <p class="msgk-udate t4">
                                            <?php echo $chat_request[$j]['chat_created_date']; ?>
                                            <i class="fas fa-ellipsis-v openchange" data-chatid="<?php echo $chatdata['id']; ?>"></i>
                                        </p>
                                        <div class="time-edit">
                                            <p class="time_msg"><?php echo $chatdata['chat_created_time']; ?></p>
                                            <?php if ($chatdata['is_deleted'] != 1) { ?>
                                                <div class="editDelete editdeletetoggle_<?php echo $chatdata['id']; ?>">
                                                    <div class="open-edit open-edit_<?php echo $chatdata['id']; ?>" style="display:none">
                                                        <a href="javascript:void(0)" class="editmsg" data-editid="<?php echo $chatdata['id']; ?>">edit</a>
                                                        <a href="javascript:void(0)" class="deletemsg" data-req_id="<?php echo $data[0]['id']; ?>" data-delid="<?php echo $chatdata['id']; ?>">delete</a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="msgk-mn-edit edit_main_<?php echo $chatdata['id']; ?>" style="display:none">
                                            <form method="post" id="editMainmsg">
                                                <textarea class="pstcmm sendtext" id="edit_main_msg_<?php echo $chatdata['id']; ?>"><?php echo $chatdata['message']; ?></textarea> 
                                                <a href="javascript:void(0)" class="edit_save"  data-id="<?php echo $chatdata['id']; ?>" data-req_id="<?php echo $data[0]['id']; ?>">Save</a> 
                                                <a href="javascript:void(0)" class="cancel_main" data-msgid="<?php echo $chatdata['id']; ?>">Cancel</a>
                                            </form>
                                        </div>
                                        <div class="msgk-mn msg_desc_<?php echo $chatdata['id']; ?>">
                                            <div class="msgk-umsgbox">
                                                <?php
                                                $varmsg = '';
                                                if ($chatdata['is_deleted'] == 1 && $chatdata['is_edited'] != 1) {
                                                    
                                                }
                                                if ($chatdata['is_deleted'] == 1 && $chatdata['is_edited'] != 1 || ($chatdata['is_edited'] == 1 && $chatdata['is_deleted'] == 1)) {
                                                    $varmsg = '<div class="edited_msg deleted_msg"><i class="fa fa-ban" aria-hidden="true"></i> You have deleted this message.</div>';
                                                } elseif ($chatdata['is_edited'] == 1 && $chatdata['is_deleted'] != 1) {
                                                    $varmsg = '<div class="edited_msg">' . $chatdata['message'] . '</div>';
                                                } else {
                                                    $varmsg = $chatdata['message'];
                                                }
                                                ?>

                                                <pre>
                                                    <span class="edit_icon_<?php echo $chatdata['id']; ?> edit_icon_msg">
                                                        <?php if ($chatdata['is_edited'] == 1 && $chatdata['is_deleted'] != 1) { ?>
                                                                        <i class="fas fa-pen"></i>
                                                        <?php } ?>
                                                    </span>
                                                    <span class="msgk-umsxxt took_<?php echo $chatdata['id']; ?>"><?php echo $varmsg; ?></span>
                                                </pre>
                                            </div>  
                                        </div>
                                    </div>
                                    <!-- Customer Right End -->
                                <?php } ?>
                            </div>
                            <!-- Customer Left End -->
                        <?php } ?>
                    </div>
                    <?php //echo "<pre/>";print_r($permissiondata);    ?>

                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="brand_profile" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="cli-ent-model-box">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">

                        </header>
                        <div class="display-pic">
                            <div class="dp-here">
                                <?php
                                $type = substr($brand_materials_files['latest_logo'], strrpos($brand_materials_files['latest_logo'], '.') + 1);
                                $allow_type = array("jpg", "jpeg", "png", "gif");
                                if (isset($brand_materials_files['latest_logo']) && in_array(strtolower($type), $allow_type)) {
                                    ?>
                                    <img src = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" height="150">
                                <?php } elseif (!in_array(strtolower($type), $allow_type)) { ?>
                                    <img src = "<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/all-file.jpg" height="150">
                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/logo-brand-defult.jpg" height="150"/>
                                <?php } ?>

                            </div>
                            <div class="brnd-nm ">
                                <label>Brand Profile</label>
                                <h3><?php echo $branddata[0]['brand_name']; ?></h3>

                            </div>
                            <div class="profile_info">
                                <?php // echo "<pre>";print_r($branddata);    ?>
                                <?php // echo "<pre>";print_r($brand_materials_files);      ?>

                                <div class="profile colm">
                                    <div class = "brand_labal">Website URL </div>
                                    <div class = "brand_info"><?php echo $branddata[0]['website_url']; ?></div>
                                </div>
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class = "brand_labal">Fonts </div>
                                        <div class = "brand_info"><?php echo $branddata[0]['fonts']; ?></div>
                                    </div>
                                    <div class="form-group">
                                        <div class = "brand_labal">Color Preferences</div>
                                        <div class = "brand_info">
                                            <?php echo $branddata[0]['color_preference']; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile colm">
                                    <div class = "brand_labal">Brand Description </div>
                                    <div class = "brand_info"><?php echo $branddata[0]['description']; ?></div>
                                </div>
                            </div>
                            <div class="profile colm materials-sp">
                                <div class = "brand_labal">Brand Logo </div>
                                <div class = "brand_info">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if ($type == "pdf") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                                                        </a>

                                                                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                            <?php } elseif ($type == "zip") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                                                        </a>

                                                                                                            <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                            <?php } elseif ($type == "doc" || $type == "docx") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/docx.png" height="150"/>
                                                        </a>

                                                                                                            <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                            <?php } elseif ($type == "mov" || $type == "mp4") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>
                                                        </a>

                                                                                                            <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                            <?php } elseif ($type == "odt" || $type == "ods") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                                                        </a>

                                                                                                                <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                            <?php } elseif ($type == "psd") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>
                                                        </a>

                                                                                                                <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                            <?php } elseif ($type == "rar") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/rar.jpg" height="150"/>
                                                        </a>

                                                                                                                <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                            <?php } elseif ($type == "ai") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/ai.png" height="150"/>
                                                        </a>

                                                                                                                <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                            <?php } else { ?> 
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <img src = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div></div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>

                                </div>
                            </div>

                            <div class="profile colm materials-sp">
                                <div class = "brand_labal">Market Materials </div>
                                <div class = "brand_info">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'materials_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if ($type == "pdf") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "zip") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "doc" || $type == "docx") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/docx.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "mov" || $type == "mp4") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "odt" || $type == "ods") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "psd") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "rar") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/rar.jpg" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "ai") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" rel="nofollow">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/ai.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } else { ?> 
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <img src = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="profile colm materials-sp">
                                <div class = "brand_labal">Additional Images </div>
                                <div class = "brand_info">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'additional_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if ($type == "pdf") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" rel="nofollow" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "zip") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" rel="nofollow" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "doc" || $type == "docx") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" rel="nofollow" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/docx.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "mov" || $type == "mp4") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" rel="nofollow" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "odt" || $type == "ods") {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" rel="nofollow" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "psd") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" rel="nofollow" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "rar") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" rel="nofollow" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/rar.jpg" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } elseif ($type == "ai") { ?>
                                                <div class="col-md-3 col-sm-6"><div class="accimgbx33">
                                                        <a target="_blank" rel="nofollow" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/ai.png" height="150"/>
                                                        </a>
                                                    </div>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                                </div>
                                            <?php } else { ?> 
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <img src = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div> </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!---------------------modal --------------------------------->
<div class="modal fade sharing-popup in" id="Usernamemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content full_height_modal">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Register your Name</h2>
                <div class="cross_popup" data-dismiss="modal"> x</div>
            </header>
            <div class="modal-body formbody two-can">
                <div class="tab-pane fade in" id="share_private">
                    <form action="<?php echo base_url(); ?>S_link_sharing/SaveSharedusername" class="clearfix" method="post">
                        <div class="col-md-12">
                            <label class="form-group">
                                <p class="label-txt">For doing message, please enter your name first!</p>
                                <input type="hidden" name="share_key" value="<?php echo $permissiondata[0]['share_key']; ?>"/>
                                <input type="text" name="user_name" value="" class="sharemail input"  required=""/>
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                            <div class="input-submitt">
                                <input type="submit" name="savename" class="btn-red" value="Submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


 <!--*********Review on design*************-->
<!--<div class="modal fade slide-3 in" id="f_reviewpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h2 class="popup_h2 del-txt dgn_revw">SEND US YOUR VALUABLE FEEDBACK!</h2>
        </div>
        <div class="modal-body">
            <div class="design_review_form">
                <div class="text-aprt">
                    <form action="<?php echo base_url(); ?>admin/Dashboard/design_review_frm/1" class="f_feedback_frm" method="post" id="f_feedback_frm">
                        <input type="hidden" name="review_draftid" id="review_draftid" value="<?php echo $data[0]['approved_attachment']; ?>">
                        <input type="hidden" name="review_reqid" id="review_reqid" value="<?php echo $data[0]['id']; ?>">
                        <input type="hidden" name="share_key" value="<?php echo $permissiondata[0]['share_key']; ?>"/>
                        <input type="hidden" name="from_markascomplete" id="from_markascomplete" value="">
                        <label class="form-group">
                         <p class="choose-feedbk"> How satisfied are you with the designers updates?</p>
                             <div class="sound-signal">
                                 <div class="form-radion happy_custm">
                                     <input type="radio" name="satisfied_with_design" class="preference" id="soundsignal1" value="5" required="">
                                     <label for="soundsignal1">
                                     <div class="emoji-check"><i class="fas fa-check-circle"></i></div>
                                     <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/happy.svg" class="img-responsive emoji-img"> 
                                     <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/happy-green.svg" class="img-responsive emoji-img-check">   <strong>Love it!  </strong></label>
                                 </div>
                                 <div class="form-radion mid_custm">
                                     <input type="radio" name="satisfied_with_design" id="soundsignal2" class="preference" value="3" required="">
                                     <label for="soundsignal2">
                                     <div class="emoji-check"><i class="fas fa-check-circle"></i></div>
                                     <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/happiness.svg" class="img-responsive emoji-img">
                                     <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/happiness-yellow.svg" class="img-responsive emoji-img-check">     <strong> Neutral  </strong></label>
                                 </div>
                                 <div class="form-radion sad_custm">
                                     <input type="radio" name="satisfied_with_design" id="soundsignal3" class="preference" value="1" required="">
                                     <label for="soundsignal3">
                                     <div class="emoji-check"><i class="fas fa-check-circle"></i></div>
                                     <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/sad.svg" class="img-responsive emoji-img">
                                     <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/sad-red.svg" class="img-responsive emoji-img-check">     <strong> Dissatisfied </strong></label>
                                 </div>
                             </div>
                         </label>
                        <label class="form-group">
                            <p class="label-txt">Additional Note</p>
                            <textarea name="additional_notes" class="review_addtional_note input"></textarea>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                        <button type="submit" name="review_frm" class="btn-red button review_frm">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>-->

<!-- jQuery (necessary for JavaScript plugins) -->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/jquery.fancybox.min.js"></script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.mousewheel.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/account/s_portal.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/all.js"></script>
<script type="text/javascript">
$('[data-fancybox="images"]').fancybox({
    buttons: [
        'download',
        'thumbs',
        'close'
    ]
});
var $fileInput = $('.file-input');
var $droparea = $('.file-drop-area');

$(document).ready(function () {
    $(".curnt_req_status").click(function () {
        $(".mark_cmplt_rq").toggleClass('show_cmpt_btn');
    });
    $(".read_more_btn").click(function () {
        $(".read_more").toggle();
    });
    $('#message_container .msgk-chatrow.clearfix').each(function (e) {
        if ($('figure', this).length > 0) {
            $(this).addClass('margin_top');
        }
    });

});
$('.sendtext').keypress(function (e) {
    if (e.which == 13) {
        $('.send_request_chat').click();
        return false;
    }
});
function validateAndUploadS(input) {
    var URL = window.URL || window.webkitURL;
    var file = input.files[0];
    if (file.type.indexOf('image/') !== 0) {
        this.value = null;
        console.log("invalid");
    }
    else {
        if (file) {
            console.log(URL.createObjectURL(file));
            $(".goup_1 .file-drop-area span").hide();
            $(".goup_1 .file-drop-area .dropify-preview").css('display', 'block');
            $(".goup_1 .img_dropify").attr('src', URL.createObjectURL(file));
        }
    }

}
function submitOnEnter(inputElement, event) {
    if (event.keyCode == 13) {
        inputElement.form.submit();
    }
}
$('.remove_selected').click(function (e) {
    e.preventDefault();
    $(this).closest('.dropify-preview').hide();
    $(this).closest('.file-drop-area').find('span').show();
});
// highlight drag area
$fileInput.on('dragenter focus click', function () {
    $droparea.addClass('is-active');
});

// back to normal state
$fileInput.on('dragleave blur drop', function () {
    $droparea.removeClass('is-active');
});

// change inner text
$fileInput.on('change', function () {
    var filesCount = $(this)[0].files.length;
    var $textContainer = $(this).prev();

    if (filesCount === 1) {
        // if single file is selected, show file name
        var fileName = $(this).val().split('\\').pop();
        $textContainer.text(fileName);
    } else {
        // otherwise show number of files
        // $textContainer.text(filesCount + ' files selected');
    }
});
if (navigator.userAgent.indexOf("MSIE") > 0) {
    $('.customer_pro').css('width', '90%');
}
</script>

<script type="text/javascript">
    $(".pstcmm").keyup(function () {
        var checkusernamenotexist = '<?php echo $checkusernamenotexist; ?>';
        if (checkusernamenotexist == '') {
            $('#Usernamemodal').modal('show');
            return false;
        }
    });

    $('.tff-con').keyup(function () {
        var checkusernamenotexist = '<?php echo $checkusernamenotexist; ?>';
        if (checkusernamenotexist == '') {
            $('#Usernamemodal').modal('show');
            return false;
            window.location.reload();
        }
    });


    function message(obj) {
        var request_id = obj.getAttribute('data-requestid');
        var sender_type = obj.getAttribute('data-senderrole');
        var sender_id = obj.getAttribute('data-senderid');
        var reciever_id = obj.getAttribute('data-receiverid');
        var reciever_type = obj.getAttribute('data-receiverrole');
        var text_id = 'text_' + request_id;
        var message = $('.' + text_id).val();
        var verified_by_admin = "1";
        var profile_image = obj.getAttribute('data-profile_pic');
        var customer_name = obj.getAttribute('data-sendername');
        var shared_user_name = '<?php echo $checkusernamenotexist; ?>';
        // alert(shared_user_name);
        //console.log('request_id: '+request_id,'sender_type: '+sender_type,'sender_id: '+sender_id,'reciever_id: '+reciever_id,'reciever_type: '+reciever_type,'text_id: '+text_id,'message: '+message,'profile_image: '+profile_image,'customer_name: '+customer_name,'shared_user_name: '+shared_user_name);
        if (message != "") {
            var checkusernamenotexist = '<?php echo $checkusernamenotexist; ?>';
            if (checkusernamenotexist == '') {
                $('#Usernamemodal').modal('show');
                return false;
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>account/request/send_message_request",
                data: {"request_id": request_id,
                    "sender_type": sender_type,
                    "sender_id": sender_id,
                    "reciever_id": reciever_id,
                    "reciever_type": reciever_type,
                    "shared_user_name": shared_user_name,
                    "message": message},
                success: function (data) {
                    var res = data.split("_");
                    var update = $.trim(res[0]);
                    data = $.trim(res[1]);
                    $('.text_' + request_id).val("");
                    if ($('#message_container > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                        $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');
                        $('#message_container').prepend('<div class="msgk-chatrow"><div class="msgk-user-chat msgk-right ' + sender_type + ' del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div><div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><span class="edit_icon_' + data + ' edit_icon_msg"></span><pre><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
                    } else {
                        $('#message_container').prepend('<div class="msgk-chatrow"><div class="msgk-user-chat msgk-right ' + sender_type + ' del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div><div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
                    }
                    if (update == 'yes') {
//                    $.noConflict();
                    $(".curnt_req_status").addClass("agency_revision");
                    $(".curnt_req_status a").html("Revision");
                    $("#review_reqid").val(request_id);
                    //$("#f_reviewpopup").modal("show");
                }
                    
//                    if (data == '1') {
//                        location.reload();
//                    }
                }
            });
        }
//$('#message_container').stop().animate({
//        scrollTop: $('#message_container')[0].scrollHeight
//    },0);
    }

//window.onload = function () {
//    $('#message_container').stop().animate({
//        scrollTop: $('#message_container')[0].scrollHeight
//    }, 0);
//}

</script>


<?php
if (!empty($chat_request)) {
    $prjtid = $chat_request[0]['request_id'];
    $customerid = $chat_request[0]['reciever_id'];
    $designerid = $chat_request[0]['sender_id'];
}
?>
</body>
</html>
