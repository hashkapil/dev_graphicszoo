<?php
$CI = & get_instance();
$CI->load->library('myfunctions');
$this->load->view('front_end/variable_css');
?>
<title>Sign In | <?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo $custom_favicon; ?>">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet canonical">
<link rel="stylesheet canonical" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/custom.css">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/bootstrap.min.js" ></script>
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet canonical">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/style.css">
<style>
    body {
        font-family: 'Poppins', sans-serif !important;
    }
.subdomain_signup {
    display: flex;
}
.subdomain_signup > div {
   padding: 0 15px;
}
.subdomain_signup .parsonial-plan {
    width: 30%;
}
.subdomain_signup .pb-info {
    width: 70%;
}
.sing-up-heading {
    padding-bottom: 40px;
    padding-top: 30px;
    background: none!important; 
}
.subdomain {
    max-width: 100%;
    margin: auto;
    width: 100%;
    background: #fff;
    box-shadow: 0 0 30px #ccc;
    padding: 40px;
    border-radius: 12px;
}
div#logo {
        text-align: center;
        margin-bottom: 24px;
        max-height: 100px;
}
div#logo img {
    max-width: 200px;
    max-height: 100px;
}
.subdomain_signup .parsonial-plan {
    padding-top: 10px;
}
.parsonial-plan .par-out .monthly-activate {
    margin: 12px 0;
}
.par-out .select-bx-p {
    margin-bottom: 20px;
}
.parsonial-plan .par-out select {
    background-position: 96%;
    padding: 0px 12px;
}
.total-pPrice p:empty, .monthly-activate:empty {
    margin: 0 !important;
}
.par-out .monthly-activate ul {
    margin-top: 0;
}
.monthly-activate:empty + .total-pPrice {
    margin-top: 35px;
}
.monthly-activate ul li {
    padding-left: 0;
}
.subdomain_signup .parsonial-plan {
    padding-top: 10px;
}
.par-out .monthly-activate {
    font-size: 14px;
    color: #727c83;
    text-align: center;
    list-style-type: none;
    line-height: 32px;
}
.monthly-activate ul li {
    padding: 0;
    line-height: 32px;
 }
 h1 {
    font-size: 40px;
    font-weight: 600;
    margin-bottom: 15px;
    margin-bottom: 50px;
}
.text-center {
    text-align: center!important;
}
.bypasspayment .monthly-activate {
    max-height: 150px;
    overflow-y: auto;
}
select:focus {
    outline: none;
}
.bypasspayment_form .form-signin-group button {
    margin-top: 15px;
}
.bypasspayment_form .pb-info .inner-addon{
    text-align: center;
}
.section-title h1 {
    margin-bottom: 8px;
}
.subdomain .section-title {
    margin-bottom: 45px;
}
div#logo {
    float: right;
}
.form-signin-group .parsonial-plan {
    padding: 0 15px;
}
form#bypasspayment_frm {
    margin-bottom: 0;
}
section#bypass .alert {
    width: 100%;
}
</style>
<section id="bypass" class="sing-up-heading">
    <div class="container">
        <div class="subdomain">
            <div id="logo" class="pull-center">
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href="<?php echo base_url(); ?>" rel="nofollow"><img src="<?php echo $custom_logo; ?>" alt="<?php echo $page_title; ?>" title="<?php echo $page_title; ?>" /></a>
            </div>
            <div class="section-title text-left">
                <h1>Payment</h1>
                <p>You have make payment first, then you can use utilize these services.</p>
            </div>
            <div class="row">
               

                    <?php if ($this->session->flashdata('message_error') != '') { ?>       
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong><?php echo $this->session->flashdata('message_error'); ?></strong>              
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('message_success') != '') { ?>             
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                        </div>
                    <?php } ?>
                    <div class="alert alert-danger alert-dismissable error_payment_procs" style="display:none"></div>
                    <div class="subdomain_signup bypasspayment">
                        <form method="post" class="bypasspayment_form" id="bypasspayment_frm">
                            <input type="hidden" name="plan_name" id="plan_name" value="<?php echo $user[0]['plan_name']; ?>">
                            <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $user[0]['customer_id']; ?>">
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user[0]['id']; ?>">
                            <div class=" form-signin-group col-md-12">
                                <div class="row">
                                    <div class="form-signin-group col-md-4 parsonial-plan">
                                        <div class="par-out">
                                            <label>Your Selected Plan</label>
                                            <div class="form-group">

                                                <select  id="priceselector" >
                                                    <?php
                                                    foreach ($subscription_list as $subscription_k => $subscription_v) {
                                                        $select = ($subscription_v['plan_id'] == $user[0]['plan_name']) ? "selected" : '';
                                                        echo '<option value="' . $subscription_v['plan_id'] . '" data-quantity="' . $subscription_v['global_inprogress_request'] . '" data-amount="' . $subscription_v['plan_price'] . '" ' . $select . '>' . $subscription_v['plan_name'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="monthly-activate"></div>


                                            <div class="total-pPrice">
                                                <p class="plan-price">
                                                    Plan Price:
                                                    <span id="change_prc" class="box item1">$
                                                        <span class="plnamount"><?php echo $user[0]['plan_amount']; ?></span>
                                                    </span>
                                                </p>
                                                <hr>
                                                <div class="g-total">
                                                    <h3>Grand Total <span id="grand_total">$<span class="plnamount"><?php echo $user[0]['plan_amount']; ?></span></span></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 pb-info">
                                        <div class="row">
                                            <div class="form-signin-group col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" id="first_name" name="first_name" value="<?php echo $user[0]['first_name'];?>" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-signin-group col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" id="last_name" name="last_name" value="<?php echo $user[0]['last_name'];?>" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-signin-group col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" id="email" name="email" value="<?php echo $user[0]['email'];?>" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-signin-group col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" id="phone" name="phone" value="<?php echo $user[0]['phone'];?>" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" pattern="\d*" class="form-control" name="card_number" placeholder="Card Number *" required="" maxlength="16">
                                                </div>
                                            </div>
                                            <div class="form-signin-group col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" id="expir_date" name="expir_date" class="form-control" placeholder="MM  /  YY *" onkeyup="dateFormat(this.value);" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-signin-group col-sm-6 cvv">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" pattern="\d*" name="cvc" class="form-control" placeholder="CVV *" maxlength="4" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                         
                                        <div class="form-signin-group col-md-12 inner-addon">
                                            <div style="display: none; justify-content: center;width: 100%;" class="dsply_img_blk">
                                         <img class="loaderimg" src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/ajax-loader.gif">
                                        </div>
                                            <button type="submit" class="red-theme-btn">Pay Now</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                           
                        </form>
                    </div>
                
            </div>
        </div>
    </div>
</section>
<script>
    var BASE_URL = "<?php echo base_url(); ?>";
    var assetspath = "<?php echo FS_PATH_PUBLIC_ASSETS ?>";
    if($('.monthly-activate').length > 0){ 
        var setSelected = jQuery("#priceselector").val();
        var setAmount = jQuery("#priceselector").find(':selected').data('amount');
        Setselectedvalue(setSelected,setAmount); 
    }
    function Setselectedvalue(selectedval,setAmount) {
            var user_id = "<?php echo $user[0]['parent_id']; ?>";
            $plan_price = setAmount;
            jQuery('#plan_name').val(selectedval);
            jQuery('.plnamount').html(setAmount);
            $.ajax({
                type: 'POST',
                url: BASE_URL+'welcome/getsubsbaseduser_byplanid',
                data: {planid: selectedval,user_id:user_id},
                dataType: 'json',
                success: function (data) {
                    if(data[0]){
                        var features = data[0].features;
                        var feature_array = features.split("_");
                        features_html = "<ul>";
                        $.each(feature_array,function(k,val){
                            features_html += "<li>"+val+"</li>";
                        });
                        features_html += "</ul>";
                        jQuery('.monthly-activate').html(features_html);

                    }
                }
            });
    }
    jQuery('#priceselector').on('change', function (e) {
        var getSelectedPrice = $(this).val();
        var getAmount = $(this).find('option:selected').attr("data-amount");
      Setselectedvalue(getSelectedPrice,getAmount);
    });
    function dateFormat(val) {
        if (val.length == 2) {
            $('#expir_date').val($('#expir_date').val() + '/');
        }
    }
    $("#bypasspayment_frm").submit(function(e){
        e.preventDefault();
        var formdata = new FormData($(this)[0]);
        $(".error_payment_procs").css("display","none");
        $(".dsply_img_blk").css("display","block");
        $.ajax({
            type: 'POST',
            url: BASE_URL+"customer/AgencyClients/payment_process",
            data: formdata,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) { 
                console.log("data",data);
                if(data.status == 1){
                    $(".error_payment_procs").css("display","none");
                    window.location.href = BASE_URL+"customer/request/design_request";
                }else{
                    console.log("data.msg",data.msg);
                    $(".error_payment_procs").html(data.msg);
                    $(".error_payment_procs").css("display","block");
                    $('html, body').animate({
                    scrollTop: $("#bypasspayment_frm").offset().top
                    }, 2000);
                }
                $(".dsply_img_blk").css("display","none");
            }
        });
    });
</script>
    
