<section class="affiliate-section affiliate-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="left-earn-sec">
					<h1>Earn Flawlessly with the <br> GraphicZoo Affiliate <br> Program!</h1>
					<p>Share and refer to earn huge! Join the Graphiczoo affiliates today!</p>
					<a class="join_today" href="/affiliate_signup">Join Today !</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="right-earn-image">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/earn.jpg" class="">
				</div>
			</div>
		</div>
	</div>
</section>
<section class="affiliate-section affiliate-progaram">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="left-earn-sec">
					<h2 class="affiliate-sub-heading">What is an Affiliate Program? </h2>
					<p>Graphiczoo’s Affiliate Program lets you make commissions by promoting sales for us. Bloggers, webmasters, professionals, marketers, trainers, digital marketers, anyone can be a part of the program. You are provided with the tools you need to promote and earn. Trust me it has never been easier!</p>
				</div>
			</div>
			<div class="col-md-7">
				<div class="right-earn-image">
					<img alt="affiliat-program" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/affiliat-program.jpg" class="">
				</div>
			</div>
		</div>
	</div>
</section>
<section class="affiliate-section why-affiliate">
	<div class="container">
		<h2 class="affiliate-sub-heading">Why should you be a Graphiczoo Affiliate? </h2>
		<div class="row">
			<div class="col-md-4">
				<div class="affil-item">
					<img alt=" " src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/file.png" class="">
					<h3>No Enrollment Fees</h3>
					<p>Yes, you herd me right! it’s free and all your money is your to keep!</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
					<img alt=" " src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/earning.png" class="">
					<h3>Unlimited Earnings</h3>
					<p>The more referrals you have, the more you earn, No Limites!</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
					<img alt=" " src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/updated.png" class="">
					<h3>Early Updates</h3>
					<p>Be the first one to know about new features, offers or launches.</p>
				</div>
			</div>

		</div>
		<div class="row">		
				<div class="col-md-4">
				<div class="affil-item">
					<img alt=" " src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/megaphone.png" class="">
					<h3>Personalised Tool Kit</h3>
					<p>Quick and easy access to a marketing and promotional tool kit,personalised according to your choice and needs.</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
					<img alt=" " src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/money.png" class="">
					<h3>Credits in Cash</h3>
					<p>Get Credit for each and every reffrral as an income</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
					<img alt=" " src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/transaction.png" class="">
					<h3>Manage Earnings</h3>
					<p>Keep track of all your earnings and reffrral through our custom built dashboard.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="affiliate-section affiliate-part">
	<div class="container">
		<h2 class="affiliate-sub-heading">How can you be a part of the <br> GraphicZoo Affiliate  Program? 
 </h2>
		<div class="row">
			<div class="col-md-6">
				<div class="left-part">
					<img alt=" " src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/program-reg.jpg" class="">
				</div>
			</div>
			<div class="col-md-6">
				<div class="right-register">
					<p>Once you register, you will get a unique referral link.</p>
					<p>Promote us on your website or in your blogs, newsletters, etc using the referral link. </p>
					<p>You can also choose from the promotions already provided in the marketing tool kit.</p>
					<p>For each guest click, signup and purchase, you earn commissions.</p>
					<a class="join_today" href="/affiliate_signup">Join Today !</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="affiliate-section affiliate-talk">
	<div class="container">
		<h2 class="affiliate-sub-heading">Our Affiliates Talk!</h2>
		<div class="row">
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-a.png" class=""></div>
						<div class="talker-info">
							<h4>Erica</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>Graphiczoo hands-down provides the best affiliate program in the industry with a mixture of unlimited offers for the affiliates. I have referred it to a lot of friends and acquaintances and they have been more than just pleased with the graphic designs and services. Earning has never been more fun for me.</p>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-b.png" class=""></div>
						<div class="talker-info">
							<h4>Adam Chris</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>I have been an affiliate for years now and have collaborated with all types of brands. The experience with Graphiczoo was rather different and better in a lot of aspects. I love their team. They’re so friendly, dedicated and hard-working. This is an awesome option for some passive income. Thank you Graphiczoo!</p>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/danny.jpg" class=""></div>
						<div class="talker-info">
							<h4>Danny</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>Responsive staff, cool designs, simple payout options, and great other services. Graphiczoo has it all! My references have been extremely satisfied and hence I would definitely urge all of you to join the impeccable affiliate program of the Graphiczoo!</p>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="affiliate-section affiliate-faq">
		<div class="container">
			
			<div class="faq-list">
				<h2 class="affiliate-sub-heading">FAQ</h2>
			<p>Still have questions? Check out our FAQs or reach out to our <br>
team directly and we'll be sure to help you out.</p>
	<div class="faq-item">
	  <a class="" data-toggle="collapse" href="#collapse-a" role="button" aria-expanded="false" aria-controls="collapseExample">
	    How to become an Affiliate?
	  </a>
		<div class="collapse" id="collapse-a">
		  <div class="card card-body">
		   Click on Join Today to register to become an affiliate for Graphics Zoo. Upon registering you’ll get a unique referral link, which you can use to promote Graphics Zoo on your website, blogs, newsletters and etc.
		  </div>
		</div>
	</div>
	<div class="faq-item">
	  <a class="" data-toggle="collapse" href="#collapse-b" role="button" aria-expanded="false" aria-controls="collapseExample">
	   How do I get my commission?
	  </a>
		<div class="collapse" id="collapse-b">
		  <div class="card card-body">
		   You will be paid using Paypal.
		  </div>
		</div>
	</div>
	<div class="faq-item">
	  <a class="" data-toggle="collapse" href="#collapse-c" role="button" aria-expanded="false" aria-controls="collapseExample">
	   How can I know about my earnings?
	  </a>
		<div class="collapse" id="collapse-c">
		  <div class="card card-body">
		   You can see your earnings details within you affiliate account. It will show you exactly how much you have earned.
		  </div>
		</div>
	</div>
	<div class="faq-item">
	  <a class="" data-toggle="collapse" href="#collapse-d" role="button" aria-expanded="false" aria-controls="collapseExample">
	   What is the process to register? 
	  </a>
		<div class="collapse" id="collapse-d">
		  <div class="card card-body">
		    Once you register using the link above to go to the registration page, it will immediately give you access to your affiliate dashboard.
		  </div>
		</div>
	</div>

			</div>
		</div>
</section>

<section class="publection-sec get-touch-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
         <h2>START EARNING MORE WITH GRAPHICZOO <br> Get In touch today!</h2>
         <p>Still, have your doubts?</p>
         <p>You can contact us with your questions in regards to the affiliate program or anything else at: <a href="mailto:support@graphicszoo.com">support@graphicszoo.com</a></p>
        <div class="get-tuch-btn">
        	<a class="red-theme-btn" href="#">Get in Touch Today
        		<img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="">
        	</a>
        </div>
    </div>
</div>
</div>
</section>