
<div class="seo_front_wrapper">
<section class="seo_detail_banner">
    <div class="container">
        <div class="seo-caption">
            <h1><strong>Website & App</strong>
            </h1>
            <p>
            Get a custom eCommerce design for your business at DesignCrowd, the #1 custom eCommerce design service with 786,540 eCommerce theme designers. Our community of professional designers are ready to create the perfect eCommerce theme for you. Businesses across the globe have used DesignCrowd to get the right eCommerce theme for their business. You can join them, simply start a eCommerce design project today and get a eCommerce theme you’ll love.
            </p>
            
            <ul>
                <li>
                <i class="far fa-check-circle"></i>
                    <span>786,540 freelance designers</span>
                </li>
                <li>
                <i class="far fa-check-circle"></i>
                    <span>25 - 100++ designs per project</span>
                </li>
                <li>
                <i class="far fa-check-circle"></i>
                    <span>356,089 completed projects</span>
                </li>
            </ul>
            <a class="seo_btns btn" href="#">Get Started Now</a>
        </div>
    </div>
</section>
<section class="seo_trusted">
    <div class="container">
    <h3 class="seo_sub_heading">Trusted by companies</h3>
    <ul class="all-logo">
                <li><img alt="goat grass" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/goat-grass.png">
                </li>
                <li><img alt="Bread_Logo_Retina" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Bread_Logo_Retina.png">
                </li>
                <li><img alt="enicode" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/enicode.png">
                <li><img alt="western-rockies" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/western-rockies.png" >
                </li>
                <li><img alt="green-gurd" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd.png" >
                </li>
                <li><img alt="perfect-imprents" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/perfect-imprents.png" >
                </li>
                <li><img alt="ourbookkiper" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ourbookkiper.png" >
                </li>
              </ul>
    </div>
</section>



<section class="catg_grid catg_grid_bg">
    <div class="container">
    <h3 class="seo_sub_heading"><?php echo $cat_name;?>
<p>Get your very own logo design through our custom logo design tool. Design your own logo or ask our expert designers to create graphic design logo for your business.
</p>
    </h3>
 <div class="container">
     <div class="row">
     	  <?php 
     	if(!empty($cat_samples)){ 
     		foreach ($cat_samples as $k => $catinfo) {

     		?>
        <div class="col-md-2 catg_gird_list">
            
            <img alt="" src="<?php echo FS_PATH_PUBLIC_UPLOADS.'samples/'.$catinfo['sample_id'].'/'.$catinfo['sample_material']?>">                
            
        </div>

    <?php } }else{ ?>
    	<p>
    		No Sample Found for this category
    	</p>
    <?php } ?>
        
        
        
         
     </div>
 </div>
    </div>
</section>


<section class="choose_artrow">
    <div class="container">
    <h3 class="seo_sub_heading">Only the best website design
<p>Here's why 100,000+ businesses have chosen GraphicsZoo
</p>
    </h3>
<div class="row">
        
    <div class="col-md-6">
        <div class="left_art_info">
                <ul>
                    <li>
                    <strong>Save money & time</strong>
Our eCommerce design starts at a low price with options to meet any budget. On average eCommerce theme projects start to receive designs within a few hours.
                    </li>
                    <li>
                    <strong>More creativity</strong>
                    With freelance eCommerce theme designers across the globe competing on your project, you'll receive heaps of eCommerce themes ideas - you just need to choose the best.
                    </li>
                    <li>
                    <strong>A world of eCommerce design</strong>
                    Professional freelance eCommerce theme designers around the world ready to create you the perfect eCommerce theme.
                    </li>
                    <li>
                    <strong>A world of eCommerce design</strong>
                    Professional freelance eCommerce theme designers around the world ready to create you the perfect eCommerce theme.
                    </li>
                    <li>
                    <strong>Save money & time</strong>
                    If you're not satisfied with the eCommerce designs and don't get the perfect eCommerce theme for your business, get your money back*
                    </li>
                </ul>
        </div>
    </div>
    <div class="col-md-6">
        <div class="art_image">
        <img alt="green-gurd" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf2.jpg" >  
        </div>
    </div>
</div>
    </div>
</section>


<section class="how_it_work">
    <div class="container">
    <h3 class="seo_sub_heading">How it works
        <p>Checkout our simple and fast work flow</p>
    </h3>
 <div class="container">
     <div class="row">
        <div class="col-md-4">
        <div class="how_work_step">
        <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/submit-project.png">
            <h3>Submit Project </h3>
            <p>Create a new project by simply adding your design details such as name, description, file formats you would like to receive, and any examples you may have.</p>
        </div>
        </div>
        <div class="col-md-4">
        <div class="how_work_step">
        <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/approve-download.png">
            <h3>Feedback and Revisions </h3>
            <p>Once you receive your initial drafts let us know if there are any revisions you need by clicking anywhere on the design to put a feedback comment. Do this as many times as you need.</p>
        </div>
        </div>
        <div class="col-md-4">
        <div class="how_work_step">
        <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/submit-rq.png">
            <h3>Approve and Download </h3>
            <p>When you get the design as you had imagined, just click approve and download the source files requested.</p>
        </div>
        </div>
     </div>
 </div>
 <div class="text-center">
 <a class="seo_btns btn" href="#">Get Started Now</a>
 </div>
    </div>
</section>


<section class="seo_review_sec">
    <div class="container">
    <h3 class="seo_sub_heading">Client Reviews
        <p>Get your very own logo design through our custom logo design tool. Design your own logo or ask our expert designers to create graphic design logo for your business.
</p>
    </h3>
    <div class="seo__review">
        <div class="seo_review_item">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proidentDuis aute irure dolor in reprehenderit in voluptate.</p>
                <div class="seo_client">
                    <figure>
                        <img alt="client_img_a" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/seo_front/client_img_a.jpg">
                    </figure>
                    <em>Client Name</em>
                </div>
        </div>
        <div class="seo_review_item">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proidentDuis aute irure dolor in reprehenderit in voluptate.</p>
                <div class="seo_client">
                    <figure>
                        <img alt="client_img_a" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/seo_front/client_img_a.jpg">
                    </figure>
                    <em>Client Name</em>
                </div>
        </div>
        <div class="seo_review_item">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proidentDuis aute irure dolor in reprehenderit in voluptate.</p>
                <div class="seo_client">
                    <figure>
                        <img alt="client_img_a" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/seo_front/client_img_a.jpg">
                    </figure>
                    <em>Client Name</em>
                </div>
        </div>
    </div>
    </div>
</section>
<section class="join_seo_customer">
    <div class="container">
        <div class="inner_seo_caption">
            <h2>Join more than 500+ customers</h2>
            <span>The one thing which, as an organization, has been picking on our operations is managing our clients’ project database. </span>
            <a class="seo_btns btn" href="#">Get Started Now</a>            
        </div>
    </div>
</section>
</div>
<script>
jQuery( document ).ready(function() {
    jQuery('.seo__review').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
    });
});
</script>