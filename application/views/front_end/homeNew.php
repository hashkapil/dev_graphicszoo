<section id="hero" class =" ">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-7">
          <h1>Unlimited Graphic Designs <br/>Flat Monthly Rate</h1>
          <p>The best solution for all your creative design needs</p>
          <p>Dedicated Design Team, Unlimited Brands, No Contracts</p>
          <a rel="canonical" href="<?php echo base_url(); ?>pricing" class="btn-get-started scrollto">Get Started Now</a>
          <div class="btns">
            <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/VjHrU_jBmDY" rel="gallery nofollow" style="color:#1f3853;border:1px solid #1f3853;background:white;"><span><i class="fas fa-play-circle"></i></span>Watch intro</a>
          </div>
        </div>
      </div>
    </div>
  </section>
<div class="satelment">
      <div class="container">
        <div class="logos-slider">
          <div class="logs-slider owl-carousel owl-theme" id="mainNav"  >
            <div class="item lazyOwl"  data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Logo-&-Branding.png">
              <div class="logos-name" >
                <h3>Logo & Branding</h3>  
              </div>
            </div>
            <div class="item lazyOwl"  data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Tshirt-Design.png">

              <div class="logos-name" >
                <h3>T-Shirt Designs</h3>  
              </div>
            </div>
            <div class="item lazyOwl"  data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Social-Media.png">
              <div class="logos-name" >
                <h3>Social Media Posts</h3>  
              </div>
            </div>
            <div class="item lazyOwl"  data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Website.png">
              <div class="logos-name" >
                <h3>Web & Mobile UI/UX</h3>  
              </div>
            </div>
            <div class="item lazyOwl"  data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Poster-&-Flyers.png">
              <div class="logos-name" >
                <h3>Flyers & Brochures</h3>  
              </div>
            </div>
            <div class="item lazyOwl"  data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Product.png">
              <div class="logos-name" >
                <h3>Product Packaging</h3>  
              </div>
            </div>
            <div class="item lazyOwl"  data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Print-Ready-files.png">
              <div class="logos-name" >
                <h3>Print Ready Files</h3>  
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  

<section id="get-started" class="text-center">
  <div class="container">
    <div class="trusted-by text-center">
      <h4>Trusted by some of the most exciting companies around the globe!</h4>
      <p>Rated 4.9/5 for quality, turnaround, and communication</p>
      <div class="trusted">
        <div class="inner-trusted">
          <ul class="all-logo">
            <li><img alt="Goat Grass"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/goat-grass.png" alt="goat-grass" class="lazyOwl">
            </li>
            <li><img alt="Bread Logo Retina"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Bread_Logo_Retina.png"  alt="Bread_Logo_Retina" class="lazyOwl"></li>
            <li><img alt="Enicode"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/enicode.png" alt="enicode" class="lazyOwl">
            <li><img alt="Western Rockies"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/western-rockies.png"  alt="western-rockies" class="lazyOwl" ></li>
            <li><img alt="Green Gurd"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd.png" alt="green-gurd" class="lazyOwl"></li>
            <li><img alt="Perfect imprents"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/perfect-imprents.png" alt="perfect-imprents" class="lazyOwl vllsrc" ></li>
            </ul>
          </div>
        </div> 
      </div>
    </div>
  </section>

<section class="old-new">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 style="text-transform: none;">How we have changed Graphic Designing</h2>
        <br/>
      </div>
    </div>
    <div class="row">
     <div class="col-md-12">
       <img alt="Old Ways" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/OldWay.svg" class="center-block oldway lazyOwl">
       <img alt="New Ways"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/NewWay.svg" class="center-block newway lazyOwl">
   </div>
   </div>
 </div>
</section>


<div class="exclusive-feature text-center">
      <div class="container">
        <h2>Features Built with You in Mind</h2>
        <p>Project management taken to the next level.<br/> See what makes us unique and why you will love working with us for your design needs. </p>
      </div>
      <div class="owl-carousel owl-theme features_slider">
        <div class="item">
          <div class="feature-here">
            <img alt="No-contract"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/no-contract.png" class="center-block lazyOwl">
            <h3>No Contracts</h3>  
            <p>Pay month to month, upgrade/downgrade as you please and cancel anytime. 
              Flat Monthly Rate
            </p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Monthly Rate"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/monthly-rate.png" class="center-block lazyOwl">
            <h3>Flat Monthly Rate</h3>  
            <p>No hidden costs of any type. One flat rate, unlimited designs. </p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Graphic Icon"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/graphic-icon.png" class="center-block lazyOwl">
            <h3>Dedicated Designer</h3>  
            <p>Work one-on-one with a dedicated designer who will take out the time to understand your business needs.</p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Communication"   src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/simple-comunication.svg" class="center-block lazyOwl">
            <h3>Simple Communication</h3>  
            <p>Chat directly with your design team via the design portal.</p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Source Files"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Source-files.svg" class="center-block lazyOwl">
            <h3>Source Files </h3>  
            <p>All designs include the source files you request.</p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Unlimited Projects"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Unlimited-projects.svg" class="center-block lazyOwl">
            <h3>Unlimited projects and revisions </h3>  
            <p>Submit as many projects as you have and request as many revisions as you need. We will work hard to make sure we get it done for you.</p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Brand Profile"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/brand-profile.svg" class="center-block lazyOwl">
            <h3>Brand Profiles</h3>  
            <p>Create brand profiles for each of your brands by uploading asset files such as logos, brand guidelines, and reference designs. </p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Link Share Icon"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/link-share-icon.svg" class="center-block lazyOwl">
            <h3>Link Sharing</h3>  
            <p>Share your designs with other team members and customers by sending a link of the design to them. </p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="White Label"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/white-label.svg" class="center-block lazyOwl">
            <h3>White Label</h3>  
            <p>Take our platform to the next level, by making it all yours.</p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="User Management"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/user-managment.svg" class="center-block lazyOwl">
            <h3>User Management</h3>  
            <p>Add team members to your account as sub users and limit their permissions as you desire. </p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Email Share"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/email-share-icon.svg" class="center-block lazyOwl">
            <h3>Email Requests</h3>  
            <p>Submit your requests via email and they will automatically get added to your dashboard.</p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Interactive Revisions"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Interactive-revisions.svg" class="center-block lazyOwl">
            <h3>Interactive Revisions</h3>  
            <p>With pinpoint accuracy, mark up your design drafts by using our click to comment feature to let you designer know exactly where you want changes. </p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Project Flow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/project-flow.svg" class="center-block lazyOwl">
            <h3>Project Flow Technology</h3>  
            <p>Our system is built with efficiency in mind. Your projects will automatically flow from one stage to the next so your designs are done in a timely manner. </p>
          </div>
        </div>
        <div class="item">
          <div class="feature-here">
            <img alt="Project Priorty"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/project-priorty.svg" class="center-block lazyOwl">
            <h3>Project Priority </h3>  
            <p>Manage your requests by having the ability to re-prioritize design requests based on your business needs.</p>
          </div>
        </div>
      </div>
      <a rel="canonical" href="<?php echo base_url(); ?>features" class="red-theme-btn">View More
        <img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid lazyOwl"></a>

      </div>

<section class="howWeWork">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h2>How We Work</h2>
              <p>Checkout our simple and fast work flow</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <div class="nav nav-tabs nav-fill"  id="nav-tab" role="tablist">
                <a href="#submit-rq" class="active" data-toggle="tab" role="tab" aria-selected="true">
                  <h3>1. Submit Project <i class="fas fa-caret-right"></i></h3>
                  <p>Create a new project by simply adding your design details such as name, description, file formats you would like to receive, and any examples you may have.</p>
                </a>
                <a href="#feedback-rev" data-toggle="tab" role="tab" aria-selected="false">
                  <h3>2. Feedback and Revisions <i class="fas fa-caret-right"></i></h3>
                  <p>Once you receive your initial drafts let us know if there are any revisions you need by clicking anywhere on the design to put a feedback comment. Do this as many times as you need. </p>
                </a>
                <a href="#approve-rq" data-toggle="tab" role="tab" aria-selected="false">
                  <h3>3. Approve and Download <i class="fas fa-caret-right"></i></h3>
                  <p>When you get the design as you had imagined, just click approve and download the source files requested.</p>
                </a>
              </div>
            </div>
            <div class="col-md-7">
             <div class="tab-content" id="nav-tabConten">
               <div class="tab-pane fade show active" role="tabpanel"  id="submit-rq">
                <img alt="Submit Project" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="  data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/submit-project.png" class="img-fluid lazyOwl">
              </div>
              <div role="tabpanel" class="tab-pane fade" id="feedback-rev">

               <img alt="Approve Download" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="  data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/approve-download.png" class="img-fluid lazyOwl">
             </div>
             <div role="tabpanel" class="tab-pane fade" id="approve-rq">
               <img alt="Submit Request"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/submit-rq.png" class="img-fluid lazyOwl">
             </div>
           </div>
         </div>
         <div class="col-md-12 text-center">
           <a rel="canonical" href="<?php echo base_url(); ?>pricing" class="red-theme-btn">Get Started Now
            <img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid lazyOwl"></a>
          </div>
        </div>
      </div>
    </section>

<section class="top-feature">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2>Our Works </h2>
            <p>We can create all types of graphic designs.<br/> From logos, brochures, flyers, web pages, t-shirts, illustrations to much more. Take a look at some of our work!</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="port-text">
              <img alt="Chesapeake Bay"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf2.jpg" class="img-fluid lazyOwl">
              <div class="hover-txt"> <a rel="canonical" href="<?php echo base_url(); ?>portfolio#Logo">View More Logos</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="Creative Media"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf3.jpg" class="img-fluid lazyOwl">
              <div class="hover-txt"> <a rel="canonical" href="<?php echo base_url(); ?>portfolio#Other">View More Branding</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="Happy New year"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf4.jpg" class="img-fluid lazyOwl">
              <div class="hover-txt"> <a rel="canonical" href="<?php echo base_url(); ?>portfolio#Print">View More Print</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="Opinion Stage"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf5.jpg" class="img-fluid lazyOwl">
              <div class="hover-txt"> <a rel="canonical" href="<?php echo base_url(); ?>portfolio#Websites">View More Web & Monile Application</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="T-Shirt"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt1.jpg" class="img-fluid lazyOwl">
              <div class="hover-txt"> <a rel="canonical" href="<?php echo base_url(); ?>portfolio#T-shirt">View More T-Shirts</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="Area Probe"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf7.jpg" class="img-fluid lazyOwl">
              <div class="hover-txt"> <a rel="canonical" href="<?php echo base_url(); ?>portfolio#Other">View More Book Covers</a></div>
            </div>
          </div>
          <div class="col-md-12 text-center m-t-50">
           <a rel="canonical nofollow" href="<?php echo base_url(); ?>portfolio" class="red-theme-btn">View More
            <img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid lazyOwl"></a>
          </div>
        </div>
      </div>
    </section> 

    <!------ testomonial-sec-------->
    <section class="testomonial-sec">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
           <h2>What others have to say about us</h2>
           <p>Our customers love us and they have shared a few reviews to say just how much.
            <br/>Take a look at some of the awesome feedback we have received.
          </p>
        </div>
      </div>
    </div>
    <div class="testomonial-out  " id="testomonial-out">
      <div class="testomonial owl-carousel owl-theme">
        <div class="item">
          <div class="comment-here">
            <img alt="Patrick Black" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post4.png" class="img-fluid no_bg lazyOwl">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Patrick Black </h4> 
              <p>President/CEO, Perfect Imprints</p>
            </div>
            <p class="comment5">
              <?php
              $title = "Graphics Zoo has been amazing to work with for my graphic design needs. They are fast and the designs are always on point. They do a great job following directions I provide, and the design queue makes it easy for me to prioritize all my design jobs needed. Their service has become an invaluable resource for my company!";
              echo substr($title,0,150);
              $length = strlen($title);
                if($length >= 100){
                    echo "...";
                }
              ?>

            </p>
            <a rel="canonical nofollow" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn testimonial">Read more<img class="lazyOwl" alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>

          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="Shea Bramer"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post2.png" class="img-fluid dark lazyOwl">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Shea Bramer</h4> 
              <p>Marketing Coordinator, Western Rockies
              </p>
            </div>
            <p class="comment5">
              <?php
              $title = "We are a small marketing team for a large financial company and new growth in the company meant that the graphic design workload was becoming overwhelming for our team. Graphicszoo created an easy way for us to keep up with the workload as our company continues to grow. They pay close attention to our branding, and are very detail oriented (as I can be quite demanding). The work they provide for us is always very high quality and I always get what I'm asking for. Their prices are also very competitive. We highly suggest this company!";
              echo substr($title,0,150);
              $length = strlen($title);
                if($length >= 100){
                    echo "...";
                }
              ?>

            </p>
            <a rel="canonical nofollow" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn testimonial">Read more<img alt="Long Arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>

          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="Skylar Domine"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post3.png" class="img-fluid light lazyOwl">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Skylar Domine </h4> 
              <p>Goat Grass Co</p>
            </div>
            <p class="comment5">
              <?php
              $title = "I've been through dozens of design teams and none of which ever checked off all the boxes- price, turnaround time, consistency, and accuracy.  I'm actually 'wowed' by this company and will have my loyalty for years to come from my circle. Highly recommend to anyone looking for design work. If you can visualize it then they can create it!";
              echo substr($title,0,150);
              $length = strlen($title);
                if($length >= 100){
                    echo "...";
                }
              ?>

            </p>
            <a rel="canonical nofollow" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn testimonial">Read more<img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="lazyOwl"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="Green Guard"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd1.jpg" class="img-fluid no_bg lazyOwl">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Nathan Leathers - Owner</h4> 
              <p>Green Gaurd, Printing and Apparel</p>
            </div>
            <p class="comment5">
              <?php
              $title = "Had another competitor prior to this, and within 3 days Graphics Zoo has far exceeded any level of service I had with the previous competitor in 2 years.  They are very responsive and so far fast and solid production of content!";
              echo substr($title,0,150);
              $length = strlen($title);
                if($length >= 100){
                    echo "...";
                }
              ?>

            </p>
            <a rel="canonical nofollow" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn testimonial">Read more<img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="lazyOwl"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="Focal Shift"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/focal-shift.png" class="img-fluid no_bg lazyOwl">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Thomas Elliott Fite</h4> 
              <p>Focal Shift </p>
            </div>
            <p class="comment5">
              <?php
              $title = "Pretty impressed! Been with them for several months now and we've finally found our work flow with them! Great job GZ and keep up the great work!";
              echo substr($title,0,150);
              $length = strlen($title);
                if($length >= 100){
                    echo "...";
                }
              ?>

            </p>
            <a rel="canonical nofollow" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn testimonial">Read more<img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="lazyOwl"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="Scott"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc ="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/scott.png" class="img-fluid no_bg lazyOwl" alt="scott">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Scott Levesque</h4>
              <p>Be Free Clothing Company</p> 
            </div>
            <p class="comment5">
              <?php
              $title = "Great designs, quick turnaround and awesome to work with. Thank you.";
              echo substr($title,0,150);
              $length = strlen($title);
                if($length >= 100){
                    echo "...";
                }
              ?>

            </p>
            <a rel="canonical nofollow" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn testimonial">Read more<img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="lazyOwl"></a>

          </div>
        </div>

      </div>
      <div class="text-center">
       <a rel="canonical" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn">View More
        <img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid lazyOwl">
      </a>
    </div>
  </div>
</section>

<!------ testomonial-sec end-------->


<!------ Risk free-sec -------->
<section class="riskFree-sec pricing-risk">
 <div class="container">
  <div class="row">
   <div class="col-md-12 text-center">
    <h2>Join more than 500+ customers</h2>
    <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
    <a rel="canonical nofollow" href="<?php echo base_url(); ?>pricing" class="red-theme-btn">Get Started Now
     <img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid lazyOwl"></a>

   </div>
 </div>
</div>
</section>

<!------/ Risk free-sec -------->

<section class="resource-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
       <h2>Graphics Zoo Blogs and Resources</h2>
       <p>Stay in the know on the newest graphic design trends by accessing our constantly 
         updated <br/>resources and guides for all business types.</p>
       </div>
     </div>
     <?php //echo "<pre>";print_r($blog_data); ?>
     <div class="row">
      <?php for($i = 0;$i<3; $i++) {
        $created = new DateTime($blog_data[$i]['created']); 
        $today = new DateTime(date('Y-m-d h:i:s')); 
        $diff = $today->diff($created)->format("%a");
        ?>
        <div class="col-md-4 item lazy">
          <div class="resource-here">
            <div class="image_container">
              <img alt="<?php echo substr($blog_data[$i]['title'], 0, 12); ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_UPLOADS_BLOGS.$blog_data[$i]['image']; ?>" class="center-block">

            </div> 
            <?php //$title = str_replace(' ', '-', $blog_data[$i]['title']); ?>
            <a rel="canonical" rel="canonical" href="<?php echo base_url(); ?>article/<?php echo $blog_data[$i]['slug']; ?>"><?php echo $blog_data[$i]['title']; ?></a>
            <a class="go-blog" rel="canonical" href="<?php echo base_url(); ?>article/<?php echo $blog_data[$i]['slug']; ?>" data-id="<?php echo $blog_data[$i]['id']; ?>">Read more </a>
            <!-- <p><?php echo $diff; ?> days ago</p> -->
          </div>
        </div>
      <?php } ?>
      <a rel="canonical nofollow" href="<?php echo base_url()."blog" ?>" class="red-theme-btn">View More   <img alt="Long Arrow"  src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-vllsrc="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid lazyOwl"></a> 
    </div>

  </div>
</div>
</section>
  