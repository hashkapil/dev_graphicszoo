<!DOCTYPE html>
<html>
    <head >
        <?= $this->Html->charset() ?>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow">
        <title>
            <?php
            if (isset($page_title)):
                echo $page_title;
            else:
                echo $this->fetch('title');
            endif;
            ?>            
        </title>
        <?= $this->fetch('meta') ?>
        <?= $this->Html->meta('image/x-icon', '/public/new_fe_images/favicon.ico') ?>

        <?= $this->element('front_end/default_css'); ?>
        <?= $this->fetch('css'); ?>
        <?= $this->element('default_js'); ?>
        <?= $this->element('front_end/analytic'); ?>
    </head>

    <?php
    $bodyClass = '';
    if (isset($body_class)):
        $bodyClass = $body_class;
    endif;
    ?>

    <body class="<?= $bodyClass ?>">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-53V9QF8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <?= $this->element('front_end/mobile_menu'); ?>
        <div class="wrap">          
            <?= $this->element('front_end/header'); ?>
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content'); ?>
            <?= $this->element('front_end/footer'); ?>
            <?= $this->element('front_end/default_js_bottom'); ?>
        </div>
        <?= $this->fetch('script') ?>
        <script>
            jQuery(document).ready(function ($) {
                setTimeout(function () {
                    $(".alert").fadeOut(1000);
                }, 3000);
            });
        </script>
    </body>
</html>


