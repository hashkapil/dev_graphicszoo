﻿<!doctype html>
<html lang="en">
<head>   
    <style>
    body {
        font-family: 'Poppins', sans-serif !important;
    }
    .select-bx-p h3 {
        font-size: 44px;
        font-weight: 700;
        text-align: center;
        color: #444;
    }
    .disc-code {
        margin: -10px 0 10px 0px;
    }
    select#state {
        height: auto;
        padding: 17px;
        margin: 10px 0;
        background: url(https://s3.us-east-2.amazonaws.com/graphics-zoo-ohio/public/assets/front_end/Updated_Design/img/sign-nep.png);
        background-repeat: no-repeat;
        background-position: 95% 50%;
        background-size: 19px;
        -webkit-appearance: none;
        border-radius: 8px;
    }
    .monthly-activate {
        margin: 40px 0;
    }

    .monthly-activate ul {
        width: 100%;
    }
    div#upgrade_to_unlimited h2.popup_h2.del-txt {
        padding-top: 0;
    }
    header.fo-rm-header {
        margin-bottom: 30px;
        text-align: center;
        border-bottom: 1px solid #dcdcdc;
        padding-bottom: 30px;
    }
    section.publection-sec {
        display: none;
    }
    .confirmation_btn .btn {
        border-radius: 4px;
        padding: 6px;    font-weight: normal;
    }
    .similar-prop .modal-dialog .modal-content img {
        max-width: 140px;
    }
    header.fo-rm-header li {
        font-weight: normal;
        font-size: 14px;
        /* line-height: 34px; */
        background: #f1f1f1;
        /* margin-bottom: 2px; */
        max-width: 260px;
        margin: 5px auto;
        border-radius: 30px;
        display: inline-block;
        padding: 6px 12px;
    }
    header.fo-rm-header li:before {
        content: "";
        display: inline-block;
        width: 7px;
        height: 12px;
        border: solid #e73250;
        border-width: 0 2px 2px 0;
        transform: rotate(45deg);
        margin-right: 7px;
    }
    body.single_request_register header.form-header .right-number {
        margin-top: 0;
        max-width: 200px;
    }
    body.single_request_register header.form-header {
        padding: 0;
    }
    @media (max-width: 992px){
        section.page-heading-sec {
            margin-top: 0 !important;    padding-top: 40px !important;
        }
    }
</style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Sign Up | Graphics Zoo</title>
<!-- Bootstrap CSS -->
<link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet nofollow canonical">
<link rel="stylesheet nofollow canonical" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/bootstrap.min.css">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/style.css">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/custom_style_for_all.css">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-57XFPLD');</script>
<!-- End Google Tag Manager -->
</head>
<body class="single_request_register">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<header  class="form-header">
    <div class="container">
        <div class="row">
            <div class="col-6 col-sm-4 col-md-3"><div id="logo" class="sign-logo">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.png" alt="Graphics Zoo" title="Graphics Zoo" /></a>
            </div>
        </div>
        <div class="col-6 col-sm-8 col-md-9">
            <div class="right-number">
                <!-- <div class="number"> <span><i class="fas fa-phone"></i></span><a href="tel:(800) 225-6674">(800) 225-6674</a></div> -->
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/google-rating.png" alt="Graphics Zoo" />
            </div>
        </div></div></div>
    </header>
    <section class="page-heading-sec">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <h1>Create your account</h1>
           <p>Ready to get started? Create an account, it takes less 
           than a minute.</p>
       </div>
   </div>
</div>
</section>
<section class="sing-up-heading " id="single_request_signup">
    <div class="container">
        <div class="row sign-here">
            <div class=col-md-12 text-center">
                <div class="both-outer">
                    <div class="alert alert-danger alert-dismissable" style="display:none">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                    </div>
                    <div class="alert alert-success alert-dismissable" style="display:none">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                    <div class="parsonial-plan">

					<div class="par-out">
						<div class="select-bx-p">
							<h3>$<?php echo FORTYNINE_REQUEST_PRICE; ?></h3>
						</div>
						<div class="monthly-activate">
                            <ul>
                               <li><i class="fas fa-check"></i>1 Design request</li>
                               <li><i class="fas fa-check"></i>Quick Turnaround</li>
                               <li><i class="fas fa-check"></i>Quality Designer</li>
                               <li><i class="fas fa-check"></i>Dedicated Support</li>
                               <li><i class="fas fa-check"></i>Free Stock Images</li>
                               <li><i class="fas fa-check"></i>No Contracts</li>
                           </ul> 
						</div>
						<div class="total-pPrice">
							<!--<p class="plan-price">
								Plan Price:
								<span id="change_prc" class="box item1">$
									<span><?php //echo FORTYNINE_REQUEST_PRICE; ?></span>
								</span>
							</p>
							<p class="plan-price tax_prc_for_texas"></p>
							<hr>
							<div class="g-total">
								<h3>Grand Total <span id="grand_total">$<span><?php //echo FORTYNINE_REQUEST_PRICE; ?></span></span></h3>
							</div>
							<span class="coupon-des-newsignup"></span>-->
						</div>
					</div>
          </div>
          <div class="bill-infoo">
            <div class="pb-info">
                <form action="" method="post" role="form" class="contactForm">
                    <input type="hidden" id="selected_price" name="selected_price" value="<?php echo FORTYNINE_REQUEST_PRICE; ?>"/>
                    <input type="hidden" id="final_plan_price" name="final_plan_price" value="<?php echo FORTYNINE_REQUEST_PRICE; ?>"/>
                    <input type="hidden" id="state_tax" name="state_tax" value="0"/>
                    <input type="hidden" id="customer_id" name="customer_id" value="0"/>
                    <input type="hidden" id="tax_amount" name="tax_amount" value="0"/>
                    <input type="hidden" id="couponinserted_ornot" name="couponinserted_ornot" value=""/>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <input type="text"  name="first_name"  id="fname" class="form-control" data-rule="minlen:4" data-msg="Please enter First Name" required="" placeholder="First Name *">
                                </div>
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <input type="text" name="last_name" class="form-control" id="lname" placeholder="Last Name *"  data-msg="Please enter Last Name" required="">
                                </div>
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <input type="email"   name="email" class="form-control" id="email" placeholder="Email Address *" data-rule="email" data-msg="Please enter a valid email" required="">
                                </div>
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password *" data-rule="minlen:4" data-msg="Please enter the password" required="" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="number"  class="form-control" name="card_number" placeholder="Card Number *" required="" oninput="onlyNumber(this.value)" onKeyDown="if(this.value.length==16 && event.keyCode!=8) return false;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="expir_date" name="expir_date" class="form-control" placeholder="MM  /  YY *"  onkeyup="dateFormat(this.value);" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 cvv">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="number" name="cvc" class="form-control" placeholder="CVV *" oninput="onlyNumber(this.value)" onKeyDown="if(this.value.length==4 && event.keyCode!=8) return false;" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control disc-code" name="Discount" placeholder="Discount Code"  data-msg="Please enter a valid email">
                                </div>
                                <div class="ErrorMsg epassError disc-code">
                                    <!--                                                    <p class="alert alert-danger"></p>-->
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <select name="state" id="state" class="form-control" required>
                                        <option value="" selected="selected">Select a State</option>
                                        <option value="International Customer">International Customer</option>
                                        <option value="---">-----------------------</option>
                                        <option value="Alabama">Alabama</option>
                                        <option value="Alaska">Alaska</option>
                                        <option value="Arizona">Arizona</option>
                                        <option value="Arkansas">Arkansas</option>
                                        <option value="California">California</option>
                                        <option value="Colorado">Colorado</option>
                                        <option value="Connecticut">Connecticut</option>
                                        <option value="Delaware">Delaware</option>
                                        <option value="District Of Columbia">District Of Columbia</option>
                                        <option value="Florida">Florida</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Hawaii">Hawaii</option>
                                        <option value="Idaho">Idaho</option>
                                        <option value="Illinois">Illinois</option>
                                        <option value="Indiana">Indiana</option>
                                        <option value="Iowa">Iowa</option>
                                        <option value="Kansas">Kansas</option>
                                        <option value="Kentucky">Kentucky</option>
                                        <option value="Louisiana">Louisiana</option>
                                        <option value="Maine">Maine</option>
                                        <option value="Maryland">Maryland</option>
                                        <option value="Massachusetts">Massachusetts</option>
                                        <option value="Michigan">Michigan</option>
                                        <option value="Minnesota">Minnesota</option>
                                        <option value="Mississippi">Mississippi</option>
                                        <option value="Missouri">Missouri</option>
                                        <option value="Montana">Montana</option>
                                        <option value="Nebraska">Nebraska</option>
                                        <option value="Nevada">Nevada</option>
                                        <option value="New Hampshire">New Hampshire</option>
                                        <option value="New Jersey">New Jersey</option>
                                        <option value="New Mexico">New Mexico</option>
                                        <option value="New York">New York</option>
                                        <option value="North Carolina">North Carolina</option>
                                        <option value="North Dakota">North Dakota</option>
                                        <option value="Ohio">Ohio</option>
                                        <option value="Oklahoma">Oklahoma</option>
                                        <option value="Oregon">Oregon</option>
                                        <option value="Pennsylvania">Pennsylvania</option>
                                        <option value="Rhode Island">Rhode Island</option>
                                        <option value="South Carolina">South Carolina</option>
                                        <option value="South Dakota">South Dakota</option>
                                        <option value="Tennessee">Tennessee</option>
                                        <option value="texas">Texas</option>
                                        <option value="Utah">Utah</option>
                                        <option value="Vermont">Vermont</option>
                                        <option value="Virginia">Virginia</option>
                                        <option value="Washington">Washington</option>
                                        <option value="est Virginia">West Virginia</option>
                                        <option value="Wisconsin">Wisconsin</option>
                                        <option value="Wyoming">Wyoming</option>
                                    </select>
                                </div>
                                <div class="ErrorMsg state_validation"></div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="ErrorMsg epassErrorSuccess disc-code">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-radion2">
                                        <p class="terms-txt">
                                          By clicking the below button, you agree to our <a href="<?php echo base_url() ?>termsandconditions" target="_blank" rel="canonical">Terms</a> and that you have read our <a href="<?php echo base_url() ?>privacypolicy" target="_blank" rel="canonical">Data Use Policy</a>, including our Cookie Use.
                                        </p>
                               </div>
                           </div>
                       </div>
                       <div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
                           <img class="loaderimg" src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/ajax-loader.gif" style="display:none">
                       </div>



                   </div>
                   <input type="submit" name="submit" id="submit" value="Finish and Pay" class="red-theme-btn upgrade_from">


                   <div class="card-sec row">
                    <div class="col-lg-12  col-12 text-center">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/stripe-lock.png" class="img-fluid stripe-lock" alt="stripe-lock">
                    </div>
                                            <!-- <div class="col-lg-5 col-md-5 col-sm-5 offset-sm-2 offset-md-2  offset-lg-2 col-7">
                                              <p>Cards accepted:</p>
                                              <img src="<?php //echo FS_PATH_PUBLIC_ASSETS;       ?>public/front_end/Updated_Design/img/payment-systm.png" class="img-fluid">
                                          </div> -->
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

  <!-- Model for delete project -->
  <div class="modal fade upgrade_to_five_pop singlesp" id="upgrade_to_five" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h4 class="special-offfer">Special Offer</h4>
                        <h3 class="head-c text-center">Get <?php echo UPGRADE_TO_FIVE_REQUEST; ?> designs for only <span><small>$</small><?php echo UPGRADE_TO_FIVE_PRICE;?></span></h3>
                        <p>Upgrade now and save !</p>
                    </header>
                    <div class="plan-feature">
                        <ul>
                            <li><i class="fas fa-check"></i> <?php echo UPGRADE_TO_FIVE_REQUEST; ?> Design Requests</li>
                        </ul>
                        <div class="price-discount">
                            <div>
                                <div>Plan Price : </div><div>$<?php echo UPGRADE_TO_FIVE_PRICE;?> <span> (39% off)</span></div>
                            </div>
                            <div class="g-totl">
                                <div>Grand Total </div><div>$<?php echo UPGRADE_TO_FIVE_PRICE;?></div>
                            </div>
                        </div>
                        <a href="" class="btn btn-ydelete upgrade_to_five"  data-dismiss="modal" data-flag="0" data-name="<?php echo SUBSCRIPTION_MONTHLY; ?>" data-price="<?php echo SUBSCRIPTION_MONTHLY_PRICE;?>" data-in_progress_request="1" data-display_name = "You can also get 3 active requests just in $<?php echo SUBSCRIPTION_MONTHLY_PRICE;?>">UPGRADE NOW</a>
                        <button class="btn-ndelete downgrade_to_three" data-dismiss="modal" data-flag="0" data-name="<?php echo UPGRADE_TO_THREE_PLAN; ?>" data-price="<?php echo UPGRADE_TO_THREE_PRICE;?>" data-in_progress_request="<?php echo UPGRADE_TO_THREE_REQUEST; ?>" data-display_name = "You can also get 3 active requests just in $<?php echo UPGRADE_TO_THREE_PRICE;?>">No thanks</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Model for delete project -->
<div class="modal fade singlesp upgrade_to_three_pop" id="upgrade_to_three" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                   <header class="fo-rm-header">
                       <h4 class="special-offfer">Special Offer</h4>
                       <h3 class="head-c text-center">Get <?php echo UPGRADE_TO_THREE_REQUEST; ?> designs for only <span><small>$</small><?php echo UPGRADE_TO_THREE_PRICE;?></span></h3>
                       <p>Upgrade now and save !</p>
                   </header>
                   <div class="plan-feature">
                    <ul>
                       <li><i class="fas fa-check"></i> <?php echo UPGRADE_TO_THREE_REQUEST; ?> Design Requests</li>
                   </ul>
                   <div class="price-discount">
                    <div>
                        <div>Plan Price : </div><div>$<?php echo UPGRADE_TO_THREE_PRICE;?> <span> (33% off)</span></div>
                    </div>
                    <div class="g-totl">
                        <div>Grand Total </div><div>$<?php echo UPGRADE_TO_THREE_PRICE;?></div>
                    </div>
                </div>
                <a href="" class="btn btn-ydelete upgrade_to_three"  data-dismiss="modal" data-flag="0" data-name="<?php echo SUBSCRIPTION_MONTHLY; ?>" data-price="<?php echo SUBSCRIPTION_MONTHLY_PRICE;?>" data-in_progress_request="1" data-display_name = "Business Plan">UPGRADE NOW</a>
                <button class="btn-ndelete no_upgrade upgrade_stripe_pln" data-dismiss="modal" data-flag="1" data-name="<?php echo FORTYNINE_REQUEST_PLAN; ?>" data-price="<?php echo FORTYNINE_REQUEST_PRICE;?>" data-in_progress_request="<?php echo FORTYNINE_REQUEST; ?>" data-display_name = "$<?php echo FORTYNINE_REQUEST_PRICE;?> One Active Request">No thanks</button>
            </div>
        </div>
    </div>
</div>
</div>
</div>


<!-- Model for delete project -->
<div class="modal fade singlesp upgrade_to_unlimited_pop" id="upgrade_to_unlimited" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h4 class="special-offfer">Special Offer</h4>
                        <h3 class="head-c text-center">Get Unlimited designs for only <span><small>$</small><?php echo SUBSCRIPTION_MONTHLY_PRICE;?></span></h3>
                        <p>Upgrade now and save !</p>
                    </header>
                    <div class="plan-feature">
                        <ul>
                            <li><i class="fas fa-check"></i> Unlimited designs requests</li>
                        </ul>
                        <div class="price-discount">
                            <div>
                                <div>Plan Price : </div><div>$<?php echo SUBSCRIPTION_MONTHLY_PRICE;?><span> (20% off)</span></div>
                            </div>
                            <div class="g-totl">
                                <div>Grand Total </div><div>$<?php echo SUBSCRIPTION_MONTHLY_PRICE;?></div>
                            </div>
                        </div>
                        <a href="" class="btn btn-ydelete upgrade_to_unlimited upgrade_stripe_pln" data-dismiss="modal" data-flag="1" data-name="<?php echo SUBSCRIPTION_MONTHLY; ?>" data-price="<?php echo SUBSCRIPTION_MONTHLY_PRICE;?>" data-in_progress_request="1" data-display_name = "Get unlimited active requests just in $<?php echo SUBSCRIPTION_MONTHLY_PRICE;?>" >Yes, Upgrade My Plan</a>
                        <button class="btn-ndelete no_upgrade_to_unlimited upgrade_stripe_pln" data-dismiss="modal" data-flag="1">No,  Don't Upgrade</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Model for delete project -->
<!--<div class="modal fade thankyou_upgrade_prop" id="thankyou_upgrade" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <img class="cross_popup" data-dismiss="modal" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cross.png">
                <div class="cli-ent-model">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/thank-you.png" alt="Graphics Zoo" title="Graphics Zoo" />
                    <h2 class="popup_h2 del-txt">Thank You</h2>
                    <header class="fo-rm-header">
                        <h3 class="head-c text-center">Your plan has been upgraded Successfully !</h3>
                    </header>
                    
                </div>
            </div>
        </div>
    </div>
</div>-->

<!--<button style="display: none;" id="downgrade_three" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#upgrade_to_three">click here</button>
<button style="display: none;" id="upgrade_five" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#upgrade_to_five">click here</button>
<button style="display: none;" id="upgrade_unlimited" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#upgrade_to_unlimited">click here</button>
<button style="display: none;" id="thankyou_upgrade_click" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#thankyou_upgrade">click here</button>-->



<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/bootstrap.min.js" ></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/jquery.fancybox.js"></script>


<script type="text/javascript">
	$plan_price = 49;
	$discount_amount = 0;
	$tax_amount = 0;
	$tax_value = 0;
	$discount_notes = '';
	
    function dateFormat(val) {
        if (val.length == 2) {
            $('#expir_date').val($('#expir_date').val() + '/');
        }
    }
    function onlyNumber(val){
        val.replace(/[^0-9.]/g, ''); 
        val = val.replace(/(\..*)\./g, '$1');
    }
    jQuery(document).ready(function ($) {
        $('#grand_total span').html("49");
        $('#final_plan_price').val("49");
		showPriceSectionHTML();
    });
    function Gettaxbasisonstate(statename = ''){
		if(statename == ''){
			var statename = jQuery('#state').val();
		}else{
			var statename = statename;
		}
		var jqueryarray = <?php echo json_encode(STATE_TEXAS); ?>;
		var final_price = parseFloat($plan_price) - parseFloat($discount_amount);
        //var tax = "<?php echo TEXAS; ?>";
        //var grand_total = jQuery('#grand_total span').html();
        //var final_price = jQuery('#final_plan_price').val();
		//console.log('grand_total',grand_total);
		//console.log('final_price',final_price);
		//console.log('jqueryarray',jqueryarray);
		//console.log('statename',statename);
        // if(statename == 'Texas'){
        if((statename in jqueryarray)){ 
            var tax = jqueryarray[statename];
			//console.log('tax from array',tax);
			//console.log(parseFloat(tax));
			var total = parseFloat(final_price) * (parseFloat(tax)/100);
			var tax_rounded = total.toFixed(2); 
			var percent_amount = parseFloat(final_price) + total;
			var total_percent_amount = percent_amount.toFixed(2); 
			//console.log(total);
			//console.log(grand_total);
			//jQuery('.tax_prc_for_texas').css("display","block");
			//jQuery('.tax_prc_for_texas').html('Sales Tax ('+tax+'%) :<span id="tax_prc" class="box item12"><span>$'+tax_rounded+'</span></span>');
			//jQuery('#grand_total span').html(total_percent_amount);
			jQuery('#selected_price').val(total_percent_amount);
			jQuery('#tax_amount').val(total);
			jQuery('#state_tax').val(tax);
			$tax_amount = total;
			$tax_value = tax;
		}else{
			//jQuery('.tax_prc_for_texas').css("display","none");
			//jQuery('#grand_total span').html(final_price);
			jQuery('#tax_amount').val("0");
			jQuery('#selected_price').val(final_price);
			jQuery('#state_tax').val("0");
			$tax_amount = 0;
			$tax_value = 0;
		}
		//$discount_amount = 0;
		//$('.disc-code').value('');
		showPriceSectionHTML();
	}

	jQuery('#state').on('change', function () {
		var statename = this.value;
		if(statename == '---'){
			jQuery('.ErrorMsg.state_validation').css('display','block');
			jQuery('.ErrorMsg.state_validation').html('<span class="validation">Please select any other state</span>')
			jQuery('#state').val("");
		}else{
			jQuery('.ErrorMsg.state_validation').html('')
		}
		Gettaxbasisonstate(statename);
	});
	
	function showPriceSectionHTML(){
		$plan_price = parseFloat($plan_price);
		$tax_amount = parseFloat($tax_amount);
		$discount_amount = parseFloat($discount_amount);
		var total_price = (($plan_price +  $tax_amount) - $discount_amount);
		if(total_price % 1 !== 0){
			total_price = total_price.toFixed(2);
		}
		var htmll = '<p class="plan-price">Plan Price:<span id="change_prc" class="box item1">$<span>'+$plan_price+'</span></span></p>';
		if(parseFloat($discount_amount) > 0){
			htmll += '<p class="plan-price">Discount: <span class="box item12"><span>- $'+$discount_amount.toFixed(2)+'</span></span></p>';	
		}
		if(parseFloat($tax_amount) > 0){
			htmll += '<p class="plan-price tax_prc_for_texas">Sales Tax ('+$tax_value.toFixed(2)+'%) :<span id="tax_prc" class="box item12"><span>$'+$tax_amount.toFixed(2)+'</span></span></p>';
		}
		htmll += '<hr>';
		htmll += '<div class="g-total"><h3>Grand Total <span id="grand_total">';
		if(parseFloat($discount_amount) > 0){
			htmll += '<strike>$';
			htmll += parseFloat($plan_price) + parseFloat($tax_amount.toFixed(2));
			htmll += '</strike> $'+total_price;
		} else{
			htmll += '$'+total_price;
		}
		htmll += '</span></h3></div>';
		if(parseFloat($discount_amount).toFixed(2) > 0){
			htmll += '<span class="coupon-des-newsignup">'+$discount_notes+'</span>';
		}
		$('.total-pPrice').html(htmll);
	}
	
	$('.disc-code').blur(function (e){
        e.preventDefault();
        var dicountCode = $('.disc-code').val();
        var setSelected = $plan_price;
        var planname = "<?php echo FORTYNINE_REQUEST_PLAN; ?>";
        $("span.CouponSucc_code, span.CouponErr_code").remove();
        if(dicountCode == ''){
            $('.ErrorMsg').css('display','none');
            //$('.coupon-des-newsignup').css({"display": "none"});
            // $('#selected_price').val(setSelected);
            //jQuery('#grand_total span').html(setSelected);
			$discount_amount = 0;
			$discount_notes = '';
			Gettaxbasisonstate();
			//showPriceSectionHTML();
            return;
		}else{
			$('.ErrorMsg').css('display','block');
		}
		$('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponSucc_code"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" alt="loading"/></span>');
		delay(function(){
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url(); ?>welcome/CheckCouponValidation',
				data: {dicountCode: dicountCode,planname:planname},
				dataType: 'json',
				success: function (data) {
					//console.log(data);
                                        $('#couponinserted_ornot').val(data.inertcoupon);
					$('.ErrorMsg').css('display','block');
					if (data.status == '1') {
						if(data.return_data.valid == '1'){
							if(data.return_data.amount_off !== null){
								var str = data.return_data.amount_off;
								var resStr = str/100;
								$discount_amount = resStr;
								//var finalAmount = setSelected - resStr;                              
							} else{
								var TotalPercent = data.return_data.percent_off;
								var calcPrice  =  setSelected * (TotalPercent / 100 );
								$discount_amount = calcPrice;
								//var finalAmount = setSelected - calcPrice;
							   // console.log(finalAmount);
						    }
						    //var total_finalAmountt = finalAmount.toFixed(2); 
						    //$('#grand_total span').html(orginal_price + ' $'+total_finalAmountt);
						    //jQuery('#final_plan_price').val(total_finalAmountt);
						    $('.CouponErr_code').css({"display": "none"});
						    //$('.coupon-des-newsignup').css('display','block');   
						    $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponSucc_code"><i class="fas fa-check-circle"></i> Coupon Applied</span>');
						    var discount_notes = '';
							if(data.return_data.duration == 'forever'){
								//$('.coupon-des-newsignup').html('<p>Forever</p>');
								discount_notes = '<p>Forever</p>';
							}else if(data.return_data.duration == 'once'){
								//$('.coupon-des-newsignup').html('<p>1st Month Only</p>');
								discount_notes = '<p>1st Month Only</p>';
							} else{
								//$('.coupon-des-newsignup').html('<p>First ' + data.return_data.duration_in_months + ' Months Only</p>');
								discount_notes = '<p>First ' + data.return_data.duration_in_months + ' Months Only</p>';
							}
							$discount_notes = discount_notes;
							Gettaxbasisonstate();
							//showPriceSectionHTML(discount_notes);
						}
					} else{
					    //$('#grand_total span').html(setSelected);
					    $('.CouponSucc_code').css({"display": "none"});
					    $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponErr_code"><i class="fas fa-times-circle"></i> ' + data.message + '</span>');
					    //$('.coupon-des-newsignup').css({"display": "none"});
						$discount_amount = 0;
						$discount_notes = '';
						Gettaxbasisonstate();
						//showPriceSectionHTML();
					}
				}
			});  
		}, 1000 );
		Gettaxbasisonstate();
	});
	
	var formdata = "";
	$( "form" ).submit(function(e) {
		// $(document).on('click','#submit',function(e){
		e.preventDefault();
		$('.loaderimg').show();
		$('#submit').prop('disabled',true); 
		formdata = $(this).serialize();
		// $('#upgrade_five').trigger('click');
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>welcome/single_request_signup",
			dataType: "json",
			data: {"formdata": formdata, "submit_form": 1},
			success: function (data) {
				//console.log('status',data.status);
				if(data.status == true){
					$('#upgrade_to_five').modal('show');
					$('#customer_id').val(data.customer_id);
				}else{
					//  console.log('message12',data.message);
					$('.alert-danger').show();
					$('.alert.alert-danger.alert-dismissable strong').html(data.message);
					$('html, body').animate({
						scrollTop: $('#single_request_signup').offset().top - 20
					}, 'slow');
					$('.loaderimg').hide();
					$('#submit').prop('disabled',false); 
				}
				//console.log('message',data.message);
			}
			//window.location.reload();
		});
	});
	/**********Yes click*************/
	$(document).on('click','.upgrade_to_five',function(e){
		e.preventDefault();
		//$('#upgrade_to_five').hide();
		$('.no_upgrade_to_unlimited').attr({	
			"data-name":"<?php echo UPGRADE_TO_FIVE_PLAN; ?>", 
			"data-price":"<?php echo UPGRADE_TO_FIVE_PRICE; ?>",
			"data-display_name":"<?php echo UPGRADE_TO_FIVE__PLAN_NAME; ?>",
			"data-in_progress_request":"<?php echo UPGRADE_TO_FIVE_REQUEST; ?>"
		});
		$('#upgrade_to_five').modal('hide'); 
		//$('#upgrade_unlimited').trigger('click');
		$("#upgrade_to_five").on('hidden.bs.modal', function () {
			$('#upgrade_to_unlimited').modal('show'); 
		});
	});
	$(document).on('click','.upgrade_to_unlimitd',function(e){
		e.preventDefault();
		$('#upgrade_to_unlimited').modal('hide');
	});
	
	/**********No click*************/
	$(document).on('click','.downgrade_to_three',function(event){
		event.preventDefault();
		// $('#downgrade_three').trigger('click');
		$('#upgrade_to_five').modal('hide');
		$("#upgrade_to_five").on('hidden.bs.modal', function () {
			$('#upgrade_to_three').modal('show');
		});
	}); 
	$(document).on('click','.no_upgrade',function(event){
		event.preventDefault();
		$('#upgrade_to_three').modal('hide'); 
	});
	$(document).on('click','.no_upgrade_to_unlimited',function(event){
		event.preventDefault();
		$('#upgrade_to_unlimited').modal('hide');
	});
	$(document).on('click','.upgrade_to_three',function(event){
		event.preventDefault();
		$('.no_upgrade_to_unlimited').attr({
			"data-name":"<?php echo UPGRADE_TO_THREE_PLAN; ?>", 
			"data-price":"<?php echo UPGRADE_TO_THREE_PRICE; ?>",
			"data-display_name":"<?php echo UPGRADE_TO_THREE__PLAN_NAME; ?>",
			"data-in_progress_request":"<?php echo UPGRADE_TO_THREE_REQUEST; ?>"
		});
   
		$('#upgrade_to_three').modal('hide');
			$("#upgrade_to_three").on('hidden.bs.modal', function () {
			$('#upgrade_to_unlimited').modal('show');
		});
	});
	$(document).on('click','.upgrade_stripe_pln',function(){
        var plan_name = $(this).attr('data-name');
        var plan_price = $(this).attr('data-price');
        var customer_id = $('#customer_id').val();
        var flag = $(this).attr('data-flag');
        var display_name = $(this).attr('data-display_name');
        var inprogres_req = $(this).attr('data-in_progress_request');
        $('.alert.alert-danger.alert-dismissable').css("display,none");
		//console.log('plan_name',plan_name);
		//console.log('plan_price',plan_price);
		//console.log('inprogres_req',inprogres_req);
		//console.log('flag',flag);
		if(flag == 1){
			//console.log('flag 1 ');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>welcome/single_request_signup",
				dataType: "json",
				data: {"formdata": formdata,"submit_form": 0, "customer_id":customer_id,"plan_name": plan_name,"plan_price":plan_price,"inprogres_req":inprogres_req,"display_name":display_name},
				success: function (data) {
					//console.log('data',data.message);
					if(data.status == true){
						window.location.href = '<?php echo base_url() . "requests_thankyou"; ?>';
					}else{
						$('.alert-danger').show();
						$('.alert.alert-danger.alert-dismissable strong').html(data.message);
						$(this).attr('data-flag',0);
						$('html, body').animate({
							scrollTop: $('#single_request_signup').offset().top - 20 //#DIV_ID is an example. Use the id of your destination on the page
						}, 'slow');
						$('.loaderimg').hide();
						$('#submit').prop('disabled',false); 
					}
				}
				//window.location.reload();
			});
		}
	});
	var delay = (function(){
		var timer = 0;
		return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();
	

	!function () {
		var t;
		if (t = window.driftt = window.drift = window.driftt || [], !t.init)
			return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
				t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
				t.factory = function (e) {
					return function () {
						var n;
						return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
					};
				}, t.methods.forEach(function (e) {
					t[e] = t.factory(e);
				}), t.load = function (t) {
					var e, n, o, i;
					e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
					o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
					n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
				});
	}();
	drift.SNIPPET_VERSION = '0.3.1';
	drift.load('d6yi38hkhwsd');
	<!-- Drift Code ---->
</script>
<!-- REFERSION TRACKING: BEGIN -->
<script rel="nofollow" src="//graphicszoo.refersion.com/tracker/v3/pub_01537b7d8dcde95d5a0d.js"></script>
<script>_refersion();</script>
<!-- REFERSION TRACKING: END -->
<!-- UTM Tracking: BEGIN -->
<script rel="nofollow" src="https://d12ue6f2329cfl.cloudfront.net/resources/utm_form-1.0.4.min.js" async></script>
<script type="text/javascript" charset="utf-8">
  var _uf = _uf || {};
  _uf.utm_source_field           = "YOUR_SOURCE_FIELD"; // Default 'USOURCE'
  _uf.utm_medium_field           = "YOUR_MEDIUM_FIELD"; // Default 'UMEDIUM'
  _uf.utm_campaign_field         = "YOUR_CAMPAIGN_FIELD"; // Default 'UCAMPAIGN'
  _uf.utm_content_field          = "YOUR_CONTENT_FIELD"; // Default 'UCONTENT'
  _uf.utm_term_field             = "YOUR_TERM_FIELD"; // Default 'UTERM'
    _uf.sessionLength = 2; // In hours. Default is 1 hour
 _uf.add_to_form = "first"; // 'none', 'all', 'first'. Default is 'all'
</script>
<!-- UTM Tracking: END -->
</body>
</html>