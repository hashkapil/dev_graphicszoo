<!--==========================
Hero Section
============================-->
<?php
$actual_link = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//echo "<pre/>";print_R($data);
?>
<section id="hero_section" class="wow fadeIn pb-2">
    <div class="hero-container">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="blog-full-img"><img style="width:100%" src="<?php echo FS_PATH_PUBLIC_UPLOADS_BLOGS . $data[0]['image']; ?>" alt="<?php echo pathinfo($data[0]['image'], PATHINFO_FILENAME);?>"></p></div>
                <div class="col-md-6">
                    <h1><?php echo $data[0]['title']; ?></h1>
                </div>
                <!-- <span class="mt-2" style="color:#888888">by Author on</span> -->
            </div>
        </div>
    </div>
</section><!-- #hero -->

<div class="container">
    <div class="row">
        <div class="social">
            <ul class="list-unstyled" data-spy="affix" data-offset-top="540" data-offset-bottom="580">
                <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>&redirect_uri=https://facebook.com&title=<?php echo $data[0]['title']; ?>" rel="nofollow"><i class="fab fa-facebook-square"></i></a></li>
                <li><a target="_blank" href="http://twitter.com/home?status=<?php echo $data[0]['title']; ?>+<?php echo $actual_link; ?>" rel="nofollow"><i class="fab fa-twitter"></i></a></li> 
                <li><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $actual_link; ?>&title=<?php echo $data[0]['title']; ?>&source=<?php echo "graphicszoo.com"; ?>" rel="nofollow"><i class="fab fa-linkedin-in"></i></a></li>                    
            </ul>
        </div>
        <div class="col-md-9 single-blog-cont ">
            <p><?php
                echo $data[0]['body'];
// echo "<pre>"; print_r($recomdata); 
                ?></p>
            <div class='row recommended_blog'>
                <?php
                if (!empty($recomdata)) {
//                     echo "<pre>"; print_r($recomdata); 
                    ?>
                    <h2>Recommended Blogs</h2>
                    <?php foreach ($recomdata as $reckey => $recdata) { ?>
                        <div class="col-md-6">
                            <div class="content-blog">
                                <a rel="canonical" href='<?php echo base_url() . "article/" . $recdata['slug']; ?>' target='_blank'>
                                    <div class="image">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_BLOGS . $recdata['image']; ?>" alt="<?php echo pathinfo($recdata['image'], PATHINFO_FILENAME);?>">
                                    </div>
                                    <p class="post-box-title"><?php echo $recdata['title']; ?></p>
                                </a>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="col-md-12 text-center">
                <a href="<?php echo base_url(); ?>blog" class="red-theme-btn" style="color: #fff !important; text-decoration: none">
                    View all blogs
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid" alt="long-arrow">
                </a>
            </div>
        </div>
    </div>


</div>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>