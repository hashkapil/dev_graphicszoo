<section class="page-heading-sec">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h1>Forgot Password</h1>
  </div>
</div>
</div>
</section>
<section id="signin">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
          <form method="post" class="signin-form" action="">
            <div class="form-signin-group col-md-12 inner-addon left-addon">
            <input type="password" name="password" class="form-control" id="uname" placeholder="Password">
            <div class="validation"></div>
            </div>
            <div class="form-signin-group col-md-12 inner-addon left-addon">
            <input type="password" name="cpassword" class="form-control" id="password" placeholder="Confirm password">
            <div class="validation"></div>
            </div>
            <div class="form-signin-group col-md-12 inner-addon">
            <button type="submit" class="red-theme-btn">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</section>