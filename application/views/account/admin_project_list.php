<?php 
if(isset($client_url) && $client_url != ''){
    $comefrm = $client_url;
}elseif(isset($designer_url) && $designer_url != ''){
     $comefrm = $designer_url;
}else{
     $comefrm = '';
}
?>
<section class="con-b">
    <div class="container-fluid">
        <div class="row_msg">
            <div class="alert alert-danger alert-dismissable error" style="display:none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c" id="show_error"></p>
            </div>
            <div class="alert alert-success alert-dismissable success" style="display:none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"></p>
            </div>
        </div>
        <div class="header-blog">
            <input type="hidden" value="" class="bucket_type"/>
            <div class="row flex-show">
                <div class="col-md-12">
                    <div class="flex-this">
                        <h2 class="main_page_heading">Projects</h2>
                        <div class="header_searchbtn">
                            <?php if (!$full_admin_access && $_GET['assign'] == 1) { ?>
                                <a href="<?php echo base_url(); ?>admin/dashboard" class="addderequesr">
                                    <span class="">All Project</span>
                                </a>
                            <?php } else if (!$full_admin_access) { ?>
                                <a href="<?php echo base_url(); ?>admin/dashboard/index?assign=1" class="addderequesr">
                                    <span class="">Project assign to me </span>
                                </a>
                            <?php } ?>
                            <a href="<?php echo base_url(); ?>admin/request/add_request" class="addderequesr">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/add_request.svg">
                                <span class="">Add Request</span>
                            </a>
                            <a href="javascript:void(0)" class="adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $active_project[$i]['id']; ?>" data-designerid= "<?php echo $active_project[$i]['designer_id']; ?>">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer.svg" class="img-responsive"><span>Assign Designer</span>
                            </a>
                            <div class="search-first">
                                <div class="focusout-search-box">
                                    <div class="search-box">
                                        <form method="post" class="search-group clearfix">
                                            <input type="text" placeholder="Search here..." class="form-control searchdata searchdata_admin search_text">
                                            <div class="ajax_searchload" style="display:none;">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                            </div>
                                            <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                            <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                            <button type="submit" class="search-btn search search_data_ajax">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php if((!isset($_GET)) || empty($_GET)){ ?>
                            <a class="dwl-oadfi-le" id="download_rquests" data-status= "active,disapprove" href="<?php echo base_url(); ?>admin/Dashboard/downloadrequest_accstatus/active-disapprove/<?php echo $bucket_type; ?>">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 12 15" enable-background="new 0 0 12 15" xml:space="preserve">
                                <g>
                                <g>
                                <path fill="#7f7f7f" d="M5.3,11.4C5.3,11.5,5.4,11.5,5.3,11.4c0.5,0.4,1,0.4,1.4,0c0.9-1,1.8-2,2.7-3c0,0,0.1-0.1,0.1-0.1
                                      c0.2-0.3,0.3-0.6,0.2-1c-0.2-0.7-1.1-1-1.6-0.4C7.8,7.3,7.4,7.7,7,8.2c0,0,0,0-0.1,0.1V8.1c0-2.3,0-4.7,0-7C7,1.1,7,1,7,1
                                      c0-0.4-0.2-0.7-0.6-0.9C6.3,0,6.2,0,6.1,0H5.9C5.8,0,5.6,0.1,5.5,0.2C5.2,0.4,5,0.7,5,1.1c0,2.3,0,4.7,0,7v0.1c0,0,0,0,0,0
                                      c0,0,0,0,0-0.1C4.6,7.7,4.2,7.3,3.8,6.9C3.5,6.6,3.1,6.5,2.7,6.7C2.1,7,2,7.9,2.5,8.4C3.4,9.4,4.4,10.4,5.3,11.4z M12,10.5
                                      c0-0.6-0.6-1.1-1.2-1c-0.4,0.1-0.7,0.5-0.7,1c0,0.7,0,1.5,0,2.2v0.1H1.9v-0.1c0-0.7,0-1.5,0-2.2c0,0,0-0.1,0-0.1
                                      c0-0.5-0.4-1-0.9-1c-0.6,0-1,0.4-1,1c0,1.1,0,2.3,0,3.4c0,0.1,0,0.1,0,0.2C0.1,14.7,0.5,15,1,15c3.3,0,6.7,0,10,0
                                      c0.6,0,1-0.4,1-1.1C12,12.8,12,11.7,12,10.5C12,10.6,12,10.5,12,10.5z"/>
                                </g>
                                </g>
                                </svg>
                            </a>
                            <?php } ?>

                            <div class="filter-site">
                                <i class="fas fa-sliders-h"></i>
                            </div>
                            <div class="gz-filter">
                                <div class="filter-header">
                                    <h2>Filter</h2>
                                    <a href="javascript:void(0)" class="close-filter">x</a>
                                </div>
                                <form method="Post" action="<?php echo base_url().'admin/dashboard/filter_form_submit'?>">
                                    <div class="filter-body">
                                        <div class="frmSearch">
                                            <input type="text" name="client_name" id="search_client" size="50"/>
                                            <div id="suggesstion-box"></div>
                                        </div>
                                        <br/>
                                        <select name="" id="" class="designers_show">
                                            <option value="">Choose Designers</option>
                                            <option value="designers">select Designer</option>
                                        </select>
                                        <ul class="designers_list" style="display: none;">
                                            <?php foreach ($designerlist as $dk => $dv) { ?>
                                                <li><input type="checkbox" class="des_name" name="des_name[]" value="<?php echo $dv['id']; ?>"/><?php echo $dv['first_name']; ?></li>
                                            <?php } ?>
                                        </ul>
                                        <div class="status-filter">
                                            <h4>Project Status</h4>
                                            <div class="sound-signal2">
                                                <div class="form-radion2">
                                                    <label class="containerr">IN-PROGRESS
                                                        <input type="checkbox" name="pro_status[]" value="active" checked="checked">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">IN-Queue
                                                        <input type="checkbox" name="pro_status[]" value="assign" checked="checked">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">PENDING REVIEW
                                                        <input type="checkbox" name="pro_status[]" value="pendingrevision">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">PENDING APPROVAL
                                                        <input type="checkbox" name="pro_status[]" value="checkforapprove">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                 <div class="form-radion2">
                                                    <label class="containerr">COMPLETED
                                                        <input type="checkbox" name="pro_status[]" value="approved">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">ON-HOLD
                                                        <input type="checkbox" name="pro_status[]" value="hold">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">CANCEL
                                                        <input type="checkbox" name="pro_status[]" value="cancel">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                         <?php if (!$full_admin_access && $_GET['assign'] == 1) { ?>
                                            <a href="<?php echo base_url(); ?>admin/dashboard" class="addderequesr">
                                                <span class="">All Project</span>
                                            </a>
                                        <?php } else if (!$full_admin_access) { ?>
                                            <a href="<?php echo base_url(); ?>admin/dashboard/index?assign=1" class="addderequesr">
                                                <span class="">Project assign to me </span>
                                            </a>
                                        <?php } ?>
                                        <div class="status-filter date-filter">
                                            <h4>Task Time</h4>
                                            <div class="date-stand">
                                                <div class="date-from">
                                                    <label>From</label>
                                                    <input type="date" class="week-picker hasDatepicker">
                                                </div>
                                            </div>
                                             <div class="date-stand">
                                                <div class="date-from">
                                                    <label>To</label>
                                                    <input type="date" class="week-picker hasDatepicker">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="status-filter verify-option">
                                            <h4>VERIFY OPTION</h4>
                                            <div class="sound-signal2">
                                                <div class="form-radion2">
                                                    <label class="containerr">VERIFY
                                                        <input type="checkbox" name="filetype[]" value="SourceFiles" checked="checked">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-radion2">
                                                    <label class="containerr">VERIFIED
                                                        <input type="checkbox" name="filetype[]" value="jpeg">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter_button">
                                            <input type="submit" name="submit" value="Apply Filter">
                                            <!--<a href="" class="more_load">Apply Filter</a>-->
                                            <a href="<?php echo base_url().'admin/dashboard/' ?>" class="reset">Reset</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-12">
                    <ul  id="status_check" class="list-unstyled list-header-blog admin_pro_listing" role="tablist" style="border:none;">
                        <!--                    <ul class="list-unstyled list-header-blog small" id="tab_menu">-->
                        <li class="active" id="1">
                            <a data-toggle="tab" href="#Admin_active" data-isloaded="0" data-status="active,disapprove" data-tabid="1" role="tab" data-pagin ="<?php echo $count_project_a; ?>">Active <span><?php echo $count_project_a; ?></span> </a>
                        </li>
                        <li id="2">
                            <a data-toggle="tab" href="#Admin_in_queue" data-isloaded="0" data-status="assign" role="tab" data-tabid="2" data-pagin ="<?php echo $count_project_i; ?>">In Queue <span><?php echo $count_project_i; ?></span>  </a>
                        </li>  
                        <li id="3">
                            <a data-toggle="tab" href="#Admin_pending_review" data-isloaded="0" data-status="pendingrevision" data-tabid="3" data-pagin ="<?php echo $count_project_p; ?>" role="tab">Pending Review <span><?php echo $count_project_p; ?></span></a>
                        </li>         
                        <li id="4">
                            <a data-toggle="tab" href="#Admin_pending_approval" data-isloaded="0" data-status="checkforapprove" data-tabid="4" data-pagin ="<?php echo $count_project_pe; ?>" role="tab">Pending Approval <span><?php echo $count_project_pe; ?> </span></a>
                        </li>       
                        <li id="5">
                            <a data-toggle="tab" href="#Admin_completed" data-isloaded="0" data-status="approved" data-tabid="5" data-pagin ="<?php echo $count_project_c; ?>" role="tab">Completed <span><?php echo $count_project_c; ?> </span></a>
                        </li> 
                        <li id="6">
                            <a data-toggle="tab" href="#Admin_hold" data-isloaded="0" data-status="hold" data-tabid="6" data-pagin ="<?php echo $count_project_h; ?>" role="tab">On Hold <span><?php echo $count_project_h; ?></span></a>
                        </li> 
                        <li id="7">
                            <a data-toggle="tab" href="#Admin_cancel" data-isloaded="0" data-status="cancel" data-tabid="7" data-pagin ="<?php echo $count_project_cancel; ?>" role="tab">Cancelled <span><?php echo $count_project_cancel; ?></span></a>
                        </li> 
                    </ul>

                </div>
            </div>
        </div>
        <div class="cli-ent table">
            <div class="tab-content">
                <!--Admin Active Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_project_a; ?>" class="tab-pane active content-datatable datatable-width" id="Admin_active" role="tabpanel">
                    <div class="product-list-show">
                        <?php //if ($count_project_a >= 1) { ?>
                            <div class="cli-ent-row tr tableHead"> 
                                <div class="cli-ent-col td">
                                    <div class="sound-signal">
                                        <div class="form-radion2">
                                            <label class="containerr">
                                                <input type="checkbox" class="checkAll" data-attr="in_active" name="project">  
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 30%;">
                                    PROJECT NAME
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    CLIENT
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    DESIGNER
                                </div>
                                <div class="cli-ent-col td" style="width: 16%;">
                                    PROJECT STATUS
                                </div>
                                <div class="cli-ent-col td"style="width: 9%;">
                                    VERIFIED
                                </div>
                                <div class="cli-ent-col td"style="width:15%;">
                                    TASK TIME
                                </div>
                                <div class="cli-ent-col td"style="width: 10%;">
                                    Action
                                </div>
                            </div>
                        <?php //} ?>
                        <div class="row two-can"> 
                            <?php
                            if($count_project_a != 0){
                            for ($i = 0; $i < count($active_project); $i++) {
                                if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                                    $data['projects'] = $active_project[$i];
                                    if($client_url){
                                        $data['projects']['bkurl'] = $comefrm;
                                    }elseif($designer_url){
                                        $data['projects']['bkurl'] = $comefrm;
                                    }else{
                                        $data['projects']['bkurl'] = '';
                                    }
                                    $this->load->view('admin/project_listing_temp', $data);
                                }
                            }}else{
                                echo "<div class='no_data_ajax'>No record found</div>";
                            }
                            ?>
                        </div>
                        <?php if ($count_project_a > LIMIT_ADMIN_LIST_COUNT) { ?>
                            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" />
                            </div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_a; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--Admin Active Section End Here -->

                <!--Admin IN Queue Section Start Here -->
                <div data-group="1" data-loaded="" data-total-count="<?php echo $count_project_i; ?>" class="tab-pane content-datatable datatable-width" id="Admin_in_queue" role="tabpanel">
                    <div class="product-list-show">
                        <?php //if ($count_project_i >= 1) { ?>
                            <div class="cli-ent-row tr tableHead">
                                <div class="cli-ent-col td">
                                    <div class="sound-signal">
                                        <div class="form-radion2">
                                            <label class="containerr">
                                                <input type="checkbox" class="checkAll" data-attr="in_queue" name="project">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 30%;">
                                    PROJECT  NAME
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    CLIENT
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    DESIGNER
                                </div>
                                <div class="cli-ent-col td" style="width: 16%;">
                                    PROJECT STATUS
                                </div>
                                <div class="cli-ent-col td" style="width: 9%;">
                                    VERIFIED
                                </div>
                                <div class="cli-ent-col td" style="width: 15%;">
                                    CREATED
                                </div>
                                <div class="cli-ent-col td"style="width: 10%;">
                                    ACTION
                                </div>
                            </div>
                        <?php //} ?>
                        <div class="row two-can">   </div>
                        <?php if ($count_project_i > LIMIT_ADMIN_LIST_COUNT) { ?>
                            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_i; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--Admin IN Queue Section End Here -->

                <!--Admin Pending Review Section Start Here -->
                <div data-group="1" data-loaded="" data-total-count="<?php echo $count_project_p; ?>" class="tab-pane content-datatable datatable-width" id="Admin_pending_review" role="tabpanel">
                    <div class="product-list-show">
                        <?php //if ($count_project_p >= 1) { ?>
                            <div class="cli-ent-row tr tableHead">
                                <div class="cli-ent-col td">
                                    <div class="sound-signal">
                                        <div class="form-radion2">
                                            <label class="containerr">
                                                <input type="checkbox" class="checkAll" name="project" data-attr="pending_rev">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 30%;">
                                    PROJECT NAME
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    CLIENT
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    DESIGNER
                                </div>
                                <div class="cli-ent-col td" style="width: 16%;">
                                    PROJECT STATUS
                                </div>
                                <div class="cli-ent-col td"style="width: 9%;">
                                    VERIFIED
                                </div>
                                <div class="cli-ent-col td"style="width: 15%;">
                                    TASK TIME
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    Action
                                </div>
                            </div>
                        <?php //} ?>
                        <div class="row">   </div>
                        <?php if ($count_project_p > LIMIT_ADMIN_LIST_COUNT) { ?>
                            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_p; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--Admin Pending Review End Here -->

                <!--Admin Pending Section Section Start Here -->
                <div data-group="1" data-loaded="" data-total-count="<?php echo $count_project_pe; ?>" class="tab-pane content-datatable datatable-width" id="Admin_pending_approval" role="tabpanel">

                    <div class="product-list-show">
                        <?php //if ($count_project_pe >= 1) { ?>
                            <div class="cli-ent-row tr tableHead">
                                <div class="cli-ent-col td">
                                    <div class="sound-signal">
                                        <div class="form-radion2">
                                            <label class="containerr">
                                                <input type="checkbox" class="checkAll" name="project" data-attr="pending_app">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 30%;">
                                    PROJECT NAME
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    CLIENT
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    DESIGNER 
                                </div>
                                <div class="cli-ent-col td" style="width: 16%;">
                                    PROJECT STATUS
                                </div>
                                <div class="cli-ent-col td"style="width: 9%;">
                                    VERIFIED
                                </div>
                                <div class="cli-ent-col td"style="width: 15%;">
                                    TASK TIME
                                </div>
                                <div class="cli-ent-col td"style="width: 10%;">
                                    Action
                                </div>
                            </div>
                        <?php //} ?>
                        <div class="row two-can">   </div>
                        <?php if ($count_project_pe > LIMIT_ADMIN_LIST_COUNT) { ?>
                            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_pe; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--Admin Pending Section End Here -->

                <!--Admin Completed Section Start Here -->
                <div data-group="1" data-loaded="" data-total-count="<?php echo $count_project_c; ?>" class="tab-pane content-datatable datatable-width" id="Admin_completed" role="tabpanel">
                    <div class="product-list-show">
                        <?php //if ($count_project_c >= 1) { ?>
                            <div class="cli-ent-row tr tableHead">
                                <div class="cli-ent-col td">
                                    <div class="sound-signal">
                                        <div class="form-radion2">
                                            <label class="containerr">
                                                <input type="checkbox" class="checkAll" data-attr="completed" name="project">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 30%;">
                                    PROJECT NAME
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    CLIENT
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    DESIGNER
                                </div>
                                <div class="cli-ent-col td" style="width: 16%;">
                                    PROJECT STATUS
                                </div>
                                <div class="cli-ent-col td"style="width: 9%;">
                                    VERIFIED
                                </div>
                                <div class="cli-ent-col td"style="width: 15%;">
                                    Approved
                                </div>
                                <div class="cli-ent-col td"style="width: 10%;">
                                    Action
                                </div>
                            </div>
                        <?php //} ?>
                        <div class="row two-can">   </div>
                        <?php if ($count_project_c > LIMIT_ADMIN_LIST_COUNT) { ?>
                            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_c; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--Admin Completed Section End Here -->

                <!--Admin Hold Section Start Here -->
                <div data-group="1" data-loaded="" data-total-count="<?php echo $count_project_h; ?>" class="tab-pane content-datatable datatable-width" id="Admin_hold" role="tabpanel">
                    <div class="tab-pane content-datatable datatable-width" id="Admin_hold" role="tabpanel">

                        <div class="product-list-show">
                            <?php //if ($count_project_h >= 1) { ?>
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <div class="sound-signal">
                                            <div class="form-radion2">
                                                <label class="containerr">
                                                    <input type="checkbox" class="checkAll" data-attr="hold" name="project">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width:30%;">
                                        PROJECT NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 10%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 10%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        VERIFIED
                                    </div>
                                    <div class="cli-ent-col td"style="width: 15%;">
                                        Hold
                                    </div>
                                    <div class="cli-ent-col td"style="width: 10%;">
                                        ACTION
                                    </div>
                                </div>
                            <?php //} ?>
                            <div class="row two-can">                    

                            </div>
                            <?php if ($count_project_h > LIMIT_ADMIN_LIST_COUNT) { ?>
                                <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                                <div class="" style="text-align:center">
                                    <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_h; ?>" class="load_more button">Load more</a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!--Admin Hold Section End Here -->
                <!--Admin Cancel Section Start Here -->
                <div data-group="1" data-loaded="" data-total-count="<?php echo $count_project_cancel; ?>" class="tab-pane content-datatable datatable-width" id="Admin_cancel" role="tabpanel">
                    <div class="tab-pane content-datatable datatable-width" id="Admin_cancel_pro" role="tabpanel">

                        <div class="product-list-show">
                            <?php //if ($count_project_cancel >= 1) { ?>
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <div class="sound-signal">
                                            <div class="form-radion2">
                                                <label class="containerr">
                                                    <input type="checkbox" class="checkAll" data-attr="cancel" name="project">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 30%;">
                                        PROJECT NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 10%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 10%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        VERIFIED
                                    </div>
                                    <div class="cli-ent-col td"style="width: 15%;">
                                        Cancel On 
                                    </div>
                                    <div class="cli-ent-col td"style="width: 10%;">
                                        ACTION
                                    </div>
                                </div>
                            <?php //} ?>
                            <div class="row two-can">                    

                            </div>
                            <?php if ($count_project_cancel > LIMIT_ADMIN_LIST_COUNT) { ?>
                                <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                                <div class="" style="text-align:center">
                                    <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_cancel; ?>" class="load_more button">Load more</a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!--Admin Cancel Section End Here -->
            </div>                   
        </div>        
    </div>
</section>


<!-- Modal -->
<div class="modal similar-prop nonflex fade" id="AddDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add Designer</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="assigndesigner_err"></div>
                <div class="cli-ent-model">
                    <div class="noti-listpopup">
                        <div class="newsetionlist">
                            <div class="cli-ent-row tr notificate">
                                <div class="cli-ent-col td" style="width: 33%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Designer</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 45%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Skill</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 22%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b text-center">Active Requests</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="" method="post">
                            <ul class="list-unstyled list-notificate two-can">
                                <?php foreach ($designer_list as $designers) { ?>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="sound-signal">
                                                        <div class="form-radion">
                                                            <input class="selected_btn" type="radio" value="<?php echo $designers['id']; ?>" name="assign_designer" id="<?php echo $designers['id']; ?>" data-image-pic="<?php echo $designers['profile_picture'] ?>" data-name="<?php echo $designers['first_name'] . " " . $designers['last_name']; ?>">
                                                            <label for="<?php echo $designers['id']; ?>" data-image-pic="<?php echo $designers['profile_picture'] ?>"></label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 30%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="setnoti-fication">
                                                            <figure class="pro-circle-k1">
                                                                <img src="<?php echo $designers['profile_picture'] ?>" class="img-responsive">
                                                            </figure>

                                                            <div class="notifitext">
                                                                <p class="ntifittext-z1">
                                                                    <strong>
                                                                        <?php echo $designers['first_name'] . " " . $designers['last_name']; ?>

                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 45%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 25%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cli-ent-xbox text-center">

                                                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers['active_request']; ?></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>  
                                <?php } ?>                          
                            </ul>
                            <input type="hidden" name="request_id" id="request_id">
                            <p class="space-c"></p>
                            <p class="btn-x text-center">
                                <button name="submit" type="submit" id="assign_desig" class="load_more button" data-dismiss="modal" aria-label="Close">Assign Designer</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal similar-prop fade" id="editExpected" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Edit Expected date</h2>
                <div class="close" data-dismiss="modal" aria-label="Close"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/edit-date.png" class="img-responsive center-block">
                    <form action="<?php echo base_url(); ?>admin/dashboard/change_expectedDate" method="post">
                        <label>Expected Date</label>
                        <input type="text" name="edit_expected" class="edit_expected"  value="" readonly/>
                        <input type="hidden" name="edit_reqid" class="edit_reqid"  value=""/>
                        <p class="space-c"></p>
                        <p class="btn-x text-center">
                            <button name="submit" type="submit" class="load_more button">Save</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="results"></div>

<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script type="text/javascript">
    var BUCKET_TYPE = '<?php echo isset($bucket_type) ? $bucket_type : ''; ?>';
    var ASSIGN_PROJECT = '<?php echo isset($_GET['assign']) ? $_GET['assign'] : ""; ?>';
    var timezone = '<?php echo isset($login_user_data[0]['timezone']) ? $login_user_data[0]['timezone'] : $_SESSION['timezone']; ?>';
    var $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
     var comefrm = '<?php echo isset($comefrm) ? $comefrm : ''; ?>';
//    $.urlParam = function(name){
//        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
//        if (results==null){
//           return null;
//       }
//       else{
//           return decodeURI(results[1]) || 0;
//       }
//   }

//   $(document).on('click', ".adddesinger", function(){
//    var request_id = $(this).attr('data-requestid');
//    var designer_id = $(this).attr('data-designerid');
//    $('#AddDesign #request_id').val(request_id);
//    $('#AddDesign input#'+designer_id).click();
//});

//   $('#assign_desig').click(function(e){
//    e.preventDefault();
//    var designer_id = $('input[name=assign_designer]:checked').val();
//    var request_id = $('#request_id').val();
//    var designer_name = $('input[name=assign_designer]:checked').attr('data-name');
//    var designer_pic = $('input[name=assign_designer]:checked').attr('data-image-pic');
//    $.ajax({
//        url:"<?php //echo base_url();   ?>/admin/Dashboard/assign_designer_ajax",
//        type:'POST',
//        data:{
//            'assign_designer':designer_id,
//            'request_id':request_id
//        },
//        success: function(data){
//            var returnedData = JSON.parse(data);
//            if(returnedData.status == 'success'){
//                    //$('#close-d').click();
//                    $("."+request_id+" p.text-h").html(designer_name);
//                    $("."+request_id+" a.adddesinger").attr('data-designerid',designer_id);
//                    if(designer_pic == ""){
//                      $("."+request_id+" img").attr('src','<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png');  
//                  }else{
//                    $("."+request_id+" img").attr('src','<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>'+designer_pic);
//                }
//            }else{
//                $('#show_error').html(returnedData.msg);
//                $('.alert-dismissable.error').css('display','block');
//            }
//        }
//    });
//
//});

//   function load_more_admin(is_loaded = '', currentstatus = '',tabActive = '',search = ''){
//    if(is_loaded != ''){
//        var dataloaded = is_loaded.attr('data-isloaded');
//    }else{
//        dataloaded = '';
//    }
//    var bucket_type = '<?php echo $bucket_type; ?>';
//    var status_Active = $('#status_check li.active a').attr('data-status');
//    $("input:hidden.bucket_type").val(bucket_type);
//    var activetab = $('#status_check li.active a').attr('href');
//    var tabid;
//    if(tabActive != ''){
//        tabid = tabActive;
//    }else{
//        tabid = activetab;
//    }
//    var activeTabPane = $('.tab-pane.active');
//    var status_scroll   = (currentstatus != '') ? currentstatus : status_Active ;
//    var searchval = $('.search_text').val();
//    if(dataloaded != '1' || search == '2'){
//        $.post('<?php echo site_url() ?>admin/dashboard/load_more', {'group_no': 0, 'status': status_scroll,'search':searchval,'bucket_type':bucket_type},
//            function (data){
//                var newTotalCount = 0;
//                if(data != ''){
//                    if(is_loaded != ''){
//                     is_loaded.attr('data-isloaded','1');
//                 }
//                 $('.ajax_loader').css('display','none');
//                 $(".ajax_searchload").fadeOut(500);
//                 $(tabid+ " .product-list-show .row").fadeOut(200, function() {
//                     $(tabid+ " .product-list-show .row").html(data);
//                     $(tabid+ " .product-list-show .row").fadeIn(800);
//                 });
//                 newTotalCount = $(tabid+" .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
//                 console.log('newTotalCount',newTotalCount);
//                 console.log('$rowperpage',$rowperpage);
//                 $(tabid+" .product-list-show .row").find("#loadingAjaxCount").remove();
//                 activeTabPane.attr("data-total-count",newTotalCount);
//                 activeTabPane.attr("data-loaded",$rowperpage);
//                 activeTabPane.attr("data-group",1);
//             } else {
//                activeTabPane.attr("data-total-count",0);
//                activeTabPane.attr("data-loaded",0);
//                activeTabPane.attr("data-group",1);
//                $(tabid+ ".product-list-show .row").html("");
//            }
//            if ($rowperpage >= newTotalCount) {
//                activeTabPane.find('.load_more').css("display", "none");
//            } else{
//                delay(function(){
//                    $('.load_more').css('display','inline-block');
//                    activeTabPane.find('.load_more').css("display", "inline-block");
//                }, 1000 );
//            }
//        });
//    }else{
//     $('.ajax_loader').css('display','none');
//     $('.load_more').css('display','flex');
// }
//}

    /*********Search *******************/
//$('.search_data_ajax').click(function (e) {
//    //console.log($rowperpage);
//    e.preventDefault();
//    load_more_admin('','','','2');
//});
//$('.searchdata').keyup(function (e) {
//    e.preventDefault();
//    delay(function(){
//     $(".ajax_searchload").fadeIn(500);
//     load_more_admin('','','','2'); 
// }, 1000 );
//});
//var delay = (function(){
//  var timer = 0;
//  return function(callback, ms){
//    clearTimeout (timer);
//    timer = setTimeout(callback, ms);
//};
//})();

    /**************start on load & tab change get all records with load more button*****************/
//$(document).ready(function () {
//    countTimer();
//    var status = $('#status_check li.active a').attr('data-status');
//    var id = $('#status_check li.active a').attr('href');
//    $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
//    $('.tab-pane').attr("data-loaded", $rowperpage);
//    $(document).on("click", "#load_more", function () {
//     countTimer();
//     var bucket_type = '<?php echo $bucket_type; ?>';
//     $(".ajax_loader").css("display","flex");
//     $(".load_more").css("display","none");
//     var row = parseInt($(this).attr('data-row'));
//            ///var allcount = parseInt($(this).attr('data-count'));
//            row = row + $rowperpage;
//            var activeTabPane = $('.tab-pane.active');
//            var searchval = activeTabPane.find('.search_text').val();
//            var allcount = parseInt(activeTabPane.attr("data-total-count"));
//            var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
//            var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
//            var status_scroll = $('#status_check li.active a').attr('data-status');
//            var tabid = $('#status_check li.active a').attr('href');
//            if (allLoaded < allcount) {
//                $.post('<?php echo site_url() ?>admin/dashboard/load_more', {'group_no': activeTabPaneGroupNumber, 'status': status_scroll,'search':searchval,'bucket_type':bucket_type},
//                    function (data) {
//                        if (data != "") {
//                            $(tabid+" .product-list-show .row").append(data);
//                            row = row + $rowperpage;
//                            $(".ajax_loader").css("display","none");
//                            $(".load_more").css("display","inline-block");
//                            activeTabPane.find('.load_more').attr('data-row', row);
//                            activeTabPaneGroupNumber++;
//                            activeTabPane.attr('data-group', activeTabPaneGroupNumber);
//                            allLoaded = allLoaded + $rowperpage;
//                            activeTabPane.attr('data-loaded', allLoaded);
//                            if (allLoaded >= allcount) {
//                                activeTabPane.find('.load_more').css("display", "none");
//                                $(".ajax_loader").css("display","none");
//                            }else{
//                                activeTabPane.find('.load_more').css("display", "inline-block");
//                                $(".ajax_loader").css("display","none");
//                            }
//                        }
//                    });
//            }
//        });
//});
    /**************End on load & tab change get all records with load more button*****************/


//$('.checkcorrect').click(function(){});

//function check(val, attr){
//        // var status = $(this).val();
//        var status = val;
//            // var request_id = $(this).attr('data-request');
//            var request_id = attr;
//            $.ajax({
//                url:"<?php echo base_url(); ?>/admin/dashboard/designer_skills_matched",
//                type:'POST',
//                data:{
//                    'designer_skills_matched':status,
//                    'request_id': request_id
//                },
//                success: function(data){
//                    if(data == 1){
//                        setTimeout(function(){ 
//                            $('#'+request_id).hide();
//                        }, 1000);
//                    }
//                }
//            });
//        }


//$(".checkAll").change(function(){
//  var attribute = $(this).attr('data-attr');
////  console.log(attribute);
//if($(this).is(':checked')) {
//$('.' + attribute).prop( "checked", true );
//}else{
//$('.' + attribute).prop( "checked", false );
//}
//var requestIDs = [];
//$.each($("input[name='project']:checked"), function(){  
//if($(this).val()!= 'on'){
//requestIDs.push($(this).val());
//}
//}); 
//$('.adddesinger').attr('data-requestid',requestIDs);
////console.log(requestIDs);
//});

//$(document).on('change','.selected_pro',function(){
//    var requestIDs = [];
//    $.each($("input[name='project']:checked"), function(){            
//        if($(this).val()!= 'on'){
//            requestIDs.push($(this).val());
//        }
//    });
//    $('.adddesinger').attr('data-requestid',requestIDs);
//}); 




//        /***************Edit Expected****************/
//        $(document).on('click', '.editexpected', function () {
//            var data_expected = $(this).attr('date-expected');
//            var data_reqid = $(this).attr('date-reqid');
//   // console.log(data_expected);
//   $('#editExpected').modal();
//   $('input[name=edit_expected]').val(data_expected);
//   $('input[name=edit_reqid]').val(data_reqid);
//});
//
//        $(document).on('click', '.edit_expected', function () {
//            var date = jQuery(this).datetimepicker();
//        });


//        $(document).on('change', '.selected_pro', function () {
//            var checked = $("input[type='checkbox']:checked").length;
//            if (checked >= 1 && $(window).scrollTop() > '370') {
//                $('.adddesinger').addClass('relate_check');
//            } else {
//                $('.adddesinger').removeClass('relate_check');
//            }
//        });



//        function countTimer() {
//            $('.expectd_date').each(function () {
//                var expected_date = $(this).attr('data-date');
//                var user_datetimezone = '<?php echo $_SESSION['timezone'] ?>';
//                var data_id = $(this).attr('data-id');
//        // Set the date we're counting down to
//        var countDownDate = new Date(expected_date).getTime();
//        
//        // Update the count down every 1 second
//        var x = setInterval(function () {
//            // Get todays date and time
//            var nowdate = new Date().toLocaleString('en-US', { timeZone: user_datetimezone });
//
//            if(user_datetimezone != '' || user_datetimezone != null){
//             var now = new Date(nowdate).getTime();
//         }else{
//           var now = new Date().getTime();
//       }
//
//            // Find the distance between now and the count down date
//            var distance = countDownDate - now;
//            
//            // Time calculations for days, hours, minutes and seconds
//            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
//            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
//            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
//            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
//            if(minutes > 0){
//                var timer = ("0" + days).slice(-2) + " : " +("0" + hours).slice(-2) + " : "
//                + ("0" + minutes).slice(-2) + " : " + ("0" + seconds).slice(-2) + " ";
//                $('#demo_'+data_id).text(timer);
//            }else{
//               $('#demo_'+data_id).text("EXPIRED");
//           }
//       }, 1000);
//    });
//        }
//$(document).on('change','.is_verified input[type="checkbox"]',function(e){
//    var data_id = $(this).attr('data-pid');
//    var ischecked = ($(this).is(':checked')) ? 1 : 0;
//    if(ischecked == 1){
//        $('#verified_'+data_id).addClass('verified_by_admin'); 
//    }else{
//     $('#verified_'+data_id).removeClass('verified_by_admin'); 
// }
// $.ajax({
//    type: 'POST',
//    url: "<?php echo base_url(); ?>admin/dashboard/request_verified_by_admin",
//    data: {data_id: data_id,ischecked: ischecked},
//    success: function (data) {
//    }
//});
//});

    /********load more using ajax**********/
//$('#status_check li a').click(function (e) {
//    $('.ajax_loader').css('display','block');
//    $('.load_more').css('display','none');
//    var currentstatus = $(this).data('status');
//    var bucket_type = "<?php echo $bucket_type; ?>";
//    var status = "";
//    if(currentstatus == "active,disapprove"){
//    status = "active-disapprove";
//}else{
//    status = currentstatus;
//}
//    var url = "<?php echo base_url(); ?>admin/Dashboard/downloadrequest_accstatus/"+status+"/"+bucket_type;
//    $('#download_rquests').attr('href', url);
//    var tab = $(this).attr('href');
//    var is_loaded = $(this).data('isloaded');
////console.log(is_loaded);
//e.preventDefault();
//load_more_admin($(this),currentstatus,tab);
//});

</script>
