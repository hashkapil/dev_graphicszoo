<?php 
if($login_user_data[0]["role"] == "manager" && $assign_designer_toproject != 1){
    $c_role = "m_user";
}else{
    $c_role = $u_role;
} ?>
<div class="se-pre-con" style="display: none;"></div>
<section class="con-b">
    <div class="custom_container">
        <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php }
            ?>
        </div>
        <input type="hidden" value="<?php echo LIMIT_ALL_LIST_COUNT ?>" id="LoadMoreLimter">
<!--        <div class="header-blog">
            <a href="javascript:void(0)" data-view='list' data-satus="" class="view_class active" id="list_view" style="display:none"></a>
            <div class="row flex-show">
                <div class="col-md-12">
                    <div class="flex-this">
                        <h2 class="main_page_heading">Projects</h2>
                        <div class="header_searchbtn">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-9">
                    <ul  id="status_check" class="list-unstyled list-header-blog" role="tablist" data-u_role="<?php echo $u_role; ?>" data-inqueue="<?php $agency_info[0]["show_inq_to_designer"]; ?>" data-vrfy_prmsn="<?php echo $agency_info[0]["req_vrfy_by_mngr"]; ?>" style="border:none;">
                        <li class="active" id="1">
                            <a data-status="active,disapprove" data-isloaded="0" data-view="grid" data-count ="<?php echo isset($count_active_project) ? $count_active_project : 0; ?>" data-toggle="tab" href="#designs_request_tab" role="tab" data-ajax-load="0">
                                Active (<?php echo isset($count_active_project) ? $count_active_project : 0; ?>)
                            </a>
                        </li>
                        <?php if($agency_info[0]["show_inq_to_designer"] != 0){ ?>
                        <li id="2">
                                <a data-toggle="tab" href="#in_queue_tab" data-isloaded="0" data-view="grid" data-status="assign" role="tab" data-s_class ="in_queue" data-tabid="2" data-pagin ="<?php echo $count_project_i; ?>">
                                    In Queue <span>(<?php echo isset($count_project_i)?$count_project_i:0; ?>)</span>
                                </a>
                        </li> 
                        <?php } ?>
                        <li class="" id="3">
                            <a data-status="pendingrevision" data-isloaded="0" data-view="grid" data-count="<?php echo isset($count_project_p) ? $count_project_p : 0; ?>" data-toggle="tab" href="#pending_review_tab" role="tab" data-ajax-load="0"> 
                                Pending Review (<?php echo isset($count_project_p) ? $count_project_p : 0; ?>)
                            </a>
                        </li>
                        <li class="" id="4">
                            <a data-status="checkforapprove"  data-isloaded="0" data-view="grid" data-count="<?php echo isset($count_project_checkforap) ? $count_project_checkforap : 0; ?>" data-toggle="tab" href="#pendingapproval_designs_tab" role="tab" data-ajax-load="0">
                                Pending Approval (<?php echo isset($count_project_checkforap) ? $count_project_checkforap : 0; ?>)
                            </a>
                        </li>
                        <li class="" id="5">
                            <a data-status="approved" data-isloaded="0" data-view="grid" data-toggle="tab" href="#approved_designs_tab" data-count="<?php echo isset($complete_project_count) ? $complete_project_count : 0; ?>" role="tab" data-ajax-load="0">
                                Completed (<?php echo isset($complete_project_count) ? $complete_project_count : 0; ?>)
                            </a>
                        </li>
                        <li class="" id="6">
                            <a data-status="hold" data-isloaded="0" data-view="grid" data-count= "<?php echo isset($hold_project_count) ? $hold_project_count : 0; ?>" data-toggle="tab" href="#hold_designs_tab" role="tab" data-ajax-load="0">
                                On Hold (<?php echo isset($hold_project_count) ? $hold_project_count : 0; ?>)
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                     <div class="search-first">
                                <div class="focusout-search-box">
                                    <div class="search-box">
                                        <form method="post" class="search-group clearfix">
                                            <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                            <div class="ajax_searchload" style="display:none;">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                            </div>
                                            <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                            <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                            <button type="submit" class="search-btn search search_data_ajax">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                            </button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
        <div class="pro-deshboard-list">
            <!-- main div start from here  -->
            <div class="tab-content s_projct_lst" data-step="2" data-intro="View and manage all of your projects from the project dashboard. You can delete, duplicate or reprioritize projects" data-position='right' data-scrollTo='tooltip'>
                <div class="loader_tab" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_active_project; ?>" class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                    <div class="product-list-show">
                        <div class="row">
                            <?php
                            $activeTab = 0;
                            foreach ($active_project as $project) {
                                if ($project['status'] == "assign") {
                                    $activeTab++;
                                    $data['activeTab'] = $activeTab;
                                }
                            }
//                            if ($count_active_project != 0) {
                                ?>	
                                <div id="prior_data">
                                    <div class="col-md-12">
                                        <div class="list-view-table">
                                            <span id="loadingAjaxCount" data-value="<?php echo $count_active_project; ?>"></span>
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>PROJECT NAME</th>
                                                        <th>CLIENT</th>
                                                        <th>PROJECT STATUS</th>
                                                        <th>PROJECT TIME</th>
                                                         <?php if($u_role != "customer" && $agency_info[0]["req_vrfy_by_mngr"] == 1){ ?>
                                                        <th>VERIFIED</th>
                                                         <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                    <?php
                                                    for ($i = 0; $i < count($active_project); $i++) {
                                                        if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "checkforapprove" || $active_project[$i]['status'] == "disapprove") {
                                                            $data['projects'] = $active_project[$i];
                                                            $data['user_data'] = $active_project[$i]['user_data'];
                                                            // echo $active_project[$i]['view_type'] ;exit;
                                                            $this->load->view('account/projects_list_view', $data);
                                                        }
                                                        if ($active_project[$i]['status'] != "active" && $active_project[$i]['status'] != "checkforapprove" && $active_project[$i]['status'] != "disapprove") {
                                                            $data['projects'] = $active_project[$i];
                                                            $data['user_data'] = $active_project[$i]['user_data'];
                                                            $this->load->view('account/projects_list_view', $data);
                                                        }
                                                    }
                                                    ?>
                                                </tbody>   
                                            </table>
                                        </div>	
                                    </div>
                                    <?php if ($count_active_project > LIMIT_ALL_LIST_COUNT) { ?>
                                        <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                                        <div class="" style="text-align:center">
                                            <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more  theme-save-btn button">Load more</a>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php //} ?>
                        </div>
                    </div>
                </div>
                
                <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $count_project_i; ?>" class="tab-pane content-datatable datatable-width" id="in_queue_tab" role="tabpanel">
                    <!-- Drafts -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->	

                        </div>

                        <?php if ($count_project_i > LIMIT_ALL_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_i; ?>" class="load_more theme-save-btn">Load more</a>
                            </div> 
                        <?php } ?>
                    </div>

                    <p class="space-e"></p>
                </div>

                <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $count_project_p; ?>" class="tab-pane content-datatable datatable-width" id="pending_review_tab" role="tabpanel">
                    <!-- Drafts -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->	

                        </div>

                        <?php if ($count_project_p > LIMIT_ALL_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_p; ?>" class="load_more theme-save-btn">Load more</a>
                            </div> 
                        <?php } ?>
                    </div>

                    <p class="space-e"></p>
                </div>
                <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $count_project_checkforap; ?>" class="tab-pane content-datatable datatable-width" id="pendingapproval_designs_tab" role="tabpanel">

                    <!-- Complete Projects -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->

                        </div>
                        <?php if ($count_project_checkforap > LIMIT_ALL_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row = "0" data-count="<?php echo $count_project_checkforap; ?>" class="load_more theme-save-btn">Load more</a>
                            </div> 
                        <?php } ?>
                    </div>
                    <p class="space-e"></p>
                </div>
                <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $complete_project_count; ?>" class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">

                    <!-- Complete Projects -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->

                        </div>
                        <?php if ($complete_project_count > LIMIT_ALL_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row = "0" data-count="<?php echo $complete_project_count; ?>" class="load_more theme-save-btn">Load more</a>
                            </div> 
                        <?php } ?>
                    </div>
                    <p class="space-e"></p>
                </div>
                <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $hold_project_count; ?>" class="tab-pane content-datatable datatable-width" id="hold_designs_tab" role="tabpanel">
                    <!-- Complete Projects -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->

                        </div>
                        <?php if ($hold_project_count > LIMIT_ALL_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/gz_customer_loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row = "0" data-count="<?php echo $hold_project_count; ?>" class="load_more theme-save-btn">Load more</a>
                            </div> 
                        <?php } ?>
                    </div>
                    <p class="space-e"></p>
                </div>
            </div>
            <!-- main div closed here  -->
        </div>
    </div>
</section>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/chosen.jquery.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/init.js"></script>

<script>
var $brand_id = "<?php echo isset($_GET['brand_id']) ? $_GET['brand_id'] : ''?>";
var $client_id = "<?php echo isset($_GET['client_id']) ? $_GET['client_id'] : '' ?>";
</script>