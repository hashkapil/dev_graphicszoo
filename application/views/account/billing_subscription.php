<style>
.pad{
	padding:5px;
}
.colorblack{
	color:#000;
	padding-bottom:0px;
	font-size:15px;
}
.nav-tab-pills-image ul li .nav-link:hover{
	border-bottom:unset !important;

}
.wrapper .nav-tab-pills-image ul li .nav-link:hover{
	color: #fff !important; 
}
.wrapper .nav-tab-pills-image ul li.active .nav-link{
	color: #fff !important; 
	    background: #1a3147 !important;
}

.nav-tab-pills-image ul .nav-item+.nav-item{
	margin-left:9px;
}
.nav-tab-pills-image ul li .nav-link:focus
{
	border-bottom:unset !important;
	background: #2f4458 !important;
	color:#fff !important;
}
.nav-tab-pills-image ul li .nav-link .active :hover{
	color:unset !important;
}
.nav-tab-pills-image ul li .nav-link .active{
	color:#fff !important;
}
#content-wrapper .table-responsive .form-control.input-sm {
	border-radius: 5px !important;
	margin: 0 15px !important;
}
.content .nav-tab-pills-image .nav-item.active a{
	/*border-bottom: 3px solid #ec1c41 !important;*/
	color: #fff;
    background: #2f4458;
}
.pricing-main{  }
.pricing-package-main{ background-image: none; margin-bottom: 30px; padding: 0px 0px 60px 0px; position: static; }
/*.pricing-package-main:before{ position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; content: ""; background: rgba(255, 255, 255, 0.6); }*/

.pricingpackage-box{ text-align: center; position: relative; }
.pricingpackage-box .offer{ min-height: 57px; }
.pricingpackage-box .offer p{ background: #e52344; font-weight: bold; color: #fff; padding: 15px 5px !important; min-height: 50px; font-size: 20px; }

.pricingpackage{ background: #fff; box-shadow: 0 3px 4px 4px rgba(0, 0, 0, 0.1); padding: 40px; }
.pricingpackage h3{ font-size: 19px; text-transform: uppercase; color: #1a3147; font-weight: normal; padding: 0px 0px 10px; }
.pricingpackage .price{ font-size: 70px; color: #1a3147; font-weight: bold; line-height: normal; }
.pricingpackage .price span{ font-size: 50px; }
.pricingpackage small{ font-size: 14px; color: #999999; width: 100%; display: block; padding-bottom: 28px; }
.pricingpackage h4{ font-size: 18px; text-transform: uppercase; color: #e52344; font-weight: bold; padding-bottom: 20px; }
.pricingpackage ul{  }
.pricingpackage li{ padding: 0px 0px; list-style: none; font-size: 16px; color: #999999; line-height: 35px; }

.pricingpackage ul .bold{  }
.pricingpackage ul li .bold{ font-weight: bold; }

.pricing-questions-main{ position: relative; }
.pricing-questions-main:before{ position: absolute; left: 0px; right: 0px; background: #47b5f3; content: ""; min-height: 320px; z-index: -1; }

.pricing-questions-main .innermain-banner{ padding: 60px 0px 40px 0px; }

.pricingquestions-data{ padding-bottom: 60px; }
.pricingquestions-databox{ background: #fff; box-shadow: 0 3px 5px 3px rgba(0, 0, 0, 0.1); padding: 40px; margin-bottom: 22px; min-height: 250px; }
.pricingquestions-databox h4{ font-size: 25px; color: #1a3147; font-weight: bold; padding-bottom: 12px; }

.btn1 { background-color: #e52344; border: none; padding: 12px 22px; font-size: 14px; font-weight: bold; color: #fff; text-decoration: none; border-radius: 30px; text-transform: uppercase; }
.btn1:hover, .btn1:focus, .btn1.active, .btn1:active{ background-color: #000 !important; color: #fff !important; text-decoration: none; box-shadow: none; }

.nav-tab-pills-image ul li .nav-link {
    color: #1a3147 !important;
}
</style>

<div class="col-md-10 offset-md-1">
    <!--div class="setting_tabs_link">
        <a href="<?php echo base_url(); ?>customer/ChangeProfile/" class="float-xs-left content-title-main" style="display:inline-block;padding-left:33px; color: #9da0a3;">
            Edit Profile
        </a>
        <a href="#" class="float-xs-left content-title-main active" style="color: #1a3147; padding-left: 5%;">
            Billing and Subscription
        </a>
    </div><!-- setting tabs link -->
</div><!-- col -->

</div>


<div class="col-md-12" style="background:white;">
    <div class="col-md-12">

        <section id="content-wrapper">

            <div class="content">

                <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:30px;">

                <?php if($this->session->flashdata('message_error') != '') {?>

                   <div class="alert alert-danger alert-dismissable">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>

                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>

                    </div>

                   <?php }?>

                   <?php if($this->session->flashdata('message_success') != '') {?>

                   <div class="alert alert-success alert-dismissable">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>

                        <strong><?php echo $this->session->flashdata('message_success');?></strong>

                    </div>

                   <?php }?>
                   <?php
                    $myplan = "";
                    for($i=0;$i<sizeof($planlist);$i++){
                        if($planlist[$i]['id'] == $current_plan){
                            $myplan = $planlist[$i]['name'];
                        }
                    }   ?>
                    

<!-- ***************** new design ******************-->

<div class="row">

<div class="col-md-8 col-md-offset-2">
    <div class="col-md-12">
<div class="clearfix billing_subs_right">
<!--    <div class="title_box">
<h2 class="head-d text-left">Subscription <span class="update_subs">
        <p class="btn-x"> <input  id="savebtn" type="button" name="savebtn" value="Update Subscription" class="form-control btn-e"/></p></span></h2>
</div>-->
    <h2 class="head-d text-left" style="margin-top:40px;">Billing Method</h2>
<div class="clearfix billing_subs_left">
<form method="post" action="<?php echo base_url(); ?>customer/ChangeProfile/change_plan">
  <div class="payment-method">

      <div class="pmformmain">
        <form class="row">
            <div class="card-js">
                <div class="form-group col-md-12">
                  <input type="text" name="card-number" class="card-number form-control" placeholder=""></div>

                            <div class="form-group col-md-7">
                                <label class="inplabel">Expiry Date</label>
                                <input class="expiry-month" name="expiry-month">
                                <input class="expiry-year" name="expiry-year">
                            </div>

                            <div class="form-group col-md-5">
                                <label class="inplabel">CVC</label>
                                <input type="text" name="cvc" class="cvc form-control" placeholder="">
                            </div>

                        </div>
                        <div class="form-group change_card_details btn-x" style="margin-top: 10px;">
                            <input type="hidden" name="current_plan" value="<?php echo $current_plan; ?>" />
                            <input type="submit" value="Edit Billing Info" class="btn btn-e site-btn" name="change_my_card_deatils"/>
                        </div>
                    </form>
                </div>
            </div>
        </form>
</div>
</div>
        </div>
 <!-- Card Details End-->
</div><!-- billing_subs_left -->
<div class="pricing-package-main">
                    <div class="row">
                       
                        <div class="col-md-8  col-md-offset-2">
                        <div class="col-sm-6 col-md-6 col-lg-6 ">
                            <div class="pricingpackage-box">
                                <div class="offer"><?php if($current_plan=="M3501D"){echo '<p>Current Plan</p>'; } ?></div>
                                <div class="pricingpackage myplan" style="<?php if($current_plan!="M3501D"){echo 'cursor:pointer;'; } ?>" data-id="M3501D">
                                    <h3>MONTHLY GOLDEN PLAN</h3>
                                    <div class="price"><span>$</span>299</div>
                                    <small>monthly</small>
                                    <h4>THIS INCLUDES</h4>
                                    <ul class="bold" style="padding: 0px;">
                                        <li>1-Day Turnaround</li>
                                        <li>14 Day Risk Free Guarantee</li>
                                        <li>No Contract</li>
                                        <li>Unlimited Graphic Designs</li>
                                    </ul>
                                    <ul style="padding: 0px;padding-top: 20px;">
                                        <li>Unlimited Revisions</li>
                                        <li>Quality Designers</li>
                                        <li>Dedicated Support</li>
                                        <li>Unlimited free stock images from Pixabay and Unsplash</li>
                                    </ul>
                                    <div class="btn-x"> <input type="submit" value="Choose Plan" class="btn btn-e site-btn" name="change_my_card_deatils" disabled=""/></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="pricingpackage-box">
                                <div class="offer"><?php if($current_plan=="A35881D"){echo '<p>Current Plan</p>'; } ?></div>
                                <div class="pricingpackage myplan" style="<?php if($current_plan!="A35881D"){echo 'cursor:pointer;'; } ?>" data-id="A35881D">
                                    <h3>Annual Golden Plan</h3>
                                    <div class="price"><span>$</span>2870</div>
                                    <small>yearly</small>
                                    <h4>THIS INCLUDES</h4>
                                    <ul class="bold" style="padding: 0px;">
                                        <li>1-Day Turnaround</li>
                                        <li>14 Day Risk Free Guarantee</li>
                                        <li>No Contract</li>
                                        <li>Unlimited Graphic Designs</li>
                                    </ul>
                                    <ul style="padding: 0px;padding-top: 20px;">
                                        <li>Unlimited Revisions</li>
                                        <li>Quality Designers</li>
                                        <li>Dedicated Support</li>
                                        <li>Unlimited free stock images from Pixabay and Unsplash</li>
                                    </ul>
                                    <div class="btn-x"> <input type="submit" value="Choose Plan" class="btn btn-e site-btn" name="change_my_card_deatils" disabled="" /></div>
                                </div>
                            </div>
                        </div>

                        
                    </div>
</div><!-- pricing-package-main -->
</div>
<!--Form Star-->
<form action="" method="post" style="display:none;">
<h3 style="text-align:center;color:#1a3147;font-size:25px;font-weight:600;">Update Plan</h3>
<div class="pad">
    <p class="colorblack">Select Plan</p>
    <!--<select name="plan_name" class="form-control" class="plan_select">
        <?php for($i=0;$i<sizeof($planlist);$i++){ ?>
        <option value="<?php echo $planlist[$i]['id'] ?>" <?php if($planlist[$i]['id'] == $current_plan){ echo "selected"; } ?>><?php echo $planlist[$i]['name'] ?></option>
        <?php } ?>
    </select>-->
    <input type="textbox" name="plan_name" class="plan_select" />
    <input style="background-color:#ec1c41;color:#fff;margin-top:10px;" type="submit" name="savebtn" value="Update Plan" class="form-control formsubmit"/>
</div>
</form>
<!--Form End-->
</div>
</div>
</div>
    <script>
    $(".myplan").click(function(){
        var value=$(this).attr("data-id");
        if($(this).css("cursor")=="pointer"){
            $(".plan_select").val(value);
            $(this).css("border","2px solid #e52344");
            $("#savebtn").css("cursor","pointer");
        }else{
        }

    });

    $("#savebtn").click(function(){
        if($(".plan_select").val()){
            $(".formsubmit").click();
        }
    });
    </script>

        </div><!-- billing_subs_right -->
    </div><!-- col -->
</div><!-- row -->


<!-- ***************** new design ******************-->



                    </div>
                </div>
            </div>
        </section>

    </div>
</div>




<?php /* ?>
<div class="col-md-12" style="background:white;">
    <div class="col-md-10 offset-md-1">

        <section id="content-wrapper">

    		<div class="content">

                <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:30px;">

    		    <?php if($this->session->flashdata('message_error') != '') {?>

    			   <div class="alert alert-danger alert-dismissable">

    					<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>

    					<strong><?php echo $this->session->flashdata('message_error'); ?></strong>

    				</div>

    			   <?php }?>

    			   <?php if($this->session->flashdata('message_success') != '') {?>

    			   <div class="alert alert-success alert-dismissable">

    					<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>

    					<strong><?php echo $this->session->flashdata('message_success');?></strong>

    				</div>

    			   <?php }?>
    			<!-- Tab Content Start -->
    			   <div class="nav-tab-pills-image">
    					<ul class="nav nav-tabs"  style="border:none;">
    						<li class="nav-item active" style="background:#ccc; margin-left:6px;border-radius: 8px 0px 0px 8px; width: 200px;">
    							<a class="nav-link" style="background:#ccc;font-weight: 600; border-radius: 8px 0px 0px 8px" data-toggle="tab" role="tab" href="#myplandetails"  style="">
    								Your Plan
    							</a>
    						</li>
    						<li class="nav-item " style="margin-left:2px; width: 200px;">
    							<a class="nav-link clickchangeplan" style="background:#ccc; font-weight: 600; border-radius: 0px"  data-toggle="tab" role="tab" href="#card_details_Section">
    								Change Card Details
    							</a>
    						</li>
    						<li class="nav-item" style="margin-left:2px; border-radius: 0px 8px 8px 0px; width: 200px;">
    							<a class="nav-link" style="background:#ccc; font-weight: 600; border-radius: 0px 8px 8px 0px;" data-toggle="tab" role="tab" href="#invoices_section">
    								Invoices
    							</a>
    						</li>
    					</ul>
    				</div>

    				<?php
    				$myplan = "";
    				for($i=0;$i<sizeof($planlist);$i++){
    					if($planlist[$i]['id'] == $current_plan){
    						$myplan = $planlist[$i]['name'];
    					}
    				}	?>
    				<div class="tab-content">
                            <div class="tab-pane active content-datatable datatable-width" id="myplandetails" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">

    								<!--<table style="margin-top: 40px;margin-left: 20px;margin-bottom: 20px;">
    									<tr>
    										<td style="font-size: 15px;color: #1a3147;">Your Plan: <?php echo $myplan; ?></td>
    										<td style="font-size: 15px;color: #1a3147;">Card: <?php echo $carddetails['last4']; ?>(Last 4 Digit)-<?php echo $carddetails['exp_month'] ?>/<?php echo $carddetails['exp_year'] ?>&nbsp;&nbsp;&nbsp;<span style="font-size: 12px;color: #0190ff;cursor: pointer;" onclick="$('.clickchangeplan').click();">Edit</span></td>
    									</tr>
    								</table>-->


    								<div class="pricing-package-main" style="padding-top: 50px;">

										<div class="row">

											<div class="col-sm-6 col-md-6 col-lg-6 ">
												<div class="pricingpackage-box">
													<div class="offer"><?php if($current_plan=="M3501D"){echo '<p>Current Plan</p>'; } ?></div>
													<div class="pricingpackage myplan" style="<?php if($current_plan!="M3501D"){echo 'cursor:pointer;'; } ?>" data-id="M3501D">
														<h3>MONTHLY GOLDEN PLAN</h3>
														<div class="price"><span>$</span>299</div>
														<small>monthly</small>
														<h4>THIS INCLUDES</h4>
														<ul class="bold" style="padding: 0px;">
															<li>1-Day Turnaround</li>
															<li>14 Day Risk Free Guarantee</li>
															<li>No Contract</li>
															<li>Unlimited Graphic Designs</li>
														</ul>
														<ul style="padding: 0px;padding-top: 20px;">
															<li>Unlimited Revisions</li>
															<li>Quality Designers</li>
															<li>Dedicated Support</li>
															<li>Unlimited free stock images from Pixabay and Unsplash</li>
														</ul>
													</div>
												</div>
											</div>


											<div class="col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 30px;">
												<div class="pricingpackage-box">
													<div class="offer"><?php if($current_plan=="A35881D"){echo '<p>Current Plan</p>'; } ?></div>
													<div class="pricingpackage myplan" style="<?php if($current_plan!="A35881D"){echo 'cursor:pointer;'; } ?>" data-id="A35881D">
														<h3>Annual Golden Plan</h3>
														<div class="price"><span>$</span>2870</div>
														<small>yearly</small>
														<h4>THIS INCLUDES</h4>
														<ul class="bold" style="padding: 0px;">
															<li>1-Day Turnaround</li>
															<li>14 Day Risk Free Guarantee</li>
															<li>No Contract</li>
															<li>Unlimited Graphic Designs</li>
														</ul>
														<ul style="padding: 0px;padding-top: 20px;">
															<li>Unlimited Revisions</li>
															<li>Quality Designers</li>
															<li>Dedicated Support</li>
															<li>Unlimited free stock images from Pixabay and Unsplash</li>
														</ul>
													</div>
												</div>
											</div>

							                <input  id="savebtn"style="background-color:#ec1c41; color:#fff; margin-top:10px; width: 96%;margin: auto;margin-top: 20px; padding: 20px 15px; text-transform: uppercase; font-weight: bold; font-size: 22px; letter-spacing: 1px;" type="button" name="savebtn" value="Upgrade" class="form-control"/>
										</div>



    								</div>


                                    <!--Form Star-->
    								<form action="" method="post" style="display:none;">
    									<h3 style="text-align:center;color:#1a3147;font-size:25px;font-weight:600;">Update Plan</h3>
    									<div class="pad">
    										<p class="colorblack">Select Plan</p>
    										<!--<select name="plan_name" class="form-control" class="plan_select">
    											<?php for($i=0;$i<sizeof($planlist);$i++){ ?>
    											<option value="<?php echo $planlist[$i]['id'] ?>" <?php if($planlist[$i]['id'] == $current_plan){ echo "selected"; } ?>><?php echo $planlist[$i]['name'] ?></option>
    											<?php } ?>
    										</select>-->
    										<input type="textbox" name="plan_name" class="plan_select" />
    										<input style="background-color:#ec1c41;color:#fff;margin-top:10px;" type="submit" name="savebtn" value="Update Plan" class="form-control formsubmit"/>
    									</div>
    								</form>
    								<!--Form End-->
    								</div>
    							</div>
    						</div>
    <script>
    $(".myplan").click(function(){
    	var value=$(this).attr("data-id");
    	if($(this).css("cursor")=="pointer"){
    		$(".plan_select").val(value);
    		$(this).css("border","2px solid #e52344");
    		$("#savebtn").css("cursor","pointer");
    	}else{
    	}

    });

    $("#savebtn").click(function(){
    	if($(".plan_select").val()){
    		$(".formsubmit").click();
    	}
    });
    </script>
    						<div class="tab-pane content-datatable datatable-width" id="card_details_Section" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-6 offset-md-3">
    								<!-- card details start-->
    							<!--		<h4 class="margin-bottom-20" style="color:#ec1c41 !important;">Change Your Billing Detail</h4>

    								<div class="col-md-3"><label>Change Billing Detail</label></div>
    							<div class="col-md-9">
    									<div class="container">
    										<div class="row" style="border: 1px solid #ccc;padding: 23px;color: #000;border-radius: 5px;">
    											<div class="form-group">

    												 <label>XXXX XXXX XXXX <?php echo $carddetails['last4'] ?></label>
    											</div>

    											<div class="form-group">

    												<div style="float:left;">
    												 <label><?php echo $carddetails['exp_month'] ?>/<?php echo $carddetails['exp_year'] ?></label>
    												 </div>


    												<div style="float:right;">
    													 <label style="font-size: 25px;font-weight: 600;"><?php echo $carddetails['brand'] ?></label>
    												</div>
    											</div>
    										</div>
    									</div>
    								</div>-->
        								<form method="post" action="<?php echo base_url(); ?>customer/ChangeProfile/change_plan">
        									<div class="payment-method">
                                                <div class="text-center">
        										    <h4 class="sign-up-form-heading margin-bottom-20" style="color:#ec1c41 !important; font-weight: bold; font-size: 26px; margin-top: 40px;">Payment Method</h4>
                                                </div>

                                                <div class="pmformmain">
                                                    <form class="row">
														<div class="card-js">
															<div class="form-group col-md-12">
																<label class="inplabel">Card Number</label>
																<input type="text" class="card-number form-control" placeholder="">
															</div>

															<div class="form-group col-md-6">
																<label class="inplabel">Expiry Date</label>
																<input class="expiry-month" name="expiry-month">
																<input class="expiry-year" name="expiry-year">
															</div>

															<div class="form-group col-md-6">
																<label class="inplabel">CVC</label>
																<input type="text" class="cvc form-control" placeholder="">
															</div>

														</div>
														<div class="form-group col-md-12" style="margin-top: 10px;">
																<input type="hidden" name="current_plan" value="<?php echo $current_plan; ?>" />
																<input type="submit" value="Change My Card Details" class="btn btn-primary site-btn" name="change_my_card_deatils" style="background: #e52344; border: unset; font-weight: bold; text-transform: uppercase; padding: 18px 15px; font-size: 18px; width: 100%; border-radius: 6px; letter-spacing: 1px; "/>
															</div>
                                                    </form>
                                                </div>


        										<!--<div class="payment-method-inputs-wrapper">
        											<div class="card-js">
        												<div class="payment-method-input">
        													<div class="col-md-3"><label>Card Number</label></div>
        													<div class="col-md-9">
        														<div class="form-group">
        															<input class="card-number my-custom-class" name="card-number">
        														</div>
        													</div>
        												</div>
        												<div class="payment-method-input exp_date">
        													<div class="col-md-3"><label>Exp Date</label></div>
        													<div class="col-md-9">
        														<div class="form-group">
        																<input class="expiry-month" name="expiry-month">
        																<input class="expiry-year" name="expiry-year">
        														</div>
        													</div>
        												</div>
        												<div class="payment-method-input">
        													<div class="col-md-3"><label>Card CVC</label></div>
        													<div class="col-md-9">
        														<div class="form-group">
        															<input class="cvc" name="cvc">
        														</div>
        													</div>
        												</div>
        											</div>
        											<div class="text-center">
        												<div class="form-group">
        												    <input type="hidden" name="current_plan" value="<?php echo $current_plan; ?>" />
        													<input type="submit" value="Change My Card Details" class="btn btn-primary site-btn" name="change_my_card_deatils" style="background: #ec3d56;border: unset; font-weight: bold; text-transform: uppercase; padding: 10px 35px; font-size: 18px; width: 100%; border-radius: 8px; "/>
        												</div>
        											</div>
        										</div>-->



        									</div>
        								</form>
    								<!-- Card Details End-->
    								</div>
    							</div>
    						</div>
    <style>
    td{
        font-size: 15px;
        color: #2f4458;
    }
    </style>
    						<div class="tab-pane content-datatable datatable-width" id="invoices_section" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">

    								<!-- Invoice Section Start-->
                                    <div class="text-center">
                                        <h4 class="sign-up-form-heading margin-bottom-20" style="color:#ec1c41 !important; font-weight: bold; margin-top: 40px; font-size: 26px;">Last Invoices</h4>
                                    </div>

                                    <div class="invoicestable">
        								<table class="table table-hover">
        									<thead>
        										<tr>
        											<td>No.</td>
        											<td>Amount</td>
        											<td>Invoice No.</td>
        											<td>Created</td>
        										</tr>
        									</thead>

        									<tbody>
        										<?php $j=1;
        											for($i=0;$i<sizeof($invoices);$i++){
        												if(!$invoices[$i]->amount_paid){
        													continue;
        												}?>
        											<tr>
        												<td><?php echo $j; ?></td>
        												<td>$<?php echo ($invoices[$i]->amount_paid)/100; ?></td>
        												<td><?php echo $invoices[$i]->number; ?></td>
        												<td><?php echo date("m-d-Y",$invoices[$i]->date); ?></td>
        											</tr>
        										<?php $j++;
        											} ?>
        									</tbody>
        								</table>
                                    </div>
    								<!-- Invoice Section End-->

    								</div>
    							</div>
    						</div>
    				</div>
    <!-- Tab Content End-->


    			<!-- **************************************************-->



    			<!-- **************************************************-->

                    </div>
                </div>
    		</div>
        </section>
    </div>
</div>
<?php */ ?>


<script>
var masking = {

  // User defined Values
  //maskedInputs : document.getElementsByClassName('masked'), // add with IE 8's death
  maskedInputs : document.querySelectorAll('.masked'), // kill with IE 8's death
  maskedNumber : 'XdDmMyY9',
  maskedLetter : '_',

  init: function () {
    masking.setUpMasks(masking.maskedInputs);
    masking.maskedInputs = document.querySelectorAll('.masked'); // Repopulating. Needed b/c static node list was created above.
    masking.activateMasking(masking.maskedInputs);
  },

  setUpMasks: function (inputs) {
    var i, l = inputs.length;

    for(i = 0; i < l; i++) {
      masking.createShell(inputs[i]);
    }
  },
  
  // replaces each masked input with a shall containing the input and it's mask.
  createShell : function (input) {
    var text = '', 
        placeholder = input.getAttribute('placeholder');

    input.setAttribute('maxlength', placeholder.length);
    input.setAttribute('data-placeholder', placeholder);
    input.removeAttribute('placeholder');

    text = '<span class="shell">' +
      '<span aria-hidden="true" id="' + input.id + 
      'Mask"><i></i>' + placeholder + '</span>' + 
      input.outerHTML +
      '</span>';

    input.outerHTML = text;
  },

  setValueOfMask : function (e) {
    var value = e.target.value,
        placeholder = e.target.getAttribute('data-placeholder');

    return "<i>" + value + "</i>" + placeholder.substr(value.length);
  },
  
  // add event listeners
  activateMasking : function (inputs) {
    var i, l;

    for (i = 0, l = inputs.length; i < l; i++) {
      if (masking.maskedInputs[i].addEventListener) { // remove "if" after death of IE 8
        masking.maskedInputs[i].addEventListener('keyup', function(e) {
          masking.handleValueChange(e);
        }, false); 
      } else if (masking.maskedInputs[i].attachEvent) { // For IE 8
          masking.maskedInputs[i].attachEvent("onkeyup", function(e) {
          e.target = e.srcElement; 
          masking.handleValueChange(e);
        });
      }
    }
  },
  
  handleValueChange : function (e) {
    var id = e.target.getAttribute('id');
        
    switch (e.keyCode) { // allows navigating thru input
      case 20: // caplocks
      case 17: // control
      case 18: // option
      case 16: // shift
      case 37: // arrow keys
      case 38:
      case 39:
      case 40:
      case  9: // tab (let blur handle tab)
        return;
      }

    document.getElementById(id).value = masking.handleCurrentValue(e);
    document.getElementById(id + 'Mask').innerHTML = masking.setValueOfMask(e);

  },

  handleCurrentValue : function (e) {
    var isCharsetPresent = e.target.getAttribute('data-charset'), 
        placeholder = isCharsetPresent || e.target.getAttribute('data-placeholder'),
        value = e.target.value, l = placeholder.length, newValue = '', 
        i, j, isInt, isLetter, strippedValue;

    // strip special characters
    strippedValue = isCharsetPresent ? value.replace(/\W/g, "") : value.replace(/\D/g, "");

    for (i = 0, j = 0; i < l; i++) {
        var x = 
        isInt = !isNaN(parseInt(strippedValue[j]));
        isLetter = strippedValue[j] ? strippedValue[j].match(/[A-Z]/i) : false;
        matchesNumber = masking.maskedNumber.indexOf(placeholder[i]) >= 0;
        matchesLetter = masking.maskedLetter.indexOf(placeholder[i]) >= 0;

        if ((matchesNumber && isInt) || (isCharsetPresent && matchesLetter && isLetter)) {

                newValue += strippedValue[j++];

          } else if ((!isCharsetPresent && !isInt && matchesNumber) || (isCharsetPresent && ((matchesLetter && !isLetter) || (matchesNumber && !isInt)))) {
                // masking.errorOnKeyEntry(); // write your own error handling function
                return newValue; 

        } else {
            newValue += placeholder[i];
        } 
        // break if no characters left and the pattern is non-special character
        if (strippedValue[j] == undefined) { 
          break;
        }
    }
    if (e.target.getAttribute('data-valid-example')) {
      return masking.validateProgress(e, newValue);
    }
    return newValue;
  },

  validateProgress : function (e, value) {
    var validExample = e.target.getAttribute('data-valid-example'),
        pattern = new RegExp(e.target.getAttribute('pattern')),
        placeholder = e.target.getAttribute('data-placeholder'),
        l = value.length, testValue = '';

    //convert to months
    if (l == 1 && placeholder.toUpperCase().substr(0,2) == 'MM') {
      if(value > 1 && value < 10) {
        value = '0' + value;
      }
      return value;
    }
    // test the value, removing the last character, until what you have is a submatch
    for ( i = l; i >= 0; i--) {
      testValue = value + validExample.substr(value.length);
      if (pattern.test(testValue)) {
        return value;
      } else {
        value = value.substr(0, value.length-1);
      }
    }
  
    return value;
  },

  errorOnKeyEntry : function () {
    // Write your own error handling
  }
}

masking.init();
</script>