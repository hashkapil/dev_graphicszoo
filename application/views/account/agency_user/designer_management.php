<section id="client_mngmnt" class="client_setting">
    
    <div class="container">
        <div class="header-blog">
         <a href="<?php echo base_url(); ?>account/user_setting" class="back-link-xx0 text-uppercase">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">
         </a>
         <?php
            if ($this->session->flashdata('message_error') != '') {
            ?>
         <div id="message" class="alert  alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="">
               <?php echo $this->session->flashdata('message_error'); ?>
            </p>
         </div>
         <?php } ?>
         <?php if ($this->session->flashdata('message_success') != '') { ?>
         <div id="message" class="alert alert-success alert-dismissable success_msg">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="">
               <?php echo $this->session->flashdata('message_success'); ?>
            </p>
         </div>
         <?php } ?>
      </div>
    <div class="tab-content mb-t30">
        <div class="tab-pane active" id="client_management">
        <?php 
//        echo "<pre/>";print_R($designerdata);
        if(!empty($designerdata)){ ?>
        <div class="sub-userlist" id="sub-userlist">
        <div class="white-boundries">
           <div class="headerWithBtn">
              <h2 class="main-info-heading">Designer Management</h2>
              <p class="fill-sub">Add and Update additional users for your account.</p>
              <div class="addPlusbtn">
                 <a href="javascript:void(0)" class="new-subuser add_subuser_main " id="add_subuser_main" data-user_type="client">+ Add New Designer</a>
              </div>
           </div>
           <div id="no-more-tables">
              <div class="managment-list">
                  <table>
                 <?php if(!empty($designerdata)){ 
//                     echo "<pre/>";print_R($designerdata);
                 foreach ($designerdata as $userkey => $userval) {  

                  if($userval['is_delete']==0){
                  ?>
                    <tr>
                      <td><span><img src="<?php echo $userval['profile_picture']; ?>" class="img-responsive"></span></td>
                      <td class="name"><span><?php echo isset($userval['first_name']) ? $userval['first_name'] : ''; ?>
                       <?php echo isset($userval['last_name']) ? $userval['last_name'] : ''; ?></span></td>
                      <td><span><?php echo isset($userval['email']) ? $userval['email'] : ''; ?></span></td>
                      <td><span><?php echo isset($userval['phone']) ? $userval['phone'] : ''; ?></span></td>
                      <td><span><?php echo (isset($userval['display_plan_name']) && $userval['display_plan_name'] != "") ? $userval['display_plan_name'] : 'No plan selected'; ?></span></td>
                      <td class="action">
                          <span>
                            <a href="javascript:void(0);" data-id="<?php echo $userval['id']; ?>" class="edit_subuser" data-user_type="client">
                                <i class="fas fa-pen"></i>
                            </a>
                            <a href="javascript:void(0)" data-target="#deletesubuser" data-toggle="modal" data-id="<?php echo $userval['id']; ?>"  class="delete_clients">
                                <i class="far fa-trash-alt"></i>
                            </a>
                            <div class="switch-custom-usersetting-check activate-user">
                                <input type="checkbox" name="enable_disable_set" data-userid="<?php echo $userval['id']; ?>" data-email="<?php echo $userval['email']; ?>" data-name="<?php echo $userval['first_name']; ?>" id="enable_disable_set_<?php echo $userval['id']; ?>"
                                <?php
                                if ($userval['is_active'] == 1) {
                                    echo 'checked';
                                } else {

                                }
                                ?>>
                                <label for="enable_disable_set_<?php echo $userval['id']; ?>"></label> 
                            </div>
                          </span>
                          <?php if($userval["mode"][0]["payment_mode"] == 1 && ($userval['customer_id'] == "" || $userval['plan_name'] == "")){ ?>
                            <a href="javascript:void(0)" class="active_blling_acont" data-target="#activateclientbilling" data-toggle="modal" data-id="<?php echo $userval['id']; ?>">Activate Billing</a>
                          <?php } ?>
                      </td>
                    </tr>
                 <?php } }
                 }else{
                     echo "<div class='no_found'>Designer not found</div>"; 
                 }?>
                    </table>
              </div>
           </div>
        </div>
            </div>
        <div class="white-boundries" id="new-subuser" style="display: none;">
            <div class="headerWithBtn">
                <h2 class="main-info-heading">New User</h2>
                <p class="fill-sub">Complete the form below to add new user.</p>
                <div class="addPlusbtn"><a class="backlist-go backtoclntlistng"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">Back
                </a>
                </div>
                <div class="designrerror_msg" style="display:none"></div>
            </div>
                <form method="post" role="form" class="add_designer_saas" id="add_designer_saas">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt">First Name <span>*</span></p>
                                <input type="text" name="first_name" class="input" id="fname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt">Last Name <span>*</span></p>
                                <input type="text" name="last_name" class="input" id="lname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt">Email Address <span>*</span></p>
                                <input type="email" class="input" name="email" id="user_email"  data-rule="email" data-msg="Please enter a valid email" value="" required="">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt">Phone Number <span>*</span></p>
                                <input type="tel" name="phone" class="input" id="phone" value=""required="">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                        </div>
                        <div class="col-md-6 password_toggle">
                            <div class="notify-lines">
                                <p>Randomly generated password</p>
                                <label class="form-group switch-custom-usersetting-check">
                                    <input type="checkbox" name="genrate_password" id="password_ques" checked/>
                                    <label for="password_ques"></label> 
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 create_password" style="display:none">
                            <label class="form-group">
                                <p class="label-txt label-active">Password<span>*</span></p>
                                <input type="password" name="password" class="input" id="password" value=""/>
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div class="designrajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                    <input type="hidden" name="cid" id="cid">
                    <div class="btn-here">
                        <input type="submit" name="save_subuser" class="btn-red" value="SAVE">
                    </div>
                </form>
            </div>
        <?php }else{ ?>
            <div class="sub-userlist white-boundries" id="edit_client_prfl">
                <div class="headerWithBtn">
                        <?php ?>
                        <h2 class="main-info-heading">Edit User</h2>
                        <?php ?>
                        <p class="fill-sub">Complete the form below to add new user.</p>
                        <div class="addPlusbtn"><a href="<?php echo base_url(); ?>account/client_management#client_management" class="backlist-go"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">Back
                            </a>
                        </div>
                        <div class="designrerror_msg alert-danger" style="display:none"></div>
                </div>
                <?php $this->load->view('account/agency_user/edit_client_profile', $clientdata,$active_subscriptions,$checksubuser_requests,$agency_data); ?>
            </div>  
            <?php } ?>
     </div>
        <div class="tab-pane" id="subscription_management">
            <div class="sub-userlist white-boundries" id="client_subs">
               <?php if($stripconnect_link == "" || $pay_offline == 1){ ?> 
               <div class="headerWithBtn subs_hdr">
                 <h2 class="main-info-heading">Subscription</h2> 
                 <p class="fill-sub">Add and Update  subscription for your account.</p>
                 <div class="addPlusbtn"><a href="javascript:void(0)" class="add_subscription f_add_subscription" id="f_add_subscription">+ Add Subscription</a></div>
               </div>
                
                <div id="no-more-tables">
                    <div class="client_billinginfo">
                        <div class="product-list-show clint_subs">
                            <?php if(!empty($subscriptions)){ ?>
                            <div class="cli-ent-row tr tableHead">
                                <div class="cli-ent-col td" style="width: 30%;">
                                    NAME
                                </div>
                                <div class="cli-ent-col td" style="width: 20%;">
                                    Price
                                </div>
                                <div class="cli-ent-col td text-center" style="width: 30%;">
                                    Type
                                </div>
                                <div class="cli-ent-col td"style="width: 20%;">
                                    Action
                                </div>
                            </div>

                            <?php foreach ($subscriptions as $plans) { ?>
                                <div class="cli-ent-row tr brdr">
                                    <div class="mobile-visibles">
                                        <i class="fa fa-plus-circle"></i>
                                    </div>
                                    <!---Start Title---->
                                    <div class="cli-ent-col td client-project" style="width: 30%; ">
                                        <div class="cli-ent-xbox text-left">
                                            <div class="cell-row">
                                                <div class="cell-col" >
                                                    <div class="name-discp">
                                                        <h3 class="pro-head space-b">
                                                            <?php echo $plans['plan_name']; ?>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---End Title---->

                                    <!---Start Client name---->
                                    <div class="cli-ent-col td" style="width: 20%;">
                                        <span class="small">&nbsp;$&nbsp;</span>
                                        <p><?php echo $plans['plan_price'].'/'.$plans['plan_type']; ?></p>
                                    </div>

                                    <div class="cli-ent-col td text-center td_center" style="width: 30%;">
                                        <p><?php echo $plans['shared_user_type']; ?></p>
                                    </div>
                                    <?php //} ?>

                                    <!----Action tab start-->
                                    <div class="cli-ent-col td" style="width: 20%;">
                                        <div class="cli-ent-xbox action-per">
                                            <a href="javascript:void(0)" data-id="<?php echo $plans['id']; ?>" class="edit_clntsubs"><i class="icon-gz_edit_icon"></i></a>
                                        </div>
                                        <div class="switch-custom-usersetting-check f_active_subs_toggle">
                                            <input type="checkbox" name="enable_disable_subs" data-subsid="<?php echo $plans['id']; ?>" id="enable_disable_subs<?php echo $plans['id']; ?>"
                                                   <?php echo ($plans['is_active'] == 1)?'checked':'';
                                                   echo ($agency_data[0]['shared_designer'] == 0 && $plans['s_user_type'] == 1)?' disabled':''; ?>>
                                            <label for="enable_disable_subs<?php echo $plans['id']; ?>"></label> 
                                        </div>
                                    </div>
                                    <!----Action tab End-->

                                </div>

                            <?php } ?>
                            <?php }else{
                                    echo "<div class='no_found'>Subscription not found</div>";
                            } ?>
                        </div>
                        <div class="add_edt_clntsubs" style="display:none">
                            <form action="" method="post" role="form" class="subscrptn_frm f_subscrptn_frm subs_mode" id="subscrptn_frm">
                                    <div  class="row">
                                        <div class="headerWithBtn">
                                            <div class="fadd_subs">
                                                <h2 class="main-info-heading">New Subscription</h2>
                                                <p class="fill-sub">Complete the form below to add new subscription.</p>
                                            </div>
                                            <div class="fedit_subs" style="display:none">
                                                <h2 class="main-info-heading">Edit Subscription</h2>
                                                <p class="fill-sub">Edit below subscription form.</p>
                                            </div>
                                            <div class="addPlusbtn">
                                                <a class="backlist-go f_bk_subs">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">Back
                                                </a>
                                            </div>
                                            <div class="subserror_msg alert-danger" style="display:none"></div>
                                        </div>
                                        <div class="col-md-12 f_payment_mod">
                                            <label class="notify-lines">
                                                    <label class="f_toggle_paymnt_m switch-custom-usersetting-check">
                                                        <input type="checkbox" name="subs_payment_mode" class="form-control" id="f_subs_payment_mode" data-payment_mode="<?php echo $agency_data[0]['online_payment']; ?>" data-stripe_link="<?php echo $stripconnect_link; ?>">
                                                        <label for="f_subs_payment_mode"></label> 
                                                    </label>
                                                    <p class="label-txt payment_mod_txt">Offline Payment</p>
                                            </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-group">
                                                    <p class="label-txt label-active">Plan Type <span>*</span></p>
                                                    <select class="input select-c" name="subs_plan_type_name" id="f_subs_plan_type_name">
                                                        <option value="all">Subscription Based</option>
                                                        <option value="one_time">Request Based</option>
                                                    </select>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                        <?php if($agency_data[0]['shared_designer'] == 1){ ?>
                                            <div class="col-md-6 f_pln_user_type">
                                                <label class="form-group">
                                                    <p class="label-txt label-active">User Type <span>*</span></p>
                                                    <select class="input select-c" name="subs_plan_user_type" id="f_subs_user_type">
                                                        <option value="1">Shared</option>
                                                        <option value="0">Dedicated</option>
                                                    </select>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                        <?php }else{ ?>
                                           <input type="hidden" name="subs_plan_user_type" value="0" id="f_subs_user_type">
                                        <?php } ?>
                                            <div class="col-md-6 stripe_is_sec">
                                                <div class="stripe_pln_sec" style="display:none">
                                                <label class="form-group">
                                                    <div class="label-txt label-active">Stripe ID<span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> <strong>Note:</strong> <span>You need to create this product for subscription based plans in stripe first and use the plan ID here. For more info <a href="https://drive.google.com/file/d/1vcaVS_NB0msCWbfkI22aOIrpj-UZqZqC/view" target="_blank"class="demo_stripe_link">click here </a></span></div></div></div>
                                                    <select class="input select-c" name="subs_planid" id="f_subs_planid" data-pay="1" value="">
                                                        <?php if(!empty($clientplans)){
                                                           foreach ($clientplans as $plan) { ?>
                                                            <option value="<?php echo $plan['id']; ?>"><?php echo $plan['plan_name']; ?></option>
                                                        <?php }
                                                        } ?>
                                                    </select>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                                </div>
                                                <div class="mannuly_subs_pln_sec" style="display:block">
                                                    <label class="form-group">
                                                            <div class="label-txt label-active">Plan ID<span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"><strong>Note:</strong> <span>Plan id should be unique.</span></div></div></div>
                                                            <input type="text" name="subs_plan_id" class="input" id="f_subs_plan_id" data-pay="1" value="" required="">
                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-group">
                                                    <div class="label-txt">Plan Name <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> This will be show to users when they signup</div></div></div>
                                                    <input type="text" name="subs_plan_name" class="input" id="f_subs_plan_name" value="" required="">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-md-6 cstm_plan_price">
                                                <label class="form-group">
                                                    <p class="label-txt">Plan Price <span>*</span></p>
                                                    <span>$</span><input type="number" name="subs_plan_price" class="input" id="f_subs_plan_price" value="" required="" min="1">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="onetimepln" style="display:none">
                                               <div class="col-md-6">
                                                <label class="form-group">
                                                    <div class="label-txt">Number of Requests <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Total Number of requests user can add.</div></div></div>
                                                    <input type="number" name="total_requests" class="input" id="f_subs_numof_req" value="" min="1">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                                </div> 
                                            </div>
                                            <div class="col-md-6 f_amount_cycle">
                                                <label class="form-group">
                                                    <p class="label-txt label-active">Payment Cycle <span>*</span></p>
                                                    <select class="input select-c" name="subs_plan_type" id="f_subs_plan_type">
                                                        <option value="monthly">Monthly</option>
                                                        <option value="yearly">Yearly</option>
                                                    </select>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="notify-lines">
                                                    <label class="f_toggle_plan_active switch-custom-usersetting-check">
                                                        <input type="checkbox" name="f_subs_plan_active" class="form-control" id="f_subs_plan_active">
                                                        <label for="f_subs_plan_active"></label> 
                                                    </label>
                                                    <p class="label-txt">Activate Plan</p>
                                                </label>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="permiss-outer">
                                                    <h2 class="main-info-heading">Permissions</h2>
                                                        <div class="col-md-6">
                                                            <label class="notify-lines">
                                                                    <label class="f_toggle_file_sharing switch-custom-usersetting-check">
                                                                    <input type="checkbox" name="f_file_sharing" class="form-control" id="f_file_sharing">
                                                                    <label for="f_file_sharing"></label> 
                                                                    </label>
                                                                    <p class="label-txt">File Sharing Capabilities</p>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="notify-lines">
                                                                    <label class="f_toggle_file_management switch-custom-usersetting-check">
                                                                    <input type="checkbox" name="f_file_management" class="form-control" id="f_file_management">
                                                                    <label for="f_file_management"></label> 
                                                                    </label>
                                                                    <p class="label-txt">File Management</p>
                                                            </label>
                                                        </div>
                                                    <div class="col-md-6">
                                                        <label class="notify-lines">
                                                                <label class="f_toggle_apply_coupon switch-custom-usersetting-check">
                                                                <input type="checkbox" name="f_apply_coupon" class="form-control" id="f_apply_coupon">
                                                                <label for="f_apply_coupon"></label> 
                                                                </label>
                                                                <p class="label-txt">Accept Coupons</p>
                                                        </label>
                                                    </div>
                                                 </div>
                                                </div>
                                            </div>
                                           <div class="plan-add-sign">
                                           
                                                    <div class="col-lg-12 col-md-12">
                                                        <label class="form-group">
                                                            <p class="label-txt label-active">Plan Features</p>
                                                            <div class="f_subs_plan_features_blok">
                                                                <input type="text" name="subs_plan_features[]" class="input f_subs_plan_features f_cls_nm"/>
                                                                <div class="line-box">
                                                                  <div class="line"></div>
                                                              </div>
                                                            </div>
                                                          <a class="add_more_button">
                                                              <i class="icon-gz_plus_icon"></i>
                                                          </a>
                                                          <div class="input_fields_container"></div>
                                                      </label>
                                                  </div>
                                               
                                           </div>
                                            <div class="form-group col-md-12 mb-t30">
                                                <div class="subsajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/gz_customer_loader.gif" /></div>
                                                <div class="btn-here">
                                                    <input type="submit" name="save_clntsubs" id="save_clntsubs" class="btn-red center-block" value="SAVE">
                                                </div>
                                            </div>
                                        
                                    </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
                <?php } else{
                    echo "<div class='no_found no_flex'><p>For using this feature, you need to connect your account with stripe and then have to create plans on stripe. After that, You can create your plan here. <a href='".$stripconnect_link."' target='_blank'>Click here</a> to connect your account.</p></div>";
                } ?>
            </div>
        </div>
        <div class="tab-pane" id="addi_setting">
            <div class="sub-userlist" id="addition_setting">
                <div class="add_setting_sec">
                    <form action="<?php echo base_url();?>/account/AgencyClients/save_addition_setting" method="post" class="f_ratio_setting">
                        <div class="headerWithBtn white-boundries">
                           <h2 class="main-info-heading">client management settings</h2> 
                            <div class="settcol right">
                              <p class="btn-x"><input type="submit" id="req_info_btn" name="savebtn" class="btn-e save_info" value="Save">
                              <input type="hidden" id="agency_usr" name="agency_usr" value="<?php echo $login_user_id; ?>"></p>
                            </div>
                        </div>
                        <div class="current-setting">
                            <div class="white-boundries">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="current_setting_item dedicated_number">
                                            <h2 class="main-info-heading">Dedicated designer</h2>
                                            <span><strong><?php echo $login_user_data[0]['total_inprogress_req']; ?> </strong> </span> <span class="plus-sign-ico">=</span> 
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="current_setting_item">
                                            <h2 class="main-info-heading">Designers for my work<span class="tool-hover"><i class="fas fa-info-circle"></i><span class="tool-text">Reserve designers for your work and also one-time requests you resell by using the dropdown below. The rest of the designers will be used to resell to your clients.</span></span></h2>
                                            <label class="form-group">
                                                <select name="reserve_count" id="f_reserve_count" class="input form-control" data-total_req="<?php echo $login_user_data[0]['total_inprogress_req']; ?>">
                                                    <option value="-1" <?php
                                                    if (!isset($agency_data[0]['reserve_count']) || $agency_data[0]['reserve_count'] == -1) {
                                                        echo "selected";
                                                    } if ($agency_data[0]['pending_reserve_count'] != $login_user_data[0]['total_inprogress_req']) {
                                                        echo "disabled";
                                                    }
                                                    ?>>&nbsp&nbsp  All Designers</option>
                                                            <?php
                                                            for ($i = 0; $i <= $login_user_data[0]['total_inprogress_req']; $i++) {
                                                                if ($i > $agency_data[0]['pending_reserve_count']) {
                                                                    $property = "disabled";
                                                                } else {
                                                                    $property = "";
                                                                }
                                                                ?>
                                                        <option value="<?php echo $i; ?>" <?php
                                                        if (isset($agency_data[0]['reserve_count']) && $agency_data[0]['reserve_count'] == $i) {
                                                            echo "selected";
                                                        } echo $property;
                                                        ?>>&nbsp&nbsp <?php echo $i; ?></option>
                                                            <?php } ?>
                                                </select>
                                                <span class="plus-sign-ico">+</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="current_setting_item">
                                            <h2 class="main-info-heading">Available Designers to resell</h2>
                                            <?php if($agency_data[0]['reserve_count'] == -1){
                                                $reserve = $login_user_data[0]['total_inprogress_req'];
                                            }else{
                                                $reserve = $agency_data[0]['reserve_count'];
                                            } ?>
                                            <span><strong class="f_resell_count"><?php echo $total_count = $login_user_data[0]['total_inprogress_req']-$reserve; ?></strong> dedicated designer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <div class="row">
                          <div class="col-md-4">
                            <div class="white-boundries addtional_card_item">
                            <h2 class="main-info-heading">Shared Designer<label class="f_toggle_shared switch-custom-usersetting-check">
                                        <input type="checkbox" name="shared_designer" class="form-control" id="shared_designer" <?php echo ($agency_data[0]['shared_designer'] == 1) ? 'checked' : '';?> <?php echo ($agency_data[0]['shared_designer'] == 1 && $checksubuser_requests["sharedusers"] > 0)?"disabled":""; ?>>
                                        <label for="shared_designer"></label> 
                                    </label></h2>
                                <label class="form-group">
                                    <div class="label-txt label-active"> Set User to Designer Ratio </div>
                                    <p class="note_text fill-sub"><strong>Note:</strong> <span>Change the ratio below to split each dedicated designers time in your account to resell design services. Or keep it at 1 to provide each designers full time to every client. </span></p>
                                    <div class="show-designers"> <select name="no_of_assign_user" id="ratio_numbr" class="input">
                                        <?php for($i = 1;$i <= 4; $i++){ ?>
                                        <option value="<?php echo $i; ?>" <?php if ($agency_data[0]['no_of_assign_user'] == $i) { echo "selected"; } ?>>&nbsp&nbsp<?php echo $i; ?></option>
                                        <?php } ?>
                                        </select> <span>customers to 1 designer</span></div>
                                    
                                </label>
                            </div>
                          </div>
    
                          <div class="col-md-4">
                            <div class="white-boundries addtional_card_item payment_mode_card">
                                <h2 class="main-info-heading">payment mode<span class="tool-hover"><i class="fas fa-info-circle"></i><span class="tool-text">Enable these settings to sync stripe payments and auto upgrade your account for a complete streamlined system.</span></span></h2>
                                 <label class="notify-lines">
                                    <label class="f_toggle_online_payment switch-custom-usersetting-check">
                                        <span id="isokyAttr" data-prop=""></span>
                                        <input type="checkbox" name="online_payment" class="form-control" id="online_payment" <?php echo ($agency_data[0]['online_payment'] == 1) ? 'checked' : ''; ?> data-target="#change_paymentflag" data-toggle="modal">
                                        <label for="online_payment"></label> 
                                       
                                    </label>
                                    <p class="label-txt"> Online Payment</p>
                                     <p class="note_text fill-sub"><strong>Note:</strong> <span>Enabling online payments allows you to accept payments from your clients directly through this platform using Stripe integration. You can always add clients who you charge outside of this system even if this option is enabled. </span></p>
                                </label>
                            </div>
                          </div>
                          
                          <div class="col-md-4">
                            <div class="white-boundries addtional_card_item">
                              <h2 class="main-info-heading">user setting</h2>
                               <label class="notify-lines">
                                    <label class="f_toggle_default_active switch-custom-usersetting-check">
                                        <input type="checkbox" name="default_active" class="form-control" id="default_active" <?php echo ($agency_data[0]['default_active'] == 1) ? 'checked' : ''; ?>>
                                        <label for="default_active"></label> 
                                    </label>
                                    <p class="label-txt"> Auto Approve New Users who Sign up<span class="tool-hover"><i class="fas fa-info-circle"></i><span class="tool-text">Whenever a new user signs up, this will auto approve them.</span></span></p>
                                </label>
                                <label class="notify-lines">
                                    <label class="f_toggle_signup_option switch-custom-usersetting-check">
                                        <input type="checkbox" name="show_signup" class="form-control" id="signup_toggle" <?php echo ($agency_data[0]['show_signup'] == 1) ? 'checked' : ''; ?>>
                                        <label for="signup_toggle"></label> 
                                    </label>
                                    <p class="label-txt">  Show Create Account Link on Sign In Page</p>
                                </label>
                              <label class="notify-lines">
                                    <label class="f_toggle_billing switch-custom-usersetting-check">
                                        <input type="checkbox" name="show_billing" class="form-control" id="show_billing" <?php echo ($agency_data[0]['show_billing'] == 1) ? 'checked' : ''; ?>>
                                        <label for="show_billing"></label> 
                                    </label>
                                    <p class="label-txt"> Show Billing Section for Offline Payment Users</p>
                                </label>
                            </div>
                          </div>  
          
                      </div>
                        
                    </form>
                </div>
            </div>
                
        </div>
        <div class="tab-pane" id="stripe_info">
            <div class="stripe_info white-boundries" id="stripe_info_sec">
                <div class="headerWithBtn">
                 <h2 class="main-info-heading">Stripe Info</h2> 
                </div><br>
                <div class="stripe_info_cntnt">
                    <?php if(!empty($strpe_acc_info)){
                      //  echo "<pre>";print_r($strpe_acc_info); ?>
                    <div class="row">
                        <?php if($strpe_acc_info['account']['settings']['dashboard']['display_name'] != ""){ ?>
                            <div class="col-md-6">
                                <label class="form-group">
                                        <p class="label-txt label-active"> Display Name</p>
                                        <span class="input"><?php echo $strpe_acc_info['account']['settings']['dashboard']['display_name']; ?></span>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                </label>
                            </div>
                        <?php } 
                        if($strpe_acc_info['account']['email'] != ""){ ?>
                        <div class="col-md-6">
                            <label class="form-group">
                                    <p class="label-txt label-active"> Email</p>
                                    <span class="input"><?php echo $strpe_acc_info['account']['email']; ?></span>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                            </label>
                        </div>
                        <?php }
                        if($agency_data[0]['stripe_account_id'] != ""){ ?>
                        <div class="col-md-6">
                            <label class="form-group">
                                    <p class="label-txt label-active"> Account ID</p>
                                    <span class="input"><?php echo $agency_data[0]['stripe_account_id']; ?></span>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                            </label>
                        </div>
                        <?php }
                        if($agency_data[0]['stripe_api_key'] != ""){ ?>
                        <div class="col-md-6">
                            <label class="form-group">
                                    <p class="label-txt label-active"> API KEY</p>
                                    <span class="input"><?php echo $agency_data[0]['stripe_api_key']; ?></span>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                            </label>
                        </div>
                        <?php }
                            if($strpe_acc_info['account']['settings']['dashboard']['apiKey'] != ""){ ?>
                            <div class="col-md-6">
                                <label class="form-group">
                                        <p class="label-txt label-active"> Email</p>
                                        <span class="input"><?php echo $strpe_acc_info['account']['settings']['dashboard']['apiKey']; ?></span>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                </label>
                            </div>
                            <?php }
                        if($strpe_acc_info['account']['support_email'] != ""){ ?>
                            <div class="col-md-6">
                                <label class="form-group">
                                        <p class="label-txt label-active"> Support Email</p>
                                        <span class="input"><?php echo $strpe_acc_info['account']['support_email']; ?></span>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                </label>
                            </div>
                        <?php } 
                        if($strpe_acc_info['account']['support_phone'] != ""){ ?>
                            <div class="col-md-6">
                                <label class="form-group">
                                        <p class="label-txt label-active"> Support Phone</p>
                                        <span class="input"><?php echo $strpe_acc_info['account']['support_phone']; ?></span>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                </label>
                            </div>
                        <?php } 
                        if($strpe_acc_info['account']['country'] != ""){ ?>
                            <div class="col-md-6">
                                <label class="form-group">
                                        <p class="label-txt label-active"> Country</p>
                                        <span class="input"><?php echo $strpe_acc_info['account']['country']; ?></span>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                </label>
                            </div>
                        <?php } 
                        if($strpe_acc_info['account']['settings']['dashboard']['timezone'] != ""){ ?>
                            <div class="col-md-6">
                                <label class="form-group">
                                        <p class="label-txt label-active"> Timezone</p>
                                        <span class="input"><?php echo $strpe_acc_info['account']['settings']['dashboard']['timezone']; ?></span>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<div class="modal fade similar-prop" id="deletesubuser" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Delete User</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">


                    <h3 class="head-c text-center">Are you sure you want to delete this user? </h3>

                    <div class="confirmation_btn three-lbl text-center"> 
                            <a data-id="<?php echo $userval['id']; ?>" class="btn btn-ydelete designer_del">Yes</a>
                            <button class="btn btn-ndelete" data-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade similar-prop" id="activateclientbilling" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Activate Billing</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <form action="<?php echo base_url(); ?>account/AgencyClients/activeclientbilling" method="post" role="form" class="activte_client_bling" id="f_activte_client_bling">
                        <input type="hidden" class="input" name="billing_client_id" id="f_billing_client_id">
                        <input type="hidden" name="stripe_key" value="<?php echo $agency_data[0]['stripe_api_key']; ?>">
                        <input type="hidden" name="active_plan_type" id="f_active_plan_type">
                        <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt label-active">Select Subscription</p>
                                <select id="f_selct_clintsubs" class="input" name="selct_clintsubs" required="">
                                    <?php foreach ($active_subscriptions as $subs) { ?> 
                                        <option value="<?php echo $subs['plan_id'] ?>" data-inprogress="<?php echo $subs['global_inprogress_request']; ?>" data-plan_type="<?php echo $subs['plan_type_name']; ?>" data-shared="<?php echo $subs['shared_user_type']; ?>"><?php echo $subs['plan_name']; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>

                            </label>
                        </div>
                        <div class="col-md-6 datepicker_billing_sec">
                            <label class="form-group">
                                <p class="label-txt label-active">Start Subscription</p>
                                <input type="text" class="input" id="datepicker" name="start_subs_picker">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>

                            </label>
                        </div>
                        <div class="confirmation_btn three-lbl text-center"> 
                            <button class="btn btn-red" type="submit">Activate</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="modal fade similar-prop" id="disablecustomer" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Disable Customer</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <h3 class="head-c text-center">Client account will be deactivated from Agency Account, but you need to manually disable subscription from Stripe (If activated).</h3>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="modal fade similar-prop" id="change_paymentflag" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Payment Mode</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <div class="f_payment_mode">
                    <h3 class="head-c text-center">Please follow the below steps for accepting payments online:</h3>
                        <ul> 
                        <li>Firstly, connect your Stripe account with GraphicsZoo. You can connect your existing account or create a new one. </li>
                        <li>Create subscriptions on Stripe and Sync with the GraphicsZoo Subscriptions. (You will get the guidelines in the subscription module.)</li>
                        <li>Sync your existing customers (if any) account with your Stripe account by clicking the Sync button in the client's management section.</li>
                        </ul>
                    </div>
                    <button class="btn btn-CnfrmChange btn-ndelete" id="change_paymnt_flg" type="submit">OK</button>
                    <?php if($stripconnect_link != ""){ ?>
                     <a class="btn btn-red" id="connect_withstripe" href='<?php echo $stripconnect_link; ?>' target='_blank'>Connect With Stripe</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade similar-prop" id="m_connectwithstripe" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Connect With Stripe</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <div class="f_stripepayment_mode">
                    <h3 class="head-c text-center">If you want create subscription with online payment mode, your account must be connect with stripe.</h3>
                    </div>
                     <a class="btn btn-red" id="connect_withstripe" href='<?php echo $stripconnect_link; ?>' target='_blank'>Connect With Stripe</a>
                </div>
            </div>
        </div>
    </div>
</div>
