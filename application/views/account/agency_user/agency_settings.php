<div class="agncy_stng_sidebar">
    <h2 class="main-info-heading">Additional Settings</h2>
    <ul class="f_addi_tabs list-header-blog">
        <li class="active" data-click="1"><a class="stayhere" href="#script_sec" data-toggle="tab">Tracking Code</a></li>
        <li><a href="#trm_prvcy_sec" class="stayhere" data-toggle="tab">Privacy Policy</a></li>
    </ul>
</div>
<div class="agncy_rignt_sidebar">
    <div class="tab-content mb-t30">
        <div class="tab-pane active" id="script_sec">
            <div class="f_javascript_code white-boundries">
                     <h2 class="main-info-heading">Tracking Script</h2>
                      <p class="fill-sub">Paste or type scripts below. < script > tags must be included.</p>
                      <form method="post" action="<?php echo base_url(); ?>account/AgencyUserSetting/tracking_code" id="tracking_code">
                          <div class="brand-outline tab-pane active" id="common_script"> 
                              <div class="trackingscript_sec">
                                  <div class="input_fields_container"></div>
                                <?php //echo "<pre>";print_r(unserialize($changeinfo[0]['script_data']));
                                  if(!empty(unserialize($changeinfo[0]['script_data']))){
                                  foreach (unserialize($changeinfo[0]['script_data']) as $k=>$value) { ?>
                                  
                                  <div class="accord-outer"><div class="social_accordion btn-primary" data-toggle="collapse" data-target="#stript_list_<?php echo $k; ?>"><?php echo ($value['script_name'] != "")?$value['script_name']:""; ?></div>
                                    
                                  </div>
                                  
                                  <div class="accordion-step collapse" id="stript_list_<?php echo $k; ?>">
                                      <div class="col-md-12">
                                          <div class="row">
                                              <div class="col-lg-12 col-md-12">
                                                <label class="form-group">
                                                  <p class="label-txt <?php echo (isset($value['script_name']) && $value['script_name'] != "") ? "label-active" : ""; ?>">Name</p>
                                                  <input name="script_name[]" class="input script_name f_cls_nm" value="<?php echo ($value['script_name'] != "")?$value['script_name']:""; ?>"/>
                                                  <div class="line-box">
                                                      <div class="line"></div>
                                                  </div>
                                                </label>
                                              </div>
                                              <div class="col-lg-12 col-md-12">
                                                  <label class="form-group">
                                                      <p class="label-txt <?php echo (isset($value['tracking_script']) && $value['tracking_script'] != "") ? "label-active" : ""; ?>">Paste your tracking code here</p>
                                                      <textarea name="tracking_script[]" class="live-script input f_trkngcls_nm" id="f_tracking_scrpt_<?php echo $k; ?>" style="height:<?php echo ($value['tracking_script'] != "") ? "150px" : "auto"; ?>"><?php echo isset($value['tracking_script']) ? base64_decode($value['tracking_script']) : ''; ?></textarea>
                                                      <div class="line-box">
                                                          <div class="line"></div>
                                                      </div>
                                                  </label>
                                              </div>
                                              <div class="col-lg-6 col-md-6">
                                                <label class="form-group">
                                                  <p class="label-txt label-active">Position inside the code</p>
                                                  <select name="script_position[]" class="input script_position f_scrpt_pos_nm">
                                                      <option value="before_head" <?php if($value['script_position'] == "before_head"){ echo "selected"; }?>>Before < /HEAD ></option>
                                                      <option value="before_body" <?php if($value['script_position'] == "before_body"){ echo "selected"; }?>>Before < /BODY ></option>
                                                  </select>
                                                </label>
                                              </div>
                                              <div class="col-lg-6 col-md-6">
                                                <label class="form-group">
                                                  <p class="label-txt label-active">Show only on specific page</p>
                                                  <select name="show_only_specific_page[]" class="input show_only_specific_page f_scrpt_pg_nm">
                                                      <option value="login" <?php if($value['show_only_specific_page'] == "login"){ echo "selected"; }?>>Login</option>
                                                      <option value="signup" <?php if($value['show_only_specific_page'] == "signup"){ echo "selected"; }?>>Signup</option>
                                                      <option value="cus_portl" <?php if($value['show_only_specific_page'] == "cus_portl"){ echo "selected"; }?>>Customer Portal</option>
                                                      <option value="all_pages" <?php if($value['show_only_specific_page'] == "all_pages"){ echo "selected"; }?>>All Pages</option>
                                                  </select>
                                                </label>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="accor-del align-center">
                                        <a href="<?php echo base_url(); ?>account/AgencyUserSetting/tracking_code/<?php echo $k; ?>">Delete</a>
                                        </div>
                                  </div>
                                  <?php }
                                  }else{ ?>
                                  <div class="input_fields_container1">
                                          <div class="remove-pre">
                                      
                                                  <div class="col-lg-12 col-md-12">
                                                      <label class="form-group">
                                                          <p class="label-txt">Name</p>
                                                          <input name="script_name[]" class="input script_name f_cls_nm" />
                                                          <div class="line-box">
                                                              <div class="line"></div>
                                                          </div>
                                                      </label>
                                                  </div>
                                                  <div class="col-lg-12 col-md-12">
                                                      <label class="form-group">
                                                          <p class="label-txt">Paste your tracking code here</p>
                                                          <textarea name="tracking_script[]" class="live-script input f_trkngcls_nm" id="f_tracking_scrpt" style="height:auto"></textarea>
                                                          <div class="line-box">
                                                              <div class="line"></div>
                                                          </div>
                                                      </label>
                                                      <div class="scriptvalidation_error"></div>
                                                  </div>
                                                  <div class="col-lg-6 col-md-6">
                                                      <label class="form-group">
                                                          <p class="label-txt label-active">Position inside the code</p>
                                                          <select name="script_position[]" class="input script_position f_scrpt_pos_nm">
                                                              <option value="before_head">Before < /HEAD ></option>
                                                              <option value="before_body">Before < /BODY ></option>
                                                          </select>
                                                          <div class="line-box">
                                                            <div class="line"></div>
                                                            </div>
                                                      </label>
                                                  </div>
                                                  <div class="col-lg-6 col-md-6">
                                                      <label class="form-group">
                                                          <p class="label-txt label-active">Show only on specific page</p>
                                                          <select name="show_only_specific_page[]" class="input show_only_specific_page f_scrpt_pg_nm">
                                                              <option value="login">Login</option>
                                                              <option value="signup">Signup</option>
                                                              <option value="cus_portl">Customer Portal</option>
                                                              <option value="all_pages">All Pages</option>
                                                          </select>
                                                          <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                      </label>
                                                  </div>
                                             
                                          </div>
                                      </div>
                                 <?php } ?>
                                <a class="add_more_script">
                                    <i class="icon-gz_plus_icon"></i>
                                </a>
                            </div>
                            </div>
                          <div class="col-lg-12 ad_new_reqz">
                              <input type="submit" id="trkng_scrpt" name="trkng_scrpt" class="btn-e save_info btn-red submit" value="Save">
                          </div>
                      </form>
                 </div>
        </div>
        <div class="tab-pane" id="trm_prvcy_sec">
            <div class="f_trm_prvcy white-boundries">
                 <form method="post" action="<?php echo base_url(); ?>account/AgencyUserSetting/Save_term_privacy" id="tc_privacy">
                     <div class="row">
                         <div class="col-md-12">
                             <h2 class="main-info-heading">T&C and Privacy Policy Setting</h2>
                             <p class="fill-sub">Please add your term & condition and privacy policy in below textarea, the policy will show to all your clients at signup.<p>
                             <div class="row">
                                 <div class="col-lg-12 col-md-12">
                                    <label class="form-group switch-custom-usersetting-check f_term_privacy_toggle">
                                        <p class="toggle-text">Show T&C and Privacy Policy</p>
                                        <input type="checkbox" name="show_term_privacy" id="show_term_privacy" <?php echo (isset($changeinfo[0]['show_term_privacy']) && $changeinfo[0]['show_term_privacy'] == 1) ? "checked" : ""; ?>/>
                                        <label for="show_term_privacy"></label> 
                                   </label>
                                 </div>
                                 <div class="term_pryc_scton" style="<?php echo (isset($changeinfo[0]['show_term_privacy']) && $changeinfo[0]['show_term_privacy'] == 1) ? "display:block" : "display:none"; ?>">
                                 <div class="col-lg-12 col-md-12">
                                     <label class="form-group">
                                         <h3>Term & Condition</h3>
                                         <textarea id="term_conditions" name="term_conditions">
                                             <?php echo isset($changeinfo[0]['term_conditions'])?$changeinfo[0]['term_conditions']:''; ?>
                                         </textarea>
                                         <div class="line-box">
                                             <div class="line"></div>
                                         </div>
                                         <div class="term_conditions_error"></div>
                                     </label>
                                 </div>
                                 <div class="col-lg-12 col-md-12">
                                     <label class="form-group">
                                         <h3>Privacy Policy</h3>
                                         <textarea id="privacy_policy" name="privacy_policy">
                                             <?php echo isset($changeinfo[0]['privacy_policy'])?$changeinfo[0]['privacy_policy']:'dcfvgbhj'; ?>
                                         </textarea>
                                         <div class="line-box">
                                             <div class="line"></div>
                                         </div>
                                         <div class="privacy_policy_error"></div>
                                     </label>
                                 </div>
                                 </div>
                             </div>
                         </div>
                         <div class="col-lg-12 ad_new_reqz">
                             <input type="submit" id="savebtntc" name="savebtntc" class="btn-e save_info btn-red submit cnsl-sbt" value="Save">
                         </div>
                     </div>
                 </form>
                 </div>
        </div>
</div> 