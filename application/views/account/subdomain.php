<style>
    .dropify-wrapper{ display:none; height: 200px !important; }
</style> 
<?php 
//$CI = & get_instance();
//$CI->load->library('myfunctions');
$frompage  = $_COOKIE["from"]; 
setcookie("from", " ",time() + (10 * 365 * 24 * 60 * 60),"/");
if ($agency_info[0]['domain_name'] != "") {
    if ($agency_info[0]['ssl_or_not'] == "0") {
        $url = "http://" . $agency_info[0]['domain_name'];
    } else {
        $url = "https://" . $agency_info[0]['domain_name'];
    }
    $info = '<a href="' . $url . '" target="_blank" class="redrct_lnk" data-toggle="tooltip" title="' . $url . '"><i class="fas fa-external-link-alt"></i></a>';
} else {
    $info = "";
}
$profilepicture = (isset($login_user_data[0]['profile_picture']) && $login_user_data[0]['profile_picture'] != '') ? FS_PATH_PUBLIC_UPLOADS_PROFILE.'_thumb/'.$login_user_data[0]['profile_picture'] : '';
if($profilepicture == ""){
    $profile_css = 'style="display: none;"';
    $main_img = 'style="display: flex;"';
}else{
    $profile_css = "";
    $main_img = 'style="display: none;"';
}
$brand_logo = (isset($agency_info[0]['logo']) && $agency_info[0]['logo'] != '') ? FS_PATH_PUBLIC_UPLOADS_USER_LOGO.$login_user_data[0]['id'].'/'.$agency_info[0]['logo'] : '';
if($brand_logo == ""){
    $brnd_css = 'style="display: none;"';
    $brndmain_img = 'style="display: block;"';
}else{
    $brnd_css = "";
    $brndmain_img = 'style="display: none;"';
}
$permission = array(
    "1" => array(
        "slug"=>"add_requests",
        "name" => "Add Request",
        "info" => "User can add/edit projects."),
    "2" => array(
        "slug"=>"add_brand_pro",
        "name" => "Add/Edit Brand Profile",
        "info" => "User can add/edit brand profiles."),
    "3" => array(
        "slug"=>"billing_module",
        "name" => "Billing Module",
        "info" => "User can manage billing modules."),
    "4" => array(
        "slug"=>"file_management",
        "name" => "File Management",
        "info" => "User can create new folder and copy/upload any files here in it using file management feature."),
    "5" => array(
        "slug"=>"white_label",
        "name" => "White Label",
        "info" => "User can manage branding setting."),
    "6" => array(
        "slug"=>"approve_revise",
        "name" => "Approve/Revise",
        "info" => "User can approve or reject projects."),
    "7" => array(
        "slug"=>"review_design",
        "name" => "Review Design",
        "info" => "User can review new uploaded design of projects."),
    "8" => array(
        "slug"=>"comnt_requests",
        "name" => "Comment on Request",
        "info" => "User can comment on projects."),
    "9" => array(
        "slug"=>"del_requests",
        "name" => "Delete Request",
        "info" => "User can delete projects."),
    "10" => array(
        "slug"=>"upload_draft",
        "name" => "Upload Draft",
        "info" => "User can upload draft."),
    "11" => array(
        "slug"=>"add_team_member",
        "name" => "Add team member",
        "info" => "User can add new team member."),
    "12" => array(
        "slug"=>"manage_clients",
        "name" => "Manage clients",
        "info" => "User can manage clients."),
    "13" => array(
        "slug"=>"change_project_status",
        "name" => "Change project status",
        "info" => "User can change project status"),
    "14" => array(
        "slug"=>"assign_designer_to_client",
        "name" => "Designer Assign to Client",
        "info" => "User can assign designer to client."),
    "15" => array(
        "slug"=>"assign_designer_to_project",
        "name" => "Designer Assign to Project",
        "info" => "User can assign designer to particular project."),
    "16" => array(
        "slug"=>"download_file",
        "name" => "Download file",
        "info" => "User can download file."),
    "17" => array(
        "slug"=>"approve_reject",
        "name" => "Approve/Reject Quality of design",
        "info" => "User can approve/reject quality of design."),
    "18" => array(
        "slug"=>"show_all_project",
        "name" => "Access all projects or only projects assigned to him/her",
        "info" => "User can see all projects or only assigned projects to him/her"));

if($frompage == "paywal"){
    $page_url = base_url()."customer/request/add_new_request";
}else{
    $page_url = base_url()."customer/request/design_request";
}
?>
<section class="welcome_wizard">
    <form id="subdomainwiz" action="javascript:void(0)" method="post" enctype="multipart/form-data" data-isagency="<?php echo $is_agency;?>">
        <div class="container">
            <ul class="welcome_progress">
                <li class="active cstm_primary_colr" id="1_bar"></li>
                <?php if($parent_user_plan[0]["is_agency"] == 1){ ?>
                    <li id="2_bar"></li>
                    <li id="3_bar"></li>
                    <li id="4_bar"></li>
                    <li id="5_bar"></li>
                <?php } ?>
                <li id="6_bar"></li>
                <?php if($parent_user_plan[0]["is_agency"] == 1){ ?>
                    <li id="7_bar"></li>
                    <li id="8_bar"></li>
                <?php }else{ ?>
                    <li id="9_bar"></li>
                <?php } ?>
            </ul>
            <fieldset class="gen_sett active" id="1_field" data-chck="1" style="display: block;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">General Settings</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="1">
                        <div class="welcome_step_ouetr">
                            <div class="form-group company_name_input">
                                <input class="form-control entr_compny_name" tabindex="1" autofocus type="text" pattern="[a-z A-Z 0-9]{1,}" maxlength="55" name="company_name" placeholder="Enter your company name" value="<?php echo isset($login_user_data[0]["company_name"])?$login_user_data[0]["company_name"]:""; ?>">
                                <em class="welcome_sub_info"><strong>Example</strong> (google,apple)</em>
                            </div>
                            <div class="form-group text-center upload_profile_pic">
                                <input type="file" onChange="validateAndUpload(this,'','welcome');" tabindex="2"  accept="image/jpeg,image/jpg,image/png,image/gif" class="form-control dropify" name="profile" data-plugin="dropify" id="onboardingpic" style="display: none" hidden/>
                                <div class="welcome_profile_pic">
                                   <a class="setimg-box" href="javascript:void(0)"  onclick="$('.dropify').click();" for="profile">
                                    <div class="custom_pic_file imagemain" <?php echo $main_img; ?>>
                                        <span>Add Your <br> Photo</span>
                                        <i class="fas fa-plus"></i>
                                    </div>
                                    <div class="imagemain3" <?php echo $profile_css; ?>>
                                        <figure class="setimg-box33">
                                            <img id="image3" src="<?php echo $profilepicture; ?>" class="img-responsive telset33">
                                                <span class="change_profile_image">
                                                    Change <br>Avatar
                                                </span>
                                        </figure>
                                    </div>
                                   </a>
                                </div>
                                <div class="validation_err"></div>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" pattern="\d*" tabindex="3" maxlength="12" minlength="10" placeholder="9876543210" name="phone" class="form-control" value="<?php echo isset($login_user_data[0]["phone"])?$login_user_data[0]["phone"]:""; ?>">
                            </div>
                            <div class="form-group">
                                <label>Website URL</label>
                                <input type="url" tabindex="4" placeholder="Example: (https://graphicszoo.com)" maxlength="100" name="website" class="form-control" value="<?php echo isset($login_user_data[0]["url"])?$login_user_data[0]["url"]:""; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="brand_sett" id="2_field" data-chck="2" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Branding Settings</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="welcome_step_ouetr">    
                            <div class="welcome_branding">
                                <div class="col-md-4">
                                    <input type="hidden" name="step" value="2">
                                    <div class="left_branding_bar">
                                        <div class="form-group">
                                            <label>Upload your logo:</label>
                                            <input type="file" onChange="validateAndUpload(this,'brand_logo','welcome');"  accept="image/*" class="form-control dropify_brand" name="brand_logo" data-plugin="dropify" id="brand_logo" style="display: none" hidden/>
                                            <div class="upload_brand_logo">
                                                <a class="setimg-box" href="javascript:void(0)"  onclick="$('.dropify_brand').click();" for="brand_logo">
                                                <div class="brand_logo_icon" <?php echo $brndmain_img; ?>>
                                                    <i class="fas fa-plus"></i>
                                                    <span>Drop your file here </span>
                                                </div>
                                                <div class="uploaded_brnd_fl" <?php echo $brnd_css; ?>>
                                                    <figure class="setimg-box33">
                                                        <img id="brand_logoimg" src="<?php echo $brand_logo; ?>" class="img-responsive telset33">
                                                        <span class="setimgblogcaps">Change <br>Avatar
                                                        </span>
                                                    </figure>
                                                </div>
                                                </a>
                                            </div>
                                            <div class="validation_err"></div>
                                        </div><!--form-group -->
                                        <input type="hidden" name="confirmed_arecord" id="confirmed_arecord" class="confirmed_arecord" value="1"/>
                                        <div class="form-group choose_domain">
                                            <div class="domain_subdomain">
                                                <div class="form-group-b">
                                                    <p class="toggle-text">Use own domain: <span class="domain_info" data-toggle="modal" data-target="#setmaindomain" id="domain_info_pop"><i class="fas fa-info-circle"></i></span>
                                                        <label class="form-group switch-custom-usersetting-check">
                                                            <input type="checkbox" name="slct_domain_subdomain" class="slct_domain_subdomain" id="slct_domain_subdomain" <?php echo (isset($agency_info[0]['is_main_domain']) && $agency_info[0]['is_main_domain'] == "1") ? "checked" : "" ?>/>
                                                            <label for="slct_domain_subdomain"></label> 
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="alternate-domain">

                                                <div class="subdomain_nm" style="display:none"> 
                                                   
                                                    <label class="form-group">
                                                    <p class="domain_https">https://</p>
                                                        <div class="label-txt label-active clint_cll">Sub Domain Name: <?php echo $info; ?></div>
                                                        <input type="text" class="form-control domain_url" name="sub_domain_name" minlength="1"  maxlength="30" pattern="[a-z A-Z 0-9]+[a-z]{1,}" id="sub_domain_name" value="<?php echo (isset($agency_info[0]['domain_name']) && $agency_info[0]['is_main_domain'] == "0") ? strstr($agency_info[0]['domain_name'], '.', true) : '' ?>">
                                                        <p class="domain_ex">.graphicszoo.com</p> 
                                                    </label>
                                                    <div class="validation"></div>
                                                </div>
                                                <div class="domain_nm" style="display:none"> 
                                                    <p class="domain_https"><?php echo (isset($agency_info[0]['ssl_or_not']) && $agency_info[0]['ssl_or_not'] == 1) ? "https://" : "http://" ?></p>
                                                    <label class="form-group">
                                                        <div class="label-txt label-active clint_cll">Domain Name: <?php echo $info; ?></div>
                                                        <input type="text" class="form-control domain_url" name="domain_name" id="domain_name" minlength="1"  maxlength="30" pattern="[A-Z a-z 0-9]+\.[A-Z a-z]{2,}$"  value="<?php echo (isset($agency_info[0]['domain_name']) && $agency_info[0]['is_main_domain'] == "1") ? $agency_info[0]['domain_name'] : '' ?>">
                                                     </label>
                                                    <div class="validation"></div>
                                                </div>

                                            </div>
                                        </div><!--form-group -->

                                        <div class="form-group brand-color">
                                            <label>Primary Brand Color</label>
                                            <input type="text" id="d_primary_color" class="form-control sel_colorpicker" name="primary_color" value="<?php echo (isset($agency_info[0]['primary_color']) && $agency_info[0]['primary_color'] != '') ? $agency_info[0]['primary_color'] : '#e42547' ?>">
                                            <label class="errspan" for="d_primary_color"><span class="bgcolor_primary cstm_primary_colr" style="background-color:<?php echo $themePrimaryColor; ?>;"></span></label>
                                        </div><!--form-group -->
                                        <div class="form-group brand-color">
                                            <label>secondary Brand Color</label>
                                            <input type="text" id="d_secondary_color" class="form-control sel_colorpicker" name="secondary_color"  value="<?php echo (isset($agency_info[0]['secondary_color']) && $agency_info[0]['secondary_color'] != '') ? $agency_info[0]['secondary_color'] : '#1a3148' ?>">
                                            <label class="errspan" for="d_secondary_color"><span class="bgcolor_secondary cstm_sec_btn" style="background-color:<?php echo $secondary_color; ?>;"></span></label>
                                        </div><!--form-group -->
                                        <div class="up-later-text">You have the option to change or update later.</div>
                                    </div>
                                </div><!--col-md-4 -->
                                <div class="col-md-8">
                                    <div class="welcome_screen">
                                        <div class="screen_outer">
                                            <div class="screen_head">
                                                <div class="col-md-3">
                                                    <div class="window_left_head">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/window-dot.png" class="img-responsive">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="window_url">
                                                        <div class="url_txt"><?php echo ($url != "")?$url:"https://graphicszoo.com"; ?></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 hidden-xs">
                                                    <div class="window_right_head text-right">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/window_tabs.png" class="img-responsive">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="browser_out">
                                                    <div class="web_header">
                                                        <div class="col-md-3">
                                                            <div class="web_logo">
                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/web_logo.png" class="img-responsive">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="web_nav">
                                                                <ul>
                                                                    <li class="cstm_primary_colr"></li>
                                                                    <li class="cstm_primary_colr"></li>
                                                                    <li class="cstm_primary_colr"></li>
                                                                    <li class="cstm_primary_colr"></li>
                                                                    <li class="cstm_primary_colr"></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="web_user">
                                                                <span><?php echo $login_user_data[0]["first_name"]; ?></span>
                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/web_user.png" class="img-responsive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="web_page">
                                                        <div class="web_top_bar">
                                                            <div class="col-md-6 text-left">
                                                                <h2 class="agecy_cmpny_name"><?php echo (isset($login_user_data[0]["company_name"]) && $login_user_data[0]["company_name"] != "")?$login_user_data[0]["company_name"]:"Company Name"; ?></h2>
                                                            </div>
                                                            <div class="col-md-6 text-right">
                                                                <button class="cstm_primary_btn cstm_primary_colr" type="button"><span></span></button>
                                                                <button class="cstm_sec_btn" type="button"><span></span></button>
                                                            </div>
                                                        </div>
                                                        <div class="web_project_list">
                                                            <div class="col-xs-6 col-sm-3 col-md-3">
                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/web_add_new.jpg" class="img-responsive">
                                                            </div>
                                                            <?php for($i=0; $i<=6; $i++){ ?>
                                                            <div class="col-xs-6 col-sm-3 col-md-3">
                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/web_project_img.jpg" class="img-responsive">
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="text-center">
                                                            <button class="cstm_primary_btn cstm_primary_colr" type="button"><span></span></button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--col-md-8 -->
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="syst_sett" id="3_field" data-chck="3" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">System Usage</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="3">
                        <div class="welcome_step_ouetr">
                            <div class="usage_field">
                                <h3 class="text-center">How would like to you use this system</h3>
                                <div class="choose_purpose">
                                    <div class="choose_item">
                                        <input id="for_client" type="radio" name="client" value="1" class="clnt_typ" checked="">
                                        <span class="wlcom_check"></span>
                                        <label for="for_client">I would like to sell the design services to my customers.</label>
                                    </div>                            
                                    <div class="choose_item">
                                        <input id="for_own" type="radio" name="client" value="2" class="clnt_typ">
                                        <span class="wlcom_check"></span>
                                        <label for="for_own">I  will use this system for my internal team.</label>
                                    </div>                            
                                </div>
                            </div>                         
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="paymnt_sett" id="4_field" data-chck="4" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Payment</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="4">
                        <div class="welcome_step_ouetr">
                            <div class="payment_mode">
                                <div class="form-group com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/wel_pmt.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Payment Mode
                                        <em>Enable these settings to sync stripe payments and auto upgrade your account for a complete streamlined system.</em>
                                    </h3>
                                </div>
                                <div class="form-group text-center">
                                    <label>Do you want to receive payment online or you want to manage this manually?</label>
                                    <div class="pmt_check">
                                        <div class="pmt_check_item">
                                            <input type="radio" name="payment_mode" value="1">
                                            <label><i class="fas fa-check"></i><span>yes</span></label>
                                        </div>	
                                        <div class="pmt_check_item">
                                            <input type="radio" name="payment_mode" value="0">
                                            <label><i class="fas fa-times"></i><span>no</span></label>
                                        </div>	
                                    </div>
                                </div>
                                <div class="form-group text-center strp_acnt" style="display:none">
                                    <label>You Need To Connect Your Account With Stripe. After completing on-boarding process you will get the button to connect with stripe.</label>
<!--                                    <div class="pmt_check">
                                        <div class="pmt_check_item">
                                            <input type="radio" name="stripe_acc" value="yes">
                                            <label><i class="fas fa-check"></i><span>yes</span></label>
                                        </div>	
                                        <div class="pmt_check_item">
                                            <input type="radio" name="stripe_acc" value="no">
                                            <label><i class="fas fa-times"></i><span>no</span></label>
                                        </div>	
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="subs_sett" id="5_field" data-chck="5" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Subscription</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="5">
                        <input type="hidden" name="f_subs_id" value="" id="f_subs_id">
                        <div class="welcome_step_ouetr">
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/Subscription_ico.png" class="img-responsive">
                                    <h3 class="wel_sub_heading subs_text">New Subscription
                                        <em>Complete the form below to add new subscription.</em>
                                    </h3>
                                </div>
<!--                                <div class="col-md-6">
                                    <div class="form-group switch_tog">
                                        <label class="payment_mod_txt">Offline Payment</label>
                                        <div class="form-group switch-custom-usersetting-check">
                                            <input type="checkbox" name="subs_payment_mode" class="form-control" id="f_subs_payment_mode" disabled="">
                                            <label for="f_subs_payment_mode">
                                            </label>
                                        </div>
                                    </div>
                                </div>-->
                                <input type="hidden" name="subs_payment_mode" value="0">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan Type</label>
                                        <select class="form-control f_subs_plan_type_name" name="subs_plan_type_name" id="f_subs_plan_type_name">
                                            <option value="one_time">Request Based</option>
                                            <option value="all">Subscription Based</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if ($agency_info[0]['shared_designer'] == 1) { ?>
                                    <div class="col-md-6 f_pln_user_type">
                                        <div class="form-group">
                                            <label>User Type</label>
                                            <select class="form-control" name="subs_plan_user_type" id="f_subs_user_type">
                                                <option value="1">Shared</option>
                                                <option value="0">Dedicated</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <input type="hidden" name="subs_plan_user_type" value="0" id="f_subs_user_type">
                                <?php } ?>
<!--                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan ID</label>
                                        <input class="form-control" name="plan_id" autofocus type="text" placeholder="Enter the ID name">
                                    </div>
                                </div>-->
                                <div class="col-md-6 stripe_is_sec" style="display:none">
                                    <div class="stripe_pln_sec" style="display:none">
                                        <div class="form-group">
                                            <label>Stripe ID<span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> <strong>Note:</strong> <span>You need to create this product for subscription based plans in stripe first and use the plan ID here. For more info <a href="https://drive.google.com/file/d/1vcaVS_NB0msCWbfkI22aOIrpj-UZqZqC/view" target="_blank"class="demo_stripe_link">click here </a></span></div></div></label>
                                            <select class="form-control" name="subs_planid" id="f_subs_planid" data-pay="1" value="">
                                                <?php
                                                if (!empty($clientplans)) {
                                                    foreach ($clientplans as $plan) {
                                                        ?>
                                                        <option value="<?php echo $plan['id']; ?>"><?php echo $plan['plan_name']; ?></option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mannuly_subs_pln_sec" style="display:none">
                                        <div class="form-group">
                                            <label>Plan ID<span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"><strong>Note:</strong> <span>Plan id should be unique.</span></div></div></label>
                                            <input type="text" name="subs_plan_id" class="form-control" id="f_subs_plan_id" data-pay="1" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Plan Name</label>
                                        <input class="form-control" name="subs_plan_name" autofocus type="text" pattern="[a-z A-Z 0-9 $-()/]+[a-z]{1,}" maxlength="30" placeholder="Plan Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group currency_icon">
                                        <label>Plan Price</label>
                                        <span class="plan_prc_currncy">$</span><input class="form-control" autofocus type="text" pattern="[0-9]{1,}" name="subs_plan_price" maxlength="8" placeholder="Plan Price">
                                    </div>
                                </div>
                                <div class="onetimepln">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Number of Requests <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Total Number of requests user can add.</div></div></label>
                                            <input type="number" name="total_requests" class="form-control" id="f_subs_numof_req" value="" min="1">
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-6 f_amount_cycle" style="display:none">
                                    <div class="form-group">
                                        <label>Payment Circle</label>
                                        <select class="form-control" name="subs_plan_type">
                                            <option value="monthly">Monthly</option>
                                            <option value="yearly">Yearly</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group switch_tog">
                                        <label>Activate Plan</label>
                                        <div class="form-group switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_subs_plan_active" id="actv_plan_1" class="active_plan">
                                            <label for="actv_plan_1">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/permission_ico.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Permissions</h3>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_file_sharing" id="file_sharing_1" class="f_file_sharing">
                                            <label for="file_sharing_1">
                                            </label>
                                            <span>File Sharing Capabilities</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_file_management" id="file_mngmnt" class="f_file_management">
                                            <label for="file_mngmnt">
                                            </label>
                                            <span>File Management</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group switch_tog">
                                        <div class="switch-custom-usersetting-check">
                                            <input type="checkbox" name="f_apply_coupon" id="acpt_cpn" class="acpt_cpn">
                                            <label for="acpt_cpn">
                                            </label>
                                            <span>Accept Coupons</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/plan_ftr.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Plan Features</h3>
                                </div>
                                <div class="form-group">
                                    <div class="input_fields_container"></div>
                                    <div class="plan_feature_field">
                                        <input class="form-control plan_features" type="text" name="subs_plan_features[]" maxlength="55" pattern="[a-z A-Z 0-9 $@!#%^/&*()]+[a-z]{1,}" placeholder="Enter the plan features">
                                        <a class="subs-btn add_more_subscription"><i class="fas fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="team_sett" id="6_field" data-chck="6" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Team Managment</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="step" value="6">
                        <div class="welcome_step_ouetr">
                            <div class="com_web_heading">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/add_user.png" class="img-responsive">
                                <h3 class="wel_sub_heading">Add New User
                                    <em>Complete the form below to add new user.</em>
                                </h3>
                            </div>
                           
                            <div class="add_user_table">
                                <div class="adduser_item">
                                    <div class="adduser_membrr">
                                        <div class="not_allow"></div>
                                    <div class="user_field_row">
                                        <input type="hidden" name="invalid_email" class="invalid_email" value="0">
                                        <div class="form-group user_fields">
                                            <label>Select The User Role</label>
                                            <select class="form-control member_role" name="user_role[]">
                                                <option value="manager">manager</option>
                                                <option value="customer">client</option>
                                                <option value="designer">designer</option>
                                            </select>
                                        </div>
                                        <div class="form-group user_fields">
                                            <label>First Name*</label>
                                            <input class="form-control" type="text" name="first_name[]" pattern="[a-z A-Z 0-9]+[a-z]{1,}" maxlength="30" placeholder="Enter the first name">
                                        </div>
                                        <div class="form-group user_fields">
                                            <label>Last Name*</label>
                                            <input class="form-control" type="text" name="last_name[]" pattern="[a-z A-Z 0-9]+[a-z]{1,}" maxlength="30" placeholder="Enter the last name">
                                        </div>
                                        <div class="form-group user_fields">
                                            <label>Email*</label>
                                            <input class="form-control chk_usr_mail" type="email" name="email[]" pattern="[a-z 0-9 -_.]+[a-z]+@[a-z 0-9]+\.[a-z]{2,}$" maxlength="30" placeholder="Enter the email">
                                            <div class="validation_err"></div>
                                        </div>
                                        <div class="form-group user_fields">
                                            <label>Password*</label> 
                                            <div class="gen_pwd"><span>Randomly generated password</span>
                                                <div class="switch-custom-usersetting-check">
                                                    <input type="checkbox" name="genrate_password[]" id="password_gen" class="password_ques user_prms_switch" checked="">
                                                    <label for="password_gen">
                                                    </label>
                                                    <input type="hidden" value="0" name="genrate_password[]">
                                                </div>
                                                <div class="form-group create_password" style="display:none">
                                                <label class="form-group">
                                                    <input type="password" name="password[]" class="form-control password-val" value="" minlength="8" maxlength="16" placeholder="Enter the password"/>
                                                    <div class="change_pss_mode"><i class="fa fa-eye" aria-hidden="true"></i></div>
                                                    <div class="validation_err"></div>
                                                </label>
                                            </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="users_permission">
                                        <div class="com_web_heading">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/permission_ico.png" class="img-responsive">
                                            <h3 class="wel_sub_heading">Permissions</h3>
                                            <div class="switch-custom-usersetting-check view_membr_prmsn_only">
                                                <input type="checkbox" name="view_only[]" id="view_only" class="view_only user_prms_switch" checked="">
                                                <label for="view_only"></label><span>View Only</span>
                                            </div>
                                        </div>
                                        <div class="permission_list" style="display:none">
                                            <ul>
                                                <?php foreach ($permission as $k => $per){  ?>
                                                <li>
                                                    <div class="permission-checked">
                                                        <input type="checkbox" name="<?php echo $per["slug"].'[]';?>" id="<?php echo $per["slug"];?>" class="user_prms_switch">
                                                       <label  for="<?php echo $per["slug"];?>"><?php echo $per["name"]; ?><i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-original-title="<?php echo $per["info"]; ?>"></i>
                                                           <span class="checked_mark"></span>
                                                       </label>
                                                        <input type='hidden' value='0' name="<?php echo $per["slug"].'[]';?>">
                                                    </div>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="client_subscription form-group" style="display:none">
                                        <label>
                                                Subscription Plan
                                        </label>
                                            <select class="form-control clintsubscription" name="requests_type[]">
                                                <option value="">Select Subscription Plan</option>
                                            </select>
                                            <p class="note_text fill-sub"><span>Please choose subscription plan for this user. Manage your plans in subscription module.</span></p>
                                            
                                    </div>
                                    </div>
                                </div>
                                 <div class="input_team_container"></div>
                                <button class="add_member_btn add_more_member"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/add_user.png" class="img-responsive"> Add New Member</button>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="email_sett" id="7_field" data-chck="7" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Email Settings</h2>
                    <span>You can update these settings in preferences area any time.</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="7">
                        <div class="welcome_step_ouetr">
                            <div class="subscription_row">
                                <div class="com_web_heading">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/mail-setting.png" class="img-responsive">
                                    <h3 class="wel_sub_heading">Email Settings
<!--                                        <em>Please fill the complete form before sending test email,email settings you might need from your email provider</em>-->
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Reply Name</label>
                                        <input class="form-control" autofocus type="text" name="reply_name" pattern="[a-z A-Z 0-9 @/.]+[a-z]{1,}" maxlength="30" placeholder="Enter the name">
                                        <em>Your name as you would like in reply</em>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Reply Email</label>
                                        <input class="form-control" autofocus type="email" name="reply_email" pattern="[a-z 0-9-_.]+[a-z]+@[a-z]+\.[a-z]{2,}$" maxlength="30" placeholder="Enter the email">
                                        <em>Your email where user want to reply your message.</em>
                                    </div>
                                </div>
                            </div>
                            <div class="subscription_row">
                                <div class="com_web_heading smtp_heading show-hidelabel">
                                    <h3 class="wel_sub_heading">Enter SMTP details <div class="switch-custom-usersetting-check is_smtp_enable">
                                            <input type="checkbox" name="is_smtp_enable" id="is_smtp_enable" class="is_smtp">
                                            <label for="is_smtp_enable"></label>
                                        </div>
                                        <em>Use your own email address to send notifications to your users</em>
                                    </h3>
                                </div>
                                <div class='port_settings' style='display:none'>
                                    <div class="col-md-12">
                                        <p  class="fill-sub">You can send your users email through your own SMTP service. For more details about custom SMTP <a class="theme-color" href="https://www.youtube.com/watch?v=5SbC2fCf5qo" target="_blank">check cpanel instruction's</a> OR <a class="theme-color" href="https://in.godaddy.com/help/configuring-mail-clients-with-cpanel-email-8861" target="_blank">godaddy instruction's</a>
					according your hosting. If you're using Gmail for your email services, enable the switch for less secure apps in <a href="https://myaccount.google.com/lesssecureapps" target="_blank">your account</a>.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Name*</label>
                                            <input class="form-control" name="from_name" autofocus type="text" pattern="[a-z A-Z 0-9 @/.]+[a-z]{1,}" maxlength="30" placeholder="Enter the name">
                                            <em>Choose your sender name as you would like it to appear in messages that you send. Example: John</em>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Email*</label>
                                            <input class="form-control" name="from_email" autofocus type="email" pattern="[a-z 0-9_-.]+[a-z]+@[a-z]+\.[a-z]{2,}$" maxlength="30" placeholder="Enter the email">
                                            <em>Your email where user want to reply your message.</em>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Port*</label>
                                            <input class="form-control" name="port" pattern="[0-9]{1,}" autofocus type="text" maxlength="4" placeholder="Enter the port">
                                            <em>The port number used by the incoming mail server, common port numbers 143,465.</em>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Host Name*</label>
                                            <input class="form-control" name="host" pattern="[a-z A-Z 0-9/.]+[a-z]{1,}" autofocus type="text" maxlength="30" placeholder="Enter the host name">
                                            <em>The host name of the incoming mail server, such as mail.example.com.</em>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email Address*</label>
                                            <input class="form-control" name="host_username" pattern="[a-z0-9_-.]+[a-z]+@[a-z]+\.[a-z]{2,}$" autofocus type="email" maxlength="30" placeholder="Enter the email">
                                            <em>Please Enter your email address.</em>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password*</label>
                                            <input class="form-control" name="host_password" pattern="[a-z A-Z 0-9 @!#$%^&*()]{1,}" autofocus type="password" placeholder="Enter the password">
                                            <em>Please Enter your email password.</em>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email Secure * </label>
                                            <select class="form-control" name="mail_secure">
                                                <option value="ssl">SSL</option>
                                                <option value="tls">TLS</option>
                                            </select>
                                            <em>Does the incoming mail server support SSL (Secure Sockets Layer) or TLS (Transport Layer Security) encryption?</em>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="stripe_sett" id="8_field" data-chck="8" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Create new stripe account</h2>
                    <span>You can  update these settings in prefrecnces ares any time</span>
                </div>
                <div class="val_err"></div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <input type="hidden" name="step" value="8">
                        <div class="welcome_step_ouetr">
                            <div class="strip_mode text-center">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/strip_logo.png" class="img-responsive">
                                <p>Do you want to receive payment online or you want to manage this manually?</p>
                                <a class="usr_connect_stripe" target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&scope=read_write&client_id=ca_Feq9KZiLMta2j2RtQOerxDJU4QK8z7Ai"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/strip_btn.png" class="img-responsive"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="getstat_sett" id="9_field" data-chck="9" style="display: none;">
                <div class="welcome_header text-center">
                    <h2 class="welcome_heading">Get Started</h2>
                </div>
                <div class="get_started_sec">	
                    <div class="row">
                        <div class="col-md-6">
                            <div class="get_started_img">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/account/welcome_wizard/started_img.jpg" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="get_started_info">
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat</p>
                                <a class="lets_go_started" href="<?php echo $page_url; ?>">Let’s Go</a>
                            </div>
                        </div>
                    </div>
                </div>	
            </fieldset>
            <div class="save_btn text-center">
                <button type="button" class="save_next back" id="back" style="display:none;">Back</button>
                <button type="submit" class="save_next save" id="next">save & next</button>
<!--                <button type="button" class="save_next finish" id="finish" style="display:none;">Finish</button>-->
            </div>
        </div>
    </form>
</section>
<div class="modal fade slide-3 model-close-button in similar-prop" id="setmaindomain_msg" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="cli-ent-model-box">
            <header class="fo-rm-header">
               <h2 class="popup_h2 del-txt">Point your domain</h2>
            </header>
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/dns.png">
               <p><br/></p>
               <p>You can use your own domain or subdomain and the complete application with run there. For this, you will have to point your domain to <b>A records</b> to our server which is <b>3.17.33.76</b>.
                  For further instructions <a href="javascript:void(0)" target="_blank" class="domain_inst site_btn" id="domain_inst">click here</a> or you can contact us and we will be here to support you.</p>
            </div>
             <div class="onborng_cnfrm">
                <button type="button" class="btn arec_ntcnfrm_btn" data-dismiss="modal" style="color:#652121">Cancel</button>
                <button type="button" class="btn arec_cnfrm_btn" data-dismiss="modal" style="color:#652121">Confirm</button>
            </div>
         </div>
      </div>
   </div>
</div>
<link href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/bootstrap-colorpicker.min.css" rel="stylesheet">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/bootstrap-colorpicker.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/account/agency_setting.js"></script>
<script>
    var is_agency = "<?php echo $parent_user_plan[0]["is_agency"]; ?>";
$(function () {
    $('.sel_colorpicker').colorpicker( {
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 150,
                maxTop: 150
            },
            hue: {
                maxTop: 150
            },
            alpha: {
                maxTop: 150
            }
        }

    });
});
</script>