<style>
.pad{
	padding:5px;
}
.colorblack{
	color:#000;
	padding-bottom:0px;
	font-size:15px;
}
.nav-tab-pills-image ul li .nav-link:hover{
	border-bottom:unset !important;
}
.nav-tab-pills-image ul .nav-item+.nav-item{
	margin-left:9px;
}
.nav-tab-pills-image ul li .nav-link:focus
{
	border-bottom:unset !important;
}
</style>

<div class="Container-fluid">
	<div class="nav-tab-pills-image">
		<ul class="nav nav-tabs"  style="border:none;">                      
			<li class="nav-item" style="color:#fff;background:#ccc;border-radius: 12px 0px 0px 12px;">
				<a class="nav-link" style="color:#2f4458;background: #ccc;font-weight: 600;border-radius: 12px 0px 0px 12px;" href="<?php echo base_url(); ?>customer/setting-edit/"  style="">
					Edit Profile
				</a>
			</li>
			<li class="nav-item active" style="background:#2f4458;margin-left:6px;color:#fff;border-radius: 0px 12px 12px 0px;">
				<a class="nav-link"  style="background:#2f4458;color:#fff;border-radius: 0px 12px 12px 0px" href="<?php echo base_url(); ?>customer/setting-edit/change_password">
					Change Password
				</a>
			</li>
			 
		</ul>
		
	</div>
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="background-color:#fff;">
		<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 col-md-offset-3 col-lg-offset-3" style="padding:15px;">
		<?php if($this->session->flashdata('message_error') != '') {?>				

			   <div class="alert alert-danger alert-dismissable">

					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

					<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				

				</div>

			   <?php }?>

			   <?php if($this->session->flashdata('message_success') != '') {?>				

			   <div class="alert alert-success alert-dismissable">

					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

					<strong><?php echo $this->session->flashdata('message_success');?></strong>

				</div>

			   <?php }?>
		<form method="post" action="">
			<h2 style="text-align:center;">Change Password</h2>
			<div class="pad">
				<p class="colorblack">Old Password</p>
				<input type="password" placeholder="Old Password" name="old_password" class="form-control"/>
			</div>
			<div class="pad">
				<p class="colorblack">New Password</p>
				<input type="password" placeholder="New Password" name="new_password" class="form-control"/>
			</div>
			<div class="pad">
				<p class="colorblack">Confirm Password</p>
				<input type="password" placeholder="Confirm Password" name="confirm_password" class="form-control"/>
			</div>
			<div class="pad" >	
				<input  style="background-color:#ec1c41;color:#fff" type="submit" name="comformbtn" value="Submit" class="form-control"/>
			</div>
		</form>
		</div>
	</div>
</div>	