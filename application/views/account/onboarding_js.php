<!DOCTYPE html>
<?php
$CI = & get_instance();
$CI->load->library('account/s_myfunctions');
$reQuestUrl = $this->router->fetch_method();
$get_method =$this->router->fetch_method();
$CI->load->library('account/S_customfunctions');
$this->load->view('front_end/variable_css');
$CI->load->helper('account/s_notifications');
$helper_data = helpr();
     // $helper_data = $CI->load->helper('account/image_helper');
$profile = $helper_data['profile_data']; 
// echo "<pre/>";print_r($profile);exit;
//echo "<pre>";print_r($_SESSION);exit;
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=7,9,10">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Language" content="en">
    <meta name="google" content="notranslate">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Proof Portal | <?php echo $page_title; ?></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700&display=swap&subset=devanagari,latin-ext" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $custom_favicon; ?>">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/reset.css" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/adjust.css" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/editor/editor.css" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'css/account/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'css/account/responsive.css'); ?>" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.css" rel="stylesheet">
    <link rel="preload" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.theme.default.min.css" as="style" onload="this.onload = null; this.rel = 'stylesheet'">
    <link rel="stylesheet" type="text/css" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/jquery.fancybox.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/css/toastr.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!--<link rel="stylesheet" href="<?php //echo FS_PATH_PUBLIC_ASSETS;             ?>js/js-card.css">-->
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'css/custom_style_for_all.css'); ?>" rel="stylesheet">
<!--    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/introjs.css" rel="stylesheet">
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/intro.js"></script>-->
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/bootstrap.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/jquery.fancybox.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/account/s_all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
    <?php
    $url = current_url();
    $u_rl = explode("/", $url);
    $messageurl = substr($url, strrpos($url, '/') + 1);
    $bodyclassondotcomnt = (in_array('project_image_view',$u_rl)) ? 'view_file_body' : '';
    if ($messageurl == 'setting-view' || $messageurl == 'user_setting') {
        ?>
        <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/chosen.css" rel="stylesheet">
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/chosen.jquery.js"></script>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/init.js"></script>
    <?php } ?>

    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/leader-line.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/anim-event.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/in-view.min.js"></script>
    
    <script>
        var baseUrl = "<?php echo site_url(); ?>";
        $rowperpage = <?php echo LIMIT_ALL_LIST_COUNT; ?>;
        $assets_path = "<?php echo FS_PATH_PUBLIC_ASSETS ?>";
        $uploadAssestPath = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS ?>";
        $uploadbrandPath = "<?php echo FS_PATH_PUBLIC_UPLOADS_SAAS . 'brand_profile/' ?>";
        $uploadcustom_files = "<?php echo FS_PATH_PUBLIC_UPLOADS_SAAS . 'custom_files/' ?>";
        is_trail = "<?php echo $login_user_data[0]['is_trail']; ?>";
        is_logged_in = "<?php echo $login_user_data[0]['is_logged_in']; ?>";
        is_without_pay_user = "<?php echo $parent_user_data[0]['without_pay_user']; ?>";
        total_active_requests = "<?php echo TOTAL_ACTIVE_REQUEST; ?>";
        jqueryarray = <?php echo json_encode(STATE_TEXAS); ?>;
        subscription_plan = '<?php echo json_encode($all_tier_prices); ?>';
        new_plans = <?php echo NEW_PLANS; ?>;
        $plan_price = '';
        $discount_amount = 0;
        $tax_amount = 0;
        $tax_value = 0;
        $discount_notes = '';
        $is_saas = '<?php echo $login_user_data[0]['is_saas'];?>';
        new_plans = <?php echo NEW_PLANS; ?>;
        var profilepath = "<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>";
        if (subscription_plan) {
            subscription_plan = jQuery.parseJSON(subscription_plan);
        }      
    </script>