<a href="<?php echo base_url(); ?>customer/request/design_request" class="float-xs-left content-title-main" style="color: #333333;">Chat</a>
<a href="<?php echo base_url(); ?>customer/request/view_designer" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Designer Profile</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147 !important; } 
.pinktext { color: #ff0024;; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
font-size: 31px !important;
padding: 8px 0px !important;
font-weight: 600 !important;
letter-spacing: -3px !important;
}
.projecttitle{
font-size: 25px;
padding-bottom: 0px;
text-align: left;
padding-left: 20px;
}
.trborder{
border: 1px solid #000;
background: unset;
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
color: #ec4159;
font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link {
color:#1a3147;
font-weight:600;
padding: 7px 25px;
}
input.empty {
font-family: FontAwesome;
font-style: normal;
font-weight: normal;
text-decoration: inherit;
}
.hiddenfile {
width: 0px;
height: 0px;
overflow: hidden;
}
</style>
<div class="content">

<div class="row">
<div class="col-md-12" style="margin-top:70px;">

<div class="nav-tab-pills-image">

<div class="tab-content">
<div class="tab-pane active content-datatable datatable-width" id="chat_with_client" role="tabpanel">


<div style="padding: 0px 10px;">

<div class="row rownopadding1 justify-content-center" style="box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.2); border-radius: 8px;">

<div class="col-md-4 chatleftpart">

<div class="searchmain">
<input class="form-control empty input" id="myInput" type="text" name="search_request" style="border-radius: 8px 0px 0px 0px !important;" placeholder="Search"/>
<span class="icon"><i class="fa fa-search"></i></span>
</div>

<div style="overflow: hidden;">
<div class="chatlist-main myscroller" style="height:598px;">
<?php

$myname = "";
for ($i = 0; $i < sizeof($chat_customer); $i++) {
?>

<div data-roomid="<?php echo $chat_customer[$i]['room_id']; ?>" class="chatlistbox <?php
if ($chat_customer[$i]['room_id'] == $room_no) {
echo 'active';
}
?>">
<div class="avatar">
<?php if ($chat_customer[$i]['image']) { ?>
<img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_customer[$i]['image']; ?>" class="img-responsive" />
<?php } else { ?>
<div class="myprofilebackground" style="width:50px; height:50px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 24px;padding-top: 7px;">
<?php echo $chat_customer[$i]['sort_name']; ?></p></div>
<?php } ?>
</div>
<div class="message-text-wrapper topspacen">
<div class="posttime">Dec 11</div>
<p class="title"><?php echo $chat_customer[$i]['title']; ?></p>
<p class="message graytext"><?php echo substr($chat_customer[$i]['last_message'],0,20); ?></p>
<!--<p class="<?php echo $chat_customer[$i]['status_class']; ?> status"><?php echo $chat_customer[$i]['status']; ?></p>-->
<div class="<?php echo strtolower($chat_customer[$i]['status']); ?>"><i class="fa fa-circle"></i></div>
</div>
<div class="clear"></div>
</div>
<?php } ?>
</div>
</div>
</div>

<div class="col-md-8 chatwithu chatrightpart" style="background-color: #f6f7fb; box-shadow: inset 6px -8px 12px 0 rgba(0, 0, 0, 0.2);">

<div class="chatpage-tit">
<h4><?php
if (isset($chat_customer[0]['chat_name'])) {
echo $chat_customer[0]['chat_name'];
}
?></h4>
</div>

<div class="chat-main">

<?php if (isset($chat)) { ?>

<div class="myscroller2 roomchat messagediv_<?php echo $room_no; ?>" style="height:500px; overflow-y: hidden;">
<?php for ($i = 0; $i < sizeof($chat); $i++) { ?>


<!--<div>-->
<?php if ($_SESSION['user_id'] == $chat[$i]['sender_id']) { ?>


<div class="chatbox2 right">
<!--<div class="avatar">
&nbsp;
</div>-->

<div class="message-text-wrapper messageright">
<p class="description">
<?php 
if($chat[$i]['isimage'] == "1"){
echo '<img src="'.base_url()."uploads/requests/".$chat[$i]['message'].'" />'; 
}else{ 
echo $chat[$i]['message']; 
} ?>
</p>
<p class="posttime">
<?php
$date = strtotime($chat[$i]['created']);
$diff = strtotime(date("Y:m:d H:i:s")) - $date;
$minutes = floor($diff / 60);
$hours = floor($diff / 3600);
if ($hours == 0) {
echo $minutes . " Minutes Ago";
} else {
$days = floor($diff / (60 * 60 * 24));
if ($days == 0) {
echo $hours . " Hours Ago";
} else {
echo $days . " Days Ago";
}
}
?>
</p>
</div>
</div>


<?php } else { ?>

<div class="chatbox left">
<div class="avatar">
<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
</div>

<div class="message-text-wrapper messageleft">

<!--<p class="title">
<?php echo $userdata[$chat[$i]['sender_id']]; ?>
</p>-->
<p class="description">
<?php 
if($chat[$i]['isimage'] == "1"){
echo '<img src="'.base_url()."uploads/requests/".$chat[$i]['message'].'" />'; 
}else{ 
echo $chat[$i]['message']; 
} ?>
</p>
<p class="posttime">
<?php
$date = strtotime($chat[$i]['created']);
$diff = strtotime(date("Y:m:d H:i:s")) - $date;
$minutes = floor($diff / 60);
$hours = floor($diff / 3600);
if ($hours == 0) {
echo $minutes . " Minutes Ago";
} else {
$days = floor($diff / (60 * 60 * 24));
if ($days == 0) {
echo $hours . " Hours Ago";
} else {
echo $days . " Days Ago";
}
}
?>
</p>

</div>
</div>
<?php } ?>
<!--</div>-->

<?php } ?>
</div>

<!--<div class="col-md-12" style="margin-top: 20px;">
<input type='text' class='form-control myText room_chat_text text_<?php echo $room_no; ?>' style="border: 1px solid !important;border-radius: 6px !important;" placeholder="Type a message here" />
<span style="position: absolute;top: 10px;right: 30px;font-size: 18px;" class="pinktext fa fa-send send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>"></span>
</div>-->


<!--<form class="myform" method="post" action="" enctype="multipart/form-data"  style="clear: both;position: absolute;bottom: -7px;z-index: 9999999999999;">

<span class="chatcamera"><i class="fa fa-camera" id="openimageupload"></i></span>
<div class="hiddenfile">
<input name="upload" type="file" id="fileinput" onchange="validateAndUpload(this);"/>
</div>
<input type="hidden" value="<?php echo $room_no; ?>" class="hidden_room_no" name="room_no"/>
<button type="submit" style="margin-top: -16px; margin-right: 15px;display: none;" class="myfilesubmit">Image Submit</button>

</form>
<input type='text' class='form-control myText room_chat_text text_<?php echo $room_no; ?>' style="border: 1px solid; border-radius: 0px !important; height: 60px; padding-right: 30px; padding-left: 60px; box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.2); margin-top: 20px; position: relative; bottom: -20px; left: -20px; right: -20px;" placeholder="Type a message here" />
<span style="margin-top: -15px; margin-right: 15px;" class="chatsendbtn send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>">Send</span>-->



<!--<input  type='text' class='form-control sendtext text_<?php echo $data[0]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px; height: 44px;" placeholder="Reply To Client" />
<span class="chatsendbtn send_request_chat send"
data-requestid="<?php echo $data[0]['id']; ?>"
data-senderrole="customer"
data-senderid="<?php echo $_SESSION['user_id']; ?>"
data-receiverid="<?php echo $data[0]['designer_id']; ?>"
data-receiverrole="designer"
data-sendername="<?php echo $data[0]['customer_name']; ?>">Send
</span>-->

<?php } ?>

</div>

<div class="chatbtn-boxmain">
<form class="myform" method="post" action="" enctype="multipart/form-data"  style="clear: both;position: absolute;bottom: -7px;z-index: 9999999999999;">
<span class="chatcamera"><i class="fa fa-paperclip" id="openimageupload"></i></span>
<div class="hiddenfile">
<input name="upload" type="file" id="fileinput" onchange="validateAndUpload(this);"/>
</div>
<input type="hidden" value="<?php echo $room_no; ?>" class="hidden_room_no" name="room_no"/>
<button type="submit" style="margin-top: -16px; margin-right: 15px;display: none;" class="myfilesubmit">Image Submit</button>
</form>
<input type='text' class='form-control chatlargeinp myText room_chat_text text_<?php echo $room_no; ?>' placeholder="Type a message here" style="border-radius: 0px 0px 8px 0px !important;" />
<span style="margin-right: 15px;" class="chatsendbtn send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>"onclick="send_room_chat();">Send</span>
</div>

</div>

</div>

</div>


</div>
</div>
</div>
</div>
</div>
</div>

</section>
</div>
<script>
$(document).ready(function () {
$("#myInput").on("keyup", function () {
var value = $(this).val().toLowerCase();
$(".chatbox").filter(function () {
$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);

});
$(".chatbox2").filter(function () {
$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);

});
});
});


$('#iconified').on('keyup', function () {
var input = $(this);
if (input.val().length === 0) {
input.addClass('empty');
} else {
input.removeClass('empty');
}
});
// enter keys
function send_room_chat()
{
if ($('.room_chat_text').val() != "") {
var room_no = $(".send_room_chat").attr("data-room");
var msg = $('.room_chat_text').val();
var sender_id = "<?php echo $_SESSION['user_id']; ?>";
var sender_name = $(".send_room_chat").attr('data-sendername');
$.ajax({
type: "POST",
url: "<?php echo base_url(); ?>customer/profile/send_room_message",
data: {"room_id": room_no,
"message": msg,
"sender_id": sender_id},
success: function (data) {
//alert("---"+data);
//alert("Settings has been updated successfully.");
$('.text_' + room_no).val("");
$('.myText').val("");
/*$('.messagediv_' + room_no).append('<div class="chatbox2 right">\
<div class="message-text-wrapper messageright">\
<p class="description">' + msg + '</p>\
<p class="posttime"> Few Minutes Ago </p>\
</div>\
</div>');*/
$(".mCSB_container").append('<div class="chatbox2 right">\
<div class="message-text-wrapper messageright">\
<p class="description">' + msg + '</p>\
<p class="posttime"> Few Minutes Ago </p>\
</div>\
</div>');
/*$('.messagediv_'+room_no).append('<div class="chatbox2 right"><div class="message-text-wrapper messageright"><p class="description">'+msg+'</p><p class="posttime">2 Minutes Ago</p></div></div>');*/
$(".myscroller2").mCustomScrollbar("update");
$(".myscroller2").mCustomScrollbar("scrollTo","bottom");
/*$('#mCSB_3_container').scrollTop($('#mCSB_3_container')[0].scrollHeight);*/
}
});
}
}
$(document).on("keypress", ".myText", function (e)
{
if (e.which == 13)
{
if ($('.room_chat_text').val() != "") {
var room_no = $(".send_room_chat").attr("data-room");
var msg = $('.room_chat_text').val();
var sender_id = "<?php echo $_SESSION['user_id']; ?>";
var sender_name = $(".send_room_chat").attr('data-sendername');
$.ajax({
type: "POST",
url: "<?php echo base_url(); ?>customer/profile/send_room_message",
data: {"room_id": room_no,
"message": msg,
"sender_id": sender_id},
success: function (data) {
//alert("---"+data);
//alert("Settings has been updated successfully.");
$('.text_' + room_no).val("");
$('.myText').val("");
/*$('.messagediv_' + room_no).append('<div class="chatbox2 right">\
<div class="message-text-wrapper messageright">\
<p class="description">' + msg + '</p>\
<p class="posttime"> Few Minutes Ago </p>\
</div>\
</div>');*/
$(".mCSB_container").append('<div class="chatbox2 right">\
<div class="message-text-wrapper messageright">\
<p class="description">' + msg + '</p>\
<p class="posttime"> Few Minutes Ago </p>\
</div>\
</div>');
/*$('.messagediv_'+room_no).append('<div class="chatbox2 right"><div class="message-text-wrapper messageright"><p class="description">'+msg+'</p><p class="posttime">2 Minutes Ago</p></div></div>');*/
$(".myscroller2").mCustomScrollbar("update");
$(".myscroller2").mCustomScrollbar("scrollTo","bottom");
/*$('#mCSB_3_container').scrollTop($('#mCSB_3_container')[0].scrollHeight);*/
}
});
}
}
});
</script>

<script>
function chatlistbox(room_id){
data = {
'roomid': room_id
};
jQuery.ajax({
url: "<?php echo base_url(); ?>customer/profile/get_chat_ajax",
type: "post",
data: data,
beforeSend: function () {
// Statement
//$('#loading').show();
},
success: function (response) {
$(".chatwithu").html(response);
$(".chatlistbox").each(function () {
if ($(this).attr("data-roomid") == room_id) {
$(this).addClass("active");
} else {
$(this).removeClass("active");
}
});
$(".myscroller2").mCustomScrollbar({
theme: "dark-3"
});
$(".myscroller2").mCustomScrollbar("scrollTo","bottom");
$(".myscroller2").mCustomScrollbar("update");
$(".myscroller2").mCustomScrollbar("scrollTo","bottom");
if (response.success == 1) {

}
if (response.success == 0) {

}
//$('#loading').hide();
},
error: function (jqXHR, exception) {
console.log(exception);
// Your error handling logic here..
}
});
}
$(".chatlistbox").click(function () {
var room_id = $(this).attr("data-roomid");
data = {
'roomid': room_id
};
jQuery.ajax({
url: "<?php echo base_url(); ?>customer/profile/get_chat_ajax",
type: "post",
data: data,
beforeSend: function () {
// Statement
//$('#loading').show();
},
success: function (response) {
$(".chatwithu").html(response);
$(".chatlistbox").each(function () {
if ($(this).attr("data-roomid") == room_id) {
$(this).addClass("active");
} else {
$(this).removeClass("active");
}
});
$(".myscroller2").mCustomScrollbar({
theme: "dark-3"
});
$(".myscroller2").mCustomScrollbar("scrollTo","bottom");
if (response.success == 1) {

}
if (response.success == 0) {

}
//$('#loading').hide();
},
error: function (jqXHR, exception) {
console.log(exception);
// Your error handling logic here..
}
});
});
(function ($) {
$(window).on("load", function () {
$(".myscroller2").mCustomScrollbar({
theme: "dark-3"
});
$(".myscroller2").mCustomScrollbar("scrollTo","bottom");
// $(".myscroller").mCustomScrollbar({
// theme:"dark-3"
// });

});
})(jQuery);
$(function ()
{
setInterval(function ()
{
jQuery.ajax({
url: "<?php echo base_url(); ?>customer/profile/get_chat_customer_ajex",
type: "post",
data: {"roomid": $(".hidden_room_no").val()},
success: function (response)
{
$('.chatlist-main').html(response);
},
error: function (jqXHR, exception) {
console.log(exception);
// Your error handling logic here..
}
});
}, 15000);
});


// image upload
$('#openimageupload').click(function () 
{
$('#fileinput').trigger('click');
});


function validateAndUpload(input){
var URL = window.URL || window.webkitURL;
var file = input.files[0];

if(file)
{
$(".myfilesubmit").click();
}
}

function myform_notsubmit(e,obj){
e.preventDefault();
$.ajax({
url: "<?php echo base_url(); ?>customer/profile/imgupload",
type: "POST",
data: new FormData(obj),
contentType: false,
cache: false,
processData: false,
success: function (response) 
{
console.log(response);
if (response > 0)
{
$('.active').each(function(){
if($(this).attr("data-roomid")){
$(this).click();
} 
});
}
},
error: function (jqXHR, exception) 
{
console.log(exception);
}
});
}

$(".myform").on('submit',function(e){
e.preventDefault();
$.ajax({
url: "<?php echo base_url(); ?>customer/profile/imgupload",
type: "POST",
data: new FormData(this),
contentType: false,
cache: false,
processData: false,
success: function (response) 
{
console.log(response);
if (response > 0)
{
$('.active').each(function(){
if($(this).attr("data-roomid")){
$(this).click();
} 
});
}
},
error: function (jqXHR, exception) 
{
console.log(exception);
}
});
})
</script>