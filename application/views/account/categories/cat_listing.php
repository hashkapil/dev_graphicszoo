<section class="con-b">
    <div class="container-fluid">
        <?php if($this->session->flashdata('message_error') != '') { ?>				
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
            </div>
        <?php }?>
        <?php if($this->session->flashdata('message_success') != '') {?>				
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success');?></strong>
            </div>
        <?php } ?>
        <div class="row flex-show">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading">Request Categories</h2>
                    <div class="header_searchbtn">
                        <a href="javascript:void(0)" class="addderequesr" data-toggle="modal" data-target="#AddCategory">+ Add New Category</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="cat-bucket">
            <div id="no-more-tables">
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th colspan="2">Name</th>
                            <th>Image</th>
                            <th>Position</th>
                            <th>Bucket</th>
                            <th>Timeline</th>
                            <th>Active</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $this->load->view('account/categories/cat_listing_template', $cat_data); ?>
                    </tbody>
        </table>
    </div>
</div>
</div>
</section>
<!-- Modal -->
<div class="modal fade similar-prop transfer-popup" id="customcategories" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id="cstm_cat_submit">
            <header class="fo-rm-header">
                 <h3 class="popup_h2 del-txt" style="text-align:left">Do you want to use default categories?</h3>
<!--                <h3 class="overwrite_label del-txt" style="text-align:left;display:none">Do you want to make the changes on top of GZ categories structure or want to create it from scratch ?</h3>-->
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="confirmation_btn text-center">
                        <div class="sound-signal">
                            <div class="form-radion">
                                <input type="radio" name="category_type" class="preference category_type" id="soundsignal_default" value="yes">
                                <label for="soundsignal_default">Yes</label>
                            </div>
                        </div>
                        <div class="sound-signal">
                            <div class="form-radion">
                                <input type="radio" name="category_type" class="preference category_type" id="soundsignal_custom" checked="" value="no">
                                <label for="soundsignal_custom">No</label>
                            </div>
                        </div>   
                        </div> 
                        <div class="request-ftr">
                        <input class="btn set_categories btn-ndelete" name="use_custom_cat" type="submit" value="Save">
                        </div>
                </div>
            </div>
                </form>
        </div>
    </div>
</div>
<div class="modal fade similar-prop transfer-popup" id="defaultcategories" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id="gz_cat_submit">
            <header class="fo-rm-header">
                <h3 class="overwrite_label del-txt" style="text-align:left;">Do you want to make the changes on top of GZ categories structure or want to create it from scratch ?</h3>
                <div class="cross_popup" data-dismiss="modal">x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    
                    <div class="confirmation_btn text-center">
                        <div class="sound-signal">
                            <div class="form-radion">
                                <input type="radio" name="gz_cat_type" class="preference category_type" id="soundsignal_copygz" checked="" value="yes">
                                <label for="soundsignal_copygz">Yes</label>
                            </div>
                        </div>
                        <div class="sound-signal">
                            <div class="form-radion">
                                <input type="radio" name="gz_cat_type" class="preference category_type" id="soundsignal_scretch"  value="no">
                                <label for="soundsignal_scretch">No</label>
                            </div>
                        </div>   
                        </div> 
                        <div class="request-ftr">
                            <input name="use_gz_cat" class="btn set_categories btn-ndelete"  type="submit" value="Save">
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal similar-prop nonflex fade" id="AddCategory" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add Category</h2>
                <div class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <form action="<?php echo base_url(); ?>admin/Categories/add_category/" method="post" id="addCatForm" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group">
                                         <p class="label-txt label-active"> Parent category</p>
                                    <select name="parent_cat" class="input parent_cat" id="parent_cat">
                                            <option value="">Choose parent</option>
                                            <?php foreach ($cat_data as $kk => $vv) { ?>
                                                <option value="<?php echo $vv['id']; ?>"><?php echo $vv['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="line-box"></div>
                                        <p class="ab-notify">Don't choose if you are adding parent category.</p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Name</p>
                                        <input type="text" name="cat_name" required  class="input name1">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">position</p>
                                        <input type="number" name="position" required class="input position1">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
<!--                                        <p class="label-txt">Bucket</p>-->
                                        <select name="bucket_type" class="input bucket_type">
                                            <option value=''>Choose Bucket</option>
                                            <option value='1'>Artwork</option>
                                            <option value='2'>Creative</option>
                                        </select>
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Timeline</p>
                                        <input type="number" name="timeline" required class=" input timeline">
                                        <div class="line-box"></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group goup-x1 text-left">
                                <div class="sound-signal">
                                    <div class="form-radion2">
                                        <label class="containerr">
                                           <input type="checkbox" name="active" class="active1">
                                           <span class="checkmark"></span> Active
                                       </label>
                                   </div>
                               </div>                               
                           </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <img src="" class="showimg">
                                <div class="file-drop-area file-upload">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input type="file" class="file-input project-file-input file-input-add" multiple="" name="cat_image" id="file_input">
                                </div>
                                <div id="file-uploadNameAdd">
                                <span></span>
                            </div>
                            </div>
                            
                          <button type="submit" class="load_more button" name="save">Save</button>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
<!-- Modal -->
<script>
var overWriteCat = "<?php echo $is_overwrite_category; ?>";
var loadCATtemp = "<?php $this->load->view('account/Categories/cat_listing_template', $data) ?>";
if(overWriteCat == '0'){
$(document).ready(function(){
   $('#customcategories').modal('show');
     return false;
});
}
$(document).on('change','.category_type',function(){
    var request_type = $("input[name='category_type']:checked").val();
    if(request_type == 'yes'){
        $('#defaultcategories').modal('show');
        return false;
    }
});
$('#gz_cat_submit,#cstm_cat_submit').on('submit', function(e){
    e.preventDefault();
    var no_overwrite = $("input[name='category_type']:checked").val();
    var overwrite = $("input[name='gz_cat_type']:checked").val();
    if(no_overwrite == 'no'){
       no_overwrite = no_overwrite; 
       overwrite = '';
    }else if(no_overwrite == 'yes'){
       no_overwrite = ''; 
       overwrite = overwrite;
    }
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: baseUrl + "account/Categories/overWriteCat",
        data: {'no_overwrite':no_overwrite,'overwrite':overwrite},
        success: function (data) {
            toastr_msg(data.status,data.message);
            $('#defaultcategories').modal('hide');
            $('#customcategories').modal('hide');
//            $this->load->view('user/login', $data);
        }
    });
});
$(document).on('click', '.show_child', function () {
    var id = $(this).attr('data-child_id');
    $('.plus_' + id).html('-');
    $(this).addClass('minus');
    //  $("child_"+id).slideToggle();
    $('.child_' + id).css('display', 'table-row');
});
$(document).on('click', '.minus', function () {
    var id = $(this).attr('data-child_id');
    $('.plus_' + id).html('+');
    $('.child_' + id).css('display', 'none');
    $(this).removeClass('minus');
});
</script>



