<?php 
if (in_array($data[0]['plan_name'], NEW_PLANS) || $data[0]['user_flag'] == "client") { ?>
    <div class="billing_plan two-can <?php echo (count($userplans) > 3)?$type_ofuser.' new_plan_billingfr_chng owl-carousel':$type_ofuser; ?>">
        <?php
        foreach ($userplans as $userplan) {
                $agencyclass = $userplan['plan_type'] . " upgrd_agency_pln";
                $prc = $userplan['plan_price'];
                $agency_priceclass = "<span class='" . $userplan['plan_type'] . " agency_price'> " . $userplan['plan_price'] . "</span><span>/" . $userplan['plan_type'] . "</span>";
                $this->load->view('account/plan_details', array("userplan" => $userplan, "agencyclass" => $agencyclass, "agency_priceclass" => $agency_priceclass, "data" => $data));
        }
        ?>
    </div>
<?php }else{ ?>
<div class="plan_selct_bt">
    <div class="switch_btn_plan">
        <ul>
            <li class="active"><a href="#f_monthly_pln" class="stayhere monthly_plan pricingpln" data-toggle="tab">Monthly Plan</a></li>
            <li><a href="#f_quarterly_pln" class="stayhere quarterly_plan pricingpln" data-toggle="tab">Quarterly Plan</a></li>
            <li><a href="#f_yearly_pln" class="stayhere yearly_plan pricingpln" data-toggle="tab">Annual Plan&nbsp;<span>(Save 15%)</span></a></li>
        </ul>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane active" id="f_monthly_pln"> 
        <div class="billing_plan two-can <?php echo $type_ofuser; ?>">
            <?php
            foreach ($userplans as $userplan) {
                if ($userplan['plan_type'] == 'monthly') {
                    $agencyclass = $userplan['plan_type'] . " upgrd_agency_pln";
                    $prc = $userplan['plan_price'];
                    $agency_priceclass = "<span class='" . $userplan['plan_type'] . " agency_price'> " . $userplan['plan_price'] . "</span><span>/" . $userplan['plan_type'] . "</span>";
                    $this->load->view('account/plan_details', array("userplan" => $userplan, "agencyclass" => $agencyclass, "agency_priceclass" => $agency_priceclass, "data" => $data));
                }
            }
            ?>
        </div>
    </div>
    <div class="tab-pane" id="f_quarterly_pln"> 
        <div class="billing_plan two-can <?php echo $type_ofuser; ?>">
            <?php
            foreach ($userplans as $userplan) {
                if ($userplan['plan_type'] == 'quarterly') {
                    $agencyclass = $userplan['plan_type'] . " upgrd_agency_pln";
                    $prc = $userplan['tier_prices'][0]['amount'];
                    $yrlyprc = $userplan['tier_prices'][0]['annual_price'];
                    $agency_priceclass = "<span class='" . $userplan['plan_type'] . " agency_price'> " . $prc . "</span><span>/Monthly</span><br><span>$<span class='f_pln_totl_amnt'>".$yrlyprc." </span> (BILLED QUARTERLY)</span>";
                    $this->load->view('account/plan_details', array("userplan" => $userplan, "agencyclass" => $agencyclass, "agency_priceclass" => $agency_priceclass, "data" => $data));
                }
            }
            ?>
        </div>
    </div>
    <div class="tab-pane" id="f_yearly_pln"> 
        <div class="billing_plan two-can <?php echo $type_ofuser; ?>">
            <?php
            foreach ($userplans as $userplan) {
                if ($userplan['plan_type'] == 'yearly') {
                    $agencyclass = $userplan['plan_type'] . " upgrd_agency_pln";
                    $prc = $userplan['tier_prices'][0]['amount'];
                    $yrlyprc = $userplan['tier_prices'][0]['annual_price'];
                    $agency_priceclass = "<span class='" . $userplan['plan_type'] . " agency_price'> " . $prc . "</span><span>/Monthly</span><br><span>$<span class='f_pln_totl_amnt'>".$yrlyprc."</span> (BILLED ANNUALLY)</span>";
                    $this->load->view('account/plan_details', array("userplan" => $userplan, "agencyclass" => $agencyclass, "agency_priceclass" => $agency_priceclass, "data" => $data));
                }
            }
            ?>
        </div>
    </div>
</div>
<?php }