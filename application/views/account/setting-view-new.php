<?php
if (isset($data[0]['first_name'])) {
    $first_name = $data[0]['first_name'];
}if (isset($data[0]['last_name'])) {
    $last_name = $data[0]['last_name'];
}
$currentuser = $this->load->get_var('main_user');
$loggedinuser = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';
$CI =& get_instance();
$CI->load->library('account/S_myfunctions');
$this->load->view('front_end/variable_css');  
if($login_user_data[0]["is_saas"] == 1){
   $billng_text = "GZ Design Subscription";
}else{
    $billng_text = "Billing & Subscription";
}
?>
<section class="edit-profile-sec ff">
   <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo base_url();?>public/assets/img/gz-ajax-loader.gif" />
   </div>
   <div class="container dd">
      <div class="row mobile_menu_mngmnt">
         <div class="col-md-12" data-step="7" data-intro="Go to settings to manage users, billing, notifications, password, and your profile." data-position='right' data-scrollTo='tooltip'>
            <div class="account-tab">
               <div class="tab-toggle togglemenuforres">
                  <p><span></span><span></span><span></span></p>
               </div>
               <ul class="list-header-blog">
                  <li class="active"><a href="#general" class="stayhere" data-toggle="tab">General</a></li>
                  <li><a href="#personal" class="stayhere" data-toggle="tab">Personal Info</a></li>
                  <li><a href="#notification" class="stayhere" data-toggle="tab">Notification</a></li>
                  <?php if ($permission["seebilling"] != 0) { ?>
                  <li><a href="#billing" class="stayhere" data-toggle="tab">
                        <?php echo $billng_text; ?></a></li>
                  <?php }  ?>
                  <li><a href="#password_tab" class="stayhere" data-toggle="tab">Password</a></li>
                  <?php
                        if ($currentuser == $loggedinuser || $login_user_data[0]['role'] == "manager") { ?>
                  <li class="usrmangment"><a href="#management" class="stayhere" data-toggle="tab">Team Management</a></li>
                  <?php } if($login_user_data[0]["is_saas"] == 1){ ?>
                  <li class="s_subscription"><a href="#s_subscription" class="stayhere" data-toggle="tab">Proof Subscription</a></li>
                  <?php } ?>
                  <?php if($parent_user_id == 0 && $parent_user_plan[0]['is_agency'] == 1){ ?>
                  <li class="for_agency_user stayhere">
                     <a href="<?php echo base_url(); ?>account/user_setting"><i class="fas fa-cog"></i> White Label Settings</a>
                     <ul class="white_label_nd_email" style="display:none">
                        <li><a href="#Admin_Email" class="stayhere" data-toggle="tab">Email Templates</a></li>
                        <li class=""><a href="<?php echo base_url(); ?>account/user_setting"><span><i class="fas fa-cogs"></i> </span> White label Setting <span class="sr-only">(current)</span></a></li>
                     </ul>
                  </li>
                  <?php } ?>
               </ul>
            </div>
         </div>
      </div>
      <?php if ($this->session->flashdata('message_error') != '') { ?>
      <div class="alert alert-danger alert-dismissable">
         <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
         <p class="head-c">
            <?php echo $this->session->flashdata('message_error'); ?>
         </p>
      </div>
      <?php } ?>
      <?php if ($this->session->flashdata('message_success') != '') { ?>
      <div class="alert alert-success alert-dismissable">
         <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
         <p class="head-c">
            <?php echo $this->session->flashdata('message_success'); ?>
            </ps>
      </div>
      <?php } ?>
      <!--    <div class="designrerror_msg" style="display: none; ">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c"></p>
        </div> -->
      <div class="tab-content">
         <div class="tab-pane active" id="general">
            <div class="row">
               <div class="col-md-12">
                  <form enctype="multipart/form-data" method="post" action="" id="edit_form_data">
                     <div class="row">
                        <div class="col-sm-4">
                           <div class="user-area">
                              <div class="display-info">
                                 <div class="dp">
                                    <img src="<?php echo $profile[0]['profile_picture']; ?>" class="img-responsive telset33">
                                 </div>
                                 <a href="javascript:void(0)">
                                    <span class="setimgcaps">
                                       <i class="icon-gz_edit_icon" id="edit_button" aria-hidden="true"></i>
                                    </span>
                                 </a>
                                 <div class="setimg-row33 changeprofilepage" style="display: none;">
                                    <div class="imagemain2" style="padding-bottom: 20px; display: none;">
                                       <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="profile" data-plugin="dropify" id="inFile" style="display: none" />
                                    </div>
                                    <div class="clearfix profile_image_sec">
                                       <div class="imagemain">
                                          <figure class="setimg-box33">
                                             <img src="<?php echo $profile[0]['profile_picture']; ?>" class="img-responsive telset33">
                                             <a class="setimg-blog33 link1 font18 bold" href="javascript:void(0)" onclick="$('.dropify').click();" class="link1 font18 bold" for="profile">
                                                <span class="setimgblogcaps">
                                                   <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-camera.png" class="img-responsive"><br>
                                                   <span class="setavatar33">Change <br>Avatar</span>
                                                </span>
                                             </a>
                                          </figure>
                                          <style>
                                             .dropify-wrapper{ display:none; height: 200px !important; }
                                                        </style>
                                       </div>
                                       <div class="imagemain3" style="display: none;">
                                          <figure class="setimg-box33">
                                             <img id="image3" src="" class="img-responsive telset33">
                                             <a class="setimg-blog33 link1 font18 bold" href="javascript:void(0)" onclick="$('.dropify').click();" class="link1 font18 bold" for="profile">
                                                <span class="setimgblogcaps">
                                                   <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-camera.png" class="img-responsive"><br>
                                                   <span class="setavatar33">Change <br>Avatar</span>
                                                </span>
                                             </a>
                                          </figure>
                                       </div>
                                    </div>
                                 </div>
                                 <?php
                                            $last_name = "";
                                            if (isset($profile[0]['last_name'])) {
                                                $last_name = substr($profile[0]['last_name'], 0, 1);
                                            }
                                            ?>
                                 <h3>
                                    <?php echo $profile[0]['first_name'] . ' ' . $last_name; ?>
                                 </h3>
                                 <p>
                                    <?php echo $profile[0]['email']; ?>
                                 </p>
                                 <p>
                                    <?php echo $profile[0]['phone']; ?>
                                 </p>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-8">
                           <div class="content-edition">
                              <h2 class="main-info-heading">General Details</h2>
                              <p class="fill-sub">Fill out the information below to tell us more about yourself.</p>
                              <div class="edit-submt">
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['first_name']) && $profile[0]['first_name'] != "") ? " label-active" : "" ; ?>"> Full Name <span>*</span> </p>
                                    <input type="text" name="first_name" class="input" value="<?php
                                                if (isset($profile[0]['first_name'])) {
                                                    echo $profile[0]['first_name'];
                                                }
                                                ?> <?php
                                                if (isset($profile[0]['last_name'])) {
                                                    echo $profile[0]['last_name'];
                                                }
                                                ?>" required>
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['company_name']) && $profile[0]['company_name'] != "") ? " label-active" : "" ; ?>"> Company Name </p>
                                    <input type="text" name="company" class="input" value="<?php echo isset($profile[0]['company_name']) ? $profile[0]['company_name'] : ''; ?>">
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                                 <!--label class="form-group">
                                            <p class="label-txt <?php echo ($profile[0]['title'] && $profile[0]['title'] != "") ? "label-active" : ""; ?>">Title </p>
                                            <input type="text" name="title" class="input" value="<?php echo isset($profile[0]['title']) ? $profile[0]['title'] : ''; ?>">
                                            <div class="line-box">
                                              <div class="line"></div>
                                          </div>
                                      </label-->
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['email']) && $profile[0]['email'] != "") ? " label-active" : "" ; ?>">Email Address <span>*</span> </p>
                                    <input type="email" class="input" name="notification_email" value="<?php echo isset($profile[0]['email']) ? $profile[0]['email'] : ''; ?>" required>
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['phone']) && $profile[0]['phone'] != "") ? " label-active" : "" ; ?>">Phone Number</p>
                                    <input type="tel" name="phone" class="input" value="<?php echo isset($profile[0]['phone']) ? $profile[0]['phone'] : ''; ?>">
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['url']) && $profile[0]['url'] != "") ? " label-active" : "" ; ?>">Website URL </p>
                                    <input type="text" name="url" class="input" value="<?php echo isset($profile[0]['url']) ? $profile[0]['url'] : ''; ?>">
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                                 <div class="form-group">
                                    <div class="btn-here">
                                       <input type="submit" id="savebtn1" name="savebtn" class="btn-e save_info btn-red" value="Save">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="tab-pane" id="personal">
            <div class="row">
               <div class="col-md-12">
                  <div class="white-boundries">
                     <h2 class="main-info-heading"> Personal Information</h2>
                     <p class="fill-sub">Enter your personal details below.</p>
                     <div class="edit-submt">
                        <form action="javascript:void(0)" method="post" accept-charset="utf-8" id="about_info_form">
                           <div class="row">
                              <div class="col-md-12">
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['address_line_1']) && $profile[0]['address_line_1'] != "") ? " label-active" : "" ; ?>">Address</p>
                                    <input type="text" name="adress" class="input" value="<?php echo isset($profile[0]['address_line_1']) ? $profile[0]['address_line_1'] : '' ?>">
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['city']) && $profile[0]['city'] != "") ? " label-active" : "" ; ?>">City</p>
                                    <input type="text" name="city" class="input" value="<?php echo isset($profile[0]['city']) ? $profile[0]['city'] : '' ?>">
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                              </div>
                              <div class="col-md-6">
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['state']) && $profile[0]['state'] != "") ? " label-active" : "" ; ?>">State</p>
                                    <input type="text" name="state" class="input" value="<?php echo isset($profile[0]['state']) ? $profile[0]['state'] : '' ?>" readonly="">
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <label class="form-group">
                                    <p class="label-txt <?php echo (isset($profile[0]['zip']) && $profile[0]['zip'] != "") ? " label-active" : "" ; ?>">Zip Code</p>
                                    <input type="text" name="zip" class="input" value="<?php echo isset($profile[0]['zip']) ? $profile[0]['zip'] : '' ?>">
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                              </div>
                              <div class="col-md-6">
                                 <label class="form-group">
                                    <p class="label-txt label-active">Select Time Zone </p>
                                    <select name="timezone" class="form-control select-a input">
                                       <option value="">Select timezone</option>
                                       <?php foreach ($getalltimezone as $kk => $vv) { ?>
                                       <option value="<?php echo $vv['zone_name']; ?>" <?php echo ($vv['zone_name']==$profile[0]['timezone']) ? 'selected' : '' ; ?>>
                                          <?php echo $vv['zone_name']; ?>
                                       </option>
                                       <?php } ?>
                                    </select>
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="btn-here">
                                 <input type="submit" id="about_info_btn" name="savebtn" class="btn-e save_info btn-red" value="SAVE" />
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!---------------------Start notifications------------------------------------>
         <!-- <?php //echo "<pre>"; print_r($profile); die("ASd")?> -->
         <div class="tab-pane" id="notification">
            <div class="row">
               <div class="col-md-12">
                  <div class="white-boundries">
                     <h2 class="main-info-heading"> Notification</h2>
                     <p class="fill-sub">Choose your notification preferences below.</p>
                     <div class="form-group">
                        <div class="notify-lines">
                           <div class="switch-custom-usersetting-check remind_mail asd">
                              <span class="checkstatus"></span>
                              <?php if($CheckForMainORsub['is_saas'] == 1){ ?>
                              <input type="checkbox" data-cid="<?php echo $profile[0]['id']; ?>" data-key="DRAFT_APPROVED_EMAIL_TO_USER" id="switch_1" <?php if ($profile[0]['DRAFT_APPROVED_EMAIL_TO_USER']==1) { echo 'checked' ; } else { } ?>/>
                              <?php }else{ ?>
                              <input type="checkbox" data-cid="<?php echo $profile[0]['id']; ?>" data-key="draft_approved_email_to_user" id="switch_1" <?php if ($profile[0]['draft_approved_email_to_user']==1) { echo 'checked' ; } else { } ?>/>
                              <?php } ?>
                              <label for="switch_1"></label>
                           </div>
                           <p>Send email to review new design draft</p>
                        </div>
                        <div class="notify-lines">
                           <div class="switch-custom-usersetting-check remind_mail">
                              <span class="checkstatus"></span>
                              <?php if($CheckForMainORsub['is_saas'] == 1){ ?>
                              <input type="checkbox" data-cid="<?php echo $profile[0]['id']; ?>" data-key="SEND_CRON_EMAIL_ON_MESSAGE" id="switch_2" <?php if ($profile[0]['SEND_CRON_EMAIL_ON_MESSAGE']==1) { echo 'checked' ; } else { } ?>/>
                              <?php }else { ?>
                              <input type="checkbox" data-cid="<?php echo $profile[0]['id']; ?>" data-key="send_cron_email_on_message" id="switch_2" <?php if ($profile[0]['send_cron_email_on_message']==1) { echo 'checked' ; } else { } ?>/>
                              <?php } ?>
                              <label for="switch_2"></label>
                           </div>
                           <p>Remind me if I have not seen messages within 30 minutes</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!---------------------End notifications------------------------------------>
         <div class="tab-pane" id="billing">
            <div class="row">
               <div class="col-md-12">
                  <div class="white-boundries">
                     <?php if ($parent_user_data[0]['is_cancel_subscription'] == 1 && $parent_user_data[0]['parent_id'] == 0) { ?>
                     <p class="next_plan_active cancel_sub_msg">Payment is past due. Subscription has been cancelled. Please reactivate your subscription for more designs.
                     </p>
                     <?php } else if ($data[0]['invoice'] == '0' && $data[0]['next_plan_name'] != "") {
                    ?>
                     <p class="next_plan_active">Your plan "
                        <?php echo $nextplan_details[0]['plan_name']; ?>" will be active from
                        <?php echo $data[0]['billing_end_date']; ?>
                     </p>
                     <?php } ?>
                     <h2 class="main-info-heading">Billing and Subscription</h2>
                     <p class="fill-sub">Update your Billing or Change your Subscription Plan below.</p>
                     <?php if ($type_ofuser == 'fortynine_plans') { ?>
                     <div class="purchase_req" style="display:none">
                        <a href="javascript:void(0)" data-value="<?php echo FORTYNINE_REQUEST_PLAN; ?>" data-price="<?php echo FORTYNINE_REQUEST_PRICE; ?>" data-inprogress="<?php echo FORTYNINE_REQUEST; ?>" data-display_name="<?php echo FORTYNINE_REQUEST_PLAN_NAME; ?>" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p red-theme-btn">
                           Purchase 1 Request
                        </a>
                     </div>
                     <?php } ?>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="project-row-qq1 settingedit-box">
                              <div class="settrow">
                                 <div class="settcol left">
                                    <h3 class="sub-head-b">Card Details</h3>
                                 </div>
                                 <div class="settcol right">
                                    <a id="about_infoform" class="review-circleww" href="javascript:void(0)">
                                       <?php if ($card) { ?><i class="icon-gz_edit_icon"></i>
                                       <?php }else{ ?>
                                       <i class="icon-gz_plus_icon"></i>
                                       <?php } ?>
                                    </a>
                                 </div>
                              </div>
                              <div class="row">
                                 <?php if ($card) { //echo "<pre>";print_r($card); ?>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                       <label class="label-x2">Card Number</label>
                                       <p class="space-a"></p>
                                       <div class="row">
                                          <div class="col-md-12">
                                             <p class="text-f">************
                                                <?php echo $card['0']->last4; ?>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                       <label class="label-x2">Expiry Month/Year</label>
                                       <p class="space-a"></p>
                                       <div class="row">
                                          <div class="col-md-12">
                                             <p class="text-f">
                                                <?php echo $card['0']->exp_month . '/' . $card['0']->exp_year; ?>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                       <label class="label-x2">CVV</label>
                                       <p class="space-a"></p>
                                       <div class="row">
                                          <div class="col-md-12">
                                             <p class="text-f">***</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php } else { ?>
                                 <h4 style="text-align:center">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/card-bg.svg" class="img-responsive">
                                    No Card Detail Found!</h4>
                                 <?php } ?>
                              </div>
                           </div>
                           <div class="carddetailsedit edit-submt" style="display:none">
                              <div class="settcol left">
                                 <h3 class="sub-head-b">Card Details</h3>
                              </div>
                              <form class="card-verifection" action="<?php echo base_url(); ?>account/ChangeProfile/change_plan" method="post" accept-charset="utf-8">
                                 <div class="card-js">
                                    <div class="row">
                                       <div class="col-sm-4">
                                          <label class="form-group">
                                             <p class="label-txt <?php echo isset($editvar['title']) ? " label-active" : "" ; ?>">Card Number <span>*</span></p>
                                             <input type="text" name="card-number" class="card-number input" required="" maxlength="16">
                                             <div class="line-box">
                                                <div class="line"></div>
                                             </div>
                                          </label>
                                       </div>
                                       <div class="col-sm-4">
                                          <label class="form-group">
                                             <p class="label-txt label-active fixedlabelactive">Expiry Datesdsds <span>*</span></p>
                                             <input class="input expiry-month expir_date" id="card_expir_date" name="expiry-month-date" required="" placeholder="MM  /  YY" onkeyup="dateFormat(this.value,this);">
                                             <div class="line-box">
                                                <div class="line"></div>
                                             </div>
                                          </label>
                                       </div>
                                       <div class="col-sm-4">
                                          <label class="form-group">
                                             <p class="label-txt <?php echo isset($editvar['title']) ? " label-active" : "" ; ?>">CVV <span>*</span></p>
                                             <input type="text" name="cvc" class="cvc input" maxlength="4" required="">
                                             <div class="line-box">
                                                <div class="line"></div>
                                             </div>
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="btn-here">
                                       <input type="hidden" name="current_plan" value="<?php echo $current_plan; ?>" />
                                       <input type="submit" value="Save" class="btn btn-e site-btn btn-red" name="change_my_card_deatils" />
                                       <input type="submit" id="cancelcard" name="cancel" class="theme-cancel-btn" value="Cancel"></div>
                                 </div>
                              </form>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="row billing_plan_view">
                              <p class="btn-x">
                                 <a href="javascript:void(0)" class="btn-f Change_Plan chossbpanss">
                                    <?php echo ($data[0]['plan_name'] != '') ? "Change your plan" : "Select your plan"; ?></a>
                              </p>
                              <div class="col-md-6 col-sm-6 current_actv_pln billing_plan">
                                 <div class="flex-grid">
                                    <div class="price-card best-offer">
                                       <div class="chooseplan active f_main_clnt">
                                          <?php
                                    if ($data[0]['plan_name'] != '') {
                                        $pln_feature = explode("_", $currentplan_details[0]['features']);
                                        $plnfeature = "<ul>";
                                        if (!empty($pln_feature)) {
                                            foreach ($pln_feature as $text) {
                                                $plnfeature .= "<li>" . $text . "</li>";
                                            }
                                        }
                                        $plnfeature .= "<ul>";
                                        $agency_priceclass = "<span class='".$currentplan_details[0]['plan_type']." agency_price'> ".$currentplan_details[0]['plan_price']."</span>" ; ?>
                                          <h2>
                                             <?php echo $currentplan_details[0]['plan_name']; ?>
                                          </h2>
                                          <h3>
                                             <font>$</font>
                                             <?php echo $agency_priceclass; ?><span>/
                                                <?php echo $currentplan_details[0]['plan_type']; ?></span>
                                          </h3>
                                          <div class="p-benifite">
                                             <?php if($type_ofuser == 'fortynine_plans' || $type_ofuser == 'one_time'){?>
                                             <ul>
                                                <li>
                                                   <p>Total Requests:
                                                      <?php echo $data[0]['total_requests']; ?>
                                                   </p>
                                                </li>
                                                <li>
                                                   <p>Added Requests:
                                                      <?php echo $request['added_request']; ?>
                                                   </p>
                                                </li>
                                                <li>
                                                   <p>Pending Requests:
                                                      <?php echo $request['pending_req']; ?>
                                                   </p>
                                                </li>
                                             </ul>
                                             <?php }else{
                                                if(!empty($currentplan_details[0]['tier_prices'])){
                                                    if ($currentplan_details[0]['plan_type'] == 'yearly' || $currentplan_details[0]['plan_type'] == 'quarterly') {
                                                        $amount = 'annual_price';
                                                    } else {
                                                        $amount = 'amount';
                                                    }
                                                    $options = "";
                                                    foreach ($currentplan_details[0]['tier_prices'] as $tier_prices) {
                                                        $options .= '<option value ="' . $tier_prices['quantity'] . '" data-amount="' . $tier_prices[$amount] . '" data-annual_price="' . $tier_prices["amount"] . '">' . $tier_prices['quantity'] . ' Dedicated Designer</option>';
                                                    }
                                                    $features = str_replace('{{QUANTTY}}', $options, $plnfeature);
                                                    echo $features; 
                                                }else{
                                                    echo $plnfeature;
                                                }
                                            } ?>
                                          </div>
                                          <div class="price-sign-up">
                                             <a type="button" class="ud-dat-p currentPlan" disabled>
                                                Current Plan
                                             </a>
                                          </div>
                                          <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
                                          <?php }else{
                                        echo "<p class='no-pln'>no selected Plan</p>";
                                    } ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <p class="btn-x bckbtntoback" style="display:none">
                              <a href="javascript:void(0)" class="btn-g Change_Plan backfromnewplan">Back</a>
                           </p>
                           <div class="f_chng_ugrd_blpln" style="display:none">
                              <?php $this->load->view('account/plan_listing', array("data" => $data)); ?>
                           </div>
                           <?php if ($data[0]['is_cancel_subscription'] != 1 && $parent_user_data[0]['parent_id'] == 0 && !in_array($data[0]['plan_name'], NEW_PLANS) && $data[0]['plan_name'] != "") { ?>
                           <a href="javascript:void(0)" class="btn-f cancel_Plan" data-toggle="modal" data-target="#CancelPopup" style="color: var(--theme-primary-color);text-decoration: underline;">Cancel Plan</a>
                           <?php } ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!---------------------Start Change Password------------------------------------>
         <div class="tab-pane" id="password_tab">
            <div class="row">
               <div class="col-md-12">
                  <div class="white-boundries">
                     <h2 class="main-info-heading">Change Password</h2>
                     <p class="fill-sub">Use the fields below to update your account password</p>
                     <div class="row">
                        <div class="col-md-7">
                           <div class="edit-submt sad">
                              <form action="<?php echo base_url(); ?>account/ChangeProfile/change_password_front" method="post" accept-charset="utf-8">
                                 <label class="form-group">
                                    <p class="label-txt <?php echo isset($editvar['title']) ? " label-active" : "" ; ?>">Current Password *</p>
                                    <input type="password" class="input" name="old_password" required>
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                                 <label class="form-group">
                                    <p class="label-txt <?php echo isset($editvar['title']) ? " label-active" : "" ; ?>">New Password *</p>
                                    <input type="password" class="input" name="new_password" required>
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                                 <label class="form-group">
                                    <p class="label-txt <?php echo isset($editvar['title']) ? " label-active" : "" ; ?>">Confirm Password *</p>
                                    <input type="password" class="input" name="confirm_password" required>
                                    <div class="line-box">
                                       <div class="line"></div>
                                    </div>
                                 </label>
                                 <div class="form-group">
                                    <div class="btn-here">
                                       <input type="submit" name="save" class="btn-red" value="SAVE">
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!---------------------END Change Password------------------------------------>
         <div class="tab-pane xyz" id="management">
            <div class="row">
               <div class="col-md-12">
                  <div class="white-boundries" id="sub-userlist">
                     <div class="headerWithBtn">
                        <h2 class="main-info-heading">Team Management</h2>
                        <p class="fill-sub">Add and Update additional users for your account.</p>
                        <div class="addPlusbtn">
                           <!-- <?php if($totalSubUserunderSAAS[0]['total_users'] < $saasPlanDetails[0]['total_sub_user']) { ?>
                        <a href="javascript:void(0)" class="new-subuser add_subuser_main" id="add_subuser_mains" data-user_type="manager">+ Add Team Member</a>
                        <?php }else { ?>
                            <a href="javascript:void(0)" data-target="#notAllowedMoreuser" data-toggle="modal">
                                + Add Team Member
                            </a>

                        
                    <?php } ?> -->
                           <?php if($permission["add_team_member"] != 0){ ?>
                           <a href="javascript:void(0)" class="new-subuser add_subuser_main" id="add_subuser_mains" data-user_type="manager">+ Add Team Member</a>
                           <?php } ?>
                        </div>
                     </div>
                     <!-- tabs for team listing start  -->
                     <?php if($login_user_data[0]["parent_id"] == 0){ 
                    $mangercls = "active";
                    $designercls = "";
                 }else{
                    $mangercls = "";
                    $designercls = "active";
                 } ?>
                     <div class="account-tab ">
                        <div class="tab-toggle togglemenuforres">
                           <p><span></span><span></span><span></span></p>
                        </div>
                        <ul class="list-header-blog TeamManagement">
                           <?php if($login_user_data[0]["parent_id"] == 0){ ?>
                           <li class="<?php echo $mangercls; ?> link"><a id="manager_tab" href="#manager-tab" class="stayhere" data-toggle="tab">Manager</a></li>
                           <?php } ?>
                           <li class="<?php echo $designercls; ?> link"><a id="designer_tab" href="#designer-tab" class="stayhere" data-toggle="tab">Designer</a></li>
                           <li class="link"><a id="client_tab" href="#client-tab" class="stayhere" data-toggle="tab">Customer</a></li>
                        </ul>
                        <input type="hidden" id="tabidentity" value="">
                     </div>
                     <div class="tab-toggle togglemenuforres">
                        <p><span></span><span></span><span></span></p>
                     </div>
                     <!-- tabs for team listing end  manage_listing -->
                     <div class="tab-content ">
                        <div class="tab-pane <?php echo $mangercls; ?>" id="manager-tab" style="margin-top: 30px; ">
                           <div class="manager managment-list">
                              <table>
                                 <?php  if(!empty($alluserData['manager'])){ 
//                     echo "<pre/>";print_R($designerdata);
                 foreach ($alluserData['manager'] as $userkey => $userval) {  

                  if($userval['is_delete']==0){
                  ?>
                                 <tr>
                                    <td><span><img src="<?php echo $userval['profile_picture']; ?>" class="img-responsive"></span></td>
                                    <td class="name"><span>
                                          <?php echo isset($userval['first_name']) ? $userval['first_name'] : ''; ?>
                                          <?php echo isset($userval['last_name']) ? $userval['last_name'] : ''; ?></span></td>
                                    <td><span>
                                          <?php echo isset($userval['email']) ? $userval['email'] : ''; ?></span></td>
                                    <td><span>
                                          <?php echo isset($userval['phone']) ? $userval['phone'] : ''; ?></span></td>
                                    <!--                      <td><span><?php //echo (isset($userval['display_plan_name']) && $userval['display_plan_name'] != "") ? $userval['display_plan_name'] : 'No plan selected'; ?></span></td>-->
                                    <td class="action">
                                       <span>
                                          <a href="javascript:void(0);" data-id="<?php echo $userval['id']; ?>" class="edit_subuser" data-user_type="manager">
                                             <i class="fas fa-pen"></i>
                                          </a>
                                          <a href="javascript:void(0)" data-target="#deletesubuser" data-toggle="modal" data-id="<?php echo $userval['id']; ?>" class="delete_clients">
                                             <i class="far fa-trash-alt"></i>
                                          </a>
                                          <div class="switch-custom-usersetting-check activate-user">
                                             <input type="checkbox" name="enable_disable_set" data-userid="<?php echo $userval['id']; ?>" data-email="<?php echo $userval['email']; ?>" data-name="<?php echo $userval['first_name']; ?>" id="enable_disable_set_<?php echo $userval['id']; ?>" <?php if ($userval['is_active']==1) { echo 'checked' ; } else { } ?>>
                                             <label for="enable_disable_set_<?php echo $userval['id']; ?>"></label>
                                          </div>
                                       </span>
                                       <?php if($userval["mode"][0]["payment_mode"] == 1 && ($userval['customer_id'] == "" || $userval['plan_name'] == "")){ ?>
                                       <a href="javascript:void(0)" class="active_blling_acont" data-target="#activateclientbilling" data-toggle="modal" data-id="<?php echo $userval['id']; ?>">Activate Billing</a>
                                       <?php } ?>
                                    </td>
                                 </tr>
                                 <?php } }
                 }else{
                     echo "<div class='no_found'>Manager not found...!!</div>"; 
                 }?>
                              </table>
                           </div>
                        </div>
                        <div class="tab-pane <?php echo $designercls; ?>" id="designer-tab">
                           <div class="designer  managment-list">
                              <table>
                                 <?php if(!empty($alluserData['designer'] )){ 
//                     echo "<pre/>";print_R($designerdata);
                 foreach ($alluserData['designer']  as $userkey => $userval) {  

                  if($userval['is_delete']==0){
                  ?>
                                 <tr>
                                    <td><span><img src="<?php echo $userval['profile_picture']; ?>" class="img-responsive"></span></td>
                                    <td class="name"><span>
                                          <?php echo isset($userval['first_name']) ? $userval['first_name'] : ''; ?>
                                          <?php echo isset($userval['last_name']) ? $userval['last_name'] : ''; ?></span></td>
                                    <td><span>
                                          <?php echo isset($userval['email']) ? $userval['email'] : ''; ?></span></td>
                                    <td><span>
                                          <?php echo isset($userval['phone']) ? $userval['phone'] : ''; ?></span></td>
                                    <!--                      <td><span><?php //echo (isset($userval['display_plan_name']) && $userval['display_plan_name'] != "") ? $userval['display_plan_name'] : 'No plan selected'; ?></span></td>-->
                                    <td class="action">
                                       <span>
                                          <a href="javascript:void(0);" data-id="<?php echo $userval['id']; ?>" class="edit_subuser" data-user_type="designer">
                                             <i class="fas fa-pen"></i>
                                          </a>
                                          <a href="javascript:void(0)" data-target="#deletesubuser" data-toggle="modal" data-id="<?php echo $userval['id']; ?>" class="delete_clients">
                                             <i class="far fa-trash-alt"></i>
                                          </a>
                                          <div class="switch-custom-usersetting-check activate-user">
                                             <input type="checkbox" name="enable_disable_set" data-userid="<?php echo $userval['id']; ?>" data-email="<?php echo $userval['email']; ?>" data-name="<?php echo $userval['first_name']; ?>" id="enable_disable_set_<?php echo $userval['id']; ?>" <?php if ($userval['is_active']==1) { echo 'checked' ; } else { } ?>>
                                             <label for="enable_disable_set_<?php echo $userval['id']; ?>"></label>
                                          </div>
                                       </span>
                                       <?php if($userval["mode"][0]["payment_mode"] == 1 && ($userval['customer_id'] == "" || $userval['plan_name'] == "")){ ?>
                                       <a href="javascript:void(0)" class="active_blling_acont" data-target="#activateclientbilling" data-toggle="modal" data-id="<?php echo $userval['id']; ?>">Activate Billing</a>
                                       <?php } ?>
                                    </td>
                                 </tr>
                                 <?php } }
                 }else{
                     echo "<div class='no_found'>Designer not found</div>"; 
                 }?>
                              </table>
                           </div>
                        </div>
                        <div class="tab-pane" id="client-tab">
                           <div class="clint managment-list">
                              <table>
                                 <?php if(!empty($alluserData['customer'])){ 
//                     echo "<pre/>";print_R($designerdata);
                 foreach ($alluserData['customer'] as $userkey => $userval) {  

                  if($userval['is_delete']==0){
                  ?>
                                 <tr>
                                    <td><span><img src="<?php echo $userval['profile_picture']; ?>" class="img-responsive"></span></td>
                                    <td class="name"><span>
                                          <?php echo isset($userval['first_name']) ? $userval['first_name'] : ''; ?>
                                          <?php echo isset($userval['last_name']) ? $userval['last_name'] : ''; ?></span></td>
                                    <td><span>
                                          <?php echo isset($userval['email']) ? $userval['email'] : ''; ?></span></td>
                                    <td><span>
                                          <?php echo isset($userval['phone']) ? $userval['phone'] : ''; ?></span></td>
                                    <td><span>
                                          <?php echo (isset($userval['display_plan_name']) && $userval['display_plan_name'] != "") ? $userval['display_plan_name'] : 'No plan selected'; ?></span></td>
                                    <?php if($permission["assigndes_to_client"] != 0){ ?>
                                    <td>
                                       <?php if ($userval['is_designer_assign'] != 0) { ?>
                                       <div class="cli-ent-xbox">
                                          <p class="text-h " title="<?php echo $userval['designer_first_name'] . " " . $userval['designer_last_name']; ?>">
                                             <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign" data-customerid="<?php echo $userval['id']; ?>">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer_edit.svg">
                                             </a>
                                             <?php echo $userval['designer_first_name']; ?>
                                             <?php
                                        if (strlen($userval['designer_last_name']) > 7) {
                                            echo ucwords(substr($userval['designer_last_name'], 0, 1));
                                        } else {
                                            echo $userval['designer_last_name'] ;
                                        }
                                        ?>
                                          </p>
                                       </div>
                                       <?php } else { ?>
                                       <div class="cli-ent-xbox">
                                          <a href="#" class="upl-oadfi-le noborder permanent_assign_design assign_de" data-toggle="modal" data-target="#AddPermaDesign" data-customerid="<?php echo $userval['id']; ?>">
                                             <p class="attachfile"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer_red.svg">
                                                <span>Assign designer</span></p>
                                          </a>
                                       </div>
                                       <?php } ?>
                                    </td>
                                    <?php } 
                   if($permission["manage_clients"] != 0){ ?>
                                    <td>
                                       <div class="cli-ent-xbox <?php echo $userval['id']; ?>">
                                          <p class="text-h " title="<?php echo $userval['va_first_name'] . " " . $userval['va_last_name']; ?>"></p>
                                       </div>
                                       <?php if ($userval['is_Va_assign'] != 0) { ?>
                                       <div class="cli-ent-xbox <?php echo $userval['id']; ?>">
                                          <p class="text-h " title="<?php echo $userval['va_first_name'] . " " . $userval['va_last_name']; ?>">
                                             <a href="#" class="assign_va assign_saasVa" data-toggle="modal" data-target="#Addva" data-requestid="<?php echo $userval['id']; ?>" data-designerid="">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer_edit.svg">
                                             </a>
                                             <?php echo $userval['va_first_name']; ?>
                                             <?php
                           if (strlen($userval['va_last_name']) > 5) {
                               echo ucwords(substr($userval['va_last_name'], 0, 3));
                           } else {
                               echo $userval['va_last_name'] ;
                           }
                           ?>
                                          </p>
                                       </div>
                                       <?php }else{  ?>
                                       <a href="#" class="assign_va assign_saasVa" data-toggle="modal" data-target="#Addva" data-requestid="<?php echo $userval['id']; ?>" data-designerid="">
                                          <img src="<?php echo base_url();?>public/assets/img/gz_icons/assign_designer_red.svg" class="img-responsive"><span>Add VA</span>
                                       </a>
                                       <?php } ?>
                                    </td>
                                    <?php } ?>
                                    <?php if($permission["manage_clients"] != 0){ ?>
                                    <td class="action">
                                       <span>
                                          <!-- <a href="<?php echo base_url();?>account/client_management/<?php echo $userval['id']; ?>#client_management" data-id="<?php echo $userval['id']; ?>" class="edit_subuser-op" data-user_type="client"> -->
                                          <a href="javascript:void(0);" data-id="<?php echo $userval['id']; ?>" class="edit_subuser-op" data-user_type="client">
                                             <i class="fas fa-pen"></i>
                                          </a>
                                          <a href="javascript:void(0)" data-target="#deletesubuser" data-toggle="modal" data-id="<?php echo $userval['id']; ?>" class="delete_clients">
                                             <i class="far fa-trash-alt"></i>
                                          </a>
                                          <div class="switch-custom-usersetting-check activate-user">
                                             <input type="checkbox" name="enable_disable_set" data-userid="<?php echo $userval['id']; ?>" data-email="<?php echo $userval['email']; ?>" data-name="<?php echo $userval['first_name']; ?>" id="enable_disable_set_<?php echo $userval['id']; ?>" <?php if ($userval['is_active']==1) { echo 'checked' ; } else { } ?>>
                                             <label for="enable_disable_set_<?php echo $userval['id']; ?>"></label>
                                          </div>
                                       </span>
                                       <?php if($userval["mode"][0]["payment_mode"] == 1 && ($userval['customer_id'] == "" || $userval['plan_name'] == "")){ ?>
                                       <a href="javascript:void(0)" class="active_blling_acont" data-target="#activateclientbilling" data-toggle="modal" data-id="<?php echo $userval['id']; ?>">Activate Billing</a>
                                       <?php } ?>
                                    </td>
                                    <?php } ?>
                                 </tr>
                                 <?php } }
                 }else{
                     echo "<div class='no_found'>Customer not found</div>"; 
                 }?>
                              </table>
                           </div>
                        </div>
                        <div id="customer_editForm" style="display: none;">
                        </div>
                     </div>
                  </div>
                  <form method="post" role="form" class="sub_user_form" id="add_designer_saas">
                     <div id="new-subuser" style="display: none;">
                        <div class="edit-submt">
                           <div class="white-boundries" id="step1">
                              <!-- <input type="hidden" name="form-type" value="save"> -->
                              <div class="headerWithBtn">
                                 <h2 class="main-info-heading">New User</h2>
                                 <p class="fill-sub">Complete the form below to add new user.</p>
                                 <div class="addPlusbtn">
                                    <a class="backlist-go teamMangmnt-back">
                                       <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">Back
                                    </a>
                                 </div>
                              </div>
                              <input type="hidden" name="cid" value="" id="cid" />
                              <input type="hidden" name="cust_url" value="account/setting-view#management" id="cust_url" />
                              <div class="row">
                                 <div class="col-md-6">
                                    <label class="form-group">
                                       <p class="label-txt">First Name <span>*</span></p>
                                       <input type="text" name="first_name" class="input" id="fname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                       <div class="line-box">
                                          <div class="line"></div>
                                       </div>
                                    </label>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="form-group">
                                       <p class="label-txt">Last Name <span>*</span></p>
                                       <input type="text" name="last_name" class="input" id="lname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                       <div class="line-box">
                                          <div class="line"></div>
                                       </div>
                                    </label>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="form-group">
                                       <p class="label-txt">Email Address <span>*</span></p>
                                       <input type="email" class="input" name="email" id="user_email" data-rule="email" data-msg="Please enter a valid email" value="" required="">
                                       <div class="line-box">
                                          <div class="line"></div>
                                       </div>
                                    </label>
                                 </div>
                                 <div class="col-md-6">
                                    <label class="form-group">
                                       <p class="label-txt">Phone Number <span>*</span></p>
                                       <input type="tel" name="phone" class="input" id="phone" value="" required="">
                                       <div class="line-box">
                                          <div class="line"></div>
                                       </div>
                                    </label>
                                 </div>
                                 <div class="col-md-6 user_role_main">
                                    <label class="form-group">
                                       <p class="label-txt role-label">User Role <span>*</span></p>
                                       <div class="user-role">
                                          <div class="sound-signal">
                                             <div class="form-radion">
                                                <?php $clientsmsg=""; $attr="";  if($totalSubUserunderSAAS['customer'][0]['total_users'] >= $saasPlanDetails[0]['total_client_allow'] && $saasPlanDetails[0]['total_client_allow'] != "-1") { 
                                $clientsmsg = "clients";
                                $attr = "disabled"; 
                             }?>
                                                <input type="radio" name="user_role" class="preference color_codrr" id="Client" data-totalClientallow="<?php echo $saasPlanDetails[0]['total_client_allow']; ?>" data-totalClient="<?php echo $totalSubUserunderSAAS['customer'][0]['total_users']; ?>" value="customer" data-permission="<?php echo $PaymentpermiSsion[0]['online_payment']; ?>" <?php echo $attr; ?>>
                                                <label for="Client">Client</label>
                                                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-original-title="Client/Basic user can add new projects, change status of their projects and approve their projects."></i>
                                             </div>
                                          </div>
                                          <div class="sound-signal">
                                             <div class="form-radion">
                                                <?php 
                                $mangageUser = $totalSubUserunderSAAS['manager'][0]['total_users'] + $totalSubUserunderSAAS['designer'][0]['total_users'] ;
                                $designermsg=""; $attr="";  if($mangageUser >= $saasPlanDetails[0]['total_sub_user'] && $saasPlanDetails[0]['total_sub_user'] != "-1") { 
                                $designermsg = "designer";
                                $attr = "disabled"; 
                             }?>
                                                <input type="radio" name="user_role" class="preference color_codrr" id="designer" value="designer" data-totalClientallow="<?php echo $mangageUser; ?>" data-totalClient="<?php echo $totalSubUserunderSAAS['designer'][0]['total_users']; ?>" <?php echo $attr; ?> >
                                                <label for="designer">Designer</label>
                                                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-original-title="Designers can upload the design drafts for their projects and change project status."></i>
                                             </div>
                                          </div>
                                          <div class="sound-signal">
                                             <div class="form-radion">
                                                <?php $managermsg=""; $attr="";  if($mangageUser >= $saasPlanDetails[0]['total_sub_user'] && $saasPlanDetails[0]['total_sub_user'] != "-1") { 
                                $managermsg = "manager";
                                $attr = "disabled"; 
                             }?>
                                                <input type="radio" name="user_role" class="preference color_codrr" id="member" value="manager" data-totalClientallow="<?php echo $mangageUser; ?>" data-totalClient="<?php echo $totalSubUserunderSAAS['manager'][0]['total_users']; ?>" <?php echo $attr; ?>>
                                                <label for="member">Team-Member</label>
                                                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-original-title="Management usres are sub-admins who can manage projects, clients, settings & other information based on the permissions they have."></i>
                                             </div>
                                          </div>
                                          <?php if($clientsmsg !="" ||  $designermsg !="" || $managermsg !="" ){ ?>
                                          <div class="info-error">
                                             <span class="cst_info_error">
                                                As per your plan you can't add more
                                                <?php if($clientsmsg !="" && $designermsg !="" && $managermsg !="" ){ 
                                echo $clientsmsg." , ".$designermsg."&nbsp; and &nbsp;".$managermsg."."; 
                            
                             }else{ 

                                if($clientsmsg !=""){ 
                                    echo $clientsmsg."&nbsp; , &nbsp;"; 
                                } 
                                if ($designermsg !="") {
                                   echo $designermsg."&nbsp; and &nbsp;";
                               } 
                               if ($managermsg !="") {
                                echo $managermsg."."; 
                               } 
                            } ?>
                                                <a target="_blank" href="<?php echo base_url().'account/setting-view#billing'?>">Click here for upgrade </a>
                                             </span>
                                          </div>
                                          <?php } ?>
                                       </div>
                                    </label>
                                 </div>
                                 <div class="col-md-6 password_toggle" style="display:none">
                                    <div class="notify-lines">
                                       <p>Randomly generated password</p>
                                       <label class="form-group switch-custom-usersetting-check">
                                          <input type="checkbox" name="genrate_password" id="password_ques" checked />
                                          <label for="password_ques"></label>
                                       </label>
                                    </div>
                                    <div class="create_password" style="display:none">
                                       <label class="form-group">
                                          <p class="label-txt">Password<span>*</span></p>
                                          <input type="password" name="password" class="input" id="password" value="" disabled="disabled" />
                                          <div class="line-box">
                                             <div class="line"></div>
                                          </div>
                                       </label>
                                    </div>
                                 </div>
                                 <div class="designer_permsn" style="display:none;">
                                    <div class="notify-lines">
                                       <p>See all the project or only the assigned ones</p>
                                       <label class="form-group switch-custom-usersetting-check">
                                          <input type="checkbox" name="all_projects" id="all_projects" checked />
                                          <label for="all_projects"></label>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                              <?php if($clientsmsg != "" &&  $designermsg!= ""    &&  $managermsg!="" ){ ?>
                              <span data-isall="1" id="is_all"></span>
                              <?php } ?>
                              <div class="wizard_btn_list btn-here step1">
                                 <div class="next-form">
                                    <input type="button" name="save_subuser" class="btn-red center-block next-w" value="next">
                                 </div>
                                 <div class="submit-form" style="display:none">
                                    <input type="submit" name="save_subuser" class="btn-red center-block" value="SAVE">
                                 </div>
                              </div>
                              <!--    <input  type="button" name="save_subuser" class="btn-red center-block next" value="next">
    <input type="submit" name="save_subuser" class="btn-red center-block" value="SAVE">-->
                           </div>
                           <div style="display:none;" class="white-boundries" id="step2">
                              <!-- Customer selection  start -->
                              <div class="row subscription-plan" style="display: none;">
                                 <div class="col-md-6">
                                    <label class="form-group">
                                       <p class="label-txt label-active clint_cll">Subscription Plan</p>
                                       <select id="requests_type" class="input" name="requests_type" required="">
                                          <option value="">Select Subscription Plan</option>
                                          <?php foreach ($active_subscriptions as $subs) { ?>
                                          <option value="<?php echo $subs['plan_id'] ?>" data-paymentmode="<?php echo $subs['payment_mode']; ?>" data-inprogress="<?php echo $subs['global_inprogress_request']; ?>" data-plan_type="<?php echo $subs['plan_type_name']; ?>" id="option_<?php echo $subs['plan_id'] ?>" data-shared="<?php echo $subs['shared_user_type']; ?>" data-payment="<?php echo $subs['payment_mode']; ?>">
                                             <?php echo $subs['plan_name']; echo ($subs['payment_mode'] == 0)?" (Offline)":" (Online)";  ?>
                                          </option>
                                          <?php } ?>
                                       </select>
                                       <div class="line-box">
                                          <div class="line"></div>
                                       </div>
                                       <p class="note_text fill-sub"><span>Please choose subscription plan for this user. Manage your plans in subscription module.</span></p>
                                    </label>
                                 </div>
                                 <div class="show_bypss_sec col-md-6" style="display:none">
                                    <div class="row">
                                       <h2 class="main-info-heading bypaspayment">Bypass Payment
                                          <label class="form-group switch-custom-usersetting-check f_bypss_payment">
                                             <input type="checkbox" name="f_bypass_payment" id="f_bypass_payment" />
                                             <label for="f_bypass_payment"></label>
                                          </label>
                                       </h2>
                                       <div class="f_card_details">
                                          <div class="col-md-6">
                                             <label class="form-group">
                                                <p class="label-txt label-active">Card Number <span>*</span></p>
                                                <input type="text" pattern="\d*" class="input f_card_number" name="card_number" required="" maxlength="16">
                                                <div class="line-box">
                                                   <div class="line"></div>
                                                </div>
                                             </label>
                                          </div>
                                          <div class="col-sm-6">
                                             <label class="form-group">
                                                <p class="label-txt label-active">Expiry Date <span>*</span></p>
                                                <input type="text" id="expir_date1" class="input expir_date f_expir_date" name="expir_date" onkeyup="dateFormat(this.value,this);" required="">
                                                <div class="line-box">
                                                   <div class="line"></div>
                                                </div>
                                             </label>
                                          </div>
                                          <div class="col-sm-6 cvv">
                                             <label class="form-group">
                                                <p class="label-txt label-active">CVV <span>*</span></p>
                                                <input type="text" pattern="\d*" name="cvc" class="input f_cvv" maxlength="4" required="">
                                                <div class="line-box">
                                                   <div class="line"></div>
                                                </div>
                                             </label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- Customer selection  end -->
                              <div class="view_only_prmsn_main" style="display: none; ">
                                 <div class="view_only_prmsn">
                                    <div class="access-brand">
                                       <h3>Permissions</h3>
                                       <div class="notify-lines finish-line">
                                          <label class="switch-custom-usersetting-check">
                                             <input type="checkbox" name="view_only" class="form-control" id="view_only">
                                             <label for="view_only"></label>
                                          </label>
                                          <h3>View Only</h3>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row permissions_for_subuser_client">
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="add_requests" class="form-control" id="add_requests">
                                             <label for="add_requests"></label>
                                          </label>
                                          <p>Add Request</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="comnt_requests" class="form-control" id="comnt_requests">
                                             <label for="comnt_requests"></label>
                                          </label>
                                          <p>Comment on Request</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="brand_profile_access" class="form-control" id="brand_profile_access">
                                             <label for="brand_profile_access"></label>
                                          </label>
                                          <p>Access All Brands</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="add_brand_pro" class="form-control" id="add_brand_pro">
                                             <label for="add_brand_pro"></label>
                                          </label>
                                          <p>Add/Edit Brand Profile</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="del_requests" class="form-control" id="del_requests">
                                             <label for="del_requests"></label>
                                          </label>
                                          <p>Delete Request</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="billing_module" class="form-control" id="billing_module">
                                             <label for="billing_module"></label>
                                          </label>
                                          <p>Billing Module</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="file_management" class="form-control" id="file_management">
                                             <label for="file_management"></label>
                                          </label>
                                          <p>File Management</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="white_label" class="form-control" id="white_label">
                                             <label for="white_label"></label>
                                          </label>
                                          <p>White Label</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="upload_draft" class="form-control" id="upload_draft">
                                             <label for="upload_draft"></label>
                                          </label>
                                          <p>Upload Draft</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="assign_designer_to_client" class="form-control" id="assign_designer_to_client">
                                             <label for="assign_designer_to_client"></label>
                                          </label>
                                          <p>Designer assign to client</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="assign_designer_to_project" class="form-control" id="assign_designer_to_project">
                                             <label for="assign_designer_to_project"></label>
                                          </label>
                                          <p>Designer assign to project</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="approve_revise" class="form-control" id="approve_revise">
                                             <label for="approve_revise"></label>
                                          </label>
                                          <p>Approve/Revise</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="add_team_member" class="form-control" id="add_team_member">
                                             <label for="add_team_member"></label>
                                          </label>
                                          <p>Add team member</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="manage_clients" class="form-control" id="manage_clients">
                                             <label for="manage_clients"></label>
                                          </label>
                                          <p>Manage clients</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="change_project_status" class="form-control" id="change_project_status">
                                             <label for="change_project_status"></label>
                                          </label>
                                          <p>Change project status</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="review_design" class="form-control" id="review_design">
                                             <label for="review_design"></label>
                                          </label>
                                          <p>Review design</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="download_file" class="form-control" id="download_file">
                                             <label for="download_file"></label>
                                          </label>
                                          <p>Download file</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="approve_reject" class="form-control" id="approve_reject">
                                             <label for="approve_reject"></label>
                                          </label>
                                          <p>Approve/Reject Quality of design</p>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="notify-lines">
                                          <label class="switch-custom-usersetting-check uncheckview">
                                             <input type="checkbox" name="show_all_project" class="form-control" id="show_all_project">
                                             <label for="show_all_project"></label>
                                          </label>
                                          <p>Access all projects or only projects assigned to him/her</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="wizard_btn_list btn-here step2">
                                 <div class="back-form">
                                    <input type="button" name="save_subuser" class="btn-red center-block prev-w" value="prev">
                                 </div>
                                 <!-- <div class="next-form">
            <input  type="button" name="save_subuser" class="btn-red center-block next" value="next">
        </div> -->
                                 <div class="submit-form">
                                    <input type="submit" name="save_subuser" class="btn-red center-block" value="SAVE">
                                 </div>
                              </div>
                              <!--    <input  type="button" name="save_subuser" class="btn-red center-block next" value="next">
    <input type="submit" name="save_subuser" class="btn-red center-block" value="SAVE">-->
                           </div>
                        </div>
                     </div>
                  </form>
                  <input type="hidden" id="formOpen">
               </div>
            </div>
         </div>
         <div class="tab-pane" id="s_subscription">
            <div class="row">
               <div class="col-md-12">
                  <div class="white-boundries">
                     <h2 class="main-info-heading">Billing and Subscription</h2>
                     <p class="fill-sub">Update your Billing or Change your Subscription Plan below.</p>
                     <div class="row billing_plan_view">
                        <p class="btn-x">
                           <a href="javascript:void(0)" class="btn-f Change_Plan chossbpanss">
                              <?php echo (isset($s_plans["current"])) ? "Change your plan" : "Select your plan"; ?></a>
                        </p>
                        <div class="col-md-6 col-sm-6 current_actv_pln billing_plan">
                           <div class="flex-grid">
                              <div class="price-card best-offer">
                                 <div class="chooseplan active f_main_clnt">
                                    <?php
                                    if (isset($s_plans["current"])) {
                                        $pln_feature = explode("_", $s_plans["current"][0]['features']);
                                        $plnfeature = "<ul>";
                                        if (!empty($pln_feature)) {
                                            foreach ($pln_feature as $text) {
                                                $plnfeature .= "<li>" . $text . "</li>";
                                            }
                                        }
                                        $plnfeature .= "<ul>";
                                        $agency_priceclass = "<span class='" . $s_plans["current"][0]['plan_type'] . " agency_price'> " . $s_plans["current"][0]['plan_price'] . "</span>";
                                        ?>
                                    <h2>
                                       <?php echo $s_plans["current"][0]['plan_name']; ?>
                                    </h2>
                                    <h3>
                                       <font>$</font>
                                       <?php echo $agency_priceclass; ?><span>/
                                          <?php echo $s_plans["current"][0]['plan_type']; ?></span>
                                    </h3>
                                    <div class="p-benifite">
                                       <?php if ($type_ofuser == 'fortynine_plans' || $type_ofuser == 'one_time') { ?>
                                       <ul>
                                          <li>
                                             <p>Total Requests:
                                                <?php echo $data[0]['total_requests']; ?>
                                             </p>
                                          </li>
                                          <li>
                                             <p>Added Requests:
                                                <?php echo $request['added_request']; ?>
                                             </p>
                                          </li>
                                          <li>
                                             <p>Pending Requests:
                                                <?php echo $request['pending_req']; ?>
                                             </p>
                                          </li>
                                       </ul>
                                       <?php
                                            } else {
                                                if (!empty($s_plans["current"][0]['tier_prices'])) {
                                                    if ($s_plans["current"][0]['plan_type'] == 'yearly') {
                                                        $amount = 'annual_price';
                                                    } else {
                                                        $amount = 'amount';
                                                    }
                                                    $options = "";
                                                    foreach ($s_plans["current"][0]['tier_prices'] as $tier_prices) {
                                                        $options .= '<option value ="' . $tier_prices['quantity'] . '" data-amount="' . $tier_prices[$amount] . '" data-annual_price="' . $tier_prices["amount"] . '">' . $tier_prices['quantity'] . ' Dedicated Designer</option>';
                                                    }
                                                    $features = str_replace('{{QUANTTY}}', $options, $plnfeature);
                                                    echo $features;
                                                } else {
                                                    echo $plnfeature;
                                                }
                                            }
                                            ?>
                                    </div>
                                    <div class="price-sign-up">
                                       <a type="button" class="ud-dat-p currentPlan" disabled>
                                          Current Plan
                                       </a>
                                    </div>
                                    <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
                                    <?php
                                    } else {
                                        echo "no selected Plan";
                                    }
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <p class="btn-x bckbtntoback" style="display:none">
                        <a href="javascript:void(0)" class="btn-g Change_Plan backfromnewplan">Back</a>
                     </p>
                     <div class="f_chng_ugrd_blpln" style="display:none">
                        <?php $this->load->view('account/s_plan_listing', array("userplans" => $s_plans["all"],"currentplan" => $s_plans["current"])); ?>
                     </div>
                     <?php if($SaaSPlan[0]['plan_id'] != '') { ?>
                     <a href="mailto:<?php echo SUPPORT_EMAIL; ?>" class="btn-f cancel_Plan" style="color: var(--theme-primary-color);text-decoration: underline;">Cancel Plan</a>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<div class="modal fade similar-prop" id="accessdenieduser" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Access Denied</h2>
            <div class="cross_popup" data-dismiss="modal"> x </div>
         </header>
         <div class="cli-ent-model">
            <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
            <h3 class="head-c text-center">You don't have access to add user. Please contact us for more info. </h3>
         </div>
      </div>
   </div>
</div>
<!-- Confirm popup for change plan -->
<div class="modal fade similar-prop" id="CancelPopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="cli-ent-model-box">
            <header class="fo-rm-header">
               <h2 class="popup_h2 del-txt">Cancel Subscription?</h2>
               <div class="cross_popup" data-dismiss="modal">x </div>
            </header>
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
               <h3 class="head-c text-center">Why you want to cancel your subscription?</h3>
               <form action="<?php echo base_url(); ?>account/ChangeProfile/cancel_current_plan" method="post" role="form" id="form_cancel_pop">
                  <select name="reason" class="form-control select-a" id="change_reason" required>
                     <option value="">Please choose one</option>
                     <option value="Designs are taking too long to complete">Designs are taking too long to complete</option>
                     <option value="Designers are not understanding my request">Designers are not understanding my request</option>
                     <option value="The design quality is not good">The design quality is not good</option>
                     <option value="The cost is too expensive for me">The cost is too expensive for me</option>
                     <option value="I don't need the service anymore">I don't need the service anymore</option>
                     <option value="My business has closed and I no longer need the service">My business has closed and I no longer need the service</option>
                     <option value="The platform is too difficult to use">The platform is too difficult to use</option>
                     <option value="Customer service is unsatisfactory">Customer service is unsatisfactory</option>
                     <option value="The system lacks the integrations we need">The system lacks the integrations we need</option>
                     <option value="This was a test account">This was a test account</option>
                     <option value="We are going with another service provider or in-house designer">We are going with another service provider or in-house designer</option>
                  </select>
                  <input type="hidden" name="is_copon_applied" value="<?php echo $checkcouponapplied;?>" />
                  <div class="error_sel"></div>
                  <div class="switch_designer_box" style="display:none">
                     <label>Do you want to switch your designer ?</label>
                     <div class="yes-radio">
                        <input id="d_yes" type="radio" name="radio_des" class="yes_rad" value="yes">
                        <label for="d_yes">Yes</label>
                     </div>
                     <div class="yes-radio">
                        <input id="d_no" type="radio" name="radio_des" class="no_rad" value="no">
                        <label for="d_no">No</label>
                     </div>
                     <!--<div class="chage-offr-desinr"><p>We will give <span> 20% off </span> the next month for this inconvenience.</p></div>-->
                  </div>
                  <div class='give_reason ur-exp' style='display:none'>
                     <label>Which one and why ? 
                        <textarea name='reason_desc' id="reason_desc"></textarea>
                     </label>
                  </div>
                  <div class="form-radion ur-exp">
                     <label class="form-group">
                        <p class=""> Please tell us how could have made this service better and if there is anything else you would like to share with us. </p>
                        <textarea name='cancel_feebback' id="cancel_feebback"></textarea>
                     </label>
                  </div>
                  <a class="btn btn-red close-cancel" href='javascript:void(0)' data-dismiss="modal">Close</a>
                  <?php if($login_user_data[0]['user_flag'] != 'client'){ ?>
                  <button class="btn btn-red reason_submit reason_submit_disable" data-is_already_cancelled="<?php echo $isalreadyrequested; ?>" data-is_coupn_applied="<?php echo $checkcouponapplied;?>" name="reason_submit" type="submit">Proceed</button>
                  <?php }else { ?>
                  <button class="btn btn-red" name="reason_submit_client" type="submit">Submit</button>
                  <?php } ?>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- End Modal popup -->
<!--------start 20% off popup *****-->
<div class="modal fade slide-3 similar-prop model-close-button in" id="prompt_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Stay with us</h2>
            <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
         </header>
         <div class="modal-body">
            <div class="stay-offer">
               <p>Stay with us and we will give you <span> 20% <small>off </small></span> for the next month.</p>
            </div>
            <div class="congrts">
               <form action="<?php echo base_url(); ?>account/ChangeProfile/cancel_current_plan" method="post">
                  <input type="hidden" name="customer_id" class="customer_id" value="" />
                  <input type="hidden" name="reason_val" class="reason_val" value="" />
                  <input type="hidden" name="is_switch_designer" class="is_switch_designer" value="" />
                  <input type="hidden" name="feedback_val" class="feedback_val" value="" />
                  <input type="hidden" name="why_reason" class="why_reason" value="" />
                  <button class="btn btn-red yes_apply" name="yes_apply" type="submit">Get 20% Off!</button>
                  <button class="no_apply" name="no_apply">Cancel anyway
                     <!--                    <small>I don't want to claim this offer</small>-->
                  </button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!--------end 20% off popup *****-->
<!--------Switch designer popup start *****-->
<div class="modal fade slide-3 similar-prop model-close-button in" id="switch_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Confirmation</h2>
            <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
         </header>
         <div class="modal-body">
            <div class="stay-offer sure-switch">
               <p>
                  We will switch your designer & give 20% off the next month for this inconvenience.
                  <span> 20% off</span>
               </p>
            </div>
            <div class="congrts">
               <form action="<?php echo base_url(); ?>account/ChangeProfile/cancel_current_plan" method="post">
                  <input type="hidden" name="customer_id" class="customer_id" value="" />
                  <input type="hidden" name="reason_val" class="reason_val" value="" />
                  <input type="hidden" name="is_switch_designer" class="is_switch_designer" value="" />
                  <input type="hidden" name="feedback_val" class="feedback_val" value="" />
                  <button class="btn btn-red yes_switch_cnfrm" name="yes_switch_cnfrm" type="submit">Confirm</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!--------Switch designer popup end *****-->
<!--------start 20% off popup *****-->
<div class="modal fade slide-3 similar-prop model-close-button in" id="sorry_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt"></h2>
            <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
         </header>
         <div class="modal-body">
            <div class="sorry-subscr">
               <p>Your cancellation request is already in process,<br /> Your account manager will contact you <br /> within 1-2 business days.<br />Thanks!</p>
            </div>
         </div>
      </div>
   </div>
</div>
<!--------end 20% off popup *****-->
<div class="modal fade similar-prop" id="deletesubuser" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="cli-ent-model-box">
            <header class="fo-rm-header">
               <h2 class="popup_h2 del-txt">Delete User</h2>
               <div class="cross_popup" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">
               <h3 class="head-c text-center">Are you sure you want to delete this user? </h3>
               <div class="confirmation_btn three-lbl text-center">
                  <a data-id="<?php echo $userval['id']; ?>" class="btn-ydelete btn designer_del">Yes</a>
                  <button class="btn btn-ndelete" data-dismiss="modal" aria-label="Close">No</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Assign designer popup start from here  -->
<div class="modal similar-prop fade" id="AddPermaDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-nose" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Add Designer</h2>
            <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
         </header>
         <div class="cli-ent-model-box">
            <div class="cli-ent-model">
               <div class="noti-listpopup">
                  <div class="newsetionlist">
                     <div class="cli-ent-row tr notificate">
                        <div class="cli-ent-col td" style="width: 32%;">
                           <div class="cli-ent-xbox text-left">
                              <h3 class="pro-head space-b">Designer</h3>
                           </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 43%;">
                           <div class="cli-ent-xbox text-left">
                              <h3 class="pro-head space-b">Skill</h3>
                           </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 25%;">
                           <div class="cli-ent-xbox text-left">
                              <h3 class="pro-head space-b text-center">Active Requests</h3>
                           </div>
                        </div>
                     </div>
                  </div>
                  <form action="<?php echo base_url(); ?>account/ChangeProfile/assign_designer_for_customer" method="post">
                     <ul class="list-unstyled list-notificate two-can">
                        <?php foreach ($alluserData['designer'] as $value): ?>
                        <?php if($value['is_delete'] == 0 && $value['is_active'] == 1){ ?>
                        <li>
                           <a href="#">
                              <div class="cli-ent-row tr notificate">
                                 <div class="cli-ent-col td">
                                    <div class="sound-signal">
                                       <div class="form-radion">
                                          <input class="selected_btn" type="radio" value="<?php echo $value['id']; ?>" name="assign_designer" id="<?php echo $value['id']; ?>" data-image-pic="<?php echo $value['profile_picture']?>" data-name="<?php echo $value['first_name'] ." ". $value['last_name'];?>">
                                          <label for="<?php echo $value['id'];?>" data-image-pic="<?php echo $value['profile_picture']?>"></label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cli-ent-col td" style="width: 30%;">
                                    <div class="cli-ent-xbox text-left">
                                       <div class="setnoti-fication">
                                          <figure class="pro-circle-k1">
                                             <img src="<?php echo $value['profile_picture'] ?>" class="img-responsive">
                                          </figure>
                                          <div class="notifitext">
                                             <p class="ntifittext-z1"><strong>
                                                   <?php echo $value['first_name'] . " " . $value['last_name']; ?></strong></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cli-ent-col td" style="width: 45%;">
                                    <div class="cli-ent-xbox text-left">
                                       <p class="pro-a">UX Designer, Landing page, Mobile App UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                    </div>
                                 </div>
                                 <div class="cli-ent-col td" style="width: 25%;">
                                    <div class="cli-ent-xbox text-left">
                                       <div class="cli-ent-xbox text-center">
                                          <p class="neft text-center"><span class="red text-uppercase">
                                                <?php echo $value['active_request']; ?></span></p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </a>
                        </li>
                        <?php } ?>
                        <?php endforeach; ?>
                     </ul>
                     <input type="hidden" name="customer_id" id="assign_customer_id">
                     <p class="space-c"></p>
                     <p class="btn-x text-center"><button name="submit" type="submit" class="red_gz_btn button" id="assign_designer_per">Assign Designer</button></p>
                     <!--<p class="btn-x text-center"><button name="submit" type="submit" class="load_more button" id="assign_designer_per" >Assign Designer</button></p>-->
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Assign designer popup End here  -->
<!-- Assign Qa popup start  -->
<div class="modal similar-prop fade" id="Addva" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-nose" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Add VA</h2>
            <div id="close-pop" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
         </header>
         <div class="cli-ent-model-box">
            <div class="cli-ent-model">
               <div class="noti-listpopup">
                  <?php if(!empty($alluserData['manager'])){ ?>
                  <form action="" method="post">
                     <ul class="list-unstyled list-notificate two-can">
                        <?php foreach($alluserData['manager'] as $va){ ?>
                        <?php if($va['is_delete'] == 0 && $va['is_active'] == 1){ ?>
                        <li>
                           <a href="#">
                              <div class="cli-ent-row tr notificate va-adder">
                                 <div class="cli-ent-col td">
                                    <div class="sound-signal">
                                       <div class="form-radion">
                                          <input class="selected_btn" type="radio" value="<?php echo $va['id'];?>" name="assign_va" id="<?php echo $va['id'];?>" data-image-pic="<?php echo $va['profile_picture']?>" data-name="<?php echo $va['first_name'] ." ". $va['last_name'];?>">
                                          <label for="<?php echo $va['id'];?>" data-image-pic="<?php echo $va['profile_picture']?>"></label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cli-ent-col td">
                                    <div class="cli-ent-xbox text-left">
                                       <div class="setnoti-fication">
                                          <figure class="pro-circle-k1">
                                             <img src="<?php echo $va['profile_picture']?>" class="img-responsive">
                                          </figure>
                                          <div class="notifitext">
                                             <p class="ntifittext-z1">
                                                <strong>
                                                   <?php echo $va['first_name'] ." ". $va['last_name'];?>
                                                </strong>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </a>
                        </li>
                        <?php } } ?>
                     </ul>
                     <input type="hidden" name="allcustomers_id" id="allcustomers_id">
                     <p class="space-c"></p>
                     <p class="btn-x text-center">
                        <button name="submit" type="submit" id="assign_vatocus" class="load_more button" data-dismiss="modal" aria-label="Close">Assign Va</button>
                     </p>
                  </form>
                  <?php }else{
                    echo "No QA Added Yet.";
                } ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Assign Qa popup end  -->
<!-- <div style="display: none;" id="animatedModal" class="animated-modal">
     <div class="inner-content">
        <h3>Your Subscription Plan not allow to add More users</h3>
        <p>
            Please Upgrade Your Plan From here
        </p>
        <div class="subscription-button">
            <a href="">Subscribe for more </a>
        </div>
         
     </div>
</div>
 -->
<div class="modal fade similar-prop" id="notAllowedMoreuser" tabindex="-1" role="dialog" aria-labelledby="animatedModal" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="cli-ent-model-box">
            <header class="fo-rm-header">
               <h2 class="popup_h2 del-txt">Plan Upgrade</h2>
               <div class="cross_popup" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/billing_subscrptn.svg">
               <h3 class="head-c text-center">Your Current Plan not allow to add More users</h3>
               <p>Please upgrade your plan from here</p>
               <div class="confirmation_btn three-lbl text-center">
                  <a class="btn btn-ydelete" href="<?php echo base_url();?>/account/setting-view#billing">Subscribe for more</a>
                  <button class="btn btn-ndelete" data-dismiss="modal" aria-label="Close">No</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
$(".next-w").click(function() {
   if ($("#formOpen").val() == "add") {
      if ($("#add_designer_saas").valid()) {
         checkForStep2($(".sub_user_form input[type='radio']:checked").attr("id"));
         $("#step1").hide();
         $("#step2").show();
      }
   } else {
      if ($("#edit_designer_saas").valid()) {
         checkForStep2($(".sub_user_form input[type='radio']:checked").attr("id"));
         $("#step1").hide();
         $("#step2").show();
      }
   }

});

$(function() {

   $("#add_designer_saas").validate({
      // Specify validation rules
      rules: {

         first_name: "required",
         last_name: "required",
         password: "required",

         email: {
            required: true,
            email: true,
            remote: {
               param: {
                  url: "<?php echo base_url();?>account/DesignerManagement/EmailCheckerValidation",
                  type: "post",
                  data: {
                     editid: function() {
                        return $("#cid").val();
                     }
                  }
               },
               depends: function(element) {
                  // compare email address in form to hidden field
                  //                return ($("#formOpen").val()== "add");
               }
            }
         },
         user_role: { required: true },
         phone: {
            required: true,
            minlength: 10,
            maxlength: 10
         },

      }, // Specify validation error messages
      messages: {
         first_name: "Please enter your firstname",
         last_name: "Please enter your lastname",
         password: "Please enter your password",
         phone: {
            required: "Please provide a phone number",
            maxlength: "Your phone number must be at least 10 digits long"
         },
         email: {
            required: "Please enter your email address.",
            email: "Please enter a valid email address.",
            remote: "Email already in use!"
         },
         user_role: {
            required: "Please select user role"
         }
      },
      errorPlacement: function(error, element) {
         // console.log("element",element); 
         if (element[0].id == 'fname') {
            error.appendTo(element.parent());
         }
         if (element[0].id == 'lname') {
            error.appendTo(element.parent());
         }
         if (element[0].id == 'user_email') {
            error.appendTo(element.parent());
         }
         if (element[0].id == 'phone') {
            error.appendTo(element.parent());
         }
         if (element[0].id == 'Client') {
            error.appendTo(element.parent());
         }
         if (element[0].id == 'password') {
            error.appendTo(element.parent());
         }
         // else {
         //     error.insertAfter(element);
         // }
      },
      submitHandler: function(form) {
         // form.submit();
         return true;
      }
   });

});
$(".prev-w").click(function() {
   $(".step1").find(".next-form").show();
   $(".step1").find(".submit-form").hide();
   $("#step1").show();
   $("#step2").hide();


});
// $(".color_codrr").click(function(){
//    if($(this).attr("id") == "designer"){
//        
//        $(".step1").find(".next-form").hide();
//        $(".step1").find(".submit-form").show();
//        $("#tabidentity").val("designer_tab"); 
//        //is_subscription_allowUserAdd($(this).data("totalclientallow"),$(this).data("totalclient"),"designer"); 
//         enableDisable(1);
//    } 
// });

$(".color_codrr").click(function() {
   var permission;
   if ($(this).attr("id") == "Client") {
      $("#tabidentity").val("client_tab");
      permission = $(this).data("permission");
      if (permission == 1) {
         $(".step1").find(".next-form").show();
         $(".step1").find(".submit-form").hide();
      } else {
         $(".step1").find(".next-form").hide();
         $(".step1").find(".submit-form").show();
      }
      $(".view_only_prmsn_main").hide();
      $(".subscription-plan").show();
      $(".designer_permsn").css("display", "none");
      enableDisable(0);

   } else if ($(this).attr("id") == "designer") {
      $(".designer_permsn").css("display", "block");
      $(".step1").find(".next-form").hide();
      $(".step1").find(".submit-form").show();
      $("#tabidentity").val("designer_tab");
      //is_subscription_allowUserAdd($(this).data("totalclientallow"),$(this).data("totalclient"),"designer"); 
      enableDisable(1);
   } else if ($(this).attr("id") == "member") {
      $(".designer_permsn").css("display", "none");
      $("#tabidentity").val("manager_tab");
      $(".subscription-plan").hide();
      enableDisable(1);
      $(".f_card_details").find(".form-group").next(".input").addClass("sad");
      $(".view_only_prmsn_main").show();
      $(".step1").find(".next-form").show();
      $(".step1").find(".submit-form").hide();
   } else {
      $(".designer_permsn").css("display", "none");
      $(".subscription-plan").hide();

   }
});


function enableDisable(ok) {
   if (ok == 1) {
      $(".f_card_number").attr("disabled", true);
      $(".f_expir_date").attr("disabled", true);
      $(".f_cvv").attr("disabled", true);
      $("#requests_type").attr("disabled", true);
   } else {
      $(".f_card_number").attr("disabled", false);
      $(".f_expir_date").attr("disabled", false);
      $(".f_cvv").attr("disabled", false);
      $("#requests_type").attr("disabled", false);
   }

}

function checkForStep2(value) {
   if (value == "Client") {
      permission = $(this).data("permission");
      if (permission == 1) {
         $(".step1").find(".next-form").show();
         $(".step1").find(".submit-form").hide();
      } else {
         $(".step1").find(".next-form").hide();
         $(".step1").find(".submit-form").show();
      }
      $(".view_only_prmsn_main").hide();
      $(".subscription-plan").show();
      enableDisable(0);

   } else if (value == "member") {
      // alert("member"); 

      $(".subscription-plan").hide();
      enableDisable(1);
      $(".f_card_details").find(".form-group").next(".input").addClass("sad");

      $(".view_only_prmsn_main").show();
   } else {
      $(".subscription-plan").hide();
      enableDisable(1);
      $(".f_card_details").find(".form-group").next(".input").addClass("sad");
   }
}

$(document).ready(function() {
   var value = $(location).attr("href");
   var url = value.substr(value.indexOf("?") + 1);
   if (value.indexOf("?") != -1) {
      if (url != "") {
         $(".usrmangment").find("a").trigger('click');
         $("#" + url).trigger('click');

      }
   }
});

function formEmailValenable() {
   if ($("#formOpen").val() == "add") {
      return true;
   } else {
      return false;
   }
}
// function is_subscription_allowUserAdd(allow,alreadyAdded,role) {
//     console.log("allow",allow)
//     console.log("alreadyAdded",alreadyAdded)
//         if(role == "client"){
//              if(allow > alreadyAdded){
//                 $(".next-w").show();
//                 return true; 
//              }else{
//                 $(".next-w").hide();
//                 $("#notAllowedMoreuser").modal("show");
//                 return false; 
//             }
//          }else if(role == "designer"){
//              if(allow > alreadyAdded){
//                 $(".next-w").show();
//                 return true; 
//              }else
//              {
//                 $(".step1").find(".submit-form").hide(); 
//                 $("#notAllowedMoreuser").modal("show")
//                 return false; 
//             }
//          }
//          else if(role == "manager"){
//              if(allow > alreadyAdded){
//                 $(".next-w").show();
//                 return true; 
//              }else{
//                 $("#notAllowedMoreuser").modal("show")
//                 $(".next-w").hide();
//                 return false; 
//             }
//          }
//      }
</script>