  <style>
     .grid {
        display: grid;
        grid-gap: 10px;
        grid-template-columns: repeat(auto-fill, minmax(250px,1fr));
        grid-auto-rows: 20px;
    }
    .upload-gallery ul.thumbnil-img li .content img{
        width: 100% !important;
        height: inherit !important;
        
    }
    .upload-gallery ul.thumbnil-img li {
       width: 100%;
    }
     
    
    </style>

<?php $get_method =$this->router->fetch_method();  ?>
    <div class="ifrm_sec" style="display:none;">
    <iframe id="myiFrame" data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/Instructions_for_domain_subdomain.pdf" src="about:blank"></iframe>
</div>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-target="#disablecustomer" data-toggle="modal" id="disablecustomer_acc" style="display: none;">Open Modal</button>
<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#site_tour" id="website_tour" style="display: none;">Open Modal</button>-->

<!-- Model for show domain setting info to user -->
<div class="modal fade slide-3 model-close-button in similar-prop" id="setmaindomain" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
 
<div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="cli-ent-model-box">
            <header class="fo-rm-header">
               <h2 class="popup_h2 del-txt">Point your domain</h2>
               <div class="cross_popup" data-dismiss="modal">x</div>
            </header>
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/dns.png">
               <p><br/></p>
               <p>You can use your own domain or subdomain and the complete application with run there. For this, you will have to point your domain to <b>A records</b> to our server which is <b>3.17.33.76</b>.
                  For further instructions <a href="javascript:void(0)" target="_blank" class="domain_inst site_btn" id="domain_inst">click here</a> or you can contact us and we will be here to support you.</p>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade site_tour_popup" id="site_tour" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="owl-carousel owl-theme  modal-body">
                <div class="site_tour">
                <h5 class="modal-title">User Management</h5>
                <img alt="manage brand profile"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/user_manag.png" class="">
                <p>Add team members to your project and let them have a firsthand look of the design as well. Keep a track on the progress and all the updates done on your designs. </p>
                </div>
                <div class="site_tour">
                <h5 class="modal-title">Manage Brand Profile</h5>
                <img alt="manage brand profile"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/manage-brand-profile.png" class="">
                <p>Create a separate brand profile for each of your clients to save time on new design requests. </p>
                </div>
                <div class="site_tour">
                <h5 class="modal-title">Project Management</h5>
                <img alt="manage brand profile"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/add-new-project.png" class="">
                <p>Create and manage all your designs from one place to improve your project turn-around time. With this feature, create and save your work for client reference and user access.</p>
                </div>
                <div class="site_tour">
                <h5 class="modal-title">White Label Settings</h5>
                <img alt="manage brand profile"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/white-label.png" class="">
                <p>Why take the pain of creating and maintaining a website for your design services, when you can use built-up platform. </p>
                </div>
                <div class="site_tour">
                <h5 class="modal-title">Files & Folders </h5>
                <img alt="manage brand profile"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/file-folder.png" class="">
                <p>Stack all your designs at one place to have easy access whenever required. Define your designs according to the clientele and put a tab on each project for simpler navigation.</p>
                </div>
<!--                <div class="site_tour">
                <h5 class="modal-title">General Settings </h5>
                <img alt="manage brand profile"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/general-setting.png" class="">
                <p>Rather than having to upload the logo, brand guidelines, reference files each time with a request, you can build out the brand profiles for each of the separate brands you work with.</p>
                </div>-->
      </div>
        <div class="modal-footer">
                <button type="button" class="hide-msg btn dont_agn_show_popup" data-dismiss="modal">Hide this and don't show this again</button>
                <button type="button" class="btn" data-dismiss="modal">Skip for now</button>
        </div>
    </div>
  </div>
</div>
<div class="modal fade locked-updated" similar-prop id="ChangePlan" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog custom-modal" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
            
         </header>
         <div class="modal-body model-tour billing_plan">
             <div class="upgrd_pln_subs"> 
                 <i class="fas fa-user-lock"></i>
                 <?php if ($login_user_data[0]['user_flag'] == "client" && $login_user_plan[0]["payment_mode"] == 0 && $agency_info[0]['show_billing'] == 0) { ?>
                     You don't have access. Please  <a  data-toggle="modal" data-target="#contact_detail">contact</a> for more info.
                 <?php } else { ?>
                     You don't have access. Please   <a href="javascript:void(0);" class="f_upgrade-link">upgrade your plan </a> to access.
                 <?php } ?>
             </div>
         </div>
      </div>
   </div>
</div>
<!-- Confirm popup for change plan -->
<div class="modal fade similar-prop" id="CnfrmPopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt">Change Plan</h2>
        <div class="cross_popup" data-dismiss="modal"> x</div>
      </header>
      <div class="cli-ent-model-box">
        <div class="cli-ent-model">
          <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/change_plan.svg">

          <form action="<?php echo base_url(); ?>account/ChangeProfile/change_current_plan" method="post" role="form" class="ChangeCurrent_Plan" id="ChangeCurrent_Plan">
            <div class="fo-rm-header">
              <br>
              <?php if ($parent_user_data[0]['is_cancel_subscription'] == 1) { ?>
                <div class="del-brfrof cancel_subs_pop">
                  <h3 class="head-c text-center">Do you want to move auto cancelled requests to its previous state? </h3><br/>
                  <label> <input type="radio" name="reactivate_pro" id="restore_project_li" class="restore_project" value="yes" checked=""> Yes, Reactivate account and move requests</label>
                  <label><input type="radio" name="reactivate_pro" id="reactivate_only_li" class="reactivate_only" value="no"> Reactivate account only</label>
                </div>
              <?php } else { ?>
                <h3 class="head-c text-center">Are you sure you want to Change Current Plan? </h3>
              <?php } ?>
            </div>
            <input type="hidden" class="form-control plan_name_to_upgrade plan_name" name="plan_name" value="">
            <input type="hidden" class="form-control plan_price" name="plan_price" value="">
            <input type="hidden" class="form-control display_namepln" id="display_namepln" name="display_name" value="" >
            <input type="hidden" class="form-control in_progress_request" name="in_progress_request" value="1">
            <div class="confirmation_btn text-center">
              <div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
               <img class="loaderimg_forreactivate" src="">
             </div>
             <button class="btn btn-CnfrmChange btn-ndelete" id="chng_currnt_pln" type="submit">Yes</button>
             <button class="btn btn-ydelete" data-dismiss="modal" aria-label="Close">No</button>
           </div>
         </form>
       </div>
     </div>
   </div>
 </div>
</div>
<!-- End Modal popup -->
<!-- Confirm popup for change plan of saas user-->
<div class="modal fade similar-prop" id="s_cnfrmPopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt">Change Plan</h2>
        <div class="cross_popup" data-dismiss="modal"> x</div>
      </header>
      <div class="cli-ent-model-box">
        <div class="cli-ent-model">
          <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/change_plan.svg">

          <form action="<?php echo base_url(); ?>account/ChangeProfile/change_s_userplan" method="post" role="form" class="s_changeCurrent_Plan" id="s_changeCurrent_Plan">
            <div class="fo-rm-header">
              <br>
              <?php //if ($parent_user_data[0]['is_cancel_subscription'] == 1) { ?>
<!--                <div class="del-brfrof cancel_subs_pop">
                  <h3 class="head-c text-center">Do you want to move auto cancelled requests to its previous state? </h3><br/>
                  <label> <input type="radio" name="reactivate_pro" id="restore_project_li" class="restore_project" value="yes" checked=""> Yes, Reactivate account and move requests</label>
                  <label><input type="radio" name="reactivate_pro" id="reactivate_only_li" class="reactivate_only" value="no"> Reactivate account only</label>
                </div>-->
              <?php //} else { ?>
                <h3 class="head-c text-center">Are you sure you want to Change Current Plan? </h3>
              <?php //} ?>
            </div>
            <input type="hidden" class="form-control plan_name_to_upgrade plan_name" name="plan_name" value="">
            <input type="hidden" class="form-control plan_price" name="plan_price" value="">
            <input type="hidden" class="form-control display_namepln" id="s_display_namepln" name="display_name" value="" >
            <input type="hidden" class="form-control in_progress_request" name="in_progress_request" value="1">
            <div class="confirmation_btn text-center">
              <div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
               <img class="loaderimg_forreactivate" src="">
             </div>
             <button class="btn btn-CnfrmChange btn-ndelete" id="s_chng_currnt_pln" type="submit">Yes</button>
             <button class="btn btn-ydelete" data-dismiss="modal" aria-label="Close">No</button>
           </div>
         </form>
       </div>
     </div>
   </div>
 </div>
</div>
<!-- End Modal popup -->
<div class="modal fade slide-2 similar-prop model-close-button in" id="s_upgradeacoount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt" id="myModalLabel"> Personal Billing Information</h2>
        <div class="cross_popup" data-dismiss="modal" aria-label="Close">x</div>
      </header>
      <div class="modal-body">
        <div class="bill-infoo">
          <div class="error-msg"></div>
          <div class="pb-info">
            <form action="" method="post" role="form" class="contactForm" id="s_upgrade_useraccount">
              <div class="row">
                <input type="hidden" class="form-control subuser_id" name="customer_id" id="s_customer_id" value="<?php echo $_SESSION['user_id']; ?>" required="">
                <input type="hidden" class="form-control plan_name" name="plan_name" id="s_plan_name" value="">
                <input type="hidden" class="form-control plan_price" name="plan_price" id="s_plan_price" value="">
                <input type="hidden" id="s_final_plan_price" class="final_plan_price" name="final_plan_price" value="" />
                <input type="hidden" id="s_subs_plan_price" class="subs_plan_price" name="subs_plan_price" value="" />
                <input type="hidden" id="s_state_tax" class="state_tax" name="state_tax" value="0"/>
                <input type="hidden" id="s_tax_amount" class="tax_amount"  name="tax_amount" value="0"/>
                <input type="hidden" class="form-control in_progress_request" name="in_progress_request" id="s_in_progress_request" value="1">
                <input type="hidden" class="form-control" name="email" id="s_email" value="<?php echo $login_user_data[0]['email']; ?>">
                <input type="hidden" id="s_couponinserted_ornot" class="couponinserted_ornot" name="couponinserted_ornot" value=""/>
                <label class="form-group">
                  <p class="label-txt">Card Number <span>*</span></p>
                  <input type="text" class="input" name="card_number" id="s_card_number" required="" maxlength="16">
                  <div class="line-box">
                    <div class="line"></div>
                  </div>
                </label>
                <div class="row">
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt label-active">Expiry Date <span>*</span></p>
                      <input type="text" name="expir_date" id="s_expir_date" class="expir_date input" placeholder="MM  /  YY" onkeyup="dateFormat(this.value,this);" required="">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                    </label>
                  </div>
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt">CVV <span>*</span></p>
                      <input type="text" name="cvc" id="s_cvc" class="input" maxlength="4" required="">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                    </label>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt">Discount</p>
                      <input type="text" class="input disc-code" id="s_discCode" name="Discount">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                      <div class="ErrorMsg epassError discount-code">
                      </div> 
                      <div class="ErrorMsg epassErrorSuccess discount-code"><span class="CouponErr_code"></span></div>
                    </label>
                  </div>
                </div>
                <div class="total-pPrice">
                  <div class="col-lg-12 toatal-bill-p tax_prc_for_texas"></div>
                  <div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p total-biled text-left">Total Billed</div>
                  <div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p text-right"><span class="coupon-des-newsignup"></span></div>
                </div>

                <div class="col-md-12 text-left">
                  <div class="sound-signal2">
                    <div class="form-radion2">
                      <p class="terms-txt">By clicking the below button, you agree to our <a href="<?php echo base_url() ?>termsandconditions" target="_blank">Terms</a> and that you have read our <a href="<?php echo base_url() ?>privacypolicy" target="_blank">Data Use Policy</a>, including our Cookie Use. </p>                       </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="loading_account_procs"></div>
              <button class="button upgrd-pln-2" id="s_payndupgrade" type="submit">Pay and Upgrade Plan</button>
              <div class="card-sec row">
                <div class="col-md-5 col-sm-5 col-xs-6">
                  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/stripe-lock.png" class="img-fluid stripe-lock">
                </div>
                <div class="col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-5 col-xs-offset-1 text-left">
                  <p>Cards accepted:</p>
                  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/payment-systm.png" class="img-fluid">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- upload multiple custom files -->
<div class="modal fade slide-3 similar-prop model-close-button in" id="f_uploadfiles_custom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Upload File</h2>
                <div class="cross_popup" data-dismiss="modal"> x</div>
            </header>
            <div class="testemail_section">
                <div class="email_header_temp_cont">
                    <?php //echo $email_header_footer_sec['header_part'];   ?>
                </div>
                <form method="POST" action="" enctype="multipart/form-data" id="f_uploadcustomfile">
                    <div class="fo-rm-body">
                        <div class="row">
                            <input type="hidden" class="form-control input-d from" name="from" value="">
                            <input type="hidden" class="form-control input-d folder_id" name="folder_id" value="0">
                            <div class="col-sm-12">
                                <div class="FolderPathForUpload">
                                    <div class="inner_path">
                                        <strong>File Path : </strong>
                                        <span> </span>
                                    </div>

                                </div>
                                <div class="form-group goup-x1 goup_1">
                                    <p class="space-a"></p>
                                    <div class="file-drop-area">
                                        <span class="fake-img"><img src="<?php echo FS_PATH; ?>public/assets/img/icon-cloud.png" class="img-responsive"></span>
                                        <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                        <input type="hidden" class="f_file_id" multiple="" name="id" id="f_file_id"/>    
                                        <input type="file" class="file-input project-file-input f_custom_files" multiple="" name="file_upload[]" id="f_custom_files"/>


                                    </div>
                                    <p class="allowed-type  text-left">Allowed file types : Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov,Ppt, Pptx </p>
                                    <p class="space-e"></p>
                                    <div class="fileuploads_customfolders row">                      
                                    </div>
                                </div>
                                <div class="f_err_upld"></div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group goup-x1">
                                    <div class="cell-col">
                                        <p class="btn-x bl-ogbtn text-right">
                                            <button type="submit" name="submit" class="btn-g text-uppercase f_uploadcustom_file">Save</button>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-------------------create popups-------------------->
<button style="display: none;" id="Select_PackForUP" data-toggle="modal" data-target="#SelectPackForUP">click here</button>
<button style="display: none;" id="upgrade_account" data-toggle="modal" data-target="#Upgradeacoount">click here</button>
<button style="display: none;" id="s_upgrade_account" data-toggle="modal" data-target="#s_upgradeacoount">click here</button>
<button style="display: none;" id="subscribed_USR" data-toggle="modal" data-target="#subscribedUSR">click here</button>
<!-- Modal for select package to upgrade -->
<div class="modal fade slide-1 in model-close-button locked-updated" id="SelectPackForUP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
   <div class="modal-dialog custom-modal" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <p class="iflogo_selected"></p>
            <div class="cross_popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></div>
         </header>
         <div class="modal-body model-tour clearfix">
             <div class="upgrd_pln_subs"> 
                 <i class="fas fa-user-lock"></i>
                 <?php if ($login_user_data[0]['user_flag'] == "client" && $login_user_plan[0]["payment_mode"] == 0 && $agency_info[0]['show_billing'] == 0) { ?>
                     You don't have access. Please  <a  data-toggle="modal" data-target="#contact_detail">contact</a> for more info.
                 <?php } else { ?>
                     You don't have access. Please   <a href="javascript:void(0);" class="f_upgrade-link">upgrade your plan </a> to access.
                 <?php } ?>
             </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal for upgrade account -->
<div class="modal fade slide-2 similar-prop model-close-button in" id="Upgradeacoount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt" id="myModalLabel"> Personal Billing Information</h2>
        <div class="cross_popup" data-dismiss="modal" aria-label="Close">x</div>
      </header>
      <div class="modal-body">
        <div class="bill-infoo">
          <div class="error-msg"></div>
          <div class="pb-info">
            <form action="" method="post" role="form" class="contactForm" id="upgrade_useraccount">
              <div class="row">
                <input type="hidden" class="form-control subuser_id" name="customer_id" id="customer_id" value="<?php echo $_SESSION['user_id']; ?>" required="">
                <input type="hidden" class="form-control plan_name" name="plan_name" id="plan_name" value="">
                <input type="hidden" class="form-control plan_price" name="plan_price" id="plan_price" value="">
                <input type="hidden" id="final_plan_price" class="final_plan_price" name="final_plan_price" value="" />
                <input type="hidden" id="subs_plan_price" class="subs_plan_price" name="subs_plan_price" value="" />
                <input type="hidden" id="state_tax" class="state_tax" name="state_tax" value="0"/>
                <input type="hidden" id="tax_amount" class="tax_amount"  name="tax_amount" value="0"/>
                <input type="hidden" class="form-control in_progress_request" name="in_progress_request" id="in_progress_request" value="1">
                <input type="hidden" class="form-control" name="email" id="email" value="<?php echo $login_user_data[0]['email']; ?>">
                <input type="hidden" id="couponinserted_ornot" class="couponinserted_ornot" name="couponinserted_ornot" value=""/>
                <label class="form-group">
                  <p class="label-txt">Card Number <span>*</span></p>
                  <input type="text" class="input" name="card_number" id="card_number" required="" maxlength="16">
                  <div class="line-box">
                    <div class="line"></div>
                  </div>
                </label>
                <div class="row">
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt label-active">Expiry Date <span>*</span></p>
                      <input type="text" name="expir_date" id="expir_date" class="expir_date input" placeholder="MM  /  YY" onkeyup="dateFormat(this.value,this);" required="">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                    </label>
                  </div>
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt">CVV <span>*</span></p>
                      <input type="text" name="cvc" id="cvc" class="input" maxlength="4" required="">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                    </label>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt">Discount</p>
                      <input type="text" class="input disc-code" id="discCode" name="Discount">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                      <div class="ErrorMsg epassError discount-code">
                      </div> 
                      <div class="ErrorMsg epassErrorSuccess discount-code"><span class="CouponErr_code"></span></div>
                    </label>
                  </div>
                  <div class="col-md-6">

                    <label class="form-group">
                      <p class="label-txt label-active">State <span>*</span></p>
                      <select name="state" id="state_upgrd" class="input state_upgrd" required="">
                        <option value="" selected="selected">Select a State</option>
                        <option value="International Customer">International Customer</option>
                        <option value="---">-----------------------</option>
                        <option value="Alabama">Alabama</option>
                        <option value="Alaska">Alaska</option>
                        <option value="Arizona">Arizona</option>
                        <option value="Arkansas">Arkansas</option>
                        <option value="California">California</option>
                        <option value="Colorado">Colorado</option>
                        <option value="Connecticut">Connecticut</option>
                        <option value="Delaware">Delaware</option>
                        <option value="District Of Columbia">District Of Columbia</option>
                        <option value="Florida">Florida</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Hawaii">Hawaii</option>
                        <option value="Idaho">Idaho</option>
                        <option value="Illinois">Illinois</option>
                        <option value="Indiana">Indiana</option>
                        <option value="Iowa">Iowa</option>
                        <option value="Kansas">Kansas</option>
                        <option value="Kentucky">Kentucky</option>
                        <option value="Louisiana">Louisiana</option>
                        <option value="Maine">Maine</option>
                        <option value="Maryland">Maryland</option>
                        <option value="Massachusetts">Massachusetts</option>
                        <option value="Michigan">Michigan</option>
                        <option value="Minnesota">Minnesota</option>
                        <option value="Mississippi">Mississippi</option>
                        <option value="Missouri">Missouri</option>
                        <option value="Montana">Montana</option>
                        <option value="Nebraska">Nebraska</option>
                        <option value="Nevada">Nevada</option>
                        <option value="New Hampshire">New Hampshire</option>
                        <option value="New Jersey">New Jersey</option>
                        <option value="New Mexico">New Mexico</option>
                        <option value="New York">New York</option>
                        <option value="North Carolina">North Carolina</option>
                        <option value="North Dakota">North Dakota</option>
                        <option value="Ohio">Ohio</option>
                        <option value="Oklahoma">Oklahoma</option>
                        <option value="Oregon">Oregon</option>
                        <option value="Pennsylvania">Pennsylvania</option>
                        <option value="Rhode Island">Rhode Island</option>
                        <option value="South Carolina">South Carolina</option>
                        <option value="South Dakota">South Dakota</option>
                        <option value="Tennessee">Tennessee</option>
                        <option value="texas">Texas</option>
                        <option value="Utah">Utah</option>
                        <option value="Vermont">Vermont</option>
                        <option value="Virginia">Virginia</option>
                        <option value="Washington">Washington</option>
                        <option value="est Virginia">West Virginia</option>
                        <option value="Wisconsin">Wisconsin</option>
                        <option value="Wyoming">Wyoming</option>
                      </select>
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                      <div class="ErrorMsg state_validation"></div>
                    </label>

                  </div>
                </div>
                <div class="total-pPrice">
                  <div class="col-lg-12 toatal-bill-p tax_prc_for_texas"></div>
                  <div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p total-biled text-left">Total Billed</div>
                  <div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p text-right"><span class="coupon-des-newsignup"></span></div>
                </div>

                <div class="col-md-12 text-left">
                  <div class="sound-signal2">
                    <div class="form-radion2">
                      <p class="terms-txt">By clicking the below button, you agree to our <a href="<?php echo base_url() ?>termsandconditions" target="_blank">Terms</a> and that you have read our <a href="<?php echo base_url() ?>privacypolicy" target="_blank">Data Use Policy</a>, including our Cookie Use. </p>                       </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="loading_account_procs"></div>
              <button class="button upgrd-pln-2" id="payndupgrade" type="submit">Pay and Upgrade Plan</button>
              <div class="card-sec row">
                <div class="col-md-5 col-sm-5 col-xs-6">
                  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/stripe-lock.png" class="img-fluid stripe-lock">
                </div>
                <div class="col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-5 col-xs-offset-1 text-left">
                  <p>Cards accepted:</p>
                  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/payment-systm.png" class="img-fluid">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Model for subscribed user -->
<div class="modal fade slide-3 similar-prop model-close-button in" id="subscribedUSR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">account upgraded</h2>
            <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
         </header>
         <div class="modal-body">
            <div class="congrts">
               <h4><i class="fa fa-check"></i></h4>
               <h3 class="head-c text-center">Congratulations!</h3>
               <p>Your account has been upgraded.</p>
               <a href="<?php echo base_url(); ?>account/dashboard" type="button" class="premium btn btn-red">Go to Premium Account</a>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Model for agency client after successfully upgrade -->
<div class="modal fade slide-3 similar-prop model-close-button in" id="f_clientsubscribed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt">account upgraded</h2>
        <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
      </header>
      <div class="modal-body">
        <div class="congrts">
          <h4><i class="fa fa-check"></i></h4>
          <h3 class="head-c text-center">Congratulations!</h3>
          <p>Your account has been upgraded.</p>
          <a href="javascript:void(0)" onclick="location.reload();" type="button" class="premium btn btn-red">Go to Premium Account</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-------------- requests upgrade popup------------->
<div class="modal fade slide-1 similar-prop in model-close-button" id="upgradefortynineplan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
   <div class="modal-dialog custom-modal" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <p class="iflogo_selected"></p>
            <h2 class="popup_h2 del-txt" id="myModalLabel">Upgrade your Plan </h2>
            <p class="plan">You are out of projects, choose a plan to get more projects designed.</p>
            <div class="purchase_49">
               <a href="javascript:void(0)" data-value="<?php echo FORTYNINE_REQUEST_PLAN; ?>" data-price="<?php echo FORTYNINE_REQUEST_PRICE; ?>" data-inprogress="<?php echo FORTYNINE_REQUEST; ?>" data-display_name = "<?php echo FORTYNINE_REQUEST_PLAN_NAME; ?>" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p upgrd_agency_pln upgrd_fortynine_pln">
               Purchase 1 Request
               </a>
            </div>
            <div class="cross_popup" data-dismiss="modal" aria-label="Close">x</div>
         </header>
         <div class="modal-body model-tour clearfix">
            <div class="row billing_plan two-can fortynine_plans">
               <div class="flex-grid">
                  <div class="price-card">
                     <div class="chooseplan">
                        <h2><?php echo UPGRADE_TO_THREE__PLAN_NAME; ?></h2>
                        <div class="price-amount text-center">
                           <h3><span class="currency-sign">$</span><?php echo UPGRADE_TO_THREE_PRICE; ?></h3>
                        </div>
                        <div class="p-benifite">
                           <ul>
                              <li>
                                 <p><?php echo UPGRADE_TO_THREE_REQUEST; ?> Active Requests</p>
                              </li>
                              <li>
                                 <div>1-Day Turnaround </div>
                              </li>
                              <li>
                                 <p>Share and Collaborate</p>
                              </li>
                              <li>
                                 <p>Unlimited Brands</p>
                              </li>
                              <li>
                                 <p>Unlimited Revisions</p>
                              </li>
                              <li>
                                 <p>Quality Designers</p>
                              </li>
                              <li>
                                 <p>Dedicated Support</p>
                              </li>
                              <li>
                                 <p>Unlimited Free Stock Images</p>
                              </li>
                              <li>
                                 <p>No Contracts</p>
                              </li>
                           </ul>
                        </div>
                        <div class="price-sign-up">
                           <a href="javascript:void(0)" data-value="<?php echo UPGRADE_TO_THREE_PLAN; ?>" data-price="<?php echo UPGRADE_TO_THREE_PRICE; ?>" data-inprogress="<?php echo UPGRADE_TO_THREE_REQUEST; ?>" data-display_name = "<?php echo UPGRADE_TO_THREE__PLAN_NAME; ?>" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p upgrd_agency_pln upgrd_fortynine_pln">
                           Purchase
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="flex-grid">
                  <div class="price-card">
                     <div class="chooseplan">
                        <h2><?php echo UPGRADE_TO_FIVE__PLAN_NAME; ?></h2>
                        <h3><span class="currency-sign">$</span><?php echo UPGRADE_TO_FIVE_PRICE; ?></h3>
                        <div class="p-benifite">
                           <ul>
                              <li>
                                 <p><?php echo UPGRADE_TO_FIVE_REQUEST; ?> Active Requests</p>
                              </li>
                              <li>
                                 <div>1-Day Turnaround </div>
                              </li>
                              <li>
                                 <p>Share and Collaborate</p>
                              </li>
                              <li>
                                 <p>Unlimited Brands</p>
                              </li>
                              <li>
                                 <p>Unlimited Revisions</p>
                              </li>
                              <li>
                                 <p>Quality Designers</p>
                              </li>
                              <li>
                                 <p>Dedicated Support</p>
                              </li>
                              <li>
                                 <p>Unlimited Free Stock Images</p>
                              </li>
                              <li>
                                 <p>No Contracts</p>
                              </li>
                           </ul>
                        </div>
                        <div class="price-sign-up">     
                           <a href="javascript:void(0)" data-value="<?php echo UPGRADE_TO_FIVE_PLAN; ?>" data-price="<?php echo UPGRADE_TO_FIVE_PRICE; ?>" data-inprogress="<?php echo UPGRADE_TO_FIVE_REQUEST; ?>" data-display_name="<?php echo UPGRADE_TO_FIVE__PLAN_NAME; ?>" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p upgrd_agency_pln upgrd_fortynine_pln">
                           Purchase
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="flex-grid">
                  <div class="price-card">
                     <div class="chooseplan">
                        <h2>BUSINESS PLAN</h2>
                        <h3><span class="currency-sign">$</span><?php echo SUBSCRIPTION_MONTHLY_PRICE; ?><span class="time-period">/Month</span></h3>
                        <div class="p-benifite">
                           <ul>
                              <li>
                                 <p>1 Active Request</p>
                              </li>
                              <li>
                                 <p>Unlimited Requests/Month</p>
                              </li>
                              <li>
                                 <div>1-Day Turnaround </div>
                              </li>
                              <li>
                                 <p>Unlimited Brands</p>
                              </li>
                              <li>
                                 <p>Unlimited Revisions</p>
                              </li>
                              <li>
                                 <p>Quality Designers</p>
                              </li>
                              <li>
                                 <p>Dedicated Support</p>
                              </li>
                              <li>
                                 <p>Unlimited Free Stock Images</p>
                              </li>
                              <li>
                                 <p>No Contracts</p>
                              </li>
                           </ul>
                        </div>
                        <div class="price-sign-up">
                           <a href="javascript:void(0)" data-value="<?php echo SUBSCRIPTION_MONTHLY; ?>" data-price="<?php echo SUBSCRIPTION_MONTHLY_PRICE; ?>" data-inprogress="<?php echo TOTAL_INPROGRESS_REQUEST; ?>" data-display_name="BUSINESS PLAN" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p upgrd_agency_pln upgrd_fortynine_pln">
                           Upgrade 
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--------------end requests upgrade popup------------->
<div class="modal fade similar-prop sharing-popup in" id="contact_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
   <div class="modal-dialog" role="document">
      <div class="modal-content full_height_modal">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Contact Us</h2>
            <div class="contact_popup cross_popup" data-dismiss="modal"> x </div>
         </header>
         <div class="modal-body">
            <div class="congrts">
               <h4><i class="fa fa-question-circle"></i></h4>
            </div>
            <div class="cli-ent-model">
               <h3 class="head-c text-center">If you have any query, Contact Us</h3>
               <div class="ms-ceter">
                  <?php if ($physical_address != '' || $physical_address != NULL) { ?>
                  <p><strong>Address : </strong> <?php echo $physical_address; ?></p>
                  <?php }if ($contact_number != '' || $contact_number != NULL) { ?>
                  <p><strong>Contact : </strong> <?php echo $contact_number; ?></p>
                  <?php } if ($email_address != '' || $email_address != NULL) { ?>
                  <p><strong>Email : </strong> <?php echo $email_address; ?></p>
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Confirm popup for change plan -->
<!--<div class="modal fade similar-prop" id="CancelPopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="cli-ent-model-box">
            <header class="fo-rm-header">
               <h2 class="popup_h2 del-txt">Cancel Subscription?</h2>
               <div class="cross_popup" data-dismiss="modal">x </div>
            </header>
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
               <h3 class="head-c text-center">Why you want to cancel your subscription?</h3>
               <form action="<?php echo base_url(); ?>customer/ChangeProfile/cancel_current_plan" method="post" role="form">
                  <div class="sound-signal">
                     <div class="form-radion">
                        <input type="radio" name="reason" id="soundsignal1" value="Designs are great, but I don't need the service anymore" checked>
                        <label for="soundsignal1">Designs are great, but I don't need the service anymore</label>
                     </div>
                     <div class="form-radion">
                        <input type="radio" name="reason" id="soundsignal2" value="The cost does not work with my budget anymore." >
                        <label for="soundsignal2">The cost does not work with my budget anymore.</label>
                     </div>
                     <div class="form-radion">
                        <input type="radio" name="reason" id="soundsignal3" value="The design quality does not meet my expectations." >
                        <label for="soundsignal3">The design quality does not meet my expectations.</label>
                     </div>
                     <div class="form-radion">
                        <input type="radio" name="reason" id="soundsignal4" value="I hired an in-house designer" >
                        <label for="soundsignal4">I hired an in-house designer.</label>
                     </div>
                     <div class="form-radion">
                        <input type="radio" name="reason" id="other_reason" value="others" >
                        <label for="other_reason">other</label>
                     </div>
                  </div>
                  <div class='give_reason' style='display:none'>
                     <label class="form-group">
                        <p class="label-txt">Describe your reason</p>
                        <textarea name='reason_desc' class="input" id="reason_desc"></textarea>
                        <div class="line-box">
                           <div class="line"></div>
                        </div>
                     </label>
                  </div>
                  <button class="btn btn-red" type="submit">Submit</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>-->
<!-- End Modal popup -->
<!-- Access denied popup for change plan -->
<div class="modal fade similar-prop" id="planpermissionspopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Access Denied</h2>
            <div class="cross_popup" data-dismiss="modal"> x</div>
         </header>
         <div class="cli-ent-model-box">
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
               <div class="fo-rm-header">
                  <br>
                  <h3 class="head-c text-center">You can't move  directly '<span class="canntmovepln"><?php echo $login_user_plan[0]["plan_type"]; ?></span> to Monthly Plan'. Please contact us for more info. </h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- End Modal popup -->
<div class="modal fade similar-prop" id="canntdowngrade" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Access Denied</h2>
            <div class="cross_popup" data-dismiss="modal"> x</div>
         </header>
         <div class="cli-ent-model-box">
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
               <div class="fo-rm-header">
                  <br>
                  <h3 class="head-c text-center">You don't have access to downgrade your plan. Please contact us for more info. </h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- End Modal popup -->
<!-- Model for Reactivate subscription -->
<div class="modal fade similar-prop" id="reActivate" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="cli-ent-model-box">
            <header class="fo-rm-header">
               <h2 class="popup_h2 del-txt"> move auto cancelled requests</h2>
               <div class="cross_popup" data-dismiss="modal"> x </div>
            </header>
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/change_plan.svg">
               <h3 class="head-c text-center">Do you want to move auto cancelled requests to its previous state?</h3>
               <form action="<?php echo base_url(); ?>account/request/reactivate_subscription" method="post" >
                  <div class="del-brfrof cancel_subs_pop">
                     <div class="sound-signal">
                        <input type="hidden" name="changeplanaftercancel" id="changeplanaftercancel" class="changeplanaftercancel" value="0">
                        <input type="hidden" name="parent_plan_name" id="prnt_plan_name" class="parent_plan_name" value="<?php echo $parent_user_data[0]['plan_name'];?>">
                        <input type="hidden" name="customer_id" id="prnt_customer_id" class="customer_id" value="<?php echo $parent_user_data[0]['customer_id'];?>"> 
                        <input type="hidden" name="total_inprogress_req" id="prnt_total_inprogress_req" class="total_inprogress_req" value="<?php echo $parent_user_data[0]['total_inprogress_req'];?>"> 
                        <input type="hidden" name="user_state" id="user_state" class="user_state" value="<?php echo $parent_user_data[0]['state'];?>"> 
                        <div class="form-radion">
                           <input type="radio" name="reactivate_pro" id="restore_project" class="restore_project" value="yes" checked="">
                           <label for="restore_project">Yes, Reactivate account and move requests</label>
                        </div>
                        <div class="form-radion">
                           <input type="radio" name="reactivate_pro" id="reactivate_only" class="reactivate_only" value="no">
                           <label for="reactivate_only"> Reactivate account only</label>
                           <div class="assign_brand_to_req"></div>
                        </div>
                     </div>
                     <div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
                        <img class="loaderimg_forreactivate" src="">
                     </div>
                  </div>
                  <button type="submit" name="cancel_subscription_pln" class="btn btn-ydelete">Reactivate</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ######## upload model start fromhere ###### -->
<div class="modal upload-gallery fade" id="uploadModel" role="dialog">

  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Images </h4>
      </div>
      <div class="modal-body clearfix">
        <div class="col-lg-12 col-sm-12">
          <?php if($get_method != "add_new_request") { ?>
            <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">

              <div class="btn-group" role="group">
                <button type="button" id="stars" class="btn btn-primary custom_button" href="#tab1" data-toggle="tab"> <i class="fa fa-upload" aria-hidden="true">
                </i>
                <div class="hidden-xs">Upload</div>
              </button>
            </div>

            <div class="btn-group" role="group">
              <button type="button" id="favorites" class="btn btn-default custom_button <?php  if($get_method == "add_new_request") { echo "btn-primary"; }else{ echo "" ;}  ?>" href="#tab2" data-toggle="tab"> <i class="fa fa-search" aria-hidden="true"></i>
                <div class="hidden-xs">Search online</div>
              </button>
            </div>

          </div>
        <?php } ?>
        <?php  if($get_method == "add_new_request") { ?>
          <div class="well-1">
          <?php }else{ ?>
            <div class="well">
            <?php } ?>
            <div class="tab-content">
              <div class="tab-pane fade in active" id="tab1">
                <?php  if($get_method == "add_new_request") { ?>

                  <?php if (isset($editvar['customer_attachment'])) { ?>
                    <hr/>
                    <div class="uploadFileListContainer row">   
                      <?php
                      for ($i = 0; $i < count($editvar['customer_attachment']); $i++) {
                        $type = substr($editvar['customer_attachment'][$i]['file_name'], strrpos($editvar['customer_attachment'][$i]['file_name'], '.') + 1);
                        ?>
                        <div  class="uploadFileRow" id="file<?php echo $editvar['customer_attachment'][$i]['id']; ?>">
                          <div class="col-md-6">
                            <div class="extnsn-lst">
                              <p class="text-mb">
                                <?php echo $editvar['customer_attachment'][$i]['file_name']; ?>
                              </p>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  <?php } ?>
                </div>
<!--                         <div class="col-md-12">
                          <div class="uploadFileListContainer  uploadFileListContain">   
                          </div>
                        </div> -->
<!--                        <div class="col-lg-12 ad_new_reqz text-center">
                            <input type="submit" class=" theme-save-btn" id="request_submit_op" data-count="1" name="request_submit">
                            <input type="submit" class="theme-cancel-btn" id="request_exit_op" value="Save in Draft" name="request_exit">
                          </div>-->
                        </div>

                      <?php } else if($get_method == "project_info"){?>
                        <div class="cli-ent-model-box">
                          <div class="cli-ent-model">
                            <div class="selectimagespopup">
                             <form method="post" action="" enctype="multipart/form-data">
                              <div class="form-group goup-x1 goup_1">
                               <p class="space-a"></p>
                               <div class="file-drop-area">
                                <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                <input type="hidden" name="from_file" value="project-info">
                                <input type="file" class="file-input project-file-input" multiple="" name="file-upload[]" id="file_input" />
                              </div>
                              <p class="text-g">Example: Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov, tiff, ttf, eps </p>
                              <p class="space-e"></p>
                              <div class="col-md-12">
                                <div class="uploadFileListContain">
                                </div>
                              </div>
                              <p class="btn-x"><input type="submit" value="Submit" name="attach_file" class="btn-g" id="<?php echo $get_method;?>_btnid"></p>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php } ?> 
                </div>
                <?php  if($get_method != "add_new_request") { ?>
                  <div class="tab-pane fade in" id="tab2">
                  <?php } ?>
                  <div class="search-container col-sm-12">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="select-search">
                          <div class="img-search">
                            <input type="text" placeholder="Enter your keyword eg:apple,books etc" name="search" class="form-control" id="search">
                          </div>
                          <select class="form-control" id="image_filter">
                            <option value="all">All</option>
                            <option value="horizontal" value="horizontal" data-filter="landscape">Landscape</option>
                            <option value="vertical" data-filter ="portrait">Portrait</option>
                          </select>
                          <button type="button" class="serach_btn">Search</i>
                          </button>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="uploadAnDsave">
                        <div class="left-up-btn">
                          <form id="uploadForm" action="<?php echo base_url();?>account/request/download_images" method="post">
                            <input type="hidden" name="req_fromPage" value="<?php echo $get_method;?>">

                            <div class="selected_images">

                            </div>
                            
                            <button type="submit" data-loading-text="<i class='fas fa-circle-notch fa-spin'></i></i> Downloading..." class="DownloadBtn" style="display:none;"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                          </form>
                          <input type="hidden" name="req_fromPage" value="<?php echo $get_method;?>">
                          <input type="hidden" name="search_keyword" value="" id="search_keyword_hid">

                          <button type="button" data-loading-text="<i class='fas fa-circle-notch fa-spin'></i></i> Uploading..." class="upload_btn" style="display: none;"><i class="fa fa-paperclip" aria-hidden="true"></i> Attach</button> 
                        </div>



                        <div class="uploaded-items">
                          <div class="content_Container">
                          </div>
                 <!--    <div class="col-lg-12 ad_new_reqz text-center" style="display: none; ">
                                <input type="submit" value ="save" class=" theme-save-btn" id="<?php echo $get_method;?>_btnClick" data-count="1" name="request_submit">
                              </div>-->
                            </div>
                          </div>
                          
                          <div class="search_head-commeneted"></div>

                          <div class="col-sm-12">
                            <div class="ajax_searchload" style="display:none;">
                              <img src="<?php echo base_url();  ?>public/assets/img/ajax-loader.gif">
                            </div>
                              <div class="img-gallery-grid">
                                <ul class="thumbnil-img grid" id="image_gallery">

                                </ul>
                                <div class="loader_button">
                                  <div class="ajax_se_loaderbtnn" style="display:none;">
                                   <img src="<?php echo base_url();  ?>public/assets/img/ajax-loader.gif">
                                 </div>
                                 <a href="#" id="loadMore" style="display:none;" onclick="load_more_images($(this))">Load More
                                 </a>
                              </div>
                             </div>
                          </div>
                        </div>

                        

                    </div>
                    <?php  if($get_method != "add_new_request") { ?>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
</div>
<!-- #######  ######## upload model ends  here  ########-->
<div class="modal fade" id="openfileType" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <div class="myiconCstm">
               <a class="action_mAf" id="myiconCstm_mAf" data-id="" data-requested-id="" data-folder_id="" data-file="" data-role="" data-isfav="" title="Add to favorite"><i class="fas fa-heart active"></i></a>
               <a id="myiconCstm_dwnld" download="" class="hover_icon" title="Download">
                  <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                x="0px" y="0px" viewBox="0 0 21 20.5" xml:space="preserve">
                <g display="none">
                  <path display="inline" fill-rule="evenodd" fill="#DB213C" d="M0,0.5v2h20v-2H0z M5,7.5h15v-2H5V7.5z M9,12.5h11v-2H9V12.5z
                  M6,17.5h14v-2H6V17.5z"/>
                </g>
                <g display="none">
                  <path display="inline" fill="#A0A0A0" d="M20,16.8c-1.2-1.2-2.3-2.3-3.5-3.5c-0.3-0.3-0.6-0.5-1.1-0.5c-0.2,0-0.5,0.1-0.7,0.1
                  c-0.2-0.2-0.4-0.4-0.7-0.6c2.7-3.5,1.9-8.2-1.1-10.6c-3-2.5-7.4-2.3-10.3,0.5c-2.7,2.7-3.1,7.1-0.6,10.2c1.2,1.4,2.7,2.3,4.5,2.7
                  c2.3,0.4,4.4-0.1,6.2-1.5c0.2,0.2,0.5,0.4,0.7,0.7c-0.3,0.6-0.2,1.2,0.3,1.7c1.2,1.2,2.4,2.4,3.5,3.5c0.6,0.6,1.5,0.6,2.1,0
                  c0.2-0.2,0.4-0.4,0.6-0.6C20.6,18.3,20.6,17.5,20,16.8z M8.1,13.9c-3.4,0-6.3-2.8-6.3-6.2c0-3.4,2.8-6.2,6.3-6.2
                  c3.4,0,6.2,2.8,6.3,6.2C14.3,11.1,11.5,13.9,8.1,13.9z M8.8,3.8H7.3v3.1H4.2v1.6h3.1v3.1h1.6V8.4H12V6.9H8.8V3.8z"/>
                </g>
                <g display="none">
                  <path display="inline" fill="#A0A0A0" d="M20.5,17.3c-1.2-1.2-2.3-2.3-3.5-3.5c-0.3-0.3-0.6-0.5-1.1-0.5c-0.2,0-0.5,0.1-0.7,0.1
                  c-0.2-0.2-0.4-0.4-0.7-0.6c2.7-3.5,1.9-8.2-1.1-10.6C10.4-0.2,6-0.1,3.2,2.7C0.4,5.4,0,9.8,2.6,12.9c1.2,1.4,2.7,2.3,4.5,2.7
                  c2.3,0.4,4.4-0.1,6.2-1.5c0.2,0.2,0.5,0.4,0.7,0.7c-0.3,0.6-0.2,1.2,0.3,1.7c1.2,1.2,2.4,2.4,3.5,3.5c0.6,0.6,1.5,0.6,2.1,0
                  c0.2-0.2,0.4-0.4,0.6-0.6C21.1,18.8,21.1,18,20.5,17.3z M8.6,14.4c-3.4,0-6.3-2.8-6.3-6.2c0-3.4,2.8-6.2,6.3-6.2
                  c3.4,0,6.2,2.8,6.3,6.2C14.8,11.6,12,14.3,8.6,14.4z M4.7,8.9l7.8,0V7.4l-7.8,0V8.9z"/>
                </g>
                <path display="none" fill="#FFFFFF" d="M17.1,2.5c-1.8,1-3.6,2.5-5.4,4.4c-1.8,1.8-3.5,3.9-5.1,6.2l-3.7-2.2l-0.4,0.5l5,6L7.7,17
                c2.7-5.8,5.7-10.1,9.8-13.9L17.1,2.5z"/>
                <g>
                  <path fill="#4A4A4A" d="M19.5,10.6c0-0.4-0.2-0.6-0.5-0.6c-0.3,0-0.5,0.2-0.5,0.6c0,0.1,0,0.2,0,0.2c0,1.6,0,3.2,0,4.8
                  c0,1-0.7,1.7-1.6,1.7c-2.6,0-5.1,0-7.7,0c-2,0-4.1,0-6.1,0c-0.7,0-1.3-0.5-1.4-1.2c0-0.2,0-0.3,0-0.5c0-1.6,0-3.3,0-4.9
                  c0-0.1,0-0.3,0-0.4C1.4,10.1,1.2,10,1,10c-0.3,0.1-0.5,0.3-0.5,0.6c0,1.7,0,3.4,0,5.1c0,1.6,1.1,2.8,2.6,2.8c2.3,0,4.6,0,6.8,0
                  c2.3,0,4.6,0,6.9,0c1.5,0,2.6-1.2,2.6-2.7c0-0.8,0-1.7,0-2.5C19.5,12.4,19.5,11.5,19.5,10.6z M5.9,10c1.2,1.3,2.4,2.5,3.6,3.8
                  c0.3,0.3,0.6,0.3,0.9,0c1.2-1.3,2.4-2.5,3.6-3.8c0.1-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.1-0.5-0.1-0.7c-0.2-0.2-0.5-0.2-0.7,0
                  c-0.1,0-0.1,0.1-0.2,0.2c-0.8,0.9-1.7,1.8-2.5,2.7c-0.1,0.1-0.1,0.1-0.2,0.2c0-0.1,0-0.2,0-0.2c0-2.3,0-4.5,0-6.8
                  c0-0.6,0-1.3,0-1.9c0-0.4-0.2-0.6-0.5-0.6c-0.3,0-0.5,0.3-0.5,0.6c0,0,0,0.1,0,0.1c0,2.9,0,5.7,0,8.6v0.3c-0.5-0.6-1-1.1-1.5-1.6
                  C7.5,10,7.1,9.6,6.6,9.1C6.4,8.9,6.2,8.9,6,9C5.8,9.1,5.7,9.4,5.7,9.6C5.8,9.7,5.8,9.9,5.9,10z"/>
                </g>
              </svg>
               </a>
               <a class="CopYfileToFolder myiconCstm_copy hover_icon" title="Copy">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 170.8 163.6" enable-background="new 0 0 170.8 163.6" xml:space="preserve">
                     <g>
                        <path d="M170.8,16.9c0-9.8-7.1-16.9-16.8-16.9C135.2,0,116.8,0,99.2,0C83.9,0,68.6,0,53.2,0.1c-9.9,0-16.9,7-17.1,17.1
                           c-0.1,3.1,0,6.2,0,9.4c0,0.8,0,1.6,0,2.4c-1.5,0-3,0-4.5,0c-4.7,0-9.3,0-13.8,0C6.9,29,0.1,35.8,0,46.6C0,79,0,112.4,0,146
                           c0,10.7,6.9,17.6,17.5,17.6c16.6,0,33.3,0,49.9,0s33.3,0,49.9,0c10.2,0,17.2-7,17.3-17.3c0-3.1,0-6.1,0-9.3c0-0.8,0-1.5,0-2.3
                           c1.5,0,3.1,0,4.6,0c4.9,0,9.7,0,14.4,0c10.1-0.1,17.1-7.1,17.1-17C170.9,86,170.9,53,170.8,16.9z M115.3,134.7v9.7H19.2V48.2h16.9
                           c0,0.5,0.1,1,0.1,1.4c0,9.3,0,18.5,0,27.8c0,12.9,0,25.8,0,38.6c0,12,6.7,18.6,18.8,18.6c8.6,0,17.1,0,25.7,0L115.3,134.7z
                           M151.5,19.4v96.1H55.4V19.4H151.5z"/>
                     </g>
                  </svg>
               </a>
               <a target="_blank" class="myiconCstm_gtD hover_icon" title="Go to draft" href="http://localhost/graphicszoolive/customer/request/project-info/7898">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  viewBox="0 0 154.4 163.4" enable-background="new 0 0 154.4 163.4" xml:space="preserve">
                     <g>
                        <path d="M154,87.9c-3.8-12-7.6-24.1-11.4-36.1l-6.1-19.2c-2.8-8.8-5.5-17.6-8.3-26.3C127,2.2,125.4,0,120.8,0c-29,0-58.1,0-87.1,0
                           c-3.9,0-6,1.6-7.2,5.2c-8.7,27.5-17.4,55.1-26,82.6c-0.4,1.3-0.6,2.7-0.4,4c2.4,18.2,4.8,36.3,7.2,54.4l1.4,10.5
                           c0.6,4.6,2.9,6.6,7.5,6.6l60.9,0l60.9,0c4.8,0,7.1-2,7.7-6.6c0.6-4.2,1.1-8.4,1.7-12.5l2-14.9c1.7-12.4,3.3-24.9,4.9-37.3
                           C154.5,90.7,154.4,89.2,154,87.9z M37.8,13.4c0-0.1,0.1-0.2,0.1-0.2c0,0,0.1,0,0.2,0V11l0,0l0,2.2c26.2,0.1,52.3,0,78.5,0
                           c6.1,19.3,12.2,38.7,18.3,58l4,12.6h-12.3c0.1-0.9,0.1-1.7,0.2-2.6c0.4-3.8,0.9-7.6,1.3-11.5c0.2-2.1-0.3-4.1-1.6-5.6
                           c-1.3-1.5-3.2-2.3-5.3-2.3c-0.8,0-1.5,0-2.3,0l-1.5,0l0.2-1.8c0.5-3.8,1-7.6,1.4-11.4c0.1-1.1,0.3-2.3,0.1-3.4
                           c-0.4-3.1-3.1-5.4-6.4-5.5c-1.4,0-2.7,0-4.2,0l0-0.2c0.2-1.9,0.4-3.8,0.7-5.7c0.2-1.5,0.4-2.9,0.6-4.4c0.2-1.3,0.4-2.7,0.6-4
                           c0.3-2-0.3-4-1.6-5.5c-1.3-1.5-3.1-2.3-5.2-2.3c-17.6,0-35.2,0-52.8,0c-2,0-3.8,0.8-5.1,2.2c-1.3,1.4-1.8,3.3-1.6,5.4
                           c0.4,3.4,0.8,6.7,1.2,10.1c0.2,1.5,0.4,2.9,0.5,4.4l-1.1,0c-0.7,0-1.4,0-2.1,0c-2.7,0-4.7,0.8-6,2.2c-1.3,1.4-1.8,3.5-1.5,6.1
                           c0.4,3.2,0.8,6.4,1.2,9.6c0.2,1.4,0.4,2.8,0.5,4.2l-1.4,0c-0.8,0-1.5,0-2.3,0c-2.3,0-4.1,0.8-5.4,2.2c-1.3,1.4-1.8,3.4-1.6,5.7
                           c0.5,4.3,0.9,8.6,1.3,12.8l0.1,1.3H15.6c0.1-0.2,0.1-0.4,0.2-0.7l4.3-13.7C26,50.8,31.9,32.1,37.8,13.4z M105,53.1l-1.1,8.7H50.5
                           l-1.1-8.7H105z M58.2,31h38c0,0.2-0.1,0.5-0.1,0.7c-0.3,2.3-0.5,4.7-0.9,7c0,0.4-0.1,0.8-0.2,1.1c-0.3,0-0.7,0-1.1,0l-16.4,0
                           c-6.1,0-12.2,0-18.3,0c-0.2-2.2-0.5-4.4-0.8-6.6C58.4,32.5,58.3,31.7,58.2,31z M114.1,75.2l-0.9,8.7l-2.1,0c-0.8,0-1.7,0-2.5,0
                           c-3.8,0-6,1.8-7,5.5c-0.8,3.3-1.7,6.5-2.5,9.8l-0.6,2.3H55.9c-0.2-0.7-0.4-1.4-0.6-2.2c-0.7-2.9-1.5-5.8-2.1-8.7
                           C52,85.7,49.9,84,44.9,84l-3.6,0l-0.9-8.7H114.1z M41.1,97.1C41.1,97.1,41.1,97.1,41.1,97.1c0.1,0,0.1,0.1,0.1,0.1
                           c0.7,2.9,1.4,5.7,2.1,8.5c0.3,1.1,0.5,2.1,0.8,3.2c1,4.1,3.3,5.9,7.5,5.9c9.6,0,19.2,0,28.8,0l22.6,0c4,0,6.4-1.9,7.4-5.8l0.7-2.8
                           c0.7-3,1.5-6.1,2.3-9.1c6.6,0,13.2,0,19.7,0l7.3,0l-7.1,52.9H21.1l-7.1-53l13.2,0C32.8,97.2,37.1,97.1,41.1,97.1z"/>
                        <path d="M47,141.3c10.9,0,21.8,0,32.7,0l16.1,0c4,0,8,0,12,0c2.3,0,4.3-0.9,5.6-2.4c1.2-1.5,1.7-3.4,1.3-5.5
                           c-0.6-3.2-3.3-5.3-7-5.3c-4.6,0-9.2,0-13.7,0l-18.5,0l-14.9,0c-4.6,0-9.2,0-13.9,0c-2.1,0-3.9,0.8-5.2,2.1
                           c-1.2,1.3-1.8,3.1-1.6,5.1C40,139,42.8,141.3,47,141.3z"/>
                     </g>
                  </svg>
               </a>
            </div>
            <button type="button" class="close hover_icon" data-dismiss="modal">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  viewBox="0 0 163.9 164.2" enable-background="new 0 0 163.9 164.2" xml:space="preserve">
                  <path d="M61.7,82.7C40.3,61.3,20,41.1,0,21.2c6.9-6.9,13.6-13.5,20.1-20c19.7,19.7,40,40.1,60.5,60.6c21.2-21.3,41.5-41.7,61.5-61.7
                     c7.3,7.4,14,14.1,20.5,20.6c-19.7,19.7-40.1,40-60.6,60.5c21.3,21.3,41.7,41.5,61.8,61.7c-7.4,7.2-14.2,13.8-20.8,20.3
                     c-19.6-19.7-39.9-40-60.4-60.5c-21.2,21.3-41.5,41.7-61.5,61.7c-7.3-7.3-14-14-20.5-20.5C20.4,123.9,40.8,103.6,61.7,82.7z"/>
               </svg>
            </button>
         </div>
         <div class="modal-body" id="openfileType_content">
         </div>
      </div>
   </div>
</div>
<!--<div class="modal fade slide-3 similar-prop model-close-button in" id="CopYfileToFolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Copy File to Custom folder</h2>
            <div class="cross_popup" data-dismiss="modal"> x</div>
         </header>
         <div class="testemail_section">
            <div class="email_header_temp_cont"><?php //echo $email_header_footer_sec['header_part'];   ?></div>
            <form method="POST" action="#" >
               <div class="fo-rm-body">
                  <div class="row">
                     <input type="hidden" class="form-control input-d from" id="from" name="from" value="">
                     <input type="hidden" class="form-control input-d folder_id" name="folder_id" value="0">
                     <input type="hidden"  id="original_file" >
                     <input type="hidden"  id="copy_folder_path" >
                     <input type="hidden"  id="req_id">
                     <input type="hidden"  id="file_id">
                     <div class="col-sm-12">
                        <div class="text-right">
                           <a class="create_folder btn-g" ><span aria-hidden="true">Create folder</span></a>
                        </div>
                        <label class="form-group">
                           <p class="label-txt label-active">Copy your file Under</p>
                                               <input type="text" class="input folder_nest" name="folder_nest" value="Custom Folder/" readonly data-id="copy_file">
                              <span id="errorSpan"></span>
                              <div class="line-box">
                                <div class="line"></div>
                              </div>
                        </label>
                        <div class="filetree" id="copy_file_filetree">
                           <div class="breadCrumCopYFolder">
                              <a class="folder_nest">My folders </a> 
                           </div>
                           <ul class="main-tree" id="copy_file_mainTree">
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group goup-x1">
                  <div class="cell-col">
                     <p class="btn-x copy-create">
                        <button type="button"  id="copy_file" class="btn btn-g text-uppercase">Copy</button>
                     </p>
                  </div>
               </div>
         </div>
         </form>
      </div>
   </div>
</div>-->
<div class="modal fade slide-3 similar-prop model-close-button in" id="CopYfileToFolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <header class="fo-rm-header">
                                <h2 class="popup_h2 del-txt">Copy File to Custom folder</h2>
                                <div class="cross_popup" data-dismiss="modal"> x</div>
                              </header>
                              <div class="testemail_section">
                                <div class="email_header_temp_cont"><?php //echo $email_header_footer_sec['header_part'];   ?></div>
                                <form method="POST" action="#" >
                                  <div class="fo-rm-body">
                                    <div class="row">
                                      <input type="hidden" class="form-control input-d from" id="from" name="from" value="">
                                      <input type="hidden" class="form-control input-d folder_id" name="folder_id" value="0">
                                      <input type="hidden"  id="original_file" >
                                      <input type="hidden"  id="copy_folder_path" >
                                      <input type="hidden"  id="req_id">
                                      <input type="hidden"  id="file_id">
                                      <div class="col-sm-12"> 
                                        <div class="text-left head-copy">
                                              <label class="form-group">
                                        <p class="label-txt label-active">Copy your file Under</p>
  
                        </label>
                                         <a class="create_folder btn-g" ><span aria-hidden="true">Create folder</span></a>
                                       </div>
                                     
                  <div class="filetree" id="copy_file_filetree"> 
                    <div class="breadCrumCopYFolder">
                     <a class="folder_nest">/My folder</a> 
                   </div>
                   <span id="eRroSpan"></span>
                   <ul class="main-tree" id="copy_file_mainTree">

                   </ul>
                 </div>
               </div>
             </div>
           </div>
           <div class="form-group goup-x1">
            <div class="cell-col">
              <p class="btn-x copy-create">
                <button type="button" disabled id="copy_file" class="btn btn-g disable text-uppercase">Copy</button>

              </p>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- create folder popup  -->
<div class="modal fade slide-3 similar-prop model-close-button in" id="DirectorySave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Create a folder</h2>
            <div class="cross_popup" data-dismiss="modal"> x</div>
         </header>
         <div class="testemail_section">
            <div class="email_header_temp_cont">
               <?php //echo $email_header_footer_sec['header_part'];   ?>
            </div>
            <form method="POST" action="<?php echo base_url(); ?>account/Filemanagement/createFolder">
               <div class="fo-rm-body">
                  <div class="row">
                     <input type="hidden" class="form-control input-d from" name="from" value="">
                     <input type="hidden" class="form-control input-d selected_folder" name="selected" id="selected_folder" value="">
                     <input type="hidden" class="form-control input-d folder_id" name="folder_id" value="0">
                     <div class="col-sm-12">
                        <label class="form-group">
                           <p class="label-txt">Folder Name </p>
                           <input type="text" class="input folder_name" id="folder_name" name="folder_name" value="" required="required" maxlength="20">
                           <span id="errorSpan"></span>
                           <div class="line-box">
                              <div class="line"></div>
                           </div>
                        </label>
                     </div>
                     <div class="col-sm-12">
                        <div class="form-group goup-x1">
                           <div class="cell-col">
                              <p class="btn-x bl-ogbtn text-right">
                                 <button type="submit" name="submit" class="btn-g text-uppercase create_folderst">Save</button>
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
            </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- create folder popup ends  -->
<!-- Access denied popup for change client plan -->
<div class="modal fade similar-prop" id="sbusrplnprmsnspopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt">Access Denied</h2>
        <div class="cross_popup" data-dismiss="modal"> x</div>
      </header>
      <div class="cli-ent-model-box">
        <div class="cli-ent-model">
          <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
          <div class="fo-rm-header">
            <br>
            <h3 class="head-c text-center">You can't move  directly 'Subscription Based to One Time Plan'. Please contact us for more info. </h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal popup -->
<!-- Access denied popup for change client plan -->
<div class="modal fade similar-prop" id="cantaddsubs" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt">Access Denied</h2>
        <div class="cross_popup" data-dismiss="modal"> x</div>
      </header>
      <div class="cli-ent-model-box">
        <div class="cli-ent-model">
          <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
          <div class="fo-rm-header">
            <br>
            <h3 class="head-c text-center f_reserveallslot">As per you plan, <span class="clntassignreq"></span> dedicated designers assigned to your account. Out of which you already assigned <span class="clntalrdyasigndreq"></span> designers to your existing clients. So you can assign <span class="clntpendingreq"></span> more designers to news. </h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal popup -->
<!--Popup for change subuser plan -->
<div class="modal fade similar-prop" id="chnagesubuserpln" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt">Change Plan</h2>
        <div class="cross_popup" data-dismiss="modal"> x</div>
      </header>
      <div class="cli-ent-model-box">
        <div class="cli-ent-model">
          <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/change_plan.svg">

          <form action="<?php echo base_url(); ?>account/AgencyClients/change_client_plan" method="post" role="form" class="ChangeCurrent_Plan" id="Changeclnt_Plan">
            <div class="fo-rm-header">
              <br>
              <?php if ($parent_user_data[0]['is_cancel_subscription'] == 1) { ?>
                <div class="del-brfrof cancel_subs_pop">
                  <h3 class="head-c text-center">Do you want to move auto cancelled requests to its previous state? </h3><br/>
                  <label> <input type="radio" name="reactivate_pro" id="restore_project_lis" class="restore_project" value="yes" checked=""> Yes, Reactivate account and move requests</label>
                  <label><input type="radio" name="reactivate_pro" id="reactivate_only_lis" class="reactivate_only" value="no"> Reactivate account only</label>
                </div>
              <?php } else { ?>
                <h3 class="head-c text-center">Are you sure you want to Change Current Plan? </h3>
              <?php } ?>
            </div>
            <input type="hidden" class="form-control plan_name_to_upgrade plan_name" name="plan_name" value="">
            <input type="hidden" class="form-control plan_price" name="plan_price" value="">
            <input type="hidden" class="form-control display_namepln" name="display_name" value="" >
            <input type="hidden" class="form-control in_progress_request" name="in_progress_request" value="1">
            <input type="hidden" class="form-control totl_active_request" name="totl_active_request" value="2">
            <input type="hidden" class="form-control plnclient_id" name="client_id" value="">
            <input type="hidden" class="form-control client_url" name="back_url" value="<?php echo current_url(); ?>">
            <div class="confirmation_btn text-center">
              <div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
               <img class="loaderimg_forreactivate" src="">
             </div>
             <button class="btn btn-CnfrmChange btn-ndelete" id="chng_clnt_pln" type="submit">Yes</button>
             <button class="btn btn-ydelete" data-dismiss="modal" aria-label="Close">No</button>
           </div>
         </form>
       </div>
     </div>
   </div>
 </div>
</div>
<!-- End Popup for change subuser plan -->
<!-- Model for upgrade subuser plan -->
<div class="modal fade slide-2 similar-prop model-close-button in" id="Upgrdsubuseracount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <h2 class="popup_h2 del-txt" id="myModalLabelcl"> Personal Billing Information</h2>
        <div class="cross_popup" data-dismiss="modal" aria-label="Close">x</div>
      </header>
      <div class="modal-body">
        <div class="bill-infoo">
          <div class="error-msg"></div>
          <div class="pb-info">
            <form action="" method="post" role="form" class="contactForm" id="f_clntupgrade_account">
              <div class="row">
                <input type="hidden" class="form-control subuser_id" name="customer_id" id="subuser_id" value="" required="">
                <input type="hidden" class="form-control subsplan_name" name="plan_name" id="client_plan" value="">
                <input type="hidden" class="form-control plan_price" name="plan_price" id="clientplanprice" value="">
                <input type="hidden" id="clientfinalplan_price" class="final_plan_price" name="final_plan_price" value="" />
                <input type="hidden" id="subsplanprice" class="subs_plan_price" name="subs_plan_price" value="" />
                <input type="hidden" id="clientstatetax" class="state_tax" name="state_tax" value="0"/>
                <input type="hidden" id="clienttaxamount" class="tax_amount" name="tax_amount" value="0"/>
                <input type="hidden" class="form-control in_progress_request" name="in_progress_request" id="clientinprogress_request" value="1">
                <input type="hidden" class="form-control" name="email" id="clntemail" value="<?php echo $login_user_data[0]['email']; ?>">
                <input type="hidden" id="clientcouponinserted_ornot" class="couponinserted_ornot" name="couponinserted_ornot" value=""/>
                <label class="form-group">
                  <p class="label-txt">Card Number <span>*</span></p>
                  <input type="text" class="input" name="card_number" id="clintcard_number" required="" maxlength="16">
                  <div class="line-box">
                    <div class="line"></div>
                  </div>
                </label>
                <div class="row">
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt label-active">Expiry Date <span>*</span></p>
                      <input type="text" name="expir_date" id="clntexpir_date" class="expir_date input" placeholder="MM  /  YY" onkeyup="dateFormat(this.value,this);" required="">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                    </label>
                  </div>
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt">CVV <span>*</span></p>
                      <input type="text" name="cvc" id="clntcvc" class="input" maxlength="4" required="">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                    </label>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <label class="form-group">
                      <p class="label-txt">Discount</p>
                      <input type="text" class="input disc-code" id="clntdiscCode" name="Discount">
                      <div class="line-box">
                        <div class="line"></div>
                      </div>
                      <div class="ErrorMsg epassError discount-code">
                      </div> 
                      <div class="ErrorMsg epassErrorSuccess discount-code"><span class="CouponErr_code"></span></div>
                    </label>
                  </div>
                </div>
                <div class="total-pPrice">
                  <div class="col-lg-12 toatal-bill-p tax_prc_for_texas"></div>
                  <div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p total-biled text-left">Total Billed</div>
                  <div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p text-right"><span class="coupon-des-newsignup"></span></div>
                </div>
              </div>
              <div class="loading_account_procs"></div>
              <button class="button upgrd-pln-2" id="clntpayndupgrade" type="submit">Pay and Upgrade Plan</button>
              <div class="card-sec row">
                <div class="col-md-5 col-sm-5 col-xs-6">
                  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/stripe-lock.png" class="img-fluid stripe-lock">
                </div>
                <div class="col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-5 col-xs-offset-1 text-left">
                  <p>Cards accepted:</p>
                  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/payment-systm.png" class="img-fluid">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Model for upgrade subuser plan -->
<?php if ($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client' && ($physical_address != '' || $contact_number != '' || $email_address != '')) { ?>
<a href='javascript:void(0)' class='help_user'>?</a>
<?php
   }
   $url = current_url();
   $messageurl = substr($url, strrpos($url, '/') + 1);
   $emailurl = explode('/', $url);
   if($messageurl != 'setting-view' && (!in_array("user_setting", $emailurl))){ 
     ?>
<!--            <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/jquery.min.js"></script>-->
<?php if (!in_array("email_content", $emailurl)) { ?>
<!--              <script src="<?php echo FS_PATH_PUBLIC_ASSETS;?>js/customer/bootstrap.min.js"></script>-->
<?php } ?>
<?php } ?>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/account/s_all.js');?>"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/owl/owl.carousel.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/lightgallery-all.min.js"></script>
<script type="text/javascript">
   var total_inprogress_req = '<?php echo $login_user_data[0]['total_inprogress_req'];?>';
   var subusers_reqdata = '<?php echo json_encode($checksubuser_requests) ?>';
    var get_method =  "<?php echo $get_method; ?>";
   
   <?php if (!empty($client_script)) {
      ?>
     $("a.help_user").addClass("space_align");
   <?php } else { ?>
     $("a.help_user").removeClass("space_align");
   <?php } ?>     
   
</script>
<!--***************Live Chat Script*************************-->
<?php
   if ($login_user_data[0]['user_flag'] != 'client') { ?>
<script>
   "use strict";
   !function() {
     var t = window.driftt = window.drift = window.driftt || [];
     if (!t.init) {
       if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
       t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
       t.factory = function(e) {
         return function() {
           var n = Array.prototype.slice.call(arguments);
           return n.unshift(e), t.push(n), t;
         };
       }, t.methods.forEach(function(e) {
         t[e] = t.factory(e);
       }), t.load = function(t) {
         var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
         o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
         var i = document.getElementsByTagName("script")[0];
         i.parentNode.insertBefore(o, i);
       };
     }
   }();
   drift.SNIPPET_VERSION = '0.3.1';
   drift.load('d6yi38hkhwsd');
</script>
<?php } else { 
    if (!empty($client_script)) {
        /***Start client script before close head tag***/
        foreach ($client_script as $k => $s_vl) {
            if (($s_vl['show_only_specific_page'] == "cus_portl" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_body") {
                if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                    echo base64_decode($s_vl["tracking_script"]);
                } else {
                    ?>
                    <script>
                    <?php echo base64_decode($s_vl["tracking_script"]); ?>
                    </script>
                    <?php
                }
            }
        }
        /***End client script before close body tag***/
    }
} ?> 
<!--***************End Live Chat Script*************************-->
<script>
   //   Hotjar Tracking Code for www.graphicszoo.com 
   (function(h,o,t,j,a,r){
       h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
       h._hjSettings={hjid:676752,hjsv:6};
       a=o.getElementsByTagName('head')[0];
       r=o.createElement('script');r.async=1;
       r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
       a.appendChild(r);
   })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
   
    jQuery(".site_tour_popup .modal-body").owlCarousel({
    loop: true,
    mouseDrag: true,
    nav: true,
    margin: 10,
    dots: true,
    autoplay: true,
    autoplayTimeout:10000,
    items: 1,
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<!--<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/customer_portal.js"></script>-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>

<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/account/s_portal.js"></script>
<?php  $login_user_data = $this->load->get_var('login_user_data'); 
//    if($login_user_data[0]['is_saas'] == 1){
?>
<!--<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/filemanagement.js"></script>-->
<?php //}else { ?> 
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/account/s_filemanagement.js"></script>
<?php //} ?>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/jquery.fancybox.min.js"></script>
</body>
</html>
