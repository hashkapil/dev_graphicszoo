<style type="text/css">
 button.go_back_file_cst {
    border: 1px solid #e5e5e5;
    height: 40px;
    width: 40px;
    border-radius: 100%;
    line-height: 40px;
    transition: 0.76s all;
    cursor: pointer;
}
li.get_sub_folder.ulactive {
    color: green;
}
li.tree-title.get_sub_folder.mlactive.active {
    color: green;
}
li.tree-title.get_sub_folder.mlactive {
    color: green;
}
li.tree-title.get_sub_folder_main.mlactive.ulactive {
    color: green;
}
.fancybox-content{
    /*width: 600px;
    height: auto;
    background: transparent;*/
}
.fancybox-slide--html .fancybox-close-small{
    /*color: #fff; */
}
/*li.copyfolder_or_files:hover */

</style>
<?php //echo "<pre>"; print_r($_SESSION); ?> 
<section class="con-b">
    <div class="custom_container">
        <div class="" id="file_manage_list">
         <div class="row">
            <div class="col-md-12">
                <?php if($isshow_file_mngmnt == 0){
                    echo "<h3 class='head-c text-center denied_fl_mngmnt'>You don't have access to file management. Please contact us for more info.</h3>";
                }else{ ?>
                   <div class="show_all_folders">
                      <div class="main_folders">
                        <div class="heading-header">
                           <h3 class="title"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/file-managment/main-folder-icon.svg"><span>File Management
                           </span></h3>
                     <div class="dropdown">
                       <div class="creat_btn_cst dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> 
                           <a href="javascript:void(0)" class="uplod_crt_new">
                               <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/file-managment/create-folder.svg"><!-- <i class="fas fa-plus"></i> --> <span>New</span>
                           </a>
                       </div>
                       <div class="creat_btn_css dropdown-menu" role="menu">
                           <ul>
                               <li>
                                   <a href="javascript:void(0)" data-toggle="modal" data-target="#DirectorySave"  class="create_directory">
                                       <i class="fas fa-folder-plus"></i><span>Folder</span>
                                   </a>
                               </li>
                               <li> 
                                   <a href="javascript:void(0)" data-toggle="modal" data-target="#f_uploadfiles_custom"  class="create_directory upldflsfrmcstom">
                                       <i class="fas fa-file-upload"></i><span>Files</span>
                                   </a>
                               </li>
                           </ul>
                       </div>
                   </div>
               </div>
               <input type="hidden" id="current_active_folder">
               <div class="customer_designer_directory">
                <a href="javascript:void(0)" class="customer_directory active" id="default_folder" data-role="default_folder" data-bc ="default_folder">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/file-managment/defult-icon.svg">
                    Default folders
                </a>
                <a href="javascript:void(0)" class="customer_directory" data-bc ="Custom-Folder" data-role="custom_structure" id="custom_structure">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/file-managment/custom-folder-icon.svg">
                    My Folders
                </a>
                <a href="javascript:void(0)" class="customer_directory" id="project_customer" data-role="projects" data-bc ="projects">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/file-managment/request-icon.svg">
                    Projects
                </a>
                <a href="javascript:void(0)" class="customer_directory" data-bc ="brand" data-role="brands">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/file-managment/brand-profile-icon.svg">
                    Brand Profiles
                </a>

            </div>
        </div>
        <div class="allsub_folder">
            <div class="file_header">
                <div class="folder_bradcromb">
                    <input type="hidden" id="caseForCustomFolder" value="">
                    <button type="button" style="display:none;" class="go_back_file" data-url="" onclick="goback(this)" data-count="0" data-sbfolder="0" aria-label="Close">  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/file-managment/back-btn.svg"></button>
                    <button type="button" style="display:none;" class="go_back_file_cst" data-url=""  aria-label="Close">  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/file-managment/back-btn.svg"></button>
                    <ul class="breadcrumb_name" style="display:none">
                    </ul>
                </div>
                <div class="search_col">
                    <div class="search_wrapper">
                        <div class="search_box">
                            <i id="srch_spinner" class="fas fa-spinner fa-spin" style="font-size:24px;display:none; "></i>
                            <i class="fas fa-search"></i>
                            <input type="text" placeholder="Search..." id="fm_searchBox">
                        </div>
                        <div class="search_result" style="display: none;">
                            <ul class="search_resultUl" id="serach_resultListing">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="folders_body">
                <div class="section_title_header">
                    <div class="title">
                        <h3>Default folders</h3>
                    </div>
                    <div class="upload_btn_cst">
                    </div>
                </div>
                <div class="overflow_control customer_designer_requests" style="display:none;">
                </div>
                <div class="overflow_control">
                    <div class="requests_files" style="display:none;">
                        <ul class="all_request_file same-pattrn">
                          
                        </ul>
                    </div>
                    <div class="requests_all_files" style="display:none;">
                        <ul class="all_request_files diffrent-pattrn">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</div>
</div>
</div>
</section>
<!-- Multiple brand file upload from folder structure Modal -->
<div class="modal similar-prop  fade" id="f_uploadbrand" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Add File</h2>
                    <div class="cross_popup close edit_close" data-dismiss="modal" aria-label="Close">x</div>
                </header>

                <div class="cli-ent-model">

                    <div class="selectimagespopup">
                        <form method="post" action="" enctype="multipart/form-data" id="f_uplod_brnd_profl">
                            <select name="file_type" class="file_type">
                                <option value="logo_upload">Brand Logo</option>
                                <option value="materials_upload">Marketing Materials</option>
                                <option value="additional_upload">Additional Images</option>
                            </select>
                            <div class="form-group goup-x1 goup_1">
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo FS_PATH; ?>public/assets/img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input type="hidden" class="f_file_id" multiple="" name="id" id="f_file_id"/>    
                                    <input type="file" class="file-input project-file-input" multiple="" name="file_upload[]" id="file_input"/>
                                    

                                </div>
                                <p class="allowed-type  text-left">Allowed file types : Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov,Ppt, Pptx </p>
                                <p class="space-e"></p>
                                <div class="uploadFileListContain row">                      
                                </div>
                                <div class="err_msg_upldfl"></div>
                            </div>
                            <div class="loading_file_procs"></div>
                            <p class="btn-x"><input type="submit" value="Submit" name="attach_file" class="btn-g brandprofile_upload button" style="border-radius: 7px;margin: auto;display: block;margin-top: 10px;"/></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
