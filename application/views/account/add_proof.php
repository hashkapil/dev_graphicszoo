<section id="add-proof">
    <div class="container">
        <div class="header-blog">
            <div class="alert-danger alert-dismissable" style="display:none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c"></p>
            </div>
        </div>
        <form method="post" id="proof_requests_form" enctype="multipart/form-data">
<!--            <input type="hidden" name="title" value="">-->
            <input type="hidden" name="request_id" value="" id="proof_req_id">
            <div class="row">
                <?php
                $editvar = '';
                $editID = isset($_GET['reqid']) ? $_GET['reqid'] : '';
                $dupID = isset($_GET['did']) ? $_GET['did'] : '';
                if ($editID && $editID != '') {
                    $editvar = $existeddata[0];
                }if ($dupID && $dupID != '') {
                    $editvar = $duplicatedata[0];
                }if ($editID == '' && $dupID == '') {
                    $editvar = '';
                }
                ?>
                <div class="col-lg-12 catagry-heading">
                    <h3>Create Proof:<span class="proof_name"><input type="text" name="title" value="New Proof"> </span></h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class="catagory">
                        <div class="step-row">
                            <h3>Step 1.</h3>
                            <span class="desc_steps">Select the category and sub-category that best suits your proof.</span>
                        </div>
                        <div class="form-group">
                            <select id="cat_page" class="form-control select-a" required name="category_id" onchange="getval(this);">
                                <?php foreach ($allcategories as $key => $value) { ?>
                                    <option value="<?php echo $value['id']; ?>" 
                                            <?php echo ($value['id'] == $editvar['category_id'] || $value['name'] == $editvar['category']) ? 'selected' : ''; ?>>
                                        <?php echo $value['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="error_msg"></div>
                        <div class="plan-boxex-xx5 clearfix two-can" id="boxes">
                            <?php foreach ($allcategories as $kp => $vp) { ?>
                                <div class="form-group" class="hide-prev" id="<?php echo "child_" . $vp['id']; ?>" style="display:none">
                                    <div class="row">
                                        <?php foreach ($vp['child'] as $kc => $vc) {
                                            ?>
                                            <div class="col-xs-6 col-md-2">
                                                <label class="radio-box-xx2 <?php echo ($vc['agency_only'] == '1') ? 'allowed_agency_file' : ''; ?>"> 
                                                    <input name="subcategory_id" class="subcategory_class"  value="<?php echo $vc['id']; ?>" type="radio" <?php echo ($editvar['subcategory_id'] == $vc['id'] || $editvar['logo_brand'] == $vc['name']) ? 'checked' : ''; ?> required="">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3">
                                                        <?php if ($is_trial == 1) { ?><span class="upgrade-label"></span><?php } ?>
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/<?php echo $vc['image_url']; ?>" class="img-responsive">
                                                        </figure>										
                                                        <h3><?php echo $vc['name']; ?></h3>
                                                    </div>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row for_loading_while">
                <section class="color-sec">
                    <div class="drop-file-wrap">
                        <div class="step-row">
                            <h3 class="step_upl2">Step 2</h3>
                            <span class="desc_steps">What to review ?</span>
                        </div>         
                        <div class="upload-draft-colm white-boundries">
                            <div class="form-group goup-x1 goup_2 text-left">
                                <label class="label-x3 ">Preview File:</label>
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input class="file-input" type="file" name="preview_file"  onchange="validateAndUploadP(this);" accept="image/*" required="">
                                    <div class="dropify-preview" style="display: none; width: 100%;">
                                        <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                        <div class="overlay">
                                            <div class="rem_btn">
                                                <button class="proof_remove_selected">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="allowed-type text-center"> <i>Allowed file types : jpg, jpeg, png, gif</i>
                                <div class="form-radion2">
                                    <label class="containerr">Add Source File
                                        <input type="checkbox" name="is_src_file" class="is_src_file" value="">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                </div>
                            </div>
                            <div class="form-group goup-x1 goup_1 text-left source_section" style="display: none">
                                <label class="label-x3">Source File:</label>
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input class="file-input" type="file" name="src_file" onchange="validateAndUploadS(this);">
                                    <div class="dropify-preview" style="display: none; width: 100%;">
                                        <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                        <div class="overlay">
                                            <div class="rem_btn">
                                                <button class="proof_remove_selected">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
<!--            <div class="col-lg-12 col-md-12">
                <div class="step-row">
                    <h3 class="step_upl2">Step 3</h3>
                    <span class="desc_steps">Reviewers</span>
                </div> 
                <label class="form-group">
                    <p class="label-txt label-active"></p>
                    <?php
                    if (!empty($editvar['google_link'])) {
                        $links_arr = explode(',', $editvar['google_link']);
                        foreach ($links_arr as $lk => $lv) {
                            ?>
                            <input type="text" name="reviewer[]" class="input reviewrs_fetch f_cls_nm" data-count_val="" value="<?php echo isset($lv) ? $lv : ''; ?>"/>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        <?php }
                    } else {
                        ?>
                        <input type="text" name="reviewer[]" class="input reviewrs_fetch f_cls_nm" data-count_val="1" id="reviewrs_fetch_1" placeholder="username or email address"/>
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    <?php } ?>
                    <a class="add_more_reviews">
                        <i class="icon-gz_plus_icon"></i>
                    </a>
                    <div class="input_fields_container"></div>
                </label>
            </div>-->
            <div class="loading_account_procs"></div>
            <div class="col-lg-12 ad_new_reqz text-center">
                <input type="submit" class="theme-save-btn" id="proof_request_submit" data-count="1" value="Submit" name="proof_submit"/>
            </div>
        </form>
    </div>
</section>
<script>
    function validateAndUploadP(input) {
        var URL = window.URL || window.webkitURL;
        var file = input.files[0];
        var name = file.name;
        var filename = name.split(".");
        //$('.proof_name').html(filename[0]);
        $("input[type=text][name=title]").val(filename[0]);
        if ($("#preview_image").parents(".file-drop-area").hasClass("red-alert")) {
            $("#preview_image").parents(".file-drop-area").removeClass("red-alert");
        }
        if (file.type.indexOf('image/') !== 0) {
            this.value = null;
            //console.log("invalid");
        }
        else {
            if (file) {
                // console.log(URL.createObjectURL(file));
                $(".goup_2 .file-drop-area span").hide();
                $(".goup_2 .file-drop-area .dropify-preview").css('display', 'block');
                $(".goup_2 .img_dropify").attr('src', URL.createObjectURL(file));
            }
        }

    }

    $(document).on('click', ".proof_remove_selected", function (e) {
        e.preventDefault();
        $(this).closest('.dropify-preview').css('display', 'none');
        $(this).closest('.file-drop-area').find('input').val('');
        $(this).closest('.file-drop-area').find('span').show();
        $(this).closest('.file-drop-area').find('.file-msg').html('Drag and drop file here or <span class="nocolsl">Click Here</span>');
    });

    function validateAndUploadS(input) {
        var URL = window.URL || window.webkitURL;
        var file = input.files[0];
        var exten = $(input).val().split('.').pop();
        var imgext = ['jpg', 'jpeg', 'png', 'gif', 'PNG', 'JPG', 'JPEG'];

        if ($("#sour").parents(".file-drop-area").hasClass("red-alert")) {
            $("#sour").parents(".file-drop-area").removeClass("red-alert");
        }

        if (exten == "zip" || exten == "rar") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-archive-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="proof_remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else if (exten == "docx" || exten == "doc") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-word-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="proof_remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else if (exten == "pdf") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-pdf-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="proof_remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else if (exten == "txt") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-text-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="proof_remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        }
        else if (exten == "psd") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="proof_remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        }
        else if (exten == "ai") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="proof_remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else if (jQuery.inArray(exten, imgext) != '-1') {
            //console.log(URL.createObjectURL(file));
            $(".goup_1 .file-drop-area .dropify-preview").html('<img src="' + URL.createObjectURL(file) + '" class="img_dropify" style="height: 150px; width: 100%;"><div class="overlay"><div class="rem_btn"><button class="proof_remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg">You have selected <strong>' + exten + '</strong> file</h4><div class="overlay"><div class="rem_btn"><button class="proof_remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        }
    }
    $("#proof_requests_form").submit(function (e){
        e.preventDefault();
        var formData = new FormData(this);
        $('.loading_account_procs').html('<span class="loading_process" style="text-align:center;"><img src="'+$assets_path+'img/customer/gz_customer_loader.gif" alt="loading"/ style="margin: auto;display: block;"></span>');
        $.ajax({
            type: 'POST',
            url: baseUrl + "account/Request/add_proof",
            dataType: "json", 
            data: formData,
            contentType: false,
            processData: false,
            success: function (res) {
                $("#proof_req_id").val(res.id);
                if(res.status == 1){
                    window.location.replace(baseUrl + "account/request/project_image_view/"+res.draft_id+"?id="+res.id);
                } else{
                    $('html, body').animate({
                        scrollTop: 0
                    }, 800, function(){
                        $(".alert-danger").css("display","block");
                        $(".alert-danger p").html(res.msg);
                    });
                }
                $('.loading_account_procs').html("");
            }
        });
        });
</script>
