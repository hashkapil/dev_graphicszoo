<div class='footer_section'>
    <div class='footer_inner'>
        <ul>
            <li>&copy; Copyright 2018</li>
            <li>|</li>
            <li>GraphicsZoo</li>
            <li>|</li>
            <li>All Rights Reserved</li>
        </ul>
    </div>
    <div class='footer_sec'>
        <p>Connect</p>
        <ul>
            <li><img src='<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/emailtemplate_img/fb.png'></li>
            <li><img src='<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/emailtemplate_img/insta.png'></li>
        </ul>
    </div>
</div>