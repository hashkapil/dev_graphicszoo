<!DOCTYPE html>
<html>
<head>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
	<title>Email Template</title>
	<style type='text/css'>
              .header_section{
                background: #102132;
                padding: 25px;
            }
		.container{
			width: 50%;
			margin: 0 auto;
		}
		.title{
			font:normal 30px/30px 'GothamPro-Medium', sans-serif;
			font-weight: 800;
    		color: #e8304d;
    		margin: 50px 35px 30px 35px;
		}
		.content_text{
			font:normal 20px/30px 'GothamPro-Medium', sans-serif;
			color: #20364c;
			margin: 0px 26px 30px 35px;
		}
		a.verify_btn {
		    background: #e8304d;
		    color: #fff;
		    text-decoration: none;
		    font: normal 16px/30px 'GothamPro-Medium', sans-serif;
		    padding: 14px 29px;
		    border-radius: 25px;
		    text-transform: uppercase;
		    font-weight: bold;
		}
		.header_img{
			width: 100%;
		}
		.button_verify{
			margin: 0px 35px 30px 35px;
		}
		a.expire_count {
			color: #e8304d;
			text-decoration: none;
		}
		.content_sec {
		    margin-top: 125px;
		}
		.footer_section{
		    background-image: url('<?php echo FS_PATH_PUBLIC_ASSETS;?>img/emailtemplate_img/footer.png');
		    background-size: 100% 100%;
		    background-repeat: no-repeat;
		    display: flex;
		}
		.footer_sec {
		    width: 30%;
		}
		.footer_inner{
			width: 70%;
		    padding: 25px 30px;
		}
		.footer_inner ul {
		    display: inline-flex;
		    list-style: none;
		    padding-left: 30px;
		}
		.footer_inner ul li{
		    margin-left: 10px;
		    font: 13px/15px 'GothamPro-Medium', sans-serif;
		    color: #91accf;
		    font-weight: 600;
		}
		.footer_sec ul {
		    display: inline-flex;
		    list-style: none;
		    padding-left: 0px;
		}
		.footer_sec ul li{
    		margin-left: 8px;
		}
		.footer_sec p{
			font: 13px/15px 'GothamPro-Medium', sans-serif;
			color: #fff;
			margin-bottom: 0px;
			font-weight: 600;
		}
		@media (max-width: 768px){
			.container{
				width: 70%;
			}
			.footer_section {
				display: block;
			}
			.footer_inner {
			    width: 100%;
			    padding: 25px 0px;
			}
			.footer_sec {
			    width: 100%;
			    text-align: center;
			}
		}
		@media (max-width: 425px){
			.container{
				width: 100%;
			}
			.footer_inner {
			    padding:0px;
			}
			.content_text {
			    font: normal 17px/30px 'GothamPro-Medium', sans-serif;
			}
		}
	</style>
</head>
<body>
	<div class='main_div'>
		<div class='container'>
			<div class='header_section'>
                            <img style="width: 100%;max-width: 250px;" class='header_img' src='<?php echo FS_PATH_PUBLIC_ASSETS;?>img/emailtemplate_img/logo.png'>
			</div>
			<div class='body_Section'>
				<div class='content'>
					<h1 class='title'>
						Welcome to GraphicsZoo
					</h1>
					<h3 class='content_text'>
						We just need to verify your email address to complete your GraphicsZoo signup.
					</h3>
					<div class='button_verify'>
						<a href="<?php echo base_url();?>welcome/activation/<?php echo $user_id;?>/<?php echo $activation; ?>" class='verify_btn'>Verify Email Address</a>
					</div>
				</div>
				<div class='content_sec'>
					<h3 class='content_text'>
						Please note that this link will expire in <a href='javascript:void(0)' class='expire_count'>5 days</a>.
						<br>
						If you haven't signed up to GraphicsZoo, Please ignore this email.
					</h3>
				</div>
			</div>
			<?php $this->load->view('emailtemplate/email_footer'); ?>
		</div>
	</div>
</body>
</html>